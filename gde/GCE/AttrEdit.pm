package GCE::AttrEdit;

=head1 NAME

  GCE::AttrEdit - an edit wiget for attributes

=cut

use Gtk2;
use Gtk2::Gdk::Keysyms;
use Gtk2::SimpleList;

use GCE::Util;
use GCE::InventoryEditor;
use GCE::AttachEditor;

use Glib::Object::Subclass
    Gtk2::HPaned;

use AnyEvent::EditText;

use Deliantra;
use strict;

sub save_layout {
   my ($self) = @_;

   $::CFG->{attr_view_hpane_pos} = $self->get_position;
   $::CFG->{attr_view_show_use}  = $self->{use_btn}->get_active * 1;

   $self->{attach_editor}->save_layout
      if $self->{attach_editor};
}

sub load_layout {
   my ($self) = @_;

   $self->set_position ($::CFG->{attr_view_hpane_pos} || 350);
   $self->{use_btn}->set_active ($::CFG->{attr_view_show_use} * 1);

   $self->{use_al}->remove ($_) for $self->{use_al}->get_children;
   if ($self->{use_btn}->get_active) {
      $self->{use_al}->add ($self->{use_lbl});
      $self->{use_lbl}->show;
   }
}

sub INIT_INSTANCE {
   my ($self) = @_;

   my $pb = $self->{arch_pb} = new_arch_pb;

   $self->add (my $topvb = Gtk2::VBox->new);
      $topvb->pack_start (my $menubar = Gtk2::MenuBar->new, 0, 0, 0);
         $menubar->append (my $actions_menu = Gtk2::MenuItem->new ('Actions'));
            my $actmenu = Gtk2::Menu->new;
            $actmenu->append (my $defitm = Gtk2::MenuItem->new ('reset to defaults'));
               $defitm->signal_connect (activate => sub {
                  my $ar = $self->{archref};
                  $ar->reset_to_defaults;
                  #XXXAR $self->set_arch ($arch, $self->{change_cb});
               });

            $actmenu->append (my $attitm = Gtk2::MenuItem->new ('attach'));
               $attitm->signal_connect (activate => sub {
                  my $ar = $self->{archref};
                  return unless $ar;
                  unless ($self->{attach_editor}) {
                     my $w = GCE::AttachEditor->new;
                     $w->set_attachment (
                        $ar->get ('attach'),
                        sub {
                           if (@{$_[0]}) {
                              $ar->set_silent (attach => $_[0])
                           } else {
                              $ar->set_silent (attach => undef)
                           }
                        }
                     );
                     $self->{attach_editor} = $w;
                     $w->signal_connect (destroy => sub { delete $self->{attach_editor} });
                     $w->show_all;
                  }
               });

            $actions_menu->set_submenu ($actmenu);
            $actmenu->show_all;

      $topvb->pack_start (my $hb2 = Gtk2::HBox->new, 0, 1, 0);
         $hb2->pack_start (my $img = $self->{arch_img} = (new_from_pixbuf Gtk2::Image $pb), 0, 0, 0);
         $img->set_alignment (0, 0.5);

         $hb2->pack_start (my $lbl = $self->{arch_name_lbl} = Gtk2::Label->new, 0, 0, 0);
         $lbl->set_alignment (0, 0.5);

         $hb2->pack_start (my $statbtn = $self->{arch_stat_btn} = Gtk2::Button->new, 0, 0, 0);

      $topvb->pack_start (my $docal = Gtk2::Alignment->new (0, 0.5, 0, 1), 0, 1, 0);
      $topvb->pack_start (my $usebtn = $self->{use_btn} = Gtk2::ToggleButton->new ('show use'), 0, 1, 0);
      $topvb->pack_start (my $useal = $self->{use_al} = Gtk2::Alignment->new (0, 0.5, 0, 1), 0, 1, 0);
      $topvb->pack_start (my $ntbook = $self->{ntbook} = Gtk2::Notebook->new, 1, 1, 0);
         $ntbook->set_scrollable (1);
         $docal->add ($self->{doc_lbl} = Gtk2::Label->new);

         $usebtn->set_active (0);
         $self->{use_lbl} = Gtk2::Label->new;
         $usebtn->signal_connect (toggled => sub {
            my ($usebtn) = @_;

            $useal->remove ($_) for $useal->get_children;
            if ($usebtn->get_active) {
               $useal->add ($self->{use_lbl});
               $self->{use_lbl}->show;
            }
         });
         $self->{doc_lbl}->set_line_wrap (1);
         $self->{use_lbl}->set_line_wrap (1);

   $self->add2 (my $sw = Gtk2::ScrolledWindow->new);
      $sw->set_policy ('automatic', 'automatic');
      $sw->add_with_viewport (my $inv = $self->{inv_edit} = GCE::InventoryEditor->new);
}

#sub spawn_editor {
#   my ($arch, $cb) = @_;
#
#   my $w = Gtk2::Window->new;
#   $w->set_title ("gce - edit attrs");
#   $w->add (my $ae = GCE::AttrEdit->new);
#
#   main::set_pos_and_size ($w, $main::CFG->{attr_view}, 200, 200);
#
#   $ae->set_arch ($arch, $cb);
#   $w->set_title ("gce - edit $arch->{_name}");
#
#   $w->show_all;
#}

sub update_arch {
   my ($self, $ar, $key, $value) = @_;
   $ar->set ($key, $value);
}

sub set_attr {
   my ($self, $key, $value) = @_;

   my $attr = $self->{archref}->get ($key);

   unless (ref $attr) {

      $self->{archref}->set ($key, $value);
   }
}

sub get_arch {
   my ($self) = @_;

   $self->{archref}
}

#sub update {
#   my ($self, $narch) = @_;
#
#   if (ref ($narch) ne 'GCE::ArchRef') { require Carp; Carp::confess ("NO ARCH REF!") }
#
#   if ($narch) {
#      $self->set_arch ($narch);
#   } else {
#      $self->set_arch ($self->{archref});
#   }
#}

sub set_arch {
   my ($self, $ar, $clear_inv) = @_;

   if ((defined $self->{archref}) && ($self->{archref} != $ar)) {
      $self->{archref}->remove_on_change ('attredit')
         if defined $self->{archref};
      $ar->add_on_change (attredit => sub { $self->set_arch ($_[0]) });
      $self->{attach_editor}->destroy if $self->{attach_editor};

   } elsif (not defined $self->{archref}) {
      $ar->add_on_change (attredit => sub { $self->set_arch ($_[0]) });
   }

   $self->{inv_edit}->clear_inv_hist if $clear_inv;
   $self->{inv_edit}->set_arch ($ar);

   $self->{archref} = $ar;

   $self->{arch_name_lbl}->set_text ($ar->longname);

   fill_pb_from_arch ($self->{arch_pb}, $ar->getarch);
   $self->{arch_img}->set_from_pixbuf ($self->{arch_pb});
   $self->label_set_color ($self->{arch_name_lbl}, 0);

   $self->{arch_stat_btn}->set_label ($ar->{source});

   # get current page (to remember it for later)
   my $pgnum = $self->{ntbook}->get_current_page;
   my $curwid = $self->{ntbook}->get_nth_page ($pgnum);
   my $curpage_text =
      defined $curwid
         ? $self->{ntbook}->get_tab_label_text ($curwid) 
         : undef;

   my $al_arch = $ar->archetype;

   $self->hide;
   $self->{ntbook}->remove ($_)
      for $self->{ntbook}->get_children;

   $self->{ttip} = Gtk2::Tooltips->new;

   my $type = $ar->type;

   for my $sec (@{$type->{section}}) {
      my $secname = shift @$sec;
      $self->add_section_edit_widgets ($self->{ntbook}, $secname, $ar, $sec);
   }

   for my $key (qw/lore msg/) {

      $self->{ntbook}->append_page (my $v = Gtk2::VBox->new, $key);
         $v->pack_start (my $ed = Gtk2::Button->new ("Open Editor"), 0, 1, 0);
         $v->pack_start (my $sw = Gtk2::ScrolledWindow->new, 1, 1, 0);
            $sw->set_policy ('automatic', 'automatic');
            $sw->add (my $tb = $self->{"${key}_txt"} = Gtk2::TextView->new);

               my $change_field = sub {
                  my ($txt) = @_;

                  my $buf = $tb->get_buffer;

                  my $old_txt = $buf->get_text ($buf->get_start_iter, $buf->get_end_iter, 0);
                  $txt = $old_txt unless defined $txt;

                  $ar->set_silent ($key, $txt)
                     if $txt ne $ar->get ($key);
                  $buf->set_text ($txt)
                     if $txt ne $old_txt;
               };

               my $buf = $tb->get_buffer;
               $change_field->($ar->get ($key));
               $buf->signal_connect (changed => sub {
                  $change_field->();
                  1
               });

      $ed->signal_connect (clicked => sub {
         AnyEvent::EditText::edit ($ar->get ($key), sub {
            my ($new, $change, $err) = @_;

            if (not defined $new) {
               quick_msg ("Error while starting editor: $err");
               return;
            }

            $change_field->($new)
               if $change;
         });
      });
   }

   my $desc = pseudohtml2txt $type->{desc};
   my $use  = pseudohtml2txt $type->{use};
   $self->{doc_lbl}->set_text ($desc);
   $self->{use_lbl}->set_text ($use);

   $self->{ttip}->enable;

   $self->show_all;

   # reset the current page if found
   # XXX: it's braindamaged: it has to be done AFTER show all for some reason
   if (defined $curpage_text) {

      for (my $i = 0; $i <= $self->{ntbook}->get_n_pages; $i++) {
         my $w = $self->{ntbook}->get_nth_page ($i);

         if ($w && $self->{ntbook}->get_tab_label_text ($w) eq $curpage_text) {
            $self->{ntbook}->set_current_page ($i);
            last;
         }
      }
   }
}

sub add_section_edit_widgets {
   my ($self, $ntbook, $name, $ar, $section) = @_;

   $self->{ntbook}->append_page (my $sw = Gtk2::ScrolledWindow->new, $name);
      $sw->set_policy ('automatic', 'automatic');
      $sw->add_with_viewport (my $vb = Gtk2::VBox->new);
      $vb->pack_start (my $table = new Gtk2::Table (2, (scalar @$section) + 1), 0, 1, 0);

   my $i = 0;
   for my $sec (@$section) {
      my ($key, $sec) = ($sec->[0], $sec->[1]);

      next if grep { $key eq $_ } qw/msg lore/;

      my $bwid = Gtk2::EventBox->new;
      $bwid->add (my $al = Gtk2::Alignment->new (0.0, 0.5, 0, 1));
         $al->add (Gtk2::Label->new (def ($sec->{name}, $key)));

      if ($sec->{desc} !~ m/^\s*$/s) {
         $self->{ttip}->set_tip ($bwid, $sec->{desc});
      }

      $table->attach ($bwid, 0, 1, $i, $i + 1, ['shrink','fill'], 'fill', 5, 0);

      $al = Gtk2::Alignment->new (0.0, 0.5, 1, 0);
      $table->attach ($al,   1, 2, $i, $i + 1, ['expand', 'fill'], 'expand', 0, 0);
         $al->add ($self->get_edit_widget ($key, $sec, $ar, $bwid));

      $i++;
   }
}

sub label_set_color {
   my ($self, $lbl, $dark) = @_;
   if ($dark) {
      for (qw/normal active prelight selected insensitive/) {
         $lbl->modify_bg ($_, $lbl->get_default_style->bg ('active'));
         $lbl->modify_fg ($_, $lbl->get_default_style->fg ('active'));
      }

  } else {
     for (qw/normal active prelight selected insensitive/) {
        $lbl->modify_bg ($_, $lbl->get_default_style->bg ($_));
     }
  }

}

sub label_set_color_default {
   my ($self, $lbl, $ar, $key, $val) = @_;
   require Carp; $ar or Carp::confess ("UNDEF");
   my $al_arch = $ar->archetype;
   $self->label_set_color ($lbl, $ar->field_value_is_default ($key, $val));

}

# XXX: Warning: Ugly code ahead:
sub get_edit_widget {
   my ($self, $key, $edspec, $ar, $lbl) = @_;

   my $type    = $edspec->{type};
   my $al_arch = $ar->archetype;

   if ($type eq 'bool') {
      my $boolval = def ($edspec->{value}, [0, 1]);


      $self->label_set_color_default ($lbl, $ar, $key, $ar->get_or_default ($key));

      my $chk = new Gtk2::CheckButton (def ($edspec->{name}, $key));
         $chk->set_active ($ar->get_or_default ($key) == $boolval->[1]);
         $chk->signal_connect (clicked => sub {
            my ($chk) = @_;

            $ar->set_silent ($key, $boolval->[$chk->get_active * 1], $type);

            $self->label_set_color_default ($lbl, $ar, $key, $boolval->[$chk->get_active * 1]);
         });

      $self->{ttip}->set_tip ($chk, $al_arch->{$key} * 1);

      return $chk

   } elsif (grep { $type eq $_ } qw/string int treasurelist float/) {
      $self->label_set_color_default ($lbl, $ar, $key, $ar->get_or_default ($key));

      my $entry = new Gtk2::Entry;
         $entry->set_text ($ar->get_or_default ($key));
         $entry->signal_connect (changed => sub {
            my ($entry) = @_;
            $self->label_set_color_default ($lbl, $ar, $key, $entry->get_text);
            $ar->set_silent ($key, $entry->get_text);
            1
         });

      $self->{ttip}->set_tip ($entry, $ar->archetype->{$key});

      return $entry

   } elsif ($type eq 'spell' or $type eq 'nz_spell') { # XXX: nz_spell bug in datafiles?
      my $comb = Gtk2::ComboBox->new_text;
      my $spells_idx = {};
      my $spells_cmd_idx = {};
      my $sp = \%Deliantra::Data::SPELL;

      $comb->append_text ("<none>");

      my $idx = 1; # XXX: replace this idx with a more save/correct method?
      for (sort { $sp->{$a} cmp $sp->{$b} } keys %$sp) {
         $spells_cmd_idx->{$idx} = $_;
         $spells_idx->{$_} = $idx++;

         $comb->append_text ($sp->{$_});
      }
      #XXX: FIXME: $self->{ttip}->set_tip ($comb, $sp->{$al_arch->{$key}});

      $comb->set_active ($spells_idx->{$ar->get_or_default ($key)});
      $self->label_set_color_default ($lbl, $ar, $key, $ar->get_or_default ($key));

      $comb->signal_connect (changed => sub {
         my ($comb) = @_;
         $self->label_set_color_default ($lbl, $ar, $key, $spells_cmd_idx->{$comb->get_active});
         $ar->set_silent ($key, $spells_cmd_idx->{$comb->get_active});
      });
      return $comb

   } elsif ($type eq 'bitmask') {
      my $chval = $ar->get_or_default ($key);
      my $btn = Gtk2::Button->new;
      $btn->add (my $lblb = Gtk2::Label->new ("bitmask: " . ($chval * 1)));
      $self->{ttip}->set_tip ($btn, $al_arch->{$key});

      my $menu = $self->create_bitmask_menu ($edspec->{value}, $lbl, $lblb, $ar, $key, \$chval);

      $self->label_set_color_default ($lbl, $ar, $key, $chval);

      $btn->signal_connect (button_press_event => sub {
         my ($btn, $ev) = @_;
         $menu->popup (undef, undef, undef, undef, $ev->button, 0);
      });

      return $btn;

   } elsif ($type eq 'list') {
      my $lblb = Gtk2::Label->new ($edspec->{value}->{$ar->get_or_default ($key) * 1});
      my $btn = Gtk2::Button->new;
      $self->{ttip}->set_tip ($btn, $edspec->{value}->{$al_arch->{$key}});
      $btn->add ($lblb);
      my $menu = $self->create_list_menu ($edspec->{value}, $lbl, $lblb, $ar, $key);

      $self->label_set_color_default ($lbl, $ar, $key, $ar->get_or_default ($key));

      $btn->signal_connect (button_press_event => sub {
         my ($btn, $ev) = @_;
         $menu->popup (undef, undef, undef, undef, $ev->button, 0);
      });
      return $btn;

   } elsif ($type eq 'fixed') {
      return Gtk2::Label->new ("$edspec->{name} = $edspec->{value}");

   } elsif ($type eq 'text') {
      return Gtk2::Label->new ("<see $key tab>");

   } elsif ($type eq 'movement_type') { # ok... this is quite a big one... awww
      my $a = Gtk2::Alignment->new (0, 0, 0, 0);
      $a->add (my $v = Gtk2::VBox->new (0, 0));
      $a->set_padding (4, 4, 0, 0);
      my $btns1 = Gtk2::HButtonBox->new;
      my $btns2 = Gtk2::HButtonBox->new;
      $btns1->set_layout ('start');
      $btns2->set_layout ('start');

      $v->pack_start ($btns1, 0, 1, 0);
      $v->pack_start ($btns2, 0, 1, 0);

      my @lblbtns;
      my $calc_lbl = sub { # update callback for the buttons
         for (@lblbtns) {
            my $btn = $_->[0];
            my $lbl = $_->[1];

            if (defined $ar->get_or_default ($key)) {
               my $res =
                  Deliantra::MoveType->new ($ar->get_or_default ($key))
                     >= $lbl;

               if (defined $res) {
                  $self->label_set_color ($btn, 0);
                  $lbl = ($res ? "+" : "-") . $lbl;

               } else {
                  $self->label_set_color ($btn, 1);
                  $lbl = "$lbl?";
               }
            } else {
               $self->label_set_color ($btn, 1);
               $lbl = "$lbl?";
            }

            $btn->set_label ($lbl);
         }
      };

      $self->label_set_color_default ($lbl, $ar, $key, $ar->get_or_default ($key));

      my $mid = (scalar @Deliantra::MOVE_TYPE) / 2;
      my $cnt = 0;
      my $box = $btns1;

      for my $mty (@Deliantra::MOVE_TYPE) {
         $box->pack_start (my $btn = Gtk2::Button->new ($calc_lbl->()), 0, 1, 0);
         push @lblbtns, [$btn, $mty];

         $self->{ttip}->set_tip ($btn, $ar->archetype->{$key});
         $btn->signal_connect (clicked => sub {
            my $v = $ar->get ($key) || Deliantra::MoveType->new;

            $v x= $mty;
            $v = $v eq '' ? undef : $v;
            $ar->set_silent ($key, $v);

            $self->label_set_color_default (
               $lbl, $ar, $key, $ar->get_or_default ($key));
            $calc_lbl->();
         });

         if (++$cnt >= $mid) { $box = $btns2 } # for wrapping into the second button box
      }
      $calc_lbl->();

      return $a;

   } else {
      return Gtk2::Label->new ("$key => $edspec->{name} ($type)");

   }
}

sub bitmask_to_list {
   my ($self, $bitlist, $bits) = @_;

   my @l;
   for (%$bitlist) {
      if ($bits & (1 << $_)) {
         push @l, $bitlist->{$_};
      }
   }
   return @l;
}

sub create_list_menu {
   my ($self, $list, $clbl, $lbl, $ar, $key) = @_;

   my $menu = Gtk2::Menu->new;

   for my $item (sort keys %$list) {
      my $lbltxt = $list->{$item};
      my $menuitem = Gtk2::MenuItem->new_with_label ($lbltxt);
      $menuitem->signal_connect (activate => sub {
         my ($menuitem) = @_;
         $lbl->set_text ($list->{$item});
         $self->label_set_color_default ($clbl, $ar, $key, $item);
         $ar->set_silent ($key, $item);
      });
      $menu->append ($menuitem);
      $menuitem->show;
   }

   return $menu;
}

sub create_bitmask_menu {
   my ($self, $bits, $clbl, $lbl, $ar, $key, $rval) = @_;

   my $menu = Gtk2::Menu->new;

   for my $bit (sort { $a <=> $b } keys %$bits) {
      my $lbltxt = $bits->{$bit};
      my $menuitem = Gtk2::CheckMenuItem->new_with_label ($lbltxt);
      if ($$rval & (1 << $bit)) {
         $menuitem->set_active (1);#$arch->{$key} & (1 << $bit));
      }
      $menuitem->signal_connect (toggled => sub {
         my ($menuitem) = @_;
         my $newval = $ar->get ($key);
         $$rval &= ~(1 << $bit);
         $$rval |= (1 << $bit) if $menuitem->get_active;
         $lbl->set_text ("bitmask: " . ($$rval * 1));
         $self->label_set_color_default ($clbl, $ar, $key, $$rval);
         $ar->set_silent ($key, $$rval);
      });
      $menu->append ($menuitem);
      $menuitem->show;
   }

   return $menu;
}


=head1 AUTHOR

 Marc Lehmann <schmorp@schmorp.de>
 http://home.schmorp.de/

 Robin Redeker <elmex@ta-sa.org>
 http://www.ta-sa.org/

=cut
1;
