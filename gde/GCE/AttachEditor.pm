package GCE::AttachEditor;

=head1 NAME

  GCE::AttachEditor - this is a editor for attachments for maps and objects

=cut

use Gtk2;
use Gtk2::Gdk::Keysyms;
use Gtk2::SimpleMenu;

use Deliantra;

use GCE::Util;

use Glib::Object::Subclass Gtk2::Window;

use Storable qw/dclone/;

use strict;

sub try_add_known_attachment {
   my ($self, $att) = @_;

   unless ($::CFG->{known_attachments}->{$att}) {
      $self->{attach_combobox}->append_text ($att);
      $::CFG->{known_attachments}->{$att} = 1;
   }
}

sub save_layout {
   my ($self) = @_;
   $::CFG->{attach_editor} = ::get_pos_and_size ($self);
}

sub INIT_INSTANCE {
   my ($self) = @_;
   $self->set_title ("deliantra editor - attachment editor");
   ::set_pos_and_size ($self, $::CFG->{attach_editor}, 400, 400, 200, 0);

   $self->add (my $vb = Gtk2::VBox->new);
      $vb->pack_start (my $hb = Gtk2::HBox->new, 0, 1, 0);
         $hb->pack_start ($self->{attach_combobox} = Gtk2::ComboBoxEntry->new_text, 0, 1, 0);
         $hb->pack_start (my $btn = Gtk2::Button->new ("new"), 0, 1, 0);
            $btn->signal_connect (clicked => sub {
               my $ent = $self->{attach_combobox}->child->get_text;
               $self->add_attachment ($ent);
            });

      $vb->pack_start ($self->{attachbox} = Gtk2::VBox->new, 1, 1, 0);

   $self->{attach_combobox}->append_text ($_)
      for keys %{$::CFG->{known_attachments} || {}};
}

sub add_attachment {
   my ($self, $name, $args) = @_;

   return if $name =~ /^\s*$/;

   $self->try_add_known_attachment ($name);
   push @{$self->{attach}}, [$name, $args];
   $self->{ntbook}->append_page (my $pg = $self->make_page ($name, $args), $name);
   $pg->show_all;

   $self->{upd_cb}->($self->{attach});
}

sub update_attachments {
   my ($self, $name, $args) = @_;

   for (@{$self->{attach}}) {
      if ($_->[0] eq $name) {
         @$_ = ($name, $args);
         $name = undef;
         last;
      }
   }

   $name and warn "Couldn't find attachment '$name', this is a bug!";

   $self->{upd_cb}->($self->{attach});
}

sub kill_attachment {
   my ($self, $pos) = @_;
   my $name = $self->{ntbook}->get_tab_label_text ($self->{ntbook}->get_nth_page ($pos));
   $self->{ntbook}->remove_page ($pos);
   @{$self->{attach}} = grep { $_->[0] ne $name } @{$self->{attach}};
   $self->{upd_cb}->($self->{attach});
}

sub set_attachment {
   my ($self, $attach, $update_cb) = @_;
   $self->{upd_cb} = $update_cb
      or die "No update callback given!";

   my $ntbook = $self->{ntbook} = Gtk2::Notebook->new;
   $ntbook->set_scrollable (1);
   $self->add_attachment (@$_) for @$attach;

   $self->{attachbox}->remove ($_) for $self->{attachbox}->get_children;
   $self->{attachbox}->add ($ntbook);
   $self->{attachbox}->show_all;
}

sub add_attachment_key {
   my ($self, $name, $key, $args) = @_;

   my $tbl = $self->{table};
   my $i = $tbl->get ('n-rows');
   $tbl->resize ($i + 1, $self->{table}->get ('n-columns'));

   $args->{$key} = "" unless defined $args->{$key};

   $tbl->attach (my $ed = Gtk2::Entry->new, 0, 1, $i, $i + 1, ['fill'], 'fill', 0, 0);
   $ed->set_editable (0);
   $ed->set_text ($key);
#      $ed->signal_connect (changed => sub {
#         my ($ed) = @_;
#         my $newkey = $ed->get_text;
#         my $val = delete $args->{$ed->{key}};
#         $args->{$ed->{key} = $newkey} = $val;
#         $self->update_attachments ($name, $args);
#         0
#      });

   $tbl->attach (my $ed2 = Gtk2::Entry->new, 1, 2, $i, $i + 1, ['fill', 'expand'], 'fill', 0, 0);
   $ed2->set_text ($ed2->{value} = $args->{$key});
   $ed2->signal_connect (changed => sub {
      my ($ed2) = @_;
      $args->{$key} = $ed2->get_text;
      $self->update_attachments ($name, $args);
      0
   });

   $tbl->attach (my $b = Gtk2::Button->new ("X"), 2, 3, $i, $i + 1, 'fill', 'fill', 0, 0);
   $b->signal_connect (clicked => sub {
      delete $args->{$key};
      $self->rebuild_key_table ($name, $args);
      0
   });

   $tbl->show_all;
}

sub rebuild_key_table {
   my ($self, $name, $args) = @_;
   my $tbl = $self->{table};
   $tbl->resize (scalar keys %$args, $tbl->get ('n-columns'));
   $tbl->remove ($_) for $tbl->get_children;
   $self->add_attachment_key ($name, $_, $args) for keys %$args;
}


sub make_page {
   my ($self, $name, $args) = @_;

   my $vb = Gtk2::VBox->new;

   $vb->pack_start (my $kb = Gtk2::Button->new ("remove attachment"), 0, 1, 0);
   $kb->signal_connect (clicked => sub {
      $self->kill_attachment ($self->{ntbook}->page_num ($vb));
      1
   });

   $vb->pack_start (my $sw = Gtk2::ScrolledWindow->new, 1, 1, 0);
   $sw->set_policy (qw/automatic automatic/);
   $sw->add_with_viewport ($self->{table} = Gtk2::Table->new (1, 3));

   $self->add_attachment_key ($name, $_, $args) for keys %$args;

   $vb->pack_start (my $hb = Gtk2::HBox->new, 0, 1, 0);
   $hb->pack_start (my $ed = Gtk2::Entry->new, 1, 1, 0);
   $hb->pack_start (my $kb = Gtk2::Button->new ("new key"), 0, 1, 0);
      $kb->signal_connect (clicked => sub {
         my $newkey = $ed->get_text;
         return 0 if exists $args->{$newkey};
         $self->update_attachments ($name, $args);
         $self->add_attachment_key ($name, $newkey, $args);
         0
      });


   $vb
}

=head1 AUTHOR

 Robin Redeker <elmex@ta-sa.org>
 http://www.ta-sa.org/

=cut
1;

