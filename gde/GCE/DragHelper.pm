package GCE::DragHelper;

=head1 NAME

  GCE::MapEditor - the map editing widget

=cut

use Gtk2;
use Gtk2::Gdk::Keysyms;
use Gtk2::SimpleMenu;

use Deliantra;
use Deliantra::Map;
use Deliantra::MapWidget;

use GCE::AttrEdit;
use GCE::Util;

use strict;

our $DRAGDATA = {};

sub set_drag_source {
   my ($widget, $key, $value_cb) = @_;

   $widget->drag_source_set (['button1_mask'], ['move'],
      { target => 'STRING', flags => [], info => 'TARGET_STRING' }
   );
   $widget->signal_connect (drag_data_get => sub {
      my ($widget, $context, $data, $info, $time) = @_;
      $data->set ($data->target, 8, $key);
      $DRAGDATA->{$key} = $value_cb->();
   });
}

sub set_drag_sink {
   my ($widget, $key, $get_cb) = @_;

   $widget->drag_dest_set (all => ['move'], 
      { target => 'STRING', flags => [], info => 'TARGET_STRING' }
   );
   # XXX: I'm unsure here, do i have to issue a get request?
   # And what if i get the data twice? Wait for transaction end?
   $widget->signal_connect (drag_data_received => sub {
      my ($widget, $context, $wx, $wy, $data, $info, $time) = @_;

      if (($data->length >= 0) && ($data->format == 8)) {

         $context->finish (1, 0, $time);

         if ($data->data eq $key) {
            my $v = $DRAGDATA->{$data->data};
            $get_cb->($v);
         }

         return;
      }
      $context->finish (0, 0, $time);
   });

}

=head1 AUTHOR

 Marc Lehmann <schmorp@schmorp.de>
 http://home.schmorp.de/

 Robin Redeker <elmex@ta-sa.org>
 http://www.ta-sa.org/

=cut
1;
