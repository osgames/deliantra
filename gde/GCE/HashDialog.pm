package GCE::HashDialogue;

=head1 NAME

  GCE::HashDialogue - this is a editor for hashes (eg. for properties and meta info)

=cut

use Gtk2;
use Gtk2::Gdk::Keysyms;
use Gtk2::SimpleMenu;

use Deliantra;

use GCE::Util;

use Glib::Object::Subclass Gtk2::Window;

use Storable qw/dclone/;

use strict;

=head1 SYNOPSIS

   my $to_edit_hash = { test => 1, blah => "foo" },

   my $diag = GCE::HashDialogue->new ();
   $diag->init (
      layout_name => 'cool_dialog_cfg_layout',
      title       => 'Cool dialog!',
      ref_hash    => $to_edit_hash,
      dialog      => [
         [qw/blah   Blah string/],
         [undef, qw/x    sep/],
         [qw/test   Test check/],
      ],
   );

=cut

sub save_layout {
   my ($self) = @_;
   $::CFG->{$self->{layout_name}} = ::get_pos_and_size ($self);
}

sub _add_prop_entry {
   my ($self, $key, $desc, $type, $cb) = @_;

   my $table = $self->{table};
   my $idx   = $self->{table_idx};

   if ($type eq 'string' || $type eq 'password') {
      $table->attach_defaults (Gtk2::Label->new ($desc), 0, 1, $idx, $idx + 1);
      $table->attach_defaults (my $edwid = Gtk2::Entry->new, 1, 2, $idx, $idx + 1);
         $edwid->set (visibility => 0) if $type eq 'password';
         $edwid->set_text ($self->{edit_hash}{$key});
         $edwid->signal_connect (changed => sub {
            $self->{edit_hash}->{$key} = $_[0]->get_text;
         });

   } elsif ($type eq 'button') {
      $table->attach_defaults (my $b = Gtk2::Button->new_with_label ($desc), 0, 2, $idx, $idx + 1);
         $b->signal_connect (clicked => sub { $cb->(\$self->{edit_hash}{$key}) if $cb });

   } elsif ($type eq 'label') {
      $table->attach_defaults (Gtk2::Label->new ($desc), 0, 1, $idx, $idx + 1);
      $table->attach_defaults (Gtk2::Label->new ($self->{edit_hash}{$key}), 1, 2, $idx, $idx + 1);

   } elsif ($type eq 'desc') {
      $table->attach_defaults (Gtk2::Label->new ($desc), 0, 2, $idx, $idx + 1);

   } elsif ($type eq 'spin') {
      $table->attach_defaults (Gtk2::Label->new ($desc), 0, 1, $idx, $idx + 1);
      $table->attach_defaults (my $spn = Gtk2::SpinButton->new_with_range ($cb->()), 1, 2, $idx, $idx + 1);
         $spn->set_value ($self->{edit_hash}{$key});
         $spn->signal_connect (value_changed => sub {
            my ($spn) = @_;
            $self->{edit_hash}{$key} = $spn->get_value;
         });

   } elsif ($type eq 'check') {
      $table->attach_defaults (Gtk2::Label->new ($desc), 0, 1, $idx, $idx + 1);
      $table->attach_defaults (my $chk = Gtk2::CheckButton->new, 1, 2, $idx, $idx + 1);
         $chk->set_active ($self->{edit_hash}{$key});
         $chk->signal_connect (toggled => sub {
            my ($lbl) = @_;
            $self->{edit_hash}{$key} = $lbl->get_active ? 1 : undef;
         });

   } elsif ($type eq 'sep') {
      $table->attach_defaults (Gtk2::HSeparator->new, 0, 2, $idx, $idx + 1);

   } else {
      die "Unrecognized dialogue type: $type\n";
   }

   $self->{table_idx}++;
}

sub save {
   my ($self) = @_;

   if ($self->{save_cb}) {
      $self->{save_cb}->($self->{edit_hash});
   }

   for (@{$self->{hash_keys}}) {
      if ((defined $self->{edit_hash}->{$_}) && $self->{edit_hash}->{$_} ne "") {
         $self->{ref_hash}->{$_} = $self->{edit_hash}->{$_}
      } else {
         delete $self->{ref_hash}->{$_};
      }
   }
}

sub reset {
   my ($self) = @_;

   for (@{$self->{hash_keys}}) {
      $self->{edit_hash}->{$_} = $self->{ref_hash}->{$_};
   }
}

sub build_table {
   my ($self) = @_;
   $self->{table}->remove ($_) for $self->{table}->get_children;

   $self->{table_idx} = 0;

   for (@{$self->{dialog}}) {
      $self->_add_prop_entry (@$_);
   }
}

sub init {
   my ($self, %args) = @_;
   for (keys %args) { $self->{$_} = $args{$_} }

   $self->set_title ("deliantra editor - $self->{title}");

   ::set_pos_and_size ($self, $::CFG->{$self->{layout_name}}, @{$self->{dialog_default_size}});

   my $cfg_lines = scalar @{$self->{dialog}};

   for (@{$self->{dialog}}) {
      push @{$self->{hash_keys}}, $_->[0] if defined $_->[0];
   }

   $self->add (my $vb = Gtk2::VBox->new);
   $vb->pack_start (my $ilbl = Gtk2::Label->new ($self->{info}), 0, 1, 0)
      if $self->{info};
   $vb->pack_start (my $sw = Gtk2::ScrolledWindow->new, 1, 1, 0);
      $sw->set_policy ('automatic', 'automatic');
      $sw->add_with_viewport (my $v = Gtk2::VBox->new);
         $v->pack_start ($self->{table} = Gtk2::Table->new (2, $cfg_lines),     0, 0, 0);
   if ($self->{text_entry}) {
      $vb->pack_start (Gtk2::Label->new ($self->{text_entry}->{label}), 0, 1, 0)
         if $self->{text_entry}->{label};
      $vb->pack_start (my $txt = Gtk2::TextView->new, 1, 1, 0);
      # TODO/XXX: Implement hash-changes!
   }
   $vb->pack_start (my $hb = Gtk2::HBox->new (1, 0), 0, 0, 0);
      $hb->pack_start (my $save_btn  = Gtk2::Button->new ($self->{save_button_label} || "save"),   1, 1, 0);
      $hb->pack_start (my $reset_btn = Gtk2::Button->new ("reset"),  1, 1, 0);
      $hb->pack_start (my $close_btn = Gtk2::Button->new ("cancel"), 1, 1, 0);

   $self->reset;

   $self->build_table;

   $save_btn->signal_connect (clicked => sub {
      $self->save;
      $self->destroy if $self->{close_on_save};
   });

   $reset_btn->signal_connect (clicked => sub {
      $self->reset;
      $self->build_table;
      $self->show_all;
   });

   $close_btn->signal_connect (clicked => sub {
      $self->destroy;
   });
}

sub INIT_INSTANCE {
   my ($self) = @_;
}

1;
