package GCE::StackView;

=head1 NAME

  GCE::StackView - the stack window class for gce

=cut

use Gtk2;
use Gtk2::Gdk::Keysyms;
use Gtk2::SimpleMenu;

use Deliantra;
use Deliantra::MapWidget;

use GCE::AttrEdit;
use GCE::Util;

use Glib::Object::Subclass Gtk2::VBox;

use Storable qw/dclone/;

use strict;

sub INIT_INSTANCE {
   my ($self) = @_;

   $self->pack_start (my $sw = Gtk2::ScrolledWindow->new, 1, 1, 0);
      $sw->add_with_viewport ($self->{stackbox} = Gtk2::VBox->new);
      $sw->set_policy ('automatic', 'automatic');
}

sub maybe_update_stack_for {
   my ($self, $mapedit, $x, $y) = @_;
   if (my $d = $self->{stack_dest}) {
      if ($d->[0] eq $mapedit && $d->[1] == $x && $d->[2] == $y) {
         $self->set_stack
      }
   }
}

sub set_stack {
   my ($self, $mapedit, $x, $y) = @_;

   if (defined $mapedit) {
      $self->{stack_dest} = [$mapedit, $x, $y];
   } else {
      $self->set_stack (@{$self->{stack_dest}})
         if defined $self->{stack_dest};
      return;
   }

   for ($self->{stackbox}->get_children) {
      $self->{stackbox}->remove ($_);
   }

   my $stack = $mapedit->get ($x, $y);
   my $idx = $stack ? (@$stack - 1) : 0;

   if ($stack) {
      for (reverse @$stack) {
         # FIXME: How to change a stack with a virtual arch????
         if ($_->{_virtual}) {
            $idx--;
            next
         }
         my $ownidx = $idx;

         #my $a = $_->{_virtual} || $_;
         my $a = $_;

         # this is awful, is this really the best way?
         my $pb = new Gtk2::Gdk::Pixbuf 'rgb', 1, 8, TILESIZE, TILESIZE;

         fill_pb_from_arch ($pb, $a);

         $self->{stackbox}->pack_start (my $hb = Gtk2::HBox->new, 0, 0, 0);
            $hb->pack_start (my $delbtn = Gtk2::Button->new_with_label ('del'), 0, 0, 0);
               $delbtn->signal_connect (clicked => sub {

                   #my $oldstack = [ @$stack ];
                   splice @$stack, $ownidx, 1;

                   $mapedit->change_begin ('stackedit');
                   $mapedit->change_stack ($x, $y, $stack);

                   if (my $changeset = $mapedit->change_end) {
                      splice @{ $mapedit->{undo_stack} ||= [] },
                             $mapedit->{undo_stack_pos}++, 1e6,
                             $changeset;
                   }

                   # XXX: force an update ? maybe some more intelligent update later?
                   $self->set_stack ($mapedit, $x, $y);
               });

            $hb->pack_start (my $elemhdl = new Gtk2::Button, 0, 0, 0);
               $elemhdl->add (my $hb2 = Gtk2::HBox->new);
               $elemhdl->signal_connect (clicked => sub {
                  my $ar =
                     GCE::ArchRef->new (
                        arch => $a,
                        source => 'map',
                        cb => sub {
                           $mapedit->change_begin (ref $self);
                           $mapedit->change_stack ($x, $y, $stack);
                           # XXX: Put this into a generic function!!! See also EditTools.pm
                           # FIXME: Fix the automatic update on undo here!
                           if (my $changeset = $mapedit->change_end) {
                              splice @{ $mapedit->{undo_stack} ||= [] },
                                     $mapedit->{undo_stack_pos}++, 1e6,
                                     $changeset;
                           }
                        }
                     );

                  $::MAINWIN->update_attr_editor ($ar)
               });

                  $hb2->pack_start (my $img = (new_from_pixbuf Gtk2::Image $pb), 0, 0, 0);
                  $img->set_alignment (0, 0.5);

                  $hb2->pack_start (my $lbl = Gtk2::Label->new ($a->{_name}), 0, 0, 0);
                  $lbl->set_alignment (0, 0.5);

               $elemhdl->drag_source_set (['button1_mask'], ['move'],
                  { target => 'STRING', flags => [], info => 'TARGET_STRING' }
               );
               $elemhdl->drag_dest_set (all => ['move'], 
                  { target => 'STRING', flags => [], info => 'TARGET_STRING' }
               );

         GCE::DragHelper::set_drag_source (
            $elemhdl, arch => sub { 
               { arch => $stack->[$ownidx], stk => $stack, stk_idx => $ownidx } 
            }
         );

         GCE::DragHelper::set_drag_sink (
            $elemhdl, arch => sub {
               my ($darch) = @_;

               if (defined $darch->{stk_idx} and $darch->{stk} == $stack) {
                  my $swapidx = $darch->{stk_idx};
                  ($stack->[$swapidx], $stack->[$ownidx])
                     = ($stack->[$ownidx], $stack->[$swapidx]);

               } else {
                  splice @$stack, $ownidx, 1, (dclone $darch->{arch});
               }

               # XXX: Insert undo here!
               $mapedit->set ($x, $y, $stack);

               $self->set_stack ($mapedit, $x, $y);
            }
         );
         $idx--;
      }
   }
   $self->show_all;
}

=head1 AUTHOR

 Marc Lehmann <schmorp@schmorp.de>
 http://home.schmorp.de/

 Robin Redeker <elmex@ta-sa.org>
 http://www.ta-sa.org/

=cut
1;

