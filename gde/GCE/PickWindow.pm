package GCE::PickWindow;

=head1 NAME

  GCE::PickWindow - the arch picker window class for gce

=cut

use Gtk2;
use Gtk2::Gdk::Keysyms;
use Gtk2::SimpleMenu;

use Deliantra;
use Deliantra::MapWidget;

use GCE::AttrEdit;
use GCE::MapEditor;
use GCE::StackView;
use GCE::EditAction;
use GCE::PickWindow;
use GCE::Util;

use GCE::DragHelper;

use Glib::Object::Subclass 
    Gtk2::Window;

use common::sense;

sub INIT_INSTANCE {
   my ($self) = @_;

   $self->{earchs} = editor_archs;

   while (my ($k, $v) = each %{ $self->{earchs} }) {
      $k =~ /^([^\/]+)/ or next;
      push @{$self->{toplevel_archs}->{$1}}, @$v;
   }

   $self->set_title ("gce - picker");

   $self->add (my $hb = Gtk2::VBox->new);

   $hb->pack_start (my $cb = $self->{combo} = Gtk2::ComboBox->new_text, 0, 1, 0);

      my $idx = 0;
      for (sort keys %{$self->{toplevel_archs}}) {

         $cb->append_text ($_);
         $self->{arch_txt_to_idx}->{$_} = $idx++;
      }

      $cb->signal_connect (changed => sub {

         my $arch = $cb->get_active_text;
         $self->set_selection ($arch);
      });

   $hb->pack_start (my $map = $self->{map} = new Deliantra::MapWidget, 1, 1, 0);
# XXX: Doesn't work yet beacuse of tooltips
#   GCE::DragHelper::set_drag_source (
#      $map, arch => sub { { arch => $self->{last_pick} } }
#   );

   $map->set_size_request (TILESIZE * 10, TILESIZE * 10);

   $map->signal_connect (button_press_event => sub {
      my ($map, $event) = @_;

      if ($event->button == 1) {

         my ($mx, $my) = $map->coord ($event->x, $event->y);
         my $as = $map->get ($mx, $my);

         my $arch = $as->[-1]
            or return;

         my ($x, $y, $arch, $as) = devirtualize ($map, $mx, $my, $arch, $as);

         $self->{last_pick} = $arch;

         my $ar = GCE::ArchRef->new (arch => $arch, source => 'picker');
         $::MAINWIN->update_attr_editor ($ar);

      } elsif ($event->button == 3) {

         my ($mx, $my) = $map->coord ($event->x, $event->y);
         my $as = $map->get ($mx, $my);

         my $arch = $as->[-1]
            or return;

         if ($arch->{_virtual}) {
            $arch = $arch->{_virtual};
         }

         $self->do_context_menu ($map, $event, $arch);
      }

#my $d = $map->disable_tooltip;

      1
   });

   $map->signal_connect (button_release_event => sub {
      my ($map, $event) = @_;

#   $map->enable_tooltip;

      1
   });

}

sub do_context_menu {
   my ($self, $map, $event, $arch) = @_;

   my ($x, $y) = $map->coord ($event->x, $event->y);

   my $menu = Gtk2::Menu->new;
   foreach my $cm (
      [
         "Add to inventory" => sub {
            my $pa = $::MAINWIN->{attr_edit}->get_arch;
            $pa->add_inv ({ _name => $arch->{_name} }) if $pa;
         }
      ]
   ) {
      my $item = Gtk2::MenuItem->new ($cm->[0]);
      $menu->append ($item);
      $item->show;
      $item->signal_connect (activate => $cm->[1]);
   }
   $menu->popup (undef, undef, undef, undef, $event->button, $event->time);
}

sub set_selection {
   my ($self, $arch) = @_;

   return unless defined $arch;

   return unless defined $self->{toplevel_archs}->{$arch};

   $self->{map}->set_map (
      new_pickmap Deliantra::Map ($self->{toplevel_archs}->{$arch})
   );
   $self->{map}->enable_tooltip;

   $self->{combo}->set_active ($self->{arch_txt_to_idx}->{$arch});

   $self->{last_selection} = $arch;
}


=head1 AUTHOR

 Marc Lehmann <schmorp@schmorp.de>
 http://home.schmorp.de/

 Robin Redeker <elmex@ta-sa.org>
 http://www.ta-sa.org/

=cut
1;
