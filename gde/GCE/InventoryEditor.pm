package GCE::InventoryEditor;

=head1 NAME

  GCE::StackView - the stack window class for gce

=cut

use Gtk2;
use Gtk2::Gdk::Keysyms;
use Gtk2::SimpleMenu;

use Deliantra;
use Deliantra::MapWidget;
use Storable qw/dclone/;

use Glib::Object::Subclass Gtk2::VBox;
use GCE::Util;
use GCE::DragHelper;
use GCE::ArchRef;

use strict;

sub INIT_INSTANCE {
   my ($self) = @_;

   $self->pack_start (my $sw = Gtk2::ScrolledWindow->new, 1, 1, 0);
      $sw->add_with_viewport (my $vb = Gtk2::VBox->new);
         $vb->pack_start ($self->{invarch}  = Gtk2::VBox->new, 0, 1, 0);
         $vb->pack_start (Gtk2::HSeparator->new, 0, 1, 0);
         $vb->pack_start ($self->{stackbox} = Gtk2::VBox->new, 1, 1, 0);

      $sw->set_policy ('automatic', 'automatic');
}

sub push_inv_hist {
   my ($self, $arch) = @_;
   push @{$self->{inv_hist}}, $arch;
}

sub clear_inv_hist {
   my ($self) = @_;
   delete $self->{inv_hist};
}

sub set_arch {
   my ($self, $ar) = @_;

   if ((defined $self->{cur_arch}) && ($self->{cur_arch} != $ar)) {
      $self->{cur_arch}->remove_on_change ('invedit')
        if defined $self->{cur_arch};
      $ar->add_on_change (invedit => sub { $self->set_arch ($_[0]) });

   } elsif (not defined $self->{cur_arch}) {
      $ar->add_on_change (invedit => sub { $self->set_arch ($_[0]) });
   }

   $self->{cur_arch} = $ar;

   $self->{invarch}->remove ($_) for $self->{invarch}->get_children;

   if (($self->{inv_hist} || [])->[-1] != $ar) {
#   unless (@{$self->{inv_hist} || []}) {
      push @{$self->{inv_hist}}, $ar;
   }

   for my $invar (@{$self->{inv_hist}}) {
      my $pba = new_arch_pb;
      fill_pb_from_arch ($pba, $invar->getarch);
      my $hb = Gtk2::HBox->new;
      $hb->pack_start ((new_from_pixbuf Gtk2::Image $pba), 0, 0, 0);
      $hb->pack_start (my $elemhdl = Gtk2::Button->new ($invar->get ('_name')), 0, 1, 0);
         $elemhdl->signal_connect (clicked => sub {
            while (pop @{$self->{inv_hist}} != $invar) { 1 }
            push @{$self->{inv_hist}}, $invar;
            $::MAINWIN->{attr_edit}->set_arch ($invar);
         });
         GCE::DragHelper::set_drag_source (
            $elemhdl, arch => sub { { arch => $invar } }
         );

         GCE::DragHelper::set_drag_sink (
            $elemhdl, arch => sub {
               return unless $_[0]->{arch};
               $invar->add_inv (dclone ($_[0]->{arch}->getarch));
               $self->set_arch ($invar) #XXX: untested yet
            }
         );

      $self->{invarch}->add ($hb);
   }

   $self->{stackbox}->remove ($_) for $self->{stackbox}->get_children;

   my $inv = $ar->get_inv_refs;
   my $idx = 0;

   for my $ia (@$inv) {
      my $pb = new_arch_pb;
      my $ownidx = $idx;

      my $a = $ia->archetype;
      fill_pb_from_arch ($pb, $ia->getarch);

      $self->{stackbox}->pack_start (my $hb = Gtk2::HBox->new, 0, 0, 0);
         $hb->pack_start (my $delbtn = Gtk2::Button->new_with_label ('del'), 0, 0, 0);
            $delbtn->signal_connect (clicked => sub {

                #my $oldstack = [ @$stack ];
                $ar->remove_inv ($ownidx); #splice @$inv, $ownidx, 1;
                $self->set_arch ($ar);
            });

         $hb->pack_start (my $elemhdl = new Gtk2::Button, 0, 0, 0);
            $elemhdl->add (my $hb2 = Gtk2::HBox->new);
            $elemhdl->signal_connect (clicked => sub {
               push @{$self->{inv_hist}}, $ia;
               $::MAINWIN->{attr_edit}->set_arch ($ia);
            });

               $hb2->pack_start (my $img = (new_from_pixbuf Gtk2::Image $pb), 0, 0, 0);
               $img->set_alignment (0, 0.5);

               $hb2->pack_start (my $lbl = Gtk2::Label->new ($ia->get ('_name')), 0, 0, 0);
               $lbl->set_alignment (0, 0.5);

         GCE::DragHelper::set_drag_source (
            $elemhdl, arch => sub {
               { arch => $inv->[$ownidx], inv => $inv, inv_idx => $ownidx } 
            }
         );

         GCE::DragHelper::set_drag_sink (
            $elemhdl, arch => sub {
               my ($darch) = @_;

               if (defined $darch->{inv_idx} && $darch->{inv} == $inv) {
                  my $swapidx = $darch->{inv_idx};
                  $ar->{arch}->swap_inv ($swapidx, $ownidx);
               } else {
                  $darch->{arch}->replace_inv ($ownidx, dclone $darch->{arch});
               }

               $self->set_arch ($ar);
            }
         );
      $idx++;
   }
   $self->show_all;

}

=head1 AUTHOR

 Marc Lehmann <schmorp@schmorp.de>
 http://home.schmorp.de/

 Robin Redeker <elmex@ta-sa.org>
 http://www.ta-sa.org/

=cut
1;

