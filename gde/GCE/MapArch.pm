package GCE::MapArch;
=head1 NAME

   GCE::MapArch - a class for handling deliantra maps

=over 4

=cut

sub new {
   my $class = shift;
   my $self = { @_ };
   bless $self, $class;
   return $self;
}

sub init_from_displaymap {
   my ($self, $dispmap) = @_; # map is the arch2map returnvalue

   $self->{dispmap} = $dispmap;
}

sub get_arch_params {
   my ($self, $arch) = @_;

   my %attrmap = (
         'msg' => [qw/msg/],
         'name' => [qw/name/],
         'enter_x' => [qw/hp enter_x/],
         'enter_y' => [qw/sp enter_y/],
         'width' => [qw/x width/],
         'height' => [qw/y height/],
         'reset_timeout' => [qw/weight reset_timeout/],
         'swap_time' => [qw/value swap_time/],
         'difficulty' => [qw/level difficulty/],
         'darkness' => [qw/invisible darkness/],
         'fixed_resettime' => [qw/stand_still fixed_resettime/],
         'unique' => [qw/unique/],
         'template' => [qw/template/],
         'region' => [qw/region/],
         'shopitems' => [qw/shopitems/],
         'shopgreed' => [qw/shopgreed/],
         'shopmin' => [qw/shopmin/],
         'shopmax' => [qw/shopmax/],
         'shoprace' => [qw/shoprace/],
         'outdoor' => [qw/outdoor/],
         'temp' => [qw/temp/],
         'pressure' => [qw/pressure/],
         'humid' => [qw/humid/],
         'windspeed' => [qw/windspeed/],
         'winddir' => [qw/winddir/],
         'sky' => [qw/sky/],
         'nosmooth' => [qw/nosmooth/],
   );

   my $params = {};

   for (keys %attrmap) {
      for my $from_attr (@{$attrmap{$_}}) {
         defined $arch->{$from_attr}
            and $params->{$_} = $arch->{$from_attr};
      }
   }

   return $params;
}


=head1 AUTHOR

 Marc Lehmann <schmorp@schmorp.de>
 http://home.schmorp.de/

 Robin Redeker <elmex@ta-sa.org>
 http://www.ta-sa.org/

=cut
