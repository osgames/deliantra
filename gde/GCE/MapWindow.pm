package GCE::MapWindow;

=head1 NAME

  GCE::MapWindow - the map window class for gce

=cut

use Gtk2;
use Gtk2::Gdk::Keysyms;

use Glib::Object::Subclass Gtk2::Window;

sub INIT_INSTANCE {
   my ($self) = @_;

   $self->add (my $b = Gtk2::Label->new ("<map here>"));
}


=head1 AUTHOR

 Marc Lehmann <schmorp@schmorp.de>
 http://home.schmorp.de/

 Robin Redeker <elmex@ta-sa.org>
 http://www.ta-sa.org/

=cut
1;
