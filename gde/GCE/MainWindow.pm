package GCE::MainWindow;

=head1 NAME

  GCE::MainWindow - the main window class for gde

=cut

use Cwd qw/abs_path getcwd/;
use Gtk2;
use Gtk2::Gdk::Keysyms;
use Gtk2::SimpleMenu;

use Deliantra;
use Deliantra::Map;
use Deliantra::MapWidget;

use GCE::AttrEdit;
use GCE::MapEditor;
use GCE::StackView;
use GCE::EditAction;
use GCE::PickWindow;

use Glib::Object::Subclass
    Gtk2::Window;

use GCE::Util;
use GCE::DragHelper;

use strict;

my $recentfile = "$Deliantra::VARDIR/gderecent";

# XXX: make a recursive call from save_layout to all (interesting) sub-widgets
sub save_layout {
   my ($self) = @_;

#   $main::CFG->{attr_edit_on}  = exists $self->{attr_edit}        ? 1 : 0;
   $main::CFG->{stack_view_on} = exists $self->{sv}               ? 1 : 0;
   $main::CFG->{picker_on}     = exists $self->{last_pick_window} ? 1 : 0;
   $main::CFG->{main_window}   = main::get_pos_and_size ($self);
   $main::CFG->{stack_view}    = main::get_pos_and_size ($self->{sv_win}) if $self->{sv_win};
   $main::CFG->{attr_view}     = main::get_pos_and_size ($self->{attr_edit_win}) if $self->{attr_edit_win};

   if ($self->{last_map_window}) {
      $main::CFG->{map_window} = main::get_pos_and_size ($self->{last_map_window});
      $self->{last_map_window}->save_layout ();
   }

   $self->{worldmap_coord_query}->save_layout ()
      if $self->{worldmap_coord_query};

   $main::CFG->{last_folders}  = $self->{fc_last_folders};

   $main::CFG->{open_pickers} = [];

   for (@{$self->{open_pick_windows}}) {

      next unless defined $_;

      push @{$main::CFG->{open_pickers}}, {
         p_and_s   => main::get_pos_and_size ($_),
         selection => $_->{last_selection}
      };
   }

   $self->{attr_edit}->save_layout;

   $self->write_cfg;
}

sub write_cfg {
   my ($self) = @_;
   main::write_cfg ("$Deliantra::VARDIR/gdeconfig");
}

sub load_layout {
   my ($self) = @_;

   $self->{fc_last_folders} = $main::CFG->{last_folders};

#   $main::CFG->{attr_edit_on}
#      and $self->show_attr_editor;

   $main::CFG->{stack_view_on}
      and $self->show_stack_view;

   for (@{$main::CFG->{open_pickers}}) {
      $self->open_pick_window ($_);
   }

   $self->{attr_edit}->load_layout;
}

sub open_map_editor {
   my ($self, $mapfile) = @_;

   my $mapkey;
   unless (ref $mapfile) {
#     unless (File::Spec->file_name_is_absolute ($mapfile)) {
#        $mapfile = File::Spec->rel2abs ($mapfile);
#      }
      $mapkey = abs_path ($mapfile);
      # File::Spec->abs2rel ($mapfile, File::Spec->catfile ($::MAPDIR));
   } else {
      $mapkey = "$mapfile";
   }

   # XXX: last_map_window is a dirty trick to get the position and size
   # for save layout

   if (defined $self->{loaded_maps}->{$mapkey}) {
      $self->{loaded_maps}->{$mapkey}->get_toplevel->present;
      return;
   }

   my $w = $self->{last_map_window} = GCE::MapEditor->new;

   $self->{editors}->{$w} = $w;

   $w->signal_connect (destroy => sub {
      my ($w) = @_;
      $w->close_windows;
      delete $self->{loaded_maps}->{$w->{mapkey}};
      delete $self->{editors}->{$w};
      0;
   });

   $self->{loaded_maps}->{$mapkey} = $w;

   eval { $w->open_map ($mapfile, $mapkey) };
   if ($@) {
      quick_msg ($self, "$@", 1);
      $w->close_windows;
      delete $self->{loaded_maps}->{$w->{mapkey}};
      delete $self->{editors}->{$w};
      $w->destroy;
      return;
   }

   $w->set_edit_tool ($self->{sel_editaction});

   $w->show_all;
}

sub show_help_window {
   my ($self) = @_;

   return if defined $self->{help_win};
   require Gtk2::Ex::PodViewer;
   my $w = $self->{help_win} = Gtk2::Window->new;
   $w->set_title ("deliantra editor - help");
   $w->set_default_size (500, 300);
   $w->signal_connect (destroy => sub {
      $self->{help_win}->hide; $self->{help_win} = undef;
      0
   });
   $w->add (my $sw = Gtk2::ScrolledWindow->new);
   $sw->add (my $h = Gtk2::Ex::PodViewer->new);
   $h->load_string ($::DOCUMENTATION);
   $w->show_all;
}

sub show_stack_view {
   my ($self) = @_;

   return if defined $self->{sv};

   my $w = $self->{sv_win} = Gtk2::Window->new ('toplevel');
   $w->set_title ('deliantra editor - stack view');
   $w->signal_connect (destroy => sub { delete $self->{sv}; 0 });
   $w->add ($self->{sv} = GCE::StackView->new);

   main::set_pos_and_size ($w, $main::CFG->{stack_view}, 150, 250);

   $w->show_all;
}

sub show_editor_properties {
   my ($self) = @_;

   return if $self->{prop_edit};

   my $w = $self->{prop_edit} = Gtk2::Window->new;
   $w->set_title ("deliantra editor - preferences");
   $w->add (my $t = Gtk2::Table->new (2, 5));
      $t->attach_defaults (my $lbl1 = Gtk2::Label->new ("LIBDIR"), 0, 1, 0, 1);
      $t->attach_defaults (my $lib = Gtk2::Entry->new, 1, 2, 0, 1);
         $lib->set_text ($::CFG->{LIBDIR});
      $t->attach_defaults (my $lbl2 = Gtk2::Label->new ("MAPDIR"), 0, 1, 1, 2);
      $t->attach_defaults (my $map = Gtk2::Entry->new, 1, 2, 1, 2);
         $map->set_text ($::CFG->{MAPDIR});
      $t->attach_defaults (my $lbl1 = Gtk2::Label->new ("Username"), 0, 1, 2, 3);
      $t->attach_defaults (my $usern = Gtk2::Entry->new, 1, 2, 2, 3);
         $usern->set_text ($::CFG->{username});
      $t->attach_defaults (my $save = Gtk2::Button->new ('save'), 0, 2, 3, 4);
         $save->signal_connect (clicked => sub {
            $::CFG->{LIBDIR} = $lib->get_text;
            $::CFG->{MAPDIR} = $map->get_text;
            $::LIBDIR = $::CFG->{LIBDIR} if $::CFG->{LIBDIR};
            $::MAPDIR = $::CFG->{MAPDIR} if $::CFG->{MAPDIR};
            $::CFG->{username} = $usern->get_text;
            Deliantra::set_libdir ($::LIBDIR);
            Deliantra::load_archetypes;
            Deliantra::load_tilecache;
            main::write_cfg ("$Deliantra::VARDIR/gdeconfig");
            $w->destroy;
         });
      $t->attach_defaults (my $close = Gtk2::Button->new ('close'), 0, 2, 4, 5);
         $close->signal_connect (clicked => sub { $w->destroy });

   $w->signal_connect (destroy => sub { delete $self->{prop_edit}; 0 });

   main::set_pos_and_size ($w, $main::CFG->{prop_edit}, 200, 200);

   $w->show_all;
}

sub show_attr_editor {
   my ($self) = @_;

   return if $self->{attr_edit_win};

   my $w = $self->{attr_edit_win} = Gtk2::Window->new;
   $w->set_title ("deliantra editor - edit attrs");
   $w->add ($self->{attr_edit} = GCE::AttrEdit->new);
   $w->signal_connect (destroy => sub { delete $self->{attr_edit_win}; 0 });

   main::set_pos_and_size ($w, $main::CFG->{attr_view}, 400, 300, 250, 0);

   $w->show_all;
}

sub update_attr_editor {
   my ($self, $ar, $map, $x, $y) = @_;

   if (ref ($ar) ne 'GCE::ArchRef') { require Carp; Carp::confess ("$ar no ARCHREF!") }

   $self->{attr_edit}
      or return;

   $self->{attr_edit}->set_arch ($ar, 1);
   $self->{attr_edit_win}->set_title ("deliantra editor - edit " . $ar->longname);
}

sub update_map_pos {
   my ($self, $mapedit, $x, $y) = @_;
   $self->{sv}->maybe_update_stack_for ($mapedit, $x, $y)
      if $self->{sv};
}

sub update_stack_view {
   my ($self, $mapedit, $x, $y) = @_;

   return unless $self->{sv};

   $self->{sv}->set_stack ($mapedit, $x, $y);
}

sub open_pick_window {
   my ($self, $layout) = @_;

   # XXX: Yes, also fix this, save _every_ pick window and their positions and their
   # selection
   my $p = GCE::PickWindow->new ();

   push @{$self->{open_pick_windows}}, $p;

   my $idx = (@{$self->{open_pick_windows}}) - 1;

   $p->signal_connect ('delete-event' => sub {
      $self->{open_pick_windows}->[$idx] = undef;
   });

   if ($layout) {
      main::set_pos_and_size ($p, $layout->{p_and_s}, 200, 200);
   }

   $p->show_all;

   $p->set_selection ($layout->{selection});
}

sub add_recent {
   my ($self, $entry) = @_;
   our @recent_entries;

   @recent_entries = grep { $_ ne $entry } @recent_entries;
   unshift @recent_entries, $entry;
   splice @recent_entries, 5;

   open my $fh, ">$recentfile" or die "Can't write to file $recentfile: $!";
   binmode $fh;
   local $/;
   print $fh (join "\0", @recent_entries);
   close $fh;

   $self->build_menu;
}

sub escape_filename {
   my $str = shift;

   $str = Glib::filename_to_unicode($str);

   # escape to prevent Gtk2::SimpleMenu parsing these especially
   $str =~ s/\//\\\//g;
   $str =~ s/_/-/g;

   $str;
}

sub recent {
   my ($self) = @_;
   my @recent;

   our @recent_entries;

   foreach my $entry (@recent_entries) {
      push @recent, escape_filename($entry) => {
            callback => sub {
               $self->open_map_editor($entry);
	       $self->build_menu
            }
         };
   }

   push @recent, Empty => { callback => sub {} }
      unless @recent;

   @recent;
}

sub build_menu {
   my ($self) = @_;

   my $menu_tree = [
      _File => {
         item_type => '<Branch>',
         children => [
            _New => {
               callback => sub { $self->new_cb },
               accelerator => '<ctrl>N'
            },
            _Open => {
               callback => sub { $self->open_cb },
               accelerator => '<ctrl>O'
            },
            'Open special' => {
               item_type => '<Branch>',
               children => [
                  "world map at"=> {
                     callback => sub { $self->open_worldmap_cb },
                  },
                  "recent files"=> {
                     item_type => '<Branch>',
                     children => [ $self->recent ],
                  },
               ]
            },
            "_Save Layout" => {
               callback => sub { $self->save_layout },
               accelerator => '<ctrl>L'
            },
            "_Preferences" => {
               callback => sub { $self->show_editor_properties },
               accelerator => "<ctrl>T"
            },
           _Quit => {
               callback => sub { Gtk2->main_quit },
               accelerator => '<ctrl>Q'
            },
         ]
      },
      _Dialogs => {
         item_type => '<Branch>',
         children => [
            "_Picker" => {
               callback => sub { $self->open_pick_window },
               accelerator => "<ctrl>P" 
            },
            "_Stack View" => {
               callback => sub { $self->show_stack_view },
               accelerator => "<ctrl>V" 
            },
            "_Attributes" => {
               callback => sub { $self->show_attr_editor },
               accelerator => "<ctrl>A" 
            },
         ]
      },
      _Help => {
         item_type => '<Branch>',
         children => [
            _Manual => {
               callback => sub { $self->show_help_window },
               accelerator => "<ctrl>H"
            },
         ]
      },
   ];

   my $men =
      Gtk2::SimpleMenu->new (
         menu_tree => $menu_tree,
         default_callback => \&default_cb,
      );

   for ($self->{vb}->get_children) { # Rebuild menu
      if ($_->isa ('Gtk2::MenuBar')) {
         $_->hide;
         $self->{vb}->remove($_);

         $self->{vb}->pack_start ($men->{widget}, 0, 1, 0);
         $self->{vb}->reorder_child ($men->{widget}, 0);
         $self->{vb}->show_all;
      }
   }

   $self->add_accel_group ($men->{accel_group});

   return $men->{widget};
}

sub add_button {
   my ($self, $table, $plcinfo, $lbl, $cb) = @_;

   my ($lx, $ly) = @{$plcinfo->{next}};

   unless ($lx < $plcinfo->{width}) {

      $ly++;
      $lx = 0;
   }

   $ly < $plcinfo->{height}
      or die "too many buttons, make table bigger!";

   $table->attach_defaults (my $btn = Gtk2::Button->new_with_mnemonic ($lbl), $lx, $lx + 1, $ly, $ly + 1);
      $btn->signal_connect (clicked => $cb);

   $plcinfo->{next} = [$lx + 1, $ly];
}

sub build_buttons {
   my ($self) = @_;

   my $tbl = Gtk2::Table->new (2, 4);
   my $plcinfo = { width => 2, height => 4, next => [0, 0] };

   $self->{edit_collection}{pick}        = GCE::EditAction::Pick->new;
   $self->{edit_collection}{place}       = GCE::EditAction::Place->new;
   $self->{edit_collection}{erase}       = GCE::EditAction::Erase->new;
   $self->{edit_collection}{select}      = GCE::EditAction::Select->new;
   $self->{edit_collection}{perl}        = GCE::EditAction::Perl->new;
   $self->{edit_collection}{connect}     = GCE::EditAction::Connect->new;
   $self->{edit_collection}{followexit}  = GCE::EditAction::FollowExit->new;

   $self->set_edit_tool ('pick');

   $self->add_button ($tbl, $plcinfo, "P_ick",         sub { $self->set_edit_tool ('pick') });
   $self->add_button ($tbl, $plcinfo, "_Place",        sub { $self->set_edit_tool ('place') });
   $self->add_button ($tbl, $plcinfo, "_Erase",        sub { $self->set_edit_tool ('erase') });
   $self->add_button ($tbl, $plcinfo, "_Select",       sub { $self->set_edit_tool ('select') });
   $self->add_button ($tbl, $plcinfo, "Eva_l",         sub { $self->set_edit_tool ('perl') });
   $self->add_button ($tbl, $plcinfo, "Connec_t",      sub { $self->set_edit_tool ('connect') });
   $self->add_button ($tbl, $plcinfo, "_Follow Exit",  sub { $self->set_edit_tool ('followexit') });

   return $tbl;
}

sub set_edit_tool {
   my ($self, $name) = @_;

   if ($name eq 'pick') {
      $self->update_edit_tool ($self->{edit_collection}{pick}, "Pick");;
   } elsif ($name eq 'place') {
      $self->update_edit_tool ($self->{edit_collection}{place}, "Place");;
   } elsif ($name eq 'erase') {
      $self->update_edit_tool ($self->{edit_collection}{erase}, "Erase");;
   } elsif ($name eq 'select') {
      $self->update_edit_tool ($self->{edit_collection}{select}, "Select");;
      $self->{edit_collection}{select}->update_overlay;
   } elsif ($name eq 'perl') {
      $self->update_edit_tool ($self->{edit_collection}{perl}, "Eval");;
   } elsif ($name eq 'connect') {
      $self->update_edit_tool ($self->{edit_collection}{connect}, "Connect");;
   } elsif ($name eq 'followexit') {
      $self->update_edit_tool ($self->{edit_collection}{followexit}, "Follow Exit");;
   }
}

sub update_edit_tool {
   my ($self, $tool, $name) = @_;

   for (values %{$self->{loaded_maps}}) {
      $_->{map}->overlay ('selection')
   }

   $self->{edit_tool}->set_text ($name);
   $self->{sel_editaction} = $tool;

   my $widget = $tool->tool_widget;

   for ($self->{edit_tool_cont}->get_children) {
      $_->hide;
      $self->{edit_tool_cont}->remove ($_);
   }

   $_->set_edit_tool ($self->{sel_editaction}) for (values %{$self->{editors}});

   defined $widget or return;

   $self->{edit_tool_cont}->add ($widget);
   $widget->show_all;
}

sub update_pick_view {
   my ($self, $arch) = @_;

   defined $arch->{_face}
      or $arch = $Deliantra::ARCH{$arch->{_name}};

   fill_pb_from_arch ($self->{pick_view_pb}, $arch);
   $self->{pick_view_img}->set_from_pixbuf ($self->{pick_view_pb});

   $self->{pick_view_btn}->set_label ($arch->{_name});
}

sub INIT_INSTANCE {
   my ($self) = @_;

   {
      open my $fh, "<$recentfile";
      binmode $fh;
      local $/;
      our @recent_entries = split /\0/, <$fh>;
      close $fh;
   }

   $::MAINWIN = $self;

   $self->set_title ("deliantra editor - toolbox");

   $self->{edit_tool} = Gtk2::Label->new;
   $self->{edit_tool_cont} = Gtk2::VBox->new;

   $self->add (my $vb = $self->{vb} = Gtk2::VBox->new);
      $vb->pack_start ($self->build_menu, 0, 1, 0);
      $vb->pack_start (my $tbl = $self->build_buttons, 0, 1, 0);

      $vb->pack_start (Gtk2::HSeparator->new, 0, 1, 0);
      $vb->pack_start ($self->{edit_tool}, 0, 1, 0);

      $vb->pack_start (Gtk2::HSeparator->new, 0, 1, 0);
      $vb->pack_start ($self->{edit_tool_cont}, 1, 1, 0);

   # XXX:load $ARGV _cleanly_?
   $self->open_map_editor ($_)
      for @ARGV;

   $self->signal_connect ('delete-event' => sub {
         Gtk2->main_quit;
   });

   ::set_pos_and_size ($self, $main::CFG->{main_window}, 150, 200, 0, 0);


   $self->show_attr_editor;
}

sub new_cb {
   my ($self) = @_;

   my $w = Gtk2::Window->new ('toplevel');
   my $width  = [width  => 20];
   my $height = [height => 20];
   $w->add (my $tbl = Gtk2::Table->new (2, 3));
      add_table_widget ($tbl, 0, $width, 'string');
      add_table_widget ($tbl, 1, $height, 'string');
      add_table_widget ($tbl, 2, 'new', 'button', sub {
         if ($width->[1] > 0 and $height->[1] > 0) {
            my $map = Deliantra::Map->new ($width->[1], $height->[1]);
            $map->{info}->{width}  = $width->[1];
            $map->{info}->{height} = $height->[1];
            $map->resize ($width->[1], $height->[1]);
            $self->open_map_editor ($map);
         }
         $w->destroy;
         1;
      });
      add_table_widget ($tbl, 3, 'close', 'button', sub { $w->destroy });
   $w->show_all;
}

sub new_filechooser {
   my ($self, $title, $save, $filename) = @_;

   $title ||= 'deliantra editor - open map';
   my $fc = new Gtk2::FileChooserDialog (
         $title, undef, $save ? 'save' : 'open', 'gtk-cancel' => 'cancel', 'gtk-ok' => 'ok'
      );

   my @shortcut_folders =
      grep { $_ && ($_ ne '') && -e $_ } keys %{$self->{fc_last_folders}};

   $fc->add_shortcut_folder ($_) for @shortcut_folders;

   unless (grep { $::MAPDIR eq $_ } @shortcut_folders) {
      $fc->add_shortcut_folder ($::MAPDIR)
         if -d $::MAPDIR;
   }

   $fc->set_current_folder  (getcwd);

   if ($filename) {
      $fc->set_filename ($filename);
   }

   $fc
}

sub new_coord_query {
   my ($self, $finishcb) = @_;

   my $coordhash = { x => 105, y => 115, worldmap => 1, overlay => 0 };
   my $diag = GCE::HashDialogue->new;
   $self->{"worldmap_coord_query"} = $diag;
   $diag->signal_connect (destroy => sub {
      delete $self->{"worldmap_coord_query"};
   });
   $diag->init (
      layout_name => 'worldmap_coord_query',
      info        => "Open worldmap at ...",
      dialog_default_size => [ 200, 200, 200, 0 ],
      title       => 'Worldmap coordinate entry',
      ref_hash    => $coordhash,
      dialog => [
         [x => 'X Coordinate' => 'spin', sub { (0, 999, 1) }],
         [y => 'Y Coordinate' => 'spin', sub { (0, 999, 1) }],
         [worldmap => 'Open worldmap'      => 'check'],
         [overlay  => 'Open overlay (CF+)' => 'check'],
      ],
      save_button_label => 'open',
      save_cb => sub {
         $finishcb->($_[0]);
      }
   );
   $diag->show_all;
}

sub open_worldmap_cb {
   my ($self) = @_;

   $self->new_coord_query (sub {
      my ($info) = @_;
      my ($x, $y) = ($info->{x}, $info->{y});
      my ($worldmap, $overlay) = ($info->{worldmap}, $info->{overlay});
      $self->open_map_editor ($::MAPDIR . "/world/world_$x\_$y")
         if $worldmap;
      $self->open_map_editor ($::MAPDIR . "/world-overlay/world_$x\_$y")
         if $overlay;
   });
}

sub open_cb {
   my ($self) = @_;

   my $fc = $self->new_filechooser;

   if ('ok' eq $fc->run) {

      $self->{fc_last_folder} = $fc->get_current_folder;
      $self->{fc_last_folders}->{$self->{fc_last_folder}}++;

      $self->open_map_editor ($fc->get_filename);
   }

   $fc->destroy;
}

sub get_pick {
   my ($self) = @_;

   $self->{attr_edit}
      or die "Couldn't find attribute editor! SERIOUS BUG!";

   # XXX: This is just to make sure that this function always returns something

   my $ar = $self->{attr_edit}->get_arch;
   return { _name => 'platinacoin' } unless defined $ar;
   return $ar->getarch || { _name => 'platinacoin' };
}

=head1 AUTHOR

 Marc Lehmann <schmorp@schmorp.de>
 http://home.schmorp.de/

 Robin Redeker <elmex@ta-sa.org>
 http://www.ta-sa.org/

=cut
1;

