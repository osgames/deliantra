                        Design notes.

Prince onefang's ice castle was designed as if Prince onefang designed it 
himself.  The general principle is that it is made from enchanted icecubes.
Like an igloo, this does not cause the internal temperature to be very low,
so everybody can quite happily live inside.  The enchantment stops them 
from being carried away, melted, or otherwise vanished.  The floor at ground
level is also enchanted, to stop spells & prayers, and to make everybody
feel good.  The layout is designed to make it look like the university face
used to represent the castle from the outside.  The icecube face changed
between the server version I started this on and the version I am using now, 
let's hope that the university face doesn't change.  The icecubes looked 
better in the previous version.

The suites don't fit into the university layout, but I fixed that by putting
them above the clouds, with a very thin elevator going up to them.

The icecubes use the same total internal reflection principle that make fibre
optics work.  Like fibre optics, a 90 degree bend causes a flaw that lets light
leak out.  Like fibre optics, light comes out of the ends.  I may not have been
consistant.  On the other hand, some places needed more light, so I either
put in a brazier or lit up an icecube as needed (provided a flaw for light to 
leak out of).  The source of the light is not on the map anywhere.

No icecubes on the outside of the castle are lit up, because Prince onefang 
doesn't want the place that easily found.  For that reason, and the fact that 
elves like living in the middle of large woodlands (Prince onefang is an elf),
the castle is in the middle of the Dark Forest, with no paths leading to it.
Moving the castle to somewhere else will be tricky, as there are many maps
with unblocked views of the forest.  

A lot of attention to detail went into this, I hope that players appreciate it.

I ignore some of the map making guidelines, but so do a lot of the standard 
maps.  I ignore the shop guidelines because the high price of powerful items 
provides balance, no need to keep them out of shops when PC's can't afford 
them anyway, assuming the system calculates prices correctly.  Prince onefang 
wants his shops this way, gives him a reason to go shopping.  It also represents 
the kind of thing that the Prince will dump at the shops after his own 
adventuring, he is their main supplier of +3 and +4 items.

Yep, I created a town, but it is not your standard town.

Some areas are for Prince onefang only.  The lever that closes the main gates,
and the treasures on display (both in the same place) are the main ones.
Portals to these and other places are in the suites unique to onefang when
he is a PC.  Prince onefang should be played by the DM, with DM privs set, but
you should not need them much.

Some areas are designed to only allow access if Prince onefang opens the door.
This allows the Prince to be a rich and powerful patron to anybody that meets
his criteria.  Currently, they must be elven worshippers of Lythander.  A
quest or three may be added later, when I can think of some.  These areas include 
the part behind the temple that contains the standard equipment handed out to 
onefangians, and the private magic library at the top of the mall.  Force 
"onefangian" is given to anybody that is let into the behind the temple area
via the door.  Force "onefang" is only held by Prince onefang.  The standard 
equipment is all godgiven, so onefangians must carry it around always, or lose
it.  The spell and prayer books in the private library are nailed down.  Check
the quality of them though B-).

One quest currently under construcrion is to find out what happened to the
party of onefangians sent to clean out the ants.  While the Prince should
really clean out the dragon section himself, he could take the newly 
baptised onefangians with him.  See "Floyd of Pink" below for another quest.

The altars to Lythander in the residential areas can be reconsecrated by anyone 
with Wisdom 20 or higher.  In the suites, the server operator should do that 
with an editor.

The strange looking flooring on the malls ground floor represents wear patterns.

Sometime between server version 1.0 and 1.4, the nearby nest of dragons seemed
to have given up hope.  Now they just sit there and let you attack them.  While
probably it hasn't been updated since some server behaviour changed, it at least
fits in with the onefang plot.  He cleared them out once, has now setup camp 
close to them, and with the Whisker on display to rub it in, so now dragon morale 
is low.

The Imperial Post Office scripts don't work on my server, something dodgy with 
the database stuff.  Using a database for this is overkill, and everything else
in Crossfire is text based anyway, so I converted the scripts to use
text files, and renamed them so they can live with the originals.  

SERVER CODE CHANGES

common/button.c     - fix inv checker object matcher
common/map.c        - fix inv checker (FIXED in 1.5)
server/skills.c     - fix written text not initialized
common/time.c       - added tod_to_errmsg() to support web page
server/weather.c    - creates weather image each tod tick and major tweaks
common/living.c     - adjusted SP,GP per level
server/skill_util.c - adjusted exp from using skills

TODO

Finish ant farm.

Make castle flyer friendly, if possible, tiling might do it.  Replicate more 
stuff upwards, tiling will do it.  Tiling is limited to 12 squares between tiles.  
Tiling would have helped a lot, but the LOS restrictions make it tricky.  I 
looked at tiling, lighting effects are broken, LOS restrictions have to be broken
(but only for one more map), and the maps would end up too fragmented.  Next time
I need a break from serious programming, I'll try to add map stacking.  That is
putting a smaller map on top of a larger map, and tiling effects as per usual.
The changelog mentions map overlay, which may be the same thing, but it is not
documented anywhere that I can find. 

The tiled dragon map consistantly crashed the server when the dragons attacked.

Do junk mail B-).  Do basic RFC headers, Python has RFC libraries, include 
date, postmark (name of map sent from).

Do money orders, cheques, interest, loans, ATM / card / PIN, B-Pay, 
account keeping fees, credit cards, government tax, etc.
Use this as a source of income for onefang.  Deposit boxes that can hold 
jewelery, keys, scrolls, and other small valuables.

Have an IPO in every major city, complete with long queues.  In /onefang/suites
have Karen give writing paper with IceCastle letterhead as a free service.  Have 
a crazy postal worker in some post offices armed with a crossbow.

Let's go crazy, pawn brokers, loan sharks, bank robbers, photo copying service.

BIG WORLD

Big map world_110_118 34,24 - 38,27 = university arch as before.
Extend the forest from the Dark Forest area, to around these lakes, and
to the other lakes to the south west.  Basically, the entire north west 
end of the valley.  forest.pl will extend the forest, see it for details.

While I have converted the ground level map to Big World, none of the other
maps have been converted, so we still have breath taking views of the small
world maps.  Overlays may do the trick for these.

For aome strange reason, when dynamiclevel is set to 1 or above, the Ice castle 
will be invisible sometimes.  I like it B-).

The dragon lord maps look to be in about the same sort of position relative
to the new castle location, so everything should still link up.  DragonLord
city is at world_111_117 31,43.  AntFarm now becomes part of under_world.  I
should double check the scale of things here.

There is a bit at the top of the world that looks like an obvious place for
a protected harbour.  I should search out such places, and also places where
people and animals would normally form tracks, then add harbours, harbour 
cities, animal and walking tracks.  Then we can setup trade routes, and have
a proper civilization grow.

Write some animal foraging code, to form animal tracks the natural way, and
have animals changing the landscape like the weather does.

FANCY MAGIC ITEM (Floyd of Pink)

Actually, it's five magic items that combine to produce their effect.  Basically
the effects of Amulet of Bling Bling, the left and right onefang rings, plus 
speed and levitation boots.  They all look identical (big pink / purple gem), 
and have an identical name (opal of flawed beauty), but you have to put 
them on in the correct order, in the correct place.  Otherwise, all hell breaks 
loose.  You cannot wear them while sleeping, otherwise you wake up with one HP, 
no SP or grace, not wearing the items, and something is summoned to fight you.  
The correct places are - one on each foot, one on each hand, and one for the 
soul (head, neck, or body). The various enhancements are distributed logically, 
dex for the hands, speed for the feet, live saving for the soul, etc.  Once you 
have them placed correctly, a backpack of utopia appears on your back, and 
disappears when you remove any item.  There is a small chance that everything 
in the backpack disappears with it.  A bigger chance that it all spills out on 
the floor, but mostly it all will remain in the pack for the next person who puts 
it on.  The items are scattered randomly at the end of ten of the most dangerous 
maps.  Rumours and clues detail the various attributes of the items, and all ten 
locations.  Sprinkle Pink Floyd lyrics into the clues.

Each item has a story, as you look deep into the gem, you see a flaw in the shape
of "several species", "small, furry animals", "a cave", "a groove", "a pict". 
That is their order, although the places each is worn are chosen randomly and stored 
in a file.  The five monsters carrying the gems are "Dave" (Dave), "Mr Wright" (wight+), 
"Nick" (galeo troll+), "Syd" (cunning gnome+, and crazy), and "Roger" (Evil master)

Don't rely on the unique flag to make sure there is only one of each.  Each gem gets 
a unique identifier, but only the first five are part of the item, the rest are
"opals of flawed beauty" and their flaws look like a cow, brick wall, prism, dog, 
sheep, pig, worm, gnome, dark side of the moon, and other Pink Floyd icons.

The story.  A powerful ancestor of Prince onefang had them made for his own use.
His lover was kidnapped and killed by someone.  Ancestor got drunk and went on a 
rampage, where he lost each item one at a time.  He didn't clear out the areas 
he rampaged through, and they rebuilt stronger than ever.

