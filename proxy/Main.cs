
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using System.Text;

namespace _olbaid_websockets
{
	class MainClass
	{
		public static Socket connectionSocket;
		public static void onClientConnect (IAsyncResult asyn){
			Console.WriteLine("Socket accepted");
			Socket client = connectionSocket.EndAccept(asyn);
			connectionSocket.BeginAccept(new AsyncCallback(onClientConnect), null);
			Client c = new Client(client);
			Thread thread = c.createThread();
			thread.Start();
		}
		
		public static void Main (string[] args)
		{
			
			if (args.Length == 1){
				ASCIIEncoding encoding = new ASCIIEncoding();
				BinaryReader br = new BinaryReader(File.Open(args[0],FileMode.Open));
				Int32 len;
				byte [] buffer;
				len = br.ReadInt32();
				
				buffer = br.ReadBytes(len);
				
				string key1=encoding.GetString(buffer,0,len);
				//Console.WriteLine(key1 + " " + len);
				len = br.ReadInt32();
				buffer = br.ReadBytes(len);
				
				string key2=encoding.GetString(buffer,0,len);
				//Console.WriteLine(key2 + " " + len);
				
				len = br.ReadInt32();
				Console.WriteLine(" Z" + len + "X");
				byte [] l8b=br.ReadBytes(8);
				byte [] hixieResp = Client.calcHixieResponse(key1,key2,l8b);
				Console.Write ("hixie: " );
				for (int i = 0;i<hixieResp.Length;++i){
					Console.Write("{0:D},",hixieResp[i].ToString());
				}
				Console.WriteLine(" ");
				return;
			}
			
			connectionSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            connectionSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            IPEndPoint ipLocal = new IPEndPoint(IPAddress.Any, 13327);
            // Bind to local IP Address
            connectionSocket.Bind(ipLocal);
            // Start Listening
            connectionSocket.Listen(1000);
            // Creat callback to handle client connections
			connectionSocket.BeginAccept(new AsyncCallback(onClientConnect), null);
			
			while (1==1){
				Thread.Sleep(10000);
			}
			/*
			 * Console.WriteLine("There is some connection awaiting");					
					Client client = new Client(listener.AcceptTcpClient());					
					Thread thread = client.createThread();
					thread.Start();
				}
				*/
			//}
			return;
		}
	}
}
