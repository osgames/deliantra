#!/opt/bin/perl

# this script walks '.' recursively and will find all archetypes
# that can be found and will look for face and state info in them
# and return a json hash to stdout with the following key/value format:
#
#    "<uuid><state>":"<face>"
#
# see also json_conv_old_faces for instructions on how to convert the facenames
#

use strict;
use File::Find;

*name   = *File::Find::name;
*dir    = *File::Find::dir;

my $first = 1; # marks the first key/value (don't print too many ','s)

sub getarch {
   my ($file, $lines, $outarchs) = @_;

   my $inarch = 0;
   my $at_end = 0;

   my $archlines = [];
   do {
      my $line = shift @$lines;
      if ($line =~ m/^arch\s*(\S+)\s*$/) {
         if ($inarch) {
            unshift @$lines, $line;
            getarch ($file, $lines, $outarchs);
         } else {
            $inarch = 1;
            push @$archlines, $line;
         }
      } elsif ($line =~ m/^end\s*$/) {
         push @$archlines, $line;
         push @$outarchs, $archlines;
         $archlines = [];
         $at_end = 1;
      } else {
         push @$archlines, $line if $inarch;
      }
   } while (not ($at_end) && @$lines);

   if ($inarch and not $at_end) {
      die "mismatched archetype in $file!";
   }
}

sub wanted {
   my $file = $_;
   -f $file or return;

   next if $file =~ /\.pst$/;

   open my $fh, "<$file" or return;
   my $map = do { local $/; <$fh> };

   my $found_magic_ear;
   my $found_npc;

   my @lines = split /\r?\n/, $map;

   my $archs = [];
   getarch ($file, \@lines, $archs) while @lines;

   for my $archlines (@$archs) {
      my $arch;
      my $lev;
      my $exp;

      for (@$archlines) {
         if (/^arch\s*skill_(\S+)\s*$/) {
            $arch = $1;
         }
         if (/^level\s*(\S+)\s*$/) {
            $lev = $1;
         }
         if (/^exp\s*(\S+)\s*$/) {
            $exp = $1;
         }
      }

      next if $arch eq 'map';

      if ($arch && $lev) {
         printf "%-30s: %30s %3d\n", $file, $arch, $lev;
      }
   }
}

find { no_chdir => 1, wanted => \&wanted }, ".";
