#!/opt/perl/bin/perl
#
# this script converts the old face names to the new ones and checks whether
# the face can be found in the list of image filenames.
#
# First make a list of filenames:
#    find cf.schmorp.de/arch/ -name "*.png" -print > /tmp/face.filenames
#
# Second make a json file with the uuid->face mapping:
#    cd /var/crossfire/
#    grep_for_uuid_faces > /tmp/face.json
#
# Third call this to convert the facenames in the face.json:
#    json_conv_old_faces /tmp/face.json /tmp/face.filenames > /tmp/newface.json
#
# finished.
#
use strict;
use JSON::Syck;

open J, $ARGV[0] or die "$!";
open F, $ARGV[1] or die "$!";

my $h = JSON::Syck::Load (do { local $/; <J> });
my @files = split /\r?\n/, do { local $/; <F> };

my %faces;
$faces{$_} = 1 for (values %$h);

my %filenames;
my %collided;

for (@files) {
   next if /\.xvpics/;
   s/^.*\/([^\/]*)$/\1/;
   if ($filenames{$_}) {
      warn "filename collision in $_\n" if $filenames{$_};
      $collided{$_} = 1;
   }
   $filenames{$_} = 1;
}

my $first = 1;
print "{";
for my $k (keys %$h) {

   local $_ = $h->{$k};

   my $fn;
   if (/\.x..$/) { $fn = $_ }
   elsif (s/(sparkling|(?:ice|fire|acid)born).(\d)\2\2$/\1.x1\2/) { $fn = $_ }
   elsif (s/\.\d(..)$/.x\1/) { $fn = $_ }
   else { die "error in face for $k: $_" }

   my $file = $fn;
   $file =~ s/^(.*)\.(x..)$/\1.base.\2.png/;

   if ($filenames{$file}) {
      if ($collided{$file}) {
         warn "face $fn -> $file has colliding filenames\n";
      }
      print "," unless $first;
      $first = 0;
      print "\n\"$k\":\"$fn\"";
   } else {
      warn "no such face: $_ -> $fn\n";
   }
}
print "\n}\n";
