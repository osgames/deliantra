" Vim syntax file                                                                                         
" Language:     Crossfire Maps
" Maintainer:   Pippijn van Steenhoven <pip88nl@gmail.com>
" Last Change:  2007 Jan 05
"
" Usage:
"   put these lines in some script:
"   if getline(1) =~ '^arch map$'
"      set ft=cfmap
"   endif
"
"   then put this into your vimrc:
"   au BufReadPost * so /path/to/that/small/script
"
"   I have:
"   au BufReadPost * so ~/.vim/ftdetect/cfmap.vim
"
"   drop this file into ~/.vim/syntax

if exists("b:current_syntax")
   finish
endif

" Comments: {{{1
"==========
syn cluster cfCommentGroup      contains=cfTodo,@Spell
syn keyword cfTodo              contained COMBAK FIXME TODO XXX
syn match   cfComment           "#.*$" contains=@cfCommentGroup

" Keywords: {{{1
"==========
syn keyword cfBlock1            arch end

syn keyword cfMap               darkness width height enter_x enter_y per_player region difficulty
syn keyword cfMap               reset_timeout reset_time swap_time maplore endmaplore
syn keyword cfMap               fixed_resettime per_player per_party shopitems shopgreed shopmin
syn keyword cfMap               shopmax shoprace outdoor tile_path_1 tile_path_2 tile_path_3 tile_path_4
syn keyword cfMap               file_format_version temp pressure humid windspeed winddir sky map

syn keyword cfArch              no_pass walk_on walk_off fly_on fly_off
syn keyword cfArch              flying ac activate_on_push activate_on_release alive anim_speed animation
syn keyword cfArch              applied armour attach attack_movement attacktype auto_apply
syn keyword cfArch              been_applied berserk blocksview body_range body_arm body_torso body_head
syn keyword cfArch              body_neck body_skill body_finger body_shoulder body_foot body_hand
syn keyword cfArch              body_wrist body_waist can_apply can_cast_spell can_roll can_see_in_dark
syn keyword cfArch              can_use_armour can_use_bow can_use_horn can_use_range can_use_ring
syn keyword cfArch              can_use_rod can_use_scroll can_use_shield can_use_skill can_use_wand
syn keyword cfArch              can_use_weapon carrying casting_time cha changing client_type con
syn keyword cfArch              confused connected container cursed custom_name dam dam_modifier damned
syn keyword cfArch              dex direction duration duration_modifier editable editor_folder elevation
syn keyword cfArch              endlore exp expmul face food friendly gen_sp_armour generator
syn keyword cfArch              glow_radius grace has_ready_bow has_ready_horn has_ready_rod
syn keyword cfArch              has_ready_scroll has_ready_skill has_ready_wand has_ready_weapon
syn keyword cfArch              has_ready_range hitback hp identified immune int inv_locked invisible
syn keyword cfArch              is_animated is_blind is_buildable is_cauldron is_dust is_floor is_hilly
syn keyword cfArch              is_lightable is_thrown is_turnable is_used_up is_water is_wooded
syn keyword cfArch              item_power known_cursed known_magical last_eat last_grace last_heal
syn keyword cfArch              last_sp level lifesave lore luck magic make_invisible material
syn keyword cfArch              materialname maxgrace maxhp maxsp monster more move_allow move_block
syn keyword cfArch              move_off move_on move_slow move_slow_penalty move_state move_type
syn keyword cfArch              neutral no_attack no_damage no_drop no_fix_player no_magic
syn keyword cfArch              no_pick no_skill_ident no_steal no_strength nrof object oid one_hit
syn keyword cfArch              only_attack other_arch overlay_floor path_attuned path_denied
syn keyword cfArch              path_repelled perm_exp pick_up player_sold pow protected race random_move
syn keyword cfArch              random_movement randomitems range range_modifier reflect_missile
syn keyword cfArch              reflect_spell reflecting resist_acid resist_blind resist_cancellation
syn keyword cfArch              resist_chaos resist_cold resist_confusion resist_counterspell
syn keyword cfArch              resist_death resist_deplete resist_disease resist_drain
syn keyword cfArch              resist_electricity resist_fear resist_fire resist_ghosthit
syn keyword cfArch              resist_godpower resist_holyword resist_internal resist_life_stealing
syn keyword cfArch              resist_magic resist_paralyze resist_physical resist_poison resist_slow
syn keyword cfArch              resist_turn_undead resist_weaponmagic run_away scared see_anywhere
syn keyword cfArch              see_invisible skill slaying sleep slow_move smoothlevel sp speed
syn keyword cfArch              speed_left splitting stand_still startequip state stealth str subtype
syn keyword cfArch              tear_down title tooltype treasure type unaggressive undead unique unpaid
syn keyword cfArch              use_content_on_gen uuid value vulnerable was_wiz wc weapontype weight
syn keyword cfArch              will_apply wis wiz x xrays y
syn keyword cfArch              savebed_map bed_x bed_y password explore shoottype bowtype petmode gen_hp
syn keyword cfArch              gen_sp gen_grace listening peaceful digestion pickup outputs_sync
syn keyword cfArch              outputs_count usekeys unapply weapon_sp lev_array endplst

" Strings: {{{1
"=========
syn region  cfString            matchgroup=cfBlock2 start=+^msg$+ end=+^endmsg$+
syn region  cfString            matchgroup=cfArch   start=+^name + end=+$+
syn region  cfString            matchgroup=cfArch   start=+^name_pl + end=+$+
syn region  cfString            matchgroup=cfBlock2 start=+^anim$+ end=+^mina$+

" Numbers: {{{1
"=========
syn match   cfNumber            "[-+]\=\(\<\d[[:digit:]_]*L\=\>\|0[xX]\x[[:xdigit:]_]*\>\)"

" Highlights: {{{1
"============
hi def link cfArch              cfStatement
hi def link cfBlock1            cfSpecial
hi def link cfBlock2            cfIdentifier
hi def link cfMap               cfPreProc

hi def link cfNumber            Number
hi def link cfTodo              Todo
hi def link cfComment           Comment
hi def link cfStatement         Statement
hi def link cfSpecial           Special
hi def link cfIdentifier        Identifier
hi def link cfPreProc           PreProc
hi def link cfString            String
