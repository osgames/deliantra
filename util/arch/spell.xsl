<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="html"/>
   <xsl:template name="row">
      <td><xsl:value-of select="name"/></td>
      <td><xsl:value-of select="level"/></td>
      <td><xsl:value-of select="sp"/></td>
      <td><xsl:value-of select="casting_time"/></td>
      <td><xsl:value-of select="attacktype"/></td>
      <td><xsl:value-of select="skill"/></td>
   </xsl:template>
   <xsl:template match="arches">
      <html>
         <head>
            <title>Arches</title>
         </head>
         <body>
            <table border="1" style="width:100%;">
               <tr>
                  <th>Name</th>
                  <th>Level</th>
                  <th>Sp</th>
                  <th>Casting Time</th>
                  <th>Attacktype</th>
                  <th>Skill</th>
               </tr>
               <xsl:for-each select="object">
                  <xsl:if test="type = 101">
                     <xsl:if test=".[skill='evocation']">
                        <tr style="color:black;background:#9999ff;">
                           <xsl:call-template name="row"/>
                        </tr>
                     </xsl:if>
                     <xsl:if test=".[skill='pyromancy']">
                        <tr style="color:black;background:#ff9999;">
                           <xsl:call-template name="row"/>
                        </tr>
                     </xsl:if>
                     <xsl:if test=".[skill='sorcery']">
                        <tr style="color:black;background:#ffff99;">
                           <xsl:call-template name="row"/>
                        </tr>
                     </xsl:if>
                     <xsl:if test=".[skill='praying']">
                        <tr style="color:black;background:white;">
                           <xsl:call-template name="row"/>
                        </tr>
                     </xsl:if>
                     <xsl:if test=".[skill='summoning']">
                        <tr style="color:black;background:#c06a6a;">
                           <xsl:call-template name="row"/>
                        </tr>
                     </xsl:if>
                     <xsl:if test=".[skill='none']">
                        <tr style="color:black;background:grey;">
                           <xsl:call-template name="row"/>
                        </tr>
                     </xsl:if>
                  </xsl:if>
               </xsl:for-each>
            </table>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
