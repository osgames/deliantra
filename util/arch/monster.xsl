<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="html"/>
   <xsl:template name="row">
      <td><img src="{face}" alt="{face}"/></td>
      <td><xsl:value-of select="arch"/></td>
      <td><xsl:value-of select="name"/></td>
      <td><xsl:value-of select="race"/></td>
      <td><xsl:value-of select="str"/></td>
      <td><xsl:value-of select="con"/></td>
      <td><xsl:value-of select="dex"/></td>
      <td><xsl:value-of select="wis"/></td>
      <td><xsl:value-of select="pow"/></td>
      <td><xsl:value-of select="int"/></td>
      <td><xsl:value-of select="cha"/></td>
      <td><xsl:value-of select="maxhp"/></td>
      <td><xsl:value-of select="maxsp"/></td>
      <td><xsl:value-of select="dam"/></td>
      <td><xsl:value-of select="wc"/></td>
      <td><xsl:value-of select="ac"/></td>
      <td><xsl:value-of select="level"/></td>
   </xsl:template>
   <xsl:template match="arches">
      <html>
         <head>
            <title>Arches</title>
         </head>
         <body>
            <table border="1" style="width:100%;">
               <tr>
                  <th>Face</th>
                  <th>Arch</th>
                  <th>Name</th>
                  <th>Race</th>
                  <th>Str</th>
                  <th>Con</th>
                  <th>Dex</th>
                  <th>Wis</th>
                  <th>Pow</th>
                  <th>Int</th>
                  <th>Cha</th>
                  <th>Hp</th>
                  <th>Sp</th>
                  <th>Dam</th>
                  <th>Wc</th>
                  <th>Ac</th>
                  <th>Level</th>
               </tr>
               <xsl:for-each select="object">
                  <xsl:if test=".[monster='1'] and .[name!=''] and not(x) and not(y) and not(.[type='101'])">
                     <tr style="color:black;background:white;">
                        <xsl:call-template name="row"/>
                     </tr>
                  </xsl:if>
               </xsl:for-each>
            </table>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
