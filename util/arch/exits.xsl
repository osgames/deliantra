<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="html"/>
   <xsl:template name="row">
      <td><img src="{face}" alt="{face}"/></td>
      <td><xsl:value-of select="arch"/></td>
      <td><xsl:value-of select="name"/></td>
   </xsl:template>
   <xsl:template match="arches">
      <html>
         <head>
            <title>Arches</title>
         </head>
         <body>
            <table border="1" style="width:100%;">
               <tr>
                  <th>Face</th>
                  <th>Arch</th>
                  <th>Name</th>
               </tr>
               <xsl:for-each select="object">
                  <xsl:if test=".[name!=''] and not(x) and not(y) and .[type='66']">
                     <tr style="color:black;background:white;">
                        <xsl:call-template name="row"/>
                     </tr>
                  </xsl:if>
               </xsl:for-each>
            </table>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
