object u_horn
anim
u_horn.x11
u_horn.x12
u_horn.x13
u_horn.x14
u_horn.x15
mina
inherit type_weapon
name unicorn horn
name_pl unicorn horns
skill one handed weapons
face u_horn.x11
dam 9
nrof 1
materialname organic
value 500
weight 10000
magic 3
last_sp 9
client_type 627
item_power 2
body_arm -1
body_combat -1
end

