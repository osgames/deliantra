object u_horn_black
anim
u_horn_black.x11
u_horn_black.x12
u_horn_black.x13
u_horn_black.x14
u_horn_black.x15
mina
name black unicorn horn
name_pl black unicorn horns
skill one handed weapons
face u_horn_black.x11
dam 9
nrof 1
type 15
materialname organic
value 500
weight 10000
magic 3
last_sp 9
client_type 627
item_power 2
body_arm -1
body_combat -1
end

