object blood_red
name puddle of red blood
name_pl puddles of red blood
face blood_red.x11
food 3
nrof 1
type 54
materialname blood
weight 20
no_pick 1
identified 1
end

object blood_red2
name puddle of red blood
name_pl puddles of red blood
face blood_red2.x11
food 3
nrof 1
type 54
materialname blood
weight 20
no_pick 1
identified 1
end

object blood_red2_medium
name puddle of red blood
name_pl puddles of red blood
face blood_red2_medium.x11
food 2
nrof 1
type 54
materialname blood
weight 20
no_pick 1
identified 1
end

object blood_red2_small
name small puddle of red blood
name_pl small puddles of red blood
face blood_red2_small.x11
food 1
nrof 1
type 54
materialname blood
weight 20
no_pick 1
identified 1
end

object blood_red_medium
name puddle of red blood
name_pl puddles of red blood
face blood_red_medium.x11
food 2
nrof 1
type 54
materialname blood
weight 20
no_pick 1
identified 1
end

object blood_red_small
name small puddle of red blood
name_pl small puddles of red blood
face blood_red_small.x11
food 1
nrof 1
type 54
materialname blood
weight 20
no_pick 1
identified 1
end

