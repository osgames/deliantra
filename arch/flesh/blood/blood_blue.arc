object blood_blue
name puddle of blue blood
name_pl puddles of blue blood
face blood_blue.x11
food 3
nrof 1
type 54
materialname blood
weight 20
no_pick 1
identified 1
end

object blood_blue_medium
name puddle of blue blood
name_pl puddles of blue blood
face blood_blue_medium.x11
food 2
nrof 1
type 54
materialname blood
weight 20
no_pick 1
identified 1
end

object blood_blue_small
name small puddle of blue blood
name_pl small puddles of blue blood
face blood_blue_small.x11
food 1
nrof 1
type 54
materialname blood
weight 20
no_pick 1
identified 1
end

