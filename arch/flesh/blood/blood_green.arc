object blood_green
name puddle of green blood
name_pl puddles of green blood
face blood_green.x11
food 3
nrof 1
type 54
materialname blood
weight 20
no_pick 1
identified 1
end

object blood_green_medium
name puddle of green blood
name_pl puddles of green blood
face blood_green_medium.x11
food 2
nrof 1
type 54
materialname blood
weight 20
no_pick 1
identified 1
end

object blood_green_small
name small puddle of green blood
name_pl small puddles of green blood
face blood_green_small.x11
food 1
nrof 1
type 54
materialname blood
weight 20
no_pick 1
identified 1
end

