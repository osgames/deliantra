object cathedral
name cathedral
face cathedral.x11
type 66
client_type 25012
move_block all
no_pick 1
end
more
object cathedral_2
name cathedral
face cathedral.x11
x 1
type 66
move_block all
no_pick 1
end
more
object cathedral_3
name cathedral
face cathedral.x11
x 2
type 66
move_block all
no_pick 1
end
more
object cathedral_4
name cathedral
face cathedral.x11
y 1
type 66
move_block all
no_pick 1
end
more
object cathedral_5
name cathedral
face cathedral.x11
x 1
y 1
type 66
move_block all
no_pick 1
end
more
object cathedral_6
name cathedral
face cathedral.x11
x 2
y 1
type 66
move_block all
no_pick 1
end
more
object cathedral_7
name cathedral
face cathedral.x11
y 2
type 66
move_block all
no_pick 1
end
more
object cathedral_8
name cathedral
face cathedral.x11
x 1
y 2
type 66
move_block all
no_pick 1
end
more
object cathedral_9
name cathedral
face cathedral.x11
x 2
y 2
type 66
move_block all
no_pick 1
end
more
object cathedral_10
name cathedral
face cathedral.x11
y 3
type 66
move_block all
no_pick 1
end
more
object cathedral_11
name cathedral
face cathedral.x11
x 1
y 3
type 66
move_block -all
no_pick 1
end
more
object cathedral_12
name cathedral
face cathedral.x11
x 2
y 3
type 66
move_block all
no_pick 1
end

