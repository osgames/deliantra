object a_civic
name civic building
face a_civic.x11
type 66
client_type 25012
move_block all
no_pick 1
end
more
object a_civic_2
name civic building
face a_civic.x11
x 1
type 66
move_block all
no_pick 1
end
more
object a_civic_3
name civic building
face a_civic.x11
x 2
type 66
no_pick 1
end
more
object a_civic_4
name civic building
face a_civic.x11
y 1
type 66
no_pick 1
end
more
object a_civic_5
name civic building
face a_civic.x11
x 1
y 1
type 66
no_pick 1
end
more
object a_civic_6
name civic building
face a_civic.x11
x 2
y 1
type 66
no_pick 1
end
more
object a_civic_7
name civic building
face a_civic.x11
y 2
type 66
no_pick 1
end
more
object a_civic_8
name civic building
face a_civic.x11
x 1
y 2
type 66
no_pick 1
end
more
object a_civic_9
name civic building
face a_civic.x11
x 2
y 2
type 66
no_pick 1
end

