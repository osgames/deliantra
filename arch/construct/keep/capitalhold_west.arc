object capitalhold_west
name capitalhold
face capitalhold_west.x11
type 66
client_type 25012
move_block all
no_pick 1
end
more
object capitalhold_2_west
name capitalhold
face capitalhold_west.x11
x 1
type 66
move_block all
no_pick 1
end
more
object capitalhold_3_west
name capitalhold
face capitalhold_west.x11
x 2
type 66
move_block all
no_pick 1
end
more
object capitalhold_4_west
name capitalhold
face capitalhold_west.x11
y 1
type 66
move_block all
no_pick 1
end
more
object capitalhold_5_west
name capitalhold
face capitalhold_west.x11
x 1
y 1
type 66
move_block all
no_pick 1
end
more
object capitalhold_6_west
name capitalhold
face capitalhold_west.x11
x 2
y 1
type 66
move_block all
no_pick 1
end
more
object capitalhold_7_west
name capitalhold
face capitalhold_west.x11
y 2
type 66
move_block all
no_pick 1
end
more
object capitalhold_8_west
name capitalhold
face capitalhold_west.x11
x 1
y 2
type 66
no_pick 1
end
more
object capitalhold_9_west
name capitalhold
face capitalhold_west.x11
x 2
y 2
type 66
move_block -all
no_pick 1
end
more
object capitalhold_10_west
name capitalhold
face capitalhold_west.x11
x 3
type 66
move_block all
no_pick 1
end
more
object capitalhold_11_west
name capitalhold
face capitalhold_west.x11
x 3
y 1
type 66
move_block all
no_pick 1
end
more
object capitalhold_12_west
name capitalhold
face capitalhold_west.x11
x 3
y 2
type 66
move_block -all
no_pick 1
end
more
object capitalhold_13_west
name capitalhold
face capitalhold_west.x11
x 4
type 66
move_block all
no_pick 1
end
more
object capitalhold_14_west
name capitalhold
face capitalhold_west.x11
x 4
y 1
type 66
move_block all
no_pick 1
end
more
object capitalhold_15_west
name capitalhold
face capitalhold_west.x11
x 4
y 2
type 66
move_block all
no_pick 1
end

