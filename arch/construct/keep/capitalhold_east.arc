object capitalhold_east
name 京洛 本塁
name_pl keiraku honrui
face capitalhold_east.x11
type 66
client_type 25012
move_block all
no_pick 1
end
more
object capitalhold_2_east
name 京洛 本塁
name_pl keiraku honrui
face capitalhold_east.x11
x 1
type 66
move_block all
no_pick 1
end
more
object capitalhold_3_east
name 京洛 本塁
name_pl keiraku honrui
face capitalhold_east.x11
x 2
type 66
move_block all
no_pick 1
end
more
object capitalhold_4_east
name 京洛 本塁
name_pl keiraku honrui
face capitalhold_east.x11
y 1
type 66
move_block all
no_pick 1
end
more
object capitalhold_5_east
name 京洛 本塁
name_pl keiraku honrui
face capitalhold_east.x11
x 1
y 1
type 66
move_block all
no_pick 1
end
more
object capitalhold_6_east
name 京洛 本塁
name_pl keiraku honrui
face capitalhold_east.x11
x 2
y 1
type 66
move_block all
no_pick 1
end
more
object capitalhold_7_east
name 京洛 本塁
name_pl keiraku honrui
face capitalhold_east.x11
y 2
type 66
move_block all
no_pick 1
end
more
object capitalhold_8_east
name 京洛 本塁
name_pl keiraku honrui
face capitalhold_east.x11
x 1
y 2
type 66
no_pick 1
end
more
object capitalhold_9_east
name 京洛 本塁
name_pl keiraku honrui
face capitalhold_east.x11
x 2
y 2
type 66
move_block all
no_pick 1
end

