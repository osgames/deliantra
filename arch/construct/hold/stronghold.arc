object stronghold
face stronghold.x11
type 66
client_type 25012
move_block all
no_pick 1
end
more
object stronghold_2
name stronghold
face stronghold.x11
x 1
type 66
move_block all
no_pick 1
end
more
object stronghold_3
name stronghold
face stronghold.x11
x 2
type 66
move_block all
no_pick 1
end
more
object stronghold_4
name stronghold
face stronghold.x11
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_5
name stronghold
face stronghold.x11
x 1
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_6
name stronghold
face stronghold.x11
x 2
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_7
name stronghold
face stronghold.x11
y 2
type 66
move_block all
no_pick 1
end
more
object stronghold_8
name stronghold
face stronghold.x11
x 1
y 2
type 66
no_pick 1
end
more
object stronghold_9
name stronghold
face stronghold.x11
x 2
y 2
type 66
move_block all
no_pick 1
end

