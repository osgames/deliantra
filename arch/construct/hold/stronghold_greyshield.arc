object stronghold_greyshield
face stronghold_greyshield.x11
type 66
move_block all
no_pick 1
end
more
object stronghold_2_greyshield
name stronghold
face stronghold_greyshield.x11
x 1
type 66
move_block all
no_pick 1
end
more
object stronghold_3_greyshield
name stronghold
face stronghold_greyshield.x11
x 2
type 66
move_block all
no_pick 1
end
more
object stronghold_4_greyshield
name stronghold
face stronghold_greyshield.x11
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_5_greyshield
name stronghold
face stronghold_greyshield.x11
x 1
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_6_greyshield
name stronghold
face stronghold_greyshield.x11
x 2
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_7_greyshield
name stronghold
face stronghold_greyshield.x11
y 2
type 66
move_block all
no_pick 1
end
more
object stronghold_8_greyshield
name stronghold
face stronghold_greyshield.x11
x 1
y 2
type 66
no_pick 1
end
more
object stronghold_9_greyshield
name stronghold
face stronghold_greyshield.x11
x 2
y 2
type 66
move_block all
no_pick 1
end

