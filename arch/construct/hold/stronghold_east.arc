object stronghold_east
name 本塁
name_pl honrui
face stronghold_east.x11
type 66
client_type 25012
move_block all
no_pick 1
end
more
object stronghold_2_east
face stronghold_east.x11
x 1
type 66
move_block all
no_pick 1
end
more
object stronghold_3_east
face stronghold_east.x11
x 2
type 66
move_block all
no_pick 1
end
more
object stronghold_4_east
face stronghold_east.x11
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_5_east
face stronghold_east.x11
x 1
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_6_east
face stronghold_east.x11
x 2
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_7_east
face stronghold_east.x11
y 2
type 66
move_block all
no_pick 1
end
more
object stronghold_8_east
face stronghold_east.x11
x 1
y 2
type 66
no_pick 1
end
more
object stronghold_9_east
face stronghold_east.x11
x 2
y 2
type 66
move_block all
no_pick 1
end

