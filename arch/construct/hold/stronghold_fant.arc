object stronghold_fant
face stronghold_fant.x11
type 66
move_block all
no_pick 1
end
more
object stronghold_2_fant
name stronghold
face stronghold_fant.x11
x 1
type 66
move_block all
no_pick 1
end
more
object stronghold_3_fant
name stronghold
face stronghold_fant.x11
x 2
type 66
move_block all
no_pick 1
end
more
object stronghold_4_fant
name stronghold
face stronghold_fant.x11
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_5_fant
name stronghold
face stronghold_fant.x11
x 1
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_6_fant
name stronghold
face stronghold_fant.x11
x 2
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_7_fant
name stronghold
face stronghold_fant.x11
y 2
type 66
move_block all
no_pick 1
end
more
object stronghold_8_fant
name stronghold
face stronghold_fant.x11
x 1
y 2
type 66
no_pick 1
end
more
object stronghold_9_fant
name stronghold
face stronghold_fant.x11
x 2
y 2
type 66
move_block all
no_pick 1
end

