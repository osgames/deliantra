object stronghold_northwest
face stronghold_northwest.x11
type 66
move_block all
no_pick 1
end
more
object stronghold_2_northwest
name stronghold
face stronghold_northwest.x11
x 1
type 66
move_block all
no_pick 1
end
more
object stronghold_3_northwest
name stronghold
face stronghold_northwest.x11
x 2
type 66
move_block all
no_pick 1
end
more
object stronghold_4_northwest
name stronghold
face stronghold_northwest.x11
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_5_northwest
name stronghold
face stronghold_northwest.x11
x 1
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_6_northwest
name stronghold
face stronghold_northwest.x11
x 2
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_7_northwest
name stronghold
face stronghold_northwest.x11
y 2
type 66
move_block all
no_pick 1
end
more
object stronghold_8_northwest
name stronghold
face stronghold_northwest.x11
x 1
y 2
type 66
no_pick 1
end
more
object stronghold_9_northwest
name stronghold
face stronghold_northwest.x11
x 2
y 2
type 66
move_block all
no_pick 1
end

