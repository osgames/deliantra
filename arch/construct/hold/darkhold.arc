object darkhold
name darkhold
face darkhold.x11
type 66
client_type 25012
move_block all
no_pick 1
end
more
object darkhold_2
name darkhold
face darkhold.x11
x 1
type 66
move_block all
no_pick 1
end
more
object darkhold_3
name darkhold
face darkhold.x11
x 2
type 66
move_block all
no_pick 1
end
more
object darkhold_4
name darkhold
face darkhold.x11
y 1
type 66
move_block all
no_pick 1
end
more
object darkhold_5
name darkhold
face darkhold.x11
x 1
y 1
type 66
move_block all
no_pick 1
end
more
object darkhold_6
name darkhold
face darkhold.x11
x 2
y 1
type 66
move_block all
no_pick 1
end
more
object darkhold_7
name darkhold
face darkhold.x11
y 2
type 66
move_block all
no_pick 1
end
more
object darkhold_8
name darkhold
face darkhold.x11
x 1
y 2
type 66
move_block all
no_pick 1
end
more
object darkhold_9
name darkhold
face darkhold.x11
x 2
y 2
type 66
move_block all
no_pick 1
end
more
object darkhold_10
name darkhold
face darkhold.x11
y 3
type 66
move_block all
no_pick 1
end
more
object darkhold_11
name darkhold
face darkhold.x11
x 1
y 3
type 66
move_block all
no_pick 1
end
more
object darkhold_12
name darkhold
face darkhold.x11
x 2
y 3
type 66
move_block all
no_pick 1
end
more
object darkhold_13
name darkhold
face darkhold.x11
y 4
type 66
move_block all
no_pick 1
end
more
object darkhold_14
name darkhold
face darkhold.x11
x 1
y 4
type 66
no_pick 1
end
more
object darkhold_15
name darkhold
face darkhold.x11
x 2
y 4
type 66
move_block all
no_pick 1
end

