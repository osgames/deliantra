object stronghold_fant_symmetrical
face stronghold_fant_symmetrical.x11
type 66
client_type 25012
move_block all
no_pick 1
end
more
object stronghold_2_fant_symmetrical
name stronghold
face stronghold_fant_symmetrical.x11
x 1
type 66
move_block all
no_pick 1
end
more
object stronghold_3_fant_symmetrical
name stronghold
face stronghold_fant_symmetrical.x11
x 2
type 66
move_block all
no_pick 1
end
more
object stronghold_4_fant_symmetrical
name stronghold
face stronghold_fant_symmetrical.x11
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_5_fant_symmetrical
name stronghold
face stronghold_fant_symmetrical.x11
x 1
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_6_fant_symmetrical
name stronghold
face stronghold_fant_symmetrical.x11
x 2
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_7_fant_symmetrical
name stronghold
face stronghold_fant_symmetrical.x11
y 2
type 66
move_block all
no_pick 1
end
more
object stronghold_8_fant_symmetrical
name stronghold
face stronghold_fant_symmetrical.x11
x 1
y 2
type 66
no_pick 1
end
more
object stronghold_9_fant_symmetrical
name stronghold
face stronghold_fant_symmetrical.x11
x 2
y 2
type 66
move_block all
no_pick 1
end

