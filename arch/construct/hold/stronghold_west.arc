object stronghold_west
face stronghold_west.x11
type 66
client_type 25012
move_block all
no_pick 1
end
more
object stronghold_2_west
name stronghold
face stronghold_west.x11
x 1
type 66
move_block all
no_pick 1
end
more
object stronghold_3_west
name stronghold
face stronghold_west.x11
x 2
type 66
move_block all
no_pick 1
end
more
object stronghold_4_west
name stronghold
face stronghold_west.x11
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_5_west
name stronghold
face stronghold_west.x11
x 1
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_6_west
name stronghold
face stronghold_west.x11
x 2
y 1
type 66
move_block all
no_pick 1
end
more
object stronghold_7_west
name stronghold
face stronghold_west.x11
y 2
type 66
move_block all
no_pick 1
end
more
object stronghold_8_west
name stronghold
face stronghold_west.x11
x 1
y 2
type 66
no_pick 1
end
more
object stronghold_9_west
name stronghold
face stronghold_west.x11
x 2
y 2
type 66
move_block all
no_pick 1
end

