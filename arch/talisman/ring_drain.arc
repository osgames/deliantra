object ring_nodrain
anim
ring_drain.x11
ring_drain.x12
ring_drain.x13
ring_drain.x14
mina
name strange ring
name_pl strange rings
face ring_drain.x11
speed 0.1
nrof 1
type 70
resist_drain 100
materialname iron
value 10000
weight 40
client_type 390
item_power 4
body_finger -1
end

