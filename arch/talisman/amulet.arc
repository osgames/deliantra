object amulet
name_pl amulets
face amulet.x11
nrof 1
type 39
materialname runestone
value 250
weight 500
client_type 381
body_neck -1
need_an 1
end

object amulet_gray
name amulet
name_pl amulets
face amulet_gray.x11
nrof 1
type 39
materialname silver
value 25
weight 500
client_type 381
body_neck -1
need_an 1
end

object amulet_white
name amulet
name_pl amulets
face amulet_white.x11
nrof 1
type 39
materialname platinum
value 1250
weight 500
client_type 381
body_neck -1
need_an 1
end

