object amulet_lifesave
name amulet of lifesaving
name_pl amulets of lifesaving
face amulet_lif.x11
nrof 1
type 39
materialname runestone
value 20000
weight 600
client_type 381
item_power 3
lifesave 1
body_neck -1
end

