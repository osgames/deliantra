object ring
anim
ring.x11
ring.x12
ring.x13
ring.x14
ring.x15
ring.x16
mina
name ring
name_pl rings
face ring.x10
is_animated 0
nrof 1
type 70
materialname iron
value 500
weight 20
client_type 391
body_finger -1
end

object white_ring
name ring
name_pl rings
face ring.x17
nrof 1
type 70
materialname iron
value 2000
weight 20
client_type 391
body_finger -1
end

