object tooth_charm
name tooth charm
name_pl tooth charms
msg
  This wonderful charm will absorb many
  magics which would otherwise affect your
  movement.
endmsg
face tooth_charm.x11
nrof 1
type 39
resist_confusion 15
resist_slow 30
resist_paralyze 50
resist_fear 50
materialname runestone
weight 300
client_type 381
item_power 6
startequip 1
body_neck -1
need_an 1
end

