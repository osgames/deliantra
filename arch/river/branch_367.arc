object rcrook_we-sw
name river junction
face rcrook_we-sw.x11
move_block all
no_pick 1
is_water 1
end

object rjunct_we-sw
name river junction
face rjunct_we-sw.x11
move_block all
no_pick 1
is_water 1
end

