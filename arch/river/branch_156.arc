object rcrook_ns-sw
name river junction
face rcrook_ns-sw.x11
move_block all
no_pick 1
is_water 1
end

object rjunct_ns-sw
name river junction
face rjunct_ns-sw.x11
move_block all
no_pick 1
is_water 1
end

