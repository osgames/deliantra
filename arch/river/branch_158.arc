object rcrook_ns-nw
name river junction
face rcrook_ns-nw.x11
move_block all
no_pick 1
is_water 1
end

object rjunct_ns-nw
name river junction
face rjunct_ns-nw.x11
move_block all
no_pick 1
is_water 1
end

