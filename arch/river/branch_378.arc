object rcrook_we-nw
name river junction
face rcrook_we-nw.x11
move_block all
no_pick 1
is_water 1
end

object rjunct_we-nw
name river junction
face rjunct_we-nw.x11
move_block all
no_pick 1
is_water 1
end

