object rcrook_ns-ne
name river junction
face rcrook_ns-ne.x11
move_block all
no_pick 1
is_water 1
end

object rjunct_ns-ne
name river junction
face rjunct_ns-ne.x11
move_block all
no_pick 1
is_water 1
end

