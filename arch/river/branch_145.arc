object rcrook_ns-se
name river junction
face rcrook_ns-se.x11
move_block all
no_pick 1
is_water 1
end

object rjunct_ns-se
name river junction
face rjunct_ns-se.x11
move_block all
no_pick 1
is_water 1
end

