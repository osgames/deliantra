object rcrook_we-ne
name river junction
face rcrook_we-ne.x11
move_block all
no_pick 1
is_water 1
end

object rjunct_we-ne
name river junction
face rjunct_we-ne.x11
move_block all
no_pick 1
is_water 1
end

