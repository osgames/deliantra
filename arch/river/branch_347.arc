object rcrook_we-se
name river junction
face rcrook_we-se.x11
move_block all
no_pick 1
is_water 1
end

object rjunct_we-se
name river junction
face rjunct_we-se.x11
move_block all
no_pick 1
is_water 1
end

