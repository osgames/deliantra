object Nimwall_0
anim
Nimwall12.x11
Nimwall12.x12
Nimwall12.x13
mina
name Pulsating Nexus
face Nimwall12.x11
is_animated 1
speed -0.2
move_block all
no_pick 1
blocksview 1
end

object Nimwall_1_1
anim
Nimwall13.x11
Nimwall13.x12
Nimwall13.x13
mina
name Pulsating Nexus
face Nimwall13.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object Nimwall_1_2
anim
Nimwall14.x11
Nimwall14.x12
Nimwall14.x13
mina
name Pulsating Nexus
face Nimwall14.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object Nimwall_1_3
anim
Nimwall16.x11
Nimwall16.x12
Nimwall16.x13
mina
name Pulsating Nexus
face Nimwall16.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object Nimwall_1_4
anim
Nimwall15.x11
Nimwall15.x12
Nimwall15.x13
mina
name Pulsating Nexus
face Nimwall15.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object Nimwall_2_1_1
anim
Nimwall1.x11
Nimwall1.x12
Nimwall1.x13
mina
name Pulsating Nexus
face Nimwall1.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object Nimwall_2_1_2
anim
Nimwall3.x11
Nimwall3.x12
Nimwall3.x13
mina
name Pulsating Nexus
face Nimwall3.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object Nimwall_2_2_1
anim
Nimwall5.x11
Nimwall5.x12
Nimwall5.x13
mina
name Pulsating Nexus
face Nimwall5.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object Nimwall_2_2_2
anim
Nimwall4.x11
Nimwall4.x12
Nimwall4.x13
mina
name Pulsating Nexus
face Nimwall4.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object Nimwall_2_2_3
anim
Nimwall6.x11
Nimwall6.x12
Nimwall6.x13
mina
name Pulsating Nexus
face Nimwall6.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object Nimwall_2_2_4
anim
Nimwall2.x11
Nimwall2.x12
Nimwall2.x13
mina
name Pulsating Nexus
face Nimwall2.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object Nimwall_3_1
anim
Nimwall8.x11
Nimwall8.x12
Nimwall8.x13
mina
name Pulsating Nexus
face Nimwall8.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object Nimwall_3_2
anim
Nimwall11.x11
Nimwall11.x12
Nimwall11.x13
mina
name Pulsating Nexus
face Nimwall11.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object Nimwall_3_3
anim
Nimwall7.x11
Nimwall7.x12
Nimwall7.x13
mina
name Pulsating Nexus
face Nimwall7.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object Nimwall_3_4
anim
Nimwall10.x11
Nimwall10.x12
Nimwall10.x13
mina
name Pulsating Nexus
face Nimwall10.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object Nimwall_4
anim
Nimwall9.x11
Nimwall9.x12
Nimwall9.x13
mina
name Pulsating Nexus
face Nimwall9.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

