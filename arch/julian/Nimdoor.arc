object Nimdoorcl1
anim
Nimdoorcl1.x11
Nimdoorcl1.x12
Nimdoorcl1.x13
mina
name Energy barrier
face Nimdoorcl1.x11
is_animated 1
hp 400
exp 1
ac 10
speed 0.2
level 1
materialname liquid
move_block all
alive 1
no_pick 1
blocksview 1
is_door 1
end

object Nimdoorcl2
anim
Nimdoorcl2.x11
Nimdoorcl2.x12
Nimdoorcl2.x13
mina
name Energy barrier
face Nimdoorcl2.x11
is_animated 1
hp 400
exp 1
ac 10
speed 0.2
level 1
materialname liquid
move_block all
alive 1
no_pick 1
blocksview 1
is_door 1
end

object Nimdoorop1
anim
Nimdoorop1.x11
Nimdoorop1.x12
Nimdoorop1.x13
mina
name Energy field
face Nimdoorop1.x11
is_animated 1
speed 0.2
materialname liquid
move_block -all
no_pick 1
blocksview 1
end

object Nimdoorop2
anim
Nimdoorop2.x11
Nimdoorop2.x12
Nimdoorop2.x13
mina
name Energy field
face Nimdoorop2.x11
is_animated 1
speed 0.2
materialname liquid
move_block -all
no_pick 1
blocksview 1
end

