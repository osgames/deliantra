object igateTrg2
anim
iron_gate2.x11
iron_gate2.x12
iron_gate2.x13
iron_gate2.x14
iron_gate2.x15
iron_gate2.x16
mina
name iron gate
face iron_gate2.x16
is_animated 0
hp 20
maxhp 20
maxsp 0
dam 6
wc 5
speed 0.35
type 26
value 1
move_block all
no_pick 1
blocksview 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

