object igate_closed_1
anim
iron_gate1.x11
iron_gate1.x12
iron_gate1.x13
iron_gate1.x14
iron_gate1.x15
iron_gate1.x16
iron_gate1.x17
iron_gate1.x18
mina
name iron gate
face iron_gate1.x18
is_animated 0
maxsp 0
dam 6
wc 7
type 91
move_block all
no_pick 1
blocksview 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

object igate_closed_2
anim
iron_gate2.x11
iron_gate2.x12
iron_gate2.x13
iron_gate2.x14
iron_gate2.x15
iron_gate2.x16
mina
name iron gate
face iron_gate2.x16
is_animated 0
maxsp 0
dam 6
wc 5
type 91
move_block all
no_pick 1
blocksview 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

object igate_open_1
anim
iron_gate1.x11
iron_gate1.x12
iron_gate1.x13
iron_gate1.x14
iron_gate1.x15
iron_gate1.x16
iron_gate1.x17
iron_gate1.x18
mina
name iron gate
face iron_gate1.x11
is_animated 0
maxsp 1
dam 6
wc 0
type 91
no_pick 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

object igate_open_2
anim
iron_gate2.x11
iron_gate2.x12
iron_gate2.x13
iron_gate2.x14
iron_gate2.x15
iron_gate2.x16
mina
name iron gate
face iron_gate2.x11
is_animated 0
hp 0
maxsp 1
dam 6
type 91
no_pick 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

