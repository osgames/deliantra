object igateTrg1
anim
iron_gate1.x11
iron_gate1.x12
iron_gate1.x13
iron_gate1.x14
iron_gate1.x15
iron_gate1.x16
iron_gate1.x17
iron_gate1.x18
mina
name iron gate
face iron_gate1.x18
is_animated 0
hp 20
maxhp 20
maxsp 0
dam 6
wc 7
speed 0.35
type 26
value 1
move_block all
no_pick 1
blocksview 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

