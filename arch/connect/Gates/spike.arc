object spike_closed
anim
spike.x22
spike.x21
spike.x19
spike.x18
spike.x17
spike.x16
spike.x15
spike.x14
spike.x13
spike.x12
spike.x11
mina
name spike
face spike.x11
is_animated 0
maxsp 0
dam 256
wc 8
ac 1
type 91
move_block all
no_pick 1
activate_on_push 1
activate_on_release 1
end

object spike_hidden
anim
spike.x10
spike.x19
spike.x18
spike.x17
spike.x16
spike.x15
spike.x14
spike.x13
spike.x12
spike.x11
mina
name spike
face spike.x10
is_animated 0
maxsp 1
dam 256
wc 0
ac 1
type 91
no_pick 1
activate_on_push 1
activate_on_release 1
end

object spike_moving
anim
spike.x22
spike.x21
spike.x20
spike.x19
spike.x18
spike.x17
spike.x16
spike.x15
spike.x14
spike.x13
spike.x12
spike.x11
mina
name spike
face spike.x14
is_animated 0
maxsp 0
dam 256
wc 6
ac 1
speed -2
type 91
move_block all
no_pick 1
activate_on_push 1
activate_on_release 1
end

object spike_open
anim
spike.x22
spike.x21
spike.x19
spike.x18
spike.x17
spike.x16
spike.x15
spike.x14
spike.x13
spike.x12
spike.x11
mina
name spike
face spike.x22
is_animated 0
maxsp 1
dam 256
wc 0
ac 1
type 91
no_pick 1
activate_on_push 1
activate_on_release 1
end

