object grate_closed_1
anim
grate_1.x11
grate_1.x12
grate_1.x13
grate_1.x14
grate_1.x15
grate_1.x16
grate_1.x17
grate_1.x18
mina
name grate
face grate_1.x18
is_animated 0
maxsp 0
dam 5
wc 7
ac 1
type 91
move_block all
no_pick 1
no_magic 1
activate_on_push 1
activate_on_release 1
end

object grate_closed_2
anim
grate_2.x11
grate_2.x12
grate_2.x13
grate_2.x14
grate_2.x15
grate_2.x16
grate_2.x17
grate_2.x18
mina
name grate
face grate_2.x18
is_animated 0
maxsp 0
dam 5
wc 7
ac 1
type 91
move_block all
no_pick 1
no_magic 1
activate_on_push 1
activate_on_release 1
end

object grate_open_1
anim
grate_1.x11
grate_1.x12
grate_1.x13
grate_1.x14
grate_1.x15
grate_1.x16
grate_1.x17
grate_1.x18
mina
name grate
face grate_1.x11
is_animated 0
maxsp 1
dam 5
wc 0
ac 1
type 91
no_pick 1
activate_on_push 1
activate_on_release 1
end

object grate_open_2
anim
grate_2.x11
grate_2.x12
grate_2.x13
grate_2.x14
grate_2.x15
grate_2.x16
grate_2.x17
grate_2.x18
mina
name iron gate
face grate_2.x11
is_animated 0
hp 0
maxsp 1
dam 5
ac 1
type 91
no_pick 1
activate_on_push 1
activate_on_release 1
end

