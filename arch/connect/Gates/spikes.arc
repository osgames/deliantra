object spikes_closed
anim
spikes.x19
spikes.x18
spikes.x17
spikes.x16
spikes.x15
spikes.x14
spikes.x13
spikes.x12
spikes.x11
mina
name spikes
face spikes.x11
is_animated 0
maxsp 0
dam 8
wc 8
ac 1
type 91
move_block all
no_pick 1
activate_on_push 1
activate_on_release 1
end

object spikes_hidden
anim
spikes.x10
spikes.x18
spikes.x17
spikes.x16
spikes.x15
spikes.x14
spikes.x13
spikes.x12
spikes.x11
mina
name spikes
face spikes.x10
is_animated 0
maxsp 1
dam 8
wc 0
ac 1
type 91
no_pick 1
activate_on_push 1
activate_on_release 1
end

object spikes_moving
anim
spikes.x19
spikes.x18
spikes.x17
spikes.x16
spikes.x15
spikes.x14
spikes.x13
spikes.x12
spikes.x11
mina
name spikes
face spikes.x13
is_animated 0
maxsp 0
dam 8
wc 6
ac 1
speed -1
type 91
move_block all
no_pick 1
activate_on_push 1
activate_on_release 1
end

object spikes_open
anim
spikes.x19
spikes.x18
spikes.x17
spikes.x16
spikes.x15
spikes.x14
spikes.x13
spikes.x12
spikes.x11
mina
name spikes
face spikes.x19
is_animated 0
maxsp 1
dam 8
wc 0
ac 1
type 91
no_pick 1
activate_on_push 1
activate_on_release 1
end

