object gate_closed_1
anim
gate_1.x11
gate_1.x12
gate_1.x13
gate_1.x14
gate_1.x15
gate_1.x16
gate_1.x17
mina
name gate
face gate_1.x17
is_animated 0
maxsp 0
dam 1
wc 6
type 91
move_block all
no_pick 1
blocksview 1
activate_on_push 1
activate_on_release 1
end

object gate_closed_2
anim
gate_2.x11
gate_2.x12
gate_2.x13
gate_2.x13
gate_2.x14
gate_2.x14
gate_2.x15
gate_2.x15
mina
name gate
face gate_2.x15
is_animated 0
maxsp 0
dam 1
wc 7
type 91
move_block all
no_pick 1
blocksview 1
activate_on_push 1
activate_on_release 1
end

object gate_open_1
anim
gate_1.x11
gate_1.x12
gate_1.x13
gate_1.x14
gate_1.x15
gate_1.x16
gate_1.x17
mina
name gate
face gate_1.x11
is_animated 0
maxsp 1
dam 1
wc 0
type 91
no_pick 1
activate_on_push 1
activate_on_release 1
end

object gate_open_2
anim
gate_2.x11
gate_2.x12
gate_2.x13
gate_2.x13
gate_2.x14
gate_2.x14
gate_2.x15
gate_2.x15
mina
name gate
face gate_2.x11
is_animated 0
hp 0
maxsp 1
dam 1
type 91
no_pick 1
activate_on_push 1
activate_on_release 1
end

