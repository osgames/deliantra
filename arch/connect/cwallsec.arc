object cwall_closed_1
anim
cwallsec.x11
cwallsec.x12
cwallsec.x13
cwallsec.x14
cwallsec.x15
cwallsec.x16
cwallsec.x17
cwallsec.x18
mina
name wall
face cwallsec.x18
is_animated 0
maxsp 0
dam 6
wc 7
type 91
move_block all
no_pick 1
blocksview 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

object cwall_open_1
anim
cwallsec.x11
cwallsec.x12
cwallsec.x13
cwallsec.x14
cwallsec.x15
cwallsec.x16
cwallsec.x17
cwallsec.x18
mina
name wall
face cwallsec.x11
is_animated 0
maxsp 1
dam 6
wc 0
type 91
no_pick 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

