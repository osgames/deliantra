object mover_1
anim
director.x11
director.x12
director.x13
mina
name mover
face director.x11
sp 1
speed -0.2
type 40
invisible 1
move_on walk fly_low
no_pick 1
end

object mover_2
anim
director.x21
director.x22
director.x23
director.x24
director.x25
mina
name mover
face director.x21
sp 2
speed -0.2
type 40
invisible 1
move_on walk fly_low
no_pick 1
end

object mover_3
anim
director.x31
director.x32
director.x33
mina
name mover
face director.x31
sp 3
speed -0.2
type 40
invisible 1
move_on walk fly_low
no_pick 1
end

object mover_4
anim
director.x41
director.x42
director.x43
director.x44
director.x45
mina
name mover
face director.x41
sp 4
speed -0.2
type 40
invisible 1
move_on walk fly_low
no_pick 1
end

object mover_5
anim
director.x51
director.x52
director.x53
mina
name mover
face director.x51
sp 5
speed -0.2
type 40
invisible 1
move_on walk fly_low
no_pick 1
end

object mover_6
anim
director.x61
director.x62
director.x63
director.x64
director.x65
mina
name mover
face director.x61
sp 6
speed -0.2
type 40
invisible 1
move_on walk fly_low
no_pick 1
end

object mover_7
anim
director.x71
director.x72
director.x73
mina
name mover
face director.x71
sp 7
speed -0.2
type 40
invisible 1
move_on walk fly_low
no_pick 1
end

object mover_8
anim
director.x81
director.x82
director.x83
director.x84
director.x85
mina
name mover
face director.x81
sp 8
speed -0.2
type 40
invisible 1
move_on walk fly_low
no_pick 1
end

object mover_turn
anim
director.x11
director.x12
director.x13
director.x21
director.x23
director.x25
director.x31
director.x32
director.x33
director.x41
director.x43
director.x45
director.x51
director.x52
director.x53
director.x61
director.x63
director.x65
director.x71
director.x72
director.x73
director.x81
director.x83
director.x85
mina
name mover
face director.x81
maxsp 1
speed 0.2
type 40
invisible 1
move_on walk fly_low
no_pick 1
end

