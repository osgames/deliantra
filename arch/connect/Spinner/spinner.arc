object spinner_-135
anim
spinner.x11
spinner.x13
spinner.x15
spinner.x17
spinner.x19
spinner.x1B
spinner.x1D
mina
name spinner
face spinner.x1B
sp 3
speed 1
type 90
move_on walk fly_low
no_pick 1
end

object spinner_-45
anim
spinner.x11
spinner.x12
spinner.x13
spinner.x14
spinner.x15
spinner.x16
spinner.x17
spinner.x18
spinner.x19
spinner.x1A
spinner.x1B
spinner.x1C
spinner.x1D
mina
name spinner
face spinner.x1B
sp 1
speed 0.5
type 90
move_on walk fly_low
no_pick 1
end

object spinner_-90
anim
spinner.x11
spinner.x12
spinner.x13
spinner.x14
spinner.x15
spinner.x16
spinner.x17
spinner.x18
spinner.x19
spinner.x1A
spinner.x1B
spinner.x1C
spinner.x1D
mina
name spinner
face spinner.x1B
sp 2
speed 1
type 90
move_on walk fly_low
no_pick 1
end

object spinner_135
anim
spinner.x1D
spinner.x1B
spinner.x19
spinner.x17
spinner.x15
spinner.x13
spinner.x11
mina
name spinner
face spinner.x11
sp -3
speed 1
type 90
move_on walk fly_low
no_pick 1
end

object spinner_180
anim
spinner.x11
spinner.x14
spinner.x17
spinner.x1A
spinner.x1D
mina
name spinner
face spinner.x1B
sp 4
speed 1
type 90
move_on walk fly_low
no_pick 1
end

object spinner_45
anim
spinner.x1D
spinner.x1C
spinner.x1B
spinner.x1A
spinner.x19
spinner.x18
spinner.x17
spinner.x16
spinner.x15
spinner.x14
spinner.x13
spinner.x12
spinner.x11
mina
name spinner
face spinner.x11
sp -1
speed 0.5
type 90
move_on walk fly_low
no_pick 1
end

object spinner_90
anim
spinner.x1D
spinner.x1C
spinner.x1B
spinner.x1A
spinner.x19
spinner.x18
spinner.x17
spinner.x16
spinner.x15
spinner.x14
spinner.x13
spinner.x12
spinner.x11
mina
name spinner
face spinner.x11
sp -2
speed 1
type 90
move_on walk fly_low
no_pick 1
end

