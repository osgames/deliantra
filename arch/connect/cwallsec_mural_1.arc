object cwallmural1_closed_1
anim
cwallsec_mural_1.x11
cwallsec_mural_1.x12
cwallsec_mural_1.x13
cwallsec_mural_1.x14
cwallsec_mural_1.x15
cwallsec_mural_1.x16
cwallsec_mural_1.x17
cwallsec_mural_1.x18
mina
name wall
face cwallsec_mural_1.x18
is_animated 0
maxsp 0
dam 6
wc 7
type 91
move_block all
no_pick 1
blocksview 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

object cwallmural1_open_1
anim
cwallsec_mural_1.x11
cwallsec_mural_1.x12
cwallsec_mural_1.x13
cwallsec_mural_1.x14
cwallsec_mural_1.x15
cwallsec_mural_1.x16
cwallsec_mural_1.x17
cwallsec_mural_1.x18
mina
name wall
face cwallsec_mural_1.x11
is_animated 0
maxsp 1
dam 6
wc 0
type 91
no_pick 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

