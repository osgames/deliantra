object pit_closed
anim
hole.x11
hole.x12
hole.x13
hole.x14
hole.x15
hole.x16
hole.x17
hole.x18
hole.x19
hole.x1A
mina
name pit
face hole.x1A
is_animated 0
maxsp 0
wc 10
type 94
range 1
no_pick 1
activate_on_push 1
activate_on_release 1
end

object pit_open
anim
hole.x11
hole.x12
hole.x13
hole.x14
hole.x15
hole.x16
hole.x17
hole.x18
hole.x19
hole.x1A
mina
name pit
face hole.x11
is_animated 0
maxsp 1
wc 0
type 94
range 1
move_on walk
no_pick 1
activate_on_push 1
activate_on_release 1
end

