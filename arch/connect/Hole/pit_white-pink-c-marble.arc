object pit_white-pink-c-marble_111_closed
anim
hole_white-any-c-marble.x11
hole_white-any-c-marble.x11
hole_white-any-c-marble.x11
hole_white-any-c-marble.x11
hole_white-any-c-marble.x11
hole_white-any-c-marble.x11
hole_white-any-c-marble.x11
hole_white-any-c-marble.x11
hole_white-any-c-marble.x11
white-pink-c-marble.x11
mina
name marble
face white-pink-c-marble.x11
is_animated 0
maxsp 0
wc 10
type 94
no_pick 1
is_floor 1
activate_on_push 1
activate_on_release 1
end

object pit_white-pink-c-marble_111_open
anim
hole_white-any-c-marble.x11
hole_white-any-c-marble.x11
hole_white-any-c-marble.x11
hole_white-any-c-marble.x11
hole_white-any-c-marble.x11
hole_white-any-c-marble.x11
hole_white-any-c-marble.x11
hole_white-any-c-marble.x11
hole_white-any-c-marble.x11
white-pink-c-marble.x11
mina
name pit
face hole_white-any-c-marble.x11
is_animated 0
maxsp 1
wc 0
type 94
move_on walk
no_pick 1
is_floor 1
activate_on_push 1
activate_on_release 1
end

object pit_white-pink-c-marble_112_closed
anim
hole_white-any-c-marble.x12
hole_white-any-c-marble.x12
hole_white-any-c-marble.x12
hole_white-any-c-marble.x12
hole_white-any-c-marble.x12
hole_white-any-c-marble.x12
hole_white-any-c-marble.x12
hole_white-any-c-marble.x12
hole_white-any-c-marble.x12
white-pink-c-marble.x12
mina
name marble
face white-pink-c-marble.x12
is_animated 0
maxsp 0
wc 10
type 94
no_pick 1
is_floor 1
activate_on_push 1
activate_on_release 1
end

object pit_white-pink-c-marble_112_open
anim
hole_white-any-c-marble.x12
hole_white-any-c-marble.x12
hole_white-any-c-marble.x12
hole_white-any-c-marble.x12
hole_white-any-c-marble.x12
hole_white-any-c-marble.x12
hole_white-any-c-marble.x12
hole_white-any-c-marble.x12
hole_white-any-c-marble.x12
white-pink-c-marble.x12
mina
name pit
face hole_white-any-c-marble.x12
is_animated 0
maxsp 1
wc 0
type 94
move_on walk
no_pick 1
is_floor 1
activate_on_push 1
activate_on_release 1
end

object pit_white-pink-c-marble_113_closed
anim
hole_white-any-c-marble.x13
hole_white-any-c-marble.x13
hole_white-any-c-marble.x13
hole_white-any-c-marble.x13
hole_white-any-c-marble.x13
hole_white-any-c-marble.x13
hole_white-any-c-marble.x13
hole_white-any-c-marble.x13
hole_white-any-c-marble.x13
white-pink-c-marble.x13
mina
name marble
face white-pink-c-marble.x13
is_animated 0
maxsp 0
wc 10
type 94
no_pick 1
is_floor 1
activate_on_push 1
activate_on_release 1
end

object pit_white-pink-c-marble_113_open
anim
hole_white-any-c-marble.x13
hole_white-any-c-marble.x13
hole_white-any-c-marble.x13
hole_white-any-c-marble.x13
hole_white-any-c-marble.x13
hole_white-any-c-marble.x13
hole_white-any-c-marble.x13
hole_white-any-c-marble.x13
hole_white-any-c-marble.x13
white-pink-c-marble.x13
mina
name pit
face hole_white-any-c-marble.x13
is_animated 0
maxsp 1
wc 0
type 94
move_on walk
no_pick 1
is_floor 1
activate_on_push 1
activate_on_release 1
end

object pit_white-pink-c-marble_114_closed
anim
hole_white-any-c-marble.x14
hole_white-any-c-marble.x14
hole_white-any-c-marble.x14
hole_white-any-c-marble.x14
hole_white-any-c-marble.x14
hole_white-any-c-marble.x14
hole_white-any-c-marble.x14
hole_white-any-c-marble.x14
hole_white-any-c-marble.x14
white-pink-c-marble.x14
mina
name marble
face white-pink-c-marble.x14
is_animated 0
maxsp 0
wc 10
type 94
no_pick 1
is_floor 1
activate_on_push 1
activate_on_release 1
end

object pit_white-pink-c-marble_114_open
anim
hole_white-any-c-marble.x14
hole_white-any-c-marble.x14
hole_white-any-c-marble.x14
hole_white-any-c-marble.x14
hole_white-any-c-marble.x14
hole_white-any-c-marble.x14
hole_white-any-c-marble.x14
hole_white-any-c-marble.x14
hole_white-any-c-marble.x14
white-pink-c-marble.x14
mina
name pit
face hole_white-any-c-marble.x14
is_animated 0
maxsp 1
wc 0
type 94
move_on walk
no_pick 1
is_floor 1
activate_on_push 1
activate_on_release 1
end

