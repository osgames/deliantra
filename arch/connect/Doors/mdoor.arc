object mdoor_closed_1
anim
wdoor.x11
wdoor.x12
wdoor.x13
wdoor.x14
mina
name wood door
face wdoor.x14
is_animated 0
maxsp 0
dam 6
wc 3
type 91
move_block all
no_pick 1
blocksview 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

object mdoor_closed_2
anim
sdoor.x11
sdoor.x12
sdoor.x13
sdoor.x14
mina
name stone door
face sdoor.x14
is_animated 0
maxsp 0
dam 6
wc 3
type 91
move_block all
no_pick 1
blocksview 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

object mdoor_open_1
anim
wdoor.x11
wdoor.x12
wdoor.x13
wdoor.x14
mina
name wood door
face wdoor.x11
is_animated 0
maxsp 1
dam 6
wc 0
type 91
no_pick 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

object mdoor_open_2
anim
sdoor.x11
sdoor.x12
sdoor.x13
sdoor.x14
mina
name stone door
face sdoor.x11
is_animated 0
maxsp 1
dam 6
wc 0
type 91
no_pick 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

