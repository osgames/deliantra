object odoorn2_closed_1
anim
odoorn2.x14
odoorn2.x13
odoorn2.x12
odoorn2.x11
mina
name wood door
face odoorn2.x11
is_animated 0
maxsp 0
dam 6
wc 3
type 91
move_block all
no_pick 1
blocksview 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

object odoorn2_open_1
anim
odoorn2.x14
odoorn2.x13
odoorn2.x12
odoorn2.x11
mina
name wood door
face odoorn2.x14
is_animated 0
maxsp 1
dam 6
wc 0
type 91
no_pick 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

