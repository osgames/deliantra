object westcwall_closed_1
anim
westcwallsec.x11
westcwallsec.x12
westcwallsec.x13
westcwallsec.x14
westcwallsec.x15
westcwallsec.x16
westcwallsec.x17
westcwallsec.x18
mina
name wall
face westcwallsec.x18
is_animated 0
maxsp 0
dam 6
wc 7
type 91
move_block all
no_pick 1
blocksview 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

object westcwall_open_1
anim
westcwallsec.x11
westcwallsec.x12
westcwallsec.x13
westcwallsec.x14
westcwallsec.x15
westcwallsec.x16
westcwallsec.x17
westcwallsec.x18
mina
name wall
face westcwallsec.x11
is_animated 0
maxsp 1
dam 6
wc 0
type 91
no_pick 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

