object stwall_closed_1
anim
stwallsec.x11
stwallsec.x12
stwallsec.x13
stwallsec.x14
stwallsec.x15
stwallsec.x16
stwallsec.x17
stwallsec.x18
mina
name stone wall
face stwallsec.x18
is_animated 0
maxsp 0
dam 6
wc 7
type 91
move_block all
no_pick 1
blocksview 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

object stwall_open_1
anim
stwallsec.x11
stwallsec.x12
stwallsec.x13
stwallsec.x14
stwallsec.x15
stwallsec.x16
stwallsec.x17
stwallsec.x18
mina
name stone wall
face stwallsec.x11
is_animated 0
maxsp 1
dam 6
wc 0
type 91
no_pick 1
no_magic 1
damned 1
activate_on_push 1
activate_on_release 1
end

