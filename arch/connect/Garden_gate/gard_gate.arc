object ggate_closed
anim
gard_gate.x14
gard_gate.x13
gard_gate.x12
gard_gate.x11
mina
name garden gate
face gard_gate.x11
is_animated 0
hp 0
maxsp 0
dam 0
wc 3
ac 1
type 91
move_block all
no_pick 1
activate_on_push 1
activate_on_release 1
end

object ggate_open
anim
gard_gate.x14
gard_gate.x13
gard_gate.x12
gard_gate.x11
mina
name garden gate
face gard_gate.x14
is_animated 0
hp 0
maxsp 1
dam 0
wc 0
ac 1
type 91
no_pick 1
activate_on_push 1
activate_on_release 1
end

