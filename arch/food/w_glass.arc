object w_glass
name glass of wine
name_pl glasses of wine
slaying vial_poison:w_glass_poison
face w_glass.x11
food 10
nrof 1
type 54
materialname glass
value 2
weight 1000
client_type 611
identified 1
on_use_yield w_glass_empty
end

