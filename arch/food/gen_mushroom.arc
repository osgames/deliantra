object gen_mushroom
name mouldy patch
face gen_mushroom.x11
hp 1
maxhp 1
exp 1
ac 25
speed -1.0
level 1
invisible 1
randomitems random_mushroom
alive 1
no_pick 1
is_used_up 1
end

