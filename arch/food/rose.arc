object rose_black
name black rose
name_pl black roses
face rose_black.x11
food 5
nrof 1
type 6
materialname organic
value 160
weight 2
client_type 601
end

object rose_pink
name pink rose
name_pl pink roses
face rose_pink.x11
food 5
nrof 1
type 6
materialname organic
value 60
weight 2
client_type 601
end

object rose_red
name red rose
name_pl red roses
face rose_red.x11
food 5
nrof 1
type 6
materialname organic
value 80
weight 2
client_type 601
end

object rose_white
name white rose
name_pl white roses
face rose_wh.x11
food 5
nrof 1
type 6
materialname organic
value 45
weight 2
client_type 601
end

object rose_yellow
name yellow rose
name_pl yellow roses
face rose_yel.x11
food 5
nrof 1
type 6
materialname organic
value 45
weight 2
client_type 601
end

