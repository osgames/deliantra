object poison
name booze
name_pl boozes
title of poison
face booze.x11
nrof 1
type 7
materialname organic
weight 6500
client_type 611
identified 0
cursed 1
known_cursed 0
on_use_yield boozebottle_empty
end

object w_glass_poison
name glass of wine
name_pl glasses of wine
title of poison
face w_glass.x11
food 10
nrof 1
type 7
materialname glass
value 2
weight 1000
client_type 611
identified 0
cursed 1
known_cursed 0
end

object water_poison
name water
name_pl waters
title of poison
face water.x11
food 5
nrof 1
type 7
materialname organic
value 5
weight 1500
client_type 611
identified 0
cursed 1
known_cursed 0
on_use_yield wbottle_empty
end

object wine_poison
name bottle of wine
name_pl bottles of wine
title of poison
face wine.x11
food 75
nrof 1
type 7
materialname glass
value 10
weight 1000
client_type 611
identified 0
cursed 1
known_cursed 0
on_use_yield winebottle_empty
end

