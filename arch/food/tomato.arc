object tomato
name tomato
name_pl tomatoes
face tomato.x11
food 50
nrof 1
type 6
materialname organic
value 7
weight 1000
client_type 601
identified 1
end

object tomato_big
name very large tomato
name_pl very large tomatoes
face tomato_big.x11
food 200
nrof 1
type 6
materialname organic
value 14
weight 1000
client_type 601
identified 1
end

