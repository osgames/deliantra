object chocolate
name_pl chocolates
msg
Hmmm... tasty chocolate!
endmsg
face chocolate.x11
food 50
nrof 1
type 6
materialname organic
value 20
weight 100
client_type 601
identified 1
end

object chocolate_w
name white chocolate
name_pl white chocolate pieces
msg
Unlike the normal brown chocolate, white chocolate
is made mostly out of butter and milk, giving it a very creamy
taste.

This is a special white chocolate for special occasions, and has a sizable
amount of almonds added to it, making it much more expensive than other
chocolates.
endmsg
face chocolate_w.x11
food 75
nrof 1
type 6
materialname organic
value 80
weight 100
client_type 601
identified 1
end

