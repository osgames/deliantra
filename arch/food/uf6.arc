object uf6
name uranium hexafluoride gas
name_pl uranium hexafluoride gas
face uf6.x11
food 1
nrof 1
type 54
materialname organic
value 155
weight 11500
client_type 611
on_use_yield wbottle_empty
end

