object apple_eighth
name apple eighth
name_pl apple eighths
face apple_eighth.x11
food 3
nrof 1
type 6
materialname organic
value 1
weight 12
client_type 601
identified 1
need_an 1
end

object apple_fourth
name apple fourth
name_pl apple fourths
slaying b_slicingknife:2 apple_eighth
face apple_fourth.x11
food 6
nrof 1
type 6
materialname organic
value 1
weight 25
client_type 601
identified 1
need_an 1
end

object apple_half
name apple halve
name_pl apple halves
slaying b_slicingknife:2 apple_fourth
face apple_half.x11
food 12
nrof 1
type 6
materialname organic
value 1
weight 50
client_type 601
identified 1
need_an 1
end

