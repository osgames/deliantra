object volcano_high
name high volcano
face volcano_hi.x11
type 66
client_type 25012
move_slow walk
move_slow_penalty 20
no_pick 1
blocksview 1
end
more
object volcano_high_2
name high volcano
face volcano_hi.x11
x 1
type 66
move_slow walk
move_slow_penalty 20
no_pick 1
blocksview 1
end
more
object volcano_high_3
name high volcano
face volcano_hi.x11
y 1
type 66
move_slow walk
move_slow_penalty 20
no_pick 1
blocksview 1
end
more
object volcano_high_4
name high volcano
face volcano_hi.x11
x 1
y 1
type 66
move_slow walk
move_slow_penalty 20
no_pick 1
blocksview 1
end

