object teleporter_connect
face teleporter.x11
type 41
no_pick 1
activate_on_push 1
activate_on_release 1
editor_folder connect
end

# should be avoided
object teleporter
inherit teleporter_connect
anim
teleporter.x11
teleporter.x12
teleporter.x13
mina
speed 0.1
end

