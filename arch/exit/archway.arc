object archway
face archwood.x11
type 66
client_type 25012
no_pick 1
end

object archway_gold
inherit archway
name golden archway
face archgold.x11
end

object archway_green
inherit archway
name green archway
face archgreen.x11
end

object archway_tree
inherit archway
name tree forming an arc
face archtree.x11
end

