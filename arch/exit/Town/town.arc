object town_1
name village
face town_1.x11
type 66
client_type 25012
no_pick 1
end

object town_2
name small town
face town_2.x11
type 66
client_type 25012
no_pick 1
end
more
object town_2.2
name small town
face town_2.x11
x 1
type 66
no_pick 1
end

object town_3
name small town
face town_3.x11
type 66
client_type 25012
no_pick 1
end
more
object town_3.2
name small town
face town_3.x11
y 1
type 66
no_pick 1
end

object town_4
name medium town
face town_4.x11
type 66
client_type 25012
no_pick 1
end
more
object town_4.2
name medium town
face town_4.x11
x 1
type 66
no_pick 1
end
more
object town_4.3
name medium town
face town_4.x11
y 1
type 66
no_pick 1
end
more
object town_4.4
name medium town
face town_4.x11
x 1
y 1
type 66
no_pick 1
end

object town_5
name city
face town_5.x11
type 66
client_type 25012
no_pick 1
end
more
object town_5.2
name city
face town_5.x11
x 1
type 66
no_pick 1
end
more
object town_5.3
name city
face town_5.x11
x 2
type 66
no_pick 1
end
more
object town_5.4
name city
face town_5.x11
y 1
type 66
no_pick 1
end
more
object town_5.5
name city
face town_5.x11
x 1
y 1
type 66
no_pick 1
end
more
object town_5.6
name city
face town_5.x11
x 2
y 1
type 66
no_pick 1
end

object town_6
name city
face town_6.x11
type 66
client_type 25012
no_pick 1
end
more
object town_6.2
name city
face town_6.x11
x 1
type 66
no_pick 1
end
more
object town_6.3
name city
face town_6.x11
y 1
type 66
no_pick 1
end
more
object town_6.4
name city
face town_6.x11
x 1
y 1
type 66
no_pick 1
end
more
object town_6.5
name city
face town_6.x11
y 2
type 66
no_pick 1
end
more
object town_6.6
name city
face town_6.x11
x 1
y 2
type 66
no_pick 1
end

object town_7
name large city
face town_7.x11
type 66
client_type 25012
no_pick 1
end
more
object town_7.2
name large city
face town_7.x11
x 1
type 66
no_pick 1
end
more
object town_7.3
name large city
face town_7.x11
x 2
type 66
no_pick 1
end
more
object town_7.4
name large city
face town_7.x11
y 1
type 66
no_pick 1
end
more
object town_7.5
name large city
face town_7.x11
x 1
y 1
type 66
no_pick 1
end
more
object town_7.6
name large city
face town_7.x11
x 2
y 1
type 66
no_pick 1
end
more
object town_7.7
name large city
face town_7.x11
y 2
type 66
no_pick 1
end
more
object town_7.8
name large city
face town_7.x11
x 1
y 2
type 66
no_pick 1
end
more
object town_7.9
name large city
face town_7.x11
x 2
y 2
type 66
no_pick 1
end

