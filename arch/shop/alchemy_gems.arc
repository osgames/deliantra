object alchemy_diamond_converter
anim
lava.x11
lava.x12
lava.x13
lava.x14
lava.x15
polymorph.x11
lava.x14
polymorph.x12
polymorph.x13
polymorph.x14
gem.x11
mina
name alchemy bath: create diamond
slaying goldcoin
other_arch gem
face lava.x11
food 40
speed 0.4
type 103
move_on walk
no_pick 1
end

object alchemy_pearl_converter
anim
lava.x11
lava.x12
lava.x13
lava.x14
lava.x15
polymorph.x11
lava.x14
polymorph.x12
polymorph.x13
polymorph.x14
pearl.x11
mina
name alchemy bath: create pearl
slaying goldcoin
other_arch pearl
face lava.x11
food 5
speed 0.4
type 103
move_on walk
no_pick 1
end

object alchemy_ruby_converter
anim
lava.x11
lava.x12
lava.x13
lava.x14
lava.x15
polymorph.x11
lava.x14
polymorph.x12
polymorph.x13
polymorph.x14
ruby.x11
mina
name alchemy bath: create ruby
slaying goldcoin
other_arch ruby
face lava.x11
food 20
speed 0.4
type 103
move_on walk
no_pick 1
end

