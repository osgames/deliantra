object diamond_converter
name drop gold to buy diamonds
slaying goldcoin
other_arch gem
face conv.x11
food 40
type 103
move_on walk
no_pick 1
end

object gold_converter
name convert gold into platinum
slaying goldcoin
other_arch platinacoin
face conv.x11
food 100
type 103
move_on walk
no_pick 1
end

# DON'T USE THIS OBJECT YET
# it needs some server code which is under discussion
# a character having amberium or jade coins without the server code in
# place would be able to simply walk out of shops with stuff, without
# actually paying anything.
object jade_converter
name convert jade into amberium
slaying jadecoin
other_arch ambercoin
face conv.x11
food 100
type 103
move_on walk
no_pick 1
editor_folder deprecated/shop
end

object pearl_converter
name drop gold to buy pearls
slaying goldcoin
other_arch pearl
face conv.x11
food 5
type 103
move_on walk
no_pick 1
end

object platinum_converter
name convert platinum into gold
slaying platinacoin
other_arch goldcoin
face conv.x11
food 1
type 103
move_on walk
no_pick 1
end

# DON'T USE THIS OBJECT YET
# it needs some server code which is under discussion
# a character having amberium or jade coins without the server code in
# place would be able to simply walk out of shops with stuff, without
# actually paying anything.
object platinum_converter2
name convert platinum into jade
slaying platinacoin
other_arch jadecoin
face conv.x11
food 100
type 103
move_on walk
no_pick 1
editor_folder deprecated/shop
end

object platinum_converter3
name convert platinum into royalties
slaying platinacoin
other_arch royalty
face conv.x11
food 100
type 103
move_on walk
no_pick 1
end

object ruby_converter
name drop gold to buy rubies
slaying goldcoin
other_arch ruby
face conv.x11
food 20
type 103
move_on walk
no_pick 1
end

object silver_converter
name convert silver into gold
slaying silvercoin
other_arch goldcoin
face conv.x11
food 100
type 103
move_on walk
no_pick 1
end

