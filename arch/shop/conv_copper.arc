object coppersilver_converter
name convert silver into copper
slaying silvercoin
other_arch coppercoin
face conv.x11
sp 10
food 1
type 103
move_on walk
no_pick 1
end

object silvercopper_converter
name convert copper into silver
slaying coppercoin
other_arch silvercoin
face conv.x11
sp 1
food 10
type 103
move_on walk
no_pick 1
end

