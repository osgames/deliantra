object shop_weapon
name tiles
face shop_weapo.x11
type 68
randomitems random_weapon
no_pick 1
auto_apply 1
no_magic 1
is_floor 1
damned 1
end

object shop_weapon_ancient
name tiles
face shop_weapo.x11
type 68
randomitems shopweapoancient
no_pick 1
auto_apply 1
no_magic 1
is_floor 1
damned 1
end

object shop_weapon_east
name tiles
face shop_weapo.x11
type 68
randomitems shopweapoeast
no_pick 1
auto_apply 1
no_magic 1
is_floor 1
damned 1
end

object shop_weapon_west
name tiles
face shop_weapo.x11
type 68
randomitems shopweapowest
no_pick 1
auto_apply 1
no_magic 1
is_floor 1
damned 1
end

