object inactive_shop_floor
name tiles
face shop_empty.x11
smoothlevel 32
smoothface shop_empty.x11 black_border_S.x11
randomitems random_shop
no_pick 1
auto_apply 1
no_magic 1
is_floor 1
damned 1
end

object shop_floor
name tiles
inherit inactive_shop_floor
type 68
end

