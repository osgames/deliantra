object shop_ring
name tiles
face shop_ring.x11
type 68
randomitems shop_rings
no_pick 1
auto_apply 1
no_magic 1
is_floor 1
damned 1
end

object shop_ring_adornment
name tiles
attach [["ring_adornment_shop",null]]
face shop_ring.x11
type 68
no_pick 1
no_magic 1
is_floor 1
damned 1
end

