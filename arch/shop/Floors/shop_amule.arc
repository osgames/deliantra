object shop_amulets
name tiles
face shop_amule.x11
type 68
randomitems random_amulet
no_pick 1
auto_apply 1
no_magic 1
is_floor 1
damned 1
end

object shop_amulets_adornment
name tiles
attach [["amulet_adornment_shop",null]]
face shop_amule.x11
type 68
no_pick 1
no_magic 1
is_floor 1
damned 1
end

