object mountain_2
name high mountain
face mountain_2.111
move_slow walk
move_slow_penalty 20
no_pick 1
blocksview 1
end
more
object mountain_2_2
name high mountain
face mountain_2.211
x 1
move_slow walk
move_slow_penalty 20
no_pick 1
blocksview 1
end
more
object mountain_2_3
name high mountain
face mountain_2.311
y 1
move_slow walk
move_slow_penalty 20
no_pick 1
blocksview 1
end
more
object mountain_2_4
name high mountain
face mountain_2.411
x 1
y 1
move_slow walk
move_slow_penalty 20
no_pick 1
blocksview 1
end

