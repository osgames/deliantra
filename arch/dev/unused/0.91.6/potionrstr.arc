object potion_restoration
name potion of restoration
face potiongen.111
str 1
dex 1
con 1
wis 1
cha 1
int 1
nrof 1
type 5
attacktype 65536
value 8000
weight 1500
end

object potion_restore_cha
name potion of restore charisma
face restorecha.111
cha 1
nrof 1
type 5
attacktype 65536
value 4000
weight 1500
end

object potion_restore_con
name potion of restore constitution
face restorecon.111
con 1
nrof 1
type 5
attacktype 65536
value 4000
weight 1500
end

object potion_restore_dex
name potion of restore dexterity
face restoredex.111
dex 1
nrof 1
type 5
attacktype 65536
value 4000
weight 1500
end

object potion_restore_int
name potion of restore intelligence
face restoreint.111
int 1
nrof 1
type 5
attacktype 65536
value 4000
weight 1500
end

object potion_restore_str
name potion of restore strength
face restorestr.111
str 1
nrof 1
type 5
attacktype 65536
value 4000
weight 1500
end

object potion_restore_wis
name potion of restore wisdom
face restorewis.111
wis 1
nrof 1
type 5
attacktype 65536
value 4000
weight 1500
end

