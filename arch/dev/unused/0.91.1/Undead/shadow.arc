object shadow
anim
shadow.111
shadow.112
shadow.113
mina
race shadow
face shadow.111
con 12
wis 14
hp 250
maxhp 250
exp 400
dam 8
wc 9
ac 0
speed 0.25
level 9
attacktype 1
resist_fire 100
resist_electricity 30
resist_cold 30
resist_slow 100
resist_paralyze 100
resist_fear 100
weight 1000
pick_up 24
will_apply 2
alive 1
no_pick 1
monster 1
undead 1
can_use_scroll 1
can_use_bow 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_wand 1
end

