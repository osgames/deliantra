object werewolf
anim
werewolf.111
werewolf.112
werewolf.111
werewolf.113
mina
race lycantherope
face werewolf.111
con 10
wis 10
hp 50
maxhp 50
exp 60
dam 8
wc 14
ac 5
speed 0.10
level 7
attacktype 3
resist_physical 30
weight 80000
randomitems standard
run_away 15
pick_up 64
alive 1
monster 1
undead 1
can_use_weapon 1
end

