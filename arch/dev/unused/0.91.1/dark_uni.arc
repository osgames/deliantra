object dark_uni
anim
dark_uni.111
dark_uni.111
dark_uni.112
dark_uni.112
mina
name dark unicorn
race unicorn
face dark_uni.111
con 8
wis 18
int 15
hp 800
maxhp 800
sp 50
maxsp 50
exp 3500
dam 20
wc 0
ac 0
speed 0.77
level 6
attacktype 1
resist_magic 100
resist_acid 100
resist_poison 100
resist_paralyze 100
resist_fear 100
weight 800000
randomitems unicorn
run_away 5
alive 1
no_pick 1
monster 1
see_invisible 1
unaggressive 1
can_cast_spell 1
sleep 1
end
more
object dark_uni_2
anim
dark_uni.211
dark_uni.211
dark_uni.212
dark_uni.212
mina
name dark unicorn
face dark_uni.211
x 1
weight 800000
alive 1
no_pick 1
monster 1
end

