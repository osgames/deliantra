object grave_close
name the empty grave
slaying grave
msg
You need the shovel to dig the grave.
endmsg
face grave_clos.111
type 20
move_block all
no_pick 1
end

object grave_open
name the open grave
face grave_open.111
no_pick 1
end

object shovel_2
anim
shovel_1.111
shovel_1.112
shovel_1.113
shovel_1.114
shovel_1.115
mina
name shovel
slaying grave
face shovel_1.111
speed 0.1
type 21
materialname iron
value 50
weight 30000
end

