object mountain_1
name high mountain
face mountain_1.111
move_slow walk
move_slow_penalty 20
no_pick 1
blocksview 1
end
more
object mountain_1_2
name high mountain
face mountain_1.211
x 1
move_slow walk
move_slow_penalty 20
no_pick 1
blocksview 1
end
more
object mountain_1_3
name high mountain
face mountain_1.311
y 1
move_slow walk
move_slow_penalty 20
no_pick 1
blocksview 1
end
more
object mountain_1_4
name high mountain
face mountain_1.411
x 1
y 1
move_slow walk
move_slow_penalty 20
no_pick 1
blocksview 1
end

