object centaur
anim
centaur.111
centaur.112
centaur.113
mina
name centaur
face centaur.111
str 17
dex 19
con 18
int 10
hp 500
maxhp 500
sp 20
maxsp 20
exp 1000
dam 16
wc 1
ac 0
speed 0.75
level 5
weight 50000
pick_up 24
will_apply 2
alive 1
monster 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_wand 1
end

