object monolith
name a strange black monolith
face monolith.111
type 66
move_block all
no_pick 1
end
more
object monolith_b
anim
monolith.212
monolith.213
monolith.214
monolith.215
monolith.216
monolith.215
monolith.214
monolith.213
mina
name a strange black monolith
face monolith.212
y 1
speed .2
type 66
no_pick 1
end

object monolith_closed
name a strange black monolith
face monolith.111
type 66
move_block all
no_pick 1
end
more
object monolith_cl_b
name a strange black monolith
face monolith.211
y 1
type 66
move_block all
no_pick 1
end

