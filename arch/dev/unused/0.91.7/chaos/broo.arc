object broo
anim
broo.111
broo.112
broo.113
broo.112
mina
face broo.111
con 5
wis 10
hp 50
maxhp 50
exp 100
dam 8
wc 3
ac 3
speed 0.16
level 6
attacktype 1025
resist_magic 30
weight 75000
pick_up 50
will_apply 2
alive 1
no_pick 1
monster 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_wand 1
end

