object c_knight
anim
c_knight.111
c_knight.112
mina
name Chaos Knight
face c_knight.111
con 8
wis 10
hp 200
maxhp 200
exp 100
dam 12
wc 1
ac 7
speed 0.15
level 12
attacktype 263169
resist_magic 30
weight 75000
randomitems standard
pick_up 60
will_apply 2
alive 1
no_pick 1
monster 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_wand 1
end

