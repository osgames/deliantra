object child
name child
face child.111
int 1
hp 10
maxhp 10
sp 10
maxsp 10
exp 15
dam 1
wc 10
ac 7
speed 0.15
level 1
weight 50000
pick_up 50
will_apply 50
alive 1
monster 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_ring 1
can_use_rod 1
can_use_wand 1
random_movement 1
end

