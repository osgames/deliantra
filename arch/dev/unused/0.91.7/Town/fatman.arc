object fatman
name Large man
face fatman.111
int 1
hp 10
maxhp 10
sp 10
maxsp 10
exp 20
dam 2
wc 10
ac 8
speed 0.05
level 1
weight 50000
pick_up 28
will_apply 2
alive 1
monster 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_rod 1
can_use_wand 1
random_movement 1
end

