object merchant
anim
merchant.171
merchant.131
mina
name Merchant
face merchant.111
int 1
hp 10
maxhp 10
sp 10
maxsp 10
exp 20
dam 2
wc 10
ac 6
speed 0.10
level 3
weight 50000
randomitems standard
pick_up 24
will_apply 8
alive 1
monster 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_rod 1
can_use_wand 1
is_turning 1
end

