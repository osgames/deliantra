object royalty
name_pl royalties
race gold and jewels
skill literacy
msg
This royal bank note shows His Majesty, the King of Scorn, but rumor has it the image was enchanted so that everybody accepted it. One royalty is worth 100 platinum.
endmsg
face royalty.x11
nrof 1
level 1
type 36
materialname paper
value 1000000
weight 1
client_type 2001
end

