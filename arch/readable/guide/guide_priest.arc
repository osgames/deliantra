object guide_priest
inherit type_book
name guide to Faith
msg
o If you plan to utilize divine powers you
  must first choose a cult. Enter a temple
  of the cult you want to join. Step onto
  the altar and pray (type "ready_skill
  praying" and shift+a direction key).
  The divine spirit will touch
  your soul, attuning your mortal being
  to your god.
o Next you must learn prayers. Prayer books
  can be bought in magic shops. Some prayers
  will have different effects for every cult.
  Try to find "holy word" and "cause light wounds".
o Casting divine spells (= prayers) will drain
  grace. When it runs low, pray to renew your
  faith. If your maximum grace is too low
  you should try to increase your Wisdom or Power
  attribute.
o You can change cults anytime, but beware:
  Your former deity will become very angry and
  punish you! You might lose prayer levels.
o Return to the temple of your cult regularly.
  Step onto the altar and pray. This will
  please your deity... And rest assured, the
  gods do know how to reward a worthy disciple!
o Never lose your faith! Rumors tell about
  divine spells of incredible power, and of
  old, wise priests that have almost reached
  immortality...
endmsg
face book_red.x11
nrof 1
subtype 2
weight 4000
end

