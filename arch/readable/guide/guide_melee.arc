object guide_melee
inherit type_book
name guide to Close Combat
msg
o To become a mighty warrior, you need
  basically three things: A good weapon,
  good armour and physical toughness.
  Then you can kill your foes simply by
  running into them.
o Physical toughness means you will need
  high Str (for good attacking power) and
  Con (for many hitpoints) stats. Look
  for "potions of Strength/Constitution".
  Try to get items that speed up your
  health-regeneration too.
o Weapons will be very important for you.
  Note that every weapon has certain
  "attacktypes". Most monsters are vulner-
  able to one or two attacktypes. Search
  for artifact weapons and experiment with
  different weapons on different monsters.
o You should know that there are ways to
  enchant ordinary weapons.  However, it
  tends to be very costly.
o It can be helpful to advance at least a
  very little bit in divine magic. Spells
  for healing, curing and protections can
  save your life more than once.
o Use resistance potions to level up quickly.
o Feeling strong already? Hah - You're
  a greenhorn unless you can at least
  kill a titan with one hit!
endmsg
face book_red.x11
nrof 1
weight 4000
end

