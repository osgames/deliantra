object book_empty
name empty book
name_pl empty books
skill literacy
msg
It is ruled to make writing in it easier.
endmsg
other_arch book_read
face book_empty.x11
nrof 1
type 110
materialname paper
value 50
weight 3000
container 1000
client_type 1041
end

object book_read
inherit type_book
name book
name_pl books
face book_read.x11
nrof 1
subtype 3
value 50
weight 3000
client_type 1041
end
