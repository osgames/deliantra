object igocert
inherit type_book
name imperial gold certificate
name_pl imperial gold certificates
race gold and jewels
msg
Five Gold Bar Imperial certificate 
	"Nunc Est Dispendum"
		etacifitrec lairepmI raB dloG eviF
endmsg
face imperialgoldcert5.x11
nrof 1
level 1
value 1
weight 5
client_type 1041
end

object iplcert
inherit type_book
name imperial platinum certificate
name_pl imperial platinum certificates
race gold and jewels
msg
Five Platinum Bar Imperial certificate 
	"Nunc Est Dispendum"
		etacifitrec lairepmI raB munitalP eviF
endmsg
face imperialplatcert5.x11
nrof 1
level 1
value 1
weight 5
client_type 1041
end

object isicert
inherit type_book
name imperial silver certificate
name_pl imperial silver certificates
race gold and jewels
msg
Five Silver Bar Imperial certificate 
	"Nunc Est Dispendum"
		etacifitrec lairepmI raB revliS eviF
endmsg
face imperialsilvercert5.x11
nrof 1
level 1
value 1
weight 5
client_type 1041
end

