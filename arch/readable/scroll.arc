object scroll
inherit type_book
name scroll
name_pl scrolls
race scrolls
face scrollr.x11
nrof 1
subtype 35
value 30
weight 300
client_type 1041
end

object scroll_read_empty
name empty scroll
name_pl empty scrolls
skill literacy
msg
It is ruled to make writing in it easier.
endmsg
other_arch scroll
face scrollr.x11
nrof 1
type 110
materialname paper
value 30
weight 300
container 250
client_type 1041
end

