object tome
inherit type_book
name tome
name_pl tomes
face tome.x11
nrof 1
subtype 4
value 80
weight 8000
client_type 1041
end

object tome_empty
name empty tome
name_pl empty tomes
skill literacy
msg
It is ruled to make writing in it easier.
endmsg
other_arch tome
face tome.x11
nrof 1
type 110
materialname paper
value 80
weight 8000
container 3000
client_type 1041
end

