object mailscroll
inherit type_book
name mail scroll
name_pl mail scrolls
race scrolls
face scroll.x11
nrof 1
subtype 35
value 30
weight 300
client_type 1040
end

object mailscroll_empty
name empty mail scroll
name_pl empty mail scrolls
race scrolls
skill literacy
msg
Nothing is written in it.
endmsg
other_arch mailscroll
face scroll_empty.x11
nrof 1
type 110
subtype 1
materialname paper
value 30
weight 300
container 500
client_type 1040
end

