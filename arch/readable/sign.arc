object sign
face sign.x11
type 98
subtype 38
client_type 25021
no_pick 1
activate_on_push 1
activate_on_release 1
end

object sign_board
inherit sign
name message board
face sign_board.x11
end

object signpost
inherit sign
name signpost
face signpost.x11
end

