object scroll_2
inherit type_book
name scroll
name_pl scrolls
race scrolls
face scroll_2.x11
nrof 1
subtype 33
value 34
weight 350
client_type 1041
end

object scroll_2_empty
name empty scroll
name_pl empty scrolls
skill literacy
msg
It is ruled to make writing in it easier.
endmsg
other_arch scroll_2
face scroll_2.x11
nrof 1
type 110
materialname paper
value 34
weight 350
container 250
client_type 1041
end

