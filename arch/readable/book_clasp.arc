object book_clasp
inherit type_book
name book
name_pl books
face book_clasp.x11
nrof 1
subtype 1
value 65
weight 4000
client_type 1041
end

object book_clasp_empty
name empty book
name_pl empty books
skill literacy
msg
It is ruled to make writing in it easier.
endmsg
other_arch book_clasp
face book_clasp.x11
nrof 1
type 110
materialname paper
value 65
weight 4000
container 1500
client_type 1041
end

