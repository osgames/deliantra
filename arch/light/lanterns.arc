object lantern2_1
name broken bright lantern
name_pl broken lanterns
other_arch lantern2_unlit
face lantern2.x11
food 10000
speed 0.02
materialname iron
value 0
weight 2500
glow_radius 8
client_type 1102
changing 1
is_lightable 1
end

object lantern2_unlit
name broken bright lantern
name_pl broken lanterns
other_arch lantern2_1
face lantern2_off.x11
food 10000
nrof 1
materialname iron
value 15000
weight 2500
glow_radius 0
client_type 1102
is_lightable 1
end

object lantern_1
name broken lantern
name_pl broken lanterns
other_arch lantern_unlit
face lantern.x11
food 10000
speed 0.01
materialname iron
value 0
weight 1500
glow_radius 5
client_type 1102
changing 1
is_lightable 1
end

object lantern_bright
inherit lantern_type
name bright lantern
name_pl bright lanterns
msg
This bright lantern comes with the same features as the basic lantern
model, but it comes with brighter burning fuel and a special glass, which
is more transparent than the normal lanterns. By lasting at least one
day it will provide enough illumination for adventures even into deep
dungeons.

H<One day in the game is about 1½ hours in real time long. You need flint & steel to light it up.>
endmsg
other_arch lantern_bright_on
face lantern2_off.x11
food 90
level 30
value 1000
range 5
end

object lantern_bright_dura
inherit lantern_bright
name dura bright lantern
name_pl dura bright lanterns
msg
The B<dura> line of lanterns have enhanced burning duration. This is
achieved by them having a much larger tank. It will give you enough light
for at over half a month of continuous adventuring. There is a saying that
claims: "if you can lift a dura once, you won't put it down again."

This bright dura model gives as much light as a normal bright lantern.

H<Half a month in game is around one day in real time. You can only light it up with flint & steel.>
endmsg
food 1440
level 40
value 100000
weight 3000
end

object lantern_bright_on
name don't use this arch in maps
face lantern2.x11
is_animated 0
end

object lantern_magic
inherit lantern_type
name magic lantern
name_pl magic lanterns
msg
This magical lantern comes with a special magical fuel which has been
invented by wizards in Brests, to support extensive adventuring in deep
caves. This lantern will make any dungeon bright as daylight for at least
one day.

Warning: Please avoid looking directly into the flame!

H<One day in game is about 1½ hours long. You can only light it up with flint & steel.>
endmsg
other_arch lantern_magic_on
face lantern3_off.x11
food 90
level 50
value 10000
range 7
end

object lantern_magic_dura
inherit lantern_magic
name dura magic lantern
name_pl dura magic lanterns
msg
The B<dura> line of lanterns have enhanced burning duration. This is
achieved by them having a much larger tank. It will give you enough light
for at over half a month of continuous adventuring. There is a saying that
claims: "if you can lift a dura once, you won't put it down again."

This magic dura model is a luxury that will serve even the most demanding
illumination needs of the experienced adventurer. Their brightness and
duration are unmatched!

Warning: Please don't look directly into the flame!

H<Half a month in game is around one day in real time. You can only light it up with flint & steel.>
endmsg
food 1440
level 60
value 1000000
weight 3000
end

object lantern_magic_on
name don't use this arch in maps
face lantern3.x11
is_animated 0
end

object lantern_normal
inherit lantern_type
name basic lantern
name_pl basic lanterns
msg
This lantern is the basic lantern model. Every adventurer should have one. It provides
the light of a torch but lasts at least one day, and you can light it up and
put it out as often as you like.

They last long enough to provide enough illumination for adventures even
into deep dungeons, where you would otherwise need to carry a lot of torches.

H<One day in the game is about 1½ hours in real time long. You need flint & steel to light it up.>
endmsg
other_arch lantern_normal_on
face lantern_off.x11
food 90
level 10
value 100
range 3
end

object lantern_normal_dura
inherit lantern_normal
name dura lantern
name_pl dura lanterns
msg
The B<dura> line of lanterns have enhanced burning duration. This is
achieved by them having a much larger tank. It will give you enough light
for at over half a month of continuous adventuring. There is a saying that
claims: "if you can lift a dura once, you won't put it down again."

This lantern gives as much light as your basic lantern.

H<Half a month in game is around one day in real time. You can only light it up with flint & steel.>
endmsg
food 1440
level 20
value 10000
weight 3000
end

object lantern_normal_on
name don't use this arch in maps
face lantern.x11
is_animated 0
end

object lantern_type
is_animated 0
food 90
speed 0.00208
direction 0
type 82
materialname iron
value 100
weight 1000
client_type 1102
range 3
is_lightable 1
end

object lantern_unlit
name broken lantern
name_pl broken lanterns
other_arch lantern_1
face lantern_off.x11
food 10000
nrof 1
materialname iron
value 5000
weight 1500
glow_radius 0
client_type 1102
is_lightable 1
end

