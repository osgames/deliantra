object light1
name light
face light_bulb_1.x11
invisible 1
glow_radius 1
end

object light2
name light
face light_bulb_2.x11
invisible 1
glow_radius 2
end

object light3
name light
face light_bulb_3.x11
invisible 1
glow_radius 3
end

object light4
name light
face light_bulb_4.x11
invisible 1
glow_radius 4
end

object light5
name light
face light_bulb_x.x11
invisible 1
glow_radius 5
end

object light6
name light
face light_bulb_x.x11
invisible 1
glow_radius 6
end

object light7
name light
face light_bulb_x.x11
invisible 1
glow_radius 7
end

object light8
name light
face light_bulb_x.x11
invisible 1
glow_radius 8
end

object light9
name light
face light_bulb_x.x11
invisible 1
glow_radius 9
end

