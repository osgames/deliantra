object burnt_torch
inherit torch
name burnt out torch
name_pl burnt out torches
msg
This is your standard torch, unfortunately, it is burned down. Except for
poking into other people's eyes with it, it is now next to useless.
endmsg
food 0
nrof 1
end

object dim_torch
inherit torch_pyro
name pyrophor torch
name_pl pyrophor torches
level 10
glow_radius 2
end

object torch
anim
torch_unlit.x11
mina
inherit torch_type
name torch
name_pl torches
msg
This is your standard torch: Due to being used so widely, standard torches
are cheap and reliable. They give as much light as lanterns, but are far
cheaper. This torch requires flint & steel to light it, and is good for
over three hours of adventuring. You can even extinguish them when you
don't need them, saving them up for later.

H<These torches give more light than dim torches. If you have flint &
steel, you can light and extinguish them as often as you want. They are
good enough for about 10 minutes of playing time.>
endmsg
face torch_unlit.x11
food 480
value 20
range 3
is_lightable 1
end

object torch_1
inherit torch
name torch
name_pl torches
glow_radius 3
end

object torch_pyro
inherit torch_type
name pyrophor torch
name_pl pyrophor torches
msg
This unique product will be revolutionising the torch business! Due to
remarkable technologies used to produce it, it doesn't need any special
means of lighting it, instead, you can start the fire simply by rubbing it
at a rough surface: It even lights up when you club it into the face of a
monster! No flint & steel is required!

Unfortunately, the light it gives off is not yet very strong, and once
burning, they can not be extinguished anymore. But at least it is cheap,
and it burns for over I<three hours>!

H<Dim torches are easy to use, but they are the least effective of all
available lights. They are good enough for about 10 minutes of playing
time.>
endmsg
level 10
end

object torch_type
anim
torch_pyro_unlit.x11
mina
other_arch torch_type_on
face torch_pyro_unlit.x11
food 144
speed 0.15
type 81
materialname ironwood
value 5
weight 500
client_type 1102
range 2
end

object torch_type_on
anim
torch_lit1.x11
torch_lit2.x11
mina
end

object torch_unlit
inherit torch
name torch
name_pl torches
end

