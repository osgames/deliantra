/*
 * This file is part of Deliantra clientV.
 *
 * Copyright (©) 2012 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 * Deliantra clientV is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */


/**
 * SOCKET EVENTS
 */
function onOpen(evt) {
    writeToScreen("CONNECTED");
    doSend('version {"clientver":"0.001","client":"deliantra html5 client","osver":"linux","protver":1}');
}

function onClose(evt) {
    if (userUsedBedToReality == true)
    {
        xLoginWindow.show(true);
        userUsedBedToReality = false;
    }
    else
    {
        writeToScreen("DISCONNECTED");
        if (window.confirm("Your player has been disconnected from the server improperly. \nWhen you have finished playing Deliantra, ALWAYS use a bed to reality to save your character. \nIf you are experiencing network problems, you may wish to try and reconnect as soon as possible by refreshing this page and logging in again. \nImproper disconnection may lead to a death by cave-in at worst. \n\nReload the page now?"))
        {
            xLoginWindow.show(true);
        }
    }
}
function onError(evt) {
    writeToScreen("ERROR!");
}

function onMessage2(data) {
    var patt = /[0-9a-z_]*[^0-9a-z_]/i;
    var feed = "" + data.match(patt);
    feed = feed.substr(0, feed.length - 1);
    if (feed == "nul") {
        if (data == "newmap") {
            feed = "newmap";
        } else {
            return;
        }
    }
    if ((window['feed_' + feed]) && (typeof(window['feed_' + feed]) == "function")) {
        var retVal = window['feed_' + feed](data, bytes2arr(data), data);
        if (!retVal) {}
    } else {
        //unhandled feed
    }
}

function onMessage(evt) {
    var data = evt.data;

    if (data instanceof Blob) {
        var r = new FileReader();
        r.readAsBinaryString(data);
        r.onloadend = function(progress) {
            onMessage2(r.result);
        };
    } else {
        onMessage2(data);
    }
}