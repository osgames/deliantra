/*
 * This file is part of Deliantra clientV.
 *
 * Copyright (©) 2012 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 * Deliantra clientV is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */


function onKeyDown(keyCode) {
    var dir = "";
    switch (keyCode) {

        case 37:
        case 38:
        case 39:
        case 40:
            dir = [ "northwest" , "west","northeast" ,"north","southeast" , "east","southwest" , "south"][ (keyCode - 37)*2 + (keys.alt?0:1)]
            break;
        case 97:
        case 98:
        case 99:
        case 100:
        case 102:
        case 103:
        case 104:
        case 105:
            dir = ["southwest", "south", "southeast", "west", "", "east", "northwest", "north", "northeast"][keyCode - 97]
            break;
        case 9:
            xPlayerWindow.show(!xPlayerWindow.visible());
            break;
        case 32:
            doSend("command apply");
            break;
        case 188:
            doSend("command get");
            break;
        case 190:
            doSend("command " + lastCommand);
            break;
        case 16:
            keys.shift = 1;
            break;
        case 18:
            keys.alt = 1;
            break;
        case 17:
            keys.ctrl = 1;
            break;
        case 113:
            showPlayerTab({},
                "statistics");
            break;
        case 114:
            showPlayerTab({},
                "skills");
            break;

        case 115:
            showPlayerTab({},
                "spellbook");
            break;

        case 116:
            showPlayerTab({},
                "inventory");
            break;
        default:
            //messageBox(keyCode);
            break;
    }
    if (dir.length) {
        if (keys.ctrl && !isRunning) {
            isRunning = true;
            doSend("command run " + compass_points2integer[dir]);
        }
        else if (!isFiring && keys.shift) {
            doSend("command fire " + compass_points2integer[dir]);
            isFiring = true;
        } else doSend("command " + dir);
    }
}

function onKeyUp(keyCode) {
    switch (keyCode) {
        case 38:
            if (isRunning && !keys.ctrl) {
                doSend("command run_stop");
                isRunning = false;
            }
            if (isFiring && !keys.shift) {
                doSend("command fire_stop");
                isFiring = false;
            }
            break;
        case 40:
            if (isFiring && !keys.shift) {
                doSend("command fire_stop");
                isFiring = false;
            }
            if (isRunning && !keys.ctrl) {
                doSend("command run_stop");
                isRunning = false;
            }
            break;
        case 37:
            if (isFiring && !keys.shift) {
                doSend("command fire_stop");
                isFiring = false;
            }
            if (isRunning && !keys.ctrl) {
                doSend("command run_stop");
                isRunning = false;
            }
            break;
        case 39:
            if (isFiring && !keys.shift) {
                doSend("command fire_stop");
                isFiring = false;
            }
            if (isRunning && !keys.ctrl) {
                doSend("command run_stop");
                isRunning = false;
            }
            break;
        case 49:
            break;
        case 16:
            keys.shift = 0;
            if (isFiring && !keys.shift) {
                doSend("command fire_stop");
                isFiring = false;
            }
            break;
        case 18:
            keys.alt = 0;
            break;
        case 17:
            keys.ctrl = 0;
            if (isRunning && !keys.ctrl) {
                doSend("command run_stop");
                isRunning = false;
            }
            break;
        default:
            //messageBox(e.keyCode);
            break;
    }
    if ((keyCode >= "A".charCodeAt(0)) && (keyCode <= "Z".charCodeAt(0))) {
        autoComplete(keyCode); //launch command console
    }
}