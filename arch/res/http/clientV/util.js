/*
 * This file is part of Deliantra.
 *
 * Deliantra is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */


var deliantra_util = {
   // these are not supposed to be fast, but short
   // why oh why do I have to reinvent a whole stdlib for this language
   utf8_encode: function (str) {
       return unescape (encodeURIComponent (str));
   },
   utf8_decode: function (str) {
       return decodeURIComponent (escape (str));
   },
   hex2bin: function (str) {
       return unescape (str.replace (/(..)/g, "%$1"));
   },
//   bin2hex: function (str) {
//       str = str.split ("");
//       for (var i in str)
//           str [i] = str [i].charCodeAt (0).toString (16).replace (/^(.)$/, "0$1");
//       return str.join ("");
//   },

   // the hash used by deliantra
   dgst: function (data) {
       // apparently, TEXT input means each unicode codepoint is one octet
       var h = new window.jsSHA (data, "TEXT");
       return this.hex2bin (h.getHash ("SHA-512", "HEX"));
   },

   // hashes a password - input unicode text, output binary
   hash_pw: function (pass) {
       pass = this.utf8_encode (pass).substr (0, 512);
       var hash = this.dgst (pass);

       for (var iteration = 0; iteration < 499; ++iteration) {
           hash = hash.split ("");
           for (var i in hash)
               hash [i] = String.fromCharCode (hash [i].charCodeAt (0) ^ pass.charCodeAt (i)); // assumes NaN ^ x == x

           hash = this.dgst (hash.join (""));
       }

       return hash;
   },

   // the HMAC used by deliantra
   auth_pw: function (hash, nonce1, nonce2) {
       return this.dgst (nonce1 + this.dgst (nonce2 + hash));
   },
};

String.prototype.each = function(f, p) {
    for (var i = 0; i < this.length; ++i) {
        f.apply(this, [this[i], i, p]);
    }
};

Array.prototype.each = function(f, p) {
    for (var i = 0; i < this.length; ++i) {
        f.apply(this, [this[i], i, p]);
    }
};

Array.prototype.contains = function(e) {
    return this.indexOf(e) >= 0 && this.indexOf(e) < this.length;
}

function Buffer(dataArray) {
    this.buffer = dataArray;
    this.position = 0;
    this.length = dataArray.length;
    return this;
}

Buffer.prototype.unpack_w = function() {
    var num = 0;
    var byte = 0;
    do {
        num *= 128;
        byte = parseInt(this.buffer[this.position++]);
        num += byte & 0x7f;
    } while (byte & 0x80);
    return num;
};

Buffer.prototype.readUint32 = function() {
    var ret = 0;
    for (var i = 0; i < 4; ++i) {
        ret *= 256;
        ret += this.buffer[this.position++];
    }
    return ret;
};

Buffer.prototype.readUint8 = function() {
    var ret = 0;
    for (var i = 0; i < 1; ++i) {
        ret *= 256;
        ret += parseInt(this.buffer[this.position++]);
    }
    return ret;
};

Buffer.prototype.readUint16 = function() {
    var ret = 0;
    for (var i = 0; i < 2; ++i) {
        ret *= 256;
        ret += this.buffer[this.position++];
    }
    return ret;
};

Buffer.prototype.readString = function(len) {
    var ret = "";

    if (typeof(len) != "undefined") {
        for (var i = 0; i < len; ++i) {
            ret += String.fromCharCode(this.buffer[this.position++]);
        }
    } else {

        while (this.buffer[this.position] != 0) {
            ret += String.fromCharCode(this.buffer[this.position++]);
        }
    }

    return ret;
};

function unselectable(target) {
    target.setAttribute('unselectable', 'on');
    if (typeof target.onselectstart != "undefined") //IE route
        target.onselectstart = function() {
            return false
        }
    else if (typeof target.style.MozUserSelect != "undefined") //Firefox route
        target.style.MozUserSelect = "none"
    else //All other route (ie: Opera)
        target.onmousedown = function() {
            return false
        }
}

function ptInRect(pt, rect) {
    if (pt.x < rect.x) return false;
    if (pt.y < rect.y) return false;
    if (pt.x > rect.x + rect.width) return false;
    if (pt.y > rect.y + rect.height) return false;
    return true;
}

function appendToBody(e) {
    document.getElementsByTagName("body")[0].appendChild(e);
}

function $_(p, o) {
    if (document.getElementById(p) != null) {
        return document.getElementById(p);
    } else {
        var ret = document.createElement(p);
        unselectable(ret);
        if (o && o.css) {
            var s = "";
            for (var s in o.css) {
                if (typeof(o.css[s]) == typeof("") || typeof(o.css[s]) == typeof(1)) {
                    ret.style[s] = o.css[s];
                }
            }
        }
        return ret;
    }
}


function decode_base64toArray(s) {
    var e = {},
        i, k, v = [],
        r = [],
        w = String.fromCharCode;
    var n = [
        [65, 91],
        [97, 123],
        [48, 58],
        [43, 44],
        [47, 48]];

    for (z in n) {
        for (i = n[z][0]; i < n[z][1]; i++) {
            v.push(w(i));
        }
    }
    for (i = 0; i < 64; i++) {
        e[v[i]] = i;
    }

    for (i = 0; i < s.length; i += 72) {
        var b = 0,
            c, x, l = 0,
            o = s.substring(i, i + 72);
        for (x = 0; x < o.length; x++) {
            c = e[o.charAt(x)];
            b = (b << 6) + c;
            l += 6;
            while (l >= 8) {
                r.push((b >>> (l -= 8)) % 256);
            }
        }
    }
    return r;
}

function writeToScreen ( message ) {
    channels_addMessage("debug", message + "\n");
}

function arr2bytes(arr) {
    var s = "";
    for (var i = 0; i < arr.length; ++i) {
        s += String.fromCharCode(arr[i]);
    }
    return s;
}
function bytes2arr(s) {
    var arr = [];
    for (var i = 0; i < s.length; ++i) {
        arr.push(s.charCodeAt(i));
    }
    return arr;
}

function imgFromString(s) {
    var img = $_('img');
    img.src = s;
    return img;
}

