/*
 * This file is part of Deliantra clientV.
 *
 * Copyright (©) 2012 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 * Deliantra clientV is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

function drawMap() {

    ctx.clearRect(0, 0, stage.stageWidth, stage.stageHeight);
    ctx.fillStyle = "#000";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    for (var i = 0; i < mapsize.height; ++i) {
        var cell = map_get_cell_ex(0, i);
        var y = i << 6;
        var x = 0;
        for (var j = 0; j < mapsize.width; ++j) {

            var cellx = cell.data;
            for (var z = 0; z <= 2; ++z) {
                if (!cellx.tiles[z]) continue;
                var img = tilesInfo.getImgByFaceNum(cellx.tiles[z]);
                if (img) {
                    ctx.drawImage(img, x, y);
                } else {
                    Faces.request(cellx.tiles[z], "map");
                }
            }
            if ((typeof(cellx.stat_hp) == typeof(1)) && cellx.stat_hp > 0) {
                ctx.fillStyle = "#f00";
                ctx.fillRect(x, y, ((255 - cellx.stat_hp) / 255) * 64, 8);

                //ctx.fillText(cellx.stat_hp,x,y);
            }
            cell = cell.nextCell;
            x += 64;

        }
    }
}

function clear_cell(cell) {
    cell.darkness = 256;
    cell.flags = 0;
    cell.player = 0;
    cell.tiles = [];
    return cell;
}

function MapCell() {
    this.previousCell = null;
    this.nextCell = null;
    this.data = {};
    clear_cell(this.data);
}

function MapRow() {
    this.previousRow = null;
    this.nextRow = null;
    this.firstCell = null;
    this.lastCell = null;
    this.length = 0;
    while (this.length < mapsize.width) {
        this.addCell(new MapCell());
    }
}
MapRow.prototype.addCell = function(cell) {
    if (this.firstCell == null) {
        this.firstCell = this.lastCell = cell;

        this.lastCell.nextCell = this.firstCell;
        this.lastCell.previousCell = this.firstCell;

        this.firstCell.nextCell = this.lastCell;
        this.firstCell.previousCell = this.lastCell;

        this.length = 1;
        return;
    }
    this.lastCell.nextCell = cell;
    cell.previousCell = this.lastCell;
    this.lastCell = cell;
    this.firstCell.previousCell = cell;
    cell.nextCell = this.firstCell;
    this.length += 1;
}

MapRow.prototype.shift = function() {
    var ret = this.firstCell;
    this.firstCell = ret.nextCell;
    this.lastCell.nextCell = this.firstCell;
    this.firstCell.previousCell = this.lastCell;
    return ret;
}

MapRow.prototype.pop = function() {
    var ret = this.lastCell;
    this.lastCell = ret.previousCell;
    this.lastCell.nextCell = this.firstCell;
    this.firstCell.previousCell = this.lastCell;
    return ret;
}

MapRow.prototype.push = function(cell) {
    this.addCell(cell);
}

MapRow.prototype.unshift = function(cell) {
    var tmp = this.firstCell;
    this.firstCell = cell;
    this.firstCell.nextCell = tmp;
    tmp.previousCell = this.firstCell;
    this.firstCell.previousCell = this.lastCell;
    this.lastCell.nextCell = this.firstCell;
}

function map_clear() {
    Map = {
        length: 0,
        firstRow: null,
        lastRow: null,
        addRow: function(row) {
            if (this.firstRow == null) {
                this.firstRow = this.lastRow = row;
                this.length = 1;
                return;
            }
            this.lastRow.nextRow = row;
            row.previousRow = this.lastRow;
            this.lastRow = row;
            row.nextRow = this.firstRow;
            this.firstRow.previousRow = row;
            this.length++;
        }
    };

    while (Map.length < mapsize.height) {
        Map.addRow(new MapRow());
    }

    if (stage && stage.stageWidth) ctx.clearRect(0, 0, stage.stageWidth, stage.stageHeight);
}

function map_get_cell(x, y) {
    var i = 0;
    var mapRow = Map.firstRow;
    while (i < y) {
        mapRow = mapRow.nextRow;
        ++i;
    }
    i = 0;
    var mapCell = mapRow.firstCell;
    while (i < x) {
        mapCell = mapCell.nextCell;
        ++i;
    }
    return mapCell.data;
}

function map_get_cell_ex(x, y) {
    var i = 0;
    var mapRow = Map.firstRow;
    while (i < y) {
        mapRow = mapRow.nextRow;
        ++i;
    }
    i = 0;
    var mapCell = mapRow.firstCell;
    while (i < x) {
        mapCell = mapCell.nextCell;
        ++i;
    }
    return mapCell;

}

function scroll_map(dx, dy) {
    var x, y;
    var tmpDx = dx;
    var tmpDy = dy;
    var i = 0;
    while (i < tmpDy) {

        Map.firstRow = Map.firstRow.nextRow;
        Map.addRow(new MapRow());
        Map.length -= 1;
        ++i;
    }
    i = 0;
    while (tmpDy < i) {
        var mapRow = new MapRow();
        mapRow.nextRow = Map.firstRow;
        Map.firstRow.previousRow = mapRow;
        mapRow.previousRow = Map.lastRow;
        Map.lastRow.nextRow = mapRow;
        Map.firstRow = mapRow;
        --i;
    }
    i = 0;
    while (i < tmpDx) {
        var row = Map.firstRow;
        do {
            row.shift();
            row.push(new MapCell());
            row = row.nextRow;
        } while (row != Map.firstRow);
        i++;
    }
    i = 0;
    while (tmpDx < i) {
        var row = Map.firstRow;
        do {
            row.pop();
            row.unshift(new MapCell());
            row = row.nextRow;
        } while (row != Map.firstRow);
        i--;
    }
}