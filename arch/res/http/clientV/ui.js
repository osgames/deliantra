/*
 * This file is part of Deliantra clientV.
 *
 * Copyright (©) 2012 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 * Deliantra clientV is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */



function createWindow(title, width, height, x, y) {
    var windowTemplate = document.getElementById("window_template_container").innerHTML;
    var div = $_("div");
    div.innerHTML = windowTemplate;

    var xwindow = div.getElementsByClassName("window")[0];
    xwindow.style.left = x + "px";
    xwindow.style.top = y + "px";
    xwindow.style.width = width + "px";
    xwindow.width = function() {
        return parseInt(xwindow.style.width);
    };

    xwindow.height = function() {
        return parseInt(xwindow.style.height);
    };
    xwindow.show = function(b) {
        xwindow.style.display = b ? "block" : "none";
        return true;
    };
    xwindow.visible = function(b) {
        return xwindow.style.display == "block";
    };
    xwindow.style.height = height + "px";
    xwindow.getElementsByClassName("window_top_title")[0].innerHTML = title;
    xwindow.getElementsByClassName("window_close")[0].onclick = function() {
        (xwindow.close());
    };
    xwindow.onmousedown = function(e) {
        if (e.clientY - parseInt(xwindow.style.top) < 25) {
            currentWindow = xwindow;
            currentWindowOffset = {
                x: e.clientX - parseInt(xwindow.style.left),
                y: e.clientY - parseInt(xwindow.style.top)
            }
        } else if (e.clientY - parseInt(xwindow.style.top) > parseInt(xwindow.style.height) - 5) {
            if (e.clientX - parseInt(xwindow.style.left) > parseInt(xwindow.style.width) - 5) {
                currentWindowResize = true;
                currentWindow = xwindow;
            }
        }
    };
    xwindow.contents = xwindow.getElementsByClassName("content")[0];
    xwindow.contents.parent = xwindow;
    xwindow.close = function() {
        document.getElementsByTagName("body")[0].removeChild(xwindow);
    };
    appendToBody(xwindow);
    return xwindow;
}

function createCommandWindow() {
    var div = createWindow("command", 250, 250, stage.stageWidth / 2 - 250 / 2, stage.stageHeight - 250 / 2);
    var login = document.createElement("input");
    login.type = "text";
    login.placeholder = login.id = "command";
    login.style.width = "100%";

    div.contents.appendChild(login);

    var button = document.createElement("input");
    button.type = "button";
    button.value = "execute command";
    button.style.width = "100%";
    div.contents.appendChild(button);
    button.addEventListener("click", function(e) {
        doSend("command " + login.value);
        xCommmandWindow.show(false);
        skipKeyTest = false;
    })
    var button = document.createElement("input");
    button.type = "button";
    button.value = "cancel";
    button.style.width = "100%";
    div.contents.appendChild(button);
    button.addEventListener("click", function(e) {
        xCommmandWindow.show(false);
        skipKeyTest = false;
    })
    return div;

}

function createLoginWindow() {
    var div = createWindow("login", 260, 180, stage.stageWidth / 2 - 250 / 2, stage.stageHeight / 2 - 250 / 2).contents;;
    var login = document.createElement("input");
    login.type = "text";
    login.placeholder = login.id = "login";
    login.style.width = "100%";
    login.value = "deliantra login";
    div.appendChild(login);
    var password = document.createElement("input");
    password.type = "password";
    password.value = "";
    password.style.width = "100%";
    password.placeholder = password.id = "password";
    div.appendChild(password);

    var button = document.createElement("input");
    button.type = "button";
    button.value = "logmein";
    button.style.width = "100%";
    div.appendChild(button);

    button.addEventListener("click", function(e) {
        div.parent.style.display = "none";
        xMapWindow.contents.appendChild(canvas);
        xMapWindow.show(true);
        xFloorWindow.show(true);
        xBarsWindow.show(true);
        ctx.fillStyle = "#000";

        setInterval(draw, 1000 / 5);

        websocket = new WebSocket(wsUri);
        websocket.onopen = function(evt) {
            onOpen(evt)
        };
        websocket.onclose = function(evt) {
            onClose(evt)
        };
        websocket.onmessage = function(evt) {
            onMessage(evt)
        };
        websocket.onerror = function(evt) {
            onError(evt)
        };
    })
    button = document.createElement("input");
    button.type = "button";
    button.value = "logmeinasnull";
    button.style.width = "100%";
    div.appendChild(button);
    button.addEventListener("click", function(e) {
        login.value = password.value = "null";
        div.parent.style.display = "none";
        xMapWindow.contents.appendChild(canvas);
        xMapWindow.show(true);
        xFloorWindow.show(true);
        xBarsWindow.show(true);
        ctx.fillStyle = "#000";
        ctx.font = "10px Courier New,MonoSpace";
        setInterval(draw, 1000 / 5);

        websocket = new WebSocket(wsUri);
        websocket.onopen = function(evt) {
            onOpen(evt)
        };
        websocket.onclose = function(evt) {
            onClose(evt)
        };
        websocket.onmessage = function(evt) {
            onMessage(evt)
        };
        websocket.onerror = function(evt) {
            onError(evt)
        };
    })
    button = document.createElement("input");
    button.type = "button";
    button.value = "logmeinasNULL";
    button.style.width = "100%";
    div.appendChild(button);
    button.addEventListener("click", function(e) {
        login.value = password.value = "NULL";
        div.parent.style.display = "none";
        xMapWindow.contents.appendChild(canvas);
        xMapWindow.show(true);
        xFloorWindow.show(true);
        xBarsWindow.show(true);
        ctx.fillStyle = "#000";
        ctx.font = "10px Courier New,MonoSpace";
        setInterval(draw, 1000 / 5);
        websocket = new WebSocket(wsUri);
        websocket.onopen = function(evt) {
            onOpen(evt)
        };
        websocket.onclose = function(evt) {
            onClose(evt)
        };
        websocket.onmessage = function(evt) {
            onMessage(evt)
        };
        websocket.onerror = function(evt) {
            onError(evt)
        };
    })
    button = document.createElement("input");
    button.type = "button";
    button.value = "logmeinasVOID";
    button.style.width = "100%";
    div.appendChild(button);
    button.addEventListener("click", function(e) {
        login.value = password.value = "VOID";
        div.parent.style.display = "none";
        xMapWindow.contents.appendChild(canvas);
        xMapWindow.show(true);
        xFloorWindow.show(true);
        xBarsWindow.show(true);
        ctx.fillStyle = "#000";
        ctx.font = "10px Courier New,MonoSpace";
        setInterval(draw, 1000 / 5);
        websocket = new WebSocket(wsUri);
        websocket.onopen = function(evt) {
            onOpen(evt)
        };
        websocket.onclose = function(evt) {
            onClose(evt)
        };
        websocket.onmessage = function(evt) {
            onMessage(evt)
        };
        websocket.onerror = function(evt) {
            onError(evt)
        };
    })
    return div.parent;
}

function draw() {
    xBarsWindow.ctx.fillStyle = "#000";
    xBarsWindow.ctx.fillRect(0, 0, xBarsWindow.sw, xBarsWindow.sh);

    if (typeof(myStats[CS_STAT_MAXHP]) == typeof(1)) {
        xBarsWindow.ctx.save();

        xBarsWindow.ctx.fillStyle = "#F00";
        xBarsWindow.ctx.globalAlpha = 0.5;
        xBarsWindow.ctx.fillRect(10, 10, 20, xBarsWindow.sh);
        xBarsWindow.ctx.fillRect(10, 10 + xBarsWindow.sh - (myStats[CS_STAT_HP] / myStats[CS_STAT_MAXHP]) * xBarsWindow.sh, 20, (myStats[CS_STAT_HP] / myStats[CS_STAT_MAXHP]) * xBarsWindow.sh);
        xBarsWindow.ctx.restore();
    }

    if (typeof(myStats[CS_STAT_MAXSP]) == typeof(1)) {
        xBarsWindow.ctx.save();

        xBarsWindow.ctx.fillStyle = "#00F";
        xBarsWindow.ctx.globalAlpha = 0.5;
        xBarsWindow.ctx.fillRect(50, 10, 20, xBarsWindow.sh);
        xBarsWindow.ctx.fillRect(50, 10 + xBarsWindow.sh - (myStats[CS_STAT_SP] / myStats[CS_STAT_MAXSP]) * xBarsWindow.sh, 20, (myStats[CS_STAT_SP] / myStats[CS_STAT_MAXSP]) * xBarsWindow.sh);
        xBarsWindow.ctx.restore();
    }
    if (typeof(myStats[CS_STAT_MAXGRACE]) == typeof(1)) {
        xBarsWindow.ctx.save();
        xBarsWindow.ctx.fillStyle = "#0FF";
        xBarsWindow.ctx.globalAlpha = 0.5;
        xBarsWindow.ctx.fillRect(90, 10, 20, xBarsWindow.sh);
        xBarsWindow.ctx.fillRect(90, 10 + xBarsWindow.sh - (myStats[CS_STAT_GRACE] / myStats[CS_STAT_MAXGRACE]) * xBarsWindow.sh, 20, (myStats[CS_STAT_GRACE] / myStats[CS_STAT_MAXGRACE]) * xBarsWindow.sh);
        xBarsWindow.ctx.restore();
    }

    if (typeof(myStats[CS_STAT_FOOD]) == typeof(1)) {
        xBarsWindow.ctx.save();
        xBarsWindow.ctx.fillStyle = "#ff0";
        xBarsWindow.ctx.globalAlpha = 0.5;
        xBarsWindow.ctx.fillRect(130, 10, 20, xBarsWindow.sh);
        xBarsWindow.ctx.fillRect(130, 10 + xBarsWindow.sh - (myStats[CS_STAT_FOOD] / 999) * xBarsWindow.sh, 20, (myStats[CS_STAT_FOOD] / 999) * xBarsWindow.sh);
        xBarsWindow.ctx.restore();
    }
}


function showPlayerTab(e, tn) {

    xPlayerWindow.show(true);
    var tabName = tn ? tn : (this.value).split(" ")[0].toLowerCase();

    var div;

    switch (tabName) {
        case "inventory":
            hideAllPlayerTabs();
            var content = xPlayerWindow.getElementsByClassName("player_widnow_content")[0];
            content.style.display = "block";
            content = xPlayerWindow.getElementsByClassName("floor_widnow_content")[0];
            content.style.display = "block";
            updateItemsList();
            break;
        case "spellbook":

            hideAllPlayerTabs();
            var content = xPlayerWindow.getElementsByClassName("spellbook_widnow_content")[0];
            content.style.display = "block";
            updateSpellList();
            break;
        case "skills":
            hideAllPlayerTabs();
            var content = xPlayerWindow.getElementsByClassName("skills_widnow_content")[0];
            content.style.display = "block";
            updateSkillList();
            break;;
        case "statistics":
            hideAllPlayerTabs();
            var content = xPlayerWindow.getElementsByClassName("statistics_widnow_content")[0];
            content.style.display = "block";
            updateStatsList();
            break;;
        default:

            break;
    }
}

function hideAllPlayerTabs() {
    for (var i = 0; i < playerTabs.length; ++i) {
        xPlayerWindow.getElementsByClassName(playerTabs[i])[0].style.display = "none";
    }
}

function updateStatsList() {
    var content = xPlayerWindow.getElementsByClassName("statistics_widnow_content")[0];
    var div;
    content.innerHTML = "";
    var tableLeft;
    var tableRight;
    var tr;
    var td;

    content.appendChild(div = $_("div"));
    div.innerHTML = "Primary/Secondary statistics.";
    content.appendChild($_("hr"));
    content.appendChild(tableLeft = $_("table"));
    tableLeft.style.width = "45%";
    tableLeft.style.float = "left";
    content.appendChild(tableRight = $_("table"));
    tableRight.style.width = "45%";
    tableRight.style.float = "right";
    content.appendChild(div = $_("br"));
    content.appendChild(div = $_("br"));
    content.appendChild(div = $_("div"));
    div.innerHTML = "Resistances.";
    div.style.clear = "both";
    content.appendChild($_("hr"));

    var j = 0;

    var basicStats = [CS_STAT_STR, CS_STAT_INT, CS_STAT_WIS, CS_STAT_DEX, CS_STAT_CON, CS_STAT_CHA, CS_STAT_WC, CS_STAT_AC, CS_STAT_DAM,
        /*CS_STAT_ARMOUR,*/
        CS_STAT_SPEED, CS_STAT_WEAP_SP];
    var basicStatsNames = ['CS_STAT_STR', 'CS_STAT_INT', 'CS_STAT_WIS', 'CS_STAT_DEX', 'CS_STAT_CON', 'CS_STAT_CHA', 'CS_STAT_WC', 'CS_STAT_AC', 'CS_STAT_DAM',
        /*'CS_STAT_ARMOUR',*/
        'CS_STAT_SPEED', 'CS_STAT_WEAP_SP'];

    for (i = 0; i < basicStats.length; ++i) {

        var table = i < 6 ? tableLeft : tableRight;

        table.appendChild(tr = $_("tr"));
        tr.index = i;

        tr.appendChild(td = $_("td"));
        td.innerHTML = basicStatsNames[i];

        tr.appendChild(td = $_("td"));
        td.innerHTML = myStats[basicStats[i]];

    };
    var resNames = ['CS_STAT_RES_PHYS', 'CS_STAT_RES_MAG', 'CS_STAT_RES_FIRE', 'CS_STAT_RES_ELEC', 'CS_STAT_RES_COLD', 'CS_STAT_RES_CONF', 'CS_STAT_RES_ACID', 'CS_STAT_RES_DRAIN', 'CS_STAT_RES_GHOSTHIT', 'CS_STAT_RES_POISON', 'CS_STAT_RES_SLOW', 'CS_STAT_RES_PARA', 'CS_STAT_TURN_UNDEAD', 'CS_STAT_RES_FEAR', 'CS_STAT_RES_DEPLETE', 'CS_STAT_RES_DEATH', 'CS_STAT_RES_HOLYWORD', 'CS_STAT_RES_BLIND'];

    content.appendChild(tableLeft = $_("table"));
    tableLeft.style.width = "40%";
    tableLeft.style.float = "left";
    content.appendChild(tableRight = $_("table"));
    tableRight.style.width = "40%";
    tableRight.style.float = "right";

    for (i = CS_STAT_RESIST_START; i < CS_STAT_RESIST_END; ++i) {
        var table = i % 2 == 0 ? tableLeft : tableRight;

        table.appendChild(tr = $_("tr"));
        tr.index = i;

        tr.appendChild(td = $_("td"));
        td.innerHTML = resNames[i - CS_STAT_RESIST_START];

        tr.appendChild(td = $_("td"));
        td.innerHTML = myStats[i];

    }

}


function updateItemsList() {
    var content = xPlayerWindow.getElementsByClassName("player_widnow_content")[0];

    content.innerHTML = "";
    myItems.each(function(item, i, p) {
            if (isNaN(item.nr_of)) return;
            content.appendChild(div = $_("div"));
            var img = tilesInfo.getImgByFaceNum(item.face);
            if (img) {
                div.innerHTML = "<div><input type='hidden' value='" + item.itemId + "'/> <img src='" + img.src + "' alt='' style='width:32px;'/>" + (item.nr_of > 1 ? item.namepl : item.name) + (item.nr_of > 1 ? item.nr_of : "") + "</div>"
            } else {
                div.innerHTML = "<div><input type='hidden' value='" + item.itemId + "'/>" + (item.nr_of > 1 ? item.namepl : item.name) + "</div>"
            }

            div.addEventListener("click", myInventoryItemClick);
            div.addEventListener("mouseover", inventoryItemMouseOver);
            div.addEventListener("mouseout", inventoryItemMouseOut);
            div.addEventListener("mousemove", inventoryItemMouseMove);
        },
        {})

    content = xPlayerWindow.getElementsByClassName("floor_widnow_content")[0];

    content.innerHTML = "";
    var container = secondaryContainer != 0 ? containers["container_" + secondaryContainer] : floorItems;
    container.each(function(item, i, p) {
            if (isNaN(item.nr_of)) return;
            content.appendChild(div = $_("div"));
            var img = tilesInfo.getImgByFaceNum(item.face);
            if (img) {
                div.innerHTML = "<div><input type='hidden' value='" + item.itemId + "'/> <img src='" + img.src + "' alt='' style='width:32px;'/>" + (item.nr_of > 1 ? item.namepl : item.name) + (item.nr_of > 1 ? item.nr_of : "") + "</div>"
            } else {
                div.innerHTML = "<div><input type='hidden' value='" + item.itemId + "'/>" + (item.nr_of > 1 ? item.namepl : item.name) + "</div>"
            }
            div.addEventListener("mouseover", inventoryItemMouseOver);
            div.addEventListener("mouseout", inventoryItemMouseOut);
            div.addEventListener("mousemove", inventoryItemMouseMove);
            div.addEventListener("click", floorInventoryItemClick);
        },
        {})

}

function floorInventoryItemClick(e) {
    var itemId = parseInt(this.getElementsByTagName("input")[0].value);

    if (!keys.shift) {
        return;
    }

    for (var i = 0; i < floorItems.length; ++i) {
        if (parseInt(floorItems[i].itemId) == itemId) {
            doSend("command get " + (floorItems[i].nr_of > 1 ? floorItems[i].namepl : floorItems[i].name));
        } else {

        }
    }
}

function myInventoryItemClick(e) {
    var itemId = parseInt(this.getElementsByTagName("input")[0].value);

    if (!keys.shift) {
        return;
    }
    for (var i = 0; i < myItems.length; ++i) {

        if (parseInt(myItems[i].itemId) == itemId) {
            doSend("command drop " + (myItems[i].nr_of > 1 ? myItems[i].namepl : myItems[i].name));
        } else {

        }
    }

}

function inventoryItemMouseOver(e) {
    this.style.background = "solid";
    this.style.backgroundColor = "white";
    var itemId = this.getElementsByTagName("input")[0].value;
    doSend("ex " + itemId);

    xInfoWindow.show(true);
    xInfoWindow.style.left = (e.clientX + 5) + "px";
    xInfoWindow.style.top = (e.clientY + 5) + "px";
}

function inventoryItemMouseMove(e) {

    xInfoWindow.style.left = (e.clientX + 5) + "px";
    xInfoWindow.style.top = (e.clientY + 5) + "px";

}

function inventoryItemMouseOut(e) {
    this.style.background = "transparent";
    xInfoWindow.show(false);
}

/**
 * UI EVENTS
 */


function onMouseUp(e) {

    if (currentWindow != null) {
        currentWindow = null;
        currentWindowResize = false;
    }
}

function onMouseMove(e) {
    stage.mouseX = e.clientX;
    stage.mouseY = e.clientY;
    if (currentWindow != null) {
        if (currentWindowResize == false) {
            currentWindow.style.left = (stage.mouseX - currentWindowOffset.x) + "px";
            currentWindow.style.top = (stage.mouseY - currentWindowOffset.y) + "px";
        } else {
            currentWindow.style.width = (stage.mouseX - parseInt(currentWindow.style.left)) + "px";
            currentWindow.style.height = (stage.mouseY - parseInt(currentWindow.style.top)) + "px";
        }
    }
}



function onClick(e) {
    x = e.clientX;
    y = e.clientY;

    try {
        e.preventDefault();
    } catch(ex) {

    }
    return false;
}


function updateSpellList() {
    var content = xPlayerWindow.getElementsByClassName("spellbook_widnow_content")[0];
    content.style.display = "block";

    content.innerHTML = "";
    var table;
    var tr;
    var td;

    content.appendChild(table = $_("table"));
    table.style.width = "100%";
    if (mySpells.length <= 0) {
        return;
    }
    table.appendChild(tr = $_("tr"));
    item = mySpells[0];
    for (s in item) {

        if ((typeof(item[s]) == typeof(1)) || (typeof(item[s]) == typeof(""))) {
            if (s == "tag") {

            } else if (s == "face") {
                tr.appendChild(td = $_("td"));
                td.innerHTML = s;
            } else {
                tr.appendChild(td = $_("td"));
                td.innerHTML = s;
            }
        }

    }

    mySpells.each(function(item, i, p) {
        if ( isNaN ( item [ 'face' ] ) ) {
            return false;
        }

        var img = tilesInfo.getImgByFaceNum ( item.face );

        if ( img == null ) {
            img = $_ ( "img" );
            img.src = "ajax-loader.gif";
        };

        table.appendChild ( tr = $_ ( "tr" ) );
        tr.index = i;

        var s;
        for ( s in item ) {


            if ((typeof(item[s]) == typeof(1)) || (typeof(item[s]) == typeof(""))) {
                if (s == "tag") {
                    tr.tag = item[s];
                } else if (s == "face") {
                    tr.appendChild(td = $_("td"));
                    td.innerHTML = "<img src='" + img.src + "' alt='' style='width:32px;'/>";
                } else {
                    tr.appendChild(td = $_("td"));
                    td.innerHTML = item[s];
                }
            }

        }
        tr.addEventListener ( "click", mySpellItemClick );
        tr.addEventListener ( "mouseover", SpellItemMouseOver );
        tr.addEventListener ( "mouseout", SpellItemMouseOut );
        tr.addEventListener ( "mousemove", SpellItemMouseMove );


        return false;
    } , { } );

}
function updateSkillList() {
    var content = xPlayerWindow.getElementsByClassName("skills_widnow_content")[0];

    content.innerHTML = "";
    var tableLeft;
    var tableRight;
    var tr;
    var td;

    content.appendChild(tableLeft = $_("table"));
    tableLeft.style.width = "45%";
    tableLeft.style.float = "left";
    content.appendChild(tableRight = $_("table"));
    tableRight.style.width = "45%";
    tableRight.style.float = "right";

    var j = 0;

    for (i = CS_STAT_SKILLINFO; i < CS_STAT_SKILLINFO + CS_NUM_SKILLS; ++i) {

        if (!myStats[i]) {
            continue;
        }
        var table = j % 2 == 0 ? tableLeft : tableRight;

        table.appendChild(tr = $_("tr"));
        tr.index = i;

        tr.appendChild(td = $_("td"));
        td.innerHTML = skillNames[i - CS_STAT_SKILLINFO][0];

        for (var k = 0; k < myStats[i].length; ++k) {

            tr.appendChild(td = $_("td"));
            td.innerHTML = myStats[i][k];

        }
        tr.appendChild(td = $_("td"));
        td.innerHTML = Math.floor(myStats[i][1] / expTable[parseInt(myStats[i][0])] * 100) + "%";
        /*tr.addEventListener("click",mySpellItemClick);
         tr.addEventListener("mouseover",SpellItemMouseOver);
         tr.addEventListener("mouseout",SpellItemMouseOut);
         tr.addEventListener("mousemove",SpellItemMouseMove);
         */
        ++j;
    };

}

function mySpellItemClick(e) {
    if (keys.ctrl) {
        doSend("command invoke " + mySpells[this.index].name);
    } else if (keys.shift) {
        doSend("command cast " + mySpells[this.index].name);
    }
}

function SpellItemMouseOver(e) {
    this.style.background = "solid";
    this.style.backgroundColor = "white";

    xInfoWindow.contents.innerHTML = this.message + "<small><br/>shift-click to cast<br/>ctrl-click to invoke</small>";
    xInfoWindow.show(true);
    xInfoWindow.style.left = (e.clientX + 5) + "px";
    xInfoWindow.style.top = (e.clientY + 5) + "px";
}

function SpellItemMouseMove(e) {

    xInfoWindow.style.left = (e.clientX + 5) + "px";
    xInfoWindow.style.top = (e.clientY + 5) + "px";

}

function SpellItemMouseOut(e) {
    this.style.background = "transparent";
    xInfoWindow.show(false);
}