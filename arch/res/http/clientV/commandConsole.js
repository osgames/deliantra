/*
 * This file is part of Deliantra clientV.
 *
 * Copyright (©) 2012 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 * Deliantra clientV is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */


Application = {
    skillAdded : function ( name, id, value ) {
    } ,
    spellAdded : function ( spell ) {
        commandsAll[0].push ( "invoke " + spell.name );
        commandsAll[0].push ( "cast " + spell.name );
    }

};

function commandLineOnKeyDown(e) {
    if (e.keyCode == 13) {
        skipKeyTest = false;

        doSend("command " + xCommandLines.value);
        command = "";
        lastCommand = xCommandLines.value;

        this.isShown == false;
        document.getElementsByTagName("body")[0].removeChild(xCommandContainer);

    } else if (e.keyCode == 27) {
        command = "";
        skipKeyTest = false;
        this.isShown == false
        document.getElementsByTagName("body")[0].removeChild(xCommandContainer);

    } else if ( e.keyCode == 38 ){

        xCommandLines.moveUp ( );

    } else if ( e.keyCode == 40 ){

        xCommandLines.moveDown ( );

    } else {
        //messageBox(JSON.stringify(commandsAll));
        return true;
    }
    return true;
}

var lastCommandTmp = "";

// matches a word against a comamnd name/args. returns null if not
// matched, otherwise an array with [cmd, score, result].
function command_match(input,cmd) {
    // the first character must always match, so quickly bail out if not
    if (input.charAt (0) != cmd.charAt (0))
        return null;

    // 1. the command is a prefix of the user input, score is cmd length
    if (input.substr (0, cmd.length) == cmd)
        return [cmd, cmd.length, input];

    // 2. user input is a prefix of the command, score, socre is input length
    if (cmd.substr (0, input.length) == input)
        return [cmd, input.length, cmd];

    // split into initial word and arguments, not very elegant
    // to avoid problems with the regex later, we only allow
    // some characters as word character.
    var match = input.match (/^([a-zA-Z0-9_-]+)\S*\s*/);
    var word = match [1];
    var args = input.substr (match [0].length);

    // 3. match first word as abbreviation
    // score is length of word + penalty if character are not initial
    var regex = "^" + word.charAt (0);
    for (var i = 1; i < word.length; ++i)
        regex += "(.*?[ _\-]|(.*?))" + word.charAt (i);

    var match = cmd.match (new RegExp (regex));
    if (match) {
        var score = cmd.length + 1;

        for (var i = 2; i < match.length; i += 2)
            if (match [i] != null)
                score -= match [i].length + 3;

        return [cmd, score, cmd + (args ? " " + args : "")];
    }

    return null;
}

function autoCompleteEx() {
    if (xCommandLine.isShown == false)
        return;

    var results = [];
    var input = xCommandLine.value;

    if  ( input.length == 0 ) {
        xCommandLines.innerHTML = "";
        setTimeout(autoCompleteEx, 300);
        return;
    }

    if ( lastCommandTmp == input ) {
        setTimeout(autoCompleteEx, 300);
        return;
    }

    /*
     [["invoke small fireball","cast small fireball"],
     ["accept-invitation","afk","apply","applymode always","applymode never","applymode nochoice","body","bowmode","bowmode bestarrow","bowmode normal","bowmode spreadshot","bowmode threewide","brace","bumpmsg","cast","chat","cointoss","drop","drop all","drop cursed","drop unlocked","drop unpaid","dropall","examine","follow","get","gsay","help","hintmode hide","hintmode mark","hintmode show","hiscore","ignore","inventory","invite","invoke","killpets","lock","mapinfo","maps","mark","me","motd","orcknuckle","output-count","output-rate","output-sync","party","peaceful","petmode arena","petmode defend","petmode normal","petmode sad","pickup","prepare","quit","range","ready_skill","rename","resistances","rotateshoottype","save","say","search-items","seen","shout","showpets","skills","sound","sourcecode","statistics","suicide","take","tell","throw","time","title","unignore","unlock","uptime","use_skill","usekeys containers","usekeys inventory","usekeys keyrings","version","weather","whereabouts","whereami","who","wimpy"],null,null,["bleed","blush","bounce","bow","burp","cackle","chuckle","clap","cough","cringe","cry","dance","flip","frown","gasp","giggle","glare","grin","groan","growl","hiccup","kiss","laugh","lick","nod","pout","puke","scream","shake","shiver","shrug","sigh","smile","smirk","snap","sneeze","snicker","sniff","snore","spit","strut","sulk","think","twiddle","wave","whistle","wink","yawn"]]
     */
    //messageBox ( JSON.stringify ( commandsAll ) );
    var counter = 0;

    for ( var k = 0 ; k < commandsAll.length ; ++k ) {
        var j = ( k + 1 ) % commandsAll.length; //make my list (cast/invoke) to be treated as last
        if ( typeof (commandsAll [ j ]) != "undefined" )
            for ( var i = 0 ; i < commandsAll [ j ].length ; ++i ) {

                var res = command_match ( input , commandsAll[ j ][ i ] );
                if ( res != null ) {
                    res[3] = counter ++;
                    results.push ( res );
                }

            }
    }

    results.sort( function ( a , b ) { return b[1] - a[1] || a[3] - b[3]; } );


    xCommandLines.innerHTML = "";
    for (i = 0; i < results.length; ++i) {
        var option = $_("option");
        option.innerHTML = results[i][2] + "(" + results[i][1] + ")" + "(" + results [ i ][ 3 ] + ")";
        option.value = results[i][2];
        xCommandLines.appendChild(option);
    }
    xCommandContainer.removeChild(xCommandLines);
    xCommandContainer.appendChild(xCommandLines);
    xCommandLines.moveDown();


    lastCommandTmp = xCommandLine.value;

    setTimeout(autoCompleteEx, 300);

}
function autoComplete(keyCode) {
    if (typeof(keyCode) == "undefined") keyCode = "c".charCodeAt(0);
    if (xCommandContainer == null) {
        xCommandContainer = $_("div", {
            css: {
                position: "absolute",
                left: (stage.stageWidth / 2) + "px",
                top: (stage.stageHeight / 2) + "px"
            }
        });

        xCommandLine = $_("input");
        xCommandLine.isShown == true;

        xCommandContainer.appendChild(xCommandLine);
        xCommandLine.onkeydown = commandLineOnKeyDown;
        xCommandContainer.appendChild($_("br"));

        xCommandLines = $_("select");
        xCommandLines.size = 5;

        xCommandContainer.appendChild(xCommandLines);
        xCommandLines.selectedIndex = 0;

        xCommandLines.moveUp = function ( ) {
            if ( ! this.selectedIndex ) {
                this.selectedIndex = 0;
            }
            this.selectedIndex -= 1;
            if ( this.selectedIndex < 0 ) {
                this.selectedIndex = 0;
            }
        };

        xCommandLines.moveDown = function ( ) {
            if ( ! this.selectedIndex ){
                this.selectedIndex = 0;
            }
            this.selectedIndex += 1;
        };
        setTimeout(autoCompleteEx, 300);

    }
    if (command == "") {
        skipKeyTest = true;
        appendToBody(xCommandContainer);
        xCommandLine.isShown == true;
        try {
            xCommandLine.focus();
        } catch(e) {

        }
    }
    command += String.fromCharCode(keyCode).toLowerCase();
    xCommandLine.value = command;
    autoCompleteEx();
}
