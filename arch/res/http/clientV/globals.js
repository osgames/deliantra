/*
 * This file is part of Deliantra clientV.
 *
 * Copyright (©) 2012 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 * Deliantra clientV is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */


/**
 * Command console
 */

var xCommandContainer = null;
var xCommandLine = null;
var xCommandLines = null;
var command = "";
var lastCommand = "";
var firstOption = null;
var commandsAll = [[]];



var ctx = null;
var stage = null;
var output = null;
var commandline = null;
var myTileset = 0;

var containers = {};
var secondaryContainer = 0;

var channels = {
    length: 0,
    list: []
};

var dx = 0;
var dy = 0;

var Map = null;

messageBox = window.alert;

alert = function(s) {
    writeToScreen(s);
}

var floorItems = [];
var myItems = [];
var mySpells = [];
var myStats = [];
var skillNames = [];

var skipKeyTest = false;

var keys = {
    shift: 0,
    alt: 0,
    ctrl: 0,
    states: []
};

var isRunning = false;
var isFiring = false;

var player = {
    tag: 0,
    weight: 0,
    face: 0,
    name: ""
}

var versionInfo = {
    perlver: "5.014002",
    clientver: "3.0Af0000000",
    client: "deliantra",
    gl_vendor: "NVIDIA Corporation",
    gl_version: "4.2.0 NVIDIA 302.11",
    modulever: "1.31",
    osver: "linux",
    protver: 1
};
var expTableFacenum = -1;



mapsize = {
    width: 0,
    height: 0
};

var currentWindow = null;
var currentWindowOffset = {
    x: 0,
    y: 0
};
var currentWindowResize = false;

var xMapWindow = null;
var xFloorWindow = null;
var xPlayerWindow = null;
var pattern = null;
var patternImage = null;
var isMainCalled = false;
var xInfoWindow = null;
var xBarsWindow = null;
var xCommmandWindow = null;
var xChatBoxWindow = null;
var xLoginWindow = null;

var compass_points2integer = {"north": 1, "northeast": 2, "east": 3, "southeast": 4, "south": 5, "southwest": 6, "west": 7, "northwest": 8}

var userUsedBedToReality = false;

var name2color = [ "black", "white", "darkblue", "red", "orange", "lightblue", "darkorange", "green", "darkgreen", "grey", "brown", "yellow", "tan" ];
var cfColor    = [[1.00, 1.00, 1.00],  //#[0.00, 0.00, 0.00]
    [1.00, 1.00, 1.00], //#[0.00, 0.00, 0.00]
    [0.50, 0.50, 1.00], //#[0.00, 0.00, 0.55]
    [1.00, 0.00, 0.00],
    [1.00, 0.54, 0.00],
    [0.11, 0.56, 1.00],
    [0.93, 0.46, 0.00],
    [0.18, 0.54, 0.34],
    [0.56, 0.73, 0.56],
    [0.80, 0.80, 0.80],
    [0.75, 0.61, 0.20],
    [0.99, 0.77, 0.26],
    [0.74, 0.65, 0.41]];
