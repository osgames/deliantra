/*
 * This file is part of Deliantra clientV.
 *
 * Copyright (©) 2012 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 * Deliantra clientV is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */


function channels_setActive(channel_info) {
    for (var i = 0; i < channels.length; ++i) {
        if (channels.list[i] == channel_info) {
            channels.list[i].isActive = true;
        } else {
            channels.list[i].isActive = false;
        }
    }
}

function channels_addMessage(channelId, o) {
    var ta = channels_get_chat_box("channel_" + channelId);
    if (ta == null) return;
    var d = new Date();

    var div = $_('div');
    div.innerHTML = (d.getHours()<10?"0"+d.getHours():d.getHours()) + ":" + (d.getMinutes()<10?"0"+d.getMinutes():d.getMinutes()) + ":" + (d.getSeconds()<10?"0"+d.getSeconds():d.getSeconds()) + ":" + (typeof(o) == typeof("") ? o : JSON.stringify(o));
    div.innerHTML = div.innerHTML.replace ( /<tt>/ig,"<code>" );
    div.innerHTML = div.innerHTML.replace ( /<\/tt>/ig,"</code>" );

    div.innerHTML = div.innerHTML.replace ( /<fg name="/ig , "<span class=\"fg\" style=\"color:" );
    div.innerHTML = div.innerHTML.replace ( /<\/fg>/ig , "</span>" );

    div.innerHTML = div.innerHTML.replace ( /<bg name="/ig , "<span class=\"bg\" style=\"background-color:" );
    div.innerHTML = div.innerHTML.replace ( /<\/bg>/ig , "</span>" );

    for ( var i = name2color.length -1 ; i >= 0; --i){
        div.innerHTML = div.innerHTML.replace ( "style=\"color:" + name2color[i] , "style=\"color:" + cfColor[i] );
        div.innerHTML = div.innerHTML.replace ( "style=\"background-color:" + name2color[i] , "style=\"background-color:" + cfColor[i] );
    }
    ta.appendChild(div);
    channels[channelId].newMessages = true;
}

function channels_add(channel_info) {
    var button, textarea;
    if (channels[channel_info['id']]) {
        return;
    }
    channel_info.messages = [];
    channel_info.index = channels.list.length;
    channels[channel_info['id']] = channel_info;
    channels.list.push(channel_info);
    channels.length += 1;
    channels_setActive(channel_info);
    xChatBoxWindow.buttonsContainer.appendChild(button = $_("input"));
    button.value = channel_info['id'];
    button.type = "button";
    xChatBoxWindow.channelContainer.appendChild(div = $_("div"));
    div.id = "channel_" + button.value;

    div.className = "channelBox";

    div.style.width = "90%";
    div.style.height = "85%";
    channels_hide_other("channel_" + button.value);
    button.onclick = function() {
        channels_hide_other("channel_" + button.value);
    };
}
function channels_get_chat_box(pid) {
    var list = xChatBoxWindow.channelContainer.getElementsByTagName("div");
    for (var i = 0; i < list.length; ++i) {
        var e = list[i];
        if (e.id == pid) return e;
    };
    return null;
}
function channels_hide_other(pid) {
    var list = xChatBoxWindow.channelContainer.getElementsByTagName("div");
    for (var i = 0; i < list.length; ++i) {
        var e = list[i];
        if ( e.className != "channelBox" ) {
            continue;
        }
        if (e.id != pid) e.style.display = "none";
        else e.style.display = "block";
    };
}
