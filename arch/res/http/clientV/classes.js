function Item() {
    this.itemId = 0;
    this.flags = 0;
    this.weight = 0;
    this.face = 0;
    this.name = "";
    this.namepl = "";
    this.animation_id = 0;
    this.anim_speed = 0;
    this.nr_of = 0;
    this.clientType = 0;
}

function FaceInfo() {
    this.name = "";
    this.num = -1;
    this.type = 0;
    this.isLoaded = false;
    this.isLoading = false;
    this.isPending = false;
    this.isRequested = false;
    this.requestedBy = "";
    this.resource = null;
    this.json = "{}";
    this.afterLoad = [];
}

FaceInfo.prototype.request = function(rby) {
    this.isRequested = true;
    this.requestedBy = rby;

    if ((this.name == "")) return this;

    if ((!this.isPending) && (!this.isLoading)) {
        this.load();
    }

    return this;
};

FaceInfo.prototype.appendOnLoad = function(f) {

    if (typeof(f) == typeof(function() {})) {
        this.afterLoad.push(f);
    }
    return this;
};

FaceInfo.prototype.setName = function(name) {
    this.name = name;
    return this;
};

FaceInfo.prototype.setType = function(type) {
    this.type = parseInt(type);
    return this;
};

FaceInfo.prototype.load = function() {

    this.isPending = true;

    Faces.load(this);

}
FaceInfo.prototype.onload = function() {
    this.isPending = false;
    this.isLoading = false;

    var o = this;
    this.afterLoad.each(function(e, i) {
        e(o);
    });
    Faces.onload(this);
}
FaceInfo.prototype.loadStart = function() {

    this.isPending = false;
    this.isLoading = true;
    if (this.requestedBy == "map" || this.requestedBy == "ui") { //map tile or to be displayed in window
        var img = this.resource = $_("img");
        img.faceInfo = this;
        img.addEventListener("load", function() {
            img.faceInfo.onload();
        });
        img.src = baseURL + "" + this.name;

    } else { //json but maybe other types
        var req = new XMLHttpRequest();
        req.open('GET', baseURL + "" + this.name, false);
        req.send(null);
        if (req.status == 200) {
            this.json = req.responseText;
            this.resource = JSON.parse(this.json);
        }
        this.onload();
    }
}

var Faces = {
    byNum: [],

    nDownloads: 0,
    pending: [],

    add: function(id, name) {
        var faceInfo = new FaceInfo;
        faceInfo.num = id;
        this.byNum[id] = faceInfo;
        return faceInfo;
    },
    getByNum: function(id, createIfNotExist) {
        if (createIfNotExist) return Faces.byNum[id] ? Faces.byNum[id] : Faces.add(id);
        return Faces.byNum[id] ? Faces.byNum[id] : null;
    },
    request: function(id, by, f) {
        return this.getByNum(id, true).appendOnLoad(f).request(by);
    },

    load: function(faceInfo) {
        if (this.nDownloads < this.MAXDOWNLOADS) {
            this.nDownloads++;
            faceInfo.loadStart();
        } else {
            this.pending.push(faceInfo);
        }
    },
    onload: function(faceInfo) {
        this.nDownloads--;

        if ((this.nDownloads < this.MAXDOWNLOADS) && (this.pending.length > 0)) {
            var faceInfo = this.pending.pop();
            faceInfo.load();
        }
    },

    FT_FACE: 0 * 2 + 0,
    // faces (images)
    FT_MUSIC: 1 * 2 + 1,
    // background music
    FT_SOUND: 2 * 2 + 1,
    // effects
    FT_RSRC: 3 * 2 + 0,
    // generic data files
    MAXDOWNLOADS: 2
}