/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CFace (game, id) {
    this.id     = id;
    this.game   = game;
    this.type   = -1;
    this.hash   = null;
    this.loaded = false;
    CLinkedList.call (this);
}

CFace.prototype             = Object.create (CLinkedList.prototype);
CFace.prototype.constructor = CFace;

CFace.prototype.play = function () {
    if (this.sound) {
        this.sound.play ();
    }
};

CFace.prototype.load = function () {
    var self = this;

    switch (this.type) {
        case 0: // img
            self.loading = true;
            var img = new Image ();

            img.onload = function () {
                self.img = this;
            };
            img.src = face_url + this.hash;
            break;

        case 6: // JSON ?
            getJSON (face_url + this.hash, function (data) {
                self.loaded = true;
                self.data   = data;
            // console.log(self.data[0], self.id);
            });
            break;

        case 5: // sound
        case 3: // music
            self.sound = new Howl ({
                src:   [face_url + this.hash],
                html5: true
            });
            console.log (self.id, self.type);
            //                getData('http://gameserver.deliantra.net:13327/' + this.hash, function (data) {
            //                self.data = data;
            //                playByteArray(self.data);
            //                /*
            //                 */
            //            }, 'arraybuffer');
            //    };
            break;
    }

    if (this.type % 2 == 1) {
        getData (face_url + this.hash, function (data) {
            self.meta = data;
        });
    }
};

var context = new AudioContext () || new webkitAudioContext ();
var buf; // Audio buffer

// Play the loaded file
function play () {
    // Create a source node from the buffer
    var source = context.createBufferSource ();

    source.buffer = buf;
    // Connect to the final output node (the speakers)
    source.connect (context.destination);
    // Play immediately
    source.start (0);
}

function playByteArray (byteArray) {
    if (fxx) {
        return;
    }
    fxx = true;
    // console.log(byteArray);
    // console.log("X");
    var arrayBuffer = new ArrayBuffer (byteArray.length);
    var bufferView  = new Uint8Array (arrayBuffer);

    for (i = 0; i < byteArray.length; i++) {
        bufferView[i] = byteArray[i];
    }
    // console.log(arrayBuffer);
    // console.log("X");
    context.decodeAudioData (arrayBuffer, function (buffer) {
        buf = buffer;
        play ();
    });
}

