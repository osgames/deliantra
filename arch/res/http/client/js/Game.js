/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CGame () {
    
    document.body.appendChild ($_ ("div", { id: "hidden" }));
    this._nextExtId = 100;
    // console.log ('created map');
    this.version  = "0.004";
    this.cellSize = 64;
    // console.log ('created container');
    /**
     * @type CNetworking
     */
    this.networking = new CNetworking (this);
    // console.log ('created networking');
    // this.networking = (confirm("playback ?" ) === true) ? new CFakeNetworking(this) : new CNetworking(this);
    /**
     * @type CKeyboard
     */
    this.keyboard      = new CKeyboard (this);
    this.containers    = new CContainers ();
    this.canvas        = $_ ("canvas");
    this.ctx           = this.canvas.getContext ("2d");
    this.canvas.width  = window.innerWidth - 5;
    this.canvas.height = 50;
    document.body.appendChild (this.canvas);
    // console.log ('created canvas');
    this.ticks       = 0;
    this.lastCommand = 0;
    this.myStats     = [];
    this.mySpells    = [];
    this.faces       = new CFaces ();
    // console.log ('created faces');
    // console.log ('created console');
    var defaultWindowOptions = {
        resizeCanvas: true
    };
    var noResizeWindowOptions = {
        resizeCanvas: true,
        resizeable:   false
    };
    var cssProperties = getStyleRuleValue (["left", "top", "width", "height"], ".mapWindow");

    this.tooltip                  = new CTooltip ("");
    this.tooltip.div.style.zIndex = 999;
    /**
     * @type CWindowMap
     */
    // var size = Math.min(Math.max(window.innerWidth-50,parseInt(cssProperties.width)),Math.max(window.innerHeight-50,parseInt(cssProperties.height)));
    var size = Math.max (window.innerWidth, window.innerHeight);

    this.mapWindow = new CWindowMap (parseInt (cssProperties.left), 0, size, size, this, defaultWindowOptions);
    // console.log ('created mapwindow');
    /**
     * @type Cmap
     */
    this.map = new CMap (this, parseInt (this.mapWindow.width / this.cellSize), parseInt (this.mapWindow.width / this.cellSize)); // adjust map size to window size
    cssProperties = getStyleRuleValue (["left", "top", "width", "height"], ".floorWindow");
    /**
     * @type CWindowFloor
     */

    this.floorWindow = new CWindowFloor (parseInt (cssProperties.left), parseInt (cssProperties.top), parseInt (cssProperties.width), parseInt (cssProperties.height), this, noResizeWindowOptions);
    // console.log ('created floor window');
    /**
     * @type CWindowBars
     */
    cssProperties = getStyleRuleValue (["left", "top", "width", "height"], ".barsWindow");

    this.barsWindow = new CWindowBars (parseInt (cssProperties.left), parseInt (cssProperties.top), parseInt (cssProperties.width), parseInt (cssProperties.height), this, defaultWindowOptions);
    // console.log ('created bars window');
    /**
     * @type CWindowSkills
     */
    this.skillsWindow = new CWindowSkills (parseInt (cssProperties.left), parseInt (cssProperties.top), parseInt (cssProperties.width), parseInt (cssProperties.height), this, noResizeWindowOptions);
    this.skillsWindow.show (false);
    // console.log ('created skills window');
    /**
     * @type CWindowBars
     */
    cssProperties = getStyleRuleValue (["left", "top", "width", "height"], ".spellsWindow");
    /**
     * @type CWindowSkills
     */

    this.spellsWindow = new CWindowSpells (parseInt (cssProperties.left), parseInt (cssProperties.top), parseInt (cssProperties.width), parseInt (cssProperties.height), this, noResizeWindowOptions);
    this.spellsWindow.show (false);
    // console.log ('created skills window');
    cssProperties = getStyleRuleValue (["left", "top", "width", "height"], ".containerWindow");
    /**
     * @type CWindowContainer
     */

    this.containerWindow = new CWindowContainer (parseInt (cssProperties.left), parseInt (cssProperties.top), parseInt (cssProperties.width), parseInt (cssProperties.height), this, noResizeWindowOptions);
    // console.log ('created container window');
    cssProperties = getStyleRuleValue (["left", "top", "width", "height"], ".messagesWindow");
    /**
     * @type CWindowMessages
     */

    this.messagesWindow = new CWindowMessages (parseInt (cssProperties.left), parseInt (cssProperties.top), parseInt (cssProperties.width), parseInt (cssProperties.height), this, {
        resizeCanvas: false
    });
    // console.log ('created messages window');
    this.containerWindow.show (false);
    this.items    = {};
    this.channels = [JSON.parse ("{\"tooltip\":\"Server info\",\"title\":\"Info\",\"id\":\"info\",\"reply\":\"\",\"messages\":[], \"last\":0}")];
    // console.log ('initialized channels');
    [this.containerWindow, this.messagesWindow, this.skillsWindow, this.mapWindow, this.barsWindow, this.floorWindow].each (function (o) {
        o.show (false);
    });
    // console.log ('hidden ui');
    this.loginWindow = new CWindowLogin (0, 0, 440, 185, this, {
        resizeCanvas: false,
        closable:     true
    });
    // console.log ('created login window');
    this.loginWindow.center ();
    // console.log ('centered login window');
    var self = this;

    this.settingsWindow = new CWindowSettings (0, 0, this);
    this.settingsWindow.show (false);
    this.rangeCombatWindow = new CWindowRangeCombat (50, 0, this);
    this.rangeCombatWindow.show (false);
    this.macroWindow = new CWindowMacro (50, 50, this);
    this.macroWindow.show (false);
    this.contextMenu = new CWindowContext (0, 0, this);
    this.contextMenu.show (false);
    this.selectAmountWindow = new CWindowSelectAmount (0, 0, this);
    this.selectAmountWindow.show (false);
    this.selectAmountWindow.center ();
    this.pickupWindow = new CWindowPickup (0, 0, this);
    this.pickupWindow.show (false);
    this.pickupWindow.center ();
    this.statisticsWindow = new CWindowStatistics (0, 0, this);
    this.statisticsWindow.show (false);
    this.statisticsWindow.center ();
    /**
     * @type CCommandConsole
     */
    this.commandConsole = new CCommandConsole (this);
    // this.miniMap = new CWindow(800,0,300,300,this,{noTitleBar:false});
    // this.miniMap.show(false);
    this.inputText = true;
    // setInterval(function ( ){ self.mainLoop( ); }, 1000/40);
    this.keyboard.onKeyPressCb.push (function (e) {
        self.onKeyPress (e);
    });
    this.keyboard.registerKeyDownHandler (CKeyboard.KEYS.CTR, function () {
        self.toggleRun (true);
    });
    this.keyboard.registerKeyUpHandler (CKeyboard.KEYS.CTR, function () {
        self.toggleRun (false);
    });
    this.keyboard.registerKeyDownHandler (CKeyboard.KEYS.SHIFT, function () {
        self.toggleFire (true);
    });
    this.keyboard.registerKeyUpHandler (CKeyboard.KEYS.SHIFT, function () {
        self.toggleFire (false);
    });
    this.keyboard.registerKeyUpHandler (CKeyboard.KEYS.COMMA, function () {
        self.networking.send ("command get");
    });
    this.keyboard.registerKeyUpHandler (CKeyboard.KEYS.TAB, function () {
        if (self.commandConsole.command.length) { // ignore if there is command console input
            return;
        }
        self.containerWindow.container = self.containers.first;
        self.containerWindow.toggleShow ();
        self.containerWindow.draw ();
    });
    this.keyboard.registerKeyUpHandler (CKeyboard.KEYS.F3, function () {
        self.skillsWindow.toggleShow ();
        self.skillsWindow.draw ();
    });
    window.addEventListener ("resize", function () {
        // self.mapWindow.resize(window.innerWidth - 90, window.innerHeight - 90);
        // self.mapWindow.center();
    });
    registerVisibilityChangeCb (function (b) {
        self.onVisibilityChange (b);
    });
    window.addEventListener ("blur", function () {
        self.keyboard.clear ();
    });
    window.addEventListener ("focus", function () {
        self.keyboard.clear ();
    });
}

CGame.prototype.onVisibilityChange = function () {
    this.keyboard.clear ();
};

CGame.prototype.showUI = function () {
    [this.messagesWindow, this.mapWindow, this.barsWindow, this.floorWindow, this.settingsWindow, this.rangeCombatWindow /* ,this.miniMap*/ ].each (function (o) {
        o.show (true);
    });
    this.inputText = false;
    this.loginWindow.show (false);
};

CGame.prototype.toggleFire = function (b) {
    if ((!this.isFiring) && b) {
        this.isFiring = true;
        this.networking.send ("command fire");
    } else if (this.isFiring && !b) {
        this.isFiring = false;
        this.lastDir  = null;
        this.networking.send ("command fire_stop");
    }
};

CGame.prototype.toggleRun = function (b) {
    if ((!this.isRunning) && b) {
        this.isRunning = true;
        this.networking.send ("command run");
    } else if (this.isRunning && !b) {
        this.isRunning = false;
        this.lastDir   = null;
        this.networking.send ("command run_stop");
    }
};

CGame.prototype.getNextExtId = function () {
    return ++this._nextExtId;
};

CGame.prototype.start = function () {
    this.networking.connect ();
};

CGame.prototype.findFace = function (id) {
    var o = this.faces.find (id);

    if (!o) {
        return null;
    }

    if (o.img) {
        return o.img;
    } else {
        if (o.loading == true) {
            return null;
        }

        o.loading = true;
        var img = new Image ();

        img.onload = function () {
            o.img = this;
        };

        img.src = server_host + o.hash;
        
        return null;
    }
};

CGame.prototype.onKeyPress = function (e) {
    if (this.commandConsole.onInput (e)) {
        return;
    }
    var dirName = "";
    var dirMap  = this.keyboard.dirMap;

    for (var keyCode in dirMap) {
        if (this.keyboard.getKeyOnce (parseInt (keyCode))) {
            if (typeof (dirMap[keyCode]) === "string") {
                dirName = dirMap[keyCode];
            } else {
                dirName = dirMap[keyCode][this.keyboard.getKey (CKeyboard.KEYS.ALT) ? "alt" : "normal"];
            }
            break;
        }
    }
    if (this.keyboard.getKeyOnce (CKeyboard.KEYS.SPACE)) {
        this.networking.send ("command apply");
    } else if (this.keyboard.getKeyOnce (this.keyboard.getNumpadKeyCode (5)) || this.keyboard.getKeyOnce (CKeyboard.KEYS.KP5)) {
        this.networking.send ("command fire");
        this.networking.send ("command stay");
        this.networking.send ("command fire_stop");
    }

    if (dirName.length) {
        if (this.isRunning) {
            if (!this.lastDir || (this.lastDir && this.lastDir != dirName)) {
                this.networking.send ("command " + dirName);
                this.lastDir = dirName;
            }
        } else {
            this.networking.send ("command " + dirName);
            this.lastDir = null;
        }
    }
};

CGame.prototype.findChannel = function (id) {
    for (var i = 0; i < this.channels.length; ++i) {
        if (this.channels[i].id == id) {
            this.channels[i].active = true; // we will surely add msg if we look for channel info
            
            return this.channels[i];
        }
    }
    
    return {
        messages: []
    };
};

CGame.prototype.mainLoop = function () {};
