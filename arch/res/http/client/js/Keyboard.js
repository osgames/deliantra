/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CKeyboard (game) {
    this.game         = game;
    this.keysDown     = [];
    this.onKeyPressCb = [];
    this.handlersDown = [];
    this.handlersUp   = [];
    var self = this;

    this.dirMap = {};
    
    this.registerKeysInDirMap (CKeyboard.KEYS.LEFT, this.getNumpadKeyCode (6), { normal : "east", alt : "southeast" });
    this.registerKeysInDirMap (CKeyboard.KEYS.RIGHT, this.getNumpadKeyCode (4), { normal : "west", alt : "northwest" });
    this.registerKeysInDirMap (CKeyboard.KEYS.DOWN, this.getNumpadKeyCode (2), { normal : "south", alt : "southwest" });
    this.registerKeysInDirMap (CKeyboard.KEYS.UP, this.getNumpadKeyCode (8), { normal : "north", alt : "northeast" });
    this.registerKeysInDirMap (CKeyboard.KEYS.DOWNLEFT, this.getNumpadKeyCode (1), "southwest");
    this.registerKeysInDirMap (CKeyboard.KEYS.DOWNRIGHT, this.getNumpadKeyCode (3), "southeast");
    this.registerKeysInDirMap (CKeyboard.KEYS.UPLEFT, this.getNumpadKeyCode (7), "northwest");
    this.registerKeysInDirMap (CKeyboard.KEYS.UPRIGHT, this.getNumpadKeyCode (9), "northeast");

    window.addEventListener ("keydown", function (e) {
        return self.onKeyPress (e);
    });

    window.addEventListener ("keyup", function (e) {
        return self.onKeyPressRelease (e);
    });
}

CKeyboard.prototype.clear = function () {
    this.keysDown = [];
};

CKeyboard.prototype.registerKeysInDirMap = function (key1, key2, dir) {
    this.dirMap[key1] = this.dirMap[key2] = dir;
};

CKeyboard.prototype.onKeyPress = function (e) {
    var i;

    if (this.game.inputText) {
        return true;
    }
    this.keysDown[e.keyCode] = true;
    e.preventDefault ();
    for (i = 0; i < this.onKeyPressCb.length; ++i) {
        if (this.onKeyPressCb[i] (e) === false) {
            return false;
        }
    }
    if (this.handlersDown[e.keyCode]) {
        var handlers = this.handlersDown[e.keyCode];

        for (i = 0; i < handlers.length; ++i) {
            handlers[i] (e, true);
        }
    }

    return false;
};

CKeyboard.prototype.registerKeyDownHandler = function (key, cb) {
    var handlers = this.handlersDown[key] = this.handlersDown[key] || [];

    handlers.push (cb);
};

CKeyboard.prototype.registerKeyUpHandler = function (key, cb) {
    var handlers = this.handlersUp[key] = this.handlersUp[key] || [];

    handlers.push (cb);
};

CKeyboard.prototype.onKeyPressRelease = function (e) {
    if (this.game.inputText) {
        return true;
    }
    this.keysDown[e.keyCode] = false;
    if (this.handlersUp[e.keyCode]) {
        var handlers = this.handlersUp[e.keyCode];

        for (var i = 0; i < handlers.length; ++i) {
            handlers[i] (e, false);
        }
    }
    e.preventDefault ();
    
    return false;
};

CKeyboard.prototype.getNumpadKey = function (digit) {
    return this.keysDown[("" + digit).charCodeAt (0)] || false;
};

CKeyboard.prototype.getNumpadKeyCode = function (digit) {
    var trans = [0, 97, 98, 99, 100, 101, 102, 103, 104, 105];
    
    return trans[digit];
};

CKeyboard.prototype.getNumpadKeyOnce = function (digit) {
    return this.getKeyOnce (this.getNumpadKeyCode (digit));
};

CKeyboard.prototype.getKey = function (code) {
    return this.keysDown[code] || false;
};

CKeyboard.prototype.getKeyOnce = function (code) {
    var r = this.keysDown[code] || false;

    this.keysDown[code] = false;
    
    return r;
};

CKeyboard.prototype.tick = function () {};
CKeyboard.KEYS           = {
    ESC:       27,
    CENTER:    12,
    UPLEFT:    36,
    UPRIGHT:   33,
    DOWNLEFT:  35,
    DOWNRIGHT: 34,
    LEFT:      39,
    RIGHT:     37,
    UP:        38,
    DOWN:      40,
    SPACE:     32,
    BACKSPACE: 8,
    ENTER2:    10,
    ENTER:     13,
    KP5:       12,
    CTR:       17,
    TAB:       9,
    ALT:       18,
    SHIFT:     16,
    F3:        114,
    COMMA:     188,
    BACKTICK:  192,
};
