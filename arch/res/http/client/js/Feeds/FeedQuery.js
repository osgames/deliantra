/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CFeedQuery (name, data, networking) {
    CFeedBase.call (this, name, data, networking);
}
CFeedQuery.prototype             = Object.create (CFeedBase.prototype);
CFeedQuery.prototype.constructor = CFeedQuery;
CFeedQuery.prototype.process     = function () {
    CFeedBase.prototype.process.apply (this);
    console.log (this.data);
    var flag = parseInt (this.data[6]);
    var r;

    if (flag & CS_QUERY_YESNO) {
        r = confirm (this.data.substr (7)) == true ? "y" : "n";

        this.networking.send ("reply " + r);
    } else {
        r = prompt (this.data.substr (7));

        this.networking.send ("reply " + r);
    }
    
    return;
};

