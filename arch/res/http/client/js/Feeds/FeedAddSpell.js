/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CFeedAddSpell (name, data, networking) {
    CFeedBase.call (this, name, data, networking);
}
CFeedAddSpell.prototype             = Object.create (CFeedBase.prototype);
CFeedAddSpell.prototype.constructor = CFeedAddSpell;
CFeedAddSpell.prototype.process     = function () {
    CFeedBase.prototype.process.apply (this);
    var buff = this.getBuffer ();
    
    while (buff.position < buff.length) {
        spell             = new CSpell ();
        spell.tag         = buff.readUint32 ();
        spell.minLevel    = buff.readUint16 ();
        spell.castingTime = buff.readUint16 ();
        spell.mana        = buff.readUint16 ();
        spell.grace       = buff.readUint16 ();
        spell.level       = buff.readUint16 ();
        spell.skill       = buff.readUint8 ();
        spell.path        = buff.readUint32 ();
        spell.face        = buff.readUint32 ();
        var len = buff.readUint8 ();

        spell.name = buff.readUtf8StringFromBinaryString (this.data, buff.position, len);
        buff.position += len;
        //        var s2 = spell.name;
        //        var s = "";
        //        for (var i = 0; i<s2.length;++i){
        //            s += s2.charCodeAt(i).toString(16) + " ";
        //        }
        //        console.log(s);
        // console.log(spell.name);
        // len = buff.readUint16();
        // spell.message = buff.readString(len);
        this.game.mySpells.push (spell);
    }
// console.log(this.game.mySpells);
};

