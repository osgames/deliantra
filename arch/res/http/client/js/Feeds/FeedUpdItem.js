/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CFeedUpdItem (name, data, networking) {
    CFeedBase.call (this, name, data, networking);
}
CFeedUpdItem.prototype             = Object.create (CFeedBase.prototype);
CFeedUpdItem.prototype.constructor = CFeedUpdItem;
CFeedUpdItem.prototype.process     = function () {
    CFeedBase.prototype.process.apply (this);
    var buff = this.getBuffer ();
    var flags = buff.readUint8 ();
    var tag   = buff.readUint32 ();
    var itm   = this.game.items[tag];

    if (!itm) {
        return;
    }
    // console.log (itm.name);
    if (flags & UPD_LOCATION) {
        // console.log (itm.containerObject);
        if (itm.containerObject) {
            itm.containerObject.remove (itm);
        }
        var container       = buff.readUint32 ();
        var containerObject = this.game.containers.find (container) || this.game.containers.add (container);

        containerObject.add (itm, true);
    // console.log (itm.containerObject);
    }
    var oldflags = itm.flags;

    if (flags & UPD_FLAGS) {
        itm.flags = buff.readUint32 ();
    }
    if (flags & UPD_WEIGHT)
        itm.weight = buff.readUint32 ();
    if (flags & UPD_FACE)
        itm.face = buff.readUint32 ();
    if (oldflags & F_OPEN && (!(itm.flags & F_OPEN))) {
        this.game.secondaryContainer = null;
    }
    if ((itm.flags & F_OPEN)) {
        secondaryContainer           = tag;
        this.game.secondaryContainer = this.game.containers.find (secondaryContainer) || this.game.containers.add (secondaryContainer);
    // console.log (this.game.secondaryContainer.items);
    }
    if (flags & UPD_NAME) {
        var len   = (buff.readUint8 ()); // my $len = unpack "C", substr $data, 0, 1, "";
        var names = buff.readUtf8StringFromBinaryString (this.data, buff.position, len);
        buff.position += len;

        names    = names.split (String.fromCharCode (0));
        itm.name = names[0];
        if (names.length > 1) {
            itm.namepl = names[1];
        }
    }
    if (flags & UPD_ANIM)
        itm.anim = buff.readUint16 ();
    if (flags & UPD_ANIMSPEED)
        itm.anim_speed = buff.readUint8 ();
    if (flags & UPD_NROF)
        itm.nr_of = buff.readUint32 ();
    this.game.containerWindow.draw ();
    this.game.floorWindow.draw ();
    this.game.macroWindow.notifyWorkersOfContainersUpdate ();
};

