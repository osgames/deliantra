/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CFeedPlayer (name, data, networking) {
    CFeedBase.call (this, name, data, networking);
}
CFeedPlayer.prototype             = Object.create (CFeedBase.prototype);
CFeedPlayer.prototype.constructor = CFeedPlayer;

CFeedPlayer.prototype.process = function () {
    CFeedBase.prototype.process.apply (this);
    var buff = this.getBuffer ();
    // my ($tag, $weight, $face, $name) = unpack "NNN C/a", $data;
    var player = (this.game.player = this.game.player || new CPlayer (this.game));

    player.tag    = buff.readUint32 ();
    player.weight = buff.readUint32 ();
    player.face   = buff.readUint32 ();
    var len = buff.readUint8 ();

    player.name = buff.readUtf8StringFromBinaryString (this.data, buff.position, len);
};

