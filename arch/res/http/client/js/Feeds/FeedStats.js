/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

var stat_32bit = [];

stat_32bit[CS_STAT_WEIGHT_LIM]   = 1;
stat_32bit[CS_STAT_SPELL_ATTUNE] = 1;
stat_32bit[CS_STAT_SPELL_REPEL]  = 1;
stat_32bit[CS_STAT_SPELL_DENY]   = 1;
stat_32bit[CS_STAT_EXP]          = 1;

function CFeedStats (name, data, networking) {
    CFeedBase.call (this, name, data, networking);
}

CFeedStats.prototype             = Object.create (CFeedBase.prototype);
CFeedStats.prototype.constructor = CFeedStats;
CFeedStats.prototype.process     = function () {
    CFeedBase.prototype.process.apply (this);
    var buff = this.getBuffer ();

    var face       = this.game.faces ? this.game.faces.find (this.game.faceIDSkillNames) : null;
    var skillNames = face ? face.data || [] : null;

    while (buff.position < buff.length) {
        var stat = buff.readUint8 ();
        var value;

        if (stat_32bit[stat]) {
            value = buff.readUint32 ();
        } else if (stat == CS_STAT_SPEED || stat == CS_STAT_WEAP_SP) {
            value = (1 / FLOAT_MULTF) * buff.readUint32 ();
        } else if (stat == CS_STAT_RANGE || stat == CS_STAT_TITLE) {
            var len = buff.readUint8 ();

            value = buff.readUtf8StringFromBinaryString (this.data, buff.position, len);
            buff.position += len;
        } else if (stat == CS_STAT_EXP64) {
            value = buff.readUint64 ();
        } else if (stat >= CS_STAT_SKILLINFO && stat < CS_STAT_SKILLINFO + CS_NUM_SKILLS) {
            var lvl     = buff.readUint8 ();
            var exp     = buff.readUint64 ();
            var prevExp = this.game.myStats[stat] instanceof Array ? this.game.myStats[stat][1] : 0;

            if (prevExp < exp)
                if (skillNames)
                    Bubble.add ("gained " + (exp - prevExp) + " exp in skill " + (skillNames.length ? skillNames[stat - CS_STAT_SKILLINFO][0] : ""));

            value = [lvl, exp];
        } else {
            value = buff.readUint16 ();
        }

        this.game.myStats[stat] = value;
        // stats_update (stat);
    }

    this.game.barsWindow.draw ();

    if (this.game.skillsWindow.isShown ()) {
        this.game.skillsWindow.draw ();
    }
};

