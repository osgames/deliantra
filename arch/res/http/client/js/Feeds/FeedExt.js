/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CFeedExt (name, data, networking) {
    CFeedBase.call (this, name, data, networking);
}
CFeedExt.prototype             = Object.create (CFeedBase.prototype);
CFeedExt.prototype.constructor = CFeedExt;
CFeedExt.prototype.process     = function () {
    CFeedBase.prototype.process.apply (this);
    if (this.data.indexOf ("nonces") > 0) { // unknown
        var nonces = JSON.parse (this.data.substring ("ext ".length));

        this.game.nonces1 = nonces[1];
        this.game.nonces2 = nonces[2];
        
        return;
    }
    //console.log(this.data);
    if (this.data.indexOf ("channel_info") > 0) {
        //console.log (this.data);
        var channel_info = JSON.parse (this.data.substr (4))[1];

        channel_info.last     = 0;
        channel_info.messages = [];
        if (!channel_info.id) {
            return;
        }
        for (var i = 0; i < this.game.channels.length; ++i) {
            if (this.game.channels[i].id == channel_info.id) {
                for (var s in channel_info) {
                    this.game.channels[i][s] = channel_info[s];
                }
                this.game.channels[i].table = null;
                this.game.messagesWindow.draw ();
                this.game.messagesWindow.setText ();
                                
                return;
            }
        }
        this.game.channels.push (channel_info);
        this.game.messagesWindow.draw ();
        
        return;
    } else {
        var o       = JSON.parse (this.data.substr (4));
        var replyID = parseInt (o[0].split ("-")[1]);

        if (replyID == 101) {
            /* baseURL = o[1];
            baseURL = "http://gameserver.deliantra.net:13327" + baseURL + "";*/
            this.networking.send ("exti [\"resource\"," + this.game.getNextExtId () + ",\"exp_table\"]");
        } else if (replyID == 102) {
            this.game.faceIDExpTable = parseInt (o[1]);
            this.networking.send ("exti [\"resource\"," + this.game.getNextExtId () + ", \"skill_info\", \"spell_paths\",\"xxx\"]");
        } else if (replyID == 103) {
            this.game.faceIDSkillNames = parseInt (o[1]);
            this.game.faceIDSpellPaths = parseInt (o[2]);
            // this.game.faceIDCommandsAll = parseInt (o[3]);
            // this.networking.send("addme");
            var req = "exti " + JSON.stringify (["fx_want", this.game.getNextExtId (), {
                "6": 1,
                "3": 1,
                "5": 1
            }]);

            req = JSON.stringify (req).replace (/\\\"/g, "\"").substr (1);
            req = req.substr (0, req.length - 1);
            this.networking.send (req);
        } else if (replyID == 104) {
            var hash = deliantra_util.hash_pw (this.game.loginWindow.inputPassword.value);
            //            if (!this.game.loginWindow.nostorage) {
            //                if (this.game.loginWindow.hash && this.game.loginWindow.inputLogin.value == localStorage.getItem('login')){
            //                    hash = localStorage.getItem('hash');
            //                } else {
            //                    localStorage.setItem('hash', hash);
            //                }
            //            }

            hash = deliantra_util.auth_pw (hash, this.game.nonces1, this.game.nonces2);
            var msg = "exti " + JSON.stringify (["login", this.game.getNextExtId () + "", this.game.loginWindow.inputLogin.value, hash]);
            var b   = new Blob (msg.split (""));

            this.networking.send (b);
        } else if (replyID == 105) {
            if (parseInt (o[1]) == 1) {
                this.game.showUI ();
            } else {
                alert (o[2]);
                this.game.loginWindow.show ();
            }
        } else {
            // console.log (this.data);
        }
    }
// //console.log(this.data);
};

