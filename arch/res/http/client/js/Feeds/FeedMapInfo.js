/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CFeedMapInfo (name, data, networking) {
    CFeedBase.call (this, name, data, networking);
}
CFeedMapInfo.prototype             = Object.create (CFeedBase.prototype);
CFeedMapInfo.prototype.constructor = CFeedMapInfo;
CFeedMapInfo.prototype.process     = function () {
    CFeedBase.prototype.process.apply (this);
    /*
    var data = this.data.split (" ");
    var x    = parseInt (data[4]);
    var y    = parseInt (data[5]);
// console.log(data);
/* this.game.mapInfo = this.game.mapInfo || {};
this.game.mapInfo = {x:this.game.mapInfo.x || x, y:this.game.mapInfo.y || y, name:data.pop()};
this.game.miniMap.data  = this.game.miniMap.data || {};*/
};

