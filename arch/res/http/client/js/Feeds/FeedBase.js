/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CFeedBase () {
    this.networking = null;
}
CFeedBase.prototype.init = function (name, data, networking) {
    this.name = name;
    if (!this.networking) {
        /**
         * @type CNetworking
         */
        this.networking = networking;
        /**
         * @type CGame
         */
        this.game = networking.game;
    }
    this.data = data;
};
CFeedBase.prototype.process = function () {
    // console.log ('processing feed: ' + this.name);
    // console.log (this.data);
};
CFeedBase.prototype.getDataArray = function () {
    var arr = [];

    for (var i = 0; i < this.data.length; ++i) {
        arr.push (this.data.charCodeAt (i));
    }
    
    return arr;
};

CFeedBase.prototype.getBuffer = function () {
    var buff = CFeedBase.buffer || new Buffer ([], 0);
    CFeedBase.buffer = buff;
    buff.buffer = this.getDataArray ();
    buff.binaryString = this.data;
    buff.position = this.name.length + 1;
    buff.length = buff.buffer.length;
    return buff;
};
