/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CMap (game, width, height) {
    this.canvas = $_ ("canvas");
    this.ctx    = this.canvas.getContext ("2d");
    /**
     * @type CGame
     */
    this.game = game;
    this.dx   = 0;
    this.dy   = 0;
    this.resize (width, height);
}
CMap.prototype.resize = function (width, height) {
    // console.log (width);
    // console.log (height);
    this.width         = width = parseInt (width);
    this.height        = height = parseInt (height);
    this.canvas.width  = width * this.game.cellSize;
    this.canvas.height = height * this.game.cellSize;
    this.width         = width;
    this.height        = height;
    this.rows          = [];
    for (var i = 0; i < height; ++i) {
        var newRow = new CMapRow (width, i, this);

        this.rows.push (newRow);
    }
    if (this.game.mapWindow) {
        this.game.mapWindow.resize (Math.min (window.innerWidth - 20, width * this.game.cellSize), Math.min (window.innerHeight - 60, height * this.game.cellSize));
    }
};
CMap.getDefaultMapWidth = function () {
    var w = window.innerWidth;

    w *= 0.98;
    w = parseInt (w / this.game.cellSize);
    
    return w;
};
CMap.getDefaultMapHeight = function () {
    var h = window.innerHeight;

    h *= 0.98;
    h = parseInt (h / this.game.cellSize);
    
    return h;
};
CMap.prototype.renderTiles = function (pos, tiles) {
    var xx      = pos.x << 6;
    var yy      = pos.y << 6;
    var missing = false;

    for (var ii = 0; ii < 3; ++ii) {
        if (tiles[ii] == 0) {
            continue;
        }
        var img = this.getImageForTile (tiles[ii]);

        if (img) {
            this.ctx.drawImage (img, xx, yy);
        } else {
            missing = true;
        }
    }
    if (missing) {
        var self = this;

        setTimeout ((function (tilesx, posx) {
            return function () {
                self.renderTiles (posx, tilesx);
            };
        }) (tiles, pos), 300);
    }
};
CMap.prototype.clearRect = function (pos) {
    this.ctx.clearRect (pos.x * this.game.cellSize, pos.y * this.game.cellSize, this.game.cellSize, this.game.cellSize);
};
CMap.prototype.get = function (x, y) {
    return this.rows[y].cells[x];
};
CMap.prototype.modifyScroll = function (x, y) {
    this.dx += x;
    this.dy += y;
};
CMap.prototype.setScroll = function (x, y) {
    this.dx = x;
    this.dy = y;
};
CMap.prototype.clear = function () {
    this.ctx.clearRect (0, 0, this.width * this.game.cellSize, this.heigth * this.game.cellSize);
    for (var i = 0; i < this.rows.length; ++i) {
        this.rows[i].clear ();
    }
};
CMap.prototype.renderDarkness = function (/* darkness, pos */) {
    /* if (darkness == 0){
        darkness = 255;
    }
    this.ctx.fillStyle = "rgba(255,0,0," + (1-((255-darkness)/255)) + ")";
    //console.log(pos,(darkness/255));
    this.ctx.fillRect(pos.x << this.game.cellSize, pos.y << this.game.cellSize,this.game.cellSize,this.game.cellSize);
      */
};
CMap.prototype.scroll = function () {
    var tmpDx = this.dx;
    var tmpDy = this.dy;
    var j;
    var i = 0;
    /*
    this.game.mapInfo.x += tmpDx;
    this.game.mapInfo.y += tmpDy;
    */

    if (parseInt (tmpDy) != 0) {
        i = 0;

        while (i < tmpDy) {
            /* var c = $_ ('canvas');
            c.width  = this.canvas.width;
            c.height = this.canvas.height;
            var ctx = c.getContext ("2d");
            ctx.drawImage (this.canvas, 0, 0);
            this.ctx.drawImage (c, 0, -this.game.cellSize);
            */
            var oldFirst = this.rows.shift ();

            oldFirst.clear ();
            this.rows.push (oldFirst);
            ++i;
        }
        i = 0;
        while (tmpDy < i) {
            /* var c = $_ ('canvas');
            c.width  = this.canvas.width;
            c.height = this.canvas.height;
            var ctx = c.getContext ("2d");
            ctx.drawImage (this.canvas, 0, 0);
            this.ctx.drawImage (c, 0, this.game.cellSize);
            */
            var oldLast = this.rows.pop ();

            oldLast.clear ();
            this.rows.unshift (oldLast);
            --i;
        }
    }
    if (tmpDx != 0) {
        i = 0;
        while (i < tmpDx) {
            /* var c = $_ ('canvas');
            c.width  = this.canvas.width;
            c.height = this.canvas.height;
            var ctx = c.getContext ("2d");
            ctx.drawImage (this.canvas, 0, 0);
            this.ctx.drawImage (c, -this.game.cellSize, 0);
            */
            for (j = 0; j < this.rows.length; ++j) {
                r = this.rows[j];
                r.push (r.shift ().clear ());
            }
            i++;
        }
        i = 0;
        while (tmpDx < i) {
            /* var c = $_ ('canvas');
            c.width  = this.canvas.width;
            c.height = this.canvas.height;
            var ctx = c.getContext ("2d");
            ctx.drawImage (this.canvas, 0, 0);
            this.ctx.drawImage (c, this.game.cellSize, 0);
            */
            for (j = 0; j < this.rows.length; ++j) {
                r = this.rows[j];
                r.unshift (r.pop ().clear ());
            }
            i--;
        }
    }
    this.setScroll (0, 0);
};
CMap.prototype.getImageForTile = function (id) {
    var o = this.game.faces.find (id);

    if (o) {
        var img;

        if ((img = o.img) == null) {
            this.game.findFace (id);
            
            return null;
        } else {
            return img;
        }
    }
    
    return null;
};
CMap.prototype.mergeTilesForMiniMap = function (src, dst) {
    if (!dst) {
        dst = src.concat ([]);
        
        return dst;
    }
    for (var i = 0; i < dst.length; ++i) {
        var sum = 0;

        for (var j = 0; j < src.length; ++j) {
            sum += src[j];
        }
        if (sum)
            dst[i] = src[i];
    }
    
    return dst;
};
CMap.prototype.draw = function (ctx) {
    ctx = ctx || this.game.mapWindow.ctx; // draw to windowMap if ctx not specified
    var x,
        y,
        cell,
        rect;
    // ctx.drawImage(this.canvas, 0, 0);
    // return;

    ctx.clearRect (0, 0, this.width * this.game.cellSize, this.height * this.game.cellSize);
    ctx.save ();
    var dx,
        dy;

    ctx.translate (dx = parseInt ((this.game.mapWindow.width - this.width * this.game.cellSize) * 0.5), dy = parseInt ((this.game.mapWindow.height - this.height * this.game.cellSize) * 0.5));
    // console.log((this.game.mapWindow.width - this.width*this.game.cellSize)*0.5, (this.game.mapWindow.height - this.height*this.game.cellSize)*0.5);
    /* var mapData = this.game.miniMap.data[this.game.mapInfo.name] || {};
    this.game.miniMap.data[this.game.mapInfo.name] = mapData;
    */
    if (!this.mapIntersect) {
        this.mapIntersect = {
            x1: 0,
            y1: 0,
            x2: 0,
            y2: 0,
        };
        var mapWindowRect = {
            left:   0,
            top:    0,
            right:  this.game.mapWindow.width,
            bottom: this.game.mapWindow.height
        };

        for (x = 0; x < this.width; ++x) {
            rect = {
                left:   x * this.game.cellSize + dx,
                right:  x * this.game.cellSize + dx + this.game.cellSize,
                top:    this.height * this.game.cellSize * 0.5 + dy,
                bottom: this.height * this.game.cellSize * 0.5 + dy + this.game.cellSize,
            };

            if (intersect (rect, mapWindowRect)) {
                this.mapIntersect.x1 = x;
                break;
            }
        }
        for (x = this.width - 1; x >= 0; --x) {
            rect = {
                left:   x * this.game.cellSize + dx,
                right:  x * this.game.cellSize + dx + this.game.cellSize,
                top:    this.height * this.game.cellSize * 0.5 + dy,
                bottom: this.height * this.game.cellSize * 0.5 + dy + this.game.cellSize,
            };

            if (intersect (rect, mapWindowRect)) {
                this.mapIntersect.x2 = x;
                break;
            }
        }
        for (y = 0; y < this.height; ++y) {
            rect = {
                left:   this.mapIntersect.x1 * this.game.cellSize + dx,
                right:  this.mapIntersect.x1 * this.game.cellSize + dx + this.game.cellSize,
                top:    y * this.game.cellSize + dy,
                bottom: y * this.game.cellSize + dy + this.game.cellSize,
            };

            if (intersect (rect, mapWindowRect)) {
                this.mapIntersect.y1 = y;
                break;
            }
        }
        for (y = this.height - 1; y >= 0; --y) {
            rect = {
                left:   this.mapIntersect.x1 * this.game.cellSize + dx,
                right:  this.mapIntersect.x1 * this.game.cellSize + dx + this.game.cellSize,
                top:    y * this.game.cellSize + dy,
                bottom: y * this.game.cellSize + dy + this.game.cellSize,
            };

            if (intersect (rect, mapWindowRect)) {
                this.mapIntersect.y2 = y;
                break;
            }
        }
    }
    var missing = false;

    for (x = this.mapIntersect.x1; x <= this.mapIntersect.x2; ++x) {
        for (y = this.mapIntersect.y1; y <= this.mapIntersect.y2; ++y) {
            cell = this.rows[y].cells[x];
            /* var miniMapX = x + this.game.mapInfo.x;
            var miniMapY = y + this.game.mapInfo.y;
            mapData[miniMapX + "x" + miniMapY] = this.mergeTilesForMiniMap(cell.tiles.concat([]),mapData[miniMapX + "x" + miniMapY]);            */

            for (z = 0; z < cell.tiles.length; ++z) {
                if (cell.tiles[z]) {
                    // console.log(cell);
                    var img = this.getImageForTile (cell.tiles[z]);

                    if (img != null) {
                        ctx.drawImage (img, x * this.game.cellSize, y * this.game.cellSize, this.game.cellSize, this.game.cellSize);
                    } else {
                        missing = true;
                    }
                    var hp = cell.stat_hp;

                    if (hp && hp != 0 && hp <= 255) {
                        hp            = 255 - Math.min (255, hp);
                        ctx.fillStyle = "red";
                        ctx.fillRect (x * this.game.cellSize, y * this.game.cellSize, (hp / 255) * this.game.cellSize, 3);
                    }
                }
            }
        }
    }
    var self = this;

    if (missing == true) {
        setTimeout (function () {
            self.draw (ctx);
        }, 200);
    } else {
        /* this.game.miniMap.canvas.width = this.game.miniMap.width;
        this.game.miniMap.canvas.heigth = this.game.miniMap.height;
         
        this.game.miniMap.draw();
        var ctx2 = this.game.miniMap.ctx;
        var centerX = this.game.miniMap.canvas.width*0.5;
        var centerY = this.game.miniMap.canvas.height*0.5;
        for (var s in mapData) {
            var tiles = mapData[s];
            
            var t = s.split("x");
            var x = t.shift()<<0;
            x-=this.game.mapInfo.x;
            var y = t.shift()<<0;
            y-=this.game.mapInfo.y;
            
            x+=centerX>>3;
            y+=centerY>>3;
            x<<=0;
            y<<=0;
            
            for (var z = 0;z<tiles.length;++z) {
                if (!tiles[z]) {
                    continue;
                }
                var img = this.getImageForTile (tiles[z]);
                
                ctx2.drawImage (img, x*8,y*8,8,8);
            }
        }*/
    }
    ctx.restore ();
};

