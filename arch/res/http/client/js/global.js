/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marc A. Lehmann <schmorp@schmorp.de>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

// global functionality and variables for use by all files

var server_host; // host:port for the server connection(s)
var server_url;  // base server url + /, e.g. "http://xyz:1234/"
var face_url;    // base url for faces, currently same as server_url

function __(str) { return str; }
