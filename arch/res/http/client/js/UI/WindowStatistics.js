/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CWindowStatistics (x, y, game, options) {
    options              = options || {};
    options.closable     = true;
    options.resizeable   = false;
    options.resizeCanvas = true;
    options.noTitleBar   = false;
    CWindow.call (this, x, y, 550, 399, game, options);
    this.appendTo.removeChild (this.canvas);
    var groups = this.groups = [
        {
            title: "Player",
            items: [
                {
                    text:      "Title",
                    attribute: CS_STAT_TITLE,
                    cell:      null
                },
                {
                    text:      "Max Weight",
                    attribute: CS_STAT_WEIGHT_LIM,
                    cell:      null,
                    formatter: function (v) {
                        return v / 1000;
                    }
                },
            ]
        },
        {
            title: "Primary/Secondary Statistics",
            prefix: "stat_",
            items: [
                {
                    text:      "Str",
                    attribute: CS_STAT_STR,
                    cell:      null
                },
                {
                    text:      "Wc",
                    attribute: CS_STAT_WC,
                    cell:      null
                },
                {
                    text:      "Dex",
                    attribute: CS_STAT_DEX,
                    cell:      null
                },
                {
                    text:      "Ac",
                    attribute: CS_STAT_AC,
                    cell:      null
                },
                {
                    text:      "Con",
                    attribute: CS_STAT_CON,
                    cell:      null
                },
                {
                    text:      "Dam",
                    attribute: CS_STAT_DAM,
                    cell:      null
                },
                {
                    text:      "Int",
                    attribute: CS_STAT_INT,
                    cell:      null
                },
                {
                    text:      "Arm",
                    attribute: CS_STAT_RES_PHYS,
                    cell:      null
                },
                {
                    text:      "Wis",
                    attribute: CS_STAT_WIS,
                    cell:      null
                },
                {
                    text:      "Spd",
                    attribute: CS_STAT_SPEED,
                    cell:      null
                },
                {
                    text:      "Pow",
                    attribute: CS_STAT_POW,
                    cell:      null
                },
                {
                    text:      "Wsp",
                    attribute: CS_STAT_WEAP_SP,
                    cell:      null
                },
                {
                    text:      "Cha",
                    attribute: CS_STAT_CHA,
                    cell:      null
                },
            ]
        },
        {
            title: "Resistances",
            cols:  3,
            prefix: "res_",
            items: [
                {
                    text:      "slow",
                    attribute: CS_STAT_RES_SLOW,
                    cell:      null
                },
                {
                    text:      "drain",
                    attribute: CS_STAT_RES_DRAIN,
                    cell:      null
                },
                {
                    text:      "blind",
                    attribute: CS_STAT_RES_BLIND,
                    cell:      null
                },
                {
                    text:      "holyw",
                    attribute: CS_STAT_RES_HOLYWORD,
                    cell:      null
                },
                {
                    text:      "acid",
                    attribute: CS_STAT_RES_ACID,
                    cell:      null
                },
                {
                    text:      "fear",
                    attribute: CS_STAT_RES_FEAR,
                    cell:      null
                },
                {
                    text:      "conf",
                    attribute: CS_STAT_RES_CONF,
                    cell:      null
                },
                {
                    text:      "pois",
                    attribute: CS_STAT_RES_POISON,
                    cell:      null
                },
                {
                    text:      "tund",
                    attribute: CS_STAT_RES_ACID,
                    cell:      null
                },
                {
                    text:      "fire",
                    attribute: CS_STAT_RES_FIRE,
                    cell:      null
                },
                {
                    text:      "para",
                    attribute: CS_STAT_RES_PARA,
                    cell:      null
                },
                {
                    text:      "elec",
                    attribute: CS_STAT_RES_ELEC,
                    cell:      null
                },
                {
                    text:      "depl",
                    attribute: CS_STAT_RES_DEPLETE,
                    cell:      null
                },
                {
                    text:      "deat",
                    attribute: CS_STAT_RES_DEATH,
                    cell:      null
                },
                {
                    text:      "cold",
                    attribute: CS_STAT_RES_COLD,
                    cell:      null
                },
                {
                    text:      "magic",
                    attribute: CS_STAT_RES_MAG,
                    cell:      null
                },
                {
                    text:      "phys",
                    attribute: CS_STAT_RES_PHYS,
                    cell:      null
                },
                {
                    text:      "ghit",
                    attribute: CS_STAT_RES_GHOSTHIT,
                    cell:      null
                },
            ]
        },
    ];

    for (var i = 0; i < groups.length; ++i) {
        this.renderGroup (groups[i]);
    }
}
CWindowStatistics.prototype = Object.create (CWindow.prototype);
var $ = CWindowStatistics.prototype;

$.constructor = CWindowStatistics;
$.renderGroup = function (group) {
    this.appendTo.appendChild ($_ ("h4", {
        innerHTML: group.title
    }));
    var table = this.appendTo.appendChild ($_ ("table", {
        className: "statistics"
    }));
    var cols = group.cols || 2;
    var tr;
    var names = { "slow":"Slow","holyw":"Holy Word","conf":"Confusion","fire":"Fire","depl":"Depletion","magic":"Magic","drain":"Draining","acid":"Acid","pois":"Poison","para":"Paralysation","deat":"Death","phys":"Physical","blind":"Blind","fear":"Fear","tund":"Turn undead","elec":"Electricity","cold":"Cold","ghit":"Ghost hit" };
    
    for (var i = 0; i < group.items.length; ++i) {
        if (i % cols == 0) {
            tr = table.appendChild ($_ ("tr"));
        }
        var tdDesc = tr.appendChild ($_ ("td"));
        var name = names[group.items[i].text] || group.items[i].text;

        this.game.tooltip.register (tdDesc, "#" + group.prefix + group.items[i].text);
        tdDesc.innerHTML = name;
        var tdCheckbox = tr.appendChild ($_ ("td"));

        group.items[i].cell = tdCheckbox.appendChild ($_ ("span", {
            innerHTML: this.game.myStats[group.items[i].attribute]
        }));
    }
};
$.updateGroup = function (group) {
    for (var i = 0; i < group.items.length; ++i) {
        group.items[i].cell.innerHTML = this.game.myStats[group.items[i].attribute];
    }
};
$.draw = function () {
    for (var i = 0; i < this.groups.length; ++i) {
        this.updateGroup (this.groups[i]);
    }
};
$.show = function (b) {
    CWindow.prototype.show.call (this, b);
    this.draw ();
};

