/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CWindowSettings (x, y, game, options) {
    options              = options || {};
    options.resizeable   = false;
    options.resizeCanvas = true;
    options.noTitleBar   = true;
    CWindow.call (this, x, y, 80, 25, game, options);
    this.appendTo.removeChild (this.canvas);
    var btn = this.appendTo.appendChild ($_ ("input", {
        type:  "button",
        value: "Menu"
    }));
    var self = this;

    btn.addEventListener ("click", function (e) {
        self.onClick (e);
    });
}
CWindowSettings.prototype = Object.create (CWindow.prototype);
var $ = CWindowSettings.prototype;

$.constructor = CWindowSettings;
$.onClick     = function (e) {
    var cm = this.game.contextMenu;

    cm.itemsSettings ();
    cm.move (e.pageX - 5, e.pageY - 5);
    cm.show (true);
};
