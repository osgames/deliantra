/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *  Copyright (©) 2017 Marc A. Lehmann <schmorp@schmorp.de>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

var default_server = "gameserver.deliantra.net:13327";

function CWindowLogin (x, y, w, h, game, options) {
    CWindow.call (this, x, y, w, h, game, options);
    var self = this;

    this.activeChannel = 0;
    this.appendTo.removeChild (this.canvas);
    this.div.id = "loginContainer";
    var header = this.appendTo.appendChild ($_ ("h1"));

    header.appendChild (document.createTextNode ("D E L I A N T R A"));
    var table = this.table2 = this.appendTo.appendChild ($_ ("table"));

    table.className = "login";
    // console.log (table.className);
    var tr         = this.table2.appendChild ($_ ("tr"));
    var td         = tr.appendChild ($_ ("td"));
    var labelLogin = td.appendChild ($_ ("h3"));

    labelLogin.innerHTML = "LOGIN";
    td                   = tr.appendChild ($_ ("td"));
    this.inputLogin      = td.appendChild ($_ ("input", {
        type: "text"
    }));
    this.inputLogin.focus ();
    tr = this.table2.appendChild ($_ ("tr"));
    td = tr.appendChild ($_ ("td"));
    var labelPassword = td.appendChild ($_ ("h3"));

    labelPassword.innerHTML = "PASSWORD";
    td                      = tr.appendChild ($_ ("td"));
    this.inputPassword      = td.appendChild ($_ ("input", {
        type: "password"
    }));
    var s = ["VOID", "null", "NULL"][parseInt (Math.random () * 3)];

    this.inputPassword.value = this.inputLogin.value = s;
    if (!this.nostorage) {
        // this.inputLogin.value = localStorage.getItem('login');
        // this.hash = localStorage.getItem('hash');
    }
    tr = this.table2.appendChild ($_ ("tr"));
    td = tr.appendChild ($_ ("td"));
    var labelServer = td.appendChild ($_ ("h3"));

    labelServer.innerHTML = "SERVER";
    td                    = tr.appendChild ($_ ("td"));
    this.inputServer      = td.appendChild ($_ ("input", {
        type: "text"
    }));
    this.inputServer.value = document.location.host || default_server;
    var inputClear = td.appendChild ($_ ("input"));

    inputClear.type        = "button";
    inputClear.value       = "x";
    inputClear.style.width = "10px";
    inputClear.addEventListener ("click", function () {
        self.inputServer.value = "";
    });
    tr         = this.table2.appendChild ($_ ("tr"));
    td         = tr.appendChild ($_ ("td"));
    td.colspan = 2;
    var inputGO = this.appendTo.appendChild ($_ ("input"));

    inputGO.type        = "button";
    inputGO.value       = "GO";
    this.game.inputText = true;
    inputGO.addEventListener ("click", function () {
        self.onGOClick ();
    });
    [this.inputPassword, this.inputLogin, this.inputServer].each (function (o) {
        o.addEventListener ("keypress", function (e) {
            if (e.keyCode == CKeyboard.KEYS.ENTER)
                self.onGOClick ();
        });
    });
}

CWindowLogin.prototype             = Object.create (CWindow.prototype);
CWindowLogin.prototype.constructor = CWindowLogin;
CWindowLogin.prototype.onGOClick   = function () {
    if (this.inputLogin.length * this.inputPassword.length == 0) {
        return;
    }
    this.show (false);

    if (!this.nostorage)
        localStorage.setItem ("login", this.inputLogin.value);

    server_host = this.inputServer.value;

    if (server_host.length) {
        if (!this.inputServer.value.match (/:[1-9][0-9]*/)) {
            this.inputServer.value += ":13327";
        }
    } else {
        server_host = default_server;
    }

    server_url = "http://" + server_host + "/";
    face_url   = server_url;

    this.game.start ();
};

