/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CWindowSelectAmount (x, y, game, options) {
    options              = options || {};
    options.resizeable   = false;
    options.resizeCanvas = true;
    options.noTitleBar   = false;
    CWindow.call (this, x, y, 200, 19, game, options);
    this.xx      = 0;
    this.min     = 0;
    this.n       = 133;
    this.current = 0;
    /*
    this.canvas.height = this.height - 15;
    this.canvas.addEventListener('mousedown', function (e) {
        this.selIsDragging = true;
        this.selStartDrag = e;
        this.xx = e.offsetX;
    }.bind(this));
    this.canvas.addEventListener('mouseup', function (e) {
        this.selIsDragging = false;
        this.selStartDrag = null;
    }.bind(this));
    this.canvas.addEventListener('mousemove', function (e) {
        if (!this.selIsDragging) {
            return;
        }
        this.xx = Math.min(e.offsetX - this.selStartDrag.offsetX,this.width-20);
        this.current = Math.min(( (this.xx/(this.width-20))*this.n)<<0,this.n);
        this.inputBox.value = this.current;
        this.draw();
        
    }.bind(this));
    */
    this.appendTo.removeChild (this.canvas);
    this.inputBox = this.appendTo.appendChild ($_ ("input"));
    var self = this;

    function f () {
        this.value = this.value.replace (/[^0-9]*/g, "");
        this.value = Math.min (Math.max (1, isNaN (parseInt (this.value)) ? 1 : parseInt (this.value)), self.n);
    }
    this.inputBox.addEventListener ("keydown", f);
    this.inputBox.addEventListener ("keyup", f);
    this.inputBox.addEventListener ("keypress", f);
    this.inputBox.addEventListener ("focus", function () {
        this.game.inputText = true;
    }.bind (this));
    this.inputBox.addEventListener ("blur", function () {
        this.game.inputText = false;
    }.bind (this));
    this.inputBox.value       = 0;
    this.inputOk              = this.appendTo.appendChild ($_ ("input"));
    this.inputOk.type         = "button";
    this.inputOk.value        = "ok";
    this.inputBox.style.width = "30px";
    this.inputCancel          = this.appendTo.appendChild ($_ ("input"));
    this.inputCancel.type     = "button";
    this.inputCancel.value    = "cancel";
    this.inputCancel.addEventListener ("click", function () {
        this.show (false);
        this.isCancelled = true;
        this.cb ();
    }.bind (this));
    this.inputOk.addEventListener ("click", function () {
        this.current = parseInt (this.inputBox.value);
        this.show (false);
        this.cb ();
    }.bind (this));
}
CWindowSelectAmount.prototype = Object.create (CWindow.prototype);
var $ = CWindowSelectAmount.prototype;

$.constructor = CWindowSelectAmount;
$.draw        = function () {
    /*
    CWindow.prototype.draw.apply(this);
    this.ctx.fillStyle= 'red';
    this.ctx.fillRect(this.xx,0,20,20);
    */
};
$.show = function (b) {
    CWindow.prototype.show.call (this, b);
    this.draw ();
    this.inputBox.value = 1;
    this.inputBox.focus ();
    this.inputBox.select ();
    this.isCancelled = false;
};

