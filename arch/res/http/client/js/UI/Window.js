/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CWindow (x, y, w, h, game, options) {
    options        = options || {};
    this.options   = options;
    this.game      = game;
    this.nostorage = false;
    this.title     = options.title || this.constructor.name.replace ("CWindow", "");
    var self = this;

    try {
        localStorage.getItem ("window_" + this.constructor.name);
    /*
     var storedSettings = localStorage.getItem ("window_" + this.constructor.name);
     if (storedSettings) {
        storedSettings = JSON.parse (storedSettings);
        x              = parseInt (storedSettings.x);
        y              = parseInt (storedSettings.y);
    }*/
    } catch (e) {
        this.nostorage = true;
    }
    this.div = document.body.appendChild ($_ ("div", {
        className: "windowContainer"
    }));
    unselectable (this.div);
    this.bordersDiv = this.div.appendChild ($_ ("div", {
        className: "borders"
    }));
    if (!options.noTitleBar) {
        this.titleDiv = this.bordersDiv.appendChild ($_ ("div", {
            className: "titlebar"
        }));
        this.titleDiv.appendChild (document.createTextNode (this.title));
        this.titleDiv.addEventListener ("mousedown", function (e) {
            self.startDragging (e);
        });
        if (self.game.tooltip) {
            self.game.tooltip.register (this.titleDiv, "click and drage to move this window");
        }
    }
    this.containerDiv = this.bordersDiv.appendChild ($_ ("div", {
        className: "windowContent"
    }));
    this.appendTo = this.containerDiv;
    
    if (options.closable) {
        this.closeBtn = (this.titleDiv || this.appendTo).appendChild ($_ ("input", {
            type:      "button",
            value:     "x",
            className: "closeBtn"
        }));
        this.closeBtn.style.zIndex = 1000;
        this.closeBtn.addEventListener ("click", function () {
            self.show (false);
        });
        self.game.tooltip.register (this.closeBtn, "Click me to close this button.");
    }
    this.canvas = this.appendTo.appendChild ($_ ("canvas"));
    this.ctx    = this.canvas.getContext ("2d");
    this.resize (w, h);
    this.move (this.x = x, this.y = y);
    if (!CWindow.globalHandler) {
        CWindow.globalHandler = true;
        window.addEventListener ("mousemove", function (e) {
            if (!CWindow.globalCurrentWindow || !CWindow.globalCurrentWindow.startDrag) {
                return;
            }
            CWindow.globalCurrentWindow.drag (e);
        });
        window.addEventListener ("mouseup", function (e) {
            if (!CWindow.globalCurrentWindow) {
                return;
            }
            CWindow.globalCurrentWindow.endDrag (e);
        });
    }
    function fborders (e) {
        var x          = e.offsetX;
        var y          = e.offsetY;
        var cursor     = "auto";
        var borderSize = 5;
        var dimensions = this.getBoundingClientRect ();
        // console.log(self.constructor.name.toString(),x,y,dimensions);

        if (x < borderSize) {
            if (y < borderSize) {
                cursor = "nw-resize";
            } else if (y > dimensions.height - borderSize) {
                cursor = "sw-resize";
            } else {
                cursor = "w-resize";
            }
        } else if (x > dimensions.width - borderSize) {
            if (y < borderSize) {
                cursor = "ne-resize";
            } else if (y > dimensions.height - borderSize) {
                cursor = "se-resize";
            } else {
                cursor = "e-resize";
            }
        } else {
            if (y < borderSize) {
                cursor = "n-resize";
            } else if (y > dimensions.height - borderSize) {
                cursor = "s-resize";
            } else {
                cursor = "move";
            }
        }
        this.style.cursor = cursor;
    }

    this.bordersDiv.addEventListener ("mouseover", fborders);
    this.bordersDiv.addEventListener ("mousemove", fborders);
    this.bordersDiv.addEventListener ("mousedown", function (e) {
        if (this.cursor != "move") {
            self.startResizing (e);
        }
    });
}
CWindow.globalHandler       = false;
CWindow.globalCurrentWindow = null;

Object.defineProperty (CWindow.prototype, "width", {
    set: function (v) {
        v <<= 0;
        this._width                   = v;
        this.containerDiv.style.width = this._width + "px";
    },
    get: function () {
        return this._width;
    }
});
Object.defineProperty (CWindow.prototype, "height", {
    set: function (v) {
        v <<= 0;
        this._height                   = v;
        this.containerDiv.style.height = (this._height + (this.options.noTitleBar ? 0 : 42)) + "px";
    },
    get: function () {
        return this._height;
    }
});
Object.defineProperty (CWindow.prototype, "x", {
    set: function (v) {
        v                   = parseInt (v);
        this._x             = v;
        this.div.style.left = (this._x) + "px";
    },
    get: function () {
        return this._x;
    }
});
Object.defineProperty (CWindow.prototype, "y", {
    set: function (v) {
        v                  = parseInt (v);
        this._y            = v;
        this.div.style.top = (this._y) + "px";
    },
    get: function () {
        return this._y;
    }
});
Object.defineProperty (CWindow.prototype, "visible", {
    set: function (v) {
        this.show (v);
        this._visible = v;
    },
    get: function () {
        return this._visible;
    }
});
CWindow.prototype.resize = function (w, h) {
    w           = Math.min (1999, Math.max (10, w));
    h           = Math.min (1999, Math.max (10, h));
    this.width  = w;
    this.height = h;
    if (!this.options.resizeCanvas === false) {
        this.canvas.width  = w;
        this.canvas.height = h;
    }
};
CWindow.prototype.draw = function () {
    this.ctx.clearRect (0, 0, this.canvas.width, this.canvas.height);
};
CWindow.prototype.show = function (b) {
    this._visible = !!b;
    this.div.style.display = b ? "block" : "none";
};
CWindow.prototype.isShown = function () {
    return this.div.style.display == "block";
};
CWindow.prototype.toggleShow = function () {
    this.show (!this.isShown ());
};

function dist (p, e) {
    return Math.sqrt ((p.x - e.y) * (p.x - e.y) + (p.y - e.y) * (p.y - e.y));
}
CWindow.prototype.cancelDrag = function () {
    this.startDrag              = null;
    this.isResize               = false;
    CWindow.globalCurrentWindow = null;
};
CWindow.prototype.startResizing = function (e) {
    if (this.startDrag) {
        return;
    }
    this.isResize               = true;
    this.startDrag              = e;
    this.currentDrag            = e;
    this.resizeMode             = this.bordersDiv.style.cursor;
    CWindow.globalCurrentWindow = this;
};
CWindow.prototype.startDragging = function (e) {
    this.resizeMode             = "move";
    this.isResize               = false;
    this.startDrag              = e;
    this.currentDrag            = e;
    CWindow.globalCurrentWindow = this;
};
CWindow.prototype.endDrag = function () {
    var w = this;

    w.isResize                  = false;
    w.startDrag                 = null;
    CWindow.globalCurrentWindow = null;
    if (this.nostorage == false) {
        var settingName  = "window_" + w.constructor.name;
        var settingValue = JSON.stringify ({
            x: w.x,
            y: w.y,
            w: w.width,
            h: w.height
        });

        localStorage.setItem (settingName, settingValue);
    }
};
CWindow.prototype.drag = function (e) {
    switch (this.resizeMode) {
        case "se-resize":
            this.resize (this.width + (e.clientX - this.currentDrag.clientX), this.height + (e.clientY - this.currentDrag.clientY));
            break;
        case "s-resize":
            this.resize (this.width, this.height + (e.clientY - this.currentDrag.clientY));
            break;
        case "sw-resize":
            this.resize (this.width - (e.clientX - this.currentDrag.clientX), this.height + (e.clientY - this.currentDrag.clientY));
            this.move (this.x + (e.clientX - this.currentDrag.clientX), this.y);
            break;
        case "e-resize":
            this.resize (this.width + (e.clientX - this.currentDrag.clientX), this.height);
            break;
        case "w-resize":
            this.resize (this.width - (e.clientX - this.currentDrag.clientX), this.height);
            this.move (this.x + (e.clientX - this.currentDrag.clientX), this.y);
            break;
        case "nw-resize":
            this.resize (this.width - (e.clientX - this.currentDrag.clientX), this.height - (e.clientY - this.currentDrag.clientY));
            this.move (this.x + (e.clientX - this.currentDrag.clientX), this.y + (e.clientY - this.currentDrag.clientY));
            break;
        case "n-resize":
            this.resize (this.width, this.height - (e.clientY - this.currentDrag.clientY));
            this.move (this.x, this.y + (e.clientY - this.currentDrag.clientY));
            break;
        case "ne-resize":
            this.resize (this.width + (e.clientX - this.currentDrag.clientX), this.height - (e.clientY - this.currentDrag.clientY));
            this.move (this.x, this.y + (e.clientY - this.currentDrag.clientY));
            break;
        default:
            if (this.isResize == false) {
                this.move (this.x + (e.clientX - this.currentDrag.clientX), this.y + (e.clientY - this.currentDrag.clientY));
            }
            break;
    }
    this.currentDrag = e;
    this.draw ();
};
CWindow.prototype.center = function () {
    this.move (window.innerWidth * 0.5 - this.width * 0.5, window.innerHeight * 0.5 - this.height * 0.5);
};
CWindow.prototype.move = function (x, y) {
    this.x = x;
    this.y = y;
};

