/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CWindowBars (x, y, w, h, game, options) {
    options            = options || {};
    options.closable   = true;
    options.noTitleBar = true;
    CWindow.call (this, game.mapWindow.width - w, game.mapWindow.height - h + game.mapWindow.y, w, h, game, options);
    var cssProperties = getStyleRuleValue (["color", "background-color", "left", "top", "width"], ".hpbar");

    this.hpBar            = new CProgressBar (this, cssProperties.color, cssProperties["background-color"], parseInt (cssProperties.width));
    this.hpBar.rotation   = Math.PI + Math.PI * 0.5;
    this.hpBar.x          = parseInt (cssProperties.left);
    this.hpBar.y          = parseInt (cssProperties.top);
    cssProperties         = getStyleRuleValue (["color", "background-color", "left", "top", "width"], ".manaBar");
    this.mpBar            = new CProgressBar (this, cssProperties.color, cssProperties["background-color"], parseInt (cssProperties.width));
    this.mpBar.rotation   = this.hpBar.rotation;
    this.mpBar.x          = parseInt (cssProperties.left);
    this.mpBar.y          = parseInt (cssProperties.top);
    cssProperties         = getStyleRuleValue (["color", "background-color", "left", "top", "width"], ".foodBar");
    this.foodBar          = new CProgressBar (this, cssProperties.color, cssProperties["background-color"], parseInt (cssProperties.width));
    this.foodBar.rotation = this.mpBar.rotation;
    this.foodBar.x        = parseInt (cssProperties.left);
    this.foodBar.y        = parseInt (cssProperties.top);
}
CWindowBars.prototype             = Object.create (CWindow.prototype);
CWindowBars.prototype.constructor = CWindowBars;
CWindowBars.prototype.draw        = function () {
    CWindow.prototype.draw.apply (this);
    this.game.ctx.fillStyle = "white";
    this.game.ctx.font      = "20px monospace";
    this.game.rangeCombatWindow.setText (this.game.myStats[CS_STAT_RANGE]);
    this.hpBar.currentVal = this.game.myStats[CS_STAT_HP];
    this.hpBar.maxVal     = this.game.myStats[CS_STAT_MAXHP];
    this.hpBar.draw (this.ctx);
    this.mpBar.currentVal = this.game.myStats[CS_STAT_SP];
    this.mpBar.maxVal     = this.game.myStats[CS_STAT_MAXSP];
    this.mpBar.draw (this.ctx);
    this.foodBar.currentVal = this.game.myStats[CS_STAT_FOOD];
    this.foodBar.maxVal     = 999;
    this.foodBar.draw (this.ctx);
};

