/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CWindowPickup (x, y, game, options) {
    options              = options || {};
    options.closable     = true;
    options.resizeable   = false;
    options.resizeCanvas = true;
    options.noTitleBar   = false;
    CWindow.call (this, x, y, 450, 399, game, options);
    /*
    PICKUP_NOTHING = 0x00000000,

    PICKUP_DEBUG   = 0x10000000,
    PICKUP_INHIBIT = 0x20000000,
    PICKUP_STOP    = 0x40000000,
    PICKUP_NEWMODE = 0x80000000,

    PICKUP_RATIO = 0x0000000F,
    */
    var pickupFlagNames = ["PICKUP_FOOD", "PICKUP_DRINK", "PICKUP_VALUABLES", "PICKUP_BOW", "PICKUP_ARROW", "PICKUP_HELMET", "PICKUP_SHIELD", "PICKUP_ARMOUR", "PICKUP_BOOTS", "PICKUP_GLOVES", "PICKUP_CLOAK", "PICKUP_KEY", "PICKUP_MISSILEWEAPON", "PICKUP_ALLWEAPON", "PICKUP_MAGICAL", "PICKUP_POTION", "PICKUP_SPELLBOOK", "PICKUP_SKILLSCROLL", "PICKUP_READABLES", "PICKUP_MAGIC_DEVICE", "PICKUP_NOT_CURSED", "PICKUP_JEWELS", "PICKUP_FLESH"];
    var table           = this.appendTo.appendChild ($_ ("table", {
        className: "pickup"
    }));

    this.appendTo.removeChild (this.canvas);
    var tr;

    for (var i = 0; i < pickupFlagNames.length; ++i) {
        if (i % 2 == 0) {
            tr = table.appendChild ($_ ("tr"));
        }
        var tdDesc = tr.appendChild ($_ ("td"));

        tdDesc.innerHTML = pickupFlagNames[i].toLowerCase ().replace ("pickup_", "");
        var tdCheckbox = tr.appendChild ($_ ("td"));

        tdCheckbox.appendChild ($_ ("input", {
            type: "checkbox"
        }));
    }
}
CWindowPickup.prototype = Object.create (CWindow.prototype);
var $ = CWindowPickup.prototype;

$.constructor = CWindowPickup;
$.draw        = function () {};
$.show        = function (b) {
    CWindow.prototype.show.call (this, b);
    this.draw ();
};

