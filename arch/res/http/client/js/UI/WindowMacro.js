/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CWindowMacro (x, y, game, options) {
    options              = options || {};
    options.closable     = true;
    options.resizeable   = false;
    options.resizeCanvas = true;
    options.noTitleBar   = false;
    CWindow.call (this, x, y, 800, 600, game, options);
    this.appendTo.removeChild (this.canvas);
    this.game.keyboard.onKeyPressCb.unshift (this.onKeyDown.bind (this));
    var self = this;

    this.macros       = {};
    this.worker       = null;
    this.currentMacro = null;
    var select = this.select = this.appendTo.appendChild ($_ ("select"));

    select.style.width = "100%";
    select.addEventListener ("change", function () {
        self.currentMacro   = this.value;
        self.textArea.value = self.macros[this.value].script;
    });

    select.appendChild ($_ ("option", {
        value:     "empty macro",
        innerHTML: "empty macro"
    }));

    if (!this.nostorage) {
        this.macros = JSON.parse (localStorage.getItem ("macros") || "{}");
        for (var s in this.macros) {
            select.appendChild ($_ ("option", {
                value:     s,
                innerHTML: s
            }));
        // Bubble.add('Added macro: ' + s);
        }
    }

    var ip = this.input = this.appendTo.appendChild ($_ ("input"));

    ip.style.width = "80%";
    var btnAdd = this.appendTo.appendChild ($_ ("input", {
        type:  "button",
        value: "add"
    }));

    btnAdd.addEventListener ("click", function () {
        if (ip.value.length) {
            select.appendChild ($_ ("option", {
                value:     ip.value,
                innerHTML: ip.value,
                selected:  "selected"
            }));
            // Bubble.add('Added macro: ' + ip.value);
            self.macros[ip.value] = {
                script:     "",
                keyBinding: ""
            };
            ip.value = "";
        }
    });

    var te = this.textArea = this.appendTo.appendChild ($_ ("textarea", {
        value: ""
    }));

    te.style.width  = "100%";
    te.style.height = "530px";
    te.value        = ["var interval = setInterval(main, 500);",
        "/*main code logic goes here*/",
        "function main() {\n\n};",
        "/*this will be fired at player inventory update*/",
        "function onInventoryUpdate(){\n\n}",
        "/*this will be fired at secondary container (eg floor) update*/",
        "function onSecondaryContainerUpdate(){\n\n}",
    ].join ("\n");

    var btn = this.appendTo.appendChild ($_ ("input", {
        type:  "button",
        value: "run"
    }));

    btn.addEventListener ("click", function (e) {
        self.onClick (e);
    });

    btn = this.appendTo.appendChild ($_ ("input", {
        type:  "button",
        value: "stop"
    }));

    btn.addEventListener ("click", function () {
        clearInterval (self.interval);
        self.interval = null;
        if (this.worker) {
            this.worker.terminate ();
            this.worker = null;
        }
    });

    btn = this.appendTo.appendChild ($_ ("input", {
        type:  "button",
        value: "close"
    }));

    btn.addEventListener ("click", function () {
        clearInterval (self.interval);
        self.interval = null;
        self.show (false);
    });

    this.keybindBtn = btn = this.appendTo.appendChild ($_ ("input", {
        type:  "button",
        value: "keybind"
    }));

    btn.addEventListener ("click", function () {
        clearInterval (self.interval);
        self.interval = null;
        self.waitForKeyPress ();
    });

    self.isRecording = false;
    this.keybindBtn  = btn = this.appendTo.appendChild ($_ ("input", {
        type:  "button",
        value: "record"
    }));

    btn.addEventListener ("click", function () {
        self.isRecording    = true;
        self.lastRecordTime = (new Date ()).getTime ();
        self.textArea.value = "";
        self.show (false);
    });

    this.keybindBtn = btn = this.appendTo.appendChild ($_ ("input", {
        type:  "button",
        value: "stop record"
    }));

    btn.addEventListener ("click", function () {
        self.isRecording = false;
    });

    this.keybindBtn = btn = this.appendTo.appendChild ($_ ("input", {
        type:  "button",
        value: "help"
    }));

    btn.addEventListener ("click", function () {
        var w = new CWindow (0, 0, 550, 260, game);

        w.appendTo.removeChild (w.canvas);
        var frame = w.appendTo.appendChild ($_ ("iframe", {
            src: "macrohelp.html"
        }));

        frame.style.border = "none";
        frame.style.width  = "100%";
        frame.style.height = "100%";
        frame.style.color  = "white";

        function h () {
            w.show (false);
            window.removeEventListener ("click", h);
        }
        w.center ();
        w.show (true);
        setTimeout (function () {
            window.addEventListener ("click", h);
        }, 100);
    });

    btn = this.appendTo.appendChild ($_ ("input", {
        type:  "button",
        value: "save"
    }));

    btn.addEventListener ("click", function () {
        this.macros[this.select.value] = {
            script:     this.textArea.value,
            keyBinding: this.macros[this.select.value].keyBinding
        };
        if (!this.nostorage) {
            localStorage.setItem ("macros", JSON.stringify (this.macros));
        }
    }.bind (this));

    btn = this.appendTo.appendChild ($_ ("input", {
        type:  "button",
        value: "clear"
    }));

    btn.addEventListener ("click", function () {
        this.macros = {};
        if (!this.nostorage) {
            localStorage.setItem ("macros", "{}");
        }
        while (select.childNodes.length > 1) {
            select.removeChild (select.childNodes[1]);
        }
        te.value = "";
    }.bind (this));
}

CWindowMacro.prototype = Object.create (CWindow.prototype);

var $ = CWindowMacro.prototype;

$.constructor = CWindowMacro;

$.registerWithKeybind = function (name, macro) {
    if (!this.macros[name]) {
        this.macros[name] = {
            script:     macro,
            keyBinding: ""
        };
        this.select.appendChild ($_ ("option", {
            value:     name,
            innerHTML: name,
            selected:  "selected"
        }));
    }
    this.select.value   = name;
    this.textArea.value = macro;
    this.waitForKeyPress ();
};

$.record = function (s) {
    var timeIdle = (new Date ()).getTime () - this.lastRecordTime;

    this.textArea.value += "sleep(" + (timeIdle / 1000) + ");\n";
    this.textArea.value += "cmd(\"" + s.replace ("command ", "") + "\");" + "\n";
    this.lastRecordTime = (new Date ()).getTime ();
};

$.waitForKeyPress = function () {
    var isShift = false;
    var isCtrl  = false;
    var isAlt   = false;
    var current = this.select.value;
    var popup   = new CWindow (0, 0, 200, 100, this.game, {
        resizable:  false,
        noTitleBar: true
    });

    popup.appendTo.removeChild (popup.canvas);
    popup.appendTo.appendChild ($_ ("div", {
        innerHTML: "Press key combination now. <br/> Good key combination is alt+number."
    }));
    popup.center ();
    popup.show (true);
    var keycb = this.keycb = function (e) {
        if (e.keyCode == CKeyboard.KEYS.SHIFT) {
            isShift = true;
            
            return false;
        }
        if (e.keyCode == CKeyboard.KEYS.CTR) {
            isCtrl = true;
            
            return false;
        }
        if (e.keyCode == CKeyboard.KEYS.ALT) {
            isAlt = true;
            
            return false;
        }
        isShift = isShift || e.shiftKey;
        isAlt   = isAlt || e.altKey;
        isCtrl  = isCtrl || e.ctrlKey;
        var s = "";

        if (isShift) {
            s += "{SHIFT} ";
        }
        if (isAlt) {
            s += "{ALT} ";
        }
        if (isCtrl) {
            s += "{CTRL} ";
        }
        s += " " + e.key + " (" + e.keyCode + ")";
        Bubble.add ("pressed : " + s);
        for (var a in this.macros) {
            if (this.macros[a].keyBinding == s) {
                this.macros[a].keyBinding = "";
            }
        }
        this.macros[current].keyBinding = s;
        window.removeEventListener ("keydown", keycb);
        this.show (true);
        popup.show (false);
        popup                    = null;
        this.keybindBtn.disabled = false;
        
        return false;
    }.bind (this);

    this.show (false);
    this.game.inputText = true;
    window.addEventListener ("keydown", keycb);
};

$.show = function (b) {
    CWindow.prototype.show.call (this, b);
    this.game.inputText = b;
};

$.runMacro = function (s) {
    // console.log(s);
    if (this.interval) {
        return;
    }
    var me = this;
    // console.log(s);

    this.interval = true;
    var macroText = s;
    var url       = document.location.href;
    var index     = url.indexOf ("index.html");

    if (index > 0) {
        url = url.substring (0, index);
    }
    var blob = new Blob (["importScripts('" + url + "js/MacroWorkerHelper.js');",
        "onmessage = function(e) { onmessageEx(e); };",
        macroText]);
    var blobURL = window.URL.createObjectURL (blob);
    var worker  = this.worker = new Worker (blobURL);

    worker.onmessage = function (e) {
        me.onWorkerMessage (e.data);
    };
    //    worker.onerror = function(e)
    //    {
    //       console.log(['filename: ', e.filename, ' lineno: ', e.lineno, ' error: ', e.message].join(' '));
    //    };
    this.notifyWorkersOfContainersUpdate ();
};

$.notifyWorkersOfContainersUpdate = function () {
    var worker = this.worker;

    if (!worker) {
        return;
    }
    worker.postMessage ({
        type: "inventory",
        data: this.game.containers.first
    });
    worker.postMessage ({
        type: "secondaryContainer",
        data: this.game.secondaryContainer || this.game.containers.find (0)
    });
};

$.onWorkerMessage = function (data) {
    var me         = this;
    var networking = me.game.networking;

    function moveSouth () {
        networking.send ("command south");
    }

    function moveNorth () {
        networking.send ("command north");
    }

    function moveEast () {
        networking.send ("command east");
    }

    function moveWest () {
        networking.send ("command west");
    }

    function moveSouthEast () {
        networking.send ("command southeast");
    }

    function moveSouthWest () {
        networking.send ("command southwest");
    }

    function moveNorthEast () {
        networking.send ("command northeast");
    }

    function moveNorthWest () {
        networking.send ("command northwest");
    }
    /*
    function lookAtSouth () {
        networking.send ("lookat 0 1");
    }
    function lookAtNorth () {
        networking.send ("lookat 0 -1");
    }
    function lookAtEast () {
        networking.send ("lookat 1 0");
    }
    function lookAtWest () {
        networking.send ("lookat -1 0");
    }
    function lookAtSouthEast () {
        networking.send ("lookat 1 1");
    }
    function lookAtSouthWest () {
        networking.send ("lookat -1 1");
    }
    function lookAtNorthEast () {
        networking.send ("lookat 1 -1");
    }
    function lookAtNorthWest () {
        networking.send ("command -1 -1");
    }
    */

    function say (s) {
        networking.send ("command say " + s);
    }

    function chat (s) {
        networking.send ("command chat " + s);
    }

    function drop (s) {
        networking.send ("command drop " + s);
    }

    function get (s) {
        networking.send ("command get " + s);
    }

    function cast (s) {
        networking.send ("command cast " + s);
    }

    function invoke (s) {
        networking.send ("command invoke " + s);
    }

    function fire_stop () {
        networking.send ("command fire_stop");
    }

    function fire () {
        networking.send ("command fire");
    }

    function apply (s) {
        s = s || "";
        networking.send ("command apply " + s);
    }

    function command (s) {
        s = s || "";
        networking.send ("command " + s);
    }

    function useSkill (s) {
        s = s || "";
        networking.send ("command use_skill " + s);
    }
    // console.log(data);
    if (data.cmd) {
        switch (data.cmd) {
            case "north":
                moveNorth ();
                break;
            case "south":
                moveSouth ();
                break;
            case "northeast":
                moveNorthEast ();
                break;
            case "southeast":
                moveSouthEast ();
                break;
            case "northwest":
                moveNorthWest ();
                break;
            case "southwest":
                moveSouthWest ();
                break;
            case "east":
                moveEast ();
                break;
            case "west":
                moveWest ();
                break;
            case "say":
                say (data.param);
                break;
            case "chat":
                chat (data.param);
                break;
            case "drop":
                drop (data.param);
                break;
            case "get":
                get (data.param);
                break;
            case "cast":
                cast (data.param);
                break;
            case "invoke":
                invoke (data.param);
                break;
            case "fire_stop":
                fire_stop ();
                break;
            case "fire":
                fire ();
                break;
            case "apply":
                apply (data.param);
                break;
            case "command":
                command (data.param);
                break;
            case "use_skill":
                useSkill (data.param);
                break;
            case "exit":
                this.worker.terminate ();
                this.worker   = null;
                this.interval = null;
                break;
        }
    }
};

$.onClick = function () {
    this.macros[this.currentMacro] = this.macros[this.currentMacro] || {
        script:     this.textArea.value,
        keyBinding: ""
    };
    if (!this.nostorage) {
        localStorage.setItem ("macros", JSON.stringify (this.macros));
    }
    this.runMacro (this.textArea.value);
};

$.onKeyDown = function (e) {
    if (this.game.inputText) {
        return;
    }
    var isShift = false;
    var isCtrl  = false;
    var isAlt   = false;

    isShift = isShift || e.shiftKey || this.game.keyboard.getKey (CKeyboard.KEYS.SHIFT);
    isAlt   = isAlt || e.altKey || this.game.keyboard.getKey (CKeyboard.KEYS.ALT);
    isCtrl  = isCtrl || e.ctrlKey || this.game.keyboard.getKey (CKeyboard.KEYS.CTR);
    var s = "";

    if (isShift) {
        s += "{SHIFT} ";
    }
    if (isAlt) {
        s += "{ALT} ";
    }
    if (isCtrl) {
        s += "{CTRL} ";
    }
    s += " " + e.key + " (" + e.keyCode + ")";
    for (var s2 in this.macros) {
        if (this.macros[s2].keyBinding == s) {
            this.runMacro (this.macros[s2].script);
            break;
        } else {
            // console.log(this.macros[s2].keyBinding);
            // console.log("not equal");
            // console.log(s);
        }
    }
};

