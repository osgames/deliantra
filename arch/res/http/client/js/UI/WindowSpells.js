/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CWindowSpells (x, y, w, h, game, options) {
    options = options || {};
    CWindow.call (this, x, y, w, h, game, options);
    this.containerDiv.style.overflow = "scroll";
    var self = this;

    this.canvas.addEventListener ("click", function (e) {
        var y     = e.offsetY;
        var index = parseInt ((y - 50) / 30);
        var cm    = self.game.contextMenu;

        cm.itemsSpells ();
        cm.item = self.game.mySpells[index];
        cm.move (e.pageX - 5, Math.min (window.innerHeight - cm.height - 30, e.pageY - 5));
        cm.show (true);
        
        return;
    });
    var btn = this.appendTo.insertBefore ($_ ("input", {
        type:  "button",
        value: "by name"
    }), this.canvas);

    btn.addEventListener ("click", function () {
        var order = self.sortOrderCost || 0;

        self.sortOrderCost = order;
        if (order == 1) {
            self.game.mySpells.sort (function (a, b) {
                return b.name.localeCompare (a.name);
            });
        } else {
            self.game.mySpells.sort (function (a, b) {
                return a.name.localeCompare (b.name);
            });
        }
        self.sortOrderCost = (self.sortOrderCost + 1) % 2;
        self.draw ();
    });
    btn = this.appendTo.insertBefore ($_ ("input", {
        type:  "button",
        value: "by skill"
    }), this.canvas);
    btn.addEventListener ("click", function () {
        var order = self.sortOrderCost || 0;

        self.sortOrderCost = order;
        if (order == 1) {
            self.game.mySpells.sort (function (a, b) {
                return b.skill - a.skill;
            });
        } else {
            self.game.mySpells.sort (function (a, b) {
                return a.skill - b.skill;
            });
        }
        self.sortOrderCost = (self.sortOrderCost + 1) % 2;
        self.draw ();
    });
    btn = this.appendTo.insertBefore ($_ ("input", {
        type:  "button",
        value: "by level"
    }), this.canvas);
    btn.addEventListener ("click", function () {
        var order = self.sortOrderCost || 0;

        self.sortOrderCost = order;
        if (order == 1) {
            self.game.mySpells.sort (function (a, b) {
                return b.level - a.level;
            });
        } else {
            self.game.mySpells.sort (function (a, b) {
                return a.level - b.level;
            });
        }
        self.sortOrderCost = (self.sortOrderCost + 1) % 2;
        self.draw ();
    });
    btn = this.appendTo.insertBefore ($_ ("input", {
        type:  "button",
        value: "by cost"
    }), this.canvas);
    btn.addEventListener ("click", function () {
        var order = self.sortOrderCost || 0;

        self.sortOrderCost = order;
        if (order == 1) {
            self.game.mySpells.sort (function (a, b) {
                return Math.max (b.mana, b.grace) - Math.max (a.mana, a.grace);
            });
        } else {
            self.game.mySpells.sort (function (a, b) {
                return Math.max (a.mana, a.grace) - Math.max (b.mana, b.grace);
            });
        }
        self.sortOrderCost = (self.sortOrderCost + 1) % 2;
        self.draw ();
    });
    this.filter = this.appendTo.insertBefore ($_ ("input", {
        type:  "text",
        value: ""
    }), this.canvas);
    var cb = function () {
        this.draw ();
    }.bind (this);

    this.filter.addEventListener ("focus", function () {
        self.game.inputText = true;
    });
    this.filter.addEventListener ("blur", function () {
        self.game.inputText = false;
    });
    this.filter.addEventListener ("keydown", cb);
    this.filter.addEventListener ("keyup", cb);
    btn = this.appendTo.insertBefore ($_ ("input", {
        type:  "button",
        value: "x"
    }), this.canvas);
    btn.addEventListener ("click", function () {
        this.filter.value = ""; this.draw ();
    }.bind (this));
}
CWindowSpells.prototype             = Object.create (CWindow.prototype);
CWindowSpells.prototype.constructor = CWindowSpells;
CWindowSpells.prototype.draw        = function () {
    CWindow.prototype.draw.apply (this);
    var spells = this.game.mySpells;
    var i;

    if (this.filter.value.length) {
        spells = [];
        var filter = this.filter.value.split ("");

        filter = filter.join (".*");
        var regex = new RegExp (filter, "gi");

        for (i = 0; i < this.game.mySpells.length; ++i) {
            if (this.game.mySpells[i].name.match (regex)) {
                spells.push (this.game.mySpells[i]);
            }
        }
    }
    var missing = false;

    this.resize (600, spells.length * 30 + 50);
    var ctx = this.ctx;

    ctx.fillStyle = "white";
    // var face       = this.game.faces ? this.game.faces.find (this.game.faceIDSkillNames) : null;
    // var skillNames = face ? face.data || [] : null;
    for (i = 0; i < spells.length; ++i) {
        var spell = spells[i];

        ctx.fillText ("" + spell.name, 40, 50 + i * 30);
        ctx.fillText ("" + spell.minLevel, 240, 50 + i * 30);
        ctx.fillText ("" + spell.level, 280, 50 + i * 30);
        ctx.fillText (spell.mana ? "" + spell.mana + "sp" : spell.grace + "gp", 320, 50 + i * 30);
        //        if (skillNames) {
        //            ctx.fillText("" + skillNames[spell.skill], 240, 50 + i*30);
        //        }
        // console.log(skillNames);
        var img = this.game.findFace (spell.face);

        if (img) {
            this.ctx.drawImage (img, 0, 35 + i * 30, 20, 20);
        } else {
            missing = true;
        }
    //        console.log(spell);
    }
    if (missing) {
        setTimeout (function () {
            this.draw ();
        }.bind (this), 200);
    }
};
CWindowSpells.prototype.resize = function (w, h) {
    CWindow.prototype.resize.call (this, w, Math.min (400, h));
    this.canvas.width  = w;
    this.canvas.height = h;
};

