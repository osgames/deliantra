/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CWindowMessages (x, y, w, h, game, options) {
    options            = options || {};
    options.closable   = true;
    options.resizeable = true;
    CWindow.call (this, game.mapWindow.width - w, y, w, h, game, options);
    var self = this;

    this.activeChannel    = 0;
    this.canvas.height    = 0;
    this.div.id           = "channels";
    this.buttonsContainer = this.appendTo.appendChild ($_ ("div"));
    this.input            = this.appendTo.appendChild ($_ ("input"));
    this.divInner         = this.appendTo.appendChild ($_ ("div"));
    this.table2           = this.divInner.appendChild ($_ ("table"));
    this.table2.className = "chat";
    this.input.className  = "inputbox";
    this.appendTo.removeChild (this.canvas);
    this.input.addEventListener ("focus", function () {
        self.game.inputText = true;
    });
    this.input.addEventListener ("blur", function () {
        self.game.inputText = false;
    });
    this.input.addEventListener ("keypress", function (e) {
        if (e.keyCode == CKeyboard.KEYS.ENTER && this.value.length) {
            self.onMessage (this.value);
            this.value = "";
        } else if ((e.keyCode == CKeyboard.KEYS.ENTER2 || e.keyCode == CKeyboard.KEYS.ENTER) && e.ctrlKey == true) {
            self.game.inputText = false;
            var ip = $_ ("input");

            document.getElementById ("hidden").appendChild (ip);
            ip.focus ();
            document.getElementById ("hidden").removeChild (ip);
            this.value = "";
        }
    });
    this.resize (this.width, this.height);
}
CWindowMessages.prototype             = Object.create (CWindow.prototype);
CWindowMessages.prototype.constructor = CWindowMessages;
var $ = CWindowMessages.prototype;

$.onMessage = function (m) {
    this.game.networking.send (new Blob (["command " + this.game.channels[this.activeChannel].reply + " " + m]));
};
$.settext = function () {
    var channel = this.game.channels[this.activeChannel];

    if (!channel.table) {
        channel.table = $_ ("table");
    }
    var table = channel.table;
    var last  = channel.last;

    if (!channel.height) {
        channel.height = 0;
    }
    var i;
    var height = channel.height;

    for (i = last; i < channel.messages.length; ++i) {
        var msg    = channel.messages[i];
        var tr     = this.table2.appendChild ($_ ("tr"));
        var tdTime = tr.appendChild ($_ ("td"));

        tdTime.innerHTML = msg[0] + ":";
        var tdMsg = tr.appendChild ($_ ("td"));

        tdMsg.innerHTML = "<pre>" + msg.pop () + "" + "</pre>";
        table.appendChild (tr);
        height += table.getBoundingClientRect ().height;
    }
    channel.last   = i;
    channel.height = height;
    if (this.divInner.children[0] != table) {
        while (this.divInner.children.length) {
            this.divInner.removeChild (this.divInner.children[0]);
        }
        this.divInner.appendChild (table);
    }
    this.divInner.scrollTop = channel.height;
    if (channel.reply) {
        this.input.style.display = "block";
    } else {
        this.input.style.display = "none";
    }
};
$.draw = function () {
    CWindow.prototype.draw.apply (this);
    var self = this;
    var inputs = this.appendTo.getElementsByTagName ("input");
    
    for (var j = inputs.length - 1; j >= 0; --j) {
        (inputs[j].type == "button") && (inputs[j].parentNode == this.appendTo) && this.appendTo.removeChild (inputs[j]);
    }
    
    for (var i = 0; i < this.game.channels.length; ++i) {
        var s   = (i + 1) + ". " + this.game.channels[i].title + (this.game.channels[i].active ? "*" : "");
        var btn = this.appendTo.insertBefore ($_ ("input", {
            type:  "button",
            value: s
        }), this.buttonsContainer);

        btn.addEventListener ("click", (function (o, ii) {
            return function () {
                self.activeChannel                            = ii;
                self.game.channels[self.activeChannel].active = false;
                self.settext ();
                self.draw ();
            };
        }) (btn, i));
    }
};

$.resize = function (w, h) {
    CWindow.prototype.resize.apply (this, [w, h]);
    if (this.divInner) {
        this.divInner.style.height = (h - 65) + "px";
    }
};
