/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CWindowContext (x, y, game, options) {
    options              = options || {};
    options.resizeable   = false;
    options.resizeCanvas = true;
    options.noTitleBar   = true;
    this.timeout = 0;
    
    CWindow.call (this, x, y, 0, 0, game, options);
    window.addEventListener ("click", function () {
        if (this.calledBy) {
            this.calledBy = false;
            return;
        }
        this.show (false);
    }.bind (this));
    
    var cm = this;

    cm.clear ();
    cm.addContextItem ({
        name: "examine",
        text: "examine",
        cb:   function () {
            cm.examine ();
        }
    });
    cm.addContextItem ({
        name: "mark",
        text: "mark",
        cb:   function () {
            cm.mark ();
        }
    });
    cm.addContextItem ({
        name: "ignite",
        text: "ignite/thaw",
        cb:   function () {}
    });
    cm.addContextItem ({
        name: "inscribe",
        text: "inscribe",
        cb:   function () {}
    });
    cm.addContextItem ({
        name: "rename",
        text: "rename",
        cb:   function () {}
    });
    cm.addContextItem ({
        name: "apply",
        text: "apply",
        cb:   function () {}
    });
    cm.addContextItem ({
        name: "lock",
        text: "lock",
        cb:   function () {}
    });
    cm.addContextItem ({
        name: "takeAll",
        text: "take all",
        cb:   function () {}
    });
    cm.addContextItem ({
        name: "takeN",
        text: "take <n>",
        cb:   function () {
            cm.getn ();
        }
    });
    cm.addContextItem ({
        name: "bind",
        text: "bind apply ${0} to a key",
        cb:   function () {}
    });
}

CWindowContext.prototype = Object.create (CWindow.prototype);
CWindowContext.prototype.constructor = CWindowContext;

CWindowContext.prototype.addContextItem = function (itm) {
    this.items.push (itm);
    var itmHTML = this.appendTo.appendChild ($_ ("div"));

    itmHTML.className = "contextMenuItem";
    itmHTML.appendChild (document.createTextNode (itm.text));
    this.resize (200, 26 * this.items.length);
    var me = this;

    itmHTML.addEventListener ("click", function () {
        itm.cb ();
        me.show (false);
    });
};

CWindowContext.prototype.clear = function () {
    while (this.appendTo.childNodes.length) {
        this.appendTo.removeChild (this.appendTo.childNodes[0]);
    }
    this.items = [];
};

CWindowContext.prototype.examine = function () {
    this.game.networking.send ("examine " + this.item.itemId);
};

CWindowContext.prototype.apply = function () {
    this.game.networking.send ("apply " + this.item.itemId);
};

CWindowContext.prototype.getn = function () {
    this.game.selectAmountWindow.cb = this.getnex.bind (this);
    this.game.selectAmountWindow.n  = this.item.nr_of;
    this.game.selectAmountWindow.show (true);
};

CWindowContext.prototype.getnex = function () {
    var amount = this.game.selectAmountWindow.current;

    if (!this.game.selectAmountWindow.isCancelled) {
        this.game.networking.send ("move " + this.game.containers.first.id + " " + this.item.itemId + " " + amount); // item.nr_of );
    }
    this.game.selectAmountWindow.show (false);
};

CWindowContext.prototype.getall = function () {
    this.game.networking.send ("move " + this.game.containers.first.id + " " + this.item.itemId + " 0"); // item.nr_of );
};

CWindowContext.prototype.dropn = function () {
    this.game.selectAmountWindow.cb = this.dropnex.bind (this);
    this.game.selectAmountWindow.n  = this.item.nr_of;
    this.game.selectAmountWindow.show (true);
};

CWindowContext.prototype.dropnex = function () {
    var amount = this.game.selectAmountWindow.current;

    if (!this.game.selectAmountWindow.isCancelled) {
        var floorContainer = this.game.secondaryContainer || this.game.containers.find (0);

        this.game.networking.send ("move " + floorContainer.id + " " + this.item.itemId + " " + amount); // item.nr_of );
    }
    this.game.selectAmountWindow.show (false);
};

CWindowContext.prototype.dropall = function () {
    var floorContainer = this.game.secondaryContainer || this.game.containers.find (0);

    this.game.networking.send ("move " + floorContainer.id + " " + this.item.itemId + " 0"); // item.nr_of );
};

CWindowContext.prototype.mark = function () {
    var buff = new Buffer ([0, 0, 0, 0], 0);

    buff.position = 0;
    buff.writeUint32 (this.item.itemId);
    /*
    var s  = "" + this.item.itemId + " ";
    var s2 = "mark ";

    for (var i = 0; i < s2.length; ++i) {
        s += s2.charCodeAt (i).toString (16) + " ";
    }
    for (i = 0; i < buff.buffer.length; ++i) {
        s += " " + buff.buffer[i].toString (16);
    }
    // console.log(s);*/
    var arr  = [];
    var name = "mark ";

    for (var i = 0; i < name.length; ++i) {
        arr.push (name.charCodeAt (i));
    }
    arr = arr.concat (buff.buffer);
    var byteArray = new Uint8Array (arr);

    this.game.networking.send (byteArray.buffer);
};

CWindowContext.prototype.lock = function () {
    var buff = new Buffer ([1, 0, 0, 0, 0], 1);

    if (this.item.flags & F_LOCKED) {
        buff.buffer[0] = 0;
    }
    buff.position = 1;
    buff.writeUint32 (this.item.itemId);
    var s  = "" + this.item.itemId + " ";
    var s2 = "lock ";
    var i;

    for (i = 0; i < s2.length; ++i) {
        s += s2.charCodeAt (i).toString (16) + " ";
    }
    for (i = 0; i < buff.buffer.length; ++i) {
        s += " " + buff.buffer[i].toString (16);
    }
    console.log (s);
    var arr  = [];
    var name = "lock ";

    for (i = 0; i < name.length; ++i) {
        arr.push (name.charCodeAt (i));
    }
    arr = arr.concat (buff.buffer);
    var byteArray = new Uint8Array (arr);

    this.game.networking.send (byteArray.buffer);
};

CWindowContext.prototype.itemsFloor = function () {
    var cm = this;

    cm.clear ();
    cm.addContextItem ({
        name: "examine",
        text: "examine",
        cb:   function () {
            cm.examine ();
        }
    });
    cm.addContextItem ({
        name: "mark",
        text: "mark",
        cb:   function () {
            cm.mark ();
        }
    });
    cm.addContextItem ({
        name: "ignite",
        text: "ignite/thaw",
        cb:   function () {}
    });
    cm.addContextItem ({
        name: "inscribe",
        text: "inscribe",
        cb:   function () {}
    });
    cm.addContextItem ({
        name: "rename",
        text: "rename",
        cb:   function () {}
    });
    cm.addContextItem ({
        name: "apply",
        text: "apply",
        cb:   function () {
            cm.apply ();
        }
    });
    cm.addContextItem ({
        name: "lock",
        text: "lock",
        cb:   function () {}
    });
    cm.addContextItem ({
        name: "takeAll",
        text: "take all",
        cb:   function () {
            cm.getall ();
        }
    });
    cm.addContextItem ({
        name: "takeN",
        text: "take <n>",
        cb:   function () {
            cm.getn ();
        }
    });
    cm.addContextItem ({
        name: "bind",
        text: "bind apply to a key",
        cb:   function () {}
    });
};

CWindowContext.prototype.itemsSpells = function () {
    var cm = this;

    cm.clear ();
    cm.addContextItem ({
        name: "cast",
        text: "cast",
        cb:   function () {
            cm.cast ();
        }
    });
    cm.addContextItem ({
        name: "invoke",
        text: "invoke",
        cb:   function () {
            cm.invoke ();
        }
    });
    cm.addContextItem ({
        name: "bind cast to key",
        text: "bind cast to key",
        cb:   function () {
            cm.bindCast ();
        }
    });
    cm.addContextItem ({
        name: "bind invoke to key",
        text: "bind invoke to key",
        cb:   function () {
            cm.bindInvoke ();
        }
    });
};

CWindowContext.prototype.cast = function () {
    this.game.networking.send ("command cast " + this.item.name);
};

CWindowContext.prototype.invoke = function () {
    this.game.networking.send ("command invoke " + this.item.name);
};

CWindowContext.prototype.bindCast = function () {
    var name      = "bind_" + this.item.name.replace (/ /g, "_");
    var macroText = "cast('" + this.item.name + "'); \nexit();";

    this.game.macroWindow.registerWithKeybind (name, macroText);
};

CWindowContext.prototype.bindInvoke = function () {
    var name      = "invoke_" + this.item.name.replace (/ /g, "_");
    var macroText = "invoke('" + this.item.name + "'); \nexit();";

    this.game.macroWindow.registerWithKeybind (name, macroText);
};

CWindowContext.prototype.itemsInventory = function () {
    var cm = this;

    cm.clear ();
    cm.addContextItem ({
        name: "examine",
        text: "examine",
        cb:   function () {
            cm.examine ();
        }
    });
    cm.addContextItem ({
        name: "mark",
        text: "mark",
        cb:   function () {
            cm.mark ();
        }
    });
    cm.addContextItem ({
        name: "ignite",
        text: "ignite/thaw",
        cb:   function () {}
    });
    cm.addContextItem ({
        name: "inscribe",
        text: "inscribe",
        cb:   function () {}
    });
    cm.addContextItem ({
        name: "rename",
        text: "rename",
        cb:   function () {}
    });
    cm.addContextItem ({
        name: "apply",
        text: "apply",
        cb:   function () {
            cm.apply ();
        }
    });
    var o = "lock";

    if (this.item.flags & F_LOCKED) {
        o = "unlock";
    }
    cm.addContextItem ({
        name: o,
        text: o,
        cb:   function () {
            cm.lock ();
        }
    });
    cm.addContextItem ({
        name: "takeAll",
        text: "drop all",
        cb:   function () {
            cm.dropall ();
        }
    });
    cm.addContextItem ({
        name: "takeN",
        text: "drop <n>",
        cb:   function () {
            cm.dropn ();
        }
    });
    cm.addContextItem ({
        name: "bind",
        text: "bind apply {0} to a key",
        cb:   function () {}
    });
};

CWindowContext.prototype.itemsSettings = function () {
    var cm = this;

    cm.clear ();
    cm.addContextItem ({
        name: "inventory",
        text: "inventory",
        cb:   function () {
            cm.showInventory ();
        }
    });
    cm.addContextItem ({
        name: "skills",
        text: "skills",
        cb:   function () {
            cm.showSkills ();
        }
    });
    cm.addContextItem ({
        name: "spells",
        text: "spells",
        cb:   function () {
            cm.showSpells ();
        }
    });

    if (!this.game.barsWindow.visible) {
        cm.addContextItem ({
            name: "bars",
            text: "bars",
            cb:   function () {
                cm.showBars ();
            }
        });
    }

    if (!this.game.messagesWindow.visible) {
        cm.addContextItem ({
            name: "messages",
            text: "messages",
            cb:   function () {
                cm.showMessages ();
            }
        });
    }

    cm.addContextItem ({
        name: "pickup",
        text: "pickup",
        cb:   function () {
            cm.pickup ();
        }
    });
    cm.addContextItem ({
        name: "statistics",
        text: "statistics",
        cb:   function () {
            cm.statistics ();
        }
    });
    cm.addContextItem ({
        name: "macro",
        text: "macro",
        cb:   function () {
            cm.showMacro ();
        }
    });
    cm.addContextItem ({
        name: "fullscreen",
        text: "fullscreen",
        cb:   function () {
            cm.fullScreen ();
        }
    });
};

CWindowContext.prototype.showMessages = function () {
    this.game.messagesWindow.show (true);
};

CWindowContext.prototype.showBars = function () {
    this.game.barsWindow.show (true);
};

CWindowContext.prototype.pickup = function () {
    this.game.pickupWindow.toggleShow ();
    this.game.pickupWindow.center ();
};

CWindowContext.prototype.statistics = function () {
    this.game.statisticsWindow.toggleShow ();
    this.game.statisticsWindow.center ();
};

CWindowContext.prototype.fullScreen = function () {
    if (!document.fullscreenElement) {
        var docelem = document.documentElement;

        if (docelem.requestFullscreen) {
            docelem.requestFullscreen ();
        } else if (docelem.mozRequestFullScreen) {
            docelem.mozRequestFullScreen ();
        } else if (docelem.webkitRequestFullscreen) {
            docelem.webkitRequestFullscreen ();
        } else if (docelem.msRequestFullscreen) {
            docelem.msRequestFullscreen ();
        }
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen ();
        }
    }
};

CWindowContext.prototype.showInventory = function () {
    this.game.containerWindow.container = this.game.containerWindow.container || this.game.containers.first;
    this.game.containerWindow.draw ();
    this.game.containerWindow.toggleShow ();
};

CWindowContext.prototype.showSkills = function () {
    this.game.skillsWindow.draw ();
    this.game.skillsWindow.toggleShow ();
};

CWindowContext.prototype.showSpells = function () {
    this.game.spellsWindow.draw ();
    this.game.spellsWindow.toggleShow ();
};

CWindowContext.prototype.showMacro = function () {
    this.game.macroWindow.draw ();
    this.game.macroWindow.toggleShow ();
};

CWindowContext.prototype.show = function (b) {
    CWindow.prototype.show.apply (this,[b]);
    this.calledBy = true;
};

