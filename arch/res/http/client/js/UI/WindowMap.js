/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CWindowMap (x, y, w, h, game, options) {
    // console.log(game);
    CWindow.call (this, x, y, w, h, game, options);
    this.canvas.style.width  = "100%";
    this.canvas.style.height = "100%";
/*
var self = this;
this.moving = false;
this.canvas.addEventListener ( 'mousemove' , function ( e ) {
   self.onMouseMove ( e );
} );
this.canvas.addEventListener ( 'mousedown' , function ( e ) {
   self.onMouseDown ( e );
} );

this.canvas.addEventListener ( 'mouseup' , function ( e ) {
   self.onMouseUp ( e );
} );*/
}
CWindowMap.prototype             = Object.create (CWindow.prototype);
CWindowMap.prototype.constructor = CWindowMap;
CWindowMap.prototype.onMouseDown = function () {
    var d = new Date ();
    var n = d.getMilliseconds () + d.getSeconds () * 1000 + d.getMinutes () * 1000 * 60;

    this.lastMove = n;
    this.moving   = true;
    this.cancelDrag ();
    var self = this;

    this.interval = setInterval (function () {
        self.onIdle ();
    }, 200);
};
CWindowMap.prototype.onIdle = function () {
    var d = new Date ();
    var n = d.getMilliseconds () + d.getSeconds () * 1000 + d.getMinutes () * 1000 * 60;

    if (!this.lastMoveS) {
        return;
    }
    if (n < this.lastMove + 200) {
        return;
    }
    if (this.moving) {
        this.lastMove = n;
        this.game.networking.send (this.lastMoveS);
    }
};
CWindowMap.prototype.onMouseUp = function () {
    this.moving = false;
    clearInterval (this.interval);
    this.cancelDrag ();
};
CWindowMap.prototype.onMouseMove = function (e) {
    if (!this.moving) {
        return;
    }
    var d = new Date ();
    var n = d.getMilliseconds () + d.getSeconds () * 1000 + d.getMinutes () * 1000 * 60;

    if (n < this.lastMove + 200) {
        return;
    }
    this.lastMove = n;
    var x       = e.offsetX;
    var y       = e.offsetY;
    var centerX = this.width >> 1;
    var centerY = this.height >> 1;
    var isX     = Math.abs (x - centerX) > 20;
    var isY     = Math.abs (y - centerY) > 20;

    if (isX && !isY) {
        if (x < centerX) {
            this.lastMoveS = ("command west");
        } else if (x > centerX) {
            this.lastMoveS = ("command east");
        }
    } else if (!isX && isY) {
        if (y < centerY) {
            this.lastMoveS = ("command north");
        } else if (y > centerY) {
            this.lastMoveS = ("command south");
        }
    } else {
        if (y < centerY && x < centerX) {
            this.lastMoveS = ("command northwest");
        } else if (y < centerY && x > centerX) {
            this.lastMoveS = ("command northeast");
        } else if (y > centerY && x < centerX) {
            this.lastMoveS = ("command southwest");
        } else {
            this.lastMoveS = ("command southeast");
        }
    }
    if (this.lastMoveS) {
        this.game.networking.send (this.lastMoveS);
    }
};
CWindowMap.prototype.draw = function () {
    // CWindow.prototype.draw.apply (this);
    this.game.map.draw (this.ctx);
};

