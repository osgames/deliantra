/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CWindowContainer (x, y, w, h, game, options, container) {
    options          = options || {};
    options.closable = true;
    this.container   = container;
    CWindow.call (this, x, y, 800, 600, game, options);
    var self = this;

    this.paneLeft = this.appendTo.appendChild ($_ ("div", { className: "paneLeft" }));
    this.paneRight = this.appendTo.appendChild ($_ ("div", { className: "paneRight" }));
    //this.appendTo.appendChild ($_ ("div", {className:"clearer"}));
    
    this.canvas.addEventListener ("click", function (e) {
        self.onClick (e);
    });
    
    this.tooltip = "Double click for context menu.<br/>Shift click to move item.";
    this.defaultTooltip = this.tooltip;
    
    /*
    var btn = this.appendTo.insertBefore ($_ ("input", {
        type:  "button",
        value: "by type"
    }), this.canvas);

    btn.addEventListener ("click", function () {
        var order = self.sortOrderType || 0;

        self.sortOrderType = order;
        if (order == 1) {
            self.container.items.sort (function (a, b) {
                return b.clientType - a.clientType;
            });
        } else {
            self.container.items.sort (function (a, b) {
                return a.clientType - b.clientType;
            });
        }
        self.sortOrderType = (self.sortOrderType + 1) % 2;
        self.draw ();
    });
    btn = this.appendTo.insertBefore ($_ ("input", {
        type:  "button",
        value: "by weight"
    }), this.canvas);
    btn.addEventListener ("click", function () {
        var order = self.sortOrderWeight || 0;

        self.sortOrderWeight = order;
        if (order == 1) {
            self.container.items.sort (function (a, b) {
                return b.weight - a.weight;
            });
        } else {
            self.container.items.sort (function (a, b) {
                return a.weight - b.weight;
            });
        }
        self.sortOrderWeight = (self.sortOrderWeight + 1) % 2;
        self.draw ();
    });
    btn = this.appendTo.insertBefore ($_ ("input", {
        type:  "button",
        value: "by name"
    }), this.canvas);
    btn.addEventListener ("click", function () {
        var order = self.sortOrderName || 0;

        self.sortOrderName = order;
        if (order == 1) {
            self.container.items.sort (function (a, b) {
                return b.name.localeCompare (a.name);
            });
        } else {
            self.container.items.sort (function (a, b) {
                return a.name.localeCompare (b.name);
            });
        }
        self.sortOrderName = (self.sortOrderName + 1) % 2;
        self.draw ();
    });
    */
    function createCallback (usePlayerContainer) {
        return function (e) {
            var container;
            
            if (usePlayerContainer) {
                container = self.container;
            } else {
                container = (self.game.secondaryContainer || self.game.containers.find (0));
            }
            
            if (!container) {
                return;
            }
            
            var index          = parseInt ((e.offsetY - 10) / 25);
            var item           = container.items[index];
            
            if (!item) {
                return;
            }
            
            if (item.exText) {
                self.tooltip = item.exText + "<hr/>" + self.defaultTooltip;
                self.game.tooltip.setText (item.exText + "<hr/>" + self.defaultTooltip);
                self.game.tooltip.setPosition (e);
            } else {
                if (item.exTextRequested) {
                    return;
                }
                item.exTextRequested = true;
                self.game.feedExCb = function () {
                    self.game.tooltip.setText (item.exText + "<hr/>" + self.defaultTooltip);
                    self.game.tooltip.setPosition (e);
                };

                self.game.networking.send ("ex " + item.itemId);
            }
        };
    }

    //this.appendTo.removeChild(this.canvas);
    this.paneLeft.appendChild (this.canvas);
    this.canvasSecondary = this.paneRight.appendChild ($_ ("canvas"));
    this.ctxSecondary = this.canvasSecondary.getContext ("2d");
    this.canvas.addEventListener ("mousemove", createCallback (true));
    this.canvasSecondary.addEventListener ("mousemove", createCallback (false));
    this.game.tooltip.register (this.canvas, this);
    this.game.tooltip.register (this.canvasSecondary, this);
}
CWindowContainer.prototype             = Object.create (CWindow.prototype);
CWindowContainer.prototype.constructor = CWindowContainer;

CWindowContainer.prototype.draw = function () {
    var missing   = this.drawContainer (this.container, this.canvas,this.ctx);
    missing   = missing || this.drawContainer (this.game.secondaryContainer || this.game.containers.find (0), this.canvasSecondary, this.ctxSecondary);
    
    if (missing) {
        setTimeout (function () {
            this.draw ();
        }.bind (this), 200);
    }
};

CWindowContainer.prototype.drawContainer = function (container, canvas, ctx) {
    
    var missing   = false;
    if (container && container.items && container.items.length) {
        
        canvas.width  = 480;
        canvas.height = 25 * container.items.length + 5;
        ctx.fillStyle = "blue";
        ctx.fillRect (0, 0, 480, 25 * container.items.length + 5);
        for (var i = 0; i < container.items.length; ++i) {
            /**
             * @type CItem
             */
            var item = container.items[i];

            ctx.fillStyle = "white";
            ctx.save ();
            ctx.translate (0, 10);
            var s = item.name;

            if (item.nr_of > 1) {
                s = item.nr_of + " x " + item.namepl;
            }
            
            s = this.shortenName (s);
            
            if (item.flags & F_APPLIED) {
                s += "(applied)";
            }
            if (item.flags & F_LOCKED) {
                s += "*";
            }
            ctx.fillText (s, 25, i * 25 + 10);
            var img = this.game.findFace (item.face);

            if (img) {
                ctx.drawImage (img, 0, i * 25, 20, 20);
            } else {
                missing = true;
            }
            ctx.restore ();
        }
    }
    return missing;
};

CWindowContainer.prototype.shortenName = function (s) {
    
    if (s.length > 30) {
        var tmp = s;
        var tmpReversed = s.reverse ();
        tmpReversed = tmpReversed.substr (0, 13);
        tmpReversed = tmpReversed.reverse ();
        s = tmp.substr (0, 13) + " ... " + tmpReversed;
    }
    return s;
};
CWindowContainer.prototype.onClick = function (e) {
    var index          = parseInt ((e.offsetY - 10) / 25);
    var floorContainer = this.container;
    var isCtrl         = false;
    var isContext      = false;

    if (!isUndefined (this.lastItem)) {
        if (this.lastItem == floorContainer.items[index].itemId) {
            isContext = true;
        }
    }
    if (isContext) {
        var cm = this.game.contextMenu;

        cm.item = floorContainer.items[index];
        cm.itemsInventory ();
        cm.container = floorContainer;
        cm.move (e.pageX - 5, Math.min (window.innerHeight - cm.height - 30, e.pageY - 5));
        cm.show (true);
        delete (this.lastItem);
        
        return;
    }
    if ((this.game.keyboard.getKey (CKeyboard.KEYS.SHIFT) || (isCtrl = this.game.keyboard.getKey (CKeyboard.KEYS.CTR))) && index >= 0 && index < floorContainer.items.length) {
        this.cancelDrag ();
        var item = floorContainer.items[index];
        var name = item.name;

        if (item.nr_of > 1) {
            name = floorContainer.items[index].namepl;
        }
        var command = isCtrl ? "apply" : "drop"; // this dont work rilly ASK SCHMORP

        if (command == "drop") {
            floorContainer = this.game.secondaryContainer || this.game.containers.find (0);

            this.game.networking.send ("move " + floorContainer.id + " " + item.itemId + " " + 0);
        } else {
            this.game.networking.send ("command " + command + " " + name);
        }
        this.draw ();
    }
    
    this.lastItem = floorContainer.items[index].itemId;
};

