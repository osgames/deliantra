/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CWindowFloor (x, y, w, h, game, options) {
    options            = options || {};
    options.noTitleBar = false;
    CWindow.call (this, game.mapWindow.width - w, game.mapWindow.height - h, w, h, game, options);
    this.startX = x;
    this.startY = y;
    var self = this;
    this.tooltip = "Double click for context menu.<br/>Shift click to move item.";
    this.defaultTooltip = this.tooltip;
    
    this.canvas.addEventListener ("click", function (e) {
        self.onClick (e);
    });
    self.tooltip = "Double click for context menu.<br/>Shift click to move item.";
    self.game.tooltip.register (this.canvas, self);
    this.containerDiv.style.overflow = "scroll";
    this.canvas.addEventListener ("mousemove", function (e) {
        var floorContainer = self.game.secondaryContainer || self.game.containers.find (0);

        var index          = floorContainer.items.length - 1 - parseInt ((e.offsetY - 10) / 25);
        var item           = floorContainer.items[index];
        
        if (item.exText) {
            self.tooltip = item.exText + "<hr/>" + self.defaultTooltip;
            self.game.tooltip.setText (item.exText + "<hr/>" + self.defaultTooltip);
            self.game.tooltip.setPosition (e);
        } else {
            if (item.exTextRequested) {
                return;
            }
            item.exTextRequested = true;
            self.game.feedExCb = function () {
                self.game.tooltip.setText (item.exText + "<hr/>" + self.defaultTooltip);
                self.game.tooltip.setPosition (e);
            };
            
            self.game.networking.send ("ex " + item.itemId);
        }
    });
}
CWindowFloor.prototype             = Object.create (CWindow.prototype);
CWindowFloor.prototype.constructor = CWindowFloor;
var $ = CWindowFloor.prototype;

$.resize = function (w, h) {
    CWindow.prototype.resize.call (this, w, Math.min (150, h));
    this.canvas.width  = w;
    this.canvas.height = h;
    this.move (this.startX, this.startY + (120 - this.height));
};
CWindowFloor.prototype.onClick = function (e) {
    var index          = parseInt ((e.offsetY - 10) / 25);
    var floorContainer = this.game.secondaryContainer || this.game.containers.find (0);

    if (!(index >= 0 && index < floorContainer.items.length)) {
        return;
    }
    var isContext = false;

    if (!isUndefined (this.lastItem)) {
        if (this.lastItem == floorContainer.items[floorContainer.items.length - 1 - index].itemId) {
            isContext = true;
        }
    }
    if (isContext) {
        var cm = this.game.contextMenu;

        cm.item = floorContainer.items[floorContainer.items.length - 1 - index];
        cm.itemsFloor ();
        cm.container = floorContainer;
        cm.move (e.pageX - 5, Math.min (window.innerHeight - cm.height - 30, e.pageY - 5));
        cm.show (true);
        delete (this.lastItem);
        
        return;
    }
    var isCtrl = false;

    if ((this.game.keyboard.getKey (CKeyboard.KEYS.SHIFT) || (isCtrl = this.game.keyboard.getKey (CKeyboard.KEYS.CTR)))) {
        this.cancelDrag ();
        var item = floorContainer.items[floorContainer.items.length - 1 - index];
        var name = item.name;

        if (item.nr_of > 1) {
            name = item.namepl;
        }
        var command = isCtrl ? "apply" : "get"; // this dont work rilly ASK SCHMORP

        if (command == "get") {
            this.game.networking.send ("move " + this.game.containers.first.id + " " + item.itemId + " 0 "); // item.nr_of );
        } else {
            this.game.networking.send ("command " + command + " " + name);
        }
        this.draw ();
    }
    this.lastItem = floorContainer.items[floorContainer.items.length - 1 - index].itemId;
};
CWindowFloor.prototype.draw = function () {
    CWindow.prototype.draw.apply (this);
    var missing        = false;
    var floorContainer = this.game.secondaryContainer || this.game.containers.find (0);
    var count = 6;
    var j = 0;
    
    if (floorContainer) {
        this.resize (220, 25 * Math.min (count, floorContainer.items.length) + 5);
        this.ctx.fillStyle = "blue";
        this.ctx.fillRect (0, 0, 220, 25 * floorContainer.items.length + 5);
        for (var i = floorContainer.items.length - 1; i >= 0; --i) {
            
            /**
             * @type CItem
             */
            var item = floorContainer.items[i];

            this.ctx.fillStyle = "white";
            this.ctx.save ();
            this.ctx.translate (0, 10);

            if (item.nr_of > 1) {
                this.ctx.fillText (item.nr_of + " x " + item.namepl, 25, j * 25 + 10);
            } else {
                this.ctx.fillText (item.name, 25, j * 25 + 10);
            }
            var img = this.game.findFace (item.face);

            if (img) {
                this.ctx.drawImage (img, 0, j * 25, 20, 20);
            } else {
                missing = true;
            }
            this.ctx.restore ();
            ++j;
            if (!count--) {
                break;
            }
        }
        if (missing) {
            setTimeout (function () {
                this.draw ();
            }.bind (this), 200);
        }
    }
};

