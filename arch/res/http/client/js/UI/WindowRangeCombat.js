/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CWindowRangeCombat (x, y, game, options) {
    options              = options || {};
    options.resizeable   = false;
    options.resizeCanvas = true;
    options.noTitleBar   = true;
    CWindow.call (this, x, y, game.mapWindow.width - 114, 25, game, options);
    this.appendTo.removeChild (this.canvas);
    this.h3 = this.appendTo.appendChild ($_ ("h3", {
        className: "rangeCombat"
    }));
}
CWindowRangeCombat.prototype = Object.create (CWindow.prototype);
var $ = CWindowRangeCombat.prototype;

$.constructor = CWindowRangeCombat;
$.setText     = function (s) {
    this.h3.innerHTML = s;
};
