/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */
var inventory          = null;
var secondaryContainer = null;

function onmessageEx (e) {
    if (e.data.type) {
        switch (e.data.type) {
            case "inventory":
                inventory = e.data.data;
                if (onInventoryUpdate && typeof (onInventoryUpdate) === "function") {
                    onInventoryUpdate ();
                }
                break;
            case "secondaryContainer":
                secondaryContainer = e.data.data;
                if (onSecondaryContainerUpdate && typeof (onSecondaryContainerUpdate) === "function") {
                    onSecondaryContainerUpdate ();
                }
                break;
        }
    }
}

function findItemByName (name, container) {
    var o = container;

    for (var i = 0; i < o.items.length; ++i) {
        if (o.items[i].name.indexOf (name) >= 0) {
            return o.items[i];
        }
    }
    
    return null;
}

function hasInventoryInformation () {
    return inventory != null;
}

function sleep (y) {
    var d        = new Date ();
    var stamp    = d.getTime ();
    var newstamp = stamp;
    
    do {
        d        = new Date ();
        newstamp = d.getTime ();
    } while (stamp + y * 1000 > newstamp);
}

function south () {
    postMessage ({
        cmd: "south"
    });
}

function north () {
    postMessage ({
        cmd: "north"
    });
}

function east () {
    postMessage ({
        cmd: "east"
    });
}

function west () {
    postMessage ({
        cmd: "west"
    });
}

function northeast () {
    postMessage ({
        cmd: "northeast"
    });
}

function northwest () {
    postMessage ({
        cmd: "northwest"
    });
}

function southeast () {
    postMessage ({
        cmd: "southeast"
    });
}

function southwest () {
    postMessage ({
        cmd: "southwest"
    });
}

function cmd (s) {
    if (!s) {
        return;
    }
    s = s + "";
    postMessage ({
        cmd:   "command",
        param: s
    });
}

function say (s) {
    if (!s) {
        return;
    }
    postMessage ({
        cmd:   "say",
        param: s
    });
}

function chat (s) {
    if (!s) {
        return;
    }
    postMessage ({
        cmd:   "chat",
        param: s
    });
}

function drop (s) {
    s = s || "";
    postMessage ({
        cmd:   "drop",
        param: s
    });
}

function get (s) {
    s = s || "";
    postMessage ({
        cmd:   "get",
        param: s
    });
}

function cast (s) {
    s = s || "";
    postMessage ({
        cmd:   "cast",
        param: s
    });
}

function invoke (s) {
    s = s || "";
    postMessage ({
        cmd:   "invoke",
        param: s
    });
}

function fire_stop () {
    postMessage ({
        cmd: "fire_stop"
    });
}

function fire () {
    postMessage ({
        cmd: "fire"
    });
}

function apply (s) {
    s = s || "";
    postMessage ({
        cmd:   "apply",
        param: s
    });
}

function useSkill (s) {
    s = s || "";
    postMessage ({
        cmd:   "use_skill",
        param: s
    });
}

function exit () {
    postMessage ({
        cmd: "exit"
    });
}

