/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *  Copyright (©) 2017 Marc A. Lehmann <schmorp@schmorp.de>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function Buffer (buffer, position) {
    this.buffer   = buffer;
    this.position = position;
    this.length   = buffer.length;
}

Buffer.prototype.readUint8 = function () {
    return this.buffer[this.position++];
};

Buffer.prototype.unpack_w = function () {
    var ret  = 0;
    var oct;

    do {
        ret *= 128;
        oct = this.readUint8 ();
        ret += oct & 0x7f;
    } while (oct & 0x80);
    
    return ret;
};

Buffer.prototype.readUint = function (octets) {
    var ret = this.readUint8 ();

    while (--octets)
        ret = ret * 256 + this.readUint8 ();
    
    return ret;
};

Buffer.prototype.readUint16 = function () {
    return this.readUint (2);
};

Buffer.prototype.readUint32 = function () {
    return this.readUint (4);
};

Buffer.prototype.readUint64 = function () {
    return this.readUint (8);
};

Buffer.prototype.writeUint32 = function (num) {
    num = parseInt (num);
    // console.log(num);
    var end = this.position + 3;

    for (var i = 0; i < 4; ++i) {
        this.buffer[end - (i)] = num & 0xff;
        num >>= 8;
        this.position++;
    }
};

Buffer.prototype.readUtf8StringFromBinaryString = function (bytes, offset, len) {
        
    var ix = offset;
    var byte1, byte2, byte3, byte4, codepoint;
    
    if (bytes.slice (ix, 3) == "\xEF\xBB\xBF") {
        ix += 3;
    }

    var string = "";

    for (; ix < offset + len; ix++) {
        byte1 = bytes[ix].charCodeAt (0);
        
        if (byte1 < 0x80) {
            string += String.fromCharCode (byte1);
        } else if (byte1 >= 0xC2 && byte1 < 0xE0) {
            byte2 = bytes[++ix].charCodeAt (0);
            string += String.fromCharCode (((byte1 & 0x1F) << 6) + (byte2 & 0x3F));
        } else if (byte1 >= 0xE0 && byte1 < 0xF0) {
            byte2 = bytes[++ix].charCodeAt (0);
            byte3 = bytes[++ix].charCodeAt (0);
            string += String.fromCharCode (((byte1 & 0xFF) << 12) + ((byte2 & 0x3F) << 6) + (byte3 & 0x3F));
        } else if (byte1 >= 0xF0 && byte1 < 0xF5) {
            byte2 = bytes[++ix].charCodeAt (0);
            byte3 = bytes[++ix].charCodeAt (0);
            byte4 = bytes[++ix].charCodeAt (0);
            codepoint = ((byte1 & 0x07) << 18) + ((byte2 & 0x3F) << 12) + ((byte3 & 0x3F) << 6) + (byte4 & 0x3F);
            codepoint -= 0x10000;
            string += String.fromCharCode (
                (codepoint >> 10) + 0xD800,
                (codepoint & 0x3FF) + 0xDC00
            );
        }
    }
        
    return string;
};

Buffer.prototype.readString = function (len) {
    var ret = "";

    if (typeof (len) != "undefined") {
        //return deliantra_util.utf8_decode (ret);
        ret = this.readUtf8StringFromBinaryString (this.binaryString, this.position, len);
        this.position += len;
    } else {
        ret = this.readUtf8StringFromBinaryString (this.binaryString, this.position, this.length - this.position);
        this.position += len;
    }
    return ret;
};

Buffer.prototype.createDump = function (arr) {
    var s = "";
    for (var i = 0,len = arr.length; i < len; ++i) {
        s += " " + buff.buffer[i].toString (16) + " ";
    }
    return s;
};

Buffer.prototype.toBinaryString = function () {
    var s = "";

    for (var i = 0; i < this.buffer.length; ++i) {
        s = s + String.fromCharCode (this.buffer[i]);
    }
    
    return s;
};

