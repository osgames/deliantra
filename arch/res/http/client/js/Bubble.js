/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

var Bubble;

function CBubble (s) {
    this.div           = $_ ("div");
    this.message       = s;
    this.count         = 1;
    this.div.className = "bubble";
    this.div.appendChild (document.createTextNode (s));
    document.body.appendChild (this.div);
    this.div.style.left = "-300px";
    this.div.style.top  = parseInt (window.innerHeight - 100 - Bubble.items.length * 65) + "px";
    var self = this;

    self.div.style.opacity = 1;
    this.interval          = setInterval (function () {
        // self.div.style.top = (parseInt(self.div.style.top ) - 2) + "px";
        self.div.style.left = Math.min ((parseInt (self.div.style.left) + 20), 20) + "px";
        if (parseInt (self.div.style.left) > 0) {
            self.div.style.opacity -= (self.div.style.opacity) / 40;
        }
        if (self.div.style.opacity <= 0.1) {
            clearInterval (self.interval);
            Bubble.remove (self);
            document.body.removeChild (self.div);
        }
    }, 1000 >> 4);
}
CBubble.prototype.increment = function () {
    this.count++;
    this.div.style.opacity = 1;
    this.div.innerHTML     = "<strong>" + this.count + " x </strong>" + this.message;
};

function CBubbleManager () {
    this.items = [];
}
CBubbleManager.prototype.add = function (s) {
    var b;

    if ((b = this.find (s)) == null) {
        this.addNew (s);
    } else {
        b.increment ();
    }
};
CBubbleManager.prototype.addNew = function (s) {
    this.items.push (new CBubble (s));
};
CBubbleManager.prototype.find = function (s) {
    for (var i = 0; i < this.items.length; ++i) {
        if (this.items[i].message == s) {
            return this.items[i];
        }
    }
    
    return null;
};
CBubbleManager.prototype.remove = function (o) {
    this.items.splice (this.items.indexOf (o), 1);
};

Bubble = new CBubbleManager (); // there will be only one
