/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CTooltip () {
    this.div           = $_ ("div");
    this.count         = 1;
    this.div.className = "tooltip";
    this.content       = this.div.appendChild ($_ ("div"));
    document.body.appendChild (this.div);
    var self = this;

    self.div.style.opacity = 1;
    self.visible           = false;
}

CTooltip.prototype.setText = function (s) {
    var tooltips = {
        "stat_health": " <b>Health points<\/b>. Measures of how much damage you can take before dying. Hit points are determined from your overall level and are influenced by the value of your Con stat. Hp value may range between 1 to beyond 500, and higher values indicate a greater ability to withstand punishment.  ",
        "stat_mana": " <b>Mana (Spell points)<\/b>. Measures of how much \"fuel\" you have for casting spells and incantations. Mana is calculated from your highest magic skill school level and your Pow. Mana values can range between 1 to beyond 500 (glowing crystals can increase the current spell points beyond your normal maximum). Higher values indicate greater amounts of mana.  ",
        "stat_grace": " <b>Grace points<\/b> - how favored you are by your god. In game terms, how much divine magic (prayers) you can cast. Your praying level, Wis and Pow effect what the value of grace is. Praying on an altar of your god can increase this value to twice your normal maximum. Grace can take on large positive and negative values. Positive values indicate favor by the gods.  ",
        "stat_food": " <b>Food<\/b>. Ranges between 0 (starving) and 999 (satiated). If it reaches 0 and your character cannot find some food to eat (e.g. in your inventory), it begins to die (health points decrease). Some magic can speed up or slow down the character digestion. Healing wounds will speed up digestion too.  ",
        "stat_exp": " <b>Experience points and overall level<\/b> - experience is increased as a reward for appropriate action (such as killing monsters) and may decrease as a result of a magical attack or dying. Level is directly derived from the experience value. As the level of the character increases, the character becomes able to succeed at more difficult tasks. A character's level starts at a value of 1 and may range up beyond 100.  ",
        "stat_ranged": " <b>Attack Slots<\/b> - how you attack when you fire (shift-direction, spell, skill etc.) - your range slot, or you walk\/run into something - your combat slot. You can apply both a ranged attack weapon (such as a spell, horn, rod, bow etc.) and a combat attack weapon (such as a sword, knife etc.) at the same time, but only one of them - marked with a C<*> - will be wielded at any one time. You can quickly toggle between the two slots by using C<KP-Plus>, but usually the server will automatically switch between the two slots as needed.  ",
        "stat_Str": " <b>Physical Strength<\/b>, determines damage dealt with weapons, how much you can carry, and how often you can attack.  ",
        "stat_Dex": " <b>Dexterity<\/b>, your physical agility. Determines chance of being hit and affects armor class and speed.  ",
        "stat_Con": " <b>Constitution<\/b>, physical health and toughness. Determines how many healthpoints you can have.  ",
        "stat_Int": " <b>Intelligence<\/b>, your ability to learn and use skills and incantations (both prayers and magic) and determines how much spell points you can have.  ",
        "stat_Wis": " <b>Wisdom<\/b>, the ability to learn and use divine magic (prayers). Determines how many grace points you can have.  ",
        "stat_Pow": " <b>Power<\/b>, your magical potential. Influences the strength of spell effects, and also how much your spell and grace points increase when leveling up.  ",
        "stat_Cha": " <b>Charisma<\/b>, how well you are received by NPCs. Affects buying and selling prices in shops, the L<oratory|skill_description\/oratory> and L<singing|skill_description\/singing> skills and a few other things.  ",
        "stat_Wc": " <b>Weapon Class<\/b>, effectiveness of melee\/missile attacks. Lower is more potent. Current weapon, level and Str are some things which effect the value of Wc. The value of Wc may range between 25 and -72.  ",
        "stat_Ac": " <b>Armour Class<\/b>, how protected you are from being hit by any attack. Lower values are better. Ac is based on your race and is modified by the Dex and current armour worn. For characters that cannot wear armour, Ac improves as their level increases.  ",
        "stat_Dam": " <b>Damage<\/b>, how much damage your melee\/missile attack inflicts. Higher values indicate a greater amount of damage will be inflicted with each attack.  ",
        "stat_Arm": " <b>Armour<\/b>, how much damage (from physical attacks) will be subtracted from successful hits made upon you. This value ranges between 0 to 99%. Current armour worn primarily determines Arm value. This is the same as the physical resistance.  ",
        "stat_Spd": " <b>Speed<\/b>, how fast you can move. The value roughly means how many movements per second you can make. It may range between nearly 0 (\"very slow\") to higher than 40 (\"lightning fast\"). Base speed is determined from the Dex stat and modified downward proportionally by the amount of weight carried which exceeds the Max Carry limit. The armour worn also sets the upper limit on speed.  ",
        "stat_WSp": " <b>Weapon Speed<\/b>, how many attacks you may make per second when combatting. Higher values indicate faster attack speed. Current weapon and Dex effect the value of weapon speed.  ",
        "res_slow": "<b>Slow</b> (slows you down when you are hit by the spell. Monsters will have an opportunity to come near you faster and hit you more often.)",
        "res_holyw": "<b>Holy Word</b> (resistance you against getting the fear when someone whose god doesn't like you spells the holy word on you.)",
        "res_conf": "<b>Confusion</b> (If you are hit by confusion you will move into random directions, and likely into monsters.)",
        "res_fire": "<b>Fire</b> (just your resistance to fire spells like burning hands, dragonbreath, meteor swarm fire, ...)",
        "res_depl": "<b>Depletion</b> (some monsters and other effects can cause stats depletion)",
        "res_magic": "<b>Magic</b> (resistance to magic spells like magic missile or similar)",
        "res_drain": "<b>Draining</b> (some monsters (e.g. vampires) and other effects can steal experience)",
        "res_acid": "<b>Acid</b> (resistance to acid, acid hurts pretty much and also corrodes your weapons)",
        "res_pois": "<b>Poison</b> (resistance to getting poisoned)",
        "res_para": "<b>Paralysation</b> (this resistance affects the chance you get paralysed)",
        "res_deat": "<b>Death</b> (resistance against death spells)",
        "res_phys": "<b>Physical</b> (this is the resistance against physical attacks, like when a monster hit you in melee combat. The value displayed here is also displayed as the 'Arm' secondary stat.)",
        "res_blind": "<b>Blind</b> (blind resistance affects the chance of a successful blinding attack)",
        "res_fear": "<b>Fear</b> (this attack will drive you away from monsters who cast this and hit you successfully, being resistant to this helps a lot when fighting those monsters)",
        "res_tund": "<b>Turn undead</b> (affects your resistancy to various forms of 'turn undead' spells. Only relevant when you are, in fact, undead...",
        "res_elec": "<b>Electricity</b> (resistance against electricity, spells like large lightning, small lightning, ...)",
        "res_cold": "<b>Cold</b> (this is your resistance against cold spells like icestorm, snowstorm, ...)",
        "res_ghit":"<b>Ghost hit</b> (special attack used by ghosts and ghost-like beings)"
    };
    var expr = /(#[a-z]*_[^ ]*)/g;
    var matches = s.match (expr);

    if (matches && tooltips[matches[0].replace ("#","")]) {
        s = s.replace (expr, tooltips[matches[0].replace ("#","")]);
    }
    
    this.content.innerHTML = s;
};
Object.defineProperty (CTooltip.prototype, "x", {
    set: function (v) {
        v                   = parseInt (v);
        this._x             = v;
        this.div.style.left = (this._x) + "px";
    },
    get: function () {
        return this._x;
    }
});
Object.defineProperty (CTooltip.prototype, "y", {
    set: function (v) {
        v                  = parseInt (v);
        this._y            = v;
        this.div.style.top = (this._y) + "px";
    },
    get: function () {
        return this._y;
    }
});
Object.defineProperty (CTooltip.prototype, "visible", {
    set: function (v) {
        this.show (v);
        this._visible = v;
    },
    get: function () {
        return this._visble;
    }
});

CTooltip.prototype.show = function (b) {
    this.div.style.display = b ? "block" : "none";
};

CTooltip.prototype.register = function (o, s) {
    var self = this;

    o.addEventListener ("mousemove", (function (tooltip) {
        return function (e) {
            self.setText (typeof (tooltip) === "string" ? tooltip : tooltip.tooltip);
            self.setPosition (e);
            self.visible = true;
        };
    }) (s));
    o.addEventListener ("mouseout", function () {
        self.visible = false;
    });
};

CTooltip.prototype.setPosition = function (e) {
    this.x = e.pageX + 10;
    this.y = Math.min (e.pageY + 10, window.innerHeight - this.div.getBoundingClientRect ().height);
};
