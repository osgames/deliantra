/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CMapRow (size, y, map) {
    CLinkedList.call (this);
    this.map    = map;
    this.length = 0;
    this.cells  = [];
    while (this.cells.length < size) {
        this.cells.push (new CMapCell (this));
        ++this.length;
    }
}
CMapRow.prototype             = Object.create (CLinkedList.prototype);
CMapRow.prototype.constructor = CMapRow;
CMapRow.prototype.clear       = function () {
    for (var i = 0; i < this.cells.length; ++i) {
        this.cells[i].clear ();
    }
};
CMapRow.prototype.findLast = function () {
    return this.cells[this.cells.length - 1];
};
CMapRow.prototype.shift = function () {
    return this.cells.shift ();
};
CMapRow.prototype.push = function (cell) {
    return this.cells.push (cell);
};
CMapRow.prototype.pop = function () {
    return this.cells.pop ();
};
CMapRow.prototype.unshift = function (row) {
    this.cells.unshift (row);
};
CMapRow.prototype.process = function () {};
var xxx = 1;

CMapRow.prototype.get = function (x) {
    /**
     *
     * @type CMapCell;
     */
    return this.cells[x];
};

