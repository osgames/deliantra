/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CCommandConsole (game) {
    this.x               = 0;
    this.y               = 0;
    this.command         = "";
    this.previousCommand = "";
    this.commandHistory = [];
    this.historyPosition = 0;
    this.game            = game;
    this.storedCommand   = "";
    this.window          = new CWindow (0, 0, 0, 0, game, {
        resizeable: false,
        noTitleBar: true
    });
    this.window.show (false);
    this.input = this.window.appendTo.appendChild ($_ ("input"));
    this.window.appendTo.removeChild (this.window.canvas);
    var f = function (e) {
        if (e.keyCode === CKeyboard.KEYS.ENTER || e.keyCode === CKeyboard.KEYS.ENTER2) {
            this.window.show (false);
            this.game.networking.send ("command " + this.input.value);
            this.previousCommand = this.input.value;
            this.commandHistory.push (this.input.value);
            this.historyPosition = this.commandHistory.length;
            this.command         = "";
            this.game.inputText  = false;
        } else if (e.keyCode == CKeyboard.KEYS.UP) {
            if (this.commandHistory.length) {
                this.historyPosition --;
                if (this.historyPosition < 0) {
                    this.historyPosition += this.commandHistory.length;
                }
                this.input.value = this.commandHistory[this.historyPosition];
            }
        } else if (e.keyCode == CKeyboard.KEYS.DOWN) {
            if (this.commandHistory.length) {
                this.historyPosition = (this.historyPosition + 1) % this.commandHistory.length;
                this.input.value = this.commandHistory[this.historyPosition];
            }
        } else {
            if (this.input.value.length === 0) {
                this.window.show (false);
                this.command        = "";
                this.game.inputText = false;
            }
        }
        this.draw ();
    }.bind (this);

    this.input.addEventListener ("blur", function () {
        this.window.show (false);
        this.command        = "";
        this.game.inputText = false;
    }.bind (this));
    this.input.addEventListener ("keydown", f);
    this.input.addEventListener ("keyup", f);
    this.input.addEventListener ("keypress", f);
}

CCommandConsole.prototype.onInput = function (e) {
    var r = false;
    
    if ((e.keyCode == CKeyboard.KEYS.ENTER || e.keyCode == CKeyboard.KEYS.ENTER2) && (this.game.keyboard.getKey (CKeyboard.KEYS.CTR) || e.ctrlKey)) {
        this.game.messagesWindow.input.focus ();
        r = false;
        
        return false;
    }
    // console.log(e.keyCode);
    if (e.key.length == 1) {
        if (e.key == "." && this.command.length == 0 && this.previousCommand.length) {
            this.game.networking.send ("command " + this.previousCommand);
            r = true;
        } else if (this.command.length == 0 && (e.key == "," || !isNaN (e.key))) {
            r = false;
        } else {
            r            = true;
            this.command = "X";
            this.window.show (true);
            this.input.focus ();
            this.input.value = e.key;
            setCaretPosition (this.input, this.input.value.length + 1);
            this.game.inputText = true;
            this.storedCommand  = null;
        }
    }
    /* if (this.command.length && e.keyCode == CKeyboard.KEYS.TAB) {
        
        if (!this.storedCommand) {
            this.storedCommand = this.command;
            this.currentSuggestion = 0;
            if (this.command.indexOf(' ') < 0) {
                this.suggest = 'command';
            }else if (this.command.indexOf('cast') == 0 || this.command.indexOf('invoke') == 0) {
                this.suggest = 'spell';
            }else if (this.command.indexOf('use_skill') == 0 || this.command.indexOf('ready_skill') == 0) {
                this.suggest = 'skill';
            }else if (this.command.indexOf('lock') == 0 ||this.command.indexOf('unlock') == 0 ||this.command.indexOf('apply') == 0 || this.command.indexOf('drop') == 0 || this.command.indexOf('get') == 0) {
                this.suggest = 'item';
            }else {
                this.suggest = 'nothing';
            }
        }
        r = true;
        //console.log(this.suggest);
        switch (this.suggest){
            case "command":
                var s = this.storedCommand;
                var matching = this.findMatch(s, "accept-invitation    afk    apply    applymode    body    bowmode    brace    bumpmsg    cast    chat    cointoss    drop    dropall    examine    follow    get    gsay    help    hintmode    hiscore    ignore    inventory    invite    invoke    killpets    lock    mapinfo    maps    mark    me    motd    orcknuckle    output-count    output-rate    output-sync    party    peaceful    petmode    pickup    prepare    quit    range    ready_skill    rename    resistances    rotateshoottype    save    say    search-items    seen    shout    showpets    skills    sound    sourcecode    statistics    suicide    take    tell    throw    time    title    unignore    unlock    uptime    use_skill    usekeys    version    weather    whereabouts    whereami    who    wimpy".replace(/[ ]{2,*}/g, " ").split(" "));
                if (matching.length) {
                    this.command = matching[this.currentSuggestion=(this.currentSuggestion+1)%matching.length];
                }
                break;
                case "item":
                    var data = [];
                    var s = this.storedCommand.split(" ");
                    var c = s.shift();
                    s = s.join(" ");
                    var playerBag = this.game.containers.first;
                    for (var i = 0; i<playerBag.items.length; ++i) {
                        data.push (playerBag.items[i].name);
                    }
                    var floor = this.game.containers.find(0);
                    for (var i = 0; i<floor.items.length; ++i) {
                        data.push (floor.items[i].name);
                    }
                    var matching = this.findMatch(s, data);
                    if (matching && matching instanceof Array && matching.length) {
                        this.currentSuggestion=(this.currentSuggestion+1)%matching.length;
                        this.command = c + " " + matching[this.currentSuggestion];
                    }
                    
                break;
                case "spell":
                    var data = this.game.mySpells;
                    var s = this.storedCommand.split(" ");
                    var c = s.shift();
                    s = s.join(" ");
                    var matching = this.findMatch(s, data);
                    this.command = c + " " + matching[this.currentSuggestion=(this.currentSuggestion+1)%matching.length];
                    
                break;
                case "skill":
                    var face = this.game.faces ? this.game.faces.find ( this.game.faceIDSkillNames ) : null;
                    var  skillNames = face ? face.data || [] : null;
                    if (skillNames){
                        var s = this.storedCommand.split(" ");
                        var c = s.shift();
                        s = s.join(" ");
                        var matching = this.findMatch(s, skillNames);
                        this.command = c + " " +matching[this.currentSuggestion=(this.currentSuggestion+1)%matching.length];
                    }
                    
                break;
                defualt:
                        this.command = this.storedCommand;
                break;
                
        }
        
    }
    }*/
    // this.draw ();
    e.cancelled = !r;
    
    return r;
};

CCommandConsole.prototype.findMatch = function (txt, data) {
    var matching = [];
    var cmd      = txt;
    var tmp = [];
    var i;
    // console.log("avaible: " + data);

    if (data[0] instanceof Array) {
        tmp = [];

        for (i = 0; i < data.length; ++i) {
            tmp.push (data[i][0]);
        }
        data = tmp;
    } else if (data[0] instanceof CSpell) {
        tmp = [];

        for (i = 0; i < data.length; ++i) {
            tmp.push (data[i].name);
        }
        data = tmp;
    }
    // console.log('searching');
    // console.log(cmd,data);
    for (i = 0; i < data.length; ++i) {
        var match     = true;
        var lastIndex = -1;
        var idx;

        for (var j = 0; j < cmd.length; ++j) {
            // console.log(cmd[j]);
            if ((idx = data[i].indexOf (cmd[j])) < 0) {
                match = false;
                break;
            }
            if (idx < lastIndex) {
                match = false;
                break;
            }
            lastIndex = idx;
        }
        if (match) {
            matching.push (data[i]);
        }
    }

    return matching;
};

CCommandConsole.prototype.draw = function () {
    // var ctx = this.window.ctx;

    this.window.draw ();
    if (this.command.length) {
        var span = $_ ("span", {
            innerHTML: this.input.value
        });

        document.getElementById ("hidden").appendChild (span);
        var w = span.getBoundingClientRect ().width;

        document.getElementById ("hidden").removeChild (span);
        this.window.resize (w, 25);
        this.input.style.width = w + "px";
        this.window.center ();
        this.window.show (true);
    } else {
        this.window.show (false);
    }
};

CCommandConsole.prototype.getPrev = function () {};
CCommandConsole.prototype.hasPrev = function () {};
