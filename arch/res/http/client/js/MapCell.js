/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CMapCell (row) {
    CLinkedList.call (this);
    this.row = row;
    this.clear ();
}

CMapCell.prototype             = Object.create (CLinkedList.prototype);
CMapCell.prototype.constructor = CMapCell;

CMapCell.prototype.clear       = function () {
    this.darkness = 0;
    this.tiles    = [0, 0, 0];
    this.stat_hp  = 0;
    // var pos = this.getXY();
    /*
     *
    if (this.row.map && pos){
        this.row.map.clearRect(pos);
    }
    */

    return this;
};

CMapCell.prototype.draw = function (pos) {
    if (this.row.map && pos) {
        this.row.map.renderTiles (pos, this.tiles);
        this.row.map.renderDarkness (this.darkness, pos);
    }
    
    return this;
};

CMapCell.prototype.process = function () {
    // CLinkedList.prototype.process.apply(this);
};

