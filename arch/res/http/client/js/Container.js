/*
 *  This file is part of Deliantra clientV.
 *
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CContainer (id) {
    this.id    = id;
    this.items = [];
    CLinkedList.call (this);
}
CContainer.prototype             = Object.create (CLinkedList.prototype);
CContainer.prototype.constructor = CContainer;
CContainer.prototype.clear       = function () {
    this.items = [];
    
    return this;
};
CContainer.prototype.add = function (item, setCurrentContainer) {
    setCurrentContainer = setCurrentContainer || false;
    if (setCurrentContainer)
        item.containerObject = this;
    this.items.push (item);
    
    return this;
};
CContainer.prototype.remove = function (id) {
    // console.log ("removing item " + id + " from " + this.id);
    for (var i = 0; i < this.items.length; ++i) {
        if (this.items[i].itemId == id) {
            this.items.splice (i, 1);
            break;
        }
    }
};
CContainer.prototype.find = function (id) {
    for (var i = 0; i < this.items.length; ++i) {
        if (this.items[i].itemId == id) {
            return this.items[i];
        }
    }
    
    return null;
};
CContainer.prototype.findByName = function (name) {
    for (var i = 0; i < this.items.length; ++i) {
        if (this.items[i].name.indexOf (name) >= 0) {
            return this.items[i];
        }
    }
    
    return null;
};

