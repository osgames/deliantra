#! perl

# the settings page in the player book

[
   ScrolledWindow => {
      s_id     => "page",
      c_tab    => ["Settings", "Various Character Settings"],
      expand   => 1,
      scroll_y => 1,
      s_cl     => [
         VBox => {
            s_cl => [
               FancyFrame => {
                  label => "Character",
                  s_cl => [
                     Table => {
                        col_expand => [0, 0.1, 1],
                        s_cl => [
                           Label => {
                              c_col => 0, c_row => 0,
                              text  => "Title: ",
                           },
                           Entry => {
                              s_id    => "title",
                              c_col   => 1, c_row => 0, c_colspan => 2,
                              tooltip => "If set, this shows up behind your name in player listings (it is customary, "
                                       . "but not required, to start with a <i>the</i>). "
                                       . "You need to press <b>Save</b> after changing this field.",
                           },
                           Label => {
                              c_col => 0, c_row => 1,
                              text  => "Info: ",
                           },
                           TextEdit => {
                              s_id    => "info",
                              c_col   => 1, c_row => 1, c_colspan => 2,
                              tooltip => "You can enter anything you like here that describes your character, or even yourself. "
                                       . "You need to press <b>Save</b> after changing this field.",
                           },
                           Button => {
                              s_id => "info_save",
                              c_col => 1, c_row => 2,
                              text => "Save",
                           },
                        ],
                     },
                  ],
               },
               FancyFrame => {
                  label => "Mode Settings",
                  s_cl => [
                     Table => {
                        col_expand => [0.1, 0.2, 0.6, 0.1, 0.2, 1.0],
                        s_cl       => [
                           Label => {
                              c_col => 0, c_row => 0,
                              text  => "Apply Mode",
                           },
                           Selector => {
                              s_id => "applymode",
                              c_col => 1, c_row => 0,
                              tooltip => "Applymode controls what happens when you are equipping something that would require something else to be unequipped.",
                              options => [
                                 [0 => "nochoice", "nochoice (only unapply an item if there is no other candidate)"],
                                 [1 => "never",    "never (never automatically unapply any items)"],
                                 [2 => "always",   "always (unapply whatever needs to be unapplied to apply an item)"],
                              ],
                           },

                           Label => {
                              c_col => 0, c_row => 1,
                              text  => "Hint Mode",
                           },
                           Selector => {
                              s_id => "hintmode",
                              c_col => 1, c_row => 1,
                              tooltip => "Controls if and how much additional hints the server shows",
                              options => [
                                 [0 => "show" , "show (default mode)"],
                                 [1 => "mark" , "mark (do not show hints, but notify you when a hint would have been displayed)"],
                                 [2 => "hide" , "hide (hide all hints, and even hints of hints)"],
                              ],
                           },

                           Label => {
                              c_col => 0, c_row => 2,
                              text  => "Pet Mode",
                           },
                           Selector => {
                              s_id => "petmode",
                              c_col => 1, c_row => 2,
                              tooltip => "Controls how your pets/charmed monsters will behave",
                              options => [
                                 [0 => "normal", "normal (normal behaviour for that monster)"],
                                 [1 => "sad"   , "search and destroy (pets will roam and attack freely)"],
                                 [2 => "defend", "defend (pets will try to stay close and defend you)"],
                                 [3 => "arens" , "arena (like normal, but also attack other players)"],
                              ],
                           },

                           Label => {
                              c_col => 3, c_row => 0,
                              text  => "Bow Mode",
                           },
                           Selector => {
                              s_id => "bowmode",
                              c_col => 4, c_row => 0,
                              tooltip => "Bowmode controls how you will fire arrows and bolts.",
                              options => [
                                 [ 0 => "normal"    , "normal (fire a single shot)"],
                                 [ 1 => "threewide" , "threewide (fires three arrows in parallel)"],
                                 [ 2 => "spreadshot", "spreadshot (fires three arrows which spread out over distance)"],
                                 [11 => "bestarrow" , "bestarrow (selects and fires the probable best arrow from your inventory)"],
                              ],
                           },

                           Label => {
                              c_col => 3, c_row => 1,
                              text  => "Key Mode",
                           },
                           Selector => {
                              s_id => "usekeys",
                              c_col => 4, c_row => 1,
                              tooltip => "Controls how, when and if to use keys automatically",
                              options => [
                                 [0 => "inventory" , "inventory (only keys in the top level inventory are used)"],
                                 [1 => "keyrings"  , "keyrings (only keys in active keyrings will be used)"],
                                 [2 => "containers", "containers (only keys in active containers will be used)"],
                              ],
                           },

#                           Label => {
#                              c_col => 3, c_row => 2,
#                              text  => "Bump Msg",
#                           },
#                           Selector => {
#                              s_id => "bumpmsg",
#                              c_col => 4, c_row => 2,
#                              tooltip => "Controls whether bumping into walls generates server messages",
#                              options => [
#                                 [0 => "off", "off (bumping into walls will be silent)"],
#                                 [1 => "on" , "on (bumping into walls generate a message)"],
#                              ],
#                           },
                        ],
                     },
                  ],
               },
            ],
         },
      ],
   },
]
