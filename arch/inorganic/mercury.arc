object fix_mercury
anim
fix_mercury.x11
fix_mercury.x12
mina
name block
name_pl blocks
title of fixed mercury
face fix_mercury.x11
speed 0.5
nrof 1
type 73
resist_electricity 100
resist_acid 100
materialname mercury
value 400
weight 8000
client_type 642
end

object mercury
name_pl mercuries
face mercury.x11
nrof 1
type 73
resist_electricity 100
resist_acid 100
materialname mercury
value 210
weight 3000
client_type 642
identified 1
end

