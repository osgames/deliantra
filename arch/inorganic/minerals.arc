object cinnabar
name pile
name_pl piles
title of cinnabar
face cinna.x11
nrof 1
type 73
materialname cinnabar
value 70
weight 3000
client_type 641
end

object graphite
name pile
name_pl piles
title of graphite
face graphite.x11
nrof 1
type 73
resist_fire -100
resist_electricity -100
resist_acid 100
materialname graphite
value 1
weight 500
client_type 641
end

object gypsum
name pile
name_pl piles
title of gypsum
face gypsum.x11
nrof 1
type 73
resist_acid 100
materialname gypsum
value 9
weight 1500
client_type 641
end

object min_oil
name bottle
name_pl bottles
title of mineral oil
face min_oil.x11
nrof 1
type 73
resist_fire -100
materialname glass
value 40
weight 2500
client_type 641
end

object phosphorus
name pile
name_pl piles
title of phosphorus
face phosphor.x11
nrof 1
type 73
resist_fire -100
resist_electricity -100
resist_acid 100
materialname phosphorus
value 45
weight 1000
client_type 641
end

object pyrite
name pile
name_pl piles
title of pyrite
face pyrite.x11
nrof 1
type 73
materialname pyrite
value 8
weight 40
client_type 641
end

object pyrite2
name large clump
name_pl large clumps
title of pyrite
face pyrite2.x11
nrof 1
type 73
materialname pyrite
value 800
weight 2500
client_type 641
end

object pyrite3
name enormous clump
name_pl enormous clumps
title of pyrite
face pyrite3.x11
nrof 1
type 73
materialname pyrite
value 8000
weight 20000
client_type 641
end

object salt
name pile
name_pl piles
title of salt
face salt.x11
nrof 1
type 73
materialname salt
value 10
weight 500
client_type 641
end

object sulphur
name pile
name_pl piles
title of sulphur
face sulphur.x11
nrof 1
type 73
resist_fire -100
resist_acid 100
materialname sulphur
value 15
weight 500
client_type 641
end

