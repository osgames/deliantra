object depleteduraniumpile
name pile
name_pl piles
title of depleted uranium
face uranium.x11
nrof 1
type 73
materialname depleted uranium
value 10
weight 5000
client_type 641
end

object enricheduraniumpile
name pile
name_pl piles
title of enriched uranium
face uranium.x11
nrof 1
type 73
materialname enriched uranium
value 4000
weight 5000
glow_radius 1
client_type 641
end

object uraniumpile
name pile
name_pl piles
title of uranium
face uranium.x11
nrof 1
type 73
materialname uranium
value 25
weight 5000
client_type 641
end

