object vial_poison
name vial
name_pl vials
title poison
slaying taint
msg
A poison of some sort.
Maybe it could be used
to taint someone's food.
endmsg
face vial_yellow.x11
food 2
nrof 1
type 117
materialname glass
value 1000
weight 100
client_type 649
end

