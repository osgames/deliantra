object phil_oil
name bottle
name_pl bottles
title of philosophical oil
face min_oil.x11
nrof 1
type 73
resist_fire -100
materialname glass
value 280
weight 500
client_type 642
end

object phil_phosphorus
anim
phil_phos.x11
phil_phos.x12
phil_phos.x13
mina
name pile
name_pl piles
title of philosophical phosphorus
face phil_phos.x11
speed -0.45
nrof 1
type 73
resist_fire -100
resist_electricity -100
resist_acid 100
materialname stone
value 350
weight 1000
client_type 642
end

object phil_salt
anim
phil_salt.x11
phil_salt.x12
phil_salt.x13
mina
name pile
name_pl piles
title of philosophical salt
face phil_salt.x11
speed -0.30
nrof 1
type 73
materialname stone
value 80
weight 500
client_type 642
end

object phil_sulphur
anim
phil_sulphur.x11
phil_sulphur.x12
phil_sulphur.x13
mina
name pile
name_pl piles
title of philosophical sulphur
face phil_sulphur.x11
speed -0.35
nrof 1
type 73
resist_fire -100
resist_acid 100
materialname stone
value 110
weight 500
client_type 642
end

