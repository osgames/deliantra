object field_stone
name stone
name_pl stones
face lstone.x11
nrof 1
type 73
resist_acid 100
materialname stone
value 1
weight 800
client_type 641
end

object mountain_stone
name mountain stone
name_pl mountain stones
face lstone.x11
nrof 1
type 73
resist_acid 100
materialname stone
value 1
weight 800
client_type 641
end

object river_stone
name river stone
name_pl river stones
face lstone.x11
nrof 1
type 73
resist_acid 100
materialname stone
value 1
weight 800
client_type 641
end

