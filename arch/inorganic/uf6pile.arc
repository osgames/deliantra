object duf6pile
name pile
name_pl piles
title of depleted uranium hexafluoride
face uf6pile.x11
nrof 1
type 73
materialname uranium
value 5
weight 5000
client_type 641
end

object euf6pile
name pile
name_pl piles
title of enriched uranium hexafluoride
face uf6pile.x11
nrof 1
type 73
materialname uranium
value 200
weight 5000
client_type 641
end

object uf6pile
name pile
name_pl piles
title of uranium hexafluoride
face uf6pile.x11
nrof 1
type 73
materialname uranium
value 77
weight 5000
client_type 641
end

