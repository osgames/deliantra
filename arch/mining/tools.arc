object pickaxe
inherit class_vein_extractor
name pickaxe
name_pl pickaxes
value 1000000
materialname steel
weight 7000
skill mining
client_type 451
body_arm -2
wc 50
speed 0.1
face pickaxe.x11
msg
This pickaxe can be used to extract minerals from veins in mines. Because
it's the most basic tool for mining it's a good pick for the aspiring
as well as for the experienced miner. Don't be deterred by its price, the
money is well spent and can make you rich in no time!

H<This pickaxe lets you use the mining skill. The main use is to extract
minerals that can be sold or used for weapon or jewelery improvements. To
use it, apply it as your range weapon and fire towards walls or other
areas where you expect veins to be present. B<Note that pickaxes require
an arm wield them, so make sure that your race can use it.>>
endmsg
end

object pickaxe_heavy
inherit pickaxe
weight 14000
wc 90
speed 0.05
msg
This pickaxe can be used to extract minerals from veins in mines. Because
it's the most basic tool for mining it's a good pick for the aspiring
as well as for the experienced miner. Don't be deterred by its price, the
money is well spent and can make you rich in no time!

The weight of this pickaxe incraeses chances of finding something, but
will also make mining slower.

H<This pickaxe lets you use the mining skill. The main use is to extract
minerals that can be sold or used for weapon or jewelery improvements. To
use it, apply it as your range weapon and fire towards walls or other
areas where you expect veins to be present. B<Note that pickaxes require
an arm wield them, so make sure that your race can use it.>>
endmsg
end

