object vein_generic
inherit type_vein
end

object vein_brazilianite
inherit type_vein
other_arch brazilianite
end

object vein_dolomite
inherit type_vein
other_arch dolomite
end

object vein_flourite
inherit type_vein
other_arch flourite
end

object vein_marcasite
inherit type_vein
other_arch marcasite
end

object vein_olivine
inherit type_vein
other_arch olivine
end

object vein_phlogopite
inherit type_vein
other_arch phlogopite
end

object vein_rhodochrosite
inherit type_vein
other_arch rhodochrosite
end

object vein_spectrolite
inherit type_vein
other_arch spectrolite
end

