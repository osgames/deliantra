object hammer
inherit type_weapon
name_pl hammers
skill one handed weapons
face hammer_1.x11
dam 7
nrof 1
attacktype 1
materialname bronze
value 25
weight 17500
last_sp 9
weapontype 7
client_type 126
body_arm -1
body_combat -1
end

