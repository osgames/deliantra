object stonehammer
inherit type_weapon
name stonehammer
name_pl stonehammers
skill one handed weapons
face stonehamme.x11
dam 6
nrof 1
attacktype 1
materialname stone
value 13
weight 15000
last_sp 10
weapontype 7
client_type 126
body_arm -1
body_combat -1
end

