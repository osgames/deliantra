object hammer_2
anim
hammer_2.x01
hammer_2.x11
hammer_2.x21
hammer_2.x31
hammer_2.x41
hammer_2.x51
hammer_2.x61
hammer_2.x71
hammer_2.x81
mina
inherit type_weapon
name throwing hammer
name_pl throwing hammers
skill one handed weapons
face hammer_2.x21
is_animated 0
dam 6
nrof 1
attacktype 1
materialname stone
value 23
weight 15500
last_sp 9
weapontype 7
client_type 126
is_thrown 1
is_turnable 1
body_arm -1
body_combat -1
end

