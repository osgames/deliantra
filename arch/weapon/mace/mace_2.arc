object mace_2
inherit type_weapon
name mace
name_pl maces
skill one handed weapons
face mace_2.x11
dam 8
nrof 1
attacktype 1
materialname bronze
value 42
weight 17500
last_sp 9
weapontype 7
client_type 129
body_arm -1
body_combat -1
end

