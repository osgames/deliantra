object mace
inherit type_weapon
name_pl maces
skill one handed weapons
face mace_1.x11
dam 7
nrof 1
attacktype 1
materialname iron
value 35
weight 16000
last_sp 9
weapontype 7
client_type 129
body_arm -1
body_combat -1
end

