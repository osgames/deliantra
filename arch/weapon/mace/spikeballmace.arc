object spikeballmace
inherit type_weapon
name spike ball mace
name_pl spike ball maces
skill one handed weapons
face spikeballmace.x11
dam 12
nrof 1
attacktype 1
materialname bronze
value 120
weight 22500
last_sp 10
weapontype 7
client_type 129
body_arm -1
body_combat -1
end

