object pipe
inherit type_weapon
name pipe
name_pl pipes
skill one handed weapons
face pipe.x11
dam 5
nrof 1
attacktype 1
materialname iron
value 10
weight 6500
last_sp 8
weapontype 8
client_type 145
body_arm -1
body_combat -1
end

