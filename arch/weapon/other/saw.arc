object saw
inherit type_weapon
name saw
name_pl saws
slaying deathtree
skill one handed weapons
face saw.x11
dam 4
nrof 1
attacktype 1
materialname bronze
value 35
weight 6500
last_sp 7
client_type 145
body_arm -1
body_combat -1
end

