object wrench
inherit type_weapon
name wrench
name_pl wrenches
skill one handed weapons
face wrench.x11
dam 3
nrof 1
attacktype 1
materialname iron
value 10
weight 3500
last_sp 6
weapontype 8
client_type 145
body_arm -1
body_combat -1
end

