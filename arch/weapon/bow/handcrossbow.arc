object handcrossbow
inherit type_bow
name crossbow pistol
name_pl crossbow pistols
race bolt
skill missile weapons
face handcrossbow.x11
sp 30
dam 5
wc 1
nrof 1
attacktype 1
materialname wood
value 52800
weight 1500
client_type 161
no_strength 1
body_arm -1
end

object handcrossbow_ivory
inherit type_bow
name crossbow pistol
name_pl crossbow pistols
race bolt
skill missile weapons
face handcrossbow_bone.x11
sp 30
dam 5
wc 1
nrof 1
attacktype 1
materialname wood
value 130050
weight 1500
client_type 161
no_strength 1
body_arm -1
end

