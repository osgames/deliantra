object reflexbow
inherit type_bow
name reflex bow
name_pl reflex bows
race arrow
skill missile weapons
face reflexbow.x11
sp 47
dam 23
nrof 1
attacktype 1
materialname wood
value 25000
weight 6000
client_type 151
no_strength 0
body_arm -2
end

object reflexbow_bone
inherit type_bow
name reflex bow
name_pl reflex bows
race arrow
skill missile weapons
face reflexbow_bone.x11
sp 42
dam 32
nrof 1
attacktype 1
materialname bone
value 10000
weight 8000
client_type 151
no_strength 0
body_arm -2
end

object reflexbow_ivory
inherit type_bow
name reflex bow
name_pl reflex bows
race arrow
skill missile weapons
face reflexbow_bone.x11
sp 47
dam 23
nrof 1
attacktype 1
materialname ivory
value 65000
weight 6000
client_type 151
no_strength 0
body_arm -2
end

