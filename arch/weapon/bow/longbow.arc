object longbow
inherit type_bow
name long bow
name_pl long bows
race arrow
skill missile weapons
face longbow.x11
sp 40
dam 20
nrof 1
attacktype 1
materialname wood
value 500
weight 8000
client_type 151
no_strength 0
body_arm -2
end

