object arrow_petrify
anim
arrow_petrify.x01
arrow_petrify.x11
arrow_petrify.x21
arrow_petrify.x31
arrow_petrify.x41
arrow_petrify.x51
arrow_petrify.x61
arrow_petrify.x71
arrow_petrify.x81
mina
name arrow
name_pl arrows
title of petrification
race arrows
face arrow_petrify.x01
is_animated 0
food 20
dam 1
wc 1
nrof 1
type 13
attacktype 1
materialname granite
value 50
weight 25
randomitems arrow_petrify
client_type 159
is_turnable 1
need_an 1
end

