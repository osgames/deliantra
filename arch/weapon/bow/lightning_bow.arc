object lightning_bow
inherit type_bow
name lightning bow
name_pl lightning bows
race bolt
skill missile weapons
face lightning_bow.x11
sp 40
dam 14
wc -2
nrof 1
attacktype 9
materialname wood
value 45000
weight 25000
magic 3
client_type 150
no_strength 1
body_arm -2
end

