object firebombarrow
anim
firebombarrow.x01
firebombarrow.x11
firebombarrow.x21
firebombarrow.x31
firebombarrow.x41
firebombarrow.x51
firebombarrow.x61
firebombarrow.x71
firebombarrow.x81
mina
name_pl firebombarrows
race arrows
face firebombarrow.x01
is_animated 0
food 20
dam 30
wc 1
nrof 1
type 13
attacktype 5
materialname wood
value 6800
weight 50
client_type 159
is_turnable 1
need_an 1
end

