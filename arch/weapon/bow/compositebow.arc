object compositebow
inherit type_bow
name composite bow
name_pl composite bows
race arrow
skill missile weapons
face compositebow.x11
sp 35
dam 14
nrof 1
attacktype 1
materialname wood
value 100
weight 3000
client_type 151
no_strength 0
body_arm -2
end

