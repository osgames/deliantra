object magic_bow
anim
magic_bow.x11
magic_bow.x12
magic_bow.x13
magic_bow.x14
mina
inherit type_bow
name Energy Bow
name_pl Energy Bows
race arrow
skill missile weapons
face magic_bow.x11
sp 80
dam 30
wc 3
speed 0.1
nrof 1
attacktype 1
materialname wood
value 1700000
weight 9000
magic 7
client_type 151
no_strength 0
body_arm -2
end

