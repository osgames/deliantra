object thundar_bolt
anim
thunder_bolt.x01
thunder_bolt.x11
thunder_bolt.x21
thunder_bolt.x31
thunder_bolt.x41
thunder_bolt.x51
thunder_bolt.x61
thunder_bolt.x71
thunder_bolt.x81
mina
name thundar bolt
name_pl thundar bolts
race bolt
face thunder_bolt.x01
is_animated 0
food 10
dam 100
wc -10
nrof 50
type 13
attacktype 9
materialname iron
value 20000
weight 1000
client_type 165
is_turnable 1
end

