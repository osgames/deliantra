object huntersbow
inherit type_bow
name hunter's bow
name_pl hunter's bows
race arrow
skill missile weapons
face huntersbow.x11
sp 20
dam 10
nrof 1
attacktype 1
materialname wood
value 50
weight 2000
client_type 150
no_strength 0
body_arm -2
end

