object elven_bow
inherit type_bow
name elven bow
name_pl elven bows
race arrow
skill missile weapons
face elven_bow.x11
sp 70
dam 15
wc 1
nrof 1
attacktype 1
materialname wood
value 17000
weight 15000
magic 3
client_type 150
no_strength 0
body_arm -2
end

