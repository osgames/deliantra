object bow
inherit type_bow
name_pl bows
race arrow
skill missile weapons
face bow.x11
sp 50
dam 7
nrof 1
attacktype 1
materialname wood
value 50
weight 12000
client_type 151
no_strength 0
body_arm -2
end

