object bolt
anim
bolt.x01
bolt.x11
bolt.x21
bolt.x31
bolt.x41
bolt.x51
bolt.x61
bolt.x71
bolt.x81
mina
name_pl bolts
race bolt
face bolt.x01
is_animated 0
food 10
dam 3
wc 2
nrof 1
type 13
attacktype 1
materialname iron
value 8
weight 50
client_type 165
is_turnable 1
end

