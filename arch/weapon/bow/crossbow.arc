object crossbow
inherit type_bow
name_pl crossbows
race bolt
skill missile weapons
face crossbow.x11
sp 19
dam 15
wc 1
nrof 1
attacktype 1
materialname wood
value 75
weight 25000
client_type 161
no_strength 1
body_arm -2
end

