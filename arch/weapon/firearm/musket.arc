object musket
inherit type_bow
name muskete
name_pl musketen
race musket balls
skill missile weapons
face musket.x11
sp 10
dam 64
wc 1
nrof 1
attacktype 1
materialname wood
value 1500
weight 45000
client_type 161
no_strength 1
body_arm -2
end

