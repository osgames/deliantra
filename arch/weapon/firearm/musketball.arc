object musketball
anim
musketball.x01
musketball.x11
musketball.x21
musketball.x31
musketball.x41
musketball.x51
musketball.x61
musketball.x71
musketball.x81
mina
name musketekugel
name_pl musketekugeln
race musket balls
face musketball.x01
is_animated 0
food 80
dam 40
wc 2
nrof 1
type 13
attacktype 1
materialname lead
value 1
weight 200
client_type 165
is_turnable 1
end

