object magi_staff
anim
magi_staff.x11
magi_staff.x12
magi_staff.x13
magi_staff.x12
mina
inherit type_weapon
name Staff of the Magi
name_pl Staves of the Magi
skill two handed weapons
face magi_staff.x11
pow 1
int 1
sp 2
dam 20
wc 5
speed .2
nrof 1
attacktype 257
resist_magic 30
materialname wood
value 500000
weight 4500
magic 5
weapontype 8
client_type 100
item_power 15
body_arm -2
body_combat -1
end

