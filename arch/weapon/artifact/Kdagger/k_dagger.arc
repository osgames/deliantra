object k_dagger
inherit type_weapon
name Kobold Dagger
name_pl Kobold Daggers
slaying troll
skill one handed weapons
face k_dagger.x11
dam 6
speed 0.100000
nrof 1
attacktype 1
materialname iron
value 60000
weight 6000
magic 3
last_sp 2
weapontype 2
client_type 100
item_power 4
body_arm -1
body_combat -1
end

