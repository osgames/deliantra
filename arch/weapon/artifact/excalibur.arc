object excalibur
inherit type_weapon
name Excalibur
name_pl Excaliburs
skill two handed weapons
face excalibur.x11
str 1
cha 2
dam 12
nrof 1
attacktype 256
resist_magic 30
resist_drain 100
materialname iron
value 220000
weight 25000
magic 5
last_sp 7
weapontype 1
client_type 100
item_power 15
body_arm -2
body_combat -1
end

