object stormbringer
inherit type_weapon
name Stormbringer
name_pl Stormbringers
skill two handed weapons
face stormbring.x11
str 1
hp 1
dam 9
nrof 1
attacktype 136
resist_magic 30
resist_electricity 30
resist_drain 100
materialname iron
value 180000
weight 25000
magic 4
last_sp 8
weapontype 1
client_type 100
item_power 20
body_arm -2
body_combat -1
end

