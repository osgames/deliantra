object dagger_f
anim
dagger_f.x11
dagger_f.x12
dagger_f.x13
dagger_f.x14
mina
inherit type_weapon
name dagger of fortune
name_pl daggers of fortune
skill one handed weapons
face dagger_f.x11
cha 4
dam 2
luck 3
speed .2
nrof 1
attacktype 1
materialname glacium
value 50000
weight 2000
last_sp 5
weapontype 2
client_type 100
item_power 5
body_arm -1
body_combat -1
end

