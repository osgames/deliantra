object defender
inherit type_weapon
name Defender
name_pl Defenders
skill one handed weapons
face defender.x11
dam 8
ac -1
nrof 1
attacktype 1
resist_physical 50
resist_drain 50
materialname iron
value 150000
weight 20000
magic 3
last_sp 8
weapontype 1
client_type 100
item_power 5
body_arm -1
body_combat -1
end

