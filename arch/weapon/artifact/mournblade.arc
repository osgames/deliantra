object mournblade
inherit type_weapon
name Mournblade
name_pl Mournblades
skill two handed weapons
face mournblade.x11
str 1
sp 1
dam 9
nrof 1
attacktype 384
resist_magic 30
resist_drain 100
materialname iron
value 170000
weight 25000
magic 4
last_sp 8
weapontype 1
client_type 100
item_power 15
body_arm -2
body_combat -1
end

