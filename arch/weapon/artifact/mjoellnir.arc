object mjoellnir
anim
mjoellnir.x11
mjoellnir.x12
mjoellnir.x13
mjoellnir.x14
mjoellnir.x15
mjoellnir.x16
mjoellnir.x17
mjoellnir.x18
mjoellnir.x19
mjoellnir.x21
mjoellnir.x22
mjoellnir.x23
mina
inherit type_weapon
name Mjoellnir
name_pl Mjoellnirs
skill two handed weapons
face mjoellnir.x11
str 1
dam 10
speed 2
nrof 1
attacktype 8
resist_electricity 30
materialname iron
value 75000
weight 30000
magic 3
last_sp 8
weapontype 7
client_type 100
item_power 6
body_arm -2
body_combat -1
end

