object holy_avenger
anim
holyave.x11
holyave.x12
holyave.x13
holyave.x14
holyave.x15
holyave.x16
holyave.x17
holyave.x18
holyave.x19
holyave.x1A
holyave.x1B
holyave.x1C
mina
inherit type_weapon
name Holy Avenger
name_pl Holy Avengers
skill two handed weapons
face holyave.x11
str 1
wis 2
cha 2
dam 15
speed 0.2
nrof 1
attacktype 4194560
resist_magic 30
resist_drain 100
value 220000
weight 25000
magic 5
last_sp 9
weapontype 1
client_type 100
item_power 25
body_arm -2
body_combat -1
end

