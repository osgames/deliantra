object uk_sd
anim
uk_sd.x11
uk_sd.x12
uk_sd.x13
uk_sd.x14
uk_sd.x15
uk_sd.x16
uk_sd.x17
mina
inherit type_weapon
name Unknown Sword
name_pl Unknown Swords
skill two handed weapons
face uk_sd.x11
str 2
dex 2
dam 40
speed .4
nrof 1
attacktype 1
materialname iron
value 1000000
weight 111000
last_sp 6
weapontype 1
client_type 100
body_arm -2
body_combat -1
end

