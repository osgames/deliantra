object skullcleaver
anim
skullcleav.x11
skullcleav.x12
skullcleav.x13
skullcleav.x14
mina
inherit type_weapon
name Skullcleaver
name_pl Skullcleavers
slaying skull
skill two handed weapons
face skullcleav.x11
dam 20
speed 0.1
nrof 1
attacktype 1
materialname bronze
value 80000
weight 32000
magic 3
last_sp 11
weapontype 3
client_type 100
item_power 4
body_arm -2
body_combat -1
end

