object uw_sword
anim
uw_sword.x11
uw_sword.x12
uw_sword.x13
uw_sword.x14
uw_sword.x15
mina
inherit type_weapon
name Belzebub's sword
name_pl Belzebub's swords
slaying angel
skill two handed weapons
face uw_sword.x11
con 2
int 1
sp 1
dam 17
speed 0.1
nrof 1
attacktype 65
resist_magic 30
resist_confusion 100
resist_drain 100
materialname abyssium
value 175000
weight 30000
magic 5
last_sp 7
weapontype 1
client_type 100
item_power 40
body_arm -2
body_combat -1
end

