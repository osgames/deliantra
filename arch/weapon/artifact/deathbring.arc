object deathbringer
inherit type_weapon
name Deathbringer
name_pl Deathbringers
skill two handed weapons
face deathbring.x11
dam 15
nrof 1
attacktype 65537
materialname iron
value 55000
weight 30000
magic 3
last_sp 9
weapontype 1
client_type 100
body_arm -2
body_combat -1
end

