object firestar
anim
firestar.x11
firestar.x12
firestar.x11
firestar.x12
firestar.x11
firestar.x12
firestar.x13
mina
inherit type_weapon
name Firestar
name_pl Firestars
title named Fearless
skill two handed weapons
face firestar.x11
dam 35
speed 0.30
nrof 1
attacktype 261
resist_fire 30
resist_drain 100
resist_fear 100
materialname iron
value 400000
weight 40000
magic 6
last_sp 9
weapontype 7
client_type 100
item_power 25
body_arm -2
body_combat -1
end

