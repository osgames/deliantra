object lamestar
anim
firestar.x11
firestar.x12
firestar.x11
firestar.x12
firestar.x11
firestar.x12
firestar.x13
mina
inherit type_weapon
name Firestar
name_pl Firestars
skill two handed weapons
face firestar.x11
dam 23
speed 0.2
nrof 1
attacktype 5
materialname iron
value 400000
weight 40000
magic 3
last_sp 9
weapontype 7
client_type 100
body_arm -2
body_combat -1
end

