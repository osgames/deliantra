object lava_s
anim
lava_s.x13
lava_s.x12
lava_s.x11
mina
inherit type_weapon
name Lava Slasher
name_pl Lava Slashers
skill two handed weapons
face lava_s.x11
str 2
con 2
dam 30
speed 0.250000
nrof 1
attacktype 5
resist_cold 75
materialname magmasium
value 220000
weight 25000
magic 3
last_sp 9
weapontype 1
client_type 100
item_power 15
body_arm -2
body_combat -1
end

