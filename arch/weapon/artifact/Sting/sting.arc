object sting
anim
sting.x11
sting.x12
sting.x13
sting.x14
sting.x13
sting.x12
mina
inherit type_weapon
name Sting
name_pl Stings
slaying troll
skill one handed weapons
face sting.x11
dam 5
speed 0.1
nrof 1
attacktype 1
materialname iron
value 42000
weight 6000
magic 3
last_sp 6
weapontype 2
client_type 100
item_power 4
body_arm -1
body_combat -1
end

