object demonbane
anim
demonbane.x11
demonbane.x12
demonbane.x13
demonbane.x14
demonbane.x15
demonbane.x16
demonbane.x17
demonbane.x18
demonbane.x19
mina
inherit type_weapon
name Demonbane
name_pl Demonbanes
slaying demon
skill two handed weapons
face demonbane.x11
dam 11
speed 0.2
nrof 1
attacktype 1
resist_fire 30
materialname iron
value 90000
weight 32000
magic 3
last_sp 9
weapontype 1
client_type 100
item_power 7
body_arm -2
body_combat -1
end

