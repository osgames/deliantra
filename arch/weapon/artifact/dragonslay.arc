object dragonslayer
inherit type_weapon
name Dragonslayer
name_pl Dragonslayers
slaying dragon
skill two handed weapons
face dragonslay.x11
dam 11
nrof 1
attacktype 1
resist_fire 30
materialname iron
value 85000
weight 35000
magic 3
last_sp 9
weapontype 1
client_type 100
item_power 7
body_arm -2
body_combat -1
end

