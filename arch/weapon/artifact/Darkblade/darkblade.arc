object darkblade
anim
darkblade.x11
darkblade.x12
darkblade.x13
darkblade.x14
darkblade.x15
darkblade.x16
darkblade.x17
darkblade.x16
darkblade.x15
darkblade.x14
darkblade.x13
darkblade.x12
darkblade.x11
mina
inherit type_weapon
name Darkblade
name_pl Darkblades
skill one handed weapons
face darkblade.x11
con 2
cha -1
hp 1
dam 10
speed 0.2
nrof 1
attacktype 256
resist_drain 100
resist_poison 30
value 143000
weight 20000
magic 4
last_sp 7
weapontype 1
client_type 100
item_power 15
body_arm -1
body_combat -1
end

