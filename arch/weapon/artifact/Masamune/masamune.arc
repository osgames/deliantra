object masamune
anim
masamune.x13
masamune.x12
masamune.x11
masamune.x14
masamune.x14
mina
inherit type_weapon
name Katana of Masamune
name_pl Katanas of Masamune
slaying undead
skill one handed weapons
face masamune.x11
str 2
dex 2
con 2
wis 1
dam 25
speed .2
nrof 1
attacktype 257
resist_drain 100
resist_ghosthit 100
resist_deplete 30
materialname iron
value 220000
weight 9000
magic 3
last_sp 3
weapontype 4
client_type 100
item_power 40
body_arm -1
body_combat -1
end

