object frostbrand
inherit type_weapon
name Frostbrand
name_pl Frostbrands
skill two handed weapons
face frostbrand.x11
dam 9
nrof 1
attacktype 16
resist_fire 30
materialname iron
value 85000
weight 22000
magic 3
last_sp 8
weapontype 1
client_type 100
item_power 5
body_arm -2
body_combat -1
end

