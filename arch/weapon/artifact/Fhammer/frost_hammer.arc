object frost_hammer
anim
frost_hammer.x11
frost_hammer.x12
frost_hammer.x13
frost_hammer.x14
mina
inherit type_weapon
name Frost Hammer
name_pl Frost Hammers
skill two handed weapons
face frost_hammer.x11
pow 3
dam 10
speed .4
nrof 1
attacktype 18
resist_cold 30
materialname glacium
value 90000
weight 30000
magic 3
last_sp 8
weapontype 7
client_type 100
item_power 10
body_arm -2
body_combat -1
end

