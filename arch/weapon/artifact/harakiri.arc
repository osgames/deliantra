object harakiri
inherit type_weapon
name Harakiri sword
name_pl Harakiri swords
skill one handed weapons
face harakiri.x11
str 1
dex 1
dam 30
nrof 1
attacktype 1
materialname iron
value 1000000
weight 5000
last_sp 6
weapontype 1
client_type 100
body_arm -1
body_combat -1
end

