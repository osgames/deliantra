object bonecrusher
inherit type_weapon
name Bonecrusher
name_pl Bonecrushers
slaying skeleton
skill two handed weapons
face bonecrush.x11
dam 50
nrof 1
attacktype 1
materialname stone
value 70000
weight 150000
magic 3
last_sp 15
weapontype 7
client_type 100
item_power 4
body_arm -2
body_combat -1
end

