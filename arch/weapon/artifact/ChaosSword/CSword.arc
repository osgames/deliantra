object CSword
anim
CSword.x13
CSword.x14
CSword.x15
CSword.x16
CSword.x17
CSword.x18
CSword.x19
CSword.x16
CSword.x1A
CSword.x1B
CSword.x1C
CSword.x11
CSword.x12
mina
inherit type_weapon
name Chaos Sword
name_pl Chaos Swords
skill one handed weapons
face CSword.x13
str 1
dex 1
con 1
wis 1
cha 1
int 1
hp -5
sp -4
dam 100
luck -3
speed 0.3
nrof 1
attacktype 262145
resist_magic 30
resist_fire 30
resist_electricity 30
resist_cold 30
resist_confusion -50
resist_acid 75
path_denied 2304
materialname iron
value 1000000
weight 20000
magic 10
last_sp 3
weapontype 1
client_type 100
item_power 15
xrays 1
body_arm -1
body_combat -1
end

