object gram
anim
gram.x11
gram.x12
gram.x13
gram.x14
mina
inherit type_weapon
name Gram
name_pl Grams
skill two handed weapons
face gram.x11
exp 1
dam 18
luck 1
speed 0.1
nrof 1
attacktype 33
resist_ghosthit 30
resist_fear 30
materialname iron
value 80000
weight 30000
magic 3
last_sp 15
weapontype 1
client_type 100
item_power 10
body_arm -2
body_combat -1
end

