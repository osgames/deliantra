object firebrand
anim
firebrand.x11
firebrand.x12
firebrand.x13
mina
inherit type_weapon
name Firebrand
name_pl Firebrands
skill two handed weapons
face firebrand.x11
dam 9
speed 0.25
nrof 1
attacktype 4
resist_cold 30
materialname iron
value 75000
weight 22000
magic 3
last_sp 8
weapontype 1
client_type 100
item_power 5
body_arm -2
body_combat -1
end

