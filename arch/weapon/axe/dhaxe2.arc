object dhaxe2
inherit type_weapon
name axe
name_pl axes
skill one handed weapons
face dhaxe_2.x11
dam 10
nrof 1
attacktype 1
materialname bronze
value 520
weight 9660
last_sp 10
weapontype 3
client_type 106
body_arm -1
body_combat -1
end

