object axe_4
inherit type_weapon
name axe
name_pl axes
skill one handed weapons
face axe_4.x11
dam 7
nrof 1
attacktype 1
materialname bronze
value 19
weight 10000
last_sp 10
weapontype 3
client_type 106
body_arm -1
body_combat -1
end

