object dhaxe1
inherit type_weapon
name war axe
name_pl war axes
skill one handed weapons
face dhaxe_1.x11
dam 11
nrof 1
attacktype 1
materialname bronze
value 600
weight 10000
last_sp 9
weapontype 3
client_type 106
body_arm -1
body_combat -1
end

