object axe
inherit type_weapon
name_pl axes
skill one handed weapons
face axe_1.x11
dam 7
nrof 1
attacktype 1
materialname bronze
value 20
weight 9000
last_sp 10
weapontype 3
client_type 106
body_arm -1
body_combat -1
end

