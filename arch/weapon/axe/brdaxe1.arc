object brdaxe1
anim
brdaxe_1.x11
brdaxe_1.x14
brdaxe_1.x11
brdaxe_1.x11
brdaxe_1.x12
brdaxe_1.x12
brdaxe_1.x13
brdaxe_1.x13
brdaxe_1.x14
mina
inherit type_weapon
name bearded axe
name_pl bearded axes
skill one handed weapons
face brdaxe_1.x11
is_animated 0
dam 6
nrof 1
attacktype 1
materialname bronze
value 25
weight 4000
last_sp 9
weapontype 3
client_type 106
is_thrown 1
is_turnable 1
body_arm -1
body_combat -1
end

