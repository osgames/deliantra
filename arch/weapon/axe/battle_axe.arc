object battle_axe
inherit type_weapon
name Battle Axe
name_pl Battle Axes
skill two handed weapons
face battle_axe.x11
dam 21
speed 0.001
nrof 1
attacktype 1
materialname bronze
value 30000
weight 10000
last_sp 8
weapontype 3
client_type 106
body_arm -2
body_combat -1
end

