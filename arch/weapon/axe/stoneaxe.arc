object stoneaxe
inherit type_weapon
name_pl stoneaxes
skill one handed weapons
face stoneaxe.x11
dam 6
nrof 1
attacktype 1
materialname stone
value 12
weight 8500
last_sp 11
weapontype 7
client_type 106
body_arm -1
body_combat -1
end

