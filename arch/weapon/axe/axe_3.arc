object axe_3
inherit type_weapon
name axe
name_pl axes
skill one handed weapons
face axe_3.x11
dam 8
nrof 1
attacktype 1
materialname bronze
value 23
weight 12000
last_sp 10
weapontype 3
client_type 106
body_arm -1
body_combat -1
end

