object shaxe1
inherit type_weapon
name axe
name_pl axes
skill one handed weapons
face shaxe_1.x11
dam 9
nrof 1
attacktype 1
materialname bronze
value 515
weight 8500
last_sp 7
weapontype 3
client_type 106
body_arm -1
body_combat -1
end

