object b_axe1
inherit type_weapon
name axe
name_pl axes
skill one handed weapons
face b_axe1.x11
dam 3
nrof 1
attacktype 1
materialname bronze
value 5
weight 6000
last_sp 10
weapontype 3
client_type 106
is_thrown 1
body_arm -1
body_combat -1
end

