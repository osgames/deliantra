object axe_5
inherit type_weapon
name vicious axe
name_pl vicious axes
skill one handed weapons
face axe_5.x11
dam 13
nrof 1
attacktype 1
materialname bronze
value 80
weight 12000
last_sp 12
weapontype 3
client_type 106
body_arm -1
body_combat -1
end

