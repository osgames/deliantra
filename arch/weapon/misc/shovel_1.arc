object shovel_1
anim
shovel_1.x11
shovel_1.x12
shovel_1.x13
shovel_1.x14
shovel_1.x15
mina
inherit type_weapon
name shovel
name_pl shovels
slaying wall
skill two handed weapons
face shovel_1.x11
dam 10
nrof 1
attacktype 1
materialname iron
value 5
weight 30000
last_sp 15
client_type 145
body_arm -2
body_combat -1
end

