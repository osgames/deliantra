object trident
anim
trident.x01
trident.x11
trident.x21
trident.x31
trident.x41
trident.x51
trident.x61
trident.x71
trident.x81
mina
inherit type_weapon
name_pl tridents
skill two handed weapons
face trident.x21
is_animated 0
dam 11
nrof 1
attacktype 1
materialname iron
value 68
weight 23000
last_sp 8
weapontype 5
client_type 136
is_thrown 1
is_turnable 1
body_arm -2
body_combat -1
end

