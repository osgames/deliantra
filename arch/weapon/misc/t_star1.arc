object t_star1
inherit type_weapon
name throwing star
name_pl throwing stars
skill one handed weapons
face t_star1.x11
dam 3
nrof 1
attacktype 1
materialname iron
value 45
weight 325
last_sp 2
weapontype 2
client_type 101
is_thrown 1
body_arm -1
body_combat -1
end

