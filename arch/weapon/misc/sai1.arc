object sai1
inherit type_weapon
name sai
name_pl sais
skill one handed weapons
face sai_1.x11
dam 2
ac 1
nrof 1
attacktype 1
resist_physical 2
materialname bronze
value 80
weight 2000
last_sp 5
weapontype 5
client_type 145
body_arm -1
body_combat -1
end

object sai2
inherit type_weapon
name sai
name_pl sais
skill one handed weapons
face sai_2.x11
dam 2
ac 1
nrof 1
attacktype 1
resist_physical 2
materialname bronze
value 80
weight 2000
last_sp 5
weapontype 5
client_type 145
body_arm -1
body_combat -1
end

