object taifu_1
inherit type_weapon
name taifu
name_pl taifus
skill one handed weapons
face taifu_1.x11
dam 2
ac 1
nrof 1
attacktype 1
materialname bronze
value 20
weight 5000
last_sp 6
weapontype 5
client_type 145
body_arm -1
body_combat -1
end

