object lspear
inherit type_weapon
name long spear
name_pl long spears
skill two handed weapons
face lspear.x11
dam 12
nrof 1
attacktype 1
materialname bronze
value 100
weight 26500
last_sp 9
weapontype 5
client_type 136
body_arm -2
body_combat -1
end

