object sickle1
inherit type_weapon
name sickle
name_pl sickles
skill one handed weapons
face sickle_1.x11
dam 7
nrof 1
attacktype 1
materialname iron
value 52
weight 8000
last_sp 4
weapontype 1
client_type 101
body_arm -1
body_combat -1
end

