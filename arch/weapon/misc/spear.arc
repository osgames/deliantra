object spear
anim
spear.x01
spear.x11
spear.x21
spear.x31
spear.x41
spear.x51
spear.x61
spear.x71
spear.x81
mina
inherit type_weapon
name_pl spears
skill two handed weapons
face spear.x21
is_animated 0
dam 7
nrof 1
attacktype 1
materialname iron
value 50
weight 8500
last_sp 9
weapontype 5
client_type 136
is_thrown 1
is_turnable 1
body_arm -2
body_combat -1
end

