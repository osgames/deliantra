object poleaxe
inherit type_weapon
name_pl poleaxes
skill two handed weapons
face poleaxe.x11
dam 18
nrof 1
attacktype 1
materialname bronze
value 500
weight 35500
last_sp 12
client_type 136
body_arm -2
body_combat -1
end

