object kama1
inherit type_weapon
name kama
name_pl kamas
skill one handed weapons
face kama_1.x11
dam 6
nrof 1
attacktype 1
materialname bronze
value 47
weight 7000
last_sp 5
weapontype 3
client_type 106
body_arm -1
body_combat -1
end

object kama2
inherit type_weapon
name kama
name_pl kamas
skill one handed weapons
face kama_2.x11
dam 6
nrof 1
attacktype 1
materialname bronze
value 47
weight 7000
last_sp 5
weapontype 3
client_type 106
body_arm -1
body_combat -1
end

