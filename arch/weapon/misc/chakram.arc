object chakram
inherit type_weapon
name chakram
name_pl chakrams
skill one handed weapons
face chakram.x11
dam 6
nrof 1
attacktype 1
materialname iron
value 50
weight 700
last_sp 3
weapontype 2
client_type 101
is_thrown 1
body_arm -1
body_combat -1
end

