object scythe1
inherit type_weapon
name scythe
name_pl scythes
skill two handed weapons
face scythe_1.x11
dam 14
nrof 1
attacktype 1
materialname bronze
value 485
weight 29500
last_sp 13
weapontype 1
client_type 136
body_arm -2
body_combat -1
end

