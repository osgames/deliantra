object nunchacu_2
inherit type_weapon
name nunchacu
name_pl nunchacus
skill two handed weapons
face nunchacu_2.x11
dam 2
nrof 1
attacktype 1
materialname bronze
value 25
weight 5500
last_sp 4
weapontype 8
client_type 141
body_arm -2
body_combat -1
end

