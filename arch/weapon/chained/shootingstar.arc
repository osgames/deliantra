object shootingstar
inherit type_weapon
name shootingstar
name_pl shootingstars
skill two handed weapons
face shootingstar.x11
dam 40
nrof 1
attacktype 1
materialname iron
value 30000
weight 20000
last_sp 6
weapontype 7
client_type 141
body_arm -2
body_combat -1
end

