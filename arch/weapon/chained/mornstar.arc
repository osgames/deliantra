object morningstar
inherit type_weapon
name_pl morningstars
skill one handed weapons
face mornstar.x11
dam 10
nrof 1
attacktype 1
materialname bronze
value 35
weight 17000
last_sp 11
weapontype 7
client_type 141
body_arm -1
body_combat -1
end

