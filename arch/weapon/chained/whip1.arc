object whip1
inherit type_weapon
name whip
name_pl whips
skill one handed weapons
face whip1.x11
dam 1
nrof 1
attacktype 1
materialname bronze
value 25
weight 2000
last_sp 1
weapontype 8
client_type 141
body_arm -1
body_combat -1
end

