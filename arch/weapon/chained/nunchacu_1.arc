object nunchacu_1
inherit type_weapon
name nunchacu
name_pl nunchacus
skill two handed weapons
face nunchacu_1.x11
dam 1
nrof 1
attacktype 1
materialname bronze
value 20
weight 5000
last_sp 4
weapontype 8
client_type 141
body_arm -2
body_combat -1
end

