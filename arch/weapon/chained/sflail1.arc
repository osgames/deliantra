object sflail1
inherit type_weapon
name spiked flail
name_pl spiked flails
skill two handed weapons
face sflail_1.x11
dam 16
nrof 1
attacktype 1
materialname bronze
value 288
weight 28000
last_sp 15
weapontype 7
client_type 141
body_arm -2
body_combat -1
end

