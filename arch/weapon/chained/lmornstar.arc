object large_morningstar
inherit type_weapon
name large morningstar
name_pl large morningstars
skill two handed weapons
face lmornstar.x11
dam 13
nrof 1
attacktype 1
materialname bronze
value 44
weight 25000
last_sp 12
weapontype 7
client_type 141
body_arm -2
body_combat -1
end

