object shortsword_2
inherit type_weapon
name shortsword
name_pl shortswords
skill one handed weapons
face ssword_2.x11
dam 4
nrof 1
attacktype 1
materialname iron
value 35
weight 8000
last_sp 6
weapontype 5
client_type 101
body_arm -1
body_combat -1
end

