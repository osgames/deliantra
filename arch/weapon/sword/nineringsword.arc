object nineringsword
inherit type_weapon
name nine ring sword
name_pl nine ring swords
skill one handed weapons
face nineringsword.x11
dam 9
nrof 1
attacktype 1
materialname iron
value 65
weight 16000
last_sp 8
weapontype 1
client_type 101
body_arm -1
body_combat -1
end

