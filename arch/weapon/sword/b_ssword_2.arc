object b_ssword_2
inherit type_weapon
name shortsword
name_pl shortswords
skill one handed weapons
face b_ssword_2.x11
dam 2
nrof 1
attacktype 1
materialname bronze
value 8
weight 8000
last_sp 6
weapontype 5
client_type 101
body_arm -1
body_combat -1
end

