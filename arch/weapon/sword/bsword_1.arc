object broadsword
inherit type_weapon
name_pl broadswords
skill one handed weapons
face bsword_1.x11
dam 9
nrof 1
attacktype 1
materialname iron
value 50
weight 20000
last_sp 8
weapontype 1
client_type 101
body_arm -1
body_combat -1
end

