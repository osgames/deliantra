object sword_3
inherit type_weapon
name two handed sword
name_pl two handed swords
skill two handed weapons
face sword_3.x11
dam 14
nrof 1
attacktype 1
materialname iron
value 53
weight 25000
last_sp 8
weapontype 1
client_type 101
body_arm -2
body_combat -1
end

