object falchion
inherit type_weapon
name_pl falchions
skill one handed weapons
face falchion.x11
dam 7
nrof 1
attacktype 1
materialname iron
value 42
weight 13000
last_sp 8
weapontype 1
client_type 101
body_arm -1
body_combat -1
end

