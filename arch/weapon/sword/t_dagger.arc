object t_dagger
anim
t_dagger.x01
t_dagger.x11
t_dagger.x21
t_dagger.x31
t_dagger.x41
t_dagger.x51
t_dagger.x61
t_dagger.x71
t_dagger.x81
mina
inherit type_weapon
name throwing dagger
name_pl throwing daggers
skill one handed weapons
face t_dagger.x31
is_animated 0
dam 1
nrof 1
attacktype 1
materialname iron
value 35
weight 1500
last_sp 4
weapontype 2
client_type 101
is_thrown 1
is_turnable 1
body_arm -1
body_combat -1
end

