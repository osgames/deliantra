object kilij1
name kilij
name_pl kilijs
skill one handed weapons
face kilij1.x11
dam 9
nrof 1
type 15
attacktype 1
materialname iron
value 60
weight 16000
last_sp 8
weapontype 1
client_type 101
body_arm -1
body_combat -1
end

object kilij2
name kilij
name_pl kilijs
skill one handed weapons
face kilij2.x11
dam 9
nrof 1
type 15
attacktype 1
materialname iron
value 120
weight 16000
last_sp 8
weapontype 1
client_type 101
body_arm -1
body_combat -1
end

object kilij3
name kilij
name_pl kilijs
skill one handed weapons
face kilij3.x11
dam 10
nrof 1
type 15
attacktype 1
materialname iron
value 70
weight 18000
last_sp 10
weapontype 1
client_type 101
body_arm -1
body_combat -1
end

