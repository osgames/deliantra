object shortsword
inherit type_weapon
name_pl shortswords
skill one handed weapons
face ssword_1.x11
dam 4
nrof 1
attacktype 1
materialname iron
value 30
weight 10000
last_sp 6
weapontype 1
client_type 101
body_arm -1
body_combat -1
end

