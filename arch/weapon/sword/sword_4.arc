object sword_4
inherit type_weapon
name sword
name_pl swords
skill one handed weapons
face sword_4.x11
dam 8
nrof 1
attacktype 1
materialname iron
value 55
weight 15500
last_sp 8
weapontype 1
client_type 101
body_arm -1
body_combat -1
end

