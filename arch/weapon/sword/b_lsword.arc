object b_lsword
inherit type_weapon
name light sword
name_pl light swords
skill one handed weapons
face b_lsword.x11
dam 3
nrof 1
attacktype 1
materialname bronze
value 11
weight 11500
last_sp 8
weapontype 1
client_type 101
body_arm -1
body_combat -1
end

