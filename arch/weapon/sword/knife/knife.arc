object knife
inherit type_weapon
name Knife
name_pl Knives
skill one handed weapons
face knife.x11
dam 7
nrof 1
attacktype 1
materialname iron
value 100
weight 2000
last_sp 5
weapontype 2
client_type 101
body_arm -1
body_combat -1
end

