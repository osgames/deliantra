object b_scimitar
inherit type_weapon
name scimitar
name_pl scimitars
skill one handed weapons
face b_scimitar.x11
dam 4
nrof 1
attacktype 1
materialname bronze
value 10
weight 15000
last_sp 8
weapontype 1
client_type 101
body_arm -1
body_combat -1
end

