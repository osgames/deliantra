object sword
inherit type_weapon
name long sword
name_pl long swords
skill one handed weapons
face sword_1.x11
dam 8
nrof 1
attacktype 1
materialname iron
value 45
weight 15000
last_sp 8
weapontype 1
client_type 101
body_arm -1
body_combat -1
end

