object vlongsword
inherit type_weapon
name two handed sword
name_pl two handed swords
skill two handed weapons
face vlongsword.x11
dam 18
nrof 1
attacktype 1
materialname iron
value 500
weight 28000
last_sp 8
weapontype 1
client_type 101
body_arm -2
body_combat -1
end

