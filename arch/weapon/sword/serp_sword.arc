object serp_sword
inherit type_weapon
name Serpentman sword
name_pl Serpentman swords
skill one handed weapons
face serp_sword.x11
dam 6
nrof 1
attacktype 1
materialname organic
value 25
weight 3000
last_sp 8
weapontype 1
client_type 101
body_arm -1
body_combat -1
end

