object butterflysword_1
inherit type_weapon
name butterflysword
name_pl butterflyswords
skill one handed weapons
face butterflysword_1.x11
dam 4
nrof 1
attacktype 1
materialname iron
value 45
weight 10000
last_sp 5
weapontype 5
client_type 101
body_arm -1
body_combat -1
end

