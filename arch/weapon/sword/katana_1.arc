object katana_1
inherit type_weapon
name katana
name_pl katanas
skill one handed weapons
face katana_1.x11
dam 7
nrof 1
attacktype 1
materialname iron
value 80
weight 11000
last_sp 6
weapontype 4
client_type 101
body_arm -1
body_combat -1
end

