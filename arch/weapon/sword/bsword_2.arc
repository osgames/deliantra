object broadsword_2
inherit type_weapon
name broadsword
name_pl broadswords
skill one handed weapons
face bsword_2.x11
dam 9
nrof 1
attacktype 1
materialname iron
value 48
weight 21500
last_sp 8
weapontype 1
client_type 101
body_arm -1
body_combat -1
end

