object b_dagger
inherit type_weapon
name dagger
name_pl daggers
skill one handed weapons
face b_dagger.x11
dam 1
nrof 1
attacktype 1
materialname bronze
value 10
weight 2000
last_sp 5
weapontype 2
client_type 101
body_arm -1
body_combat -1
end

