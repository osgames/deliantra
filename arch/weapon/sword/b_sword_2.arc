object b_sword_2
inherit type_weapon
name long sword
name_pl long swords
skill one handed weapons
face b_sword_2.x11
dam 4
nrof 1
attacktype 1
materialname bronze
value 11
weight 14500
last_sp 8
weapontype 1
client_type 101
body_arm -1
body_combat -1
end

