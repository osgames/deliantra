object sabre
inherit type_weapon
name_pl sabres
skill one handed weapons
face sabre.x11
dam 7
nrof 1
attacktype 1
materialname iron
value 38
weight 13500
last_sp 8
weapontype 4
client_type 101
body_arm -1
body_combat -1
end

