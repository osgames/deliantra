object dagger
inherit type_weapon
name_pl daggers
skill one handed weapons
face dagger.x11
dam 2
nrof 1
attacktype 1
materialname iron
value 20
weight 2000
last_sp 5
weapontype 2
client_type 101
body_arm -1
body_combat -1
end

object dagger_r
inherit type_weapon
name dagger
name_pl daggers
skill one handed weapons
face dagger_r.x11
dam 2
nrof 1
attacktype 1
materialname iron
value 20
weight 2000
last_sp 5
weapontype 2
client_type 101
body_arm -1
body_combat -1
end

