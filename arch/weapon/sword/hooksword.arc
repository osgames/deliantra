object hooksword
inherit type_weapon
name hooksword
name_pl hookswords
skill one handed weapons
face hooksword.x11
dam 6
ac 2
nrof 1
attacktype 1
materialname iron
value 50
weight 13000
last_sp 7
weapontype 4
client_type 101
body_arm -1
body_combat -1
end

