object sicklesword
inherit type_weapon
name sickle sword
name_pl sickle swords
skill one handed weapons
face sicklesword.x11
dam 8
nrof 1
attacktype 1
materialname iron
value 440
weight 8600
last_sp 4
weapontype 1
client_type 101
body_arm -1
body_combat -1
end

