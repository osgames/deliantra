object rapier
inherit type_weapon
name_pl rapiers
skill one handed weapons
face rapier.x11
dam 7
nrof 1
attacktype 1
materialname iron
value 38
weight 10000
last_sp 8
weapontype 2
client_type 101
body_arm -1
body_combat -1
end

