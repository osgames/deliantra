object big_club
inherit type_weapon
name large club
name_pl large clubs
skill two handed weapons
face big_club.x11
dam 20
nrof 1
attacktype 1
materialname wood
value 20
weight 40000
last_sp 17
weapontype 8
client_type 121
body_arm -2
body_combat -1
end

