object stonebig_club
name large club
name_pl large clubs
skill two handed weapons
face stonebig_club.x11
dam 36
nrof 1
type 15
attacktype 1
materialname stone
value 20
weight 400000
last_sp 34
weapontype 8
client_type 121
body_arm -2
body_combat -1
end

