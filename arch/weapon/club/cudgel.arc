object cudgel
inherit type_weapon
name cudgel
name_pl cudgels
skill two handed weapons
face cudgel.x11
dam 40
nrof 1
attacktype 1
materialname bronze
value 654
weight 85000
last_sp 25
weapontype 8
client_type 121
body_arm -2
body_combat -1
end

