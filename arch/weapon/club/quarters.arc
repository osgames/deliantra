object quarterstaff
inherit type_weapon
name_pl quarterstaves
skill two handed weapons
face quarters.x11
dam 5
nrof 1
attacktype 1
materialname wood
value 20
weight 9000
last_sp 8
weapontype 8
client_type 121
body_arm -2
body_combat -1
end

