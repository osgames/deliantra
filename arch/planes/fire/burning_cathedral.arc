object burning_cathedral
name burning cathedral
face burning_cathedral.x11
type 66
client_type 25012
move_block all
no_pick 1
end
more
object burning_cathedral_2
name burning cathedral
face burning_cathedral.x11
x 1
type 66
move_block all
no_pick 1
end
more
object burning_cathedral_3
name burning cathedral
face burning_cathedral.x11
x 2
type 66
move_block all
no_pick 1
end
more
object burning_cathedral_4
name burning cathedral
face burning_cathedral.x11
y 1
type 66
move_block all
no_pick 1
end
more
object burning_cathedral_5
name burning cathedral
face burning_cathedral.x11
x 1
y 1
type 66
move_block all
no_pick 1
end
more
object burning_cathedral_6
name burning cathedral
face burning_cathedral.x11
x 2
y 1
type 66
move_block all
no_pick 1
end
more
object burning_cathedral_7
name burning cathedral
face burning_cathedral.x11
y 2
type 66
move_block all
no_pick 1
end
more
object burning_cathedral_8
name burning cathedral
face burning_cathedral.x11
x 1
y 2
type 66
move_block all
no_pick 1
end
more
object burning_cathedral_9
name burning cathedral
face burning_cathedral.x11
x 2
y 2
type 66
move_block all
no_pick 1
end
more
object burning_cathedral_10
name burning cathedral
face burning_cathedral.x11
y 3
type 66
move_block all
no_pick 1
end
more
object burning_cathedral_11
name burning cathedral
face burning_cathedral.x11
x 1
y 3
type 66
move_block all
no_pick 1
end
more
object burning_cathedral_12
name burning cathedral
face burning_cathedral.x11
x 2
y 3
type 66
move_block all
no_pick 1
end

