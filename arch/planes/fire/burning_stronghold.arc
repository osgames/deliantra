object burning_stronghold
face burning_stronghold.x11
type 66
client_type 25012
move_block all
no_pick 1
end
more
object burning_stronghold_2
name burning stronghold
face burning_stronghold.x11
x 1
type 66
move_block all
no_pick 1
end
more
object burning_stronghold_3
name burning stronghold
face burning_stronghold.x11
x 2
type 66
move_block all
no_pick 1
end
more
object burning_stronghold_4
name burning stronghold
face burning_stronghold.x11
y 1
type 66
move_block all
no_pick 1
end
more
object burning_stronghold_5
name burning stronghold
face burning_stronghold.x11
x 1
y 1
type 66
move_block all
no_pick 1
end
more
object burning_stronghold_6
name burning stronghold
face burning_stronghold.x11
x 2
y 1
type 66
move_block all
no_pick 1
end
more
object burning_stronghold_7
name burning stronghold
face burning_stronghold.x11
y 2
type 66
move_block all
no_pick 1
end
more
object burning_stronghold_8
name burning stronghold
face burning_stronghold.x11
x 1
y 2
type 66
no_pick 1
end
more
object burning_stronghold_9
name burning stronghold
face burning_stronghold.x11
x 2
y 2
type 66
move_block all
no_pick 1
end

