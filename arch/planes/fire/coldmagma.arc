object coldmagma
anim
coldmagma.x11
coldmagma.x11
coldmagma.x11
coldmagma.x12
coldmagma.x11
coldmagma.x11
coldmagma.x12
coldmagma.x12
coldmagma.x12
coldmagma.x11
coldmagma.x12
coldmagma.x12
mina
name cold magma
other_arch fire_elemental
face coldmagma.x11
is_animated 1
maxsp 5000
dam 75
wc -30
speed 0.15
level 30000
type 102
subtype 7
attacktype 4
move_on walk
no_pick 1
generator 1
is_floor 1
lifesave 1
end

