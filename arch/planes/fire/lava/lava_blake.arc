object lava_biglake_center
face lava_blake_F.x11
move_block all
no_pick 1
is_floor 1
is_water 1
end

object lava_biglake_e
face lava_blake_2.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_ew
face lava_blake_A.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_n
face lava_blake_1.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_ne
face lava_blake_3.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_nes
face lava_blake_7.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_new
face lava_blake_B.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_nse
face lava_blake_D.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_nw
face lava_blake_9.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_s
face lava_blake_4.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_se
face lava_blake_6.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_sew
face lava_blake_E.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_sn
face lava_blake_5.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_sw
face lava_blake_C.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_w
face lava_blake_8.x11
move_block all
no_pick 1
is_water 1
end

object lava_lake
face lava_blake_0.x11
move_block all
no_pick 1
is_water 1
end

