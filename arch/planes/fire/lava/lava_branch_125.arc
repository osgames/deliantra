object lava_rcrook_ns-ne
name lava river junction
face lava_rcrook_ns-ne.x11
move_block all
no_pick 1
is_water 1
end

object lava_rjunct_ns-ne
name lava river junction
face lava_rjunct_ns-ne.x11
move_block all
no_pick 1
is_water 1
end

