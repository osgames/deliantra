object lava_rcrook_ns-nw
name lava river junction
face lava_rcrook_ns-nw.x11
move_block all
no_pick 1
is_water 1
end

object lava_rjunct_ns-nw
name lava river junction
face lava_rjunct_ns-nw.x11
move_block all
no_pick 1
is_water 1
end

