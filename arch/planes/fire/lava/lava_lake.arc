object lava_biglake_0
face lava_blake_0.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_1_1
face lava_blake_B.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_1_2
face lava_blake_E.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_1_3
face lava_blake_7.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_1_4
face lava_blake_D.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_2_1_1
face lava_blake_A.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_2_1_2
face lava_blake_5.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_2_2_1
face lava_blake_C.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_2_2_2
face lava_blake_9.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_2_2_3
face lava_blake_3.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_2_2_4
face lava_blake_6.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_3_1
face lava_blake_4.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_3_2
face lava_blake_8.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_3_3
face lava_blake_1.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_3_4
face lava_blake_2.x11
move_block all
no_pick 1
is_water 1
end

object lava_biglake_4
face lava_blake_F.x11
move_block all
no_pick 1
is_floor 1
is_water 1
end

