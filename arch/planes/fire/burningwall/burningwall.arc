object burningwall_0
anim
burningwall_0.x11
burningwall_0.x12
mina
name burning wall
face burningwall_0.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object burningwall_1_1
anim
burningwall_4.x11
burningwall_4.x12
mina
name burning wall
face burningwall_4.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object burningwall_1_2
anim
burningwall_1.x11
burningwall_1.x12
mina
name burning wall
face burningwall_1.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object burningwall_1_3
anim
burningwall_8.x11
burningwall_8.x12
mina
name burning wall
face burningwall_8.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object burningwall_1_4
anim
burningwall_2.x11
burningwall_2.x12
mina
name burning wall
face burningwall_2.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object burningwall_2_1_1
anim
burningwall_5.x11
burningwall_5.x12
mina
name burning wall
face burningwall_5.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object burningwall_2_1_2
anim
burningwall_A.x11
burningwall_A.x12
mina
name burning wall
face burningwall_A.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object burningwall_2_2_1
anim
burningwall_3.x11
burningwall_3.x12
mina
name burning wall
face burningwall_3.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object burningwall_2_2_2
anim
burningwall_6.x11
burningwall_6.x12
mina
name burning wall
face burningwall_6.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object burningwall_2_2_3
anim
burningwall_C.x11
burningwall_C.x12
mina
name burning wall
face burningwall_C.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object burningwall_2_2_4
anim
burningwall_9.x11
burningwall_9.x12
mina
name burning wall
face burningwall_9.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object burningwall_3_1
anim
burningwall_B.x11
burningwall_B.x12
mina
name burning wall
face burningwall_B.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object burningwall_3_2
anim
burningwall_7.x11
burningwall_7.x12
mina
name burning wall
face burningwall_7.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object burningwall_3_3
anim
burningwall_E.x11
burningwall_E.x12
mina
name burning wall
face burningwall_E.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object burningwall_3_4
anim
burningwall_D.x11
burningwall_D.x12
mina
name burning wall
face burningwall_D.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

object burningwall_4
anim
burningwall_F.x11
burningwall_F.x12
mina
name burning wall
face burningwall_F.x11
is_animated 1
speed 0.2
move_block all
no_pick 1
blocksview 1
end

