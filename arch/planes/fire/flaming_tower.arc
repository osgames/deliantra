object flaming_tower
anim
flamingtower.x11
flamingtower.x12
flamingtower.x13
flamingtower.x11
flamingtower.x14
flamingtower.x13
flamingtower.x12
flamingtower.x14
flamingtower.x11
flamingtower.x13
flamingtower.x14
flamingtower.x12
mina
name fire tower
face flamingtower.x11
is_animated 1
speed 0.5
type 66
client_type 25012
no_pick 1
end
more
object flaming_tower_2
anim
flamingtower.x11
flamingtower.x12
flamingtower.x13
flamingtower.x11
flamingtower.x14
flamingtower.x13
flamingtower.x12
flamingtower.x14
flamingtower.x11
flamingtower.x13
flamingtower.x14
flamingtower.x12
mina
name fire tower
face flamingtower.x11
is_animated 1
y 1
speed 0.5
type 66
no_pick 1
end

