object s_ship
name Ship
face s_ship.x11
type 66
client_type 25012
move_block boat
move_allow walk
no_pick 1
end
more
object s_ship2
name big galleon
face s_ship.x11
x 1
type 66
move_block boat
move_allow walk
no_pick 1
end
more
object s_ship3
name big galleon
face s_ship.x11
y 1
type 66
move_block boat
move_allow walk
no_pick 1
end
more
object s_ship4
name big galleon
face s_ship.x11
x 1
y 1
type 66
move_block boat
move_allow walk
no_pick 1
end

