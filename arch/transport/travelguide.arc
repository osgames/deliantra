object travelguide
name Travel Guide
face sailor.x11
type 98
subtype 43
no_pick 1
move_block all
value 2000
sound fx/signature-31
msg
@match travel
@cond $who->pay_amount ($npc->value)
@eval $who->contr->play_sound ($npc->sound)
@eval $who->goto ($npc->slaying)

As you wish!

@match travel
@msg "Sorry G<Sir|Madam>, but you need " . (cf::cost_string_from_value $npc->value) . "."

@match *
@msg $npc->lore
@msg "\nIt will cost you " . (cf::cost_string_from_value $npc->value) . ".\n\n"

Do you want to travel there?
endmsg
end

