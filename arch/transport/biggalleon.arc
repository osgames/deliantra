object big_galleon
name big galleon
face biggalleon.x11
type 66
client_type 25012
move_block boat
move_allow walk
no_pick 1
end
more
object big_galleon_2
name big galleon
face biggalleon.x11
x 1
type 66
move_block boat
move_allow walk
no_pick 1
end
more
object big_galleon_3
name big galleon
face biggalleon.x11
y 1
type 66
move_block boat
move_allow walk
no_pick 1
end
more
object big_galleon_4
name big galleon
face biggalleon.x11
x 1
y 1
type 66
move_block boat
move_allow walk
no_pick 1
end

