object zeppelin1
anim
zeppelin1.x11
zeppelin1.x12
zeppelin1.x13
zeppelin1.x14
zeppelin1.x14
zeppelin1.x13
zeppelin1.x12
zeppelin1.x11
mina
name Luftschiff
face zeppelin1.x11
speed 1
type 2
client_type 25012
move_block flying
move_allow walk
no_pick 1
end
more
object zeppelin1_2
anim
zeppelin1.x11
zeppelin1.x12
zeppelin1.x13
zeppelin1.x14
zeppelin1.x14
zeppelin1.x13
zeppelin1.x12
zeppelin1.x11
mina
name Luftschiff
face zeppelin1.x11
x 1
speed 1
type 2
move_block boat
move_allow walk
no_pick 1
end
more
object zeppelin1_3
anim
zeppelin1.x11
zeppelin1.x12
zeppelin1.x13
zeppelin1.x14
zeppelin1.x14
zeppelin1.x13
zeppelin1.x12
zeppelin1.x11
mina
name Luftschiff
face zeppelin1.x11
y 1
speed 1
type 2
move_block flying
move_allow walk
no_pick 1
end
more
object zeppelin1_4
anim
zeppelin1.x11
zeppelin1.x12
zeppelin1.x13
zeppelin1.x14
zeppelin1.x14
zeppelin1.x13
zeppelin1.x12
zeppelin1.x11
mina
name Luftschiff
face zeppelin1.x11
x 1
y 1
speed 1
type 2
move_block flying
move_allow walk
no_pick 1
end

