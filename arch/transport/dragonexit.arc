object dragon_exit
anim
dragon.x71
dragon.x72
dragon.x73
dragon.x72
mina
name dragon
face dragon.x71
speed 0.4
type 66
client_type 25012
no_pick 1
end
more
object dragon_exit_2
anim
dragon.x71
dragon.x72
dragon.x73
dragon.x72
mina
name dragon
face dragon.x71
x 1
type 66
weight 4000000
no_pick 1
end
more
object dragon_exit_3
anim
dragon.x71
dragon.x72
dragon.x73
dragon.x72
mina
name dragon
face dragon.x71
x 2
type 66
weight 4000000
no_pick 1
end
more
object dragon_exit_4
anim
dragon.x71
dragon.x72
dragon.x73
dragon.x72
mina
name dragon
face dragon.x71
y 1
type 66
weight 4000000
no_pick 1
end
more
object dragon_exit_5
anim
dragon.x71
dragon.x72
dragon.x73
dragon.x72
mina
name dragon
face dragon.x71
x 1
y 1
type 66
weight 4000000
no_pick 1
end
more
object dragon_exit_6
anim
dragon.x71
dragon.x72
dragon.x73
dragon.x72
mina
name dragon
face dragon.x71
x 2
y 1
type 66
weight 4000000
no_pick 1
end

