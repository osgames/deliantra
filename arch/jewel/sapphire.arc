object pretty_sapphire
name pretty sapphire
name_pl pretty sapphires
race gold and jewels
face pretty_sapphire.x11
speed -0.1
nrof 1
type 60
value 1850
weight 200
client_type 2011
identified 1
end

object sapphire
anim
sapphire.x11
sapphire.x12
mina
name_pl sapphires
race gold and jewels
face sapphire.x11
speed -0.1
nrof 1
type 60
value 185
weight 50
client_type 2011
identified 1
end

