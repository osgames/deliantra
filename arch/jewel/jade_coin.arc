# DON'T USE THIS OBJECT YET
# it needs some server code which is under discussion
# a character having jade coins without the server code in place
# would be able to simply walk out of shops with stuff, without
# actually paying anything.
object jadecoin
name jade coin
name_pl jade coins
race gold and jewels
face jade_coin.x11
nrof 1
type 36
materialname jade
value 5000
weight 10
client_type 2001
editor_folder deprecated/jewel
end

