object mithril
anim
mithril.x11
mithril.x12
mithril.x13
mithril.x14
mina
name mithril crystal
name_pl mithril crystals
race gold and jewels
face mithril.x11
speed 0.2
nrof 1
type 73
materialname mithril
value 800
weight 50
client_type 2011
identified 1
end

