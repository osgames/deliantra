#Above composition ^^
#99.9% U235
object depleteduraniumbar
name depleted uranium bar
name_pl depleted uranium bars
race gold and jewels
face uraniumbar.x11
nrof 1
type 73
materialname depleted uranium
value 20
weight 10000
client_type 2005
identified 1
end

#Above composition ^^
#99.5% U238
#0.5% U235
#(Note in the real world usually 0.7% U235).
object enricheduraniumbar
name enriched uranium bar
name_pl enriched uranium bars
race gold and jewels
face uraniumbar.x11
dam 5
speed 0.100000
nrof 1
type 73
attacktype 4
materialname enriched uranium
value 400000
weight 10000
glow_radius 1
client_type 2005
identified 1
# adeb0
end

object uraniumbar
name uranium bar
name_pl uranium bars
race gold and jewels
face uraniumbar.x11
nrof 1
type 73
materialname uranium
value 50
weight 10000
client_type 2005
identified 1
end

