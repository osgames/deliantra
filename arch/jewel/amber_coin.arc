# DON'T USE THIS OBJECT YET
# it needs some server code which is under discussion
# a character having amberium coins without the server code in place
# would be able to simply walk out of shops with stuff, without
# actually paying anything.
object ambercoin
name amberium coin
name_pl amberium coins
race gold and jewels
face amber_coin.x11
nrof 1
type 36
materialname amberium
value 500000
weight 23
client_type 2001
editor_folder deprecated/jewel
end

