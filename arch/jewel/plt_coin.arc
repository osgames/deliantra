object platinacoin
name platinum coin
name_pl platinum coins
race gold and jewels
msg
One platinum piece is worth 100 gold; One royalty is worth 100 platinum.
endmsg
face plt_coin.x11
nrof 1
type 36
materialname platinum
value 10000
weight 10
client_type 2001
end

