object goldcoin
name gold coin
name_pl gold coins
race gold and jewels
msg
One gold piece is worth 100 silver; One platinum piece is worth 100 gold.
endmsg
face goldcoin.x11
nrof 1
type 36
materialname gold
value 100
weight 10
client_type 2001
end

