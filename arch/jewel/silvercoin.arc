object silvercoin
name silver coin
name_pl silver coins
race gold and jewels
msg
One gold piece is worth 100 silver.
endmsg
face silvercoin.x11
nrof 1
type 36
materialname silver
value 1
weight 10
client_type 2001
end

