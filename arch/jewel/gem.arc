object gem
anim
gem.x11
gem.x12
mina
name diamond
name_pl diamonds
race gold and jewels
face gem.x11
speed -0.1
nrof 1
type 60
materialname stone
value 400
weight 50
client_type 2011
identified 1
end

object pretty_crystal
name diamond
name_pl diamonds
race gold and jewels
face pretty_crystal.x11
speed -0.1
nrof 1
type 90
materialname stone
value 4000
weight 200
client_type 2011
identified 1
end

