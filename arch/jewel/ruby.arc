object pretty_ruby
name pretty ruby
name_pl pretty rubies
race gold and jewels
face pretty_ruby.x11
speed -0.1
nrof 1
type 60
value 2000
weight 200
client_type 2011
identified 1
end

object ruby
anim
ruby.x11
ruby.x12
mina
name_pl rubies
race gold and jewels
face ruby.x11
speed -0.1
nrof 1
type 60
value 200
weight 50
client_type 2011
identified 1
end

