object emerald
anim
emerald.x11
emerald.x12
mina
name_pl emeralds
race gold and jewels
face emerald.x11
speed -0.1
nrof 1
type 60
value 160
weight 50
client_type 2011
identified 1
need_an 1
end

object pretty_emerald
name pretty emerald
name_pl pretty emeralds
race gold and jewels
face pretty_emerald.x11
speed -0.1
nrof 1
type 60
value 1600
weight 200
client_type 2011
identified 1
need_an 1
end

