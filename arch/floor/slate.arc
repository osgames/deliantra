object blackslate
face slate_b.x11
smoothlevel 30
smoothface slate_b.x11 empty_S.x11
no_pick 1
is_floor 1
end

object redslate
face slate_r.x11
smoothlevel 30
smoothface slate_r.x11 empty_S.x11
no_pick 1
is_floor 1
end

object whiteslate
face slate_w.x11
smoothlevel 30
smoothface slate_w.x11 empty_S.x11
no_pick 1
is_floor 1
end

