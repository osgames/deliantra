#for backward compatibility, this arch is named "dungeon magic"
#It forbids both magic and cleric spells.
object dungeon_magic
name no_spells
face no_spells.x11
invisible 1
no_pick 1
no_magic 1
damned 1
editor_folder floor_special
end

