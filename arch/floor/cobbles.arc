object cobbles_black
name cobblestones
face cobbles_black.x11
smoothlevel 30
smoothface cobbles_black.x11 black_border_S.x11
move_block swim boat
no_pick 1
is_floor 1
end

object cobbles_blue
name cobblestones
face cobbles_blue.x11
smoothlevel 30
smoothface cobbles_blue.x11 black_border_S.x11
move_block swim boat
no_pick 1
is_floor 1
end

object cobbles_brown
name cobblestones
face cobbles_brown.x11
smoothlevel 30
smoothface cobbles_brown.x11 black_border_S.x11
move_block swim boat
no_pick 1
is_floor 1
end

object cobbles_green
name cobblestones
face cobbles_green.x11
smoothlevel 30
smoothface cobbles_green.x11 black_border_S.x11
move_block swim boat
no_pick 1
is_floor 1
end

object cobbles_red
name cobblestones
face cobbles_red.x11
smoothlevel 30
smoothface cobbles_red.x11 black_border_S.x11
move_block swim boat
no_pick 1
is_floor 1
end

object cobbles_white
name cobblestones
face cobbles_white.x11
smoothlevel 30
smoothface cobbles_white.x11 black_border_S.x11
move_block swim boat
no_pick 1
is_floor 1
end

