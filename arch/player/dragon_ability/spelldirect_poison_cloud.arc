# This archetype was used for dragon players' abilities. It has been replaced
# by spell_poison_cloud. Nevertheless it cannot be deleted because existing
# players may contain such an object.
object spelldirect_poison_cloud
inherit type_spell
name poison cloud
name_pl poison cloud
skill sorcery
other_arch poisonbullet
face spell_sorcery.x11
sp 5
maxsp 8
food 5
dam 0
level 2
subtype 5
attacktype 1026
path_attuned 16
value 20
invisible 1
duration 4
range 4
dam_modifier 3
no_drop 1
casting_time 10
end

