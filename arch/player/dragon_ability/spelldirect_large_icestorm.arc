# This archetype was used for dragon players' abilities. It has been replaced
# by spell_large_icestorm. Nevertheless it cannot be deleted because existing
# players may contain such an object.
object spelldirect_large_icestorm
inherit type_spell
name large icestorm
name_pl large icestorm
skill evocation
other_arch icestorm
face spell_evocation.x11
sp 13
maxsp 11
dam 4
level 12
subtype 7
attacktype 18
path_attuned 4
value 120
invisible 1
duration 2
range 14
range_modifier 5
dam_modifier 3
no_drop 1
casting_time 40
end

