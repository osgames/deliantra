# This archetype was used for dragon players' abilities. It has been replaced
# by spell_ball_lightning. Nevertheless it cannot be deleted because existing
# players may contain such an object.
object spelldirect_ball_lightning
inherit type_spell
name ball lightning
name_pl ball lightning
skill evocation
other_arch ball_lightning
face spell_evocation.x11
sp 10
maxsp 15
dam 8
level 9
subtype 35
attacktype 10
path_attuned 8
value 90
invisible 1
duration 40
duration_modifier 1
dam_modifier 3
no_drop 1
casting_time 30
end

