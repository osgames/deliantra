object direct_attune_cold
name attune cold
type 114
path_attuned 4
invisible 1
no_drop 1
end

object direct_attune_elec
name attune electricity
type 114
path_attuned 8
invisible 1
no_drop 1
end

object direct_attune_fire
name attune fire
type 114
path_attuned 2
invisible 1
no_drop 1
end

