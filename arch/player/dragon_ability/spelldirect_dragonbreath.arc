# This archetype was used for dragon players' abilities. It has been replaced
# by spell_dragonbreath. Nevertheless it cannot be deleted because existing
# players may contain such an object.
object spelldirect_dragonbreath
inherit type_spell
name dragonbreath
name_pl dragonbreath
skill pyromancy
other_arch firebreath
face spell_pyromancy.x11
sp 13
maxsp 11
dam 4
level 12
subtype 7
attacktype 4
path_attuned 2
value 120
invisible 1
duration 2
range 7
range_modifier 5
dam_modifier 3
no_drop 1
# Note this is not magical!
casting_time 30
end

