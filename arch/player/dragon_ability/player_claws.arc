object electric_clawing
anim
pl_dragon_bl.x11
pl_dragon_bl.x12
pl_dragon_bl.x31
pl_dragon_bl.x32
pl_dragon_bl.x51
pl_dragon_bl.x52
pl_dragon_bl.x71
pl_dragon_bl.x72
facings 4
mina
inherit type_skill_tool
name electric claws
title clawing
skill clawing
msg
Your claws emit sparks of electricity!
endmsg
subtype 31
attacktype 8
invisible 1
no_drop 1
end

object fire_clawing
anim
pl_dragon_r.x11
pl_dragon_r.x12
pl_dragon_r.x31
pl_dragon_r.x32
pl_dragon_r.x51
pl_dragon_r.x52
pl_dragon_r.x71
pl_dragon_r.x72
facings 4
mina
inherit type_skill_tool
name fire claws
title clawing
skill clawing
msg
Your claws are enveloped in a hot blazing fire!
endmsg
subtype 31
attacktype 4
invisible 1
no_drop 1
end

object ice_clawing
anim
pl_dragon_bl.x11
pl_dragon_bl.x12
pl_dragon_bl.x31
pl_dragon_bl.x32
pl_dragon_bl.x51
pl_dragon_bl.x52
pl_dragon_bl.x71
pl_dragon_bl.x72
facings 4
mina
inherit type_skill_tool
name ice claws
title clawing
skill clawing
msg
Your claws are covered with shards of ice!
endmsg
subtype 31
attacktype 16
invisible 1
no_drop 1
end

object poison_clawing
anim
pl_dragon_g.x11
pl_dragon_g.x12
pl_dragon_g.x31
pl_dragon_g.x32
pl_dragon_g.x51
pl_dragon_g.x52
pl_dragon_g.x71
pl_dragon_g.x72
facings 4
mina
inherit type_skill_tool
name poison claws
title clawing
skill clawing
msg
Your claws are now poisoned!
endmsg
subtype 31
attacktype 1024
invisible 1
no_drop 1
end

