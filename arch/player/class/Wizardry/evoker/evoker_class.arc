object evoker_class
anim
evoker.x11
evoker.x12
evoker.x31
evoker.x32
evoker.x51
evoker.x52
evoker.x71
evoker.x72
facings 4
mina
name evoker
face evoker.x51
is_animated 1
str -2
dex 0
con -1
wis 0
pow 3
int 0
type 37
randomitems evoker_class_items
anim_speed -1
end

object talisman_evoker
inherit type_skill_tool
name Evoker's talisman
skill evocation
face talisman.x11
nrof 1
path_attuned 14
path_repelled 50240
materialname runestone
value 3000
weight 1000
client_type 451
body_neck -1
end

