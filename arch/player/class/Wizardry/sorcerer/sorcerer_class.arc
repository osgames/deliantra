object sorcerer_class
anim
sorcerer.x11
sorcerer.x12
sorcerer.x31
sorcerer.x32
sorcerer.x51
sorcerer.x52
sorcerer.x71
sorcerer.x72
facings 4
mina
name sorcerer
face sorcerer.x71
is_animated 1
str -3
dex 2
con -3
wis -2
pow 3
int 3
type 37
randomitems sorcerer_class_items
anim_speed -1
end

object sorcerer_hat
name Sorcerer's hat
face wiz_hat.x11
animation wiz_hat
sp 1
ac 1
speed 0.1
nrof 1
type 34
materialname cloth
weight 1000
last_sp 12
client_type 270
body_head -1
end

