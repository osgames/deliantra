object summoner_class
anim
facings 4
wizard_yellow.x11
wizard_yellow.x31
wizard_yellow.x51
wizard_yellow.x71
mina
name summoner
face wizard_yellow.x71
is_animated 0
str -1
dex 0
con -1
wis 0
pow 1
int 1
type 37
randomitems summoner_class_items
end

object talisman_summoner
inherit type_skill_tool
name Summoner's talisman
skill summoning
face talisman.x11
nrof 1
path_attuned 64
path_repelled 65536
materialname runestone
value 3000
weight 1000
client_type 451
body_neck -1
end

