object alchemist_class
anim
alchemist.x11
alchemist.x12
alchemist.x31
alchemist.x32
alchemist.x51
alchemist.x52
alchemist.x71
alchemist.x72
facings 4
mina
name alchemist
face alchemist.x51
is_animated 1
str -3
dex 0
con -1
wis 0
pow 1
int 3
type 37
randomitems alchemist_class_items
anim_speed -1
end

object talisman_alchemist
inherit type_skill_tool
name Alchemist's talisman
skill sorcery
face talisman.x11
nrof 1
path_attuned 16896
materialname runestone
value 3000
weight 1000
client_type 451
body_neck -1
end

