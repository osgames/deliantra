object pyromancer_class
anim
pyromancer.x11
pyromancer.x12
pyromancer.x31
pyromancer.x32
pyromancer.x51
pyromancer.x52
pyromancer.x71
pyromancer.x72
facings 4
mina
name pyromancer
face pyromancer.x51
is_animated 1
str -1
dex 0
con -1
wis 0
pow 2
int 0
type 37
randomitems pyromancer_class_items
anim_speed -1
end

object talisman_pyromancer
inherit type_skill_tool
name Pyromancers's talisman
skill pyromancy
face talisman.x11
nrof 1
path_attuned 2
path_repelled 33028
materialname runestone
value 3000
weight 1000
client_type 451
body_neck -1
end

