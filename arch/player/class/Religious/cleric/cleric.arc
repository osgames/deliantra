object cleric_class
anim
cleric.x11
cleric.x12
cleric.x31
cleric.x32
cleric.x51
cleric.x52
cleric.x71
cleric.x72
facings 4
mina
name cleric
race cleric
face cleric.x51
is_animated 1
str 0
dex -1
con -1
wis 2
pow 1
cha 1
int -2
food 500
dam 1
wc 21
ac 10
speed 0.5
level 1
type 37
attacktype 1
weight 70000
randomitems cleric
alive 1
can_use_shield 1
can_use_armour 1
can_use_weapon 1
anim_speed -1
end

