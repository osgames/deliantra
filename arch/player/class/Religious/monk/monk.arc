object monk_class
anim
monk.x11
monk.x12
monk.x31
monk.x32
monk.x51
monk.x52
monk.x71
monk.x72
facings 4
mina
name monk
face monk.x51
is_animated 1
str 2
dex 2
con 1
wis 1
int -1
type 37
randomitems monk_class_items
can_use_weapon 1
anim_speed -1
end

