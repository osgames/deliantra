# This force object is used for monk players. It prevents the character from
# using weapons.
object monk_no_use_weapon
name no weapon force
face blank.x11
type 114
invisible 1
no_drop 1
neutral 1
end

