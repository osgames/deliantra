object paladin_class
anim
paladin.x11
paladin.x12
paladin.x31
paladin.x32
paladin.x51
paladin.x52
paladin.x71
paladin.x72
facings 4
mina
name paladin
face paladin.x71
is_animated 1
str 0
dex -1
con 0
wis 2
pow 1
cha 1
int -2
type 37
randomitems paladin_class_items
anim_speed -1
end

object paladin_holy_symbol
inherit type_skill_tool
name Paladin's holy symbol
skill praying
face holy_symbol.x11
nrof 1
resist_magic 30
materialname ancient wood
value 1500
weight 5000
client_type 451
end

