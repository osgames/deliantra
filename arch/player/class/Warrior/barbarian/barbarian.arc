object barbarian_class
anim
barbarian.x11
barbarian.x12
barbarian.x31
barbarian.x32
barbarian.x51
barbarian.x52
barbarian.x71
barbarian.x72
facings 4
mina
name barbarian
face barbarian.x51
is_animated 1
str 3
dex 2
con 3
wis -1
pow -1
cha -2
int -6
type 37
randomitems barbarian_class_items
anim_speed -1
end

