object dragon_ability_force
name dragon ability
title fire hatchling
face blank.x11
exp 2
type 114
invisible 1
no_drop 1
neutral 1
end

object dragon_skin_force
name dragon skin
face blank.x11
type 114
invisible 1
applied 1
no_drop 1
end

object pl_dragon
inherit type_player
name fire hatchling
race dragon
msg
Dragons are completely different than any
other race. Their vast size and bizarre body
prevents them both from wearing armour and
wielding weapons -- tools that they disdain
in any case. Instead, they are gifted with
the ability to evolve and grow stronger by
eating the flesh of their defeated foes.

Over the years, their dragonhide hardens and
it can provide better protection than the
best armour. Instead of wielding weapons,
they use their sharp and lethal claws in
combat.  Moreover, dragons have a natural
talent for magic.

Dragons are very interested in the lore of
the elements and usually choose to
specialize.  While focusing their metabolism
on a certain element, they can gain various
new abilities -- including new spells,
enhanced claws and more.  Eventually, a
dragon is able to evolve from the hatchling
stage into a grown-up terrifying ancient
dragon.
endmsg
face pl_dragon_r.x51
animation red_dragon1
is_animated 1
str 5
dex 0
con 6
wis -8
pow 5
int -3
food 999
dam 10
wc 21
ac 5
speed 1
level 1
attacktype 1
weight 70000
randomitems dragon_player_items
alive 1
can_use_shield 0
can_use_armour 0
can_use_weapon 0
body_range 1
body_arm 0
body_neck 1
body_skill 1
body_finger 2
body_shoulder 1
body_wrist 2
body_waist 1
# bracers, and girdles.
anim_speed -1
end

