object troll_player
anim
troll_p.x11
troll_p.x12
troll_p.x31
troll_p.x32
troll_p.x51
troll_p.x52
troll_p.x71
troll_p.x72
facings 4
mina
inherit type_player
name troll
race troll
msg
Trolls are usually nasty creatures: scions
of Gnarg. They have amazing strength and
hardiness, but they're a bit clumsy, very
stupid, impious, and bad at handling and
obtaining magic.  They heal with amazing
rapidity and can see in the dark.  Because of
the above attributes, they're great in any
fight.  Well, they're great until someone
waves torches at them: fire presents great
peril to trolls.

Most trolls are destroyed on sight by
civilized people: but a few have proved
deserving of tolerance and are permitted to
enter cities.
endmsg
face troll_p.x51
is_animated 1
str 5
dex -1
con 5
wis -2
pow -2
cha -4
int -5
food 999
dam 1
wc 21
ac 10
speed 1
level 1
attacktype 1
resist_fire -30
resist_cold 30
path_repelled 2
weight 90000
randomitems troll_player_items
alive 1
can_use_shield 1
can_use_armour 1
can_use_weapon 1
can_see_in_dark 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_neck 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
anim_speed -1
end

object troll_player_force
name troll_force
face blank.x11
hp 3
sp -2
speed 0
type 114
invisible 1
applied 1
no_drop 1
end

