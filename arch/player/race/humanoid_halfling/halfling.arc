object halfling_player
anim
halfling.x11
halfling.x12
halfling.x31
halfling.x32
halfling.x51
halfling.x52
halfling.x71
halfling.x72
facings 4
mina
inherit type_player
name halfling
race human
msg
Halflings are another sub-race of humankind,
like the Northman. They are much smaller and
shorter than humans, so they are weaker, but
they are far quicker and hardier. They're
quite unreligious, but many take up magic.

Their small size lets them hide more
effectively, and for some reason chance seems
to favor them with good luck. They are also
surprisingly resistant to loss of lifeforce.
endmsg
face halfling.x51
is_animated 1
str -4
dex 3
con 4
wis -3
food 999
dam 1
luck 1
wc 21
ac 10
speed 1
level 1
attacktype 1
resist_drain 30
weight 40000
randomitems halfling_player
alive 1
can_use_shield 1
can_use_armour 1
can_use_weapon 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_neck 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
# Arguably, halfings should have a body_small_torso or something.
anim_speed -1
end

