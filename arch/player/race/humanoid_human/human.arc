object human_player
anim
human.x11
human.x12
human.x31
human.x32
human.x51
human.x52
human.x71
human.x72
facings 4
mina
inherit type_player
name human
race human
msg
Humans are the generalists of the races.
They are fit for any trade, and usually
pick up a skill from their parents in
addition to what they've learned as
apprentices in their craft.
endmsg
face human.x51
is_animated 1
str 0
dex 0
con 0
wis 0
pow 0
cha 0
int 0
food 999
dam 1
wc 21
ac 10
speed 1
level 1
attacktype 1
weight 70000
randomitems human_player
alive 1
can_use_shield 1
can_use_armour 1
can_use_weapon 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_neck 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
anim_speed -1
end

