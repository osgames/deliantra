object pl_half_orc
anim
pl_half_orc.x11
pl_half_orc.x12
pl_half_orc.x31
pl_half_orc.x32
pl_half_orc.x51
pl_half_orc.x52
pl_half_orc.x71
pl_half_orc.x72
facings 4
mina
inherit type_player
name half orc
race goblin
msg
Half-Orcs are usually the product of rape,
slavery, or some such unpleasantness.  They
look it too:  Half-Orcs are rather ugly.  On
average, they're stronger, a bit quicker, and
hardier than humans, but also stupid, impious,
and unmagical. They're resistant to poison and
can see in the dark, and every half-orc seems
to know how to steal.
endmsg
face pl_half_orc.x51
is_animated 1
str 2
dex 1
con 2
wis -2
pow -1
cha -3
int -2
dam 1
wc 21
ac 10
speed 1
level 1
resist_poison 30
path_repelled 65536
weight 70000
randomitems half_orc_player_items
alive 1
can_use_shield 1
no_pick 1
can_use_armour 1
can_use_weapon 1
can_see_in_dark 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_neck 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
anim_speed -1
end

