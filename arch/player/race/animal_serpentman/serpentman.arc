object serpentman_player
anim
serpentman.x11
serpentman.x12
serpentman.x11
serpentman.x12
serpentman.x11
serpentman.x12
serpentman.x21
serpentman.x22
serpentman.x21
serpentman.x22
serpentman.x21
serpentman.x22
serpentman.x31
serpentman.x32
serpentman.x33
serpentman.x31
serpentman.x32
serpentman.x33
serpentman.x41
serpentman.x42
serpentman.x41
serpentman.x42
serpentman.x41
serpentman.x42
serpentman.x51
serpentman.x52
serpentman.x51
serpentman.x52
serpentman.x51
serpentman.x52
serpentman.x61
serpentman.x62
serpentman.x61
serpentman.x62
serpentman.x61
serpentman.x62
serpentman.x71
serpentman.x72
serpentman.x73
serpentman.x71
serpentman.x72
serpentman.x73
serpentman.x81
serpentman.x82
serpentman.x81
serpentman.x82
serpentman.x81
serpentman.x82
facings 8
mina
inherit type_player
name Serpentman
race reptile
msg
The Serpentman adventurer is a rare breed.
Few Serpentmen ever leave the swamps, as they
are not well adjusted for human society.
Their heads and feet are not shaped properly
to use boots or helmets made for humanoid
folk, but their unusual build has given rise
to a cultural habit of wearing pieces of
bracelet- and ring-like jewelry on their
tails.  They possess a tough hide and strong
legs which enable them to leap further than
most folks.  Their nasty claws can also double
as weapons.

They are only partially warm-blooded, and are
as a result somewhat susceptible to cold.
endmsg
face serpentman.x31
is_animated 0
str 2
dex 2
con 4
wis -2
cha -4
int -2
food 999
dam 8
wc 21
ac 5
speed 1
level 1
attacktype 1
resist_physical 5
resist_fire 30
resist_electricity 30
resist_cold -50
resist_acid 30
resist_poison 30
weight 70000
randomitems serpentman_player_items
alive 1
can_use_shield 1
can_use_armour 1
can_use_weapon 1
can_see_in_dark 1
body_range 1
body_arm 2
body_torso 1
body_neck 1
body_skill 1
body_finger 3
body_shoulder 1
body_hand 2
body_wrist 4
body_waist 1
end

