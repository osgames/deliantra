object elf_player
anim
elf.x11
elf.x12
elf.x31
elf.x32
elf.x51
elf.x52
elf.x71
elf.x72
facings 4
mina
inherit type_player
name elf
race faerie
msg
An Elf is a child of Faerie, a scion of
Lythander. Being of Faerie, they heal more
slowly than other races, but mana flows into
them more quickly. They also need far less
food than other races.

Every elf is taught the use of a bow, but
their long lives allow them to take up any
trade. They are weaker and less hardy than
humans, but quicker of body and mind, and
generally make more powerful spellcasters.
However, they have a notoriously offhanded
attitude toward their religious devotions
and make poor priests.

As a race, Elves possess a preternatural
beauty and eyes that see in the dark.
endmsg
face elf.x51
is_animated 1
str -2
dex 3
con -2
wis -3
pow 2
cha 4
int 2
food 500
dam 1
wc 21
ac 10
speed 1
level 1
attacktype 1
weight 70000
randomitems elf_player_items
alive 1
can_use_shield 1
can_use_armour 1
can_use_weapon 1
can_see_in_dark 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_neck 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
anim_speed -1
end

object elf_player_force
name elf_force
face blank.x11
hp -1
sp 1
food 2
speed 0
type 114
invisible 1
applied 1
no_drop 1
can_see_in_dark 1
end

