object fireborn_player
anim
fireborn.x11
fireborn.x12
fireborn.x13
fireborn.x14
mina
inherit type_player
name fireborn
race fire_elemental
msg
Legends say that fireborn are sparks struck
off the anvil of creation, with life and
spirit, and eventually mind. Those who
research magic think that they are relatives
of some kind to will o' the wisps. An
observer seeing one for the first time would
see a strangely intelligent vortex of
flame somehow carrying items without burning
them.

Their insubstantial nature makes them both
very weak and very quick.  Their minds are
agile, and they are able to commune well with
the gods. However, their area of excellence
is magic. They spellcast more powerfully than
any other race, and mana flows into them
readily. They can even cast cold spells with
devastating effectiveness.  They all know a
basic fire spell.

They have the ability to levitate at will, and
they can touch to burn. As they gain more
power, they are increasingly difficult to hit
with weapons. However, they cannot use weapons
or armour, and thereby miss out on many benefits
(including powerful magical enchantments) those
items may bring.

Fire does not harm them, nor poison. Cold,
spiritual drain, and physical drain present
great peril to them.
endmsg
face fireborn.x11
is_animated 1
str -5
dex 4
con 0
wis 2
pow 7
cha -2
int 2
food 999
dam 0
wc 21
ac 0
speed 1
level 1
attacktype 5
resist_fire 100
resist_cold -30
resist_ghosthit -50
resist_poison 100
path_attuned 2
weight 30000
glow_radius 4
randomitems fireborn_player_items
alive 1
can_use_shield 0
can_use_armour 0
can_use_weapon 0
body_range 1
body_neck 2
body_skill 1
body_finger 4
# Do they have shoulders?  Or waists?  Or wrists?
end

object fireborn_player_force
name Fireborn_Force
face blank.x11
sp 2
speed 0
type 114
invisible 1
applied 1
no_drop 1
end

