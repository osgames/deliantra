object northman_player
anim
player_northman.x11
player_northman.x12
player_northman.x31
player_northman.x32
player_northman.x51
player_northman.x52
player_northman.x71
player_northman.x72
facings 4
mina
inherit type_player
name Northman
race human
msg
Northmen are simply humans who have been
shaped by their harsh environment. They are
stronger, quicker, and hardier than most
humans, but also stupid and impious. Most
races also consider them a bit unattractive.
They have some resistance to cold, but the
few of them who take up magic find it
difficult to master fire spells.
endmsg
face player_northman.x51
is_animated 1
str 1
dex 1
con 2
wis -1
pow -1
cha -1
int -2
food 500
dam 1
wc 21
ac 10
speed 0.5
level 1
attacktype 1
resist_cold 30
path_repelled 2
weight 70000
randomitems northman_player_items
alive 1
can_use_shield 1
can_use_armour 1
can_use_weapon 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_neck 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
anim_speed -1
end

