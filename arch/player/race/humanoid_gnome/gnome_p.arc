object gnome_player
anim
gnome_p.x11
gnome_p.x12
gnome_p.x31
gnome_p.x32
gnome_p.x51
gnome_p.x52
gnome_p.x71
gnome_p.x72
facings 4
mina
inherit type_player
name gnome
race dwarf
msg
Gnomes and dwarves look so much alike that
some speculate that they are the same race,
but with very different cultures. No one has
been able to talk a Dwarf and a Gnome into
attempting interbreeding, however, which would
settle the issue.  Dwarves and gnomes have a
disgust for one another and both find the
thought of intimacy with the other repulsive.
Despite that, they coexist peacefully enough:
perhaps because their interests and needs
never conflict.

Gnomes do not have nearly the strength or
hardiness of dwarves, and they are even more
clumsy. They are quite spiritual, and make
very good priests, and many are very strong
magicians.  They can see in the dark, and their
familiarity with the gods seems to bring a
general blessing of good luck on all Gnomekind.
endmsg
face gnome_p.x51
is_animated 1
str -1
dex -3
con -2
wis 3
pow 3
cha 0
int 0
food 999
dam 1
luck 1
wc 21
ac 10
speed 1
level 1
attacktype 1
weight 50000
randomitems gnome_player_items
alive 1
can_use_shield 1
can_use_armour 1
can_use_weapon 1
can_see_in_dark 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_neck 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
anim_speed -1
end

