object wraith_player
anim
wraithp.x11
wraithp.x12
wraithp.x31
wraithp.x32
wraithp.x51
wraithp.x52
wraithp.x71
wraithp.x72
facings 4
mina
inherit type_player
name Wraith
race undead
msg
A Wraith is the spirit of a person who has
made an evil bargain in order to persist
after natural death, or has been forced
involuntarily into its current undead state.
This transformation frees them of the limits
of a natural lifespan, and grants them
immunity to diseases, poison, and loss of
lifeforce.  Wraiths also need feel little
fear of cold, and they can see well in the
dark.  A Wraith's corporeal form has terrible
vulnerabilities: fire and most godpower are
dangerous to Wraiths. Their tenuous form
makes them quick and somewhat hard to hit,
but also weaker and less hardy.

Wraiths have a strong affinity to magic,
especially cold spells, and are very strong
spellcasters. However, they cannot use fire
spells for fear of self-destruction.
endmsg
face wraithp.x51
is_animated 1
str -3
dex 4
con -3
wis -3
pow 3
cha -4
int 2
food 999
dam 1
wc 21
ac 6
level 1
attacktype 17
resist_fire -25
resist_cold 30
resist_drain 100
resist_ghosthit 100
resist_poison 100
path_attuned 4
path_denied 2
weight 30000
randomitems wraith_player_items
alive 1
can_use_shield 1
undead 1
can_use_armour 1
can_use_weapon 1
can_see_in_dark 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_neck 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
anim_speed -1
end

object wraith_player_force
name Wraith_Force
face blank.x11
food 60
speed 0
type 114
invisible 1
applied 1
no_drop 1
can_see_in_dark 1
end

