object dwarf_player
anim
dwarf_p.x11
dwarf_p.x12
dwarf_p.x31
dwarf_p.x32
dwarf_p.x51
dwarf_p.x52
dwarf_p.x71
dwarf_p.x72
facings 4
mina
inherit type_player
name dwarf
race dwarf
msg
The Dwarves are the hardy scions of Mostrai.
The traditional profession of the dwarf is
smithery, and you'll be hard-pressed to find
a dwarf who is not competent at it. Whether
their great strength and hardiness leads them
to become smiths and miners, or whether their
profession leads to their strength and
hardiness is unknown. Their thick musculature
makes them slow of body, and seemingly, some
of their muscle has crept into their brain.
Perhaps their repetitive tasks dull their
minds, perhaps they're simply born dumb.

Dwarves can see in the dark, but they're
slightly impious and tend to be a bit poor at
controlling magic. Despite this, there are
indeed dwarven clerics and mages. They're
just not quite as good as human clerics or
mages.
endmsg
face dwarf_p.x51
is_animated 1
str 3
dex -2
con 4
wis -1
pow -2
cha -1
int -2
food 999
dam 1
wc 21
ac 10
speed 1
level 1
attacktype 1
weight 80000
randomitems dwarf_player_items
alive 1
can_use_shield 1
can_use_armour 1
can_use_weapon 1
can_see_in_dark 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_neck 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
anim_speed -1
end

