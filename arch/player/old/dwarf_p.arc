object dwarf_p
anim
dwarf_p.x51
dwarf_p.x31
dwarf_p.x11
dwarf_p.x71
facings 4
mina
name dwarf
face dwarf_p.x11
is_animated 0
str 3
dex -4
con 4
cha -3
food 500
dam 1
wc 21
ac 10
speed 0.5
level 1
type 37
attacktype 1
weight 80000
randomitems dwarf
alive 1
can_use_shield 1
can_use_armour 1
can_use_weapon 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_neck 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
end

