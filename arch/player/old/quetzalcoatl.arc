object quetzalcoatl
anim
facings 2
quetzalcoatl.x31
quetzalcoatl.x32
quetzalcoatl.x71
quetzalcoatl.x72
mina
name Quetzalcoatl
race Quetzalcoatl
face quetzalcoatl.x31
is_animated 1
str 6
con 6
wis -8
pow 8
int -8
food 999
dam 10
wc 21
ac 5
speed 0.5
level 1
type 37
attacktype 1
resist_fire 100
resist_cold -100
resist_poison -100
resist_paralyze -100
weight 70000
randomitems quetzalcoatl
alive 1
can_use_shield 0
can_use_armour 0
can_use_weapon 1
body_range 1
body_arm 2
body_neck 1
body_skill 1
body_finger 2
body_shoulder 1
body_hand 2
body_wrist 2
body_waist 1
end

