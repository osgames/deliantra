object Wraith
anim
wraithp.x11
wraithp.x31
wraithp.x51
wraithp.x71
facings 4
mina
name Wraith
race Wraith
face wraithp.x11
is_animated 0
str -3
dex 4
con -3
wis -3
pow 3
cha -10
int 2
food 500
dam 1
wc 21
ac 6
level 1
type 37
attacktype 17
resist_fire -100
resist_cold 30
resist_drain 100
resist_ghosthit 100
resist_poison 100
weight 30000
randomitems wizard
alive 1
can_use_shield 1
undead 1
can_use_armour 1
can_use_weapon 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_neck 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
end

