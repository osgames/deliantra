object viking
anim
viking.x11
viking.x31
viking.x51
viking.x71
facings 4
mina
name viking
race viking
face viking.x71
is_animated 0
str 1
dex 1
con 2
wis -1
cha -1
int -2
food 500
dam 1
wc 21
ac 10
speed 0.5
level 1
type 37
attacktype 1
weight 70000
randomitems viking
alive 1
can_use_shield 1
can_use_armour 1
can_use_weapon 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_neck 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
end

