object fireborn
anim
fireborn.x11
fireborn.x12
fireborn.x13
fireborn.x14
mina
name fireborn
race fireborn
face fireborn.x11
is_animated 1
str -7
dex 4
con -4
pow 6
cha -4
int 3
food 999
dam 0
wc 21
ac 0
speed 1
level 1
type 37
attacktype 5
resist_fire 100
resist_cold -100
resist_drain -100
resist_ghosthit -100
resist_poison 100
weight 30000
randomitems fireborn
alive 1
can_use_shield 0
can_use_armour 0
can_use_weapon 0
body_range 1
body_neck 1
body_skill 1
body_finger 2
end

