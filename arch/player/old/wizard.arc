object wizard
anim
wizard.x11
wizard.x12
wizard.x21
wizard.x22
wizard.x31
wizard.x32
wizard.x41
wizard.x42
wizard.x51
wizard.x52
wizard.x61
wizard.x62
wizard.x71
wizard.x72
wizard.x81
wizard.x82
facings 8
mina
name wizard
race wizard
face wizard.x71
is_animated 0
str -3
dex 0
con -3
wis 0
pow 3
cha 0
int 3
food 500
dam 1
wc 21
ac 10
speed 0.5
level 1
type 37
attacktype 1
weight 70000
randomitems wizard
alive 1
can_use_shield 1
can_use_armour 1
can_use_weapon 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_neck 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
end

