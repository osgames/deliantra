object ability_stone_throw
inherit type_bow
name rock thrower
race rock
skill missile weapons
sp 40
dam 10
wc 1
nrof 1
invisible 1
no_drop 1
no_strength 0
end

