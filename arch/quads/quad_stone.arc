object quad_stone_floor
inherit type_quad_floor
name stone floor
name_pl stone floors
face quad_stone_floor.x11
smoothlevel 5
smoothface quad_stone_floor.x11 quad_stone_floor_S.x11
other_arch quad_stone_material
end

object quad_stone_wall
inherit type_quad_wall
name stone wall
name_pl stone walls
face quad_stone_wall.x11
other_arch quad_stone_material
end

object quad_stone_material
inherit type_quad_material
name stone material
name_pl stone materials
face buildmaterial:quad_stone_wall.x11
slaying quad_stone_floor
other_arch quad_stone_wall
end
