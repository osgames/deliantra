# should become nithong when removed, or maybe one water?
object quad_water_floor
inherit type_quad_floor
name water
name_pl water
face sea.x11
end

# just a different name?
object quad_ocean_floor
inherit quad_water_floor
name ocean
name_pl ocean
end

# special "endless source of water" quad?
object quad_water_source
inherit type_quad_wall
name water
name_pl water
face quad_water_wall.x11
type 25
subtype 1
end

# water, with 7 animation faces (levels)
object quad_water_flow
inherit type_quad_wall
name water
name_pl water
face quad_water_wall.x11
anim
quad_water_wall.x11
quad_water_wall.x12
quad_water_wall.x13
quad_water_wall.x14
quad_water_wall.x15
quad_water_wall.x16
quad_water_wall.x17
mina
is_animated 0
type 25
subtype 2
move_block -all
end
