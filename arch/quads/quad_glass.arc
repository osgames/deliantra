#object quad_dirt_floor
#inherit type_quad_floor
#is_transparent_floor 1
#name glass floor
#name_pl glass floors
#face quad_glass_floor.x11
#smoothlevel 5
#smoothface quad_dirt_floor.x11 quad_dirt_floor_S.x11
#end

object quad_glass_wall
inherit type_quad_wall
name window
name_pl windows
blocksview 0
face quad_glass_wall.x11
other_arch quad_glass_material
end

object quad_glass_material
inherit type_quad_material
name glas material
name_pl glas materials
face buildmaterial:quad_glass_wall.x11
other_arch quad_glass_wall
end
