object quad_dirt_floor
inherit type_quad_floor
name dirt floor
name_pl dirt floors
face quad_dirt_floor.x11
smoothlevel 5
smoothface quad_dirt_floor.x11 quad_dirt_floor_S.x11
other_arch quad_dirt_material
end

object quad_dirt_wall
inherit type_quad_wall
name dirt wall
name_pl dirt walls
face quad_dirt_wall.x11
other_arch quad_dirt_material
end

object quad_dirt_material
inherit type_quad_material
name dirt material
name_pl dirt materials
face buildmaterial:quad_dirt_wall.x11
slaying quad_dirt_floor
other_arch quad_dirt_wall
end
