object quad_marble_floor
inherit type_quad_floor
name marble floor
name_pl marble floors
face quad_marble_floor.x11
other_arch quad_marble_material
end

object quad_marble_wall
inherit type_quad_wall
name marble wall
name_pl marble walls
face quad_marble_wall.x11
other_arch quad_marble_material
end

object quad_marble_material
inherit type_quad_material
name marble material
name_pl marble materials
face buildmaterial:quad_marble_wall.x11
slaying quad_marble_floor
other_arch quad_marble_wall
end
