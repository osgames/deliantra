object trap
anim
trap.x11
trap.x11
mina
name trap
msg
You set off a trap!
endmsg
face trap.x11
is_animated 0
cha 20
hp 1
dam 90
speed 1
level 1
type 97
attacktype 1
invisible 1
move_on walk
no_pick 1
end

