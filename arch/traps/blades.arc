object trap_blades
anim
blades.x11
blades.x11
mina
name Blades trap
msg
You set off a Blades trap!
endmsg
face blades.x11
is_animated 0
cha 20
hp 1
dam 90
speed 1
level 1
type 97
attacktype 1
invisible 1
move_on walk
no_pick 1
end

