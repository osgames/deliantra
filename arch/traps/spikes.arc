object trap_spikes2
anim
tspikes.x11
tspikes.x11
mina
name spikes
msg
You are stabbed by spikes!
endmsg
face tspikes.x11
is_animated 0
cha 20
hp 1
dam 20
speed 1
level 1
type 97
attacktype 1
invisible 1
move_on walk
no_pick 1
end

