object trap_needle
anim
needle.x11
needle.x11
mina
name poison needle
msg
You are pricked by a poison needle!
endmsg
face needle.x11
is_animated 0
cha 20
hp 1
dam 10
speed 1
level 1
type 97
attacktype 1025
invisible 1
move_on walk
no_pick 1
end

