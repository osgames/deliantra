object trap_rocks
anim
falling_rocks.x11
falling_rocks.x11
mina
name falling rocks trap
msg
You set off a falling rocks trap!
endmsg
face falling_rocks.x11
is_animated 0
cha 20
hp 1
dam 50
speed 1
level 1
type 97
attacktype 1
invisible 1
move_on walk
no_pick 1
end

