object trap_spike_pit
anim
spiked_pit.x11
spiked_pit.x11
mina
name spiked pit
msg
You are stabbed by spikes!
endmsg
face spiked_pit.x11
is_animated 0
cha 15
hp 1
dam 40
speed 1
level 1
type 97
attacktype 1
invisible 1
move_on walk
no_pick 1
end

