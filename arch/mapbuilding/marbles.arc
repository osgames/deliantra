object building_blackmarble
name Black Marble floor material
name_pl Black Marble floor materials
slaying blackmarble
face buildmaterial:blackmarble.x11
nrof 1
type 126
subtype 1
value 40540
weight 1500
end

object building_greenmarble
name Green Marble floor material
name_pl Green Marble floor materials
slaying greenmarble
face buildmaterial:greenmarble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_lightmagentamarble
name Light Magenta Marble floor material
name_pl Light Magenta Marble floor materials
slaying lightmagentamarble
face buildmaterial:lightmagentamarble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_marble
name White Marble floor material
name_pl White Marble floor materials
slaying marble
face buildmaterial:marble.x11
nrof 1
type 126
subtype 1
value 40540
weight 1500
end

object building_pinkmarble
name Pink Marble floor material
name_pl Pink Marble floor materials
slaying pinkmarble
face buildmaterial:pinkmarble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

## Added by boes 2009-11-05/06
object building_beigemarble
name Beige Marble floor material
name_pl Beige Marble floor materials
slaying beigemarble
face buildmaterial:beigemarble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_lightgreenmarble
name Light Green Marble floor material
name_pl Light Green Marble floor materials
slaying lightgreenmarble
face buildmaterial:lightgreenmarble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_graymarble
name Gray Marble floor material
name_pl Gray Marble floor materials
slaying graymarble
face buildmaterial:graymarble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_cyanmarble
name Cyan Marble floor material
name_pl Cyan Marble floor materials
slaying cyanmarble
face buildmaterial:cyanmarble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_redmarble
name Red Marble floor material
name_pl Red Marble floor materials
slaying redmarble
face buildmaterial:redmarble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_magentamarble
name Dark Magenta Marble floor material
name_pl Dark Magenta Marble floor materials
slaying magentamarble
face buildmaterial:magentamarble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_marble_blgr
name Blue Grey Marble floor material
name_pl Blue Grey Marble floor materials
slaying marble_blgr
face buildmaterial:marble_blgr.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end
object building_beige-cyan-c-marble_111
name Beige Cyan Circle 1 Marble floor material
name_pl Beige Cyan Circle 1 Marble floor materials
slaying beige-cyan-c-marble_111
face buildmaterial:beige-cyan-c-marble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_beige-cyan-c-marble_112
name Beige Cyan Circle 2 Marble floor material
name_pl Beige Cyan Circle 2 Marble floor materials
slaying beige-cyan-c-marble_112
face buildmaterial:beige-cyan-c-marble.x12
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_beige-cyan-c-marble_113
name Beige Cyan Circle 3 Marble floor material
name_pl Beige Cyan Circle 3 Marble floor materials
slaying beige-cyan-c-marble_113
face buildmaterial:beige-cyan-c-marble.x13
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_beige-cyan-c-marble_114
name Beige Cyan Circle 4 Marble floor material
name_pl Beige Cyan Circle 4 Marble floor materials
slaying beige-cyan-c-marble_114
face buildmaterial:beige-cyan-c-marble.x14
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_beige-green-c-marble_111
name Beige Green Circle 1 Marble floor material
name_pl Beige Green Circle 1 Marble floor materials
slaying beige-green-c-marble_111
face buildmaterial:beige-green-c-marble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_beige-green-c-marble_112
name Beige Green Circle 2 Marble floor material
name_pl Beige Green Circle 2 Marble floor materials
slaying beige-green-c-marble_112
face buildmaterial:beige-green-c-marble.x12
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_beige-green-c-marble_113
name Beige Green Circle 3 Marble floor material
name_pl Beige Green Circle 3 Marble floor materials
slaying beige-green-c-marble_113
face buildmaterial:beige-green-c-marble.x13
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_beige-green-c-marble_114
name Beige Green Circle 4 Marble floor material
name_pl Beige Green Circle 4 Marble floor materials
slaying beige-green-c-marble_114
face buildmaterial:beige-green-c-marble.x14
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_beige-pink-c-marble_111
name Beige Pink Circle 1 Marble floor material
name_pl Beige Pink Circle 1 Marble floor materials
slaying beige-pink-c-marble_111
face buildmaterial:beige-pink-c-marble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_beige-pink-c-marble_112
name Beige Pink Circle 2 Marble floor material
name_pl Beige Pink Circle 2 Marble floor materials
slaying beige-pink-c-marble_112
face buildmaterial:beige-pink-c-marble.x12
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_beige-pink-c-marble_113
name Beige Pink Circle 3 Marble floor material
name_pl Beige Pink Circle 3 Marble floor materials
slaying beige-pink-c-marble_113
face buildmaterial:beige-pink-c-marble.x13
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_beige-pink-c-marble_114
name Beige Pink Circle 4 Marble floor material
name_pl Beige Pink Circle 4 Marble floor materials
slaying beige-pink-c-marble_114
face buildmaterial:beige-pink-c-marble.x14
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_beige-white-c-marble_111
name Beige White Circle 1 Marble floor material
name_pl Beige White Circle 1 Marble floor materials
slaying beige-white-c-marble_111
face buildmaterial:beige-white-c-marble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_beige-white-c-marble_112
name Beige White Circle 2 Marble floor material
name_pl Beige White Circle 2 Marble floor materials
slaying beige-white-c-marble_112
face buildmaterial:beige-white-c-marble.x12
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_beige-white-c-marble_113
name Beige White Circle 3 Marble floor material
name_pl Beige White Circle 3 Marble floor materials
slaying beige-white-c-marble_113
face buildmaterial:beige-white-c-marble.x13
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_beige-white-c-marble_114
name Beige White Circle 4 Marble floor material
name_pl Beige White Circle 4 Marble floor materials
slaying beige-white-c-marble_114
face buildmaterial:beige-white-c-marble.x14
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_cyan-beige-c-marble_111
name Cyan Beige Circle 1 Marble floor material
name_pl Cyan Beige Circle 1 Marble floor materials
slaying cyan-beige-c-marble_111
face buildmaterial:cyan-beige-c-marble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_cyan-beige-c-marble_112
name Cyan Beige Circle 2 Marble floor material
name_pl Cyan Beige Circle 2 Marble floor materials
slaying cyan-beige-c-marble_112
face buildmaterial:cyan-beige-c-marble.x12
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_cyan-beige-c-marble_113
name Cyan Beige Circle 3 Marble floor material
name_pl Cyan Beige Circle 3 Marble floor materials
slaying cyan-beige-c-marble_113
face buildmaterial:cyan-beige-c-marble.x13
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_cyan-beige-c-marble_114
name Cyan Beige Circle 4 Marble floor material
name_pl Cyan Beige Circle 4 Marble floor materials
slaying cyan-beige-c-marble_114
face buildmaterial:cyan-beige-c-marble.x14
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_green-beige-c-marble_111
name Green Beige Circle 1 Marble floor material
name_pl Green Beige Circle 1 Marble floor materials
slaying green-beige-c-marble_111
face buildmaterial:green-beige-c-marble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_green-beige-c-marble_112
name Green Beige Circle 2 Marble floor material
name_pl Green Beige Circle 2 Marble floor materials
slaying green-beige-c-marble_112
face buildmaterial:green-beige-c-marble.x12
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_green-beige-c-marble_113
name Green Beige Circle 3 Marble floor material
name_pl Green Beige Circle 3 Marble floor materials
slaying green-beige-c-marble_113
face buildmaterial:green-beige-c-marble.x13
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_green-beige-c-marble_114
name Green Beige Circle 4 Marble floor material
name_pl Green Beige Circle 4 Marble floor materials
slaying green-beige-c-marble_114
face buildmaterial:green-beige-c-marble.x14
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_green-white-c-marble_111
name Green White Circle 1 Marble floor material
name_pl Green White Circle 1 Marble floor materials
slaying green-white-c-marble_111
face buildmaterial:green-white-c-marble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_green-white-c-marble_112
name Green White Circle 2 Marble floor material
name_pl Green White Circle 2 Marble floor materials
slaying green-white-c-marble_112
face buildmaterial:green-white-c-marble.x12
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_green-white-c-marble_113
name Green White Circle 3 Marble floor material
name_pl Green White Circle 3 Marble floor materials
slaying green-white-c-marble_113
face buildmaterial:green-white-c-marble.x13
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_green-white-c-marble_114
name Green White Circle 4 Marble floor material
name_pl Green White Circle 4 Marble floor materials
slaying green-white-c-marble_114
face buildmaterial:green-white-c-marble.x14
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_pink-beige-c-marble_111
name Pink Beige Circle 1 Marble floor material
name_pl Pink Beige Circle 1 Marble floor materials
slaying pink-beige-c-marble_111
face buildmaterial:pink-beige-c-marble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_pink-beige-c-marble_112
name Pink Beige Circle 2 Marble floor material
name_pl Pink Beige Circle 2 Marble floor materials
slaying pink-beige-c-marble_112
face buildmaterial:pink-beige-c-marble.x12
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_pink-beige-c-marble_113
name Pink Beige Circle 3 Marble floor material
name_pl Pink Beige Circle 3 Marble floor materials
slaying pink-beige-c-marble_113
face buildmaterial:pink-beige-c-marble.x13
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_pink-beige-c-marble_114
name Pink Beige Circle 4 Marble floor material
name_pl Pink Beige Circle 4 Marble floor materials
slaying pink-beige-c-marble_114
face buildmaterial:pink-beige-c-marble.x14
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-beige-c-marble_111
name White Beige Circle 1 Marble floor material
name_pl White Beige Circle 1 Marble floor materials
slaying white-beige-c-marble_111
face buildmaterial:white-beige-c-marble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-beige-c-marble_112
name White Beige Circle 2 Marble floor material
name_pl White Beige Circle 2 Marble floor materials
slaying white-beige-c-marble_112
face buildmaterial:white-beige-c-marble.x12
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-beige-c-marble_113
name White Beige Circle 3 Marble floor material
name_pl White Beige Circle 3 Marble floor materials
slaying white-beige-c-marble_113
face buildmaterial:white-beige-c-marble.x13
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-beige-c-marble_114
name White Beige Circle 4 Marble floor material
name_pl White Beige Circle 4 Marble floor materials
slaying white-beige-c-marble_114
face buildmaterial:white-beige-c-marble.x14
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-green-c-marble_111
name White Green Circle 1 Marble floor material
name_pl White Green Circle 1 Marble floor materials
slaying white-green-c-marble_111
face buildmaterial:white-green-c-marble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-green-c-marble_112
name White Green Circle 2 Marble floor material
name_pl White Green Circle 2 Marble floor materials
slaying white-green-c-marble_112
face buildmaterial:white-green-c-marble.x12
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-green-c-marble_113
name White Green Circle 3 Marble floor material
name_pl White Green Circle 3 Marble floor materials
slaying white-green-c-marble_113
face buildmaterial:white-green-c-marble.x13
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-green-c-marble_114
name White Green Circle 4 Marble floor material
name_pl White Green Circle 4 Marble floor materials
slaying white-green-c-marble_114
face buildmaterial:white-green-c-marble.x14
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-pink-c-marble_111
name White Pink Circle 1 Marble floor material
name_pl White Pink Circle 1 Marble floor materials
slaying white-pink-c-marble_111
face buildmaterial:white-pink-c-marble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-pink-c-marble_112
name White Pink Circle 2 Marble floor material
name_pl White Pink Circle 2 Marble floor materials
slaying white-pink-c-marble_112
face buildmaterial:white-pink-c-marble.x12
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-pink-c-marble_113
name White Pink Circle 3 Marble floor material
name_pl White Pink Circle 3 Marble floor materials
slaying white-pink-c-marble_113
face buildmaterial:white-pink-c-marble.x13
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-pink-c-marble_114
name White Pink Circle 4 Marble floor material
name_pl White Pink Circle 4 Marble floor materials
slaying white-pink-c-marble_114
face buildmaterial:white-pink-c-marble.x14
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-red-c-marble_111
name White Red Circle 1 Marble floor material
name_pl White Red Circle 1 Marble floor materials
slaying white-red-c-marble_111
face buildmaterial:white-red-c-marble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-red-c-marble_112
name White Red Circle 2 Marble floor material
name_pl White Red Circle 2 Marble floor materials
slaying white-red-c-marble_112
face buildmaterial:white-red-c-marble.x12
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-red-c-marble_113
name White Red Circle 3 Marble floor material
name_pl White Red Circle 3 Marble floor materials
slaying white-red-c-marble_113
face buildmaterial:white-red-c-marble.x13
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-red-c-marble_114
name White Red Circle 4 Marble floor material
name_pl White Red Circle 4 Marble floor materials
slaying white-red-c-marble_114
face buildmaterial:white-red-c-marble.x14
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_gray-white-h-marble_111
name Gray White Angle 1 Marble floor material
name_pl Gray White Angle 1 Marble floor materials
slaying gray-white-h-marble_111
face buildmaterial:gray-white-h-marble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_gray-white-h-marble_112
name Gray White Angle 2 Marble floor material
name_pl Gray White Angle 2 Marble floor materials
slaying gray-white-h-marble_112
face buildmaterial:gray-white-h-marble.x12
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-gray-h-marble_111
name White Gray Angle 1 Marble floor material
name_pl White Gray Angle 1 Marble floor materials
slaying white-gray-h-marble_111
face buildmaterial:white-gray-h-marble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-gray-h-marble_112
name White Gray Angle 2 Marble floor material
name_pl White Gray Angle 2 Marble floor materials
slaying white-gray-h-marble_112
face buildmaterial:white-gray-h-marble.x12
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-green-h-marble_111
name White Green Angle 1 Marble floor material
name_pl White Green Angle 1 Marble floor materials
slaying white-green-h-marble_111
face buildmaterial:white-green-h-marble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_white-green-h-marble_112
name White Green Angle 2 Marble floor material
name_pl White Green Angle 2 Marble floor materials
slaying white-green-h-marble_112
face buildmaterial:white-green-h-marble.x12
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_green-white-h-marble_111
name Green White Angle 1 Marble floor material
name_pl Green White Angle 1 Marble floor materials
slaying green-white-h-marble_111
face buildmaterial:green-white-h-marble.x11
nrof 1
type 126
subtype 1
value 50000
weight 1500
end

object building_green-white-h-marble_112
name Green White Angle 2 Marble floor material
name_pl Green White Angle 2 Marble floor materials
slaying green-white-h-marble_112
face buildmaterial:green-white-h-marble.x12
nrof 1
type 126
subtype 1
value 50000
weight 1500
## end add by boes
end
