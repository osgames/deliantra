object building_builder
name generic builder
name_pl generic builders
msg
To use, mark a material, then fire the builder at the square you want to build in.
endmsg
face mbtrowel.x11
nrof 1
type 125
subtype 1
value 150
weight 1500
end

object building_button
name Button material
name_pl Button materials
slaying button_small
msg
Creates a button which triggers linked items when anything with weight [such as a player] is on it. If you want something hidden, use a pedestal instead.

Place a Marking Rune on the map square before building. The rune will be removed, and the button will be linked to all other elements on the map marked with the same text.
endmsg
face buildmaterial-sbox:button_sma.x11
nrof 1
type 126
subtype 3
value 2000
weight 100
end

object building_destroyer
name generic destroyer
name_pl generic destroyers
msg
To use, fire the destroyer at the element you want to destroy. Can only destroy things built with a builder. Will destroy the lowest item in a square first.

If you want to remove a wall, build a floor over it instead.
endmsg
face mbhammer.x11
nrof 1
type 125
subtype 2
value 150
weight 1500
end

object building_horizontal_gate
name Horizontal gate material
name_pl Horizontal gate materials
slaying igate_closed_2
msg
Place a Marking Rune on the map square before building. The rune will be removed, and the gate will be linked to all other elements on the map marked with the same text.
endmsg
face buildmaterial:iron_gate2.x16
nrof 1
type 126
subtype 3
value 1500
weight 150
end

object building_horizontal_gate_inv
name Horizontal inverted gate material
name_pl Horizontal inverted gate materials
slaying igate_open_2
msg
Place a Marking Rune on the map square before building. The rune will be removed, and the gate will be linked to all other elements on the map marked with the same text.
endmsg
face buildmaterial:iron_gate2.x11
nrof 1
type 126
subtype 3
value 3000
weight 150
end

object building_lever
name Lever material
name_pl Lever materials
slaying button_lever
msg
Creates a lever which triggers linked items when applied.

Place a Marking Rune on the map square before building. The rune will be removed, and the lever will be linked to all other elements on the map marked with the same text.
endmsg
face buildmaterial:lever.x11
nrof 1
type 126
subtype 3
value 1500
weight 150
end

object building_magma
name Magma floor material
name_pl Magma floor materials
slaying magma
face buildmaterial:magma.x11
nrof 1
type 126
subtype 1
value 6666
weight 1500
end

object building_pedestal
name Pedestal material
name_pl Pedestal materials
slaying pedestal
msg
Creates a pedestal which triggers linked items when a player stands on it. The pedestal will be hidden under the floor. If you want something visible, use a button instead.

Place a Marking Rune on the map square before building. The rune will be removed, and the pedestal will be linked to all other elements on the map marked with the same text.
endmsg
face buildmaterial:pedestal.x11
str 1
nrof 1
type 126
subtype 3
value 1500
weight 150
end

object building_redrugfloor1
name Red Rug floor material
name_pl Red Rug floor materials
slaying redrugfloor1
face buildmaterial:redrugfloor1.x11
nrof 1
type 126
subtype 1
value 16000
weight 500
end

object building_vertical_gate
name Vertical gate material
name_pl Vertical gate materials
slaying igate_closed_1
msg
Place a Marking Rune on the map square before building. The rune will be removed, and the gate will be linked to all other elements on the map marked with the same text.
endmsg
face buildmaterial:iron_gate1.x18
nrof 1
type 126
subtype 3
value 1500
weight 150
end

object building_vertical_gate_inv
name Vertical inverted gate material
name_pl Vertical inverted gate materials
slaying igate_open_1
msg
Place a Marking Rune on the map square before building. The rune will be removed, and the gate will be linked to all other elements on the map marked with the same text.
endmsg
face buildmaterial:iron_gate1.x11
nrof 1
type 126
subtype 3
value 3000
weight 150
end

object building_wall
name Wall material
name_pl Wall materials
slaying wall_0
face buildmaterial:wall_0.x11
nrof 1
type 126
subtype 2
value 1500
weight 1500
end

object building_wall2
name DWall material
name_pl DWall materials
slaying dwall_0
face buildmaterial:dwall_0.x11
nrof 1
type 126
subtype 2
value 1750
weight 1700
end

object building_wall2_fake
name DWall material
name_pl DWall materials
slaying dwall_fake_0
face buildmaterial:dwall_0.x11
nrof 1
type 126
subtype 2
value 1750
weight 1700
end

object building_wall3
name WestCWall material
name_pl WestCWall materials
slaying westcwall_0
face buildmaterial:westcwall_0.x11
nrof 1
type 126
subtype 2
value 1650
weight 1700
end

object building_wall3_fake
name WestCWall material
name_pl WestCWall materials
slaying westcwall_fake_0
face buildmaterial:westcwall_0.x11
nrof 1
type 126
subtype 2
value 1650
weight 1700
end

object building_wall4
name Red CWall material
name_pl Red CWall materials
slaying redcwall_0
face buildmaterial:redcwall_0.x11
nrof 1
type 126
subtype 2
value 1900
weight 1700
end

object building_wall4_fake
name Red CWall material
name_pl Red CWall materials
slaying redcwall_fake_0
face buildmaterial:redcwall_0.x11
nrof 1
type 126
subtype 2
value 1900
weight 1700
end

object building_wall5
name EastWall material
name_pl EastWall materials
slaying ewall_0
face buildmaterial:ewall_0.x11
nrof 1
type 126
subtype 2
value 1600
weight 1000
end

object building_wall5_fake
name EastWall material
name_pl EastWall materials
slaying ewall_fake_0
face buildmaterial:ewall_0.x11
nrof 1
type 126
subtype 2
value 1600
weight 1000
end

object building_wall6
name BurningWall material
name_pl BurningWall materials
slaying burningwall_0
face buildmaterial:burningwall_0.x11
nrof 1
type 126
subtype 2
value 1650
weight 1700
end

object building_wall6_fake
name BurningWall material
name_pl BurningWall materials
slaying burningwall_fake_0
face buildmaterial:burningwall_0.x11
nrof 1
type 126
subtype 2
value 1650
weight 1700
end

object building_wall7
name STWall material
name_pl STWall materials
slaying stwall_0
face buildmaterial:stwall_0.x11
nrof 1
type 126
subtype 2
value 1850
weight 2100
end

object building_wall7_fake
name STWall material
name_pl STWall materials
slaying stwall_fake_0
face buildmaterial:stwall_0.x11
nrof 1
type 126
subtype 2
value 1850
weight 2100
end

object building_wall8
name WWall material
name_pl WWall materials
slaying wwall_0
face buildmaterial:wwall_0.x11
nrof 1
type 126
subtype 2
value 1550
weight 1600
end

object building_wall8_fake
name WWall material
name_pl WWall materials
slaying wwall_fake_0
face buildmaterial:wwall_0.x11
nrof 1
type 126
subtype 2
value 1550
weight 1600
end

object building_wall9
name Earth Wall material
name_pl Earth Wall materials
slaying mbbwall_0
face buildmaterial:bwall_0.x11
nrof 1
type 126
subtype 2
value 800
weight 2000
end

object building_wall_fake
name Wall material
name_pl Wall materials
slaying wall_fake_0
face buildmaterial:wall_0.x11
nrof 1
type 126
subtype 2
value 1500
weight 1500
end

object building_whiterugfloor1
name White Rug floor material
name_pl White Rug floor materials
slaying whiterugfloor1
face buildmaterial:whiterugfloor1.x11
nrof 1
type 126
subtype 1
value 16000
weight 500
end

object building_woodfloor
name Wood floor material
name_pl Wood floor materials
slaying woodfloor
face buildmaterial:woodfloor.x11
nrof 1
type 126
subtype 1
value 1500
weight 1500
end

object building_woodfloor2
name Dark Wood floor material
name_pl Dark Wood floor materials
slaying woodfloor2
face buildmaterial:woodfloor2.x11
nrof 1
type 126
subtype 1
value 1500
weight 1500
end

object shop_building
name tiles
face shop_building.x11
type 68
randomitems shop_building
no_pick 1
auto_apply 1
no_magic 1
is_floor 1
end

object shop_building_abyss
name tiles
face shop_building.x11
type 68
randomitems shop_building_abyss
no_pick 1
auto_apply 1
no_magic 1
is_floor 1
end

object shop_building_east
name tiles
face shop_building.x11
type 68
randomitems shop_building_east
no_pick 1
auto_apply 1
no_magic 1
is_floor 1
end

object shop_building_fant
name tiles
face shop_building.x11
type 68
randomitems shop_building_fant
no_pick 1
auto_apply 1
no_magic 1
is_floor 1
end

object shop_building_goth
name tiles
face shop_building.x11
type 68
randomitems shop_building_goth
no_pick 1
auto_apply 1
no_magic 1
is_floor 1
end

object shop_building_west
name tiles
face shop_building.x11
type 68
randomitems shop_building_west
no_pick 1
auto_apply 1
no_magic 1
is_floor 1
end

## boes added this for the marble shop 2009-11-08
object shop_building_marble
name tiles
face shop_building.x11
type 68
randomitems shop_building_marble
no_pick 1
auto_apply 1
no_magic 1
is_floor 1
end

