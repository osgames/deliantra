object building_earbook
name Listening book material
name_pl Listening book materials
slaying magic_ear
msg
Creates a book which triggers other map elements when someone says the key phrase to it or within two squares.

Place a Marking Rune on the map square before building. The rune will be removed, and the book will be linked to all other elements on the map marked with the same text.

Place a book or scroll, which will become the listening book, on top of the Marking Rune. The text should be in a special format like:

@match blah

This will cause the book to trigger when "blah" is said. You can optionally also add text for the book to say in response on a separate line below the @match.
endmsg
face buildmaterial-box:book_clasp.x11
nrof 1
type 126
subtype 3
value 70000
weight 1000
end

object building_mouthbook
name Talking book material
name_pl Talking book materials
slaying magic_mouth
msg
Creates a book which speaks when triggered by another map element.

Place a Marking Rune on the map square before building. The rune will be removed, and the book will be linked to all other elements on the map marked with the same text.

Place a book or scroll, which will become the talking book, on top of the Marking Rune. The text is the message to be spoken by the talking book.
endmsg
face buildmaterial-box:book_clasp.x11
nrof 1
type 126
subtype 3
value 45000
weight 1000
end

