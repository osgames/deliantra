object building_altar_none
name Altar material
name_pl Altar materials
slaying altar_none
face buildmaterial-box:altarnone.x11
nrof 1
type 126
subtype 3
value 2500000
weight 1500
end

object building_brazier
name Brazier material
name_pl Brazier materials
slaying brazier
face buildmaterial-sbox:brazier.x11
nrof 1
type 126
subtype 3
value 9500
weight 1500
end

object building_fireplace
name Fireplace material
name_pl Fireplace materials
slaying fireplace
face buildmaterial-box:fireplace.x11
nrof 1
type 126
subtype 3
value 10000
weight 2500
end

object building_firepot
name Firepot material
name_pl Firepot materials
slaying firepot
face buildmaterial-sbox:firepot.x11
nrof 1
type 126
subtype 3
value 7500
weight 1500
end

object building_fountain
name Fountain material
name_pl Fountain materials
slaying fountain
face buildmaterial-box:fountain.x11
nrof 1
type 126
subtype 3
value 50500
weight 25000
end

object building_hangingfirepot
name Hanging Firepot material
name_pl Hanging Firepot materials
slaying hangingfirepot
face buildmaterial-sbox:hangingfirepot.x11
nrof 1
type 126
subtype 3
value 8500
weight 1500
end

object building_sign
name Sign material
name_pl Sign materials
slaying sign
msg
Place a book or scroll, which will be destroyed, on the square before building. The text will be used as the text of the sign.
endmsg
face buildmaterial-box:sign.x11
nrof 1
type 126
subtype 3
value 25000
weight 1500
end

