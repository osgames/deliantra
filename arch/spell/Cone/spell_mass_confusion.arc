object spell_mass_confusion
inherit type_spell
name mass confusion
name_pl mass confusion
skill sorcery
msg
Creates a cone of confusion that makes your enemies act weird and
uncontrolled, making them easy to hit and making it difficult for them
to hit you. You probably died by confusion more than once by now, so you
probably know very well what this means.
endmsg
other_arch confusion
face spell_sorcery.x11
sp 20
maxsp 25
dam 0
level 24
subtype 7
attacktype 34
path_attuned 1024
value 1
invisible 1
duration 2
range 8
range_modifier 5
dam_modifier 0
no_drop 1
casting_time 10
end

