object spell_icestorm
inherit type_spell
name icestorm
name_pl icestorm
skill evocation
msg
Creates a cone of ice which freezes monsters facing the caster.

Gains power with level, so it remains useful even at high level.
endmsg
other_arch icestorm
sound misc/river_deep_fast3
face spell_evocation.x11
sp 5
maxsp 9
dam 4
level 10
subtype 7
attacktype 18
path_attuned 4
value 1
invisible 1
duration 2
range 6
range_modifier 5
dam_modifier 3
no_drop 1
casting_time 8
end

