object spell_wonder
inherit type_spell
name wonder
name_pl wonder
skill sorcery
msg
This spell invokes many effects, most of which are not very useful.
endmsg
other_arch flowers
face spell_sorcery.x11
sp 10
maxsp 0
dam 0
level 8
subtype 9
attacktype 4096
path_attuned 16384
value 1
invisible 1
randomitems wonder_spells
duration 3
range 6
range_modifier 0
dam_modifier 0
no_drop 1
casting_time 0
end

