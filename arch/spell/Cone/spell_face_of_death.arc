object spell_face_of_death
inherit type_spell
name face of death
name_pl face of death
skill praying
msg
"The death rays hit! It dies! It dies! It dies!" Yeah....

Fires a fan of pure death.

Creatures caught within this cone either die outright (regardless of how
many hit points they have) or are completely unharmed.

This spell does not harm any equipment either on the creatures or on the
floor.
endmsg
other_arch face_of_death
face spell_praying.x11
grace 80
maxgrace 35
dam 0
level 42
subtype 7
attacktype 131072
path_attuned 262144
value 1
invisible 1
duration 2
range 4
range_modifier 12
dam_modifier 0
no_drop 1
casting_time 10
end

