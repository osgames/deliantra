object spell_color_spray
inherit type_spell
name color spray
name_pl color spray
skill sorcery
msg
Color spray fires a fan of energy. This energy hits with many different
attacktypes, so creatures that are highly resistant to some will still
take damage from some of the other attacktypes of the spray. This spell
will not do as much damage compared to a spell that hits with only one
attacktype, but is useful if it is not known what attacktype a creature
can be damaged with, or if the room is full of varying creatures, with
different protections.
endmsg
other_arch color_spray
face spell_sorcery.x11
sp 35
maxsp 16
dam 20
level 30
subtype 7
attacktype 262144
path_attuned 0
value 1
invisible 1
duration 2
range 10
range_modifier 5
dam_modifier 3
no_drop 1
casting_time 9
end

