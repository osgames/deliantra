object spell_fear
inherit type_spell
name fear
name_pl fear
skill sorcery
msg
Project a cone of fear that will make your enemies panic and run away
into the targetted direction.
endmsg
other_arch fear
face spell_sorcery.x11
sp 6
maxsp 12
dam 5
level 12
subtype 7
attacktype 16386
path_attuned 1024
value 1
invisible 1
duration 2
range 4
range_modifier 3
dam_modifier 0
no_drop 1
casting_time 5
end

