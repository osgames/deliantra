object peace
name peace
other_arch peacemaker
face archangel.x18
wc -90
speed 1
type 102
subtype 7
move_type fly_low
move_on walk fly_low
no_pick 1
end

#
# the peace cone above drops these peacemakers,
# which are not really spell effects.
object peacemaker
name peacemaker
face fireball.x11
food 2
speed 1
type 59
invisible 1
move_type fly_low
no_pick 1
is_used_up 1
end

