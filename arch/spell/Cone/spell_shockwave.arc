object spell_shockwave
inherit type_spell
name shockwave
name_pl shockwave
skill evocation
msg
Creates a cone of force, hitting
creatures with magical and physical force.
Because it hits with physical force, armor
provides some protection from this attack.
endmsg
other_arch shockwave
face spell_evocation.x11
sp 26
maxsp 25
dam 10
level 22
subtype 7
attacktype 3
path_attuned 0
value 1
invisible 1
duration 2
range 18
range_modifier 5
dam_modifier 3
no_drop 1
casting_time 9
end

