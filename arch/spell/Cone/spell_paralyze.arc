object spell_paralyze
inherit type_spell
name paralyze
name_pl paralyze
skill sorcery
msg
Creates a paralyzing cone in the targetted direction.

The paralyse effect will I<not> be broken when you attack paralyzed creatures.

This is a very useful spell, at least when your enemies are affected by it
- some monsters cast it, too.

endmsg
other_arch paralyze
face spell_sorcery.x11
sp 5
maxsp 0
dam 25
level 22
subtype 7
attacktype 4098
path_attuned 1024
value 1
invisible 1
duration 2
range 5
range_modifier 3
dam_modifier 3
no_drop 1
casting_time 10
end

