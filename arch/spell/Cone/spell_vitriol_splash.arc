object spell_vitriol_splash
inherit type_spell
name vitriol splash
name_pl vitriol splash
skill praying
msg
The vitriol splash is actually not a spell but a secondary effect of the vitriol prayer.
endmsg
other_arch vitriol_splash
sound wn/poison
face spell_praying.x11
grace 15
maxgrace 10
dam 40
level 35
subtype 7
attacktype 64
path_attuned 512
value 1
invisible 1
duration 2
range 5
range_modifier 40
dam_modifier 4
no_drop 1
casting_time 10
end

