object spell_dragonbreath
inherit type_spell
name dragonbreath
name_pl dragonbreath
skill pyromancy
msg
Fire sheets of fire as a cone into the targetted direction.  Since
this fire is not magical, it even affects creatures that are immune to
magic. Target creatures still have to be vulnerable to the effects of fire
for this to do any damage, of course.
endmsg
other_arch firebreath
sound wn/fire
face spell_pyromancy.x11
sp 18
maxsp 11
dam 25
level 12
subtype 7
attacktype 4
path_attuned 2
value 1
invisible 1
duration 2
range 7
range_modifier 5
dam_modifier 3
no_drop 1
# Note this is not magical!
casting_time 5
end

