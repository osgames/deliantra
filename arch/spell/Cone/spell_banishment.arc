object spell_banishment
inherit type_spell
name banishment
name_pl banishment
skill praying
msg
This prayer will smite the enemies of your god!
It may cause weaker enemies of your god
to immediately disappear or die. It is a cone spell.
endmsg
other_arch banishment
face spell_praying.x11
grace 10
maxgrace 36
dam 50
level 25
subtype 7
attacktype 2228224
path_attuned 65536
value 1
invisible 1
duration 2
range 4
range_modifier 9
dam_modifier 0
no_drop 1
casting_time 10
end

