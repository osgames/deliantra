object icestorm
anim
icestorm.x11
icestorm.x12
icestorm.x13
mina
name icestorm
face icestorm.x11
smoothlevel 255
smoothface icestorm.x11 icestorm_S.x11 icestorm.x12 icestorm_S.x12 icestorm.x13 icestorm_S.x13
wc -30
speed 1
level 1
type 102
subtype 7
move_type fly_low
move_on walk fly_low
no_pick 1
end

