object spell_holy_word
inherit type_spell
name holy word
name_pl holy word
skill praying
msg
This prayer is a cone spell, doing damage to the enemies of your god!
On other creatures the spell has no effect, so be careful.
endmsg
other_arch holy_word
sound wn/wail-long
face spell_praying.x11
grace 4
maxgrace 10
dam 2
level 1
subtype 7
attacktype 2097152
path_attuned 65536
value 1
invisible 1
duration 1
range 2
range_modifier 4
dam_modifier 4
no_drop 1
casting_time 1
end

