object spell_mana_blast
inherit type_spell
name mana blast
name_pl mana blast
skill evocation
msg
Fires a cone of magical energy.
The energy is pure magic, which few creatures
have resistance to.
endmsg
other_arch manablast
face spell_evocation.x11
sp 10
maxsp 9
dam 4
level 21
subtype 7
attacktype 2
path_attuned 32768
value 1
invisible 1
duration 2
range 5
range_modifier 4
dam_modifier 4
no_drop 1
casting_time 7
end

