object spell_spiderweb
inherit type_spell
name spiderweb
name_pl spiderweb
skill praying
msg
Creates a cone of gooey spider silk at your enemies.
It can nearly immobilize anyone hit.

This is a special prayer of the priests of Gaea. It is granted at low level.
endmsg
other_arch spiderweb_cone
face spell_praying.x11
grace 10
maxgrace 50
dam 0
level 12
subtype 7
attacktype 0
path_attuned 2048
value 1
invisible 1
duration 2
range 10
range_modifier 5
dam_modifier 0
no_drop 1
casting_time 10
end

