object spell_counterspell
inherit type_spell
name counterspell
name_pl counterspell
skill summoning
msg
Counterspell shoots out a cone of energy that
absorbs other spell energy. This can be used
to counter incoming spells so that they do
not hit the character.
endmsg
other_arch counterspell
face spell_summoner.x11
sp 10
maxsp 18
dam 0
level 16
subtype 7
attacktype 524288
path_attuned 128
value 1
invisible 1
duration 2
range 6
range_modifier 3
dam_modifier 0
no_drop 1
casting_time 2
end

