object sparkshower
anim
sparkshower.x11
sparkshower.x12
sparkshower.x13
mina
name Shower of Sparks
face sparkshower.x11
dam 1
wc -30
speed 0.075
level 1
type 102
subtype 7
attacktype 14
glow_radius 4
move_type fly_low
move_on walk fly_low
no_pick 1
anim_speed 1.0
end

