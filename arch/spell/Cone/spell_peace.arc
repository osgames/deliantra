object spell_peace
inherit type_spell
name peace
name_pl peace
skill praying
msg
The prayer of peace, a special prayer of Gaea,
causes those monsters affected to forswear violence forever.
    
Granted at medium level.
endmsg
other_arch peace
face spell_praying.x11
grace 80
maxgrace 36
dam 0
level 40
subtype 7
attacktype 0
path_attuned 1
value 1
invisible 1
duration 2
range 4
range_modifier 9
dam_modifier 0
no_drop 1
casting_time 10
end

