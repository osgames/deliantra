object spell_slow
inherit type_spell
name slow
name_pl slow
skill sorcery
msg
Slows down the target.
endmsg
other_arch slow
face spell_sorcery.x11
sp 5
maxsp 20
dam 5
level 12
subtype 7
attacktype 2050
path_attuned 0
value 1
invisible 1
duration 2
range 5
range_modifier 4
dam_modifier 0
no_drop 1
casting_time 10
end

