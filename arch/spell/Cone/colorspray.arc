object color_spray
anim
explosion.x11
burnout.x1O
fireball.x11
ball_lightning.x11
icestorm.x11
confusion.x11
acid_pool.x11
poisoncloud.x11
slow.x11
paralyse.x11
fear.x11
mina
name color spray
face confusion.x11
wc -30
speed 1
type 102
subtype 7
move_type fly_low
move_on walk fly_low
no_pick 1
end

