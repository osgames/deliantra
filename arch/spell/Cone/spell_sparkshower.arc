object spell_spark_shower
inherit type_spell
name spark shower
name_pl spark shower
skill sorcery
msg
Creates a cone of electrical energy.
Creatures caught within the cone take magical
and electrical damage.

The damage is not very high, but the spell lasts a long time
and can be used to gradually weaken hordes of monsters while
you deal with individual creatures.

Some of the hipper mages around also use spark shower as a large area
light, as the sparks shine quite bright.
endmsg
other_arch sparkshower
face spell_sorcery.x11
sp 4
maxsp 9
dam 15
level 20
subtype 7
attacktype 10
path_attuned 8
value 1
invisible 1
duration 10
range 6
range_modifier 5
dam_modifier 3
no_drop 1
casting_time 6
end

