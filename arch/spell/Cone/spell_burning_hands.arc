object spell_burning_hands
inherit type_spell
name burning hands
name_pl burning hands
skill pyromancy
msg
Project a cone of fire in front of you.
The gains strength as the caster grows in level,
so it remains one of the best spells even at high level.
endmsg
other_arch firebreath
sound wn/fire
face spell_pyromancy.x11
sp 10
maxsp 9
dam 4
level 11
subtype 7
attacktype 6
path_attuned 2
value 1
invisible 1
duration 2
range 5
range_modifier 4
dam_modifier 4
no_drop 1
casting_time 5
end

