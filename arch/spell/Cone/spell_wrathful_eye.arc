object spell_wrathful_eye
inherit type_spell
name wrathful eye
name_pl wrathful eye
skill praying
msg
Emanates a blinding cone. Creatures
caught within the area may become blinded.
Blinded creatures have a harder time
attacking other creatures. The blinding is
not permanent, and will eventually wear off.
endmsg
other_arch wrathful_eye
face spell_praying.x11
grace 30
maxgrace 7
dam 20
level 15
subtype 7
attacktype 4194304
path_attuned 32
value 1
invisible 1
duration 2
range 5
range_modifier 5
dam_modifier 3
no_drop 1
casting_time 8
end

