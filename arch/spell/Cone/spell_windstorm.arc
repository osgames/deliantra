object spell_windstorm
inherit type_spell
name windstorm
name_pl windstorm
skill praying
msg
The windstorm prayer, a special prayer of Sorig,
strikes enemies in its cone of effect with minor wind damage,
and forces them back. It also sweeps along items which are not too heavy.

Granted at medium level.
endmsg
other_arch windstorm
face spell_praying.x11
grace 3
maxgrace 8
dam 0
level 5
subtype 7
attacktype 1
path_attuned 0
value 1
invisible 1
duration 3
range 20
range_modifier 4
dam_modifier 0
no_drop 1
casting_time 10
end

