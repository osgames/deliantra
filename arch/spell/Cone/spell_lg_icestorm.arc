object spell_large_icestorm
inherit type_spell
name large icestorm
name_pl large icestorm
skill evocation
msg
Fires a cone of freezing cold
air, damaging creatures caught inside it. It
does have the side effect of encasing objects
in blocks of ice, however, so having flint and steel
to thaw those is useful.
endmsg
other_arch icestorm
sound misc/river_deep_fast3
face spell_evocation.x11
sp 13
maxsp 11
dam 4
level 25
subtype 7
attacktype 18
path_attuned 4
value 1
invisible 1
duration 2
range 14
range_modifier 5
dam_modifier 3
no_drop 1
casting_time 9
end

