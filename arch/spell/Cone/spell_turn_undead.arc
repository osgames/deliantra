object spell_turn_undead
inherit type_spell
name turn undead
name_pl turn undead
skill praying
msg
This prayer inflict fear in the undeads so they flee. This is a cone spell.
endmsg
other_arch turn_undead
face spell_praying.x11
grace 2
maxgrace 12
dam 0
level 10
subtype 7
attacktype 8192
path_attuned 65536
value 1
invisible 1
duration 1
range 3
range_modifier 4
dam_modifier 0
no_drop 1
casting_time 5
end

