object spider_web2
name spider web
face spider_web.x11
animation spider_web
materialname cloth
move_slow walk
move_slow_penalty 7
no_pick 1
end

# below is the cone effect for the spider web.
# all it really does is drop the spider_web2 archs, which
# then hamper movement.
object spiderweb_cone
name spider web
other_arch spider_web2
face spider_web.x11
wc -90
speed 1
type 102
subtype 7
move_type fly_low
move_on walk fly_low
no_pick 1
end

