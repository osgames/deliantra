object spell_wave
inherit type_spell
name wave
name_pl wave
skill praying
msg
This prayer will cast a wave of water which will spread like a cone.
If the wave hits something it will inflict physical damage.

It can also push monsters and items away, so can be used to create some distance between
hordes of monsters and thew caster.
endmsg
other_arch wave
face spell_praying.x11
grace 8
maxgrace 8
dam 0
level 15
subtype 7
attacktype 1
path_attuned 0
value 1
invisible 1
duration 3
range 20
range_modifier 4
dam_modifier 3
no_drop 1
casting_time 10
end

