object negabolt
anim
negabolt.x11
negabolt.x11
negabolt.x21
negabolt.x31
negabolt.x41
negabolt.x51
negabolt.x61
negabolt.x71
negabolt.x81
mina
name negative energy bolt
face negabolt.x11
is_animated 0
speed 1
type 102
subtype 4
glow_radius 2
move_type fly_low
no_pick 1
is_turnable 1
reflecting 1
end

