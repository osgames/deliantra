object spell_sm_lightning
inherit type_spell
name small lightning
name_pl small lightnings
skill pyromancy
msg
This is the weakest lightning bolt, inflicting electric damage.
endmsg
other_arch lightning
face spell_pyromancy.x11
sp 6
maxsp 24
dam 8
level 3
subtype 4
attacktype 10
path_attuned 8
value 1
invisible 1
duration 8
range 12
dam_modifier 3
no_drop 1
casting_time 5
end

