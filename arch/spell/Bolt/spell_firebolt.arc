object spell_firebolt
inherit type_spell
name firebolt
name_pl firebolt
skill pyromancy
msg
Invokes a powerful, directed bolt of fire.
Avoid running in the fire after invoking this spell.
The spell hits with fire damage.
endmsg
other_arch firebolt
face spell_pyromancy.x11
sp 9
maxsp 30
dam 10
level 8
subtype 4
attacktype 6
path_attuned 2
value 1
invisible 1
duration 9
range 12
dam_modifier 3
no_drop 1
casting_time 10
end

