object spell_steambolt
inherit type_spell
name steambolt
name_pl steambolts
skill sorcery
msg
Creates a bolt of steam that does fire damage and leave behind fog,
which may conceal the caster.

This is one of the main offensive spells for sorcerers.
endmsg
other_arch steambolt
face spell_sorcery.x11
sp 10
maxsp 40
dam 12
level 11
subtype 4
attacktype 3
path_attuned 2
value 1
invisible 1
duration 8
range 12
duration_modifier 5
dam_modifier 2
no_drop 1
casting_time 10
end

