object spell_forked_lightning
inherit type_spell
name forked lightning
name_pl forked lightnings
skill praying
msg
Creates a lighting bolt that forks into smaller bolts, causing a wider area of effect.

While it is a single ray, the forking can cause it to act more like a cone.
endmsg
other_arch forked_lightning
face spell_praying.x11
dex 30
con 50
grace 15
maxgrace 24
dam 10
level 35
subtype 4
attacktype 10
path_attuned 8
value 1
invisible 1
duration 16
range 24
dam_modifier 3
no_drop 1
casting_time 10
end

