object spell_frostbolt
inherit type_spell
name frostbolt
name_pl frostbolts
skill evocation
msg
Produces a directed bolt of frost. It is useful in
corridors with mostly demons, as demons typically cannot abide cold.
endmsg
other_arch frostbolt
face spell_evocation.x11
sp 12
maxsp 48
dam 12
level 12
subtype 4
attacktype 18
path_attuned 4
value 1
invisible 1
duration 11
range 12
dam_modifier 4
no_drop 1
casting_time 10
end

