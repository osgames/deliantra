object spell_manabolt
inherit type_spell
name mana bolt
name_pl mana bolts
skill evocation
msg
Creates a bullet, or bolt, of mana doing magic damage to the target.
endmsg
other_arch manabolt
face spell_evocation.x11
sp 18
maxsp 30
dam 10
level 16
subtype 4
attacktype 2
path_attuned 32768
value 1
invisible 1
duration 9
range 12
dam_modifier 3
no_drop 1
casting_time 9
end

