object spell_cancellation
inherit type_spell
name cancellation
name_pl cancellation
skill evocation
msg
Removes magic from locations and items.
It also removes enchantments from items, so beware!
endmsg
other_arch cancellation
face spell_evocation.x11
sp 30
level 28
subtype 4
attacktype 32770
path_attuned 128
value 1
invisible 1
duration 8
range 12
no_drop 1
casting_time 10
end

