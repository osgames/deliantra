object spell_sunspear
inherit type_spell
name sunspear
name_pl sunspears
slaying undead,troll
skill praying
msg
This special prayer of Valriel will smite enemies with a bolt
of blindness and heat.

This spell is granted at medium level.
endmsg
other_arch sunspear
face spell_praying.x11
grace 8
maxgrace 30
dam 8
level 22
subtype 4
attacktype 4194308
path_attuned 524288
value 1
invisible 1
duration 8
range 6
duration_modifier 9
dam_modifier 3
no_drop 1
casting_time 8
end

