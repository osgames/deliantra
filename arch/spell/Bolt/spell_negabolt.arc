object spell_negabolt
inherit type_spell
name negative energy bolt
name_pl negative energy bolts
skill evocation
msg
Fires a negative energe bolt that hits creatures,
draining them of experience and also
hitting them with a cold attack.
endmsg
other_arch negabolt
face spell_evocation.x11
sp 13
maxsp 24
dam 8
level 30
subtype 4
attacktype 144
path_attuned 8
value 1
invisible 1
duration 16
range 24
dam_modifier 3
no_drop 1
casting_time 6
end

