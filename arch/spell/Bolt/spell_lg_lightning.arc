object spell_large_lightning
inherit type_spell
name large lightning
name_pl large lightnings
skill pyromancy
msg
Creates a bolt of lightning.

Has a longer range,
the bolt persists longer, and it does more
damage than small lightning.
endmsg
other_arch lightning
face spell_pyromancy.x11
sp 15
maxsp 24
dam 8
level 25
subtype 4
attacktype 10
path_attuned 8
value 1
invisible 1
duration 16
range 24
dam_modifier 3
no_drop 1
casting_time 7
end

