object spell_resurrection
inherit type_spell
name resurrection
name_pl resurrection
skill praying
msg
Brings back a dead character. The
character suffers less experience and
constitution loss than that caused by the raise
dead spell.

Note: This prayer is only useful on servers
which use the permanent death mode of play.
endmsg
face spell_praying.x11
con 1
grace 250
exp 10
level 60
subtype 1
path_attuned 256
value 1
invisible 1
randomitems resurrection_failure
no_drop 1
casting_time 50
end

