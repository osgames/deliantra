object spell_reincarnation
inherit type_spell
name reincarnation
name_pl reincarnation
race reincarnation_races
skill praying
msg
Brings back a dead character,
usually as a new race, although the character
could be lucky and end up as the same race
they had previously. The character will have
all the benefits and penalties of the new
race.

Note: This prayer is only useful on servers
which use the permanent death mode of play.
endmsg
face spell_praying.x11
grace 350
exp 20
level 40
subtype 1
path_attuned 256
value 1
invisible 1
randomitems reincarnation_failure
no_drop 1
casting_time 30
end

