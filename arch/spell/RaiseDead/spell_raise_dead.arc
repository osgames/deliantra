object spell_raise_dead
inherit type_spell
name raise dead
name_pl raise dead
skill praying
msg
Brings back a dead character. The
raised character loses some experience and
their constitution is diminished.

Note: This prayer is only useful on servers
which use the permanent death mode of play.
endmsg
face spell_praying.x11
con 2
grace 150
exp 5
level 80
subtype 1
path_attuned 256
value 1
invisible 1
randomitems raise_dead_failure
no_drop 1
casting_time 25
end

