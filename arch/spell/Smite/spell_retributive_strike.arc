object spell_retributive_strike
inherit type_spell
name retributive strike
name_pl retributive strike
skill praying
msg
Damages the targeted creature with
power directly from the god. Very few
creatures have any form of protection from
godly magic.

It is usually reserved for the gods themselves, but beings who can cast this
usually consider themselves demigods.
endmsg
other_arch god_power
face spell_praying.x11
grace 100
maxgrace 75
dam 55
level 50
subtype 10
attacktype 1048576
path_attuned 131072
value 1
invisible 1
duration 2
range 8
range_modifier 12
dam_modifier 1
no_drop 1
casting_time 8
end

