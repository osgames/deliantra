object spell_insect_plague
inherit type_spell
name insect plague
name_pl insect plague
skill praying
msg
Summons insects that will attack your enemies.
You get all experience from the kills.

Granted by Gaea at low level.
endmsg
other_arch insect_plague
face spell_praying.x11
grace 45
maxgrace 35
dam 20
level 16
subtype 10
attacktype 3
path_attuned 64
value 1
invisible 1
duration 2
range 8
range_modifier 12
dam_modifier 3
no_drop 1
casting_time 5
end

