object spell_holy_wrath
inherit type_spell
name holy wrath
name_pl holy wrath
skill praying
msg
This is a very powerful version of holy word.
endmsg
other_arch holy_wrath
face spell_praying.x11
grace 40
maxgrace 20
dam 15
level 30
subtype 10
attacktype 2097152
path_attuned 65536
value 1
invisible 1
duration 2
range 6
range_modifier 7
dam_modifier 2
no_drop 1
casting_time 5
end

