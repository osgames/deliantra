object spell_finger_of_death
inherit type_spell
name finger of death
name_pl finger of death
skill praying
msg
May cause targets to die instantly.

Granted by Devourers at medium level.
endmsg
other_arch face_of_death
face spell_praying.x11
grace 50
maxgrace 35
dam 24
level 25
subtype 10
attacktype 131072
path_attuned 262144
value 1
invisible 1
duration 1
range 0
dam_modifier 4
no_drop 1
casting_time 5
end

