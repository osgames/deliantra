# God power isn't cast as a spell per se, but instead is cast
# as a fumble effect.  As such, more of the values in this
# object are relevant.
object god_power
anim
manabolt.x11
manabolt.x12
manabolt.x13
manabolt.x14
manabolt.x13
manabolt.x14
mina
name godly retribution
face manabolt.x11
wc -30
speed 1
type 102
subtype 6
attacktype 1048576
duration 4
range 4
move_type fly_low
no_pick 1
end

