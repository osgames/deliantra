object spell_create_missile
inherit type_spell
name create missile
name_pl create missile
skill summoning
msg
Creates missiles matching your applied range weapon (arrows for bows,
crossbow bolts for crossbows and so on), for use against monsters.
endmsg
face spell_summoner.x11
sp 5
maxsp 5
level 1
subtype 39
path_attuned 2048
value 1
invisible 1
duration 5
duration_modifier 2
dam_modifier 6
no_drop 1
casting_time 20
end

