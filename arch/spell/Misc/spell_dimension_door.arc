object spell_dimension_door
inherit type_spell
name dimension door
name_pl dimension door
skill sorcery
msg
Dimension door allows for a short range teleportation (the exact range
depending on casting level).

The character is transported in the direction the spell is cast. Zones
where magic is blocked cannot be passed through, and the character will
not end up in the same space as a monster or anything else that blocks the
space.

If the player casts the spell directly and specifies an argument, the
spell will attempt to transport the character exactly the specified number
of spaces (no more, no less).

Otherwise, the spell will attempt to transport the character the maximum
number of spaces allowed by spell range and the environment.
endmsg
face spell_sorcery.x11
sp 25
maxsp 25
level 26
subtype 13
path_attuned 4096
value 1
invisible 1
range 25
range_modifier 5
no_drop 1
casting_time 10
end

