object spell_disarm
inherit type_spell
name disarm
name_pl disarm
skill sorcery
msg
Disarm is a safe way to disarm traps. It
will never trigger a trap, even if it fails,
but one must cast the spell, and thus use up spell
points to do so.
endmsg
face spell_sorcery.x11
sp 7
level 30
subtype 38
path_attuned 128
value 1
invisible 1
no_drop 1
casting_time 10
end

