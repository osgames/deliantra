object spell_earth_to_dust
inherit type_spell
name earth to dust
name_pl earth to dust
skill summoning
msg
Causes damage to all earth
walls and weak walls within the
immediate area of the caster.

It does not damage any other objects or monsters, and is a great way
to find secret passages, or simply help destroy earthwalls for the
notoriously weak magician.
endmsg
other_arch destruction
face spell_summoner.x11
sp 5
maxsp 3
level 11
subtype 23
path_attuned 0
value 1
invisible 1
range 2
range_modifier 3
no_drop 1
casting_time 15
end

