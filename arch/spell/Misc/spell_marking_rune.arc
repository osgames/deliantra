object spell_marking_rune
inherit type_spell
name marking rune
name_pl marking rune
skill sorcery
msg
Creates a special marking rune, which is basically a sign on the ground.

You may store any words you like in this rune, and people may apply it to
read it. It will do no harm whatsoever and is usually always visible.

This can be useful to warn others, or give yourself hints when in a maze.

Note that Elbereth is not a goddess known to these lands.
endmsg
other_arch rune_mark
face spell_sorcery.x11
sp 2
level 1
subtype 3
path_attuned 0
value 1
invisible 1
no_drop 1
casting_time 10
end

