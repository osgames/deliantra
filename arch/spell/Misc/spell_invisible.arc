object spell_invisible
inherit type_spell
name invisible
name_pl invisible
skill sorcery
msg
Makes you invisible.

Monsters will not attack or cast spells because they can't see you, but
you will start be visible again if you attack or use spells.
endmsg
other_arch enchantment
face spell_sorcery.x11
sp 15
maxsp 15
level 14
subtype 19
path_attuned 0
value 1
invisible 1
duration 300
duration_modifier 1
no_drop 1
casting_time 5
end

