object spell_consecrate
inherit type_spell
name consecrate
name_pl consecrate
skill praying
msg
Consecrate is used to convert an altar into
an altar of your god. Some altars are harder than others to consecreate,
a higher praying level helps a lot.
endmsg
face spell_praying.x11
grace 35
level 25
subtype 40
path_attuned 128
value 1
invisible 1
no_drop 1
casting_time 25
end

