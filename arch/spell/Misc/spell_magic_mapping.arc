object spell_magic_mapping
inherit type_spell
name magic mapping
name_pl magic mapping
skill sorcery
msg
This spell shows an abstract map of the area where the players character is, indicating
walls, monsters and sometimes items.

Very useful in new dungeons, or mazes.
endmsg
other_arch enchantment
face spell_sorcery.x11
sp 15
level 18
subtype 14
path_attuned 8192
value 1
invisible 1
no_drop 1
casting_time 1
end

