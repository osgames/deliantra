object spell_word_of_recall
inherit type_spell
name word of recall
name_pl word of recall
skill praying
msg
Transports the character back to the last place where he or
she used a B<Bed to Reality>.

There is some delay between invoking the spell and it taking effect, so this
spell is not a great choice to cast if you are about to die, as you will
likely die before the spell actually transports you back home.

It can be most handy when in a deep dungeon
and don't want to walk back up all the stairs.
endmsg
sound ss/spiritvoice11
face spell_praying.x11
grace 40
maxgrace 20
level 22
subtype 18
path_attuned 4096
value 1
invisible 1
duration 40
duration_modifier 5
no_drop 1
casting_time 15
end

