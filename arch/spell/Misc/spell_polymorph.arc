object spell_polymorph
inherit type_spell
name polymorph
name_pl polymorph
skill none
msg
Fires a polymorph bolt that transforms items and creatures in other items or creatures.

Careful, you can lose items or transform peaceful creatures in deadly monsters!
endmsg
other_arch polymorph
sp 20
level 6
subtype 29
path_attuned 16384
value 1
invisible 1
range 15
range_modifier 5
no_drop 1
casting_time 30
end

