object spell_transference
inherit type_spell
name transference
name_pl transference
skill sorcery
msg
Transfers mana to the target.

If you use this spell on a normal creature, they will perhaps be
overloaded from the magical energy and explode in a blast of fire.

Can also be used to reload mana in another player.

endmsg
face spell_sorcery.x11
sp 10
maxsp 4
dam 8
level 12
subtype 37
path_attuned 32768
value 1
invisible 1
dam_modifier 2
no_drop 1
casting_time 20
end

