object spell_remove_curse
inherit type_spell
name remove curse
name_pl remove curse
skill praying
msg
Eliminates the cursed status of some of the items in your inventory, as
long as their level is below the level of the spell.

There is no way to know which items will be uncursed, except that the
marked item will by uncursed first.

The number of objects uncursed increases as caster level increases.
endmsg
face spell_praying.x11
grace 80
maxgrace 30
level 40
subtype 31
path_attuned 256
value 1
invisible 1
no_drop 1
last_sp 0
casting_time 20
end

