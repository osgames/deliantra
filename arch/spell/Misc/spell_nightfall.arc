object spell_nightfall
inherit type_spell
name nightfall
name_pl nightfall
skill praying
msg
This prayer will bring about a general darkness in the whole area you are in!

Granted by Gaea and Devourers at medium level.
endmsg
face spell_praying.x11
grace 120
dam 1
level 16
subtype 43
path_attuned 524288
value 1
invisible 1
no_drop 1
casting_time 15
end

