object spell_create_food
inherit type_spell
name create food
name_pl create food
skill summoning
msg
If this prayer is successful, your god will grant you some food.
However, your god will take away this gift if you drop it.

This spell accepts an argument that let's you create any food
that your current casting level is able to create.
endmsg
face spell_summoner.x11
sp 10
maxsp 10
food 100
level 6
subtype 22
path_attuned 2048
value 1
invisible 1
duration_modifier 1
no_drop 1
casting_time 20
end

