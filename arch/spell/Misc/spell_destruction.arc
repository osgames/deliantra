object spell_destruction
inherit type_spell
name destruction
name_pl destruction
skill sorcery
msg
Damages all creatures in the area of effect.
The size of the area depends on the spell strength.

Unlike most spells, destruction will work through walls and doors.
Thus, the caster need not see all the targets for this
spell to work. The damage is all magical in nature.
endmsg
other_arch destruction
face spell_sorcery.x11
sp 15
maxsp 20
dam 90
level 18
subtype 16
attacktype 2
path_attuned 0
value 1
invisible 1
range 5
range_modifier 10
dam_modifier 1
no_drop 1
casting_time 10
end

