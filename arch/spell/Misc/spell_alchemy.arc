object spell_alchemy
inherit type_spell
name alchemy
name_pl alchemy
skill alchemy
msg
Transforms surrounding objects into gold nuggets.
That's what it does, yes, certainly, what else could it do?
endmsg
face spell_alchemy.x11
sp 5
level 1
subtype 30
path_attuned 16384
value 1
invisible 1
duration 100
duration_modifier 5
no_drop 1
casting_time 25
end

