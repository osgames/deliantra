object spell_probe
inherit type_spell
name probe
name_pl probe
skill sorcery
msg
Gives you some information about the target, such as the shape it is in,
and how good your chances to beat it are.
endmsg
face spell_sorcery.x11
sp 3
level 10
subtype 20
path_attuned 8192
value 1
invisible 1
range 25
range_modifier 5
no_drop 1
casting_time 2
end

