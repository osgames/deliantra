object spell_improved_invisibility
inherit type_spell
name improved invisibility
name_pl improved invisibility
skill sorcery
msg
An imporoved version of invisible:
You will stay invisible even if you attack or use spells!!

Note that you still make noises, and most monsters
will still feel where they hit and try to hit back.
endmsg
other_arch enchantment
face spell_sorcery.x11
sp 25
maxsp 10
level 40
subtype 19
path_attuned 0
value 1
invisible 1
duration 200
duration_modifier 1
no_drop 1
make_invisible 1
casting_time 10
end

