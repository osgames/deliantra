object spell_light
inherit type_spell
name light
name_pl light
skill praying
msg
This prayer will light a fixed area.
endmsg
other_arch light
face spell_praying.x11
grace 4
maxgrace 10
dam 2
level 6
subtype 42
attacktype 4194306
path_attuned 524288
value 1
invisible 1
duration 1000
range_modifier 10
duration_modifier 1
dam_modifier 20
no_drop 1
casting_time 5
end

