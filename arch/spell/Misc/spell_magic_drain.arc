object spell_magic_drain
inherit type_spell
name magic drain
name_pl magic drain
skill sorcery
msg
Drains mana from an enemies and adds it to your mana pool.
endmsg
face spell_sorcery.x11
sp 20
maxsp 10
dam -75
level 12
subtype 37
path_attuned 32768
value 1
invisible 1
dam_modifier 1
no_drop 1
casting_time 1
end

