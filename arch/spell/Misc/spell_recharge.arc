object spell_charging
inherit type_spell
name charging
name_pl charging
skill none
msg
Charges the marked staff or wand.
endmsg
sp 200
dam 40
level 10
subtype 28
path_attuned 32768
value 1
invisible 1
dam_modifier 5
no_drop 1
casting_time 75
end

