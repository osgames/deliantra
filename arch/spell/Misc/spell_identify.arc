object spell_identify
inherit type_spell
name identify
name_pl identify
skill sorcery
msg
Fully identifies some number of otherwise unknown objects in the
characters inventory (or on the floor where the character is standing).

There is no way to know which items will be identified, except that the
marked item will by identified first.

The number of objects identified increases as caster level increases.
endmsg
other_arch enchantment
face spell_sorcery.x11
sp 20
dam 3
level 15
subtype 32
path_attuned 8192
value 1
invisible 1
dam_modifier 5
no_drop 1
casting_time 20
end

