object spell_invisible_to_undead
inherit type_spell
name invisible to undead
name_pl invisible to undead
race undead
skill praying
msg
This prayer works like invisible spell, except living creatures can see you.
endmsg
other_arch enchantment
face spell_praying.x11
grace 25
maxgrace 15
level 14
subtype 19
path_attuned 0
value 1
invisible 1
duration 300
duration_modifier 1
no_drop 1
casting_time 5
end

