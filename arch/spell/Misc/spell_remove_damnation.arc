object spell_remove_damnation
inherit type_spell
name remove damnation
name_pl remove damnation
skill praying
msg
Eliminates the damned status of some of the items in your inventory, as
long as their level is below the level of the spell.

There is no way to know which items will be undamned, except that the
marked item will be tried first.

The number of objects undamned increases as caster level increases.
endmsg
face spell_praying.x11
grace 150
maxgrace 30
level 55
subtype 31
path_attuned 256
value 1
invisible 1
no_drop 1
last_sp 1
casting_time 25
end

