object spell_perceive_self
inherit type_spell
name perceive self
name_pl perceive self
skill praying
msg
Shows you I<most> of your character attributes.

A message like 'your dexterity is depleted by 2' will show that you have
lost two dexterity points.  You lost it because you die or an undead
creature has touched you. You need a Potion of Life to restore them.

Will also reveal the current elemental focus for Dragon PCs.
endmsg
face spell_praying.x11
grace 5
level 2
subtype 17
path_attuned 8192
value 1
invisible 1
no_drop 1
casting_time 1
end

