object spell_faery_fire
inherit type_spell
name faery fire
name_pl faery fire
skill pyromancy
msg
All creatures near the caster start to glow,
indicating where they are and making them easier to hit.

Unlike most spells, this spell works through doors and walls.
endmsg
other_arch detect_magic
sound ss/sonar_ping
face spell_pyromancy.x11
sp 5
maxsp 15
level 5
subtype 44
path_attuned 524288
value 1
invisible 1
duration 5
range 5
range_modifier 6
duration_modifier 5
no_drop 1
casting_time 15
end

