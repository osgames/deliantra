object spell_meteor_swarm
inherit type_spell
name meteor swarm
name_pl meteor swarm
skill pyromancy
msg
Fires several comets, causing a swath of fire damage.
endmsg
other_arch spell_comet
face spell_pyromancy.x11
sp 60
maxsp 12
level 60
subtype 36
path_attuned 16
value 1
invisible 1
duration 3
duration_modifier 4
no_drop 1
casting_time 12
end

