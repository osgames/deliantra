object spell_bullet_swarm
inherit type_spell
name bullet swarm
name_pl bullet swarm
skill evocation
msg
Sends multiple bullets in the targetted direction,
hurting anything that gets hit by the bullets.
endmsg
other_arch spell_small_bullet
face spell_evocation.x11
sp 6
maxsp 12
level 16
subtype 36
path_attuned 16
value 1
invisible 1
duration 3
duration_modifier 4
no_drop 1
casting_time 5
end

