object spell_missile_swarm
inherit type_spell
name missile swarm
name_pl missile swarm
skill sorcery
msg
Fires many magic missiles.

They can actually turn to reach the target, but the turning is weak, even stupid:
at times these missiles will fly right into a wall.
endmsg
other_arch spell_magic_missile
face spell_sorcery.x11
sp 6
maxsp 12
level 17
subtype 36
path_attuned 16
value 1
invisible 1
duration 3
duration_modifier 4
no_drop 1
casting_time 3
end

