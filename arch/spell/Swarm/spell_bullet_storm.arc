object spell_bullet_storm
inherit type_spell
name bullet storm
name_pl bullet storm
skill evocation
msg
Fires multiple bullets in the targetted direction,
hurting anything that gets hit by the bullets.
endmsg
other_arch spell_lg_magic_bullet
face spell_evocation.x11
sp 8
maxsp 12
level 24
subtype 36
path_attuned 16
value 1
invisible 1
duration 3
duration_modifier 4
no_drop 1
casting_time 5
end

