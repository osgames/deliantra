object spell_cause_many_wounds
inherit type_spell
name cause many wounds
name_pl cause many wounds
skill praying
msg
Shoots a bunch of grace balls in the casting direction
which will cause light wounds in anything they hit.
endmsg
other_arch spell_cause_light_wounds
face spell_praying.x11
maxsp 12
grace 30
level 23
subtype 36
path_attuned 131072
value 1
invisible 1
duration 3
duration_modifier 4
no_drop 1
casting_time 5
end

