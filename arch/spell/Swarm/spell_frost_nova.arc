object spell_frost_nova
inherit type_spell
name frost nova
name_pl frost nova
skill evocation
msg
Fire a swarm of asteroids which will explode in a
big icestorm.
endmsg
other_arch spell_asteroid
face spell_evocation.x11
sp 30
maxsp 12
level 60
subtype 36
path_attuned 16
value 1
invisible 1
duration 3
duration_modifier 4
no_drop 1
casting_time 12
end

