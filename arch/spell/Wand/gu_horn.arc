object gu_horn
anim
gu_horn.x11
gu_horn.x12
gu_horn.x13
gu_horn.x14
gu_horn.x15
mina
inherit type_horn
name Golden Unicorn Horn
name_pl Golden Unicorn Horns
skill use magic item
msg
A beautiful unicorn horn shining with golden
light. You feel a sence of peace when you
hold it.

This is one of the most powerful healing
artifact in the realm. It is rumored to have
been created by a god of healing. Hanuk
imprisoned the god by trickery and acquired
the artifact.
endmsg
other_arch spell_restoration
face gu_horn.x14
hp 100
maxhp 130
speed 0.3
nrof 1
level 1
materialname iron
value 10000000
weight 5000
client_type 721
body_range -1
end

