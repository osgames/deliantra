object rod_light
anim
rod_light.x11
rod_light.x12
rod_light.x13
mina
inherit type_rod
name rod
name_pl rods
skill use magic item
face rod_light.x11
hp 5
maxhp 5
speed -0.25
materialname stone
value 350
weight 3500
randomitems rod_spell
client_type 702
body_range -1
end

