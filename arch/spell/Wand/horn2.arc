object horn2
inherit type_horn
name horn
name_pl horns
skill use magic item
face horn2.x11
speed 0.1
nrof 1
level 1
materialname iron
value 5900
weight 4000
client_type 721
body_range -1
end

