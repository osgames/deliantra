object rod_heavy
anim
rod_heavy.x11
rod_heavy.x12
rod_heavy.x13
mina
inherit type_rod
name heavy rod
name_pl heavy rods
skill use magic item
face rod_heavy.x11
hp 20
maxhp 20
speed -0.25
materialname stone
value 700
weight 7000
randomitems rod_spell
client_type 701
body_range -1
end

