object shellhorn2
inherit type_horn
name shell horn
name_pl shell horns
skill use magic item
face shellhorn2.x11
speed 0.1
nrof 1
level 1
materialname iron
value 5900
weight 4000
client_type 721
body_range -1
end

