object spell_curse
inherit type_spell
name curse
name_pl curse
skill praying
msg
Your god will cast a curse on your target.
The curse will alter some of its stats in a negative way.
endmsg
face spell_praying.x11
grace 8
maxgrace 30
wc -1
ac -1
level 4
subtype 26
resist_godpower 35
path_attuned 128
value 1
invisible 1
duration 500
range 30
duration_modifier 4
no_drop 1
casting_time 5
end

