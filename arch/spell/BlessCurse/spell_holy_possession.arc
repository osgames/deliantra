object spell_holy_possession
inherit type_spell
name holy possession
name_pl holy possession
race holy_blessing
skill praying
msg
This is a stronger form of blessing of your god, which alters
some of your stats in a positive way.
endmsg
face spell_praying.x11
grace 30
maxgrace 30
wc 2
ac 2
level 15
subtype 25
attacktype 1
resist_godpower 95
path_attuned 128
value 1
invisible 1
last_grace 1
duration 500
duration_modifier 4
no_drop 1
casting_time 10
end

