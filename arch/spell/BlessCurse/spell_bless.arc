object spell_bless
inherit type_spell
name bless
name_pl bless
race holy_blessing
skill praying
msg
Your god will bless you or your target.
The blessing will temporarily alter some of your stats in a positive way.
endmsg
face spell_praying.x11
grace 8
maxgrace 30
wc 1
ac 1
level 5
subtype 25
resist_godpower 35
path_attuned 128
value 1
invisible 1
duration 500
duration_modifier 4
no_drop 1
casting_time 5
end

