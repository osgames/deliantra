# this arch is set up so that it can be inserted into a map with
# no changes, and it will explode into a fireball
object exploding_fireball
name exploding fireball
other_arch fireball
hp 15
maxhp 10
speed 1.0
speed_left 1.0
type 102
subtype 5
range 1
dam_modifier 10
end

