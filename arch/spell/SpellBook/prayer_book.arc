object cleric_book
name prayerbook
name_pl prayerbooks
skill literacy
face prayer_book.x11
nrof 1
type 85
materialname paper
value 400
weight 5000
randomitems prayer_book
client_type 1002
end

object cleric_book_l1
name prayerbook
name_pl prayerbooks
skill literacy
face prayer_book.x11
nrof 1
type 85
materialname paper
value 100
weight 5000
randomitems praying_l1
client_type 1002
end

