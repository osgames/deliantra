object evoker_book
name evoker's spellbook
name_pl evoker's spellbooks
skill literacy
face evoker_book.x11
nrof 1
type 85
materialname paper
value 500
weight 5000
randomitems evocation_book
client_type 1001
end

object evoker_book_l1
name evoker's spellbook
name_pl evoker's spellbooks
skill literacy
face evoker_book.x11
nrof 1
type 85
materialname paper
value 100
weight 5000
randomitems evocation_l1
client_type 1001
end

