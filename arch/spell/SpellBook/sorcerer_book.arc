object sorcerer_book
name sorcerer's spellbook
name_pl sorcerer's spellbooks
skill literacy
face sorcerer_book.x11
nrof 1
type 85
materialname paper
value 600
weight 5000
randomitems sorcery_book
client_type 1001
end

#
# Starting spellbooks for sorcers.
#
object sorcerer_book_l1
name sorcerer's spellbook
name_pl sorcerer's spellbooks
skill literacy
face sorcerer_book.x11
nrof 1
type 85
materialname paper
value 200
weight 5000
randomitems sorcery_l1
client_type 1001
end

