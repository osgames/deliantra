object summoner_book
name summoner's spellbook
name_pl summoner's spellbooks
skill literacy
face summoner_book.x11
nrof 1
type 85
materialname paper
value 500
weight 5000
randomitems summoner_book
client_type 1001
end

object summoner_book_l1
name summoners's spellbook
name_pl summoners's spellbooks
skill literacy
face summoner_book.x11
nrof 1
type 85
materialname paper
value 100
weight 5000
randomitems summoning_l1
client_type 1001
end

