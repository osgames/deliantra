object pyro_book
name pyromancer's spellbook
name_pl pyromancer's spellbooks
skill literacy
face pyro_book.x11
nrof 1
type 85
materialname paper
value 500
weight 5000
randomitems pyromancy_book
client_type 1001
end

object pyro_book_l1
name pyromancer's spellbook
name_pl pyromancer's spellbooks
skill literacy
face pyro_book.x11
nrof 1
type 85
materialname paper
value 100
weight 5000
randomitems pyromancy_l1
client_type 1001
end

