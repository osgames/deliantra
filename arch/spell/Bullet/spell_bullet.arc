object spell_magic_bullet
inherit type_spell
name magic bullet
name_pl magic bullet
skill sorcery
msg
A magic bullet causes damage in anything it hits.
It is fired in the targetted direction.
endmsg
other_arch bullet
sound wn/magic-missile-1-miss
face spell_sorcery.x11
sp 1
maxsp 6
dam 10
level 1
subtype 5
attacktype 2
path_attuned 16
value 1
invisible 1
dam_modifier 1
no_drop 1
casting_time 2
end

