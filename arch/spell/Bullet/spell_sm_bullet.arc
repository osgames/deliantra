# Just an evoker version of magic bullet, as a low
# mana costing sp to kill things with.
object spell_small_bullet
inherit type_spell
name small bullet
name_pl small bullet
skill evocation
msg
Creates a magic bullet that causes damage in anything it hits.
It is fired at the targetted direction.

Magic bullets are not the magic bullet to solve all of your problems,
but especially at higher levels it surely does it's job nicely and
efficiently.
endmsg
other_arch bullet
sound wn/magic-missile-1-miss
face spell_evocation.x11
sp 1
maxsp 6
dam 10
level 1
subtype 5
attacktype 2
path_attuned 16
value 1
invisible 1
dam_modifier 1
no_drop 1
casting_time 2
end

