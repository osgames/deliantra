object spell_lg_magic_bullet
inherit type_spell
name large bullet
name_pl large bullet
skill evocation
msg
Like magic bullet, except that it does much more damage.
endmsg
other_arch lbullet
sound wn/magic-missile-1-miss
face spell_evocation.x11
sp 3
maxsp 25
dam 25
level 20
subtype 5
attacktype 2
path_attuned 16
value 1
invisible 1
dam_modifier 1
no_drop 1
casting_time 6
end

