object spell_large_fireball
inherit type_spell
name large fireball
name_pl large fireball
skill pyromancy
msg
Fires a small bullet of fire, that
explodes into a large area of fire when it
hits something.
endmsg
other_arch firebullet
sound wn/crossbow-fire-miss
face spell_pyromancy.x11
sp 16
maxsp 32
food 4
dam 8
level 25
subtype 5
attacktype 6
path_attuned 2
value 1
invisible 1
duration 4
range 8
dam_modifier 4
no_drop 1
casting_time 8
end

