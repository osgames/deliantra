# vitriol is the 'bullet' form of vitriol.
object vitriol
name vitriol
other_arch vitriol_splash
face v_splash.x11
animation vitriol_splash
speed 1
type 102
subtype 5
attacktype 64
glow_radius 2
move_type fly_low
move_on walk fly_low
no_pick 1
is_turnable 0
end

#
# vitriol pool is what the splash above drops.
# Note that this is put unchanged onto the map.
object vitriol_pool
anim
v_pool.x11
v_pool.x12
mina
name vitriol pool
face v_pool.x11
dam 15
wc -30
speed 0.2
level 1
type 102
subtype 7
attacktype 64
duration 30
move_on walk
no_pick 1
stand_still 1
end

#
# vitriol splash is the cone form the bullet turns into
#
object vitriol_splash
anim
v_splash.x11
v_splash.x12
mina
name vitriol splash
other_arch vitriol_pool
face v_splash.x11
wc -30
speed 1
type 102
subtype 7
glow_radius 2
move_type fly_low
move_on walk fly_low
no_pick 1
is_turnable 1
end

