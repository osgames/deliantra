object spell_poison_cloud
inherit type_spell
name poison cloud
name_pl poison cloud
skill sorcery
msg
Produces a ball which flies forward and explodes into
a poisonous gas cloud. Monsters within it are slowed, weakened, and possibly killed.
endmsg
other_arch poisonbullet
sound wn/bow-puny-miss
face spell_sorcery.x11
sp 5
maxsp 8
food 5
dam 0
level 10
subtype 5
attacktype 1026
path_attuned 16
value 1
invisible 1
duration 25
range 4
dam_modifier 2
no_drop 1
casting_time 10
end

