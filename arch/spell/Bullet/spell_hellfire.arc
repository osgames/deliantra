object spell_hellfire
inherit type_spell
name hellfire
name_pl hellfire
skill pyromancy
msg
Fires a, well, small bullet of fire at the targetted direction, which
then explodes into a large area of fire. The main difference compared to
the traditional fireball spell is that this spell is pure fire, with no
magical elements, which enables it to damage creatures that are immune to
magic attacks, but not to fire.
endmsg
other_arch firebullet
sound wn/crossbow-fire-miss
face spell_pyromancy.x11
sp 18
maxsp 24
food 10
dam 8
level 35
subtype 5
attacktype 4
path_attuned 2
value 1
invisible 1
duration 4
range 6
dam_modifier 4
no_drop 1
casting_time 8
end

