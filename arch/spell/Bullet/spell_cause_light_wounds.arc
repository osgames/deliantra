object spell_cause_light_wounds
inherit type_spell
name cause light wounds
name_pl cause light wounds
skill praying
msg
Cause light wounds shoots a ball of grace which will
cause light wounds in anything it hits.
endmsg
other_arch cause_wounds
sound wn/bow-puny-miss
face spell_praying.x11
grace 4
maxgrace 6
dam 6
level 1
subtype 5
attacktype 1048576
path_attuned 131072
value 1
invisible 1
dam_modifier 1
no_drop 1
casting_time 5
end

