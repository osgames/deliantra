object spell_cause_medium_wounds
inherit type_spell
name cause medium wounds
name_pl cause medium wounds
skill praying
msg
Cause medium wounds shoots a ball of grace which will
cause medium wounds in anything it hits.
endmsg
other_arch cause_wounds
sound wn/bow-puny-miss
face spell_praying.x11
grace 8
maxgrace 24
dam 24
level 10
subtype 5
attacktype 1048576
path_attuned 131072
value 1
invisible 1
dam_modifier 1
no_drop 1
casting_time 5
end

