object spell_asteroid
inherit type_spell
name asteroid
name_pl asteroid
skill evocation
msg
An asteroid is a large ball of ice, which is shot in the cast
direction and will explode into a big icestorm once it hits something.
endmsg
other_arch asteroid
sound wn/bow-puny-miss
face spell_evocation.x11
sp 15
maxsp 18
food 10
dam 75
level 30
subtype 5
attacktype 256
path_attuned 16
value 1
invisible 1
duration 10
range 12
duration_modifier 4
dam_modifier 1
no_drop 1
casting_time 10
end

