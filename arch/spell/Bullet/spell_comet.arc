object spell_comet
inherit type_spell
name comet
name_pl comet
skill pyromancy
msg
Fires a comet, a large ball of fire,
in the tragetted direction. This comet inflicts serious
physical harm on whatever it hits, and then
explodes into a ball of fire.
endmsg
other_arch meteor
sound wn/crossbow-fire-miss
face spell_pyromancy.x11
sp 30
maxsp 18
food 10
dam 75
level 40
subtype 5
attacktype 256
path_attuned 16
value 1
invisible 1
duration 10
range 12
duration_modifier 4
dam_modifier 1
no_drop 1
casting_time 10
end

