object fireball
anim
fireball.x11
fireball.x12
fireball.x13
mina
sound wn/fire
face fireball.x11
smoothlevel 250
smoothface fireball.x11 fireball_S.x11 fireball.x12 fireball_S.x12 fireball.x13 fireball_S.x13
wc -30
speed 0.2
speed_left -0.21
type 102
subtype 6
attacktype 4
glow_radius 1
move_type fly_low
no_pick 1
end

