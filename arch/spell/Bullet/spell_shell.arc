# this is used by cannons to fire spells.
# as such, most of the values aren't that meaningful compared
# to spells players casts.
object spell_shell
inherit type_spell
name cannon shell
name_pl cannon shell
msg
*TODO* please report
endmsg
other_arch shell
sp 10
maxsp 0
food 4
dam 7
level 1
subtype 5
attacktype 1
path_attuned 2
value 1
invisible 1
duration 4
range 8
dam_modifier 0
no_drop 1
casting_time 15
end

