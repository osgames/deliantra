object spell_large_manaball
inherit type_spell
name large manaball
name_pl large manaball
skill evocation
msg
Fires a small bullet of magical
energy, that then explodes when it hits
something into a burst of magical energy.
The great advantage of using pure magical
energy is that few creatures are resistant to
it.
endmsg
other_arch manabullet
sound wn/bow-puny-miss
face spell_evocation.x11
sp 32
maxsp 32
food 15
dam 8
level 36
subtype 5
attacktype 2
path_attuned 32768
value 1
invisible 1
duration 4
range 8
dam_modifier 4
no_drop 1
casting_time 9
end

