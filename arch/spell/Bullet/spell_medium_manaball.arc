object spell_medium_manaball
inherit type_spell
name medium manaball
name_pl medium manaball
skill evocation
msg
Fires a small bullet of magical
energy, that then explodes when it hits
something into a burst of magical energy.
The great advantage of using pure magical
energy is that few creatures are resistant to
it.
endmsg
other_arch manabullet
sound wn/bow-puny-miss
face spell_evocation.x11
sp 6
maxsp 24
food 15
dam 8
level 16
subtype 5
attacktype 2
path_attuned 32768
value 1
invisible 1
duration 4
range 6
dam_modifier 3
no_drop 1
casting_time 18
end

