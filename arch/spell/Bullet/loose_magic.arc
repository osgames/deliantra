# this is only used for a spell failure, so more values are
# needed in this object.
object loose_magic
anim
manabolt.x11
manabolt.x12
manabolt.x13
manabolt.x14
mina
name uncontrolled mana
face manabolt.x11
wc -30
speed 1
type 102
subtype 6
attacktype 2
duration 3
range 3
move_type fly_low
no_pick 1
end

