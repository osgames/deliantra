object holy_orb
name holy orb
slaying undead,demon
other_arch holy_effect
sound_destroy wn/wail-long
face holy_orb.x11
is_animated 0
speed 1
type 102
subtype 5
attacktype 2097152
move_type fly_low
move_on walk fly_low
no_pick 1
end

