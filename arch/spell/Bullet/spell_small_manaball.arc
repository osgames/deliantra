object spell_small_manaball
inherit type_spell
name small manaball
name_pl small manaball
skill evocation
msg
Similar to a small fireball, but the mana explosion does magic damage.
endmsg
other_arch manabullet
sound wn/bow-puny-miss
face spell_evocation.x11
sp 12
maxsp 24
food 15
dam 8
level 5
subtype 5
attacktype 2
path_attuned 32768
value 1
invisible 1
duration 4
range 4
dam_modifier 3
no_drop 1
casting_time 9
end

