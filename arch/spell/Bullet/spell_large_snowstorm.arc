object spell_large_snowstorm
inherit type_spell
name large snowstorm
name_pl large snowstorm
skill evocation
msg
Fires a small snowball that
explodes into a ice and cold, freezing
anything within it. The small snowball
explodes when it hits something.
endmsg
other_arch snowball
sound wn/bow-puny-miss
face spell_evocation.x11
sp 16
maxsp 32
food 8
dam 8
level 25
subtype 5
attacktype 18
path_attuned 4
value 1
invisible 1
duration 4
range 8
dam_modifier 4
no_drop 1
casting_time 6
end

