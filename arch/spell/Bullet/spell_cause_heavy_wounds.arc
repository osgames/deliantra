object spell_cause_heavy_wounds
inherit type_spell
name cause heavy wounds
name_pl cause heavy wounds
skill praying
msg
Cause heavy wounds shoots a ball of grace which will
cause heavy wounds in anything it hits.
endmsg
other_arch cause_wounds
sound wn/bow-puny-miss
face spell_praying.x11
grace 16
maxgrace 50
dam 50
level 20
subtype 5
attacktype 1048576
path_attuned 131072
value 1
invisible 1
dam_modifier 1
no_drop 1
casting_time 5
end

