object spell_small_snowstorm
inherit type_spell
name small snowstorm
name_pl small snowstorm
skill evocation
msg
Shoots a ball of ice which explodes when it hits a target. Useful versus fire-creatures!
endmsg
other_arch snowball
sound wn/bow-puny-miss
face spell_evocation.x11
sp 6
maxsp 24
food 4
dam 8
level 3
subtype 5
attacktype 18
path_attuned 4
value 1
invisible 1
duration 6
range 4
dam_modifier 6
no_drop 1
casting_time 5
end

