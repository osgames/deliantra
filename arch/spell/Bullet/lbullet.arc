object lbullet
anim
lbullet.x11
lbullet.x11
lbullet.x21
lbullet.x31
lbullet.x41
lbullet.x51
lbullet.x61
lbullet.x71
lbullet.x81
mina
name large bullet
face lbullet.x11
is_animated 0
dam 25
wc -10
speed 1
type 102
subtype 5
attacktype 2
move_type fly_low
move_on walk fly_low
no_pick 1
is_turnable 1
end

