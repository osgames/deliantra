object spell_medium_fireball
inherit type_spell
name medium fireball
name_pl medium fireball
skill pyromancy
msg
Creates a ball of fire which explodes when it hits a target.
Be prepared to run from the flames!
endmsg
other_arch firebullet
sound wn/crossbow-fire-miss
face spell_pyromancy.x11
sp 10
maxsp 24
food 4
dam 8
level 10
subtype 5
attacktype 6
path_attuned 2
value 1
invisible 1
duration 4
range 6
dam_modifier 3
no_drop 1
casting_time 10
end

