object poisoncloud
anim
poisoncloud.x11
poisoncloud.x12
poisoncloud.x13
mina
name poison cloud
other_arch poisoning
sound wn/poison
face poisoncloud.x11
hp 10
dam 1
speed 0.2
type 102
subtype 6
attacktype 1026
move_type fly_low
no_pick 1
end

