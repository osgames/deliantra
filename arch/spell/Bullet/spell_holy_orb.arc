object spell_holy_orb
inherit type_spell
name holy orb
name_pl holy orb
skill praying
msg
This prayer missile explodes when it hits a target and invokes a powerful holy word
cloud. The damage type depends on your god.

Be careful, you can be damaged by the spell too in some cases! This is a ball spell.
endmsg
other_arch holy_orb
sound wn/magic-missile-1-miss
face spell_praying.x11
maxhp 7
grace 10
maxgrace 25
food 18
dam 30
level 25
subtype 5
attacktype 2097152
path_attuned 65536
value 1
invisible 1
duration 4
range 3
duration_modifier 19
dam_modifier 2
no_drop 1
casting_time 5
end

