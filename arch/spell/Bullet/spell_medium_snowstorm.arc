object spell_medium_snowstorm
inherit type_spell
name medium snowstorm
name_pl medium snowstorm
skill evocation
msg
Fires a ball of ice which explodes when it hits a target. Be ready to run away!
endmsg
other_arch snowball
sound wn/bow-puny-miss
face spell_evocation.x11
sp 10
maxsp 24
food 7
dam 8
level 15
subtype 5
attacktype 18
path_attuned 4
value 1
invisible 1
duration 4
range 6
dam_modifier 3
no_drop 1
casting_time 5
end

