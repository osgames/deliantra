object spell_small_fireball
inherit type_spell
name small fireball
name_pl small fireball
skill pyromancy
msg
Shoots a ball of fire which explodes when it hits a target.
Perfect for roasting trolls!
endmsg
other_arch firebullet
sound wn/crossbow-fire-miss
face spell_pyromancy.x11
sp 6
maxsp 24
food 4
dam 8
level 1
subtype 5
attacktype 6
path_attuned 2
value 1
invisible 1
duration 4
range 4
dam_modifier 3
no_drop 1
casting_time 5
end

