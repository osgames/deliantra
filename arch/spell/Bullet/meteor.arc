object fire_trail
anim
burnout.x11
burnout.x12
burnout.x13
burnout.x14
burnout.x15
burnout.x16
burnout.x17
burnout.x18
burnout.x19
mina
name burningtrail
face burnout.x11
food 9
speed 1
no_pick 1
is_used_up 1
end

object meteor
other_arch fireball
face meteor.x11
is_animated 0
speed 1
type 102
subtype 5
move_type fly_low
move_on walk fly_low
no_pick 1
end

