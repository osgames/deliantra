object spell_vitriol
inherit type_spell
name vitriol
name_pl vitriol
skill praying
msg
The vitriol prayer is a special prayer of Gorokh. This is a very powerful
prayer since few monsters are immune to acid. The ball of vitriol (acid)
strikes the monster for great damage, then splashes into a puddle which
sears the area around the victim.

Granted at medium level.
endmsg
other_arch vitriol
sound wn/poison
face spell_praying.x11
grace 15
maxgrace 6
food 20
dam 100
level 25
subtype 5
attacktype 64
path_attuned 512
value 1
invisible 1
duration 4
range 10
duration_modifier 4
dam_modifier 25
no_drop 1
casting_time 10
end

