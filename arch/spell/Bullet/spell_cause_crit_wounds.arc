object spell_cause_critical_wounds
inherit type_spell
name cause critical wounds
name_pl cause critical wounds
skill praying
msg
Cause critical wounds shoots a ball of grace which will
cause critical wounds in anything it hits.
endmsg
other_arch cause_wounds
sound wn/bow-puny-miss
face spell_praying.x11
grace 25
maxgrace 100
dam 100
level 27
subtype 5
attacktype 1048576
path_attuned 131072
value 1
invisible 1
dam_modifier 1
no_drop 1
casting_time 5
end

