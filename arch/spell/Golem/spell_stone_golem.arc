object spell_summon_stone_golem
inherit type_spell
name summon stone golem
name_pl summon stone golem
skill summoning
msg
Summons a stone golem that does
the caster's wishes. The caster can have it
attack other creatures, bash down doors,
detonate runes and so on.

The stone golem has a finite
lifetime, and this life is shortened any time
it takes damage. This stone golem is more
powerful than the paper golem but has diffrent
weaknesses.
endmsg
other_arch stonegolem
face spell_summoner.x11
sp 120
maxsp 40
dam 200
level 40
subtype 12
attacktype 1
path_attuned 64
value 1
invisible 1
duration 150
range_modifier 15
duration_modifier 5
dam_modifier 1
no_drop 1
casting_time 10
end

