object spell_dancing_sword
inherit type_spell
name dancing sword
name_pl dancing sword
skill summoning
msg
Summons a magical sword as your golem.
The sword moves in the direction it was targetted at.
It can be moved in different directions from player like any other golem.
You will get the experience for creatures that the swords kills!
endmsg
other_arch dancingsword
face spell_summoner.x11
sp 25
maxsp 10
dam 10
level 17
subtype 12
attacktype 1
path_attuned 2048
value 1
invisible 1
duration 75
range_modifier 15
duration_modifier 1
dam_modifier 2
no_drop 1
casting_time 10
end

