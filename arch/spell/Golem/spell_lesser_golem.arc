# Summoners need a first level spell, and this is it - make it
# so most of the values don't scale - otherwise, this could be too
# powerful at higher levels.
object spell_lesser_summon_golem
inherit type_spell
name summon lesser golem
name_pl summon lesser golem
skill summoning
msg
Summons a lesser golem, a magical creature that
does the caster's wishes. The caster can
have it attack other creatures, bash down
doors, detonate runes and so on.

The golem has a finite lifetime,
and this life is shortened
any time it takes damage.
endmsg
other_arch golem
face spell_summoner.x11
sp 3
maxsp 15
dam 10
level 1
subtype 12
attacktype 1
path_attuned 64
value 1
invisible 1
duration 75
duration_modifier 5
no_drop 1
casting_time 10
end

