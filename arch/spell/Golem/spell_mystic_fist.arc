object spell_mystic_fist
inherit type_spell
name mystic fist
name_pl mystic fist
skill summoning
msg
Makes a magical fist appear, which you can control like any other golem.
You will get all experience of the fist's kills!

Do not get in front of your fist - hitting yourself with your own fist
is considered a face-losing incident.
endmsg
other_arch mystic_fist
face spell_summoner.x11
sp 10
maxsp 15
dam 8
level 5
subtype 12
attacktype 8
path_attuned 64
value 1
invisible 1
duration 50
range_modifier 20
duration_modifier 10
dam_modifier 10
no_drop 1
casting_time 15
end

