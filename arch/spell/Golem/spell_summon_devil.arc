# This is currently only used by runes
#
object spell_summon_devil
inherit type_spell
name summon devil
name_pl summon devil
skill summoning
msg
Calls up a devil from the
infernal regions of the underworld. The
devil is under the control of the caster,
much the same way a golem is. The attacks and
protections are same as that of a normal
devil, which can make it particularly useful
against some creatures.
endmsg
other_arch devil
face spell_summoner.x11
sp 20
maxsp 15
dam 10
level 19
subtype 12
attacktype 8
path_attuned 64
value 1
invisible 1
duration 160
range_modifier 20
duration_modifier 10
dam_modifier 10
no_drop 1
casting_time 13
end

