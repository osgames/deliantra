object spell_call_holy_servant
inherit type_spell
name call holy servant
name_pl call holy servant
race holy servant
skill praying
msg
Summons a strong fighter from your god, whom you may direct like a golem.
It can be sent in various directions like any other golem, and
you get all experience from its kills.
endmsg
face spell_praying.x11
grace 30
maxgrace 20
dam 0
level 12
subtype 12
path_attuned 64
value 1
invisible 1
duration 120
range_modifier 15
duration_modifier 8
dam_modifier 8
no_drop 1
casting_time 5
end

