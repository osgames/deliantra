object avatar
anim
facings 2
avatar.x31
avatar.x32
avatar.x71
avatar.x72
mina
name Avatar
face avatar.x31
is_animated 1
hp 500
maxhp 500
exp 1000
dam 50
wc -1
ac -10
speed 0.25
level 12
type 46
resist_physical 45
resist_magic 100
weight 200000
alive 1
monster 1
end

