object spell_summon_air_elemental
inherit type_spell
name summon air elemental
name_pl summon air elemental
skill summoning
msg
Summons
an air elemental. The air elemental acts in
much the same way as a golem. Its attacks
and protections are that of a standard air
elemental, which can make it particularly
useful against some creatures, whose names
we won't tell you here.
endmsg
other_arch air_elemental
face spell_summoner.x11
sp 20
maxsp 15
dam 10
level 13
subtype 12
attacktype 8
path_attuned 64
value 1
invisible 1
duration 160
range_modifier 20
duration_modifier 10
dam_modifier 10
no_drop 1
casting_time 12
end

