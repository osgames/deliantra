object mystic_fist
anim
mystic_fist.x11
mystic_fist.x12
mystic_fist.x13
mina
name Mystic Fist
face mystic_fist.x11
hp 50
maxhp 50
exp 200
dam 8
wc 12
ac 5
speed 0.5
level 9
type 102
subtype 12
weight 20
alive 1
monster 1
end

