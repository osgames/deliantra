object holy_servant
anim
golem.x11
golem.x12
mina
name Servant
face golem.x11
is_animated 1
hp 50
maxhp 50
exp 50
dam 5
wc 6
ac 4
speed 0.15
level 8
type 46
resist_physical 25
resist_magic 100
weight 200000
alive 1
monster 1
end

