object spell_summon_water_elemental
inherit type_spell
name summon water elemental
name_pl summon water elemental
skill summoning
msg
Summons a creature
from the elemental plane of water. This
elemental will do the caster's wishes, much
like a golem. Water elementals attack with
cold and physical attacks. They are not,
however, completely immune to cold attacks.
endmsg
other_arch water_elemental
face spell_summoner.x11
sp 15
maxsp 15
dam 40
level 11
subtype 12
attacktype 17
path_attuned 64
value 1
invisible 1
duration 140
range_modifier 20
duration_modifier 10
dam_modifier 10
no_drop 1
casting_time 11
end

