object spell_summon_golem
inherit type_spell
name summon golem
name_pl summon golem
skill summoning
msg
Summons a magical golem that does
the caster's wishes. The caster can have it
attack other creatures, bash down doors,
detonate runes and so on. The golem has a finite
lifetime, and this life is shortened any time
it takes damage. This golem is a bit more
powerful than the lesser golem.
endmsg
other_arch golem
face spell_summoner.x11
sp 5
maxsp 15
dam 16
level 10
subtype 12
attacktype 1
path_attuned 64
value 1
invisible 1
duration 75
range_modifier 15
duration_modifier 5
dam_modifier 10
no_drop 1
casting_time 10
end

