object spell_summon_avatar
inherit type_spell
name summon avatar
name_pl summon avatar
race avatar
skill praying
msg
Summons the strongest fighter from your god.
You may direct him like any other golem, and you will
get all experience form its kills.

This is the strongest summon golem spell.
endmsg
face spell_praying.x11
grace 60
maxgrace 30
dam 0
level 20
subtype 12
path_attuned 64
value 1
invisible 1
duration 380
range_modifier 15
duration_modifier 5
dam_modifier 5
no_drop 1
casting_time 15
end

