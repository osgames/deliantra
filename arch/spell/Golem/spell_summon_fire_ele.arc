object spell_summon_fire_elemental
inherit type_spell
name summon fire elemental
name_pl summon fire elemental
skill summoning
msg
Summons a creature
from the elemental plane of fire. This
elemental will do the caster's wishes, much
like a golem. Because they are made of fire,
fire elementals are impervious to fire
based damage, and have fire based attacks.
endmsg
other_arch fire_elemental
face spell_summoner.x11
sp 25
maxsp 15
dam 20
level 18
subtype 12
attacktype 4
path_attuned 64
value 1
invisible 1
duration 200
range_modifier 20
duration_modifier 10
dam_modifier 10
no_drop 1
casting_time 12
end

