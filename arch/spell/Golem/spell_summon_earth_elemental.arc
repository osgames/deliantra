object spell_summon_earth_elemental
inherit type_spell
name summon earth elemental
name_pl summon earth elemental
skill summoning
msg
Summons an elemental
from the surrounding rock. This elemental
will do the caster's wishes, much like a
golem. Earth elementals are slow but very
durable. They have powerful physical
attacks.
endmsg
other_arch earth_elemental
face spell_summoner.x11
sp 15
maxsp 15
dam 50
level 12
subtype 12
attacktype 1
path_attuned 64
value 1
invisible 1
duration 280
range_modifier 20
duration_modifier 10
dam_modifier 10
no_drop 1
casting_time 11
end

