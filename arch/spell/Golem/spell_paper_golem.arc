object spell_summon_paper_golem
inherit type_spell
name summon paper golem
name_pl summon paper golem
skill summoning
msg
Summons a paper golem that does
the caster's wishes. The caster can have it
attack other creatures, bash down doors,
detonate runes and so on.

The paper golem has a finite
lifetime, and this life is shortened any time
it takes damage. This paper golem is more
powerful than the golem.
endmsg
other_arch papergolem
face spell_summoner.x11
sp 100
maxsp 40
dam 100
level 20
subtype 12
attacktype 1
path_attuned 64
value 1
invisible 1
duration 150
range_modifier 15
duration_modifier 5
dam_modifier 1
no_drop 1
casting_time 10
end

