object spell_darkness
inherit type_spell
name darkness
name_pl darkness
skill praying
msg
Creates a wall of pure darkness in the direction it is cast but perpendicular to it.

The darkness will block vision, but nothing else, and will eventually go away on its
own accord.
endmsg
other_arch darkness
face spell_praying.x11
grace 15
maxgrace 50
level 12
subtype 15
path_attuned 524288
value 1
invisible 1
duration 50
range 2
range_modifier 13
duration_modifier 2
move_block all
no_drop 1
casting_time 5
end

