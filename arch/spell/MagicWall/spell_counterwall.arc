object spell_counterwall
inherit type_spell
name counterwall
name_pl counterwall
skill summoning
msg
Counterwall creates a wall of counterspell.
If the wall is strong enough, it prevents
other spells from penetrating it, giving you
a safe area behind it. The caster's spells
are not blocked.

Counterwalls can be useful to
constantly neutralize spell effects. The
counterwall itself has finite duration.
endmsg
other_arch counterspell
face spell_summoner.x11
sp 8
maxsp 20
level 22
subtype 15
attacktype 524288
path_attuned 256
value 1
invisible 1
duration 100
range 2
range_modifier 15
duration_modifier 1
move_block all
no_drop 1
casting_time 14
end

