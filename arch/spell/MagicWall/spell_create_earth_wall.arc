object spell_create_earth_wall
inherit type_spell
name create earth wall
name_pl create earth wall
skill summoning
msg
Creates a line of earth
walls. Unlike other magic wall spells, the
earthwalls cast no spells and do no damage.
However, they block most creatures from
passing over them, and block vision. Also,
unlike many of the magic wall spells,
earthwalls will stay around until they are
physically destroyed.
endmsg
other_arch earthwall
face spell_summoner.x11
sp 6
level 15
subtype 15
path_attuned 2048
value 1
invisible 1
duration 40
range 2
range_modifier 10
duration_modifier 1
move_block all
no_drop 1
casting_time 12
end

