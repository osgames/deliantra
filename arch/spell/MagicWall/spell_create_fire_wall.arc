object spell_create_fire_wall
inherit type_spell
name create fire wall
name_pl create fire wall
skill pyromancy
msg
Creates a wall of raging fire in the direction it is cast, perpendicular to it.
The wall will hurt creatures that try to cross it with fire, and will eventually
evaporate.
endmsg
other_arch firebreath
face spell_pyromancy.x11
sp 5
maxsp 8
dam 4
level 18
subtype 15
attacktype 6
path_attuned 2048
value 1
invisible 1
duration 260
range 2
range_modifier 10
duration_modifier 1
dam_modifier 3
move_block all
no_drop 1
casting_time 10
end

