# This spell is used by the volcanoes
# As such, the values it contains may need quite a bit of
# adjustment if this is ever given to a player.
object spell_create_lava
inherit type_spell
name create lava
name_pl create lava
skill none
msg
**BROKEN**TODO**
endmsg
other_arch lava
sp 8
maxsp 20
level 8
subtype 15
path_attuned 256
value 1
invisible 1
duration 100
range 0
duration_modifier 1
move_block all
no_drop 1
casting_time 30
end

