object spell_build_bullet_wall
inherit type_spell
name build bullet wall
name_pl build bullet wall
race lbulletwall_%d
skill evocation
msg
Creates a wall that fires
magic bullets in the direction it was targetted,
as per the magic bullet spell.

The bullet wall itself can be torn down, and
will eventually expire on its own.
endmsg
face spell_evocation.x11
sp 35
maxsp 10
dam 80
level 35
subtype 15
path_attuned 2048
value 1
invisible 1
duration 100
range 0
duration_modifier 1
dam_modifier 1
move_block all
is_used_up 1
tear_down 1
no_drop 1
casting_time 15
end

