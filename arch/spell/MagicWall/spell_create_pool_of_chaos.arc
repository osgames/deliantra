object spell_create_pool_of_chaos
inherit type_spell
name create pool of chaos
name_pl create pool of chaos
skill evocation
msg
Creates a wall-like area with a chaos pool in it, in the direction it is cast but perpendicular to it.
The wall will hurt creatures that try to cross it with chaos, and will eventually evaporate.
endmsg
other_arch color_spray
face spell_evocation.x11
sp 10
maxsp 15
dam 4
level 40
subtype 15
attacktype 262144
path_attuned 2048
value 1
invisible 1
duration 240
range 2
range_modifier 20
duration_modifier 1
dam_modifier 3
move_block all
no_drop 1
casting_time 15
end

