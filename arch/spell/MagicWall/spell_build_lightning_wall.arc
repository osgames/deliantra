object spell_build_lightning_wall
inherit type_spell
name build lightning wall
name_pl build lightning wall
race lightningwall_%d
skill pyromancy
msg
Creates a wall that
fires lightning bolts. The wall fires in the
direction it was targetted at. The wall can be
torn down by creatures, and will eventually
expire of its own accord.
endmsg
face spell_pyromancy.x11
sp 40
maxsp 10
dam 80
level 28
subtype 15
path_attuned 2048
value 1
invisible 1
duration 100
range 0
duration_modifier 1
dam_modifier 1
move_block all
is_used_up 1
tear_down 1
no_drop 1
casting_time 14
end

