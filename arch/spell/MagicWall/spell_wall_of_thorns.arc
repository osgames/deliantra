object spell_wall_of_thorns
inherit type_spell
name wall of thorns
name_pl wall of thorns
skill praying
msg
Creates a wall of thorns perpendicular to the direction it was targetted at.

The wall will hurt creatures that try to cross it physically, and will eventually
evaporate.
endmsg
other_arch thorns
face spell_praying.x11
grace 20
maxgrace 50
dam 4
level 12
subtype 15
attacktype 1
path_attuned 2048
value 1
invisible 1
duration 300
range 2
range_modifier 14
duration_modifier 1
dam_modifier 3
move_block all
no_drop 1
casting_time 5
end

