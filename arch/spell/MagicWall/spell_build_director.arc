object spell_build_director
inherit type_spell
name build director
name_pl build director
race director_%d
skill summoning
msg
Creates a director facing into the direction it was taregtted
at. Directors can be used to change the course of certain spells - most
notably bullets and bolts. Thus, it can be used to be able to fire a spell
around a corner, or direct a spell back at the caster.
endmsg
face spell_summoner.x11
sp 30
maxsp 10
dam 80
level 20
subtype 15
path_attuned 2048
value 1
invisible 1
duration 200
range 0
duration_modifier 1
dam_modifier 1
move_block all
is_used_up 1
no_drop 1
casting_time 13
end

