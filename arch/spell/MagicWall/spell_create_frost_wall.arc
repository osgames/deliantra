object spell_create_frost_wall
inherit type_spell
name create frost wall
name_pl create frost wall
skill evocation
msg
Creates a wall-like area with an icestorm in it, in the direction it is cast but perpendicular to it.
The wall will hurt creatures that try to cross it with frost, and will eventually
evaporate.
endmsg
other_arch icestorm
face spell_evocation.x11
sp 8
maxsp 8
dam 2
level 14
subtype 15
attacktype 18
path_attuned 2048
value 1
invisible 1
duration 240
range 2
range_modifier 12
duration_modifier 1
dam_modifier 3
move_block all
no_drop 1
casting_time 10
end

