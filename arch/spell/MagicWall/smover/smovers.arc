object smover_1
anim
pmarrow.x11
pmarrow.x12
pmarrow.x13
mina
name force
face pmarrow.x11
hp 10
sp 1
speed -0.4
type 40
attacktype 1
invisible 0
move_on walk
no_pick 1
lifesave 1
end

object smover_2
anim
pmarrow.x21
pmarrow.x22
pmarrow.x23
pmarrow.x24
pmarrow.x25
mina
name force
face pmarrow.x21
hp 10
sp 2
speed -0.4
type 40
attacktype 1
invisible 0
move_on walk
no_pick 1
lifesave 1
end

object smover_3
anim
pmarrow.x31
pmarrow.x32
pmarrow.x33
mina
name force
face pmarrow.x31
hp 10
sp 3
speed -0.4
type 40
attacktype 1
invisible 0
move_on walk
no_pick 1
lifesave 1
end

object smover_4
anim
pmarrow.x41
pmarrow.x42
pmarrow.x43
pmarrow.x44
pmarrow.x45
mina
name force
face pmarrow.x41
hp 10
sp 4
speed -0.4
type 40
attacktype 1
invisible 0
move_on walk
no_pick 1
lifesave 1
end

object smover_5
anim
pmarrow.x51
pmarrow.x52
pmarrow.x53
mina
name force
face pmarrow.x51
hp 10
sp 5
speed -0.4
type 40
attacktype 1
invisible 0
move_on walk
no_pick 1
lifesave 1
end

object smover_6
anim
pmarrow.x61
pmarrow.x62
pmarrow.x63
pmarrow.x64
pmarrow.x65
mina
name force
face pmarrow.x61
hp 10
sp 6
speed -0.4
type 40
attacktype 1
invisible 0
move_on walk
no_pick 1
lifesave 1
end

object smover_7
anim
pmarrow.x71
pmarrow.x72
pmarrow.x73
mina
name force
face pmarrow.x71
hp 10
sp 7
speed -0.4
type 40
attacktype 1
invisible 0
move_on walk
no_pick 1
lifesave 1
end

object smover_8
anim
pmarrow.x81
pmarrow.x82
pmarrow.x83
pmarrow.x84
pmarrow.x85
mina
name force
face pmarrow.x81
hp 10
sp 8
speed -0.4
type 40
attacktype 1
invisible 0
move_on walk
no_pick 1
lifesave 1
end

object smover_turn
anim
pmarrow.x11
pmarrow.x12
pmarrow.x13
pmarrow.x21
pmarrow.x23
pmarrow.x25
pmarrow.x31
pmarrow.x32
pmarrow.x33
pmarrow.x41
pmarrow.x43
pmarrow.x45
pmarrow.x51
pmarrow.x52
pmarrow.x53
pmarrow.x61
pmarrow.x63
pmarrow.x65
pmarrow.x71
pmarrow.x72
pmarrow.x73
pmarrow.x81
pmarrow.x83
pmarrow.x85
mina
name force
face pmarrow.x81
hp 10
maxsp 1
speed 0.2
type 40
attacktype 1
invisible 0
move_on walk
no_pick 1
lifesave 1
end

