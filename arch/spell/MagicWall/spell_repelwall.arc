object spell_repelwall
inherit type_spell
name create repulsion wall
name_pl create repulsion wall
race smover_%d
skill sorcery
msg
Creates a wall that will repel any creature unless
it flies. However it requires a lot of mana to cast and has limited duration.
endmsg
face spell_sorcery.x11
sp 30
maxsp 20
level 35
subtype 15
attacktype 1
path_attuned 2048
value 1
invisible 1
duration 60
range 1
range_modifier 60
duration_modifier 5
is_used_up 1
no_drop 1
casting_time 20
end

