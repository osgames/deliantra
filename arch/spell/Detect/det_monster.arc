# Basically same as detect magic, but slower so the effect
# sticks around longer.
object detect_monster
anim
det_magic.x11
det_magic.x12
det_magic.x13
mina
name magic glow
face det_magic.x11
food 3
speed 0.2
no_pick 1
is_used_up 1
see_anywhere 1
end

