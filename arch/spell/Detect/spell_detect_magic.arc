object spell_detect_magic
inherit type_spell
name detect magic
name_pl detect magic
skill sorcery
msg
All magic items in your inventory will be marked as magic.
endmsg
other_arch detect_magic
face spell_sorcery.x11
sp 1
level 3
subtype 33
path_attuned 8192
value 1
invisible 1
range 12
range_modifier 5
no_drop 1
known_magical 1
casting_time 13
end

