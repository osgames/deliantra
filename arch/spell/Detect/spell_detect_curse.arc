object spell_detect_curse
inherit type_spell
name detect curse
name_pl detect curse
skill praying
msg
This prayer will mark all cursed or damned items in your inventory.
endmsg
other_arch detect_magic
face spell_praying.x11
grace 10
level 5
subtype 33
path_attuned 8192
value 1
invisible 1
range 12
range_modifier 5
no_drop 1
known_cursed 1
casting_time 20
end

