object spell_detect_mineral
inherit type_spell
name detect mineral
name_pl detect mineral
skill mining
msg
This spell will show you all mineral veins on the current map.
The places where veins are will light up and maybe even show you
the mineral that you can harvest there.
endmsg
other_arch detect_mineral
sound olpc/detect_minerals
face spell_mining.x11
sp 4
level 1
subtype 33
path_attuned 8192
value 1
invisible 1
last_sp 1
range 12
range_modifier 5
no_drop 1
casting_time 13
end

