object spell_show_invisible
inherit type_spell
name show invisible
name_pl show invisible
skill praying
msg
This prayer may reveal invisible objects or monsters.

You need this spell to show invisible levers or items on maps.
endmsg
other_arch detect_magic
face spell_praying.x11
grace 10
level 10
subtype 33
path_attuned 8192
value 1
invisible 1
range 12
range_modifier 5
no_drop 1
make_invisible 1
casting_time 20
end

