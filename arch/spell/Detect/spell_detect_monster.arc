object spell_detect_monster
inherit type_spell
name detect monster
name_pl detect monster
skill evocation
msg
Unseen or hidden monsters will be shown.
endmsg
other_arch detect_monster
face spell_evocation.x11
sp 2
level 14
subtype 33
path_attuned 8192
value 1
invisible 1
range 12
range_modifier 5
monster 1
no_drop 1
casting_time 15
end

