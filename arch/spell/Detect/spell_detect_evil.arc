object spell_detect_evil
inherit type_spell
name detect evil
name_pl detect evil
race GOD
skill praying
msg
This prayer detect all enemies with evil minds.
endmsg
other_arch detect_monster
face spell_praying.x11
grace 3
level 3
subtype 33
path_attuned 8192
value 1
invisible 1
range 12
range_modifier 5
no_drop 1
casting_time 15
end

