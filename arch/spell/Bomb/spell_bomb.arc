object spell_create_bomb
inherit type_spell
name create bomb
name_pl create bomb
skill pyromancy
msg
Create bomb does just that - it creates a
bomb in the direction the spell is fired.
The bomb detonates after a few seconds,
firing sharp shrapnel as well as encompassing the
area in an explosions. Creatures will be
wounded if they are in the explosion or are
hit by the flying shrapnel.
endmsg
other_arch bomb
face spell_pyromancy.x11
sp 12
maxsp 72
dam 12
level 20
subtype 8
attacktype 1
path_attuned 512
value 1
invisible 1
duration 8
range 5
dam_modifier 6
no_drop 1
casting_time 5
end

