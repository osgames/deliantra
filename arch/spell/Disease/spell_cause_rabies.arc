object spell_cause_rabies
inherit type_spell
name cause rabies
name_pl cause rabies
skill praying
msg
This prayer will cause rabies in the infected target.
It reduces it's strength, dexterity and makes it look very ugly.
endmsg
other_arch rabies
face spell_praying.x11
grace 120
maxgrace 12
level 35
subtype 45
path_attuned 131072
value 1
invisible 1
range 5
range_modifier 10
duration_modifier 1
dam_modifier 1
no_drop 1
casting_time 10
end

