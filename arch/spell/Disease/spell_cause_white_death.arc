object spell_cause_white_death
inherit type_spell
name cause white death
name_pl cause white death
skill praying
msg
He who unleashes the white death had best flee.
This highly contageous and deadly disease does not respect its own master.
endmsg
other_arch pneumonic_plague
face spell_praying.x11
grace 350
maxgrace 24
level 85
subtype 45
path_attuned 131072
value 1
invisible 1
range 5
range_modifier 10
duration_modifier 5
dam_modifier 1
no_drop 1
casting_time 10
end

