object spell_cause_cold
inherit type_spell
name cause cold
name_pl cause cold
skill praying
msg
This prayer causes a disease to spread.
This disease is highly contageous, and somewhat debilitating,
but rarely fatal, unless it is complicated by other infections.

Players and monsters affected will move more slowly, and be weaker,
until the disease runs its course or is cured.
endmsg
other_arch disease_cold
face spell_praying.x11
grace 10
maxgrace 10
level 18
subtype 45
path_attuned 131072
value 1
invisible 1
range 5
range_modifier 10
duration_modifier 5
dam_modifier 3
no_drop 1
casting_time 10
end

