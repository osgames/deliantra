object spell_cause_red_death
inherit type_spell
name cause red death
name_pl cause red death
skill praying
msg
The red death prayer unleashes a virulent, broadly effective,
and deadly plague. He who unleashes the red death had better avoid
his victims and their remains, lest he die.

Granted to by Devourers at medium level.
endmsg
other_arch ebola
face spell_praying.x11
grace 100
maxgrace 24
level 32
subtype 45
path_attuned 131072
value 1
invisible 1
range 5
range_modifier 10
duration_modifier 1
dam_modifier 1
no_drop 1
casting_time 10
end

