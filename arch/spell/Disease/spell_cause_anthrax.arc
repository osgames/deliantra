object spell_cause_anthrax
inherit type_spell
name cause anthrax
name_pl cause anthrax
skill praying
msg
Anthrax is a deadly disease to the animal kind.
It does not usually affect other creatures.
endmsg
other_arch anthrax
face spell_praying.x11
grace 50
maxgrace 10
level 22
subtype 45
path_attuned 131072
value 1
invisible 1
range 5
range_modifier 10
duration_modifier 10
dam_modifier 1
no_drop 1
casting_time 10
end

