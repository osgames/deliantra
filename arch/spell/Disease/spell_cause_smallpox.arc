object spell_cause_smallpox
inherit type_spell
name cause smallpox
name_pl cause smallpox
skill praying
msg
This prayer unleashes an outbreak of the deadly disease smallpox.
This disease is highly contageous and often fatal to weaker monsters,
and sometimes to stronger monsters. However, its deadliness is restricted
to humanoids.

Granted by Gnarg at medium level.
endmsg
other_arch smallpox
face spell_praying.x11
grace 85
maxgrace 10
level 20
subtype 45
path_attuned 131072
value 1
invisible 1
range 5
range_modifier 10
duration_modifier 4
dam_modifier 1
no_drop 1
casting_time 10
end

