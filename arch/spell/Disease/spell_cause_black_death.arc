object spell_cause_black_death
inherit type_spell
name cause black death
name_pl cause black death
skill praying
msg
This prayer will unleash the bubonic plague, or black death.
This plague is broadly fatal, very deadly, and highly contageous.
He who unleashes the bubonic plague is wise to flee.

Granted by Devourers at high level.
endmsg
other_arch bubonic_plague
face spell_praying.x11
grace 120
maxgrace 20
level 25
subtype 45
path_attuned 0
value 1
invisible 1
range 5
range_modifier 10
duration_modifier 10
dam_modifier 3
no_drop 1
casting_time 10
end

