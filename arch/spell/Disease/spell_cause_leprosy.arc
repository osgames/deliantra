object spell_cause_leprosy
inherit type_spell
name cause leprosy
name_pl cause leprosy
skill praying
msg
This prayer inflicts leprosy upon one target.
Leprosy is not usually contageous, but touching may spread the disease.
Beware the attack of a monster on which you've inflicted leprosy!
endmsg
other_arch leprosy
face spell_praying.x11
grace 20
maxgrace 10
level 25
subtype 45
path_attuned 131072
value 1
invisible 1
range 5
range_modifier 10
duration_modifier 1
dam_modifier 1
no_drop 1
casting_time 10
end

