object spell_cause_flu
inherit type_spell
name cause flu
name_pl cause flu
skill praying
msg
The flu prayer unleashes an outbreak of the flu.
It is rarely deadly by itself, except to weak monsters,
but it is very debilitating.
endmsg
other_arch flu
face spell_praying.x11
grace 10
maxgrace 10
level 18
subtype 45
path_attuned 131072
value 1
invisible 1
range 5
range_modifier 10
duration_modifier 5
dam_modifier 3
no_drop 1
casting_time 10
end

