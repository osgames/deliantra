object spell_cause_typhoid
inherit type_spell
name cause typhoid
name_pl cause typhoid
skill praying
msg
Typhoid is a deadly disease to humanoids, but other monsters are not affected.
endmsg
other_arch typhoid
face spell_praying.x11
grace 60
maxgrace 24
level 28
subtype 45
path_attuned 131072
value 1
invisible 1
range 5
range_modifier 10
duration_modifier 10
dam_modifier 1
no_drop 1
casting_time 10
end

