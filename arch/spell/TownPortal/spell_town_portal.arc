object spell_town_portal
inherit type_spell
name town portal
name_pl town portal
skill sorcery
msg
Creates a magical portal to the nearest town.

You can create as many portals as you want, and when you (or anybody else)
uses one to go to the nearest town, you can return from the town to that
portal once. The portals will evaporate after a (long) while, and you
cannot return to a portal once it has done so.

What town is "nearest" depends not completely on the normal distance to
the town, but on the distance in the so-called "Noiger" space, which can
sometimes be quite different.

This spell is great to transport and sell loot to the nearest shop
and quickly return to the dungeon for more carnage, and pilfering.
endmsg
other_arch town_portal_magic
face spell_sorcery.x11
sp 50
level 20
subtype 47
path_attuned 4096
value 1
invisible 1
no_drop 1
casting_time 10
end

