object town_portal
name Town portal destination
slaying Town portal destination
face blank.x11
hp 0
type 114
invisible 1
no_drop 1
end

object town_portal_active
name Existing town portal
slaying Existing town portal
face blank.x11
hp 0
type 114
invisible 1
no_drop 1
end

object town_portal_magic
anim
magic_portal.x11
magic_portal.x12
magic_portal.x13
magic_portal.x14
magic_portal.x15
magic_portal.x16
magic_portal.x17
magic_portal.x18
magic_portal.x19
magic_portal.x18
magic_portal.x17
magic_portal.x16
magic_portal.x15
magic_portal.x14
magic_portal.x13
magic_portal.x12
mina
name magic portal
attach [["town_portal"]]
face magic_portal.x11
hp 15
sp 19
food 10000
speed 1
type 66
client_type 25011
no_pick 1
is_used_up 1
end

