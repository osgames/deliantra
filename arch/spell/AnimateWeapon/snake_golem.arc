object snake_golem
anim
snake.x11
snake.x12
mina
name snake
face snake.x11
is_animated 1
hp 10
maxhp 10
exp 50
dam 3
wc 12
ac 5
speed 0.10
level 3
type 102
subtype 12
attacktype 1025
resist_poison 100
weight 200000
alive 1
monster 1
end

