object spell_animate_weapon
inherit type_spell
name animate weapon
name_pl animate weapon
skill sorcery
msg
This will animate the marked weapon from your inventory - it will become
a golem. The duration of control is limited and after the spell runs out,
the weapon will drop to the floor.
endmsg
other_arch dancingsword
face spell_sorcery.x11
sp 25
maxsp 20
dam 2
level 27
subtype 41
path_attuned 4096
value 1
invisible 1
duration 20
range_modifier 20
duration_modifier 1
dam_modifier 1
no_drop 1
casting_time 10
end

