object spell_staff_to_snake
inherit type_spell
name staff to snake
name_pl staff to snake
race quarterstaff
skill praying
msg
Transforms a marked quarterstaff from your inventory
to a snake that you can control like a golem. After it runs out
the snake will be transformed back to the quarterstaff and drop to the floor.
endmsg
other_arch snake_golem
face spell_praying.x11
grace 8
maxgrace 18
dam 10
level 10
subtype 41
path_attuned 2048
value 1
invisible 1
duration 100
range_modifier 20
duration_modifier 1
dam_modifier 1
no_drop 1
casting_time 5
end

