object spell_summon_dark_angel
inherit type_spell
name summon dark angel
name_pl summon dark angels
skill summoning
msg
Forth from the abyss is called an angel of darkness...
endmsg
face spell_summoner.x11
sp 30
attack_movement 16
level 35
subtype 27
path_attuned 64
value 100
invisible 1
randomitems summon_dark_angel
no_drop 1
casting_time 40
end

