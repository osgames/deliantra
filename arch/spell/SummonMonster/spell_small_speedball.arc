object spell_small_speedball
inherit type_spell
name small speedball
name_pl small speedball
skill evocation
msg
Speedballs are bundles of magical energy that
hunt out enemies to hit. They do a small
amount of damage and slow the creatures they
strike. After speedballs hit a creature,
they dissipate.
endmsg
other_arch speedball
face spell_evocation.x11
sp 3
level 11
subtype 27
path_attuned 16
value 1
invisible 1
monster 1
no_drop 1
casting_time 5
end

