object spell_large_speedball
inherit type_spell
name large speedball
name_pl large speedball
skill evocation
msg
Speedballs are bundles of magical energy that
hunt out enemies to hit. They do a small
amount of damage and slow the creatures they
strike. After speedballs hit a creature,
they dissipate.
endmsg
other_arch lg_speedball
face spell_evocation.x11
sp 6
level 36
subtype 27
path_attuned 16
value 1
invisible 1
monster 1
no_drop 1
casting_time 10
end

