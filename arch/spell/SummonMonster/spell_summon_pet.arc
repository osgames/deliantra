object spell_summon_pet_monster
inherit type_spell
name summon pet monster
name_pl summon pet monster
skill summoning
msg
Creates a number of monsters to help out
the caster.

Which and how many appear depend on the spell level, but lower level pets
might be summoned by explicitly mentioning their archetype name as spell
argument.

The pets can't be controlled by the caster directly, but they will move
around and follow him. You can use the C<petmode> command to influence
their general behaviour, however.

They typically attack other creatures and bash down doors. The creatures
persist until they are killed.
endmsg
face spell_summoner.x11
sp 5
attack_movement 16
level 3
subtype 27
path_attuned 64
value 1
invisible 1
randomitems mage_pet_monster
no_drop 1
casting_time 10
end

