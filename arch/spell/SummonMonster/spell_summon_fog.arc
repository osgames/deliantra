object spell_summon_fog
inherit type_spell
name summon fog
name_pl summon fog
skill summoning
msg
Creates fog near the player. The
fog moves about randomly after being
summoned. It has no harmful effect, but can
block visibility and thus can be used to hide.

The fog is actually a magical creature, and killing
it is possible, but hard.
endmsg
other_arch fog
face spell_summoner.x11
sp 5
dam 2
level 4
subtype 27
path_attuned 2048
value 1
invisible 1
dam_modifier 10
monster 1
no_drop 1
casting_time 10
end

