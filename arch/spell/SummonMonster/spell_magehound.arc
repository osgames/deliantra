object spell_magehound
inherit type_spell
name magehound
name_pl magehounds
skill summoning
msg
Summons a hound pet monster which will attack your enemies.
endmsg
face spell_summoner.x11
sp 12
attack_movement 16
level 25
subtype 27
path_attuned 64
value 1
invisible 1
randomitems mage_hound
no_drop 1
casting_time 40
end

