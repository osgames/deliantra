object spell_summon_cult_monsters
inherit type_spell
name summon cult monsters
name_pl summon cult monsters
race GODCULTMON
skill praying
msg
Summons pets from your god which will defend you and kill your enemies.

The pets can't be controlled by the caster directly, but they will move
around and follow him. You can use the C<petmode> command to influence
their general behaviour, however.

You get all experience from their kills.
endmsg
face spell_praying.x11
grace 12
maxgrace 10
attack_movement 16
level 8
subtype 27
path_attuned 64
value 1
invisible 1
no_drop 1
casting_time 10
end

