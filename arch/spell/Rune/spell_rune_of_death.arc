object spell_rune_of_death
inherit type_spell
name rune of death
name_pl rune of death
skill sorcery
msg
Creates a rune that, when triggered,
hits the target with death magic. This
either results in the death of the creature,
no matter the creatures hit points, or no
harm at all.
endmsg
other_arch rune_death
face spell_sorcery.x11
sp 20
level 17
subtype 2
path_attuned 262144
value 1
invisible 1
no_drop 1
casting_time 15
end

