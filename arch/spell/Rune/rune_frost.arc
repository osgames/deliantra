object rune_frost
anim
rune_frost.x11
icestorm.x11
mina
name Rune of Frost
msg
You detonate a Rune of Frost!
endmsg
face rune_frost.x11
is_animated 0
cha 20
hp 1
dam 35
speed 1
level 1
type 96
attacktype 18
invisible 1
move_on walk
no_pick 1
end

object rune_icestorm
anim
rune_frost.x11
icestorm.x11
mina
name Rune of Icestorm
msg
You detonate a Rune of Icestorm
endmsg
other_arch spell_icestorm
face rune_frost.x11
is_animated 0
cha 20
hp 1
speed 1
level 1
type 96
invisible 1
move_on walk
no_pick 1
end

object rune_large_icestorm
anim
rune_frost.x11
icestorm.x11
mina
name Rune of Large Icestorm
msg
You detonate a Rune of Large Icestorm!
endmsg
other_arch spell_large_icestorm
face rune_frost.x11
is_animated 0
cha 20
hp 1
speed 1
level 1
type 96
invisible 1
move_on walk
no_pick 1
end

