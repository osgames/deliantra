object rune_poison_cloud
anim
rune_pcloud.x11
poisoncloud.x11
mina
name Rune of Poison Cloud
msg
You detonate a Rune of Poison Cloud!
endmsg
other_arch spell_poison_cloud
face rune_pcloud.x11
is_animated 0
cha 20
hp 1
speed 1
level 1
type 96
invisible 1
move_on walk
no_pick 1
end

