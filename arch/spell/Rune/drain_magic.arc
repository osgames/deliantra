object rune_drain_magic
anim
drain_magic.x11
drain_magic.x11
mina
name Rune of Magic Draining
msg
You feel depleted of psychic energy!
endmsg
other_arch spell_magic_drain
face drain_magic.x11
is_animated 0
cha 20
hp 1
speed 1
level 1
type 96
invisible 1
move_on walk
no_pick 1
end

