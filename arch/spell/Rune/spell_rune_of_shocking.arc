object spell_rune_of_shocking
inherit type_spell
name rune of shocking
name_pl rune of shocking
skill pyromancy
msg
Inscribes a near-invisible rune upon the ground.
When someone steps onto the rune, they take electrical damage.
endmsg
other_arch rune_shock
face spell_pyromancy.x11
sp 14
level 7
subtype 2
path_attuned 8
value 1
invisible 1
no_drop 1
casting_time 10
end

