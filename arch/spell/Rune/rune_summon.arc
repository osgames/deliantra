object rune_summon_air_elemental
anim
rune_summon_air.x11
rune_summon_air.x11
mina
name Rune of Summoning
msg
A portal opens up, and screaming hordes pour
through!
endmsg
other_arch spell_summon_air_elemental
face rune_summon_air.x11
is_animated 0
cha 20
hp 1
maxhp 5
dam 90
speed 1
level 1
type 96
attacktype 2
invisible 1
move_on walk
no_pick 1
end

object rune_summon_devil
anim
rune_summon.x11
rune_summon.x11
mina
name Rune of Summoning
msg
A portal opens up, and screaming hordes pour
through!
endmsg
other_arch spell_summon_devil
face rune_summon.x11
is_animated 0
cha 20
hp 1
maxhp 5
dam 90
speed 1
level 1
type 96
attacktype 2
invisible 1
move_on walk
no_pick 1
end

object rune_summon_earth_elemental
anim
rune_sum_earth.x11
rune_sum_earth.x11
mina
name Rune of Summoning
msg
A portal opens up, and screaming hordes pour
through!
endmsg
other_arch spell_summon_earth_elemental
face rune_sum_earth.x11
is_animated 0
cha 20
hp 1
maxhp 5
dam 90
speed 1
level 1
type 96
attacktype 2
invisible 1
move_on walk
no_pick 1
end

object rune_summon_fire_elemental
anim
rune_sum_fire.x11
rune_sum_fire.x11
mina
name Rune of Summoning
msg
A portal opens up, and screaming hordes pour
through!
endmsg
other_arch spell_summon_fire_elemental
face rune_sum_fire.x11
is_animated 0
cha 20
hp 1
maxhp 5
dam 90
speed 1
level 1
type 96
attacktype 2
invisible 1
move_on walk
no_pick 1
end

object rune_summon_water_elemental
anim
rune_sum_water.x11
rune_sum_water.x11
mina
name Rune of Summoning
msg
A portal opens up, and screaming hordes pour
through!
endmsg
other_arch spell_summon_water_elemental
face rune_sum_water.x11
is_animated 0
cha 20
hp 1
maxhp 5
dam 90
speed 1
level 1
type 96
attacktype 2
invisible 1
move_on walk
no_pick 1
end

