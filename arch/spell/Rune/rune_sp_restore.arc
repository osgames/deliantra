object rune_regenerate_spellpoints
anim
rune_sp_res.x11
rune_sp_res.x11
mina
name Rune of Magic Power
msg
You feel powerful!
endmsg
other_arch spell_regenerate_spellpoints
face rune_sp_res.x11
is_animated 0
hp 1
speed 1
level 1
type 96
invisible 1
move_on walk
no_pick 1
end

