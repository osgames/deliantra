object spell_rune_of_fire
inherit type_spell
name rune of fire
name_pl rune of fire
skill pyromancy
msg
Creates a rune that detonates in fire
when a creature triggers it.
endmsg
other_arch rune_fire
face spell_pyromancy.x11
sp 10
level 8
subtype 2
path_attuned 2
value 1
invisible 1
no_drop 1
casting_time 10
end

