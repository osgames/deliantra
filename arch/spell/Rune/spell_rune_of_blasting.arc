object spell_rune_of_blasting
inherit type_spell
name rune of blasting
name_pl rune of blasting
skill pyromancy
msg
Creates a rune that will hit the
creature that activates it with physical and
magical damage.
endmsg
other_arch rune_blast
face spell_pyromancy.x11
sp 18
level 14
subtype 2
path_attuned 512
value 1
invisible 1
no_drop 1
casting_time 15
end

