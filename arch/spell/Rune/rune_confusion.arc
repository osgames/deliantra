object rune_confusion
anim
rune_confusion.x11
confusion.x11
mina
name Rune of Confusion
msg
You detonate a Rune of Mass Confusion!
endmsg
other_arch spell_mass_confusion
face rune_confusion.x11
is_animated 0
cha 20
hp 2
speed 1
level 1
type 96
invisible 1
move_on walk
no_pick 1
end

