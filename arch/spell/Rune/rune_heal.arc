object rune_heal
anim
rune_heal.x11
rune_heal.x11
mina
name Rune of Heal
msg
You set off a Rune of Heal
endmsg
other_arch spell_heal
face rune_heal.x11
is_animated 0
cha 1
hp 1
speed 1
level 1
type 96
invisible 1
move_on walk
no_pick 1
end

