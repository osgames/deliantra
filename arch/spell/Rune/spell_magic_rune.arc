object spell_magic_rune
inherit type_spell
name magic rune
name_pl magic rune
skill sorcery
msg
Creates a special magical rune, which contains another spell.

When the magic rune is activated, that other spell is cast on the target.
endmsg
face generic_rune.x11
sp 5
level 12
subtype 2
path_attuned 2048
value 1
invisible 1
no_drop 1
casting_time 15
end

