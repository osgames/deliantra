object rune_paralysis
anim
rune_paralysis.x11
rune_paralysis.x12
mina
name Rune of Paralysis
msg
You detonate a Rune of Paralysis!
endmsg
other_arch spell_paralyze
face rune_paralysis.x11
is_animated 0
cha 20
hp 4
speed 1
level 1
type 96
invisible 1
move_on walk
no_pick 1
end

