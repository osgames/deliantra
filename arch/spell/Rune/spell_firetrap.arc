object spell_firetrap
inherit type_spell
name firetrap
name_pl firetrap
skill pyromancy
msg
Creates a special firetrap rune, which contains another spell.

The caster specifies which other spell
should be encapsulated in the firetrap spell
(it is not required that it be a fire spell).

Then when the firetrap is activated, that
other spell is cast on the target.
endmsg
face penta.x11
sp 5
level 9
subtype 2
path_attuned 2048
value 1
invisible 1
no_drop 1
casting_time 15
end

