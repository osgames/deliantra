object rune_small_lightning
anim
rune_lightning.x11
rune_lightning.x11
mina
name Rune of Lightning
msg
You set off a bolt of lightning!
endmsg
other_arch spell_sm_lightning
face rune_lightning.x11
is_animated 0
cha 20
hp 2
dam 90
speed 1
level 1
type 96
attacktype 10
invisible 1
move_on walk
no_pick 1
end

