object rune_ball_lightning
anim
rune_blightning.x11
rune_blightning.x11
mina
name Rune of Ball Lightning
msg
You detonate a Rune of Ball Lightning
endmsg
other_arch spell_ball_lightning
face rune_blightning.x11
is_animated 0
cha 20
hp 1
dam 1
speed 1
level 1
type 96
attacktype 10
invisible 1
move_on walk
no_pick 1
end

