object rune_transferrence
anim
rune_transfer.x11
rune_transfer.x11
mina
name Rune of Transferrence
msg
The Rune transfers power to you!
endmsg
other_arch spell_transference
face rune_transfer.x11
is_animated 0
hp 1
speed 1
level 1
type 96
invisible 1
move_on walk
no_pick 1
end

