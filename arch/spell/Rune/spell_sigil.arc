object spell_sigil
inherit type_spell
name sigil
name_pl sigil
skill evocation
msg
Creates a special sigil rune, which contains another spell.

When the sigil is activated, that other spell is cast on the target.
endmsg
face penta.x11
sp 5
level 12
subtype 2
path_attuned 2048
value 1
invisible 1
no_drop 1
casting_time 15
end

