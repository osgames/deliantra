object spell_rune_of_frost
inherit type_spell
name rune of frost
name_pl rune of frost
skill evocation
msg
Sets a magical trap which strikes with cold.
Anyone stepping on it will detonate it take freezing damage.
endmsg
other_arch rune_frost
face spell_evocation.x11
sp 10
level 12
subtype 2
path_attuned 2
value 1
invisible 1
no_drop 1
casting_time 10
end

