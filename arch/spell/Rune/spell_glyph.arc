object spell_glyph
inherit type_spell
name glyph
name_pl glyph
skill praying
msg
Creates a special firetrap rune, which contains another spell.

When the glyph is activated, the encapsulated
spell is cast on the target.
endmsg
face glyph.x11
grace 5
level 12
subtype 2
path_attuned 2048
value 1
invisible 1
no_drop 1
casting_time 15
end

