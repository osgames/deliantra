object rune_large_fireball
anim
rune_fireball.x11
rune_fireball.x11
mina
name Rune of Fireball
msg
You set off a large fireball!
endmsg
other_arch spell_large_fireball
face rune_fireball.x11
is_animated 0
cha 20
hp 1
dam 90
speed 1
level 1
type 96
attacktype 6
invisible 1
move_on walk
no_pick 1
end

object rune_medium_fireball
anim
rune_fireball.x11
rune_fireball.x11
mina
name Rune of Fireball
msg
You set off a fireball!
endmsg
other_arch spell_medium_fireball
face rune_fireball.x11
is_animated 0
cha 20
hp 1
dam 90
speed 1
level 1
type 96
attacktype 6
invisible 1
move_on walk
no_pick 1
end

