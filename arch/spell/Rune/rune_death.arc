object rune_death
anim
rune_death.x11
rune_death.x11
mina
name Rune of Death
msg
You detonate a Rune of Death!
endmsg
face rune_death.x11
is_animated 0
cha 20
hp 1
dam 400
speed 1
level 15
type 96
attacktype 131072
invisible 1
move_on walk
no_pick 1
end

