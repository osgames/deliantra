object rune_shock
anim
rune_shock.x11
rune_shock.x11
mina
name Rune of Shocking
msg
You detonate a Rune of Shocking
endmsg
face rune_shock.x11
is_animated 0
cha 20
hp 1
dam 40
speed 1
level 1
type 96
attacktype 10
invisible 1
move_on walk
no_pick 1
end

