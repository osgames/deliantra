object rune_blast
anim
rune_blast.x11
explosion.x11
mina
name Rune of Blasting
msg
You detonate a Rune of Blasting!
endmsg
face rune_blast.x11
is_animated 0
cha 20
hp 1
dam 90
speed 1
level 1
type 96
attacktype 3
invisible 1
move_on walk
no_pick 1
end

