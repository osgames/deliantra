object rune_antimagic
anim
rune_antimagic.x11
rune_antimagic.x11
mina
name Rune of Nullification
face rune_antimagic.x11
is_animated 0
cha 20
dam 1
level 0
type 96
attacktype 32770
invisible 1
move_on walk
no_pick 1
no_magic 1
end

