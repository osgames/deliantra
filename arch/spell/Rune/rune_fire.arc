object rune_burning_hands
anim
rune_fire.x11
fireball.x11
mina
name Rune of Burning Hands
msg
You detonate a Rune of Burning Hands!
endmsg
other_arch spell_burning_hands
face rune_fire.x11
is_animated 0
cha 20
hp 1
speed 1
level 1
type 96
invisible 1
move_on walk
no_pick 1
end

object rune_dragonbreath
anim
rune_fire.x11
fireball.x11
mina
name Rune of Dragon's Breath
msg
You detonate a Rune of Firebreath!
endmsg
other_arch spell_dragonbreath
face rune_fire.x11
is_animated 0
cha 20
hp 1
speed 1
level 1
type 96
invisible 1
move_on walk
no_pick 1
end

object rune_fire
anim
rune_fire.x11
fireball.x11
mina
name Rune of Fire
msg
You detonate a Rune of Fire!
endmsg
face rune_fire.x11
is_animated 0
cha 20
hp 1
dam 30
speed 1
level 1
type 96
attacktype 6
invisible 1
move_on walk
no_pick 1
end

