object rune_create_bomb
anim
rune_bomb.x11
rune_bomb.x11
mina
name Rune of Create Bomb
msg
RUN!  The timer's ticking!
endmsg
other_arch spell_create_bomb
face rune_bomb.x11
is_animated 0
cha 20
hp 1
dam 90
speed 1
level 1
type 96
attacktype 1
invisible 1
move_on walk
no_pick 1
end

