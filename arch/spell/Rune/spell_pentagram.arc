object spell_pentagram
inherit type_spell
name pentagram
name_pl pentagram
skill summoning
msg
Creates a special pentagram rune, which contains another spell.

When the pentagram is activated, the
encapsulated spell, which could be a
summoning spell, is cast.
endmsg
face penta.x11
sp 5
level 12
subtype 2
path_attuned 2048
value 1
invisible 1
no_drop 1
casting_time 15
end

