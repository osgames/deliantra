object spell_rune_of_magic_drain
inherit type_spell
name rune of magic drain
name_pl rune of magic drain
skill evocation
msg
Creates a rune that, when triggered,
drains the mana of the target. This drain in
mana will make it impossible for the target
to cast spells until it regains sufficient
mana to do so.
endmsg
other_arch rune_drain_magic
face spell_evocation.x11
sp 30
level 14
subtype 2
path_attuned 32768
value 1
invisible 1
no_drop 1
casting_time 15
end

