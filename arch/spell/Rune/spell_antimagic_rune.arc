object spell_antimagic_rune
inherit type_spell
name antimagic rune
name_pl antimagic rune
skill sorcery
msg
Places a magical trap onto the ground.
Anyone who steps there will spring this antimagic trap.
endmsg
other_arch rune_antimagic
face spell_sorcery.x11
sp 5
level 30
subtype 2
path_attuned 128
value 1
invisible 1
no_drop 1
casting_time 20
end

