object spell_rune_of_transference
inherit type_spell
name rune of transference
name_pl rune of transference
skill sorcery
msg
Creates a rune that, when triggered,
causes the recipient to gain mana. This can
be useful to make a mana recharging area.
endmsg
other_arch rune_transferrence
face spell_sorcery.x11
sp 12
maxsp 24
level 10
subtype 2
path_attuned 32768
value 1
invisible 1
no_drop 1
casting_time 15
end

