object generic_rune
anim
generic_rune.x11
generic_rune.x11
mina
name Magical Rune
face generic_rune.x11
is_animated 0
cha 20
hp 1
speed 1
level 1
type 96
attacktype 2
invisible 1
move_on walk
no_pick 1
end

