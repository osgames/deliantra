object spell_magic_missile
inherit type_spell
name magic missile
name_pl magic missile
skill sorcery
msg
Fires a weakly-tracking magical bolt.
It can actually track the target, but the turning is weak, even stupid:
at times these missiles will fly right into a wall.
endmsg
other_arch magic_missile
sound wn/magic-missile-1-miss
face spell_sorcery.x11
sp 1
maxsp 10
dam 9
level 4
subtype 11
attacktype 2
path_attuned 16
value 1
invisible 1
range 25
dam_modifier 1
no_drop 1
casting_time 3
end

