object magic_missile
anim
magic_miss.x31
magic_miss.x31
magic_miss.x51
magic_miss.x41
magic_miss.x61
magic_miss.x11
magic_miss.x71
magic_miss.x21
magic_miss.x81
mina
name magic missile
name_pl magic missile
face magic_miss.x31
is_animated 0
wc 2
speed 1
type 102
subtype 11
move_type fly_low
move_on walk fly_low
no_pick 1
is_turnable 1
end

