object power_crystal
anim
power_crystal.x11
power_crystal.x12
power_crystal.x13
power_crystal.x14
mina
name Glowing Crystal
name_pl Glowing Crystals
face power_crystal.x11
is_animated 1
maxsp 1000
type 118
materialname stone
value 10000000
weight 3
client_type 41
end

