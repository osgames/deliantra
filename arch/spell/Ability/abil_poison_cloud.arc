object abil_poison_cloud
inherit type_spell
name poison cloud ability
name_pl poison cloud ability
other_arch poisonbullet
sp 5
maxsp 8
food 5
dam 0
level 2
subtype 5
attacktype 1024
path_attuned 16
value 20
invisible 1
duration 4
range 4
dam_modifier 3
no_drop 1
casting_time 10
end

