object abil_medium_fireball
inherit type_spell
name medium fireball ability
name_pl medium fireball ability
other_arch firebullet
sp 10
maxsp 24
food 4
dam 8
level 3
subtype 5
attacktype 4
path_attuned 2
value 30
invisible 1
duration 4
range 6
dam_modifier 3
no_drop 1
casting_time 10
end

