object abil_create_frost_wall
inherit type_spell
name create frost wall ability
name_pl create frost wall ability
skill evocation
other_arch icestorm
sp 8
maxsp 8
dam 2
level 8
subtype 15
attacktype 16
path_attuned 2048
value 80
invisible 1
duration 240
range 2
duration_modifier 1
dam_modifier 3
move_block all
no_drop 1
casting_time 3
end

