object abil_large_icestorm
inherit type_spell
name large icestorm ability
name_pl large icestorm ability
other_arch icestorm
sp 13
maxsp 11
dam 4
level 12
subtype 7
attacktype 16
path_attuned 4
value 120
invisible 1
duration 2
range 14
range_modifier 5
dam_modifier 3
no_drop 1
casting_time 4
end

