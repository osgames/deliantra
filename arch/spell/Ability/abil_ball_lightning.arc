object abil_ball_lightning
inherit type_spell
name ball lightning ability
name_pl ball lightning ability
other_arch ball_lightning
sp 10
maxsp 15
dam 8
level 9
subtype 35
attacktype 8
path_attuned 8
value 90
invisible 1
duration 40
duration_modifier 1
dam_modifier 3
no_drop 1
casting_time 3
end

