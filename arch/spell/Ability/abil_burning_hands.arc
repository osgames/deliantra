object abil_burning_hands
inherit type_spell
name burning hands ability
name_pl burning hands ability
other_arch firebreath
sound wn/fire
sp 5
maxsp 9
dam 4
level 1
subtype 7
attacktype 4
path_attuned 2
value 10
invisible 1
duration 2
range 5
range_modifier 4
dam_modifier 4
no_drop 1
casting_time 5
end

