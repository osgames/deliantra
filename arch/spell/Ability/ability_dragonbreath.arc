# Basically same as the spell, but does a little less damage.
#
object ability_dragonbreath
inherit type_spell
name dragonbreath ability
name_pl dragonbreath ability
skill pyromancy
other_arch firebreath
sp 13
maxsp 11
dam 4
level 12
subtype 7
attacktype 4
path_attuned 2
value 120
invisible 1
duration 2
range 7
range_modifier 5
dam_modifier 3
no_drop 1
# Note this is not magical!
casting_time 5
end

