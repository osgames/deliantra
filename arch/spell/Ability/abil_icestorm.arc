object abil_icestorm
inherit type_spell
name icestorm ability
name_pl icestorm ability
other_arch icestorm
sp 5
maxsp 9
dam 4
level 1
subtype 7
attacktype 16
path_attuned 4
value 10
invisible 1
duration 2
range 6
range_modifier 5
dam_modifier 3
no_drop 1
casting_time 3
end

