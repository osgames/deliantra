object abil_slow
inherit type_spell
name slow ability
name_pl slow ability
other_arch slow
sp 5
maxsp 20
dam 5
level 1
subtype 7
attacktype 2048
path_attuned 0
value 10
invisible 1
duration 2
range 5
range_modifier 4
dam_modifier 0
no_drop 1
casting_time 10
end

