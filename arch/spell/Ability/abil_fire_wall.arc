object abil_create_fire_wall
inherit type_spell
name create fire wall ability
name_pl create fire wall ability
other_arch firebreath
sp 5
maxsp 8
dam 4
level 6
subtype 15
attacktype 4
path_attuned 2048
value 60
invisible 1
duration 260
range 2
duration_modifier 1
dam_modifier 3
move_block all
no_drop 1
casting_time 4
end

