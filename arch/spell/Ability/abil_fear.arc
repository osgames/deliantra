object abil_fear
inherit type_spell
name fear ability
name_pl fear ability
other_arch fear
sp 6
maxsp 12
dam 5
level 4
subtype 7
attacktype 16384
path_attuned 1024
value 40
invisible 1
duration 2
range 4
range_modifier 3
dam_modifier 0
no_drop 1
casting_time 5
end

