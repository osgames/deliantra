object abil_frostbolt
inherit type_spell
name frostbolt ability
name_pl frostbolts ability
other_arch frostbolt
sp 12
maxsp 48
dam 12
level 3
subtype 4
attacktype 16
path_attuned 4
value 30
invisible 1
duration 11
range 12
dam_modifier 4
no_drop 1
casting_time 3
end

