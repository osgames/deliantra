# this should only be found in potions!
object spell_regenerate_spellpoints
inherit type_spell
name regenerate spellpoints
name_pl regenerate spellpoints
skill none
msg
Regenerates your magic powers H<i.e. it restores
your mana/spell points>.
endmsg
other_arch healing
sound wn/heal
sp 0
level 99
subtype 21
path_attuned 256
value 1
invisible 1
last_sp 9999
no_drop 1
casting_time 0
end

