object spell_minor_healing
inherit type_spell
name minor healing
name_pl minor healing
skill praying
msg
This prayer heals minor wounds on either the beseecher or the target.
endmsg
other_arch healing
sound wn/heal
face spell_praying.x11
hp 1
grace 4
dam 1
level 1
subtype 21
path_attuned 256
value 1
invisible 1
no_drop 1
casting_time 3
end

