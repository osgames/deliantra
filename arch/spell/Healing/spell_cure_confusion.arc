object spell_cure_confusion
inherit type_spell
name cure confusion
name_pl cure confusion
skill praying
msg
This prayer removes confusion from your character.
endmsg
other_arch healing
sound wn/heal
face spell_praying.x11
grace 8
level 17
subtype 21
attacktype 32
path_attuned 256
value 1
invisible 1
no_drop 1
casting_time 15
end

