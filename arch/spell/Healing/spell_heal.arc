object spell_heal
inherit type_spell
name heal
name_pl heal
skill praying
msg
This prayer heals I<all> wounds on either the beseecher or the target.
However, it will not cure other problems, such as blindness or disease.
endmsg
other_arch healing
sound wn/heal
face spell_praying.x11
hp 0
grace 50
dam 9999
level 30
subtype 21
path_attuned 256
value 1
invisible 1
no_drop 1
casting_time 12
end

