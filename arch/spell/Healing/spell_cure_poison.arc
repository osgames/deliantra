object spell_cure_poison
inherit type_spell
name cure poison
name_pl cure poison
skill praying
msg
This prayer cures I<all> poison effect of your character.
endmsg
other_arch healing
sound wn/heal
face spell_praying.x11
grace 7
level 14
subtype 21
attacktype 1024
path_attuned 256
value 1
invisible 1
no_drop 1
casting_time 10
end

