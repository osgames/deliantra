object spell_restoration
inherit type_spell
name restoration
name_pl restoration
skill praying
msg
Heals all damage, confusion,
poison, and disease, and also provides a full
stomach. In other words, it restores about
any negative effect you might suffer from.
of food to the recipient.

The only thing it does not take care of is actual death.
endmsg
other_arch healing
sound wn/heal
face spell_praying.x11
grace 80
food 999
dam 9999
level 40
subtype 21
attacktype 37749792
path_attuned 256
value 1
invisible 1
no_drop 1
casting_time 15
end

