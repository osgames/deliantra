object spell_medium_healing
inherit type_spell
name medium healing
name_pl medium healing
skill praying
msg
This prayer heals medium wounds on either the beseecher or the target.
endmsg
other_arch healing
sound wn/heal
face spell_praying.x11
hp 3
grace 7
dam 2
level 13
subtype 21
path_attuned 256
value 1
invisible 1
no_drop 1
casting_time 6
end

