object spell_cure_blindness
inherit type_spell
name cure blindness
name_pl cure blindness
skill praying
msg
This prayer removes any magical blindness effect from your character.
endmsg
other_arch healing
sound wn/heal
face spell_praying.x11
grace 30
level 11
subtype 21
attacktype 4194304
path_attuned 256
value 1
invisible 1
no_drop 1
casting_time 10
end

