object spell_major_healing
inherit type_spell
name major healing
name_pl major healing
skill praying
msg
This prayer heals major wounds on either the beseecher or the target.
endmsg
other_arch healing
sound wn/heal
face spell_praying.x11
hp 5
grace 10
dam 4
level 20
subtype 21
path_attuned 256
value 1
invisible 1
no_drop 1
casting_time 9
end

