object spell_cure_disease
inherit type_spell
name cure disease
name_pl cure disease
skill praying
msg
This prayer cures I<all> diseases from your character.
endmsg
other_arch healing
sound wn/heal
face spell_praying.x11
grace 30
level 28
subtype 21
attacktype 33554432
path_attuned 256
value 1
invisible 1
no_drop 1
casting_time 10
end

