object spell_xray
inherit type_spell
name xray
name_pl xray
skill sorcery
msg
Allows the caster to see through one layer of wall... for a while.
endmsg
face spell_sorcery.x11
sp 20
maxsp 36
level 10
subtype 24
path_attuned 8192
value 1
invisible 1
duration 500
duration_modifier 4
no_drop 1
xrays 1
casting_time 20
end

