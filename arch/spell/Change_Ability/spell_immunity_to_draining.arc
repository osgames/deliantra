# Obsolete spell - here for backward compatibility
object spell_immunity_to_draining
inherit type_spell
name immunity to draining
name_pl immunity to draining
skill praying
msg
Grants a temporary, and total, immunity to draining attacks.
endmsg
grace 75
maxgrace 50
level 50
subtype 24
resist_drain 100
path_attuned 0
value 1
invisible 1
duration 500
duration_modifier 4
no_drop 1
casting_time 10
end

