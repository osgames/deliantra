object spell_protection_from_cancellation
inherit type_spell
name protection from cancellation
name_pl protection from cancellation
skill praying
msg
This prayer gives you increased protection from cancellation.
endmsg
face spell_praying.x11
grace 30
maxgrace 40
level 16
subtype 24
resist_cancellation 40
path_attuned 1
value 1
invisible 1
duration 500
duration_modifier 4
dam_modifier 3
no_drop 1
casting_time 10
end

