object spell_charisma
inherit type_spell
name charisma
name_pl charisma
skill sorcery
msg
Temporarily increase your charisma by some amount.
The amount depends on your current charisma and spell level.
endmsg
face spell_sorcery.x11
cha 1
sp 12
level 16
subtype 24
path_attuned 32
value 1
invisible 1
duration 500
duration_modifier 4
no_drop 1
casting_time 20
end

