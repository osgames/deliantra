object spell_protection_from_poison
inherit type_spell
name protection from poison
name_pl protection from poison
skill praying
msg
This prayer gives you increased protection from poison and poison attacks.
endmsg
face spell_praying.x11
grace 20
maxgrace 40
level 16
subtype 24
resist_poison 50
path_attuned 1
value 1
invisible 1
duration 500
duration_modifier 4
dam_modifier 4
no_drop 1
casting_time 10
end

