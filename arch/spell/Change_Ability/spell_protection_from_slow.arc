object spell_protection_from_slow
inherit type_spell
name protection from slow
name_pl protection from slow
skill praying
msg
This prayer gives you increased protection from slow attacks.
endmsg
face spell_praying.x11
grace 20
maxgrace 40
level 17
subtype 24
resist_slow 50
path_attuned 1
value 1
invisible 1
duration 500
duration_modifier 4
dam_modifier 4
no_drop 1
casting_time 10
end

