object spell_rage
inherit type_spell
name rage
name_pl rage
skill praying
msg
The rage prayer is a special prayer of Gorokh and of Ruggilli.
The beseecher is possessed by the rage of his god,
which enhances his strength, speed, endurance, and regeneration.

Granted at medium level.
endmsg
face spell_praying.x11
str 1
dex 1
con 1
hp 1
grace 5
maxgrace 4
exp 3
wc 2
ac 2
level 8
subtype 24
resist_physical 20
path_attuned 131072
value 1
invisible 1
duration 500
duration_modifier 4
dam_modifier 5
no_drop 1
casting_time 10
end

