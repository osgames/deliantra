object spell_protection_from_confusion
inherit type_spell
name protection from confusion
name_pl protection from confusion
skill praying
msg
This prayer gives you increased protection from confusion.
endmsg
face spell_praying.x11
grace 20
maxgrace 40
level 17
subtype 24
resist_confusion 50
path_attuned 1
value 1
invisible 1
duration 500
duration_modifier 4
dam_modifier 4
no_drop 1
casting_time 10
end

