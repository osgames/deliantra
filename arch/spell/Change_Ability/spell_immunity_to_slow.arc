# Obsolete spell - here for backward compatibility
object spell_immunity_to_slow
inherit type_spell
name immunity to slow
name_pl immunity to slow
skill praying
msg
Grants a temporary, and total, immunity to slow attacks.
endmsg
grace 60
maxgrace 50
level 50
subtype 24
resist_slow 100
path_attuned 0
value 1
invisible 1
duration 500
duration_modifier 4
no_drop 1
casting_time 10
end

