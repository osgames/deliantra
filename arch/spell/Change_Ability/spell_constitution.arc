object spell_constitution
inherit type_spell
name constitution
name_pl constitution
skill sorcery
msg
Temporarily increase your constitution by some amount.
The amount depends on your current constitution and the spell level.
endmsg
face spell_sorcery.x11
con 1
sp 15
level 16
subtype 24
path_attuned 32
value 1
invisible 1
duration 500
duration_modifier 4
no_drop 1
casting_time 20
end

