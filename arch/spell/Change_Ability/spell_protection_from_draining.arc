object spell_protection_from_draining
inherit type_spell
name protection from draining
name_pl protection from draining
skill praying
msg
Reduces the
experience loss when the character is hit
with a draining attack. The character will
still lose some experience, but not as much
as without it being in effect.
endmsg
face spell_praying.x11
grace 25
maxgrace 40
level 19
subtype 24
resist_drain 35
path_attuned 1
value 1
invisible 1
duration 500
duration_modifier 4
dam_modifier 3
no_drop 1
casting_time 10
end

