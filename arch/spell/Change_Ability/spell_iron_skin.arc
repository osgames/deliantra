object spell_iron_skin
inherit type_spell
name iron skin
name_pl iron skin
skill praying
msg
Toughens the skin of the recipient,
making them harder to hit and reducing the
amount of damage they take from physical
attacks.

It does not provide any additional
protection to non-physical attacks.
endmsg
face spell_praying.x11
grace 8
maxgrace 8
ac 2
level 16
subtype 24
resist_physical 20
path_attuned 33
value 1
invisible 1
duration 500
duration_modifier 4
dam_modifier 5
no_drop 1
casting_time 10
end

