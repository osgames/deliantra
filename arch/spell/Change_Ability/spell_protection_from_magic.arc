object spell_protection_from_magic
inherit type_spell
name protection from magic
name_pl protection from magic
skill praying
msg
Reduces damage from
magical attacks or spells. There are a few
spells that do not hit with a magical attack;
this spell does nothing to reduce those
effects.
endmsg
face spell_praying.x11
grace 30
maxgrace 40
level 20
subtype 24
resist_magic 30
path_attuned 1
value 1
invisible 1
duration 500
duration_modifier 4
dam_modifier 3
no_drop 1
casting_time 12
end

