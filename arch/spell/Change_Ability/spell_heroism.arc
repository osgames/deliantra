object spell_heroism
inherit type_spell
name heroism
name_pl heroism
skill none
msg
Increases none, some or all of these stats:

dexterity, strength, constitution, speed, wc, dam.
endmsg
str 1
dex 1
con 1
sp 50
level 10
subtype 24
path_attuned 32
value 1
invisible 1
duration 500
duration_modifier 4
no_drop 1
casting_time 10
end

