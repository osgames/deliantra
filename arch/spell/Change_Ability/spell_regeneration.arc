object spell_regeneration
inherit type_spell
name regeneration
name_pl regeneration
skill praying
msg
This prayer causes the target to heal more quickly.
endmsg
face spell_praying.x11
hp 1
grace 15
maxgrace 10
level 18
subtype 24
path_attuned 128
value 1
invisible 1
duration 500
duration_modifier 6
dam_modifier 5
no_drop 1
casting_time 10
end

