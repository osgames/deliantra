object spell_dexterity
inherit type_spell
name dexterity
name_pl dexterity
skill sorcery
msg
Temporarily increase your dexterity by some amount.
The amount depends on your current dexterity and your strength as a magician.
endmsg
face spell_sorcery.x11
dex 1
sp 12
level 16
subtype 24
path_attuned 32
value 1
invisible 1
duration 500
duration_modifier 4
no_drop 1
casting_time 20
end

