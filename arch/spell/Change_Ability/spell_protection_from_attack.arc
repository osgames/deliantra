object spell_protection_from_attack
inherit type_spell
name protection from attack
name_pl protection from attack
skill praying
msg
Creates a powerful
force that protects the recipient from
physical damage.

While the effect stacks with armour the characters are wearing, it will be
weaker in that case.
endmsg
face spell_praying.x11
grace 50
maxgrace 40
level 19
subtype 24
resist_physical 40
path_attuned 1
value 1
invisible 1
duration 500
duration_modifier 4
dam_modifier 3
no_drop 1
casting_time 15
end

