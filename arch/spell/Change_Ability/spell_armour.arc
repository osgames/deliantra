object spell_armour
inherit type_spell
name armour
name_pl armour
skill evocation
msg
Creates fields of force around the
player, reducing the amount of damage the
character takes from physical attacks. It
does not provide any additional protection to
non-physical attacks, however.
endmsg
face spell_evocation.x11
sp 8
maxsp 8
ac 2
level 6
subtype 24
resist_physical 20
path_attuned 33
value 1
invisible 1
duration 500
duration_modifier 4
dam_modifier 5
no_drop 1
casting_time 10
end

