# Obsolete spell - here for backward compatibility
object spell_immunity_to_magic
inherit type_spell
name immunity to magic
name_pl immunity to magic
skill praying
msg
Grants a temporary, and total, immunity to magical attacks.
endmsg
grace 150
maxgrace 50
level 50
subtype 24
resist_magic 100
path_attuned 0
value 1
invisible 1
duration 500
duration_modifier 4
no_drop 1
casting_time 30
end

