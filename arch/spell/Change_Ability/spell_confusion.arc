object spell_confusion
inherit type_spell
name confusion
name_pl confusion
skill sorcery
msg
Adds confusion to your attack types!

Very useful to kill enemies with immunity to physical attacks.
endmsg
other_arch confusion
face spell_sorcery.x11
sp 10
level 14
subtype 24
attacktype 32
path_attuned 1024
value 1
invisible 1
duration 500
duration_modifier 4
no_drop 1
casting_time 10
end

