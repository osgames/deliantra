object spell_protection_from_electricity
inherit type_spell
name protection from electricity
name_pl protection from electricity
skill praying
msg
This prayer gives you increased protection from electric damage.
endmsg
face spell_praying.x11
grace 15
maxgrace 40
level 16
subtype 24
resist_electricity 30
path_attuned 1
value 1
invisible 1
duration 500
duration_modifier 4
dam_modifier 3
no_drop 1
casting_time 10
end

