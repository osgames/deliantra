# Obsolete spell - here for backward compatibility
object spell_defense
inherit type_spell
name defense
name_pl defense
skill praying
msg
The defense prayer, a special prayer of Lythander,
protects the beseecher from many things all at once.

Granted at medium level.
endmsg
face spell_praying.x11
grace 75
maxgrace 50
level 40
subtype 24
resist_physical 30
resist_magic 30
resist_fire 30
resist_electricity 30
resist_cold 30
resist_confusion 30
resist_acid 30
resist_drain 30
resist_ghosthit 30
resist_poison 30
resist_slow 30
resist_paralyze 30
resist_turn_undead 30
resist_fear 30
resist_deplete 30
resist_death 30
resist_blind 30
path_attuned 1
value 1
invisible 1
duration 500
duration_modifier 4
no_drop 1
casting_time 30
end

