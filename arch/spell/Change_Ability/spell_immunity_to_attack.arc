# Obsolete spell - here for backward compatibility
object spell_immunity_to_attack
inherit type_spell
name immunity to attack
name_pl immunity to attack
skill praying
msg
Grants a temporary, and total, immunity to physical attacks.
endmsg
grace 170
maxgrace 50
level 66
subtype 24
resist_physical 100
path_attuned 0
value 1
invisible 1
duration 500
duration_modifier 4
no_drop 1
casting_time 50
end

