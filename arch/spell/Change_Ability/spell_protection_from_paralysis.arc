object spell_protection_from_paralysis
inherit type_spell
name protection from paralysis
name_pl protection from paralysis
skill praying
msg
This prayer gives you increased protection from paralysis attacks.
But it won't get you a perfect protection, be careful, you can still
be paralysed!
endmsg
face spell_praying.x11
grace 20
maxgrace 40
level 18
subtype 24
resist_paralyze 50
path_attuned 1
value 1
invisible 1
duration 500
duration_modifier 4
dam_modifier 4
no_drop 1
casting_time 10
end

