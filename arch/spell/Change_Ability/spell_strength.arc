object spell_strength
inherit type_spell
name strength
name_pl strength
skill sorcery
msg
Increase your strength by some amount.
The amount depends on your current strength and the spell level.
endmsg
face spell_sorcery.x11
str 1
sp 10
level 16
subtype 24
path_attuned 32
value 1
invisible 1
duration 500
duration_modifier 4
no_drop 1
casting_time 20
end

