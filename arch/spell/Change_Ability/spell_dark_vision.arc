object spell_dark_vision
inherit type_spell
name dark vision
name_pl dark vision
skill sorcery
msg
Lets you see in dark places without light. Unlike torches
or magical light, you don't atttract monsters this way.
endmsg
face spell_sorcery.x11
sp 10
maxsp 30
level 30
subtype 24
path_attuned 8192
value 1
invisible 1
duration 500
duration_modifier 4
no_drop 1
can_see_in_dark 1
casting_time 12
end

