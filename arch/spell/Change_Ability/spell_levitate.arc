object spell_levitate
inherit type_spell
name levitate
name_pl levitate
skill sorcery
msg
Lets you levitate above the floor for a while, which makes it impossible
for you to trigger any floor traps or fall into holes.

This spell is needed in some mazes to pass hole traps or other trapped areas.

When you are levitating, you can't fetch items from the ground, so be
careful when you need to grab food for example, as there is no way to
cancel the levitiation before it normally runs out (well, except suicide).
endmsg
face spell_sorcery.x11
sp 10
level 14
subtype 24
path_attuned 0
value 1
invisible 1
duration 500
duration_modifier 4
move_type fly_low
no_drop 1
casting_time 10
end

