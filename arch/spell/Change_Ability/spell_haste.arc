object spell_haste
inherit type_spell
name haste
name_pl haste
skill none
msg
This prayer increases the speed of the beseecher or target.
endmsg
sp 50
exp 3
level 12
subtype 24
path_attuned 32
value 1
invisible 1
duration 500
duration_modifier 4
no_drop 1
casting_time 10
end

