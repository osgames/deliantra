object spell_conflict
inherit type_spell
name conflict
name_pl conflict
skill praying
msg
This special prayer of Lythander will confuse and distract any monsters nearby,
setting them to attack whatever is nearest, be it friend or foe.

Granted at medium level.
endmsg
other_arch detect_magic
sound ss/wah1
face spell_praying.x11
grace 50
maxgrace 10
level 10
subtype 34
path_attuned 1024
value 1
invisible 1
range 5
range_modifier 10
no_drop 1
berserk 1
casting_time 10
end

