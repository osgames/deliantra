object spell_pacify
inherit type_spell
name pacify
name_pl pacify
skill praying
msg
Target starts to be peaceful and stops attacking caster.
endmsg
other_arch detect_magic
sound ss/wah1
face spell_praying.x11
grace 10
maxgrace 25
level 9
subtype 34
path_attuned 1024
value 1
invisible 1
range 1
range_modifier 5
unaggressive 1
no_drop 1
casting_time 2
end

