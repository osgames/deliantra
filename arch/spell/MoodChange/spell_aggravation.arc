object spell_aggravation
inherit type_spell
name aggravation
name_pl aggravation
skill none
msg
Makes creatures around you more upset so they will attack.
endmsg
sound ss/wah1
sp 1
level 1
subtype 34
path_attuned 0
value 1
invisible 1
range 25
monster 1
undead 1
no_drop 1
casting_time 1
end

