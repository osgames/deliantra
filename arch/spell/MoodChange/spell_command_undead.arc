object spell_command_undead
inherit type_spell
name command undead
name_pl command undead
race undead
skill praying
msg
Turns undead creatures in the area into pets. This area it affects depends
on the praying level.

You can think of it as a B<charm monsters> for undead creatures.
It is very useful (of course)!
endmsg
other_arch detect_magic
sound ss/wah1
face spell_praying.x11
grace 12
maxgrace 25
level 15
subtype 34
path_attuned 1024
value 1
invisible 1
range 1
range_modifier 3
undead 1
no_drop 1
no_attack 1
casting_time 10
end

