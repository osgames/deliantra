object spell_siren_call
inherit type_spell
name siren call
name_pl siren call
skill praying
msg
This prayer will stop creatures from attacking you.
endmsg
other_arch charm_aura
sound ss/wah1
face spell_praying.x11
grace 20
maxgrace 27
level 13
subtype 34
path_attuned 1024
value 1
invisible 1
range 3
range_modifier 3
no_drop 1
no_attack 1
casting_time 10
end

