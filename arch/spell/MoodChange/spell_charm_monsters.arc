object spell_charm_monsters
inherit type_spell
name charm monsters
name_pl charm monsters
skill summoning
msg
This prayer may convert monsters in the area into pets. Its radius will expand with the
casters level. The level of the creature has to be at least the level of the
summoning skill to be able to charm it. Having a lots of charisma can help a lot!
endmsg
other_arch charm_aura
sound ss/wah1
face spell_summoner.x11
sp 20
maxsp 27
level 10
subtype 34
path_attuned 1024
value 1
invisible 1
range 1
range_modifier 3
no_drop 1
no_attack 1
casting_time 10
end

