object spell_poison_fog
inherit type_spell
name poison fog
name_pl poison fog
skill praying
msg
This special prayer of Gnarg summons a cloud of poisonous vapors which
pursues and poisons enemies of the player.

Granted at low level.
endmsg
other_arch poison_fog
face spell_praying.x11
grace 15
maxgrace 10
dam 10
level 15
subtype 35
attacktype 1024
path_attuned 131072
value 1
invisible 1
duration 5
duration_modifier 4
dam_modifier 5
no_drop 1
casting_time 10
end

