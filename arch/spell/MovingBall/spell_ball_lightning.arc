object spell_ball_lightning
inherit type_spell
name ball lightning
name_pl ball lightning
skill evocation
msg
Creates a ball of electrical
energy. This ball heads in the general direction it
was targetted at, and if it encounters a monster, it
sticks to the monster, dancing around and
shocking them continuously. The ball
has a limited duration.
endmsg
other_arch ball_lightning
face spell_evocation.x11
sp 10
maxsp 15
dam 8
level 35
subtype 35
attacktype 10
path_attuned 8
value 1
invisible 1
duration 40
duration_modifier 1
dam_modifier 3
no_drop 1
casting_time 10
end

