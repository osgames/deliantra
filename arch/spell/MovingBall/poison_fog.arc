object poison_fog
anim
poisoncloud.x11
poisoncloud.x12
poisoncloud.x13
mina
name poison fog
other_arch poison_fog_sign
face poisoncloud.x11
is_animated 1
wc -30
speed 0.2
type 102
subtype 35
move_type fly_low
move_on walk
no_pick 1
is_used_up 1
end

object poison_fog_sign
anim
poisoncloud.x11
poisoncloud.x12
poisoncloud.x13
mina
name poison fog
face poisoncloud.x11
food 30
speed 1
type 98
move_type fly_low
no_pick 1
is_used_up 1
blocksview 1
end

