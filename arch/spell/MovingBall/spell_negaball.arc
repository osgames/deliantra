object spell_negaball
inherit type_spell
name negative energy ball
name_pl negative energy balls
skill evocation
msg
Creates a ball of
negative energy. This ball heads in the
direction it is targetted at, and if it encounters a
monster, it sticks to the monster, draining
the monster and doing cold damage as it does
so. The spell has a limited duration.
endmsg
other_arch negaball
face spell_evocation.x11
sp 10
maxsp 15
dam 8
level 60
subtype 35
attacktype 144
path_attuned 8
value 1
invisible 1
duration 40
duration_modifier 1
dam_modifier 3
no_drop 1
casting_time 11
end

