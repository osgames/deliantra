object spell_divine_shock
inherit type_spell
name divine shock
name_pl divine shock
skill praying
msg
The divine shock prayer is very like ball lightning,
however, the lightning balls have the extra sting of having
godpower behind them.

This is a special prayer of Sorig granted at low level.
endmsg
other_arch divine_shock
face spell_praying.x11
grace 3
maxgrace 5
dam 1
level 8
subtype 35
attacktype 1048584
path_attuned 131072
path_repelled 8
value 1
invisible 1
duration 1
duration_modifier 4
dam_modifier 3
no_drop 1
casting_time 10
end

