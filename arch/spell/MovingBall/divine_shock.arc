object divine_shock
anim
ball_lightning.x11
ball_lightning.x12
ball_lightning.x13
ball_lightning.x14
mina
name divine shock
face ball_lightning.x11
is_animated 1
wc -30
speed 1
type 102
subtype 35
glow_radius 2
move_type fly_low
move_on walk fly_low
no_pick 1
is_used_up 1
end

