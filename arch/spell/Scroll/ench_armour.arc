object ench_armour
name Enchant Armour
name_pl Enchant Armours
race scrolls
face scroll.x11
sp 1
nrof 1
type 123
materialname paper
value 9500
weight 200
client_type 1011
identified 1
need_an 1
msg
This scroll possesses magical powers to enchant armour.
H<This scroll can be used to enchant any of your armour. You first need to mark
a piece of armour in your inventory and then read this scroll to enhance it.>
endmsg
end

