object improve_damage
name Improve Weapon Damage
name_pl Improve Weapon Damage
race scrolls
face scroll.x11
sp 2
nrof 1
type 124
materialname paper
value 10000
weight 200
client_type 1016
identified 1
msg
This scroll will magically improve your weapon to do more damange.
H<You first need to prepare a weapon using a Prepare Weapon scroll.
Then you can read this scroll to improve it.>
endmsg
end

