object scroll_empty
name empty spell scroll
name_pl empty spell scrolls
race scrolls
skill use magic item
msg
Nothing is written in it.
endmsg
other_arch scroll_new
face scroll_empty.x11
nrof 1
type 110
materialname paper
value 1
weight 400
client_type 660
end

object scroll_new
name scroll
name_pl scrolls
race scrolls
skill use magic item
face scroll.x11
nrof 1
type 111
materialname paper
value 1
weight 400
randomitems scroll
client_type 661
end

