object prepare_weapon
name Prepare Weapon
name_pl Prepare Weapons
slaying gem
face scroll.x11
sp 1
nrof 1
type 124
materialname paper
value 10000
weight 200
client_type 1016
identified 1
msg
This scroll will magically prepare a weapon for improvements.

H<This is done in two steps: first you have to prepare your weapon for the
desired number of enchantments, then you apply the enchanments (weapons
remember how many times they can be enchanted further).

To prepare a weapon, wield it and mark (e.g. using the popup menu in the
inventory) it. Then, as a sacrifice, drop some diamonds on the floor then read
the Prepare Weapon scroll. The square root of the total number of diamonds
sacrificed this way determines the number of enchantments the weapon accepts:
one diamond for one enchanment, nine diamonds for three enchantments, 100
diamonds for ten enchantments, and so on.

Remember, once you prepare the weapon, it can only be wielded by you.>
endmsg
end
