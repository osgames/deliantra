object improve_weight
name Lower Weapon Weight
name_pl Lower Weapon Weights
race scrolls
face scroll.x11
sp 3
nrof 1
type 124
materialname paper
value 10000
weight 200
client_type 1016
identified 1
msg
This scroll will magically make your sword lighter.
H<This "improves" the weight of your weapon: each scroll reduces the weight by
one fifth (20%). It will not, however, create weightless weapons. To use this
scroll you first need to prepare a weapon using a Prepare Weapon scroll. Then
you can read this scroll to improve the weapon.>
endmsg
end

