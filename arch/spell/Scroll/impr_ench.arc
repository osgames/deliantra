object improve_enchantment
name Enchant Weapon
name_pl Enchant Weapons
race scrolls
face scroll.x11
sp 4
nrof 1
type 124
materialname paper
value 10000
weight 200
client_type 1016
identified 1
msg
This scroll will improve your weapon magically.
H<You first need to prepare a weapon using a Prepare Weapon scroll.
Then you can read this scroll to improve it's magic bonus.>
endmsg
end

