object improve_int
name Improve Intelligence Bonus
name_pl Improve Intelligence Bonus
race scrolls
slaying spectrolite
face scroll.x11
sp 10
nrof 1
type 124
materialname paper
value 10000
weight 200
client_type 1016
identified 1
msg
This scroll will magically make your weapon to support your intelligence.
But to accomplis that you will need some special mineral.

H<This scroll helps to improve the intelligence bonus of weapons. You
will need spectrolite minerals to use it. You first need to prepare a weapon using
a Prepare Weapon scroll. Then you can read this scroll to improve the weapon.

The number of minerals you need is calculated this way: You sum up the stat
improvements the weapon already gives. For example, a weapon that has Str+1,
Int-1, Wis+2 will add up to: (1 + 2) - 1 = 2.

Then you need to doulbe the sum you got:  2 * 2 = 4.

The end result is the number of minerals you need. However, the minimum number
of minerals you need is 2. You then have to drop the minals on the floor, make
sure your weapon is marked, and then read this scroll.>
endmsg
end

