object sanctuary
name sanctuary
other_arch sanctuary_mark
face burnout.x11
dam 1
speed 1
type 102
subtype 46
attacktype 524288
invisible 1
no_pick 1
no_drop 1
end

object sanctuary_mark
name sanctuary
face counterspell.x11
food 2
speed 1
type 98
move_type fly_low
no_pick 1
is_used_up 1
end

