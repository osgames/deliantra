object spell_sanctuary
inherit type_spell
name sanctuary
name_pl sanctuary
skill praying
msg
Creates a counterspell aura around you
which will block spells from your enemy and will follow
your movement.
endmsg
other_arch sanctuary
face spell_praying.x11
grace 30
maxgrace 20
level 28
subtype 46
attacktype 524288
path_attuned 1
value 1
invisible 1
duration 100
duration_modifier 4
no_drop 1
casting_time 10
end

