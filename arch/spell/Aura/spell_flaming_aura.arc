object spell_flaming_aura
inherit type_spell
name flaming aura
name_pl flaming aura
skill praying
msg
Creates an aura of fire around you,
which will follow your movement will hurt any creatures within it.
endmsg
other_arch flaming_aura
face spell_praying.x11
grace 5
maxgrace 20
dam 6
level 16
subtype 46
attacktype 4
path_attuned 2
value 1
invisible 1
duration 100
duration_modifier 4
dam_modifier 25
no_drop 1
casting_time 10
end

