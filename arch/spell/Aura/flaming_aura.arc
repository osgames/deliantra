object flaming_aura
name Flaming Aura
other_arch flaming_aura_mark
face burnout.x11
dam 1
speed 1
type 102
subtype 46
attacktype 4
invisible 1
no_pick 1
no_drop 1
end

object flaming_aura_mark
name Flaming Aura
face fireball.x11
food 2
speed 1
type 98
move_type fly_low
no_pick 1
is_used_up 1
end

