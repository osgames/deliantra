object class_monster
randomitems standard
monster 1
end

object class_undead_monster
inherit class_monster
race undead
sound_destroy wn/wail
undead 1
end

object class_item
nrof 1
weight 1000000000
end

object class_vein_extractor
inherit type_ranged_item
race mining
dam 1
client_type 451
body_arm -2
speed 1
end

object type_player
type 1
body_range 1
body_skill 1
body_combat 1
body_shield 1
end

object type_rod
inherit type_ranged_item
skill use magic item
type 3
end

object type_book
skill literacy
sound elmex/paper_crumble
type 8
materialname paper
end

object type_vein
face vein.x11
race mining
food 1
ac 50
type 10
invisible 1
end

object type_ranged
type 11
body_range -1
end

object type_ranged_item
inherit type_ranged,class_item
end

object type_bow
inherit type_ranged_item
type 14
end

object type_weapon
type 15
body_combat -1
end

object type_locked_door
inherit type_door
sound_destroy misc/doorkickopen
type 20
end

object type_door
name door
sound_destroy open_door
hp 400
exp 1
ac 10
level 1
type 23
randomitems door
move_block all
alive 1
no_pick 1
end

object type_shield
type 33
body_shield -1
end

object type_horn
inherit type_ranged_item
skill use magic item
type 35
end

object type_skill
type 43
body_skill -1
end

object type_skill_hth
inherit type_skill
dam 1
body_combat -1
end

object type_skill_tool
type 74
end

object type_spell
inherit type_ranged
type 101
end

object type_wand
inherit type_ranged_item
skill use magic item
type 109
end

object type_force
face blank.x11
type 114
invisible 1
no_drop 1
end

object type_quad_floor
no_pick 1
no_drop 1
is_floor 1
is_quad 1
nrof 1
end

object type_quad_wall
move_block all
no_pick 1
no_drop 1
blocksview 1
is_quad 1
nrof 1
end

object type_quad_material
nrof 1
weight 1000
type 126
subtype 4
end
