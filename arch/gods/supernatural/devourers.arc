# supernatural -- the devourers of souls
object Devourers
title Gaea
race undead
msg
Soul Eaters, Harbingers of Death, Nameless
Lords of the Tomb
endmsg
face grimreaper.x12
animation grimreaper
is_animated 1
str 30
dex 30
con 30
wis 30
pow 30
int 30
hp 350
maxhp 350
exp 1
dam 50
wc -1
ac -12
speed 0.35
level 15
type 50
attacktype 16973824
resist_fire -5
resist_cold 15
resist_drain 100
resist_ghosthit 50
resist_poison 100
resist_fear 100
resist_deplete 100
resist_death 100
path_attuned 393216
path_repelled 524547
path_denied 65536
weight 1
last_heal -1
last_eat 60
randomitems Devourers
alive 1
can_use_shield 1
monster 1
undead 1
can_cast_spell 1
can_use_armour 1
can_use_weapon 1
can_see_in_dark 1
end

