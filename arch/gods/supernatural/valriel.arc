# supernatural -- lord of angels Valriel
object Valriel
anim
archangel.x11
archangel.x12
archangel.x13
archangel.x14
archangel.x15
archangel.x16
archangel.x17
archangel.x18
mina
title Gorokh
race angel
slaying demon
msg
Lord of Angels, Duke of the Heavens, Healer and Protector
endmsg
face archangel.x15
is_animated 1
str 30
dex 30
con 30
wis 30
pow 30
int 30
hp 350
maxhp 350
exp 1
dam 50
wc -1
ac -7
speed 0.25
level 15
type 50
attacktype 4194304
resist_confusion 20
resist_fear 100
resist_blind 100
path_attuned 1025
path_denied 393216
weight 100000
randomitems Valriel
alive 1
can_use_shield 1
monster 1
can_cast_spell 1
can_use_armour 1
can_use_weapon 1
end

