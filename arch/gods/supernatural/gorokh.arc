# supernatural -- demon king Gorokh
object Gorokh
title Valriel
race demon
slaying angel
msg
Demon King, Duke of Hell, Temptor and Tormentor
endmsg
face devil.x11
animation devil
is_animated 1
str 30
dex 30
con 30
wis 30
pow 30
int 30
hp 350
maxhp 350
exp 1
dam 50
luck -1
wc -3
ac -5
speed 0.25
level 15
type 50
attacktype 16384
resist_magic 30
resist_cold -5
resist_fear 100
path_attuned 131072
path_repelled 257
weight 400000
last_heal -2
last_sp -1
randomitems Gorokh
alive 1
can_use_shield 1
monster 1
can_cast_spell 1
can_use_armour 1
can_use_weapon 1
end

