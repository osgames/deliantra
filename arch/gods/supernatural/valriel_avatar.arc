object valriel_avatar
anim
archangel.x11
archangel.x12
archangel.x13
archangel.x14
archangel.x15
archangel.x16
archangel.x17
archangel.x18
mina
race angel
slaying demon
face archangel.x15
is_animated 1
str 30
dex 30
con 30
wis 30
pow 30
int 30
hp 350
maxhp 350
exp 1
dam 40
wc -1
ac -7
speed 0.25
level 15
attacktype 4194305
resist_physical 50
resist_confusion 20
resist_fear 100
resist_blind 100
path_attuned 1025
path_denied 393216
weight 100000
alive 1
can_use_shield 1
monster 1
can_cast_spell 1
can_use_armour 1
can_use_weapon 1
end

