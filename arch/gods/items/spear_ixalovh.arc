object spear_ixalovh
anim
spear_ixalovh.x11
spear_ixalovh.x12
spear_ixalovh.x13
mina
inherit type_weapon
name Ixalovh's spear
name_pl Ixalovh's spears
skill two handed weapons
face spear_ixalovh.x11
pow 1
sp 1
dam 35
speed -0.02
nrof 1
attacktype 17
resist_magic 10
resist_fire 15
materialname adamant
value 75000
weight 26500
magic 12
last_sp 9
glow_radius 2
weapontype 5
client_type 136
item_power 20
startequip 1
body_arm -2
body_combat -1
end

