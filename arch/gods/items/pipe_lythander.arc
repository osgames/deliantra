object pipe_lythander
inherit type_horn
name Lythander's pipes
msg
This pipe is the finest you have ever
seen. Imagine the smoke rings you
could blow with it...
endmsg
face claypipe.x11
hp 60
maxhp 60
luck 1
speed 0.1
level 35
materialname adamant
value 58
weight 40
randomitems pipe_lythander
client_type 721
startequip 1
body_range -1
end

