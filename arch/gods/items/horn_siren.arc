object horn_siren
inherit type_horn
name horn
name_pl horns
title of the Sirens
msg
  Putting this shell to your ear, you hear a
  strange and haunting melody.
endmsg
face shellhorn1.x11
hp 20
maxhp 20
speed 0.2
level 40
materialname iron
value 590
weight 1500
randomitems horn_siren
client_type 721
startequip 1
body_range -1
end

