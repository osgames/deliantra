object mail_mostrai
name Thorin's Plate Mail
name_pl Thorin's Plate Mails
msg
  This shining plate mail is Mostrai's
  gift to the bravest of his warriors.
  It is highly enchanted, increasing the
  strength of the wearer and protecting
  against fire.
endmsg
face plate_mail.x11
str 1
ac 3
nrof 1
type 16
resist_physical 50
resist_fire 30
weight 50000
magic 2
last_sp 13
client_type 250
item_power 4
gen_sp_armour 9
startequip 1
body_torso -1
end

