object shield_gaea
anim
earth_shield.x11
earth_shield.x12
earth_shield.x13
earth_shield.x14
earth_shield.x13
earth_shield.x12
mina
inherit type_shield
name Gaea's Shield of Earth
name_pl Gaea's Shields of Earth
msg
  This shield is highly enchanted by the forces
  of life and nature. It is a personal gift from
  Gaea, to protect her beloved children.
endmsg
face earth_shield.x11
is_animated 1
ac 4
speed 0.2
nrof 1
resist_physical 15
resist_fire 30
resist_electricity 30
resist_cold 30
resist_ghosthit -20
weight 20000
client_type 260
startequip 1
body_arm -1
body_shield -1
end

