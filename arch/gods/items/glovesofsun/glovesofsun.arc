object glovesofsun
anim
glovesofsun.x11
glovesofsun.x12
glovesofsun.x13
mina
name Gloves of the Sun
name_pl Gloves of the Sun
msg
  This pair of gloves will aid any of Gaea's
  most faithful in seeing her will protected.
endmsg
face glovesofsun.x11
dex 2
dam 2
wc 2
ac 1
speed 0.25
nrof 1
type 100
attacktype 5
materialname leather
value 20000
weight 900
magic 1
client_type 300
startequip 1
body_hand -2
end

