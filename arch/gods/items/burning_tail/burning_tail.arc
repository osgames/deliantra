object burningtail
anim
burningtail.x11
burningtail.x12
burningtail.x13
mina
inherit type_weapon
name Burning Tail of many lashings of Ruggilli
name_pl Burning Tails of many lashings of Ruggilli
skill one handed weapons
face burningtail.x11
dam 40
speed 0.1
nrof 1
attacktype 4101
resist_fire 15
resist_cold 25
materialname adamant
value 75000
weight 10000
magic 15
last_sp 8
glow_radius 3
weapontype 6
client_type 100
item_power 25
startequip 1
body_arm -1
body_combat -1
end

