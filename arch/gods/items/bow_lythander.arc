object bow_lythander
inherit type_bow
name Lythander's Elven Bow
name_pl Lythander's Elven Bows
race arrow
skill missile weapons
msg
  You look at this wonderful bow with
  pride. It is only granted to the best
  of Lythander's disciples.
endmsg
face elven_bow.x11
sp 70
dam 30
luck 1
wc 3
nrof 1
attacktype 1
weight 8000
magic 5
client_type 150
startequip 1
body_arm -2
end

