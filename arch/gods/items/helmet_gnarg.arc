object helmet_gnarg
name Gnarg's Orc Helmet
name_pl Gnarg's Orc Helmets
msg
  This helmet good protection. From mighty
  Gnarg it is.
endmsg
face bighorn_he.x11
dex 1
ac 3
nrof 1
type 34
resist_physical 20
resist_poison 30
weight 7000
client_type 270
gen_sp_armour 1
startequip 1
body_head -1
end

