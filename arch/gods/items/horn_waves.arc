object horn_waves
inherit type_horn
name horn
name_pl horns
title of Ocean Waves
msg
  Putting this shell to your ear, you hear
  the crashing sound of ocean waves.
endmsg
face shellhorn2.x11
hp 24
maxhp 24
speed 0.1
level 30
materialname iron
value 590
weight 1000
randomitems horn_waves
client_type 721
startequip 1
body_range -1
end

