object mithril_ar_ele_sorig
name mithril chainmail of lightning of Sorig
name_pl mithril chainmails of lightning of Sorig
msg
  Sorig has granted you this fine mail.  It
  grants great protection from electricity
  and physical attacks without reducing your
  mobility. Be warned that what Sorig gives,
  Sorig can take away.
endmsg
face mithril_ar_ele.x11
animation mithril_ar_ele
str 1
dex 1
exp 1
ac 4
speed 0.3
nrof 1
type 16
resist_physical 40
resist_electricity 40
value 4000
weight 15000
magic 5
last_sp 27
client_type 250
item_power 10
gen_sp_armour 2
startequip 1
body_torso -1
end

