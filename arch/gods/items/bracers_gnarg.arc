object bracers_gnarg
name bracers
name_pl bracers
title of strength of Wargs
msg
  An exceptional pair of bracers. Not only do
  they provide the wearer with protection
  from cold, they also increase the wearer's
  damage and strength, and help heal the body
  from damage. A mighty gift from Gnarg sent
  to aid you in annihilating His enemies.
endmsg
face bracersdex.x11
str 2
hp 1
dam 15
nrof 1
type 104
resist_physical 30
resist_cold 20
value 65000
weight 9000
client_type 310
startequip 1
body_wrist -2
end

