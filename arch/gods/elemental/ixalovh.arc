# elemental -- sea goddess Ixalovh
object Ixalovh
anim
witch_water.x11
witch_water.x12
mina
title Ruggilli
race chaotic_water_creatures
slaying consuming_fire_creatures
msg
Chaos Goddess of many tentacles, Sea Serpent, Ocean Wave
endmsg
face witch_water.x11
is_animated 1
str 30
dex 30
con 30
wis 30
pow 30
int 30
hp 400
maxhp 400
exp 1
dam 60
wc -3
ac -5
speed 0.50
level 15
type 50
attacktype 17
resist_physical 20
resist_magic 20
resist_fire -30
resist_cold 80
path_attuned 16388
path_repelled 514
path_denied 65536
weight 500000
randomitems Ixalovh
alive 1
can_use_shield 1
monster 1
can_cast_spell 1
can_use_armour 1
can_use_weapon 1
end

