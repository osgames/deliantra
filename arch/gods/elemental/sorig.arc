# elemental -- storm god Sorig
object Sorig
race air_elemental
msg
Lord of Storms, King of Thunder and Lightning, Sky Lord
endmsg
face para_light.x11
animation para_lightning
is_animated 1
str 30
dex 30
con 30
wis 30
pow 30
int 30
hp 300
maxhp 300
exp 1
dam 40
wc -1
ac -5
speed 0.40
level 15
type 50
attacktype 8
resist_electricity 100
path_attuned 131096
path_repelled 256
path_denied 65537
weight 1
randomitems Sorig
alive 1
can_use_shield 1
monster 1
can_cast_spell 1
can_use_armour 1
can_use_weapon 1
end

