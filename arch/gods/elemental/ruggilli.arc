# elemental -- war/dragon god Ruggilli
object Ruggilli
title Ixalovh
race consuming_fire_creatures
slaying chaotic_water_creatures
msg
Chaos God of slaughter and terror, Consuming worm, Greedy gut
endmsg
face elem_fire.x11
animation fire_elemental
is_animated 1
str 30
dex 30
con 30
wis 30
pow 30
int 30
hp 400
maxhp 400
exp 1
dam 60
wc -3
ac -5
speed 0.50
level 15
type 50
attacktype 5
resist_physical 30
resist_magic 30
resist_fire 100
resist_cold -30
path_attuned 131074
path_repelled 2308
path_denied 65536
weight 500000
last_heal 1
last_eat -1
randomitems Ruggilli
alive 1
monster 1
reflect_missile 1
can_cast_spell 1
can_use_weapon 1
end

