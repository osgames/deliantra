# demihumans -- elven god Lythander
object Lythander
title Gnarg
race faerie
slaying goblin,troll
msg
Elven god of luck, Huntsman of Goblins,
Trollslayer, the Trickster
endmsg
face elf_1.x11
animation elf_1
is_animated 1
str 30
dex 30
con 30
wis 30
pow 30
int 30
hp 350
maxhp 350
exp 1
dam 40
luck 2
wc -1
ac -7
speed 0.25
level 15
type 50
attacktype 2080
resist_confusion 100
resist_acid -15
resist_poison -30
path_attuned 65552
path_denied 512
weight 500000
last_sp 1
randomitems Lythander
alive 1
can_use_shield 1
monster 1
can_cast_spell 1
can_use_armour 1
can_use_weapon 1
stealth 1
end

