# demihumans -- goblin/troll god Gnarg
object Gnarg
title Lythander
race goblin,giant,troll
slaying faerie,dwarf
msg
Father of goblins, Lord Troll, Master of
poisons, Patron of assassins
endmsg
face smalltroll.x11
animation small_troll
is_animated 1
str 30
dex 30
con 30
wis 30
pow 30
int 30
hp 500
maxhp 500
exp 1
dam 50
wc -3
ac -5
speed 0.25
level 15
type 50
attacktype 1024
resist_magic -20
resist_poison 100
path_attuned 131088
path_repelled 65537
weight 500000
randomitems Gnarg
alive 1
can_use_shield 1
monster 1
can_cast_spell 1
can_use_armour 1
can_use_weapon 1
end

