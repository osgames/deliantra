object snowstorm_0
other_arch spell_icestorm
face icestorm.x13
sp 0
exp 50
dam 1
ac 3
speed -0.02
level 10
type 62
resist_cold 100
invisible 1
move_block -all
blocksview 0
activate_on_push 1
activate_on_release 1
end

