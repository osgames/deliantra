object lbulletwall
anim
lbull_wall.x11
lbull_wall.x21
lbull_wall.x31
lbull_wall.x41
lbull_wall.x51
lbull_wall.x61
lbull_wall.x71
lbull_wall.x81
mina
name large bulletwall
other_arch spell_lg_magic_bullet
face lbull_wall.x01
is_animated 0
dam 29
speed -0.16
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lbulletwall_1
name large bulletwall
other_arch spell_lg_magic_bullet
face lbull_wall.x11
sp 1
dam 29
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lbulletwall_2
name large bulletwall
other_arch spell_lg_magic_bullet
face lbull_wall.x21
sp 2
dam 29
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lbulletwall_3
name large bulletwall
other_arch spell_lg_magic_bullet
face lbull_wall.x31
sp 3
dam 29
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lbulletwall_4
name large bulletwall
other_arch spell_lg_magic_bullet
face lbull_wall.x41
sp 4
dam 29
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lbulletwall_5
name large bulletwall
other_arch spell_lg_magic_bullet
face lbull_wall.x51
sp 5
dam 29
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lbulletwall_6
name large bulletwall
other_arch spell_lg_magic_bullet
face lbull_wall.x61
sp 6
dam 29
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lbulletwall_7
name large bulletwall
other_arch spell_lg_magic_bullet
face lbull_wall.x71
sp 7
dam 29
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lbulletwall_8
name large bulletwall
other_arch spell_lg_magic_bullet
face lbull_wall.x81
sp 8
dam 29
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

