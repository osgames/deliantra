object wwall_weak_1
anim
wwall_w_0.x11
wwall_w_0.x12
wwall_w_0.x13
wwall_w_0.x14
wwall_w_0.x15
wwall_w_0.x16
wwall_w_0.x17
mina
name wall
face wwall_w_0.x11
is_animated 0
hp 200
maxhp 200
ac 30
level 1
resist_magic 100
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
alive 1
no_pick 1
blocksview 1
tear_down 1
end

object wwall_weak_2
anim
wwall_w_1.x11
wwall_w_1.x12
wwall_w_1.x13
wwall_w_1.x14
wwall_w_1.x15
wwall_w_1.x16
wwall_w_1.x17
mina
name wall
face wwall_w_1.x11
is_animated 0
hp 200
maxhp 200
ac 30
level 1
resist_magic 100
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
alive 1
no_pick 1
blocksview 1
tear_down 1
end

