object weak_wall_1
anim
awall_w_0.x11
awall_w_0.x12
awall_w_0.x13
awall_w_0.x14
awall_w_0.x15
awall_w_0.x16
awall_w_0.x17
mina
name wall
race wall
face awall_w_0.x11
is_animated 0
hp 80
maxhp 80
ac 30
level 1
resist_magic 100
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
alive 1
no_pick 1
blocksview 1
tear_down 1
end

object weak_wall_2
anim
awall_w_1.x11
awall_w_1.x12
awall_w_1.x13
awall_w_1.x14
awall_w_1.x15
awall_w_1.x16
awall_w_1.x17
awall_w_1.x18
awall_w_1.x19
awall_w_1.x1A
mina
name wall
race wall
face awall_w_1.x11
is_animated 0
hp 80
maxhp 80
ac 30
level 1
resist_magic 100
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
alive 1
no_pick 1
blocksview 1
tear_down 1
end

