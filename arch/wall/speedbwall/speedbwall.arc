object speedballwall
anim
speedbwall.x11
speedbwall.x12
mina
name speedball wall
other_arch spell_small_speedball
face speedbwall.x11
dam 56
speed -0.03
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

