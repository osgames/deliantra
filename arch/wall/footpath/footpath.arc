object footpath_0
name footpath
face footpath_0.x11
smoothlevel 40
smoothface footpath_F.x11 grass_S.x11
no_pick 1
is_floor 1
end

object footpath_1_1
name footpath
face footpath_4.x11
smoothlevel 40
no_pick 1
is_floor 1
end

object footpath_1_2
name footpath
face footpath_1.x11
smoothlevel 40
no_pick 1
is_floor 1
end

object footpath_1_3
name footpath
face footpath_8.x11
smoothlevel 40
no_pick 1
is_floor 1
end

object footpath_1_4
name footpath
face footpath_2.x11
smoothlevel 40
no_pick 1
is_floor 1
end

object footpath_2_1_1
name footpath
face footpath_5.x11
smoothlevel 40
no_pick 1
is_floor 1
end

object footpath_2_1_2
name footpath
face footpath_A.x11
smoothlevel 40
no_pick 1
is_floor 1
end

object footpath_2_2_1
name footpath
face footpath_3.x11
smoothlevel 40
no_pick 1
is_floor 1
end

object footpath_2_2_2
name footpath
face footpath_6.x11
smoothlevel 40
no_pick 1
is_floor 1
end

object footpath_2_2_3
name footpath
face footpath_C.x11
smoothlevel 40
no_pick 1
is_floor 1
end

object footpath_2_2_4
name footpath
face footpath_9.x11
smoothlevel 40
no_pick 1
is_floor 1
end

object footpath_3_1
name footpath
face footpath_B.x11
smoothlevel 40
no_pick 1
is_floor 1
end

object footpath_3_2
name footpath
face footpath_7.x11
smoothlevel 40
no_pick 1
is_floor 1
end

object footpath_3_3
name footpath
face footpath_E.x11
smoothlevel 40
no_pick 1
is_floor 1
end

object footpath_3_4
name footpath
face footpath_D.x11
smoothlevel 40
no_pick 1
is_floor 1
end

object footpath_4
name footpath
face footpath_F.x11
smoothlevel 40
no_pick 1
is_floor 1
end

