object bwall-gray_weak_wall_5
anim
bwall-gray_w_0.x11
bwall-gray_w_0.x12
bwall-gray_w_0.x13
bwall-gray_w_0.x14
bwall-gray_w_0.x15
bwall-gray_w_0.x16
bwall-gray_w_0.x17
mina
name wall
race wall
face bwall-gray_w_0.x11
is_animated 0
hp 1000
maxhp 1000
ac 30
level 1
resist_magic 100
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
alive 1
no_pick 1
blocksview 1
tear_down 1
end

object bwall-gray_weak_wall_6
anim
bwall-gray_w_1.x11
bwall-gray_w_1.x12
bwall-gray_w_1.x13
bwall-gray_w_1.x14
bwall-gray_w_1.x15
bwall-gray_w_1.x16
bwall-gray_w_1.x17
mina
name wall
race wall
face bwall-gray_w_1.x11
is_animated 0
hp 1000
maxhp 1000
ac 30
level 1
resist_magic 100
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
alive 1
no_pick 1
blocksview 1
tear_down 1
end

