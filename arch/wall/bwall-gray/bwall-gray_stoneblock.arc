object bwall-gray_stoneblock_door_closed
anim
bwall-gray_stoneblock.x15
bwall-gray_stoneblock.x14
bwall-gray_stoneblock.x13
bwall-gray_stoneblock.x12
bwall-gray_stoneblock.x11
bwall-gray_earthwall.x11
mina
name stone block
face bwall-gray_earthwall.x11
is_animated 0
maxsp 0
dam 5
wc 5
type 91
move_block all
no_pick 1
blocksview 1
activate_on_push 1
activate_on_release 1
end

object bwall-gray_stoneblock_door_open
anim
bwall-gray_stoneblock.x15
bwall-gray_stoneblock.x14
bwall-gray_stoneblock.x13
bwall-gray_stoneblock.x12
bwall-gray_stoneblock.x11
bwall-gray_earthwall.x11
mina
name stone block
face bwall-gray_stoneblock.x15
is_animated 0
maxsp 1
dam 5
wc 0
type 91
no_pick 1
activate_on_push 1
activate_on_release 1
end

