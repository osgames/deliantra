object stwall_0
name wall
face stwall_0.x11
move_block all
no_pick 1
blocksview 1
end

object stwall_1_1
name wall
face stwall_4.x11
move_block all
no_pick 1
blocksview 1
end

object stwall_1_2
name wall
face stwall_1.x11
move_block all
no_pick 1
blocksview 1
end

object stwall_1_3
name wall
face stwall_8.x11
move_block all
no_pick 1
blocksview 1
end

object stwall_1_4
name wall
face stwall_2.x11
move_block all
no_pick 1
blocksview 1
end

object stwall_2_1_1
name wall
face stwall_5.x11
move_block all
no_pick 1
blocksview 1
end

object stwall_2_1_2
name wall
face stwall_A.x11
move_block all
no_pick 1
blocksview 1
end

object stwall_2_2_1
name wall
face stwall_3.x11
move_block all
no_pick 1
blocksview 1
end

object stwall_2_2_2
name wall
face stwall_6.x11
move_block all
no_pick 1
blocksview 1
end

object stwall_2_2_3
name wall
face stwall_C.x11
move_block all
no_pick 1
blocksview 1
end

object stwall_2_2_4
name wall
face stwall_9.x11
move_block all
no_pick 1
blocksview 1
end

object stwall_3_1
name wall
face stwall_B.x11
move_block all
no_pick 1
blocksview 1
end

object stwall_3_2
name wall
face stwall_7.x11
move_block all
no_pick 1
blocksview 1
end

object stwall_3_3
name wall
face stwall_E.x11
move_block all
no_pick 1
blocksview 1
end

object stwall_3_4
name wall
face stwall_D.x11
move_block all
no_pick 1
blocksview 1
end

object stwall_4
name wall
face stwall_F.x11
move_block all
no_pick 1
blocksview 1
end

