object mine_secret_1_1
anim
minedoor_4.x19
minedoor_4.x18
minedoor_4.x17
minedoor_4.x16
minedoor_4.x15
minedoor_4.x14
minedoor_4.x13
minedoor_4.x12
minedoor_4.x11
mina
name secret door
face minedoor_4.x11
is_animated 0
hp 50
maxhp 50
maxsp 0
dam 10
wc 8
speed 0.35
type 26
value 1
move_block all
no_pick 1
blocksview 1
activate_on_push 1
activate_on_release 1
end
more
object mine_secret_1_2
anim
minedoor_4.x19
minedoor_4.x18
minedoor_4.x17
minedoor_4.x16
minedoor_4.x15
minedoor_4.x14
minedoor_4.x13
minedoor_4.x12
minedoor_4.x11
mina
name secret door
face minedoor_4.x11
is_animated 0
hp 50
maxhp 50
maxsp 0
dam 10
wc 8
x 1
speed 0.35
type 26
value 1
move_block all
no_pick 1
blocksview 1
activate_on_push 1
activate_on_release 1
end

object mine_secret_2_1
anim
minedoor_8.x19
minedoor_8.x18
minedoor_8.x17
minedoor_8.x16
minedoor_8.x15
minedoor_8.x14
minedoor_8.x13
minedoor_8.x12
minedoor_8.x11
mina
name secret door
face minedoor_8.x11
is_animated 0
hp 50
maxhp 50
maxsp 0
dam 10
wc 8
speed 0.35
type 26
value 1
move_block all
no_pick 1
blocksview 1
activate_on_push 1
activate_on_release 1
end
more
object mine_secret_2_2
anim
minedoor_8.x19
minedoor_8.x18
minedoor_8.x17
minedoor_8.x16
minedoor_8.x15
minedoor_8.x14
minedoor_8.x13
minedoor_8.x12
minedoor_8.x11
mina
name secret door
face minedoor_8.x11
is_animated 0
hp 50
maxhp 50
maxsp 0
dam 10
wc 8
y 1
speed 0.35
type 26
value 1
move_block all
no_pick 1
blocksview 1
activate_on_push 1
activate_on_release 1
end

