object yellow_0
name brick road
face yellow_0.x11
no_pick 1
is_floor 1
end

object yellow_1_1
name brick road
face yellow_4.x11
no_pick 1
end

object yellow_1_2
name brick road
face yellow_1.x11
no_pick 1
end

object yellow_1_3
name brick road
face yellow_8.x11
no_pick 1
end

object yellow_1_4
name brick road
face yellow_2.x11
no_pick 1
end

object yellow_2_1_1
name brick road
face yellow_5.x11
no_pick 1
end

object yellow_2_1_2
name brick road
face yellow_A.x11
no_pick 1
end

object yellow_2_2_1
name brick road
face yellow_3.x11
no_pick 1
end

object yellow_2_2_2
name brick road
face yellow_6.x11
no_pick 1
end

object yellow_2_2_3
name brick road
face yellow_C.x11
no_pick 1
end

object yellow_2_2_4
name brick road
face yellow_9.x11
no_pick 1
end

object yellow_3_1
name brick road
face yellow_B.x11
no_pick 1
end

object yellow_3_2
name brick road
face yellow_7.x11
no_pick 1
end

object yellow_3_3
name brick road
face yellow_E.x11
no_pick 1
end

object yellow_3_4
name brick road
face yellow_D.x11
no_pick 1
end

object yellow_4
name brick road
face yellow_F.x11
no_pick 1
end

object yellow_ne
name brick road
face yellow_ne.x11
no_pick 1
end

object yellow_nw
name brick road
face yellow_nw.x11
no_pick 1
end

object yellow_se
name brick road
face yellow_se.x11
no_pick 1
end

object yellow_sw
name brick road
face yellow_sw.x11
no_pick 1
end

