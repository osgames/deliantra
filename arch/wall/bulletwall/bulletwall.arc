object bulletwall
name bulletwall
other_arch spell_magic_bullet
face bul_wall_0.x11
speed -0.4
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object bulletwall_1
name bulletwall
other_arch spell_magic_bullet
face bul_wall_1.x11
sp 1
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object bulletwall_2
name bulletwall
other_arch spell_magic_bullet
face bul_wall_2.x11
sp 2
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object bulletwall_3
name bulletwall
other_arch spell_magic_bullet
face bul_wall_3.x11
sp 3
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object bulletwall_4
name bulletwall
other_arch spell_magic_bullet
face bul_wall_4.x11
sp 4
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object bulletwall_5
name bulletwall
other_arch spell_magic_bullet
face bul_wall_5.x11
sp 5
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object bulletwall_6
name bulletwall
other_arch spell_magic_bullet
face bul_wall_6.x11
sp 6
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object bulletwall_7
name bulletwall
other_arch spell_magic_bullet
face bul_wall_7.x11
sp 7
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object bulletwall_8
name bulletwall
other_arch spell_magic_bullet
face bul_wall_8.x11
sp 8
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

