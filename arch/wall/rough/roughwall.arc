object roughwall_0
name wall
face rough_0.x11
randomitems vein_brazilianite1
move_block all
no_pick 1
blocksview 1
treasure_env 1
end

object roughwall_1_1
name wall
face rough_4.x11
randomitems vein_brazilianite1
move_block all
no_pick 1
blocksview 1
treasure_env 1
end

object roughwall_1_2
name wall
face rough_1.x11
randomitems vein_brazilianite1
move_block all
no_pick 1
blocksview 1
treasure_env 1
end

object roughwall_1_3
name wall
face rough_8.x11
randomitems vein_brazilianite1
move_block all
no_pick 1
blocksview 1
treasure_env 1
end

object roughwall_1_4
name wall
face rough_2.x11
randomitems vein_brazilianite1
move_block all
no_pick 1
blocksview 1
treasure_env 1
end

object roughwall_2_1_1
name wall
face rough_5.x11
randomitems vein_brazilianite1
move_block all
no_pick 1
blocksview 1
treasure_env 1
end

object roughwall_2_1_2
name wall
face rough_A.x11
randomitems vein_brazilianite1
move_block all
no_pick 1
blocksview 1
treasure_env 1
end

object roughwall_2_2_1
name wall
face rough_3.x11
randomitems vein_brazilianite1
move_block all
no_pick 1
blocksview 1
treasure_env 1
end

object roughwall_2_2_2
name wall
face rough_6.x11
randomitems vein_brazilianite1
move_block all
no_pick 1
blocksview 1
treasure_env 1
end

object roughwall_2_2_3
name wall
face rough_C.x11
randomitems vein_brazilianite1
move_block all
no_pick 1
blocksview 1
treasure_env 1
end

object roughwall_2_2_4
name wall
face rough_9.x11
randomitems vein_brazilianite1
move_block all
no_pick 1
blocksview 1
treasure_env 1
end

object roughwall_3_1
name wall
face rough_B.x11
randomitems vein_brazilianite1
move_block all
no_pick 1
blocksview 1
treasure_env 1
end

object roughwall_3_2
name wall
face rough_7.x11
randomitems vein_brazilianite1
move_block all
no_pick 1
blocksview 1
treasure_env 1
end

object roughwall_3_3
name wall
face rough_E.x11
randomitems vein_brazilianite1
move_block all
no_pick 1
blocksview 1
treasure_env 1
end

object roughwall_3_4
name wall
face rough_D.x11
randomitems vein_brazilianite1
move_block all
no_pick 1
blocksview 1
treasure_env 1
end

object roughwall_4
name wall
face rough_F.x11
randomitems vein_brazilianite1
move_block all
no_pick 1
blocksview 1
treasure_env 1
end

