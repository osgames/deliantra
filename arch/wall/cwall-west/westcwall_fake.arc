object westcwall_fake_0
name wall
face westcwall_0.x11
move_block -all
no_pick 1
blocksview 1
end

object westcwall_fake_1_1
name wall
face westcwall_4.x11
move_block -all
no_pick 1
blocksview 1
end

object westcwall_fake_1_2
name wall
face westcwall_1.x11
move_block -all
no_pick 1
blocksview 1
end

object westcwall_fake_1_3
name wall
face westcwall_8.x11
move_block -all
no_pick 1
blocksview 1
end

object westcwall_fake_1_4
name wall
face westcwall_2.x11
move_block -all
no_pick 1
blocksview 1
end

object westcwall_fake_2_1_1
name wall
face westcwall_5.x11
move_block -all
no_pick 1
blocksview 1
end

object westcwall_fake_2_1_2
name wall
face westcwall_A.x11
move_block -all
no_pick 1
blocksview 1
end

object westcwall_fake_2_2_1
name wall
face westcwall_3.x11
move_block -all
no_pick 1
blocksview 1
end

object westcwall_fake_2_2_2
name wall
face westcwall_6.x11
move_block -all
no_pick 1
blocksview 1
end

object westcwall_fake_2_2_3
name wall
face westcwall_C.x11
move_block -all
no_pick 1
blocksview 1
end

object westcwall_fake_2_2_4
name wall
face westcwall_9.x11
move_block -all
no_pick 1
blocksview 1
end

object westcwall_fake_3_1
name wall
face westcwall_B.x11
move_block -all
no_pick 1
blocksview 1
end

object westcwall_fake_3_2
name wall
face westcwall_7.x11
move_block -all
no_pick 1
blocksview 1
end

object westcwall_fake_3_3
name wall
face westcwall_E.x11
move_block -all
no_pick 1
blocksview 1
end

object westcwall_fake_3_4
name wall
face westcwall_D.x11
move_block -all
no_pick 1
blocksview 1
end

object westcwall_fake_4
name wall
face westcwall_F.x11
move_block -all
no_pick 1
blocksview 1
end

