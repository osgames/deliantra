object firewall
other_arch spell_small_fireball
face earthwall.x11
hp 250
maxhp 250
exp 50
dam 1
ac 3
speed -0.02
level 1
type 62
resist_fire 100
alive 1
blocksview 1
activate_on_push 1
activate_on_release 1
end

object firewall_1
name firewall
other_arch spell_small_fireball
face firewall_1.x11
sp 1
dam 1
speed -0.02
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object firewall_2
name firewall
other_arch spell_small_fireball
face firewall_2.x11
sp 2
dam 1
speed -0.02
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object firewall_3
name firewall
other_arch spell_small_fireball
face firewall_3.x11
sp 3
dam 1
speed -0.02
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object firewall_4
name firewall
other_arch spell_small_fireball
face firewall_4.x11
sp 4
dam 1
speed -0.02
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object firewall_5
name firewall
other_arch spell_small_fireball
face firewall_5.x11
sp 5
dam 1
speed -0.02
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object firewall_6
name firewall
other_arch spell_small_fireball
face firewall_6.x11
sp 6
dam 1
speed -0.02
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object firewall_7
name firewall
other_arch spell_small_fireball
face firewall_7.x11
sp 7
dam 1
speed -0.02
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object firewall_8
name firewall
other_arch spell_small_fireball
face firewall_8.x11
sp 8
dam 1
speed -0.02
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

