object earthwall
anim
earthwall.x11
earthwall.x12
earthwall.x13
blank.x11
mina
race wall
face earthwall.x11
is_animated 0
hp 24
maxhp 24
ac 30
level 1
type 45
resist_magic 100
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
alive 1
no_pick 1
blocksview 1
tear_down 1
end

