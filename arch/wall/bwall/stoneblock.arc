object block_door_closed
anim
stoneblock.x15
stoneblock.x14
stoneblock.x13
stoneblock.x12
stoneblock.x11
earthwall.x11
mina
name stone block
face earthwall.x11
is_animated 0
maxsp 0
dam 5
wc 5
type 91
move_block all
no_pick 1
blocksview 1
activate_on_push 1
activate_on_release 1
end

object block_door_open
anim
stoneblock.x15
stoneblock.x14
stoneblock.x13
stoneblock.x12
stoneblock.x11
earthwall.x11
mina
name stone block
face stoneblock.x15
is_animated 0
maxsp 1
dam 5
wc 0
type 91
no_pick 1
activate_on_push 1
activate_on_release 1
end

