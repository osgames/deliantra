object weak_wall_5
anim
bwall_w_0.x11
bwall_w_0.x12
bwall_w_0.x13
bwall_w_0.x14
bwall_w_0.x15
bwall_w_0.x16
bwall_w_0.x17
mina
name wall
race wall
face bwall_w_0.x11
is_animated 0
hp 150
maxhp 150
ac 30
level 1
resist_magic 100
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
alive 1
no_pick 1
blocksview 1
tear_down 1
end

object weak_wall_6
anim
bwall_w_1.x11
bwall_w_1.x12
bwall_w_1.x13
bwall_w_1.x14
bwall_w_1.x15
bwall_w_1.x16
bwall_w_1.x17
mina
name wall
race wall
face bwall_w_1.x11
is_animated 0
hp 150
maxhp 150
ac 30
level 1
resist_magic 100
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
alive 1
no_pick 1
blocksview 1
tear_down 1
end

