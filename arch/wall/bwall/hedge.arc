object hedge
anim
hedge.x11
hedge.x12
hedge.x13
blank.x11
mina
face hedge.x11
is_animated 0
hp 24
maxhp 24
ac 20
level 1
type 45
resist_magic 100
resist_fire 30
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
alive 1
no_pick 1
blocksview 1
tear_down 1
end

