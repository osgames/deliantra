object lightningwall
name lightningwall
other_arch spell_sm_lightning
face light_wall.x01
dam 5
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lightningwall_1
name lightningwall
other_arch spell_sm_lightning
face light_wall.x11
sp 1
dam 5
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lightningwall_2
name lightningwall
other_arch spell_sm_lightning
face light_wall.x21
sp 2
dam 5
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lightningwall_3
name lightningwall
other_arch spell_sm_lightning
face light_wall.x31
sp 3
dam 5
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lightningwall_4
name lightningwall
other_arch spell_sm_lightning
face light_wall.x41
sp 4
dam 5
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lightningwall_5
name lightningwall
other_arch spell_sm_lightning
face light_wall.x51
sp 5
dam 5
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lightningwall_6
name lightningwall
other_arch spell_sm_lightning
face light_wall.x61
sp 6
dam 5
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lightningwall_7
name lightningwall
other_arch spell_sm_lightning
face light_wall.x71
sp 7
dam 5
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lightningwall_8
name lightningwall
other_arch spell_sm_lightning
face light_wall.x81
sp 8
dam 5
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object lightningwall_t
anim
light_wall.x11
light_wall.x21
light_wall.x31
light_wall.x41
light_wall.x51
light_wall.x61
light_wall.x71
light_wall.x81
mina
name lightningwall
other_arch spell_sm_lightning
face light_wall.x11
is_animated 0
sp 1
maxsp 1
dam 5
speed -0.04
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

