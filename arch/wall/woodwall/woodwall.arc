object woodwall_0
name wall
face woodwall_0.x11
move_block all
no_pick 1
blocksview 1
end

object woodwall_1_1
name wall
face woodwall_4.x11
move_block all
no_pick 1
blocksview 1
end

object woodwall_1_2
name wall
face woodwall_1.x11
move_block all
no_pick 1
blocksview 1
end

object woodwall_1_3
name wall
face woodwall_8.x11
move_block all
no_pick 1
blocksview 1
end

object woodwall_1_4
name wall
face woodwall_2.x11
move_block all
no_pick 1
blocksview 1
end

object woodwall_2_1_1
name wall
face woodwall_5.x11
move_block all
no_pick 1
blocksview 1
end

object woodwall_2_1_2
name wall
face woodwall_A.x11
move_block all
no_pick 1
blocksview 1
end

object woodwall_2_2_1
name wall
face woodwall_3.x11
move_block all
no_pick 1
blocksview 1
end

object woodwall_2_2_2
name wall
face woodwall_6.x11
move_block all
no_pick 1
blocksview 1
end

object woodwall_2_2_3
name wall
face woodwall_C.x11
move_block all
no_pick 1
blocksview 1
end

object woodwall_2_2_4
name wall
face woodwall_9.x11
move_block all
no_pick 1
blocksview 1
end

object woodwall_3_1
name wall
face woodwall_B.x11
move_block all
no_pick 1
blocksview 1
end

object woodwall_3_2
name wall
face woodwall_7.x11
move_block all
no_pick 1
blocksview 1
end

object woodwall_3_3
name wall
face woodwall_E.x11
move_block all
no_pick 1
blocksview 1
end

object woodwall_3_4
name wall
face woodwall_D.x11
move_block all
no_pick 1
blocksview 1
end

object woodwall_4
name wall
face woodwall_F.x11
move_block all
no_pick 1
blocksview 1
end

