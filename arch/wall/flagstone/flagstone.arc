object flagstone_0
name wall
face flagstone_0.x11
move_block all
no_pick 1
blocksview 1
end

object flagstone_1_1
name wall
face flagstone_4.x11
move_block all
no_pick 1
blocksview 1
end

object flagstone_1_2
name wall
face flagstone_1.x11
move_block all
no_pick 1
blocksview 1
end

object flagstone_1_3
name wall
face flagstone_8.x11
move_block all
no_pick 1
blocksview 1
end

object flagstone_1_4
name wall
face flagstone_2.x11
move_block all
no_pick 1
blocksview 1
end

object flagstone_2_1_1
name wall
face flagstone_5.x11
move_block all
no_pick 1
blocksview 1
end

object flagstone_2_1_2
name wall
face flagstone_A.x11
move_block all
no_pick 1
blocksview 1
end

object flagstone_2_2_1
name wall
face flagstone_3.x11
move_block all
no_pick 1
blocksview 1
end

object flagstone_2_2_2
name wall
face flagstone_6.x11
move_block all
no_pick 1
blocksview 1
end

object flagstone_2_2_3
name wall
face flagstone_C.x11
move_block all
no_pick 1
blocksview 1
end

object flagstone_2_2_4
name wall
face flagstone_9.x11
move_block all
no_pick 1
blocksview 1
end

object flagstone_3_1
name wall
face flagstone_B.x11
move_block all
no_pick 1
blocksview 1
end

object flagstone_3_2
name wall
face flagstone_7.x11
move_block all
no_pick 1
blocksview 1
end

object flagstone_3_3
name wall
face flagstone_E.x11
move_block all
no_pick 1
blocksview 1
end

object flagstone_3_4
name wall
face flagstone_D.x11
move_block all
no_pick 1
blocksview 1
end

object flagstone_4
name wall
face flagstone_F.x11
move_block all
no_pick 1
blocksview 1
end

