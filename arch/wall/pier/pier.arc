object pier_0
name pier
face pier_0.x11
move_block boat
move_allow walk
no_pick 1
end

object pier_1_1
name pier
face pier_4.x11
move_block boat
move_allow walk
no_pick 1
end

object pier_1_2
name pier
face pier_1.x11
move_block boat
move_allow walk
no_pick 1
end

object pier_1_3
name pier
face pier_8.x11
move_block boat
move_allow walk
no_pick 1
end

object pier_1_4
name pier
face pier_2.x11
move_block boat
move_allow walk
no_pick 1
end

object pier_2_1_1
name pier
face pier_5.x11
move_block boat
move_allow walk
no_pick 1
end

object pier_2_1_2
name pier
face pier_A.x11
move_block boat
move_allow walk
no_pick 1
end

object pier_2_2_1
name pier
face pier_3.x11
move_block boat
move_allow walk
no_pick 1
end

object pier_2_2_2
name pier
face pier_6.x11
move_block boat
move_allow walk
no_pick 1
end

object pier_2_2_3
name pier
face pier_C.x11
move_block boat
move_allow walk
no_pick 1
end

object pier_2_2_4
name pier
face pier_9.x11
move_block boat
move_allow walk
no_pick 1
end

object pier_3_1
name pier
face pier_B.x11
move_block boat
move_allow walk
no_pick 1
end

object pier_3_2
name pier
face pier_7.x11
move_block boat
move_allow walk
no_pick 1
end

object pier_3_3
name pier
face pier_E.x11
move_block boat
move_allow walk
no_pick 1
end

object pier_3_4
name pier
face pier_D.x11
move_block boat
move_allow walk
no_pick 1
end

object pier_4
name pier
face pier_F.x11
move_block boat
move_allow walk
no_pick 1
end

