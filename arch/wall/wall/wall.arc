object wall_0
name wall
face wall_0.x11
move_block all
no_pick 1
blocksview 1
end

object wall_1_1
name wall
face wall_4.x11
move_block all
no_pick 1
blocksview 1
end

object wall_1_1_ruin
name crumbled wall
face wall_4_ruin.x11
move_block all
no_pick 1
blocksview 1
end

object wall_1_2
name wall
face wall_1.x11
move_block all
no_pick 1
blocksview 1
end

object wall_1_2_ruin
name crumbled wall
face wall_1_ruin.x11
move_block all
no_pick 1
blocksview 1
end

object wall_1_3
name wall
face wall_8.x11
move_block all
no_pick 1
blocksview 1
end

object wall_1_3_ruin
name crumbled wall
face wall_8_ruin.x11
move_block all
no_pick 1
blocksview 1
end

object wall_1_4
name wall
face wall_2.x11
move_block all
no_pick 1
blocksview 1
end

object wall_1_4_ruin
name crumbled wall
face wall_2_ruin.x11
move_block all
no_pick 1
blocksview 1
end

object wall_2_1_1
name wall
face wall_5.x11
move_block all
no_pick 1
blocksview 1
end

object wall_2_1_2
name wall
face wall_A.x11
move_block all
no_pick 1
blocksview 1
end

object wall_2_2_1
name wall
face wall_3.x11
move_block all
no_pick 1
blocksview 1
end

object wall_2_2_2
name wall
face wall_6.x11
move_block all
no_pick 1
blocksview 1
end

object wall_2_2_3
name wall
face wall_C.x11
move_block all
no_pick 1
blocksview 1
end

object wall_2_2_4
name wall
face wall_9.x11
move_block all
no_pick 1
blocksview 1
end

object wall_3_1
name wall
face wall_B.x11
move_block all
no_pick 1
blocksview 1
end

object wall_3_2
name wall
face wall_7.x11
move_block all
no_pick 1
blocksview 1
end

object wall_3_3
name wall
face wall_E.x11
move_block all
no_pick 1
blocksview 1
end

object wall_3_4
name wall
face wall_D.x11
move_block all
no_pick 1
blocksview 1
end

object wall_4
name wall
face wall_F.x11
move_block all
no_pick 1
blocksview 1
end

