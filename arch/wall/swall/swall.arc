object swall_0
name wall
face swall_0.x11
move_block all
end

object swall_1_1
name wall
face swall_4.x11
move_block all
end

object swall_1_1_half
name wall
face swall_4_half.x11
move_block all
end

object swall_1_2
name wall
face swall_1.x11
move_block all
end

object swall_1_2_half
name wall
face swall_1_half.x11
move_block all
end

object swall_1_3
name wall
face swall_8.x11
move_block all
end

object swall_1_3_half
name wall
face swall_8_half.x11
move_block all
end

object swall_1_4
name wall
face swall_2.x11
move_block all
end

object swall_1_4_half
name wall
face swall_2_half.x11
move_block all
end

object swall_2_1_1
name wall
face swall_5.x11
move_block all
end

object swall_2_1_2
name wall
face swall_A.x11
move_block all
end

object swall_2_2_1
name wall
face swall_3.x11
move_block all
end

object swall_2_2_2
name wall
face swall_6.x11
move_block all
end

object swall_2_2_3
name wall
face swall_C.x11
move_block all
end

object swall_2_2_4
name wall
face swall_9.x11
move_block all
end

object swall_3_1
name wall
face swall_B.x11
move_block all
end

object swall_3_2
name wall
face swall_7.x11
move_block all
end

object swall_3_3
name wall
face swall_E.x11
move_block all
end

object swall_3_4
name wall
face swall_D.x11
move_block all
end

object swall_4
name wall
face swall_F.x11
move_block all
end

