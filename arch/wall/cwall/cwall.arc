object cwall_0
name wall
face cwall_0.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_1_1
name wall
face cwall_4.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_1_2
name wall
face cwall_1.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_1_3
name wall
face cwall_8.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_1_4
name wall
face cwall_2.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_2_1_1
name wall
face cwall_5.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_2_1_2
name wall
face cwall_A.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_2_2_1
name wall
face cwall_3.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_2_2_2
name wall
face cwall_6.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_2_2_3
name wall
face cwall_C.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_2_2_4
name wall
face cwall_9.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_3_1
name wall
face cwall_B.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_3_2
name wall
face cwall_7.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_3_3
name wall
face cwall_E.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_3_4
name wall
face cwall_D.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_4
name wall
face cwall_F.x11
move_block all
no_pick 1
blocksview 1
end

