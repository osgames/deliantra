object cwall_beigemarble
name marble wall column
face cwall_beigemarble.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_bluemarblemedium
name marble wall column
face cwall_bluemarblemedium.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_cyanmarble
name marble wall column
face cwall_cyanmarble.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_cyanmarbledark
name marble wall column
face cwall_cyanmarbledark.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_graymarble
name marble wall column
face cwall_graymarble.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_greenmarble
name marble wall column
face cwall_greenmarble.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_greenmarblemedium
name marble wall column
face cwall_greenmarblemedium.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_magentamarble
name marble wall column
face cwall_magentamarble.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_marble
name marble wall column
face cwall_marble.x11
move_block all
no_pick 1
blocksview 1
end

object cwall_pinkmarble
name marble wall column
face cwall_pinkmarble.x11
move_block all
no_pick 1
blocksview 1
end

