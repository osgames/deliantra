object magentaguard_house_1
name Guard House
face magentaguard_ho_8.x11
type 66
move_on walk fly_low
no_pick 1
blocksview 1
end

object magentaguard_house_2
name Guard House
face magentaguard_ho_2.x11
type 66
move_on walk fly_low
no_pick 1
blocksview 1
end

object magentaguard_house_3
name Guard House
face magentaguard_ho_4.x11
type 66
move_on walk fly_low
no_pick 1
blocksview 1
end

object magentaguard_house_4
name Guard House
face magentaguard_ho_1.x11
type 66
move_on walk fly_low
no_pick 1
blocksview 1
end

