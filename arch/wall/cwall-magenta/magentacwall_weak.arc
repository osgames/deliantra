object magentaweak_wall_3
anim
magentacwall_w_0.x11
magentacwall_w_0.x12
magentacwall_w_0.x13
magentacwall_w_0.x14
magentacwall_w_0.x15
magentacwall_w_0.x16
magentacwall_w_0.x17
magentacwall_w_0.x18
magentacwall_w_0.x19
magentacwall_w_0.x1A
mina
name wall
race wall
face magentacwall_w_0.x11
is_animated 0
hp 100
maxhp 100
ac 30
level 1
resist_magic 100
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
alive 1
no_pick 1
blocksview 1
tear_down 1
end

object magentaweak_wall_4
anim
magentacwall_w_1.x11
magentacwall_w_1.x12
magentacwall_w_1.x13
magentacwall_w_1.x14
magentacwall_w_1.x15
magentacwall_w_1.x16
magentacwall_w_1.x17
magentacwall_w_1.x18
magentacwall_w_1.x19
mina
name wall
race wall
face magentacwall_w_1.x11
is_animated 0
hp 100
maxhp 100
ac 30
level 1
resist_magic 100
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
alive 1
no_pick 1
blocksview 1
tear_down 1
end

