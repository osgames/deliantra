object door_0
inherit type_door
name door
face door_0.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object door_1_1
inherit type_door
name door
face door_1.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object door_1_2
inherit type_door
name door
face door_4.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object door_1_3
inherit type_door
name door
face door_2.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object door_1_4
inherit type_door
name door
face door_8.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object door_2_1_1
inherit type_door
name door
face door_5.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object door_2_1_2
inherit type_door
name door
face door_A.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object door_2_2_1
inherit type_door
name door
face door_3.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object door_2_2_2
inherit type_door
name door
face door_6.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object door_2_2_3
inherit type_door
name door
face door_C.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object door_2_2_4
inherit type_door
name door
face door_9.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object door_3_1
inherit type_door
name door
face door_B.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object door_3_2
inherit type_door
name door
face door_7.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object door_3_3
inherit type_door
name door
face door_E.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object door_3_4
inherit type_door
name door
face door_D.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object door_4
inherit type_door
name door
face door_F.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

