object odoor_2_fant_blue-green
inherit type_door
name door
face odoor_2_fant_blue-green.x11
hp 400
exp 1
ac 10
level 1
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object odoor_2_fant_blue-yellow
name door
face odoor_2_fant_blue-yellow.x11
hp 400
exp 1
ac 10
level 1
type 23
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object odoor_2_fant_brown
name door
face odoor_2_fant_brown.x11
hp 400
exp 1
ac 10
level 1
type 23
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object odoor_2_fant_red-white
name door
face odoor_2_fant_red-white.x11
hp 400
exp 1
ac 10
level 1
type 23
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

object odoor_2_fant_red-yellow
name door
face odoor_2_fant_red-yellow.x11
hp 400
exp 1
ac 10
level 1
type 23
materialname wood
randomitems door
move_block all
alive 1
no_pick 1
blocksview 1
end

