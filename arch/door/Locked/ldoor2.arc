object locked_door2
inherit type_locked_door
name locked door
slaying set_individual_value
msg
You need the special key to open this door.
endmsg
face ldoor2.x11
move_block all
no_pick 1
no_magic 1
damned 1
end

