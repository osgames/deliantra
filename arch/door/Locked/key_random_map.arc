object key_random_map
inherit key2
name some random key
name_pl some random keys
msg
It was found in an otherwise unremarkable area.
It probably opens some nearby treasure chest or door.
It looks very fragile, so it might be better to hurry.
endmsg
food 100
speed 0.003
speed_left -1
is_used_up 1
end

