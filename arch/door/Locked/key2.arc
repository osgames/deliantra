object key2
name strange key
name_pl strange keys
race keys
slaying set_individual_value
face key2.x11
type 21
value 100
weight 100
client_type 810
end

object key2_blue
inherit key2
name strange key
name_pl strange keys
face key_blue.x11
end

object key2_brown
inherit key2
name strange key
name_pl strange keys
face key_brown.x11
end

object key2_white
inherit key2
name strange key
name_pl strange keys
face key_white.x11
end

object key2_darkgray
inherit key2
name strange key
name_pl strange keys
face key_darkgray.x11
end

object key2_darkgreen
inherit key2
name strange key
name_pl strange keys
face key_darkgreen.x11
end

object key2_gray
inherit key2
name strange key
name_pl strange keys
face key_gray.x11
end

object key2_green
inherit key2
name strange key
name_pl strange keys
face key_green.x11
end

object key2_magenta
inherit key2
name strange key
name_pl strange keys
face key_magenta.x11
end

object key2_red
inherit key2
name strange key
name_pl strange keys
face key_red.x11
end

object key2_skull
inherit key2
name strange key
name_pl strange keys
face key_skull.x11
end

