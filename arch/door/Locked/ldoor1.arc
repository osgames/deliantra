object locked_door1
inherit type_locked_door
name locked door
slaying set_individual_value
msg
You need the special key to open this door.
endmsg
face ldoor1.x11
move_block all
no_pick 1
no_magic 1
damned 1
end

object locked_door_blue1
inherit type_locked_door
name locked door
slaying set_individual_value
msg
You need the special key to open this door.
endmsg
face ldoor_blue1.x11
move_block all
no_pick 1
no_magic 1
damned 1
end

object locked_door_blue2
inherit type_locked_door
name locked door
slaying set_individual_value
msg
You need the special key to open this door.
endmsg
face ldoor_blue2.x11
move_block all
no_pick 1
no_magic 1
damned 1
end

object locked_door_brown1
inherit type_locked_door
name locked door
slaying set_individual_value
msg
You need the special key to open this door.
endmsg
face ldoor_brown1.x11
move_block all
no_pick 1
no_magic 1
damned 1
end

object locked_door_brown2
inherit type_locked_door
name locked door
slaying set_individual_value
msg
You need the special key to open this door.
endmsg
face ldoor_brown2.x11
move_block all
no_pick 1
no_magic 1
damned 1
end

object locked_door_white1
inherit type_locked_door
name locked door
slaying set_individual_value
msg
You need the special key to open this door.
endmsg
face ldoor_white1.x11
move_block all
no_pick 1
no_magic 1
damned 1
end

object locked_door_white2
inherit type_locked_door
name locked door
slaying set_individual_value
msg
You need the special key to open this door.
endmsg
face ldoor_white2.x11
move_block all
no_pick 1
no_magic 1
damned 1
end

