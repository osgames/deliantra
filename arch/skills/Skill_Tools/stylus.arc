object stylus
inherit type_skill_tool
name writing pen
name_pl writing pens
skill inscription
face stylus.x11
materialname organic
value 5000
weight 100
client_type 451
body_arm -1
end

