object holy_symbol
inherit type_skill_tool
name holy symbol
name_pl holy symbols
skill praying
face holy_symbol.x11
nrof 1
materialname ancient wood
value 1500
weight 5000
client_type 451
end

