object talisman
name talisman
name_pl talismans
face talisman.x11
nrof 1
type 39
materialname runestone
value 300
weight 1000
client_type 451
body_neck -1
end

object talisman_evocation
inherit type_skill_tool
name talisman
name_pl talismans
title of evocation
skill evocation
face talisman.x11
nrof 1
materialname runestone
value 3000
weight 1000
client_type 451
body_neck -1
end

object talisman_pyromancy
inherit type_skill_tool
name talisman
name_pl talismans
title of pyromancy
skill pyromancy
face talisman.x11
nrof 1
materialname runestone
value 3000
weight 1000
client_type 451
body_neck -1
end

object talisman_sorcery
inherit type_skill_tool
name talisman
name_pl talismans
title of sorcery
skill sorcery
face talisman.x11
nrof 1
materialname runestone
value 3000
weight 1000
client_type 451
body_neck -1
end

object talisman_summoning
inherit type_skill_tool
name talisman
name_pl talismans
title of summoning
skill summoning
face talisman.x11
nrof 1
materialname runestone
value 3000
weight 1000
client_type 451
body_neck -1
end

