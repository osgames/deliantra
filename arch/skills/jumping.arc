object skill_jumping
inherit type_skill
name jumping
skill jumping
msg
You can "skip" over one, two or three spaces in the selected
direction. Distance depends on weight carried, L<strength|stat_Str> and
L<dexterity|stat_Dex> of the user. This skill may also be used as an
attack.
endmsg
exp 5
dam 5
level 250
subtype 10
attacktype 1
invisible 1
last_sp 1
no_drop 1
end

