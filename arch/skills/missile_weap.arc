object skill_missile_weapon
inherit type_skill
name missile weapons
skill missile weapons
msg
The user is capable of making attacks with ranged weapons (eg bow, crossbow).
Fire arrows by applying a bow and using the fire and direction keys. To fire
specific arrows to kill a monster that is weak against a certain arrow, mark
this arrow and fire.
endmsg
exp 0
dam 1
level 100
subtype 24
invisible 1
no_drop 1
end

