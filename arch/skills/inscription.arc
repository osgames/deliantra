object skill_inscription
inherit type_skill
name inscription
skill inscription
msg
You can inscribe empty scrolls and books with a text. This is useful to
create messages, especially via the Imperial Post Office (IPO).

You can also write spell scrolls with a spell known to you. You need
L<mana|stat_mana>, time and an old scroll for that. Backfire effects are
possible. Only available as a 'writing pen'.

How to write into a mail scroll/books etc.:

   mark <name of scroll> # or use the menu
   use_skill inscription <your message goes here>

How to rewrite a spell scroll:

   mark <name of scroll> # or use the menu
   cast <spell>          # prepare the spell you want to write
   use_skill inscription # write the spell on the scroll

If you succeed, the scroll will be a scroll of the spell you chose and
your level of inscription as its level. Hence, you don't need to be that
level in the spell skill to write higher level spellscrolls.

Caution: Magic spell writing can be risky to one's health. There can be
some negative effects if one fails to write a spell scroll. The most
benign is that one becomes confused for a short time. Other effects are
more severe and can even be dangerous.
endmsg
exp 100
level 250
subtype 22
invisible 1
no_drop 1
end

