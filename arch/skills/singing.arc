object skill_singing
inherit type_skill
name singing
skill singing
msg
You may pacify hostile monsters with this area attack skill (it is
directionless and affects the area around you - the higher the skill,
the larger the area). Certain kinds of monsters are immune. Success
depends on user's level and her L<charisma|stat_Cha> vs. the monster's
L<intelligence|stat_Int> and level.

Singing is a good companion skill for the L<oratory
skill|skill_description/oratory>: first you pacify creates using singing,
then you can orate them, convincing them if your cause. It is a useful
skill when monsters are overwhelming you, too.
endmsg
exp 10
level 150
subtype 13
invisible 1
no_drop 1
end

