object skill_find_traps
inherit type_skill
name find traps
skill find traps
msg
You can search (more effectively) for traps. This is not a "passive"
skill, it must be applied in order to gain the advantage in discovering
traps, or actively used when in vicinity of a trap. To use it, fire into
any direction.
endmsg
exp 100
level 150
subtype 15
invisible 1
no_drop 1
end

