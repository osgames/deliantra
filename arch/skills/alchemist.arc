object skill_alchemy
inherit type_skill
name alchemy
skill alchemy
msg
You can identify potions, containers, and different kinds of ore and
mineral, mix potions and create other stuff (such as gold, weapons, deadly
monsters killing him/her etc.) from other ingredients using a cauldron.

To identify alchemy stuff you have to stand over it or have it in your inventory and
issue the command C<use_skill alchemy>. The alchemy items will be identified. Some might
not get identified if your alchemy level is too low.

A higher alchemy level will also help you to estimate the value of items better.

To create something new, for example some dust or potion, you have to get a B<cauldron>.
You find these in shops, guilds and some appartments. Some alchemy shops also let you use
their cauldron for a fee. All you need now is to know what items creates which other item.
You learn the ingredients this by finding recipes. It is B<very important> that you B<identify>
all ingredients for a recipe before using them!

For a start you could try to make B<water of the wise>:

Get 7 water bottles (filled), identify them, put them in a cauldron, do C<use_skill alchemy>.
If everything worked there should be B<water of the wise> in the cauldron now.
Congratulations, your first alchemy was done!

Be careful when doing alchemy and trying complicated receipes, they might fail and
will backfire heavily at you.

I<Never, ever, do alchemy in your home or in public places!> You can
easily create powerful monsters that will kill you, kill you again when
you wake up in your savebed, kill you again... etc.
endmsg
exp 250
level 250
subtype 6
invisible 1
no_drop 1
end

