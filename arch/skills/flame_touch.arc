object skill_flame_touch
inherit type_skill_hth
name flame touch
skill flame touch
msg
You can make a "bare-handed attack". Damage is based on the user's
L<strength|stat_Str> and level. This is the default hand-to-hand fighting
skill for the fireborn character class.
endmsg
exp 0
dam 2
level 100
subtype 18
attacktype 4
invisible 1
last_sp 4
no_drop 1
end

