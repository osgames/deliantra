object skill_lockpick
inherit type_skill
name lockpicking
skill lockpicking
msg
You may "pick locks" (open doors). You need to have readied some
B<lockpicks> to use this skill.
endmsg
exp 10
level 200
subtype 1
invisible 1
no_drop 1
end

