object skill_smithery
inherit type_skill
name smithery
skill smithery
msg
You can identify arms and armour that you hold, and, with the help of a
B<forge>, can even create weapons using this skill.
endmsg
exp 50
level 250
subtype 3
invisible 1
no_drop 1
end

