object skill_evocation
inherit type_skill
name evocation
skill evocation
msg
You can cast spells related to the B<evocation> (mostly cold magic)
school of wizardry. This skill may be acquired either through the use of
an appropriate B<talisman> or learned via a L<skill scroll>.

The spell you should start with is 'icestorm' which casts a cone
of ice which freezes your enemies. It becomes stronger with your evocation level.
endmsg
exp 0
level 100
subtype 35
invisible 1
no_drop 1
end

