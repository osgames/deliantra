object skill_oratory
inherit type_skill
name oratory
skill oratory
msg
You may "recruit" followers targeting them with your B<oratory>
skill. Recruitees must be unaggressive to start (a good way to pacify
monsters is the L<singing skill|skill_description/singing>). Use of
this skill may anger the audience. Also, some monsters are immune to
recruitment. Success depends on user's level and her L<charisma|stat_Cha>
vs. the recruitee's L<intelligence|stat_Int> and level.

Kill experience from your followers goes directly to your oratory skill.

Cleaning women and unagggressive low-level monsters (such as the dogs in
the wizards house in scorn) are good initial targets for this skill.
endmsg
exp 10
level 250
subtype 12
invisible 1
no_drop 1
end

