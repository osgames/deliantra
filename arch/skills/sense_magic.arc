object skill_sense_magic
inherit type_skill
name sense magic
skill sense magic
msg
You can detect whether items in your inventory or on the floor are
B<magic> by using this skill.
endmsg
exp 5
level 100
subtype 11
invisible 1
no_drop 1
end

