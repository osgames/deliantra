object skill_bargaining
inherit type_skill
name bargaining
skill bargaining
msg
This skill helps you to bargain in shops when buying and selling. The higher
your bargaining level is the better prices you will get. Besides having a high
bargaining skill it helps a lot to have a high L<charisma|stat_Cha> stat.

When you are in a shop, you can C<use_skill bargaining> to see whether that
shop will buy the items you want to sell at a higher price than others and
whether the shopkeeper likes you. Elf shopkeepers will like elf players.
endmsg
exp 50
level 100
subtype 9
invisible 1
no_drop 1
end

