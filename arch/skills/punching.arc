object skill_punching
inherit type_skill_hth
name punching
skill punching
msg
You can make a "bare-handed attack". Damage is based on the user's
L<strength|stat_Str> and level. This is the most feeble of the
hand-to-hand attacks.
endmsg
exp 0
level 100
subtype 17
attacktype 1
invisible 1
no_drop 1
end

