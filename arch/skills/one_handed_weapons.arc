object skill_one_handed_weapon
inherit type_skill
name one handed weapons
skill one handed weapons
msg
You can use one-handed hand-held weapons (e.g. B<shortsword>, B<dagger>,
B<mace>, etc). Wield and ready the weapon by applying it and use the fire and
direction keys.
endmsg
exp 0
dam 1
level 100
subtype 23
invisible 1
no_drop 1
end

