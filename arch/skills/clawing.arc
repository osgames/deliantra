object skill_clawing
inherit type_skill_hth
name clawing
skill clawing
msg
You can make a "bare-handed attack". Damage is based on the user's
L<strength|stat_Str> and level. With time, one can have different claws
such as fire claws or electric claws. An elemental residue from the dragon
guild is required to change this. The dragon guild can be found in scorn.
endmsg
exp 0
dam 2
level 100
subtype 31
attacktype 1
invisible 1
last_sp 3
no_drop 1
end

