object skill_throwing
inherit type_skill
name throwing
skill throwing
msg
You can throw items at monsters. To do this, fire at a monster with this skill
readied. You can chose what to throw by marking an item.
endmsg
exp 0
dam 1
level 100
subtype 25
invisible 1
no_drop 1
end

