object skill_poison_nail
inherit type_skill_hth
name poison nail
skill poison nail
msg
You can do a "bare-handed attack" and inject poison into the opponent with
this skill readied. Attacking is done by running into the opponent. This is a
unique skill that can only be used by goblins.
endmsg
exp 0
dam 2
level 100
subtype 41
attacktype 1025
invisible 1
last_sp 4
no_drop 1
end

