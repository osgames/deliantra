object skill_pyromancy
inherit type_skill
name pyromancy
skill pyromancy
msg
You can cast spells related to the B<pyromancy> (fire) school of
wizardry. This skill may be acquired either through the use of an
appropriate B<talisman> or learned via a B<skill scroll>.

The first spell would be 'burning hands' which casts a cone of fire which
burns your enemies. It becomes stronger with your pyromancy level.
endmsg
exp 0
level 100
subtype 34
invisible 1
no_drop 1
end

