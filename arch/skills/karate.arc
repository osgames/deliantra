object skill_karate
inherit type_skill_hth
name karate
skill karate
msg
You can make a "bare-handed attack". Damage is based on the user's
L<strength|stat_Str> and level. This attack is the fastest and (at higher
levels) most deadly of the hand-to-hand attacks available.
endmsg
exp 0
dam 4
level 100
subtype 19
attacktype 1
invisible 1
last_sp 2
no_drop 1
end

