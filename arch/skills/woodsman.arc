object skill_woodsman
inherit type_skill
name woodsman
skill woodsman
msg
While the skill is readied, the possessor will move faster through
"wooded" terrain (B<forest>, B<grasslands>, B<brush>, B<jungle>, etc.).

Woodsmen can also craft various items useful to their skill.
endmsg
exp 25
level 100
subtype 21
invisible 1
no_drop 1
end

