object skill_sense_curse
inherit type_skill
name sense curse
skill sense curse
msg
You can detect whether items in your inventory or on the floor are
B<cursed> by using this skill.
endmsg
exp 5
level 100
subtype 14
invisible 1
no_drop 1
end

