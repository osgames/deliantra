object skill_shiver
inherit type_skill_hth
name shiver
skill shiver
msg
You can do a "bare-handed attack" and send waves of frost to the opponent with
this skill readied. Attacking is done by running into the opponent. This is a
unique skill that can only be used by iceborns.
endmsg
exp 0
dam 2
level 100
subtype 39
attacktype 16
invisible 1
last_sp 4
no_drop 1
end

