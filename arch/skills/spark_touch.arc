object skill_spark_touch
inherit type_skill_hth
name spark touch
skill spark touch
msg
You can do a "bare-handed attack" and shock the opponent with this skill
readied. Attacking is done by running into the opponent. This is a unique skill
that can only be used by sparklings.
endmsg
exp 0
dam 2
level 100
subtype 38
attacktype 10
invisible 1
last_sp 4
no_drop 1
end

