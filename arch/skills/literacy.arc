object skill_literacy
inherit type_skill
name literacy
skill literacy
msg
With this skill you can identify books and scrolls in your inventory,
giving you more experience that you can invest into identifying higher
level books and scrolls.

Literacy is also required to read books and scrolls, and to learn new
spells and skills from books and scrolls.
endmsg
exp 10
level 250
subtype 8
invisible 1
no_drop 1
end

