object skill_levitate
inherit type_skill
name levitation
skill levitation
msg
You can levitate (fly low) at will. Using the skill first makes you
levitate, and at the second time returns you to the earth.
Levitation can be useful to fly over water streams that move you
if you walk in them, or it prevents you from falling into a pit.
endmsg
exp 50
level 100
subtype 32
invisible 1
last_sp 2
move_type fly_low
no_drop 1
body_skill 0
end

