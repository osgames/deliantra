object skill_two_handed_weapon
inherit type_skill
name two handed weapons
skill two handed weapons
msg
You can use two-handed hand-held weapons (e.g. B<longsword>, B<club>, etc).
Wield and ready the weapon by applying it and use the fire and direction keys.
endmsg
exp 0
level 100
subtype 37
invisible 1
no_drop 1
end

