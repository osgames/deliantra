object skill_acid_splash
inherit type_skill_hth
name acid splash
skill acid splash
msg
You can do a "bare-handed attack" and splash the opponent with acid using this
skill. Attacking is done by running into the opponent. This is a unique skill
that can only be used by acidborns.
endmsg
exp 0
dam 2
level 100
subtype 40
attacktype 64
invisible 1
last_sp 4
no_drop 1
end

