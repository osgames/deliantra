object skill_remove_trap
inherit type_skill
name disarm traps
skill disarm traps
msg
You can disarm previously discovered traps (for example, with the B<find
traps> skill).
endmsg
exp 50
level 350
subtype 27
invisible 1
no_drop 1
end

