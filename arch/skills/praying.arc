object skill_praying
inherit type_skill
name praying
skill praying
msg
The praying skill allows you to cast "cleric" spells. In addition, this
skill may be used to accelerate the accumulation of grace points by
praying. This skill may be either learned (e.g. using a L<skill scroll>)
or acquired through the use of a B<holy symbol>.

Since praying can keep your character busy for a while, it is best to
C<ready_skill praying>, and then keep firing into any direction. That way,
you can instantly stop praying when something unexpected shows up.

One of the best praying spells to start with is the 'holy word' spell and
later, if you find it, the 'banishment' spell, which is much stronger.
endmsg
exp 0
level 100
subtype 30
invisible 1
no_drop 1
end

