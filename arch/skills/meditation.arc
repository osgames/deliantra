object skill_meditation
inherit type_skill
name meditation
skill meditation
msg
Player can regain mana/hp at an accelerated rate by C<use_skill
meditation>. Player must first strip off encumbering armour however. This
skill is only available to the "monk" character class.
endmsg
exp 10
level 100
subtype 16
invisible 1
no_drop 1
end

