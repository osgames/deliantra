object skill_bowyer
inherit type_skill
name bowyer
skill bowyer
msg
You can identify missile weapons and missiles such as arrows, bolts and
bows. One can also create special bows and arrows from other ingredients
using the bowyer skill like the L<alchemy skill|skill_description/alchemy>
skill and a B<workbench> (you just have to find some recipes).
endmsg
exp 100
level 150
subtype 4
invisible 1
no_drop 1
end

