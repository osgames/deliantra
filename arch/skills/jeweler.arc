object skill_jeweler
inherit type_skill
name jeweler
skill jeweler
msg
You can identify amulets, rings and talismans. One can also improve magic
rings or enchant non-magic rings to give them stat bonuses, this works
similar to the L<alchemy skill|skill_description/alchemy>, only that
you need to put the ingredients in a B<jeweler's workbench>.

For a more detailed documentation look in the L<jeweler skill documentation|jeweler_skill>.
endmsg
exp 100
level 300
subtype 5
invisible 1
no_drop 1
end

