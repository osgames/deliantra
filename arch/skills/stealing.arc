object skill_stealing
inherit type_skill
name stealing
skill stealing
msg
You can take items from the inventory of NPCs, monsters and maybe other
players.
endmsg
exp 20
level 70
subtype 7
invisible 1
no_drop 1
end

