object skill_climbing
inherit type_skill
name climbing
skill climbing
msg
Having this skill allows you to move faster through hilly areas than players
without this skill. B<Note:> You can't gain experience in this skill. Having
this skill is enough to move faster.
endmsg
exp 100
level 100
subtype 20
invisible 1
no_drop 1
end

