object skill_summoning
inherit type_skill
name summoning
skill summoning
msg
You can cast spells related to the B<summoning> school of wizardry, which
mostly summon monsters, bullet walls and similar things. This skill
may be acquired either through the use of an appropriate B<talisman> or
learned via a L<skill scroll>.

The most basic spells you might want to get is 'summon pet monster'
and 'charm monsters'. Look in the L<Spellbook|spellbook> if you got
the spells and read their description.
endmsg
exp 0
level 100
subtype 33
invisible 1
no_drop 1
end

