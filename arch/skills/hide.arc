object skill_hide
inherit type_skill
name hiding
skill hiding
msg
This skill lets you hide, that means that you will become invisible.
To hide you need to find a place with other stuff on it where you can hide.

You enjoy limited form of invisibility. If you attack or move too much
you become visible.
endmsg
exp 1
level 100
subtype 2
invisible 1
no_drop 1
end

