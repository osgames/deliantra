object skill_sorcery
inherit type_skill
name sorcery
skill sorcery
msg
You can cast spells related to the B<sorcery> (identify, town portal and
other meta-spells) school of wizardry. This skill may be acquired either
through the use of an appropriate B<talisman> or learned via a B<skill
scroll>.

Sorcery has many utility spells, like 'identify', 'town portal' (making portals
between two points in the world) and also attack spells like 'spark shower'
and 'steambolt'.
endmsg
exp 0
level 100
subtype 36
invisible 1
no_drop 1
end

