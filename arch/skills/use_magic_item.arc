object skill_use_magic_item
inherit type_skill
name use magic item
skill use magic item
msg
You can use magic items like rods/wands/horns. The level of experience
influences how powerful the spells are you can evoke from the
rod/wand/horn.
endmsg
exp 0
level 100
subtype 26
invisible 1
no_drop 1
end

