object skill_thaumaturgy
inherit type_skill
name thaumaturgy
skill thaumaturgy
msg
You can identify rods, wands and horns that you are holding, and, with
the help of a B<thaumaturgists workbench> and the alchemy skill, create
those things.
endmsg
exp 150
level 200
subtype 29
invisible 1
no_drop 1
end

