object slimecrawler
anim
slimecrawler.x11
slimecrawler.x12
slimecrawler.x13
slimecrawler.x12
mina
name slime crawler
race slime
face slimecrawler.x12
is_animated 1
con 1
wis 5
int 0
hp 70
maxhp 70
exp 400
dam 12
wc 3
ac 4
speed -0.08
attack_movement 5
level 8
attacktype 81
resist_physical 40
resist_cold 50
resist_acid 90
resist_blind 80
weight 150000
pick_up 6
alive 1
monster 1
see_invisible 1
hitback 1
sleep 1
can_see_in_dark 1
anim_speed 6
end

