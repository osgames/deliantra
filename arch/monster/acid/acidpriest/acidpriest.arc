object acidpriest
anim
facings 8
acidpriest.x11
acidpriest.x12
acidpriest.x13
acidpriest.x14
acidpriest.x15
acidpriest.x16
acidpriest.x17
acidpriest.x18
mina
name Acid priest
race unnatural
face acidpriest.x12
str 35
dex 23
con 40
pow 60
int 25
hp 80000
maxhp 80000
sp 500
maxsp 800
exp 25000000
dam 95
wc -100
ac -120
speed 1.5
attack_movement 0
level 100
attacktype 464323
resist_physical 98
resist_magic 85
resist_fire 85
resist_electricity 60
resist_cold 100
resist_confusion 85
resist_acid 100
resist_drain 98
resist_weaponmagic 10
resist_death 60
resist_chaos 85
resist_godpower 85
resist_holyword 85
resist_blind 85
path_attuned 393232
path_repelled 589824
weight 15000
randomitems acidpriest_big
run_away 5
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
hitback 1
reflect_missile 1
reflect_spell 1
can_cast_spell 1
can_use_ring 1
can_see_in_dark 1
movement_type 4
end
more
object acidpriest_2
anim
facings 8
acidpriest.x11
acidpriest.x12
acidpriest.x13
acidpriest.x14
acidpriest.x15
acidpriest.x16
acidpriest.x17
acidpriest.x18
mina
name Acid priest
face acidpriest.x12
x 1
move_type fly_low
alive 1
no_pick 1
end
more
object acidpriest_3
anim
facings 8
acidpriest.x11
acidpriest.x12
acidpriest.x13
acidpriest.x14
acidpriest.x15
acidpriest.x16
acidpriest.x17
acidpriest.x18
mina
name Acid priest
face acidpriest.x12
y 1
move_type fly_low
alive 1
no_pick 1
end
more
object acidpriest_4
anim
facings 8
acidpriest.x11
acidpriest.x12
acidpriest.x13
acidpriest.x14
acidpriest.x15
acidpriest.x16
acidpriest.x17
acidpriest.x18
mina
name Acid priest
face acidpriest.x12
x 1
y 1
move_type fly_low
alive 1
no_pick 1
end

