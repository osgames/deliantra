object slime
anim
slime.x11
slime.x12
slime.x13
mina
race slime
other_arch slime
face slime.x11
wis 5
int 0
hp 5
maxhp 5
maxsp 10
exp 7
dam 4
wc 18
ac 10
speed 0.06
level 2
resist_blind 100
weight 20000
alive 1
no_pick 1
monster 1
generator 1
sleep 1
can_see_in_dark 1
end

