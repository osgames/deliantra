object pet_necro
anim
pet_necro.x11
pet_necro.x12
mina
name pet of Necromancer
race animal
face pet_necro.x11
con 1
wis 15
int 8
hp 100
maxhp 100
exp 300
dam 20
wc -100
ac -10
speed -1.000000
level 80
attacktype 1
resist_fire -100
weight 30000
alive 1
monster 1
hitback 1
sleep 1
end

