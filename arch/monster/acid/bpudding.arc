object black_pudding
anim
bpudding.x11
bpudding.x12
mina
name black pudding
race slime
other_arch black_pudding_split
face bpudding.x11
con 1
wis 15
int 0
hp 250
maxhp 250
exp 800
food 1
dam 15
wc 2
ac 8
speed -0.1
level 5
attacktype 64
resist_fire -100
resist_blind 100
weight 20000
alive 1
monster 1
splitting 1
hitback 1
sleep 1
can_see_in_dark 1
end

object black_pudding_grow
anim
bpudding_g.x11
bpudding_g.x12
bpudding_g.x13
bpudding_g.x14
bpudding_g.x15
bpudding_g.x16
bpudding_g.x17
bpudding_g.x18
bpudding_g.x19
mina
name black pudding
other_arch black_pudding
face bpudding_g.x11
con 1
int 0
hp 250
maxhp 250
exp 400
food 1
dam 10
wc 2
ac 8
speed 0.1
level 5
attacktype 64
resist_fire -100
weight 20000
alive 1
monster 1
changing 1
hitback 1
end

object black_pudding_split
anim
bpudding_s.x11
bpudding_s.x12
bpudding_s.x13
bpudding_s.x14
mina
name black pudding
other_arch black_pudding_grow
face bpudding_s.x11
con 1
int 0
hp 250
maxhp 250
exp 400
food 2
dam 10
wc 2
ac 8
speed 0.1
level 5
attacktype 64
resist_fire -100
weight 20000
alive 1
monster 1
changing 1
hitback 1
end

