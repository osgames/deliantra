object acid_sphere
anim
acidsphere.x11
acidsphere.x12
acidsphere.x13
acidsphere.x14
acidsphere.x13
acidsphere.x12
mina
name acid sphere
race slime
face acidsphere.x11
wis 5
int 0
hp 1
maxhp 1
exp 100
dam 100
wc 1
ac 1
speed 0.01
level 5
attacktype 64
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
resist_weaponmagic 100
resist_ghosthit 100
resist_poison 100
resist_slow 100
resist_paralyze 100
resist_turn_undead 100
resist_blind 100
weight 1
alive 1
monster 1
hitback 1
sleep 1
one_hit 1
anim_speed 1
end

object bluesphere
anim
bluesphere.x11
bluesphere.x12
bluesphere.x13
bluesphere.x14
mina
name acid sphere
race slime
face bluesphere.x11
wis 5
int 0
hp 1
maxhp 1
exp 100
dam 100
wc 1
ac 1
speed 0.01
level 5
attacktype 64
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
resist_weaponmagic 100
resist_ghosthit 100
resist_poison 100
resist_slow 100
resist_paralyze 100
resist_turn_undead 100
resist_blind 100
weight 1
alive 1
monster 1
hitback 1
sleep 1
one_hit 1
anim_speed 1
end

