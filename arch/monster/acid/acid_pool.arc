object acid_pool
anim
acid_pool.x11
acid_pool.x12
mina
name acid pool
race slime
other_arch acid_sphere
face acid_pool.x11
hp 100
maxhp 100
exp 100
ac 8
speed 0.01
level 1
resist_blind 100
weight 750000
alive 1
no_pick 1
generator 1
can_see_in_dark 1
anim_speed 10
end

