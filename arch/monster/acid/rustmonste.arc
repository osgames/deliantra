object rustmonster
anim
facings 2
rustmonste.x31
rustmonste.x32
rustmonste.x71
rustmonste.x72
mina
name rustmonster
race animal
face rustmonste.x31
con 1
wis 15
int 6
hp 100
maxhp 100
exp 300
dam 0
wc -5
ac -10
speed -0.5
level 4
attacktype 64
resist_fire -100
weight 30000
alive 1
monster 1
hitback 1
sleep 1
end

