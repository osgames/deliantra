object green_slime
anim
greenslime.x11
greenslime.x12
mina
name green slime
race slime
face greenslime.x11
wis 5
int 1
hp 20
maxhp 20
exp 200
dam 40
wc 5
ac 9
speed -0.1
level 5
attacktype 64
resist_blind 100
weight 5000
alive 1
monster 1
hitback 1
sleep 1
can_see_in_dark 1
end

