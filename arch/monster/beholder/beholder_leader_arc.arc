object beholder_leader_arc
anim
facings 8
beholder.x11
beholder.x12
beholder.x13
beholder.x14
beholder.x15
beholder.x16
beholder.x17
beholder.x18
mina
name strange beholder
race unnatural
face beholder.x11
con 1
wis 15
pow 2
int 16
hp 95
maxhp 95
sp 15
maxsp 15
exp 2500
dam 20
wc 2
ac 2
speed -0.07
attack_movement 1
level 11
resist_physical 40
resist_magic 100
resist_poison 100
resist_paralyze 100
resist_fear 100
resist_blind -60
weight 1000
randomitems beholder
run_away 15
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
end

