object dread
anim
facings 8
dread.x11
dread.x12
dread.x13
dread.x14
dread.x15
dread.x16
dread.x17
dread.x18
mina
name Dread
race unnatural
face dread.x11
con 5
wis 20
pow 14
int 18
hp 1500
maxhp 1500
maxsp 30
exp 50000
dam 25
wc -20
ac -10
speed -0.30
attack_movement 1
level 15
resist_magic 90
resist_fire 100
resist_poison 100
resist_paralyze 100
resist_fear 100
resist_blind -50
weight 30000
randomitems dread
run_away 2
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
end
more
object dread_2
anim
facings 8
dread.x11
dread.x12
dread.x13
dread.x14
dread.x15
dread.x16
dread.x17
dread.x18
mina
name Dread
face dread.x11
x 1
move_type fly_low
alive 1
no_pick 1
end
more
object dread_3
anim
facings 8
dread.x11
dread.x12
dread.x13
dread.x14
dread.x15
dread.x16
dread.x17
dread.x18
mina
name Dread
face dread.x11
y 1
move_type fly_low
alive 1
no_pick 1
end
more
object dread_4
anim
facings 8
dread.x11
dread.x12
dread.x13
dread.x14
dread.x15
dread.x16
dread.x17
dread.x18
mina
name Dread
face dread.x11
x 1
y 1
move_type fly_low
alive 1
no_pick 1
end

