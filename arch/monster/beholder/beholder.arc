object beholder
anim
facings 8
beholder.x11
beholder.x12
beholder.x13
beholder.x14
beholder.x15
beholder.x16
beholder.x17
beholder.x18
mina
race unnatural
face beholder.x11
con 1
wis 15
pow 2
int 12
hp 80
maxhp 80
maxsp 15
exp 2000
dam 10
wc 7
ac 5
speed -0.07
attack_movement 1
level 9
resist_magic 100
resist_poison 100
resist_paralyze 100
resist_fear 100
resist_blind -75
weight 1000
randomitems beholder
run_away 15
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
end

