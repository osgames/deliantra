object generate_beholder
name generator
race unnatural
other_arch beholder
face behold_gen.x11
hp 100
maxhp 100
maxsp 1
exp 180
ac 13
speed -0.001
level 1
resist_magic 100
weight 90000
alive 1
generator 1
end

