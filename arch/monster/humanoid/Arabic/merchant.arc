object merchant
anim
facings 4
merchant.x11
merchant.x71
merchant.x11
merchant.x31
mina
name Merchant
race human
face merchant.x11
is_animated 0
str 13
dex 13
con 2
pow 1
int 14
hp 10
maxhp 10
sp 10
maxsp 10
exp 20
dam 4
wc 14
ac 6
speed 0.20
level 3
weight 50000
randomitems merchant
alive 1
can_use_shield 1
monster 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end

