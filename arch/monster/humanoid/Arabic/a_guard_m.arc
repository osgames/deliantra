object a_guard_m
name Medium guard
race human
face a_guard_m.x11
str 25
con 15
wis 10
pow 2
int 12
hp 120
maxhp 120
sp 20
maxsp 20
exp 250
dam 15
wc 1
ac 1
speed 0.06
level 6
resist_physical 65
resist_magic 30
weight 150000
randomitems guard_m
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
unaggressive 1
reflect_missile 1
can_cast_spell 1
can_use_armour 1
can_use_weapon 1
sleep 1
stand_still 1
body_arm 2
body_torso 1
body_head 1
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
end

