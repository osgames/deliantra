object dancing_girl
anim
dancing_girl.x11
dancing_girl.x12
dancing_girl.x13
mina
name Dancing girl
race human
face dancing_girl.x11
str 15
con 5
int 12
hp 10
maxhp 10
exp 10
dam 5
wc 18
ac 6
speed 0.5
level 1
weight 100000
randomitems dancing_girl
alive 1
no_pick 1
monster 1
unaggressive 1
can_use_bow 1
can_use_weapon 1
can_use_rod 1
body_range 1
body_arm 2
can_use_wand 1
random_movement 1
end

