object big_slave
name big slave
race human
face big_slave.x11
str 20
con 5
int 14
hp 30
maxhp 30
exp 20
dam 8
wc 15
ac 8
speed 0.16
level 1
weight 150000
randomitems slave
alive 1
can_use_shield 1
monster 1
unaggressive 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
body_arm 2
body_torso 1
body_head 1
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
end

