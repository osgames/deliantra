object holy_priest
anim
holy_priest.x11
holy_priest.x12
holy_priest.x13
mina
name Holy man
race human
face holy_priest.x11
str 25
con 40
wis 10
pow 30
int 30
hp 500
maxhp 500
sp 100
maxsp 100
exp 5000
dam 18
wc 19
ac -7
speed 0.16
attack_movement 7
level 17
resist_physical 100
resist_magic 100
resist_fire 30
resist_electricity 30
resist_cold 30
resist_confusion 30
resist_acid 30
resist_drain 100
resist_poison 30
resist_fear 100
resist_deplete 100
resist_death 100
resist_chaos 30
weight 100000
randomitems holy_priest
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end

