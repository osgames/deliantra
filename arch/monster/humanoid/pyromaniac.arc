object pyromaniac
anim
pyromaniac.x11
pyromaniac.x12
pyromaniac.x13
pyromaniac.x14
mina
name wild pyromaniac
race human
face pyromaniac.x11
str 15
dex 15
con 2
wis 20
pow 3
int 8
hp 100
maxhp 100
sp 300
maxsp 1
exp 500
dam 3
wc 5
ac 6
speed 0.2
level 4
resist_magic 100
weight 50000
randomitems pyromaniac
run_away 99
pick_up 0
alive 1
no_pick 1
monster 1
can_cast_spell 1
can_use_skill 1
body_skill 1
random_movement 1
end

