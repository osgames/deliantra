object laoch
anim
facings 8
laoch.x11
laoch.x21
laoch.x12
laoch.x22
laoch.x13
laoch.x23
laoch.x14
laoch.x24
laoch.x15
laoch.x25
laoch.x16
laoch.x26
laoch.x17
laoch.x27
laoch.x18
laoch.x28
mina
name Laoch
race human
face laoch.x11
str 100
dex 80
con 30
int 14
hp 9000
maxhp 9000
exp 20000
dam 150
wc -20
ac -120
speed 0.5
level 40
attacktype 410971
resist_physical 75
resist_magic 50
resist_fire 45
resist_electricity 45
resist_cold 65
resist_confusion 100
resist_acid 90
resist_weaponmagic 60
resist_poison 10
resist_slow 65
resist_paralyze 100
resist_fear 100
resist_cancellation 65
resist_death 65
resist_chaos 100
resist_counterspell 65
weight 50000
randomitems laoch
run_away 0
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

