object necromancer
anim
necro.x11
necro.x12
mina
name Necromancer
race human
face necro.x11
str 5
dex 5
wis 1
pow 5
int 20
hp 1800
maxhp 3600
sp 40
maxsp 40
dam 1
wc 25
ac 10
speed 1.000000
level 15
carrying 6000
weight 50000
randomitems sage
run_away 100
alive 1
monster 1
see_invisible 1
scared 1
unaggressive 1
can_cast_spell 1
sleep 1
can_use_skill 1
body_skill 1
end

