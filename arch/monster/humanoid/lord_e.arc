object lord_e
anim
lord_e.x11
lord_e.x12
mina
name Lord Eureca (commanded by Ghothwolte)
race human
face lord_e.x11
str 30
dex 30
con 1
wis 1
pow 1
int 1
hp 10000
maxhp 10000
sp 400
maxsp 400
exp 10000
dam 60
wc -50
ac -50
speed 0.95
speed_left -2.000000
level 30
attacktype 30
resist_physical 50
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion -100
resist_slow -100
resist_paralyze -100
carrying 90000
weight 50000
randomitems skill_use_magic_item
pick_up 24
will_apply 2
alive 1
monster 1
undead 1
can_use_scroll 1
can_use_bow 1
can_use_ring 1
can_use_skill 1
body_range 1
body_arm 2
body_skill 1
body_finger 2
can_use_wand 1
end

