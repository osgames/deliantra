object gnome
anim
gnome.x11
gnome.x12
mina
name cunning gnome
race dwarf
face gnome.x11
str 10
dex 13
con 8
wis 20
pow 1
int 15
hp 100
maxhp 100
sp 300
maxsp 300
exp 1000
dam 4
wc 5
ac 1
speed 0.4
level 4
attacktype 4098
resist_fire 100
resist_cold 100
resist_confusion 100
resist_turn_undead 100
weight 50000
randomitems gnome
run_away 99
pick_up 3
will_apply 15
alive 1
can_use_shield 1
no_pick 1
monster 1
reflect_spell 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
random_movement 1
end

