object dark_elf2
anim
facings 2
dark_elf2.x31
dark_elf2.x32
dark_elf2.x71
dark_elf2.x72
mina
name dark elf captain
race faerie
face dark_elf2.x31
str 13
dex 25
wis 25
pow 2
int 15
hp 40
maxhp 40
maxsp 40
exp 40
dam 8
wc 1
ac 1
speed 0.5
attack_movement 1
level 18
weight 50000
randomitems dark_elf
run_away 30
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end

