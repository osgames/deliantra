object dark_elf
anim
dark_elf.x11
dark_elf.x12
mina
name dark elf
race faerie
face dark_elf.x11
str 13
dex 25
wis 20
pow 1
int 15
hp 20
maxhp 20
maxsp 20
exp 20
dam 4
wc 1
ac 1
speed 0.5
attack_movement 1
level 9
weight 50000
randomitems dark_elf
run_away 30
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end

