object medium_guard
name castle guard
race human
face guard_med.x11
str 25
con 15
wis 10
pow 2
int 12
hp 120
maxhp 120
sp 20
maxsp 20
exp 250
dam 15
wc 1
ac 1
speed 0.06
level 6
resist_physical 65
resist_magic 30
weight 150000
alive 1
no_pick 1
monster 1
see_invisible 1
unaggressive 1
reflect_missile 1
can_cast_spell 1
sleep 1
stand_still 1
end

