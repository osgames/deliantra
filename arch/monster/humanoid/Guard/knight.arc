object knight
anim
knight.x11
knight.x12
knight.x13
knight.x14
knight.x15
mina
name Royal Guard
race human
face knight.x11
str 25
con 15
wis 15
pow 3
int 16
hp 250
maxhp 250
sp 25
maxsp 25
exp 1500
dam 15
wc 4
ac 0
speed 0.1
level 10
resist_physical 75
resist_magic 100
weight 150000
alive 1
no_pick 1
monster 1
see_invisible 1
unaggressive 1
reflect_missile 1
reflect_spell 1
can_cast_spell 1
sleep 1
end

