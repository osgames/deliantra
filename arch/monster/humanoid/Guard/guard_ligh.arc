object light_guard
name castle guard
race human
face guard_ligh.x11
str 25
con 15
wis 5
pow 1
int 12
hp 80
maxhp 80
sp 10
maxsp 10
exp 150
dam 10
wc 5
ac 3
speed 0.06
level 4
resist_physical 30
resist_magic 30
weight 150000
alive 1
no_pick 1
monster 1
see_invisible 1
unaggressive 1
can_cast_spell 1
sleep 1
stand_still 1
end

