object guard
name castle guard
race human
face guard_hard.x11
str 25
con 15
wis 15
pow 3
int 14
hp 250
maxhp 250
sp 25
maxsp 25
exp 1200
dam 15
wc 1
ac 0
speed 0.06
level 10
resist_physical 75
resist_magic 100
weight 150000
alive 1
no_pick 1
monster 1
see_invisible 1
unaggressive 1
reflect_missile 1
reflect_spell 1
can_cast_spell 1
sleep 1
stand_still 1
end

