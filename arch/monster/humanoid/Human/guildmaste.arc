object guildmaster
name guildmaster
race human
msg
@match *
I'm too busy to help you.
endmsg
face guildmaste.x11
pow 5
int 14
hp 50
maxhp 50
sp 20
maxsp 20
exp 0
dam 1
wc 25
ac 10
speed 0.06
level 10
weight 50000
randomitems random_read
pick_up 1
alive 1
monster 1
unaggressive 1
can_cast_spell 1
end

