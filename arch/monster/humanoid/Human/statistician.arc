object statistician
inherit sign
name Statistician
msg
@match *
@addtopic kills|deaths
@eval statistician::talk $who, $msg, $npc
endmsg
face dwarf_pr.x11
animation NONE
move_block all
end

