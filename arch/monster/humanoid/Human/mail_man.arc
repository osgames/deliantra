object mail_man
inherit man
name Mail office clerk
msg
@match literacy
@eval ipo::command $who, $msg, $npc
@match pen
@eval ipo::command $who, $msg, $npc
@match receive
@eval ipo::command $who, $msg, $npc
@match *
@eval ipo::command $who, $msg, $npc
endmsg
int 12
end

