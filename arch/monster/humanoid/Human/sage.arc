object sage
name sage
race human
msg
@match *
I'm too busy to answer your queries.
endmsg
face sage.x11
str 5
dex 5
pow 5
int 20
hp 18
maxhp 18
sp 40
maxsp 40
exp 0
dam 1
wc 25
ac 10
speed 0.06
level 15
weight 50000
randomitems sage
alive 1
monster 1
unaggressive 1
can_cast_spell 1
can_use_skill 1
body_skill 1
end

