object child_thief
name young rogue
race human
face child.x11
str 8
dex 16
con 2
int 14
hp 15
maxhp 15
exp 15
dam 2
wc 18
ac 7
speed 0.4
level 1
weight 50000
randomitems c_thief
alive 1
monster 1
can_use_scroll 1
can_use_ring 1
can_use_skill 1
can_use_rod 1
body_range 1
body_skill 1
body_finger 2
can_use_wand 1
end

