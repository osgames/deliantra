object armless_cook
anim
facings 4
armless_cook.x11
armless_cook.x31
armless_cook.x51
armless_cook.x71
mina
name armless cook
race human
face armless_cook.x51
animation armless_cook
str 12
dex 10
pow 1
int 12
hp 18
maxhp 18
sp 10
maxsp 10
exp 25
dam 2
wc 10
ac 8
speed -0.15
level 3
weight 50000
randomitems man
pick_up 24
will_apply 2
move_type walk
alive 1
monster 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_armour 1
can_use_skill 1
body_range 1
body_torso 1
body_head 1
body_skill 1
body_shoulder 1
body_foot 1
body_wrist 1
body_waist 1
end

