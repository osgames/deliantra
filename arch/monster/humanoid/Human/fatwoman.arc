object fatwoman
name large woman
race human
face fatwoman.x11
str 14
dex 9
con 3
int 13
hp 10
maxhp 10
exp 15
dam 2
wc 14
ac 8
speed 0.05
level 1
weight 50000
randomitems skill_use_magic_item
alive 1
can_use_shield 1
monster 1
unaggressive 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
random_movement 1
end

