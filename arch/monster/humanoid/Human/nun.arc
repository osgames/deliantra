object nun
anim
facings 4
nun.x51
nun.x31
nun.x51
nun.x71
mina
name woman
race human
face nun.x51
str 10
dex 10
wis 16
pow 15
int 12
hp 28
maxhp 28
sp 15
maxsp 15
exp 200
dam 1
wc 15
ac 10
speed -0.10
level 3
weight 50000
randomitems priest_class_items
pick_up 24
will_apply 2
alive 1
can_use_shield 1
monster 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

