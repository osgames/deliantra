object crazy_jeweler_child
name crazy jeweler child
race human
face child.x11
str 8
dex 13
con 1
int 8
hp 250
maxhp 250
sp 20
maxsp 20
exp 20000
dam 15
wc -2
ac -2
speed 0.3
level 20
attacktype 33
resist_physical 30
resist_magic 20
resist_fire 100
resist_cold 30
resist_confusion 100
weight 50000
randomitems jeweler_child
alive 1
monster 1
unaggressive 0
reflect_spell 1
can_cast_spell 1
can_use_ring 1
can_use_skill 1
can_use_rod 1
body_range 1
body_skill 1
body_finger 2
can_use_wand 1
random_movement 1
end

