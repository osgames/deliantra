object postman
anim
facings 2
postman.x31
postman.x32
postman.x71
postman.x72
mina
race human
face postman.x31
int 12
hp 15
maxhp 15
exp 40
dam 5
wc 12
ac 6
speed -0.20
level 3
resist_slow -100
weight 50000
randomitems postman
alive 1
can_use_shield 1
monster 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end

