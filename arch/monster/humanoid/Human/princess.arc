object princess
anim
princess.x11
princess.x12
princess.x13
princess.x12
mina
name princess
race human
face princess.x11
pow 9
int 12
hp 30
maxhp 30
sp 9
maxsp 9
exp 20
dam 9
wc 8
ac 5
speed 0.15
level 2
weight 50000
randomitems princess
pick_up 24
will_apply 2
alive 1
can_use_shield 1
monster 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

