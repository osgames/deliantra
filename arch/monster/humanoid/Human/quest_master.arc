object quest_master
inherit sign
name The quest master
msg
@match start
@eval quest_master::talk $who, $msg, $npc
@match spoiler
@eval quest_master::talk $who, $msg, $npc
@match finished
@eval quest_master::talk $who, $msg, $npc
@match unfinished
@eval quest_master::talk $who, $msg, $npc
@match *
@eval quest_master::talk $who, $msg, $npc
endmsg
face sage.x11
animation NONE
move_block all
end

