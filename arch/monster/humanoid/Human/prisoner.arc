object prisoner
anim
prisoner.x11
prisoner.x11
prisoner.x11
prisoner.x11
prisoner.x12
prisoner.x12
mina
race human
face prisoner.x11
pow 1
int 12
hp 10
maxhp 10
exp 1
dam 2
wc 1
ac 8
level 3
alive 1
monster 1
unaggressive 1
end

#Mutilated Prisoners, These might be found in Navar etc.
object prisoner211
name prisoner missing a hand
race human
face prisoner211.x11
pow 1
int 10
hp 10
maxhp 10
exp 1
dam 2
wc 1
ac 8
level 3
alive 1
monster 1
unaggressive 1
end

object prisoner221
name prisoner with hands chopped off
race human
face prisoner221.x11
pow 1
int 10
hp 10
maxhp 10
exp 1
dam 2
wc 1
ac 8
level 3
alive 1
monster 1
unaggressive 1
end

object prisoner231
name prisoner with a foot and both hands chopped off
race human
face prisoner231.x11
pow 1
int 10
hp 10
maxhp 10
exp 1
dam 2
wc 1
ac 8
level 3
alive 1
monster 1
unaggressive 1
end

object prisoner241
name prisoner with both feet and both hands chopped off
race human
face prisoner241.x11
pow 1
int 10
hp 10
maxhp 10
exp 1
dam 2
wc 1
ac 8
level 3
alive 1
monster 1
unaggressive 1
end

object prisoner251
name prisoner with both hands and a leg sawn off
race human
face prisoner251.x11
pow 1
int 10
hp 10
maxhp 10
exp 1
dam 2
wc 1
ac 8
level 3
alive 1
monster 1
unaggressive 1
end

object prisoner261
name prisoner with both hands and legs sawn off
race human
face prisoner261.x11
pow 1
int 10
hp 10
maxhp 10
exp 1
dam 2
wc 1
ac 8
level 3
alive 1
monster 1
unaggressive 1
end

object prisoner271
name prisoner with both hands and torso severed
race human
face prisoner271.x11
pow 1
int 10
hp 10
maxhp 10
exp 1
dam 2
wc 1
ac 8
level 3
alive 1
monster 1
unaggressive 1
end

object prisoner311
name prisoner with right hand and left foot cut off
race human
face prisoner311.x11
pow 1
int 10
hp 10
maxhp 10
exp 1
dam 2
wc 1
ac 8
level 3
alive 1
monster 1
unaggressive 1
end

object prisoner411
name prisoner with left foot cut off
race human
face prisoner411.x11
pow 1
int 10
hp 10
maxhp 10
exp 1
dam 2
wc 1
ac 8
level 3
alive 1
monster 1
unaggressive 1
end

object prisoner421
name prisoner with both feet cut off
race human
face prisoner421.x11
pow 1
int 10
hp 10
maxhp 10
exp 1
dam 2
wc 1
ac 8
level 3
alive 1
monster 1
unaggressive 1
end

