object charwoman
anim
facings 2
charwoman.x31
charwoman.x32
charwoman.x71
charwoman.x72
mina
name cleaning woman
race human
face charwoman.x71
pow 1
int 8
hp 8
maxhp 8
sp 1
maxsp 1
exp 0
dam 1
wc 25
ac 10
speed 0.15
level 1
weight 50000
run_away 90
alive 1
monster 1
unaggressive 1
random_movement 1
end

