object pirate
anim
pirate.x11
pirate.x12
mina
race human
face pirate.x11
str 12
dex 20
con 2
wis 10
int 13
hp 20
maxhp 20
exp 45
dam 6
wc 10
ac 7
speed -0.12
level 4
weight 75000
randomitems pirate
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end

