object ninja2
anim
ninja_2.x11
ninja_2.x12
ninja_2.x13
ninja_2.x12
mina
name ninja
race human
face ninja_2.x11
str 20
dex 15
pow 1
int 20
hp 30
maxhp 30
sp 7
maxsp 7
exp 30
dam 5
wc 12
ac 0
speed 0.25
level 1
weight 50000
randomitems npc_ninja
pick_up 24
will_apply 2
alive 1
can_use_shield 1
monster 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end

