object conjurer2
anim
conjurer.x11
conjurer.x12
conjurer.x13
conjurer.x12
mina
name conjurer
race human
face conjurer.x11
str 10
dex 10
pow 5
int 20
hp 40
maxhp 40
sp 40
maxsp 40
exp 100
dam 4
wc 15
ac 1
speed -0.1
level 15
weight 50000
randomitems conjurer2
alive 1
can_use_shield 1
monster 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
can_use_rod 1
can_use_horn 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

