object warrior_1
anim
warrior.x11
warrior.x12
warrior.x31
warrior.x32
warrior.x51
warrior.x52
warrior.x71
warrior.x72
facings 4
mina
name warrior
race human
face warrior.x51
is_animated 1
str 18
dex 13
pow 1
cha 18
int 12
hp 50
maxhp 50
sp 5
maxsp 5
exp 40
dam 8
wc 9
ac 1
speed 0.15
level 3
weight 50000
randomitems warrior_1
pick_up 24
will_apply 2
alive 1
can_use_shield 1
monster 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
anim_speed -1
can_use_wand 1
end

