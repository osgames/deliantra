object fighter
anim
fighter.x11
fighter.x12
fighter.x13
fighter.x12
mina
name fighter
race human
face fighter.x11
str 15
dex 15
pow 1
int 10
hp 50
maxhp 50
sp 5
maxsp 5
exp 40
dam 8
wc 9
ac 1
speed 0.15
level 3
weight 50000
randomitems fighter
pick_up 24
will_apply 2
alive 1
can_use_shield 1
monster 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end

