object thief_1
anim
facings 4
thief.x11
thief.x31
thief.x51
thief.x71
mina
name thief
race human
face thief.x51
str 10
dex 25
int 13
hp 25
maxhp 25
exp 50
wc 10
ac 0
speed 0.28
level 1
weight 52000
randomitems thief_1
pick_up 26
will_apply 8
alive 1
can_use_shield 1
monster 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
end

