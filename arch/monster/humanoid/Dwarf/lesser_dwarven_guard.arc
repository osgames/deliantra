object lesser_dwarven_guard
anim
lesser_dwarven_guard.x11
lesser_dwarven_guard.x12
mina
name lesser dwarven guard
race dwarf
face lesser_dwarven_guard.x11
str 25
dex 12
pow 28
int 12
hp 300
maxhp 300
sp 9
maxsp 9
exp 7000
dam 10
wc 15
ac -5
speed 0.20
attack_movement 4
level 15
weight 50000
randomitems ldwar_guard
pick_up 24
will_apply 2
alive 1
can_use_shield 1
monster 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end

