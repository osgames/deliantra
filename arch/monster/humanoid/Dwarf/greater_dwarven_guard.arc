object greater_dwarven_guard
anim
greater_dwarven_guard.x11
greater_dwarven_guard.x12
mina
name greater dwarven guard
race dwarf
face greater_dwarven_guard.x11
str 30
dex 14
pow 43
int 9
hp 600
maxhp 600
sp 9
maxsp 9
exp 20000
dam 12
wc 20
ac -10
speed 0.30
attack_movement 4
level 20
weight 50000
randomitems gdwar_guard
pick_up 24
will_apply 2
alive 1
can_use_shield 1
monster 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end

