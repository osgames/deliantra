object dwarf_wiz
anim
dwarf_wiz.x11
dwarf_wiz.x12
dwarf_wiz.x13
mina
name dwarf wizard
race dwarf
msg
@match *
I'm too busy to answer your queries.
endmsg
face dwarf_wiz.x11
pow 18
int 18
hp 38
maxhp 38
sp 50
maxsp 50
exp 10
dam 5
wc 15
ac 10
speed 0.06
level 15
weight 50000
randomitems monster_spells
alive 1
monster 1
unaggressive 1
can_cast_spell 1
end

