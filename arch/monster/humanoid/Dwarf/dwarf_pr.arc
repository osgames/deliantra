object dwarf_priest
anim
dwarf_pr.x11
dwarf_pr.x12
dwarf_pr.x13
mina
name dwarf priest
race dwarf
msg
@match *
I'm too busy to answer your queries.
endmsg
face dwarf_pr.x11
pow 9
int 16
hp 28
maxhp 28
sp 40
maxsp 40
exp 5
dam 5
wc 15
ac 10
speed 0.06
level 15
weight 50000
randomitems prayer_book
alive 1
monster 1
unaggressive 1
can_cast_spell 1
end

