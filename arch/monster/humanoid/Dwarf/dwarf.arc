object dwarf
anim
dwarf.x11
dwarf.x12
mina
name dwarf
race dwarf
face dwarf.x11
str 20
dex 10
pow 21.6
int 15
hp 70
maxhp 70
sp 9
maxsp 9
exp 100
dam 9
wc 8
ac 1
speed 0.15
level 3
weight 50000
randomitems dwarf
pick_up 24
will_apply 2
alive 1
can_use_shield 1
monster 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end

