object halfling
anim
halfling.x51
halfling.x52
mina
name halfling
race halfling
face halfling.x51
str 12
dex 10
pow 1
int 10
hp 18
maxhp 18
sp 10
maxsp 10
exp 25
dam 2
wc 10
ac 8
speed 0.15
level 3
weight 30000
randomitems halfling
pick_up 24
will_apply 2
alive 1
can_use_shield 1
monster 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end

