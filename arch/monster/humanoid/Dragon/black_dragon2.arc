object black_dragon2
anim
facings 2
black_dragon2.x51
black_dragon2.x52
black_dragon2.x71
black_dragon2.x72
mina
name black dragon
race dragon
face black_dragon2.x51
int 20
food 999
dam 10
wc 21
ac 5
speed -0.3
level 1
attacktype 1
weight 70000
randomitems dragon_player_items
alive 1
can_use_shield 0
monster 1
unaggressive 1
can_use_armour 0
can_use_weapon 0
body_range 1
body_arm 0
body_neck 1
body_skill 1
body_finger 2
body_shoulder 1
body_wrist 2
body_waist 1
end

