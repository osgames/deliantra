object Balrog
anim
jessyb.x11
jessyb.x12
jessyb.x13
jessyb.x12
mina
name Balrog
race demon
face jessyb.x11
str 90
dex 40
con 40
wis 20
pow 40
int 30
hp 4000
maxhp 4000
maxsp 90
exp 250000
dam 60
wc -30
ac -20
speed 0.4
attack_movement 5
level 30
attacktype 1029
resist_physical 80
resist_magic 80
resist_fire 100
resist_confusion 100
resist_poison 100
resist_paralyze 100
resist_fear 100
resist_godpower 80
resist_holyword 80
resist_blind 100
weight 500000
randomitems Balrog
run_away 10
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end
more
object Balrog_2
anim
jessyb.x11
jessyb.x12
jessyb.x13
jessyb.x12
mina
name Balrog
face jessyb.x11
x 1
weight 300000
alive 1
no_pick 1
monster 1
end
more
object Balrog_3
anim
jessyb.x11
jessyb.x12
jessyb.x13
jessyb.x12
mina
name Balrog
face jessyb.x11
x 2
weight 300000
alive 1
no_pick 1
monster 1
end
more
object Balrog_4
anim
jessyb.x11
jessyb.x12
jessyb.x13
jessyb.x12
mina
name Balrog
face jessyb.x11
x 3
weight 300000
alive 1
no_pick 1
monster 1
end
more
object Balrog_5
anim
jessyb.x11
jessyb.x12
jessyb.x13
jessyb.x12
mina
name Balrog
face jessyb.x11
y 1
weight 300000
alive 1
no_pick 1
monster 1
end
more
object Balrog_6
anim
jessyb.x11
jessyb.x12
jessyb.x13
jessyb.x12
mina
name Balrog
face jessyb.x11
x 1
y 1
weight 300000
alive 1
no_pick 1
monster 1
end
more
object Balrog_7
anim
jessyb.x11
jessyb.x12
jessyb.x13
jessyb.x12
mina
name Balrog
face jessyb.x11
x 2
y 1
weight 300000
alive 1
no_pick 1
monster 1
end
more
object Balrog_8
anim
jessyb.x11
jessyb.x12
jessyb.x13
jessyb.x12
mina
name Balrog
face jessyb.x11
x 3
y 1
weight 300000
alive 1
no_pick 1
monster 1
end
more
object Balrog_9
anim
jessyb.x11
jessyb.x12
jessyb.x13
jessyb.x12
mina
name Balrog
face jessyb.x11
y 2
weight 300000
alive 1
no_pick 1
monster 1
end
more
object Balrog_10
anim
jessyb.x11
jessyb.x12
jessyb.x13
jessyb.x12
mina
name Balrog
face jessyb.x11
x 1
y 2
weight 300000
alive 1
no_pick 1
monster 1
end
more
object Balrog_11
anim
jessyb.x11
jessyb.x12
jessyb.x13
jessyb.x12
mina
name Balrog
face jessyb.x11
x 2
y 2
weight 300000
alive 1
no_pick 1
monster 1
end
more
object Balrog_12
anim
jessyb.x11
jessyb.x12
jessyb.x13
jessyb.x12
mina
name Balrog
face jessyb.x11
x 3
y 2
weight 300000
alive 1
no_pick 1
monster 1
end
more
object Balrog_13
anim
jessyb.x11
jessyb.x12
jessyb.x13
jessyb.x12
mina
name Balrog
face jessyb.x11
y 3
weight 300000
alive 1
no_pick 1
monster 1
end
more
object Balrog_14
anim
jessyb.x11
jessyb.x12
jessyb.x13
jessyb.x12
mina
name Balrog
face jessyb.x11
x 1
y 3
weight 300000
alive 1
no_pick 1
monster 1
end
more
object Balrog_15
anim
jessyb.x11
jessyb.x12
jessyb.x13
jessyb.x12
mina
name Balrog
face jessyb.x11
x 2
y 3
weight 300000
alive 1
no_pick 1
monster 1
end
more
object Balrog_16
anim
jessyb.x11
jessyb.x12
jessyb.x13
jessyb.x12
mina
name Balrog
face jessyb.x11
x 3
y 3
weight 300000
alive 1
no_pick 1
monster 1
end

