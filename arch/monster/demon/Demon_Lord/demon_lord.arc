object demon_lord
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
race demon
face demon_lord.x11
con 8
wis 20
pow 35
int 14
hp 3000
maxhp 3000
sp 200
maxsp 200
exp 50000
dam 40
wc -30
ac -11
speed -0.4
level 19
attacktype 5
resist_physical 75
resist_magic 80
resist_fire 100
resist_cold 80
resist_paralyze 100
resist_fear 100
resist_godpower 90
resist_holyword 90
resist_blind 100
weight 8000000
randomitems demonlord
run_away 2
alive 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
end
more
object demon_lord_1
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x -1
y -3
alive 1
end
more
object demon_lord_2
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
y -3
alive 1
end
more
object demon_lord_3
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x 1
y -3
alive 1
end
more
object demon_lord_4
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x 2
y -3
alive 1
end
more
object demon_lord_5
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x -1
y -2
alive 1
end
more
object demon_lord_6
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
y -2
alive 1
end
more
object demon_lord_7
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x 1
y -2
alive 1
end
more
object demon_lord_8
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x 2
y -2
alive 1
end
more
object demon_lord_9
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x -1
y -1
alive 1
end
more
object demon_lord_10
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
y -1
alive 1
end
more
object demon_lord_11
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x 1
y -1
alive 1
end
more
object demon_lord_12
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x 2
y -1
alive 1
end
more
object demon_lord_13
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x -1
alive 1
end
more
object demon_lord_15
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x 1
alive 1
end
more
object demon_lord_16
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x 2
alive 1
end
more
object demon_lord_17
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x -1
y 1
alive 1
end
more
object demon_lord_18
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
y 1
alive 1
end
more
object demon_lord_19
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x 1
y 1
alive 1
end
more
object demon_lord_20
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x 2
y 1
alive 1
end
more
object demon_lord_21
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x -1
y 2
alive 1
end
more
object demon_lord_22
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
y 2
alive 1
end
more
object demon_lord_23
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x 1
y 2
alive 1
end
more
object demon_lord_24
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x 2
y 2
alive 1
end
more
object demon_lord_25
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x -1
y 3
alive 1
end
more
object demon_lord_26
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
y 3
alive 1
end
more
object demon_lord_27
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x 1
y 3
alive 1
end
more
object demon_lord_28
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x 2
y 3
alive 1
end
more
object demon_lord_29
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x -1
y 4
alive 1
end
more
object demon_lord_30
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
y 4
alive 1
end
more
object demon_lord_31
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x 1
y 4
alive 1
end
more
object demon_lord_32
anim
demon_lord.x11
demon_lord.x12
mina
name Demon Lord
face demon_lord.x11
x 2
y 4
alive 1
end

