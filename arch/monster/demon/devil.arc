object devil
anim
devil.x11
devil.x12
mina
name demon
race demon
face devil.x11
con 4
wis 15
pow 4
int 15
hp 165
maxhp 165
sp 30
maxsp 30
exp 800
dam 12
wc 3
ac 1
speed 0.11
level 9
resist_physical 30
resist_fire 100
resist_cold -100
resist_confusion -100
resist_blind 30
weight 180000
randomitems devil
run_away 15
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

