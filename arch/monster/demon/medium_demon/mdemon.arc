object medium_demon
anim
facings 1
mdemon.x11
mdemon.x12
mina
name demon
race demon
face mdemon.x11
str 40
dex 15
con 10
wis 13
pow 10
int 12
hp 2000
maxhp 2000
sp 30
maxsp 50
exp 10000
dam 30
wc -3
ac -2
speed 0.3
attack_movement 5
level 12
attacktype 5
resist_magic 50
resist_fire 100
resist_confusion 100
resist_poison 100
resist_godpower 50
resist_holyword 50
resist_blind 75
weight 300000
randomitems devil
run_away 3
alive 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_weapon 1
can_use_skill 1
can_see_in_dark 1
body_arm 2
body_skill 1
end
more
object medium_demon_2
anim
facings 1
mdemon.x11
mdemon.x12
mina
name demon
face mdemon.x11
x 1
weight 300000
alive 1
monster 1
end
more
object medium_demon_3
anim
facings 1
mdemon.x11
mdemon.x12
mina
name demon
face mdemon.x11
y 1
weight 300000
alive 1
monster 1
end
more
object medium_demon_4
anim
facings 1
mdemon.x11
mdemon.x12
mina
name demon
face mdemon.x11
x 1
y 1
weight 300000
alive 1
monster 1
end

