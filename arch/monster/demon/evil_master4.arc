object evil_master4
anim
facings 2
evil_master4.x11
evil_master4.x12
mina
name Evil Master
race demon
face evil_master4.x11
is_animated 1
str 30
con 80
wis 25
pow 25
int 25
hp 20000
maxhp 20000
sp 250
maxsp 250
exp 200000
dam 100
wc -50
ac -105
speed 0.500000
attack_movement 5
level 100
attacktype 1
resist_physical 100
resist_magic 20
resist_fire -100
weight 200000
run_away 15
alive 1
no_pick 1
monster 1
unaggressive 1
end

