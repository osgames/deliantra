object fiend
anim
fiend.x11
fiend.x12
mina
name Fiend
race demon
face fiend.x11
con 5
wis 15
pow 8
int 15
hp 265
maxhp 265
sp 40
maxsp 40
exp 3600
dam 22
wc 0
ac -3
speed 0.2
level 15
resist_physical 30
resist_magic 50
resist_fire 100
resist_cold -50
resist_blind 50
weight 180000
randomitems fiend
run_away 15
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

