object evil_master3
anim
evil_master3.x11
evil_master3.x12
mina
name Evil Master
race demon
face evil_master3.x11
is_animated 1
con 100
wis 40
pow 40
int 30
hp 31000
maxhp 31000
sp 1000
maxsp 1000
exp 1000000
dam 30
wc -80
ac -120
speed 0.500000
level 120
attacktype 1049089
resist_physical 100
resist_fire 100
resist_electricity 50
resist_cold 100
resist_confusion 30
resist_acid 30
resist_drain 100
resist_weaponmagic 95
resist_poison 100
resist_paralyze 100
resist_fear 100
resist_godpower 100
resist_holyword 30
resist_blind 100
carrying 100
weight 180000
run_away 3
pick_up 24
will_apply 2
alive 1
no_pick 1
monster 1
see_invisible 1
undead 1
unaggressive 1
can_cast_spell 1
can_use_bow 1
sleep 1
can_use_skill 1
can_use_rod 1
can_see_in_dark 1
body_range 1
body_arm 2
body_skill 1
body_finger 2
end

