object gr_hellhound
anim
facings 2
hellhound.x31
hellhound.x32
hellhound.x33
hellhound.x32
hellhound.x71
hellhound.x72
hellhound.x73
hellhound.x72
mina
name greater hellhound
race demon
face hellhound.x31
con 40
wis 20
pow 60
int 14
hp 6000
maxhp 6000
sp 150
maxsp 150
exp 300000
dam 70
wc -30
ac -11
speed -0.2
attack_movement 5
level 40
attacktype 1029
resist_physical 90
resist_magic 80
resist_fire 100
resist_confusion 100
resist_poison 100
resist_paralyze 100
resist_fear 100
resist_godpower 60
resist_holyword 60
resist_blind 100
weight 130000
randomitems gr_hellhound
run_away 2
alive 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
can_use_skill 1
can_see_in_dark 1
end

object hellhound
anim
facings 2
hellhound.x31
hellhound.x32
hellhound.x33
hellhound.x32
hellhound.x71
hellhound.x72
hellhound.x73
hellhound.x72
mina
name hellhound
race demon
face hellhound.x31
con 6
wis 20
pow 5
int 12
hp 800
maxhp 800
sp 25
maxsp 40
exp 5000
dam 9
wc -20
ac -9
speed -0.2
attack_movement 5
level 20
attacktype 1029
resist_physical 30
resist_magic 20
resist_fire 100
resist_cold -10
weight 40000
randomitems hellhound
run_away 5
alive 1
monster 1
can_cast_spell 1
sleep 1
can_see_in_dark 1
end

