object evil_master2
anim
evil_master2.x11
evil_master2.x12
mina
name Evil Master
race demon
face evil_master2.x11
is_animated 1
con 100
wis 40
pow 30
int 30
hp 20000
maxhp 20000
sp 1000
maxsp 1000
exp 400000
dam 80
wc -60
ac -110
speed 0.700000
attack_movement 3
level 100
attacktype 5242880
resist_physical 100
resist_magic 30
resist_fire 80
resist_weaponmagic 50
resist_blind 100
weight 75000
randomitems all_spell_skills
run_away 3
pick_up 24
will_apply 1
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_ring 1
sleep 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_skill 1
body_finger 2
can_use_wand 1
end

