object generate_devil
name generator
race demon
other_arch devil
face devil_gen.x11
hp 400
maxhp 400
maxsp 1
exp 180
ac 7
speed 0.01
level 1
resist_fire 100
resist_cold -100
resist_confusion -100
weight 300000
alive 1
generator 1
end

