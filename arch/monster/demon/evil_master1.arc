object evil_master1
anim
evil_master.x11
evil_master.x12
mina
name Evil Master
race demon
face evil_master.x11
con 100
wis 50
pow 50
int 30
hp 30000
maxhp 30000
sp 1000
maxsp 1000
exp 1000000
dam 50
wc -50
ac -120
speed 0.850000
level 117
attacktype 66332
resist_physical 100
resist_magic 30
resist_fire 30
resist_electricity 60
resist_cold 100
resist_acid 30
resist_weaponmagic 60
resist_paralyze 100
resist_turn_undead 100
resist_fear 30
resist_death 100
resist_godpower 100
resist_holyword 95
resist_blind 100
randomitems all_spell_skills
pick_up 24
will_apply 2
alive 1
no_pick 1
monster 1
see_invisible 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_ring 1
sleep 1
can_use_skill 1
can_use_rod 1
body_range 1
body_finger 2
can_use_wand 1
end

