object raas
anim
facings 2
raas.x71
raas.x31
mina
name raas
race demon
msg
@match *
Rrrgghhg kill you
endmsg
face raas.x31
str 24
con 14
wis 10
int 4
hp 100
maxhp 165
maxsp 5
exp 700
dam 25
wc 1
ac 0
speed 0.20
attack_movement 5
level 9
attacktype 1
resist_physical 20
resist_fire -100
resist_blind 30
weight 200000
randomitems raas
run_away 15
alive 1
no_pick 1
monster 1
end

