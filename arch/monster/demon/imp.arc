object imp
anim
facings 2
imp.x71
imp.x31
mina
name imp
race demon
face imp.x71
str 24
con 1
wis 3
pow 1
int 4
hp 5
maxhp 5
sp 0
maxsp 5
exp 150
dam 2
wc 4
ac 4
speed 0.25
attack_movement 5
level 1
attacktype 5
resist_physical 20
resist_magic 30
resist_fire 100
weight 200000
randomitems imp
run_away 15
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_see_in_dark 1
end

