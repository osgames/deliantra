object bone_head
anim
bone_head.x11
bone_head.x12
bone_head.x13
bone_head.x12
mina
name Evil Master, Bonehead
race demon
face bone_head.x11
con 50
wis 20
pow 16
int 24
hp 25000
maxhp 25000
maxsp 50
exp 3500
dam 15
wc -5
ac -2
speed 0.2
attack_movement 1
level 15
attacktype 17
resist_magic 100
resist_cold 100
resist_poison 100
resist_paralyze 100
resist_fear 100
weight 1500
randomitems beholder
run_away 15
move_type fly_low
alive 1
no_pick 1
monster 1
friendly 1
undead 1
can_cast_spell 1
sleep 1
can_see_in_dark 1
end

