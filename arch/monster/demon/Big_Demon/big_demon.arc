object big_demon
anim
facings 2
big_demon.x71
big_demon.x31
mina
name big demon
race demon
face big_demon.x71
str 40
dex 15
con 10
wis 13
pow 10
int 20
hp 1000
maxhp 1000
sp 30
maxsp 50
exp 20000
dam 30
wc -3
ac -2
speed 0.3
attack_movement 5
level 12
attacktype 1029
resist_magic 50
resist_fire 100
resist_confusion 100
resist_poison 100
resist_godpower 50
resist_holyword 50
resist_blind 100
weight 300000
randomitems imp
run_away 3
alive 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_weapon 1
can_use_skill 1
can_see_in_dark 1
body_arm 2
body_skill 1
end
more
object big_demon_2
anim
facings 2
big_demon.x71
big_demon.x31
mina
name big demon
face big_demon.x71
x 1
weight 300000
alive 1
monster 1
end
more
object big_demon_3
anim
facings 2
big_demon.x71
big_demon.x31
mina
name big demon
face big_demon.x71
x 2
weight 300000
alive 1
monster 1
end
more
object big_demon_4
anim
facings 2
big_demon.x71
big_demon.x31
mina
name big demon
face big_demon.x71
y 1
weight 300000
alive 1
monster 1
end
more
object big_demon_5
anim
facings 2
big_demon.x71
big_demon.x31
mina
name big demon
face big_demon.x71
x 1
y 1
weight 300000
alive 1
monster 1
end
more
object big_demon_6
anim
facings 2
big_demon.x71
big_demon.x31
mina
name big demon
face big_demon.x71
x 2
y 1
weight 300000
alive 1
monster 1
end
more
object big_demon_7
anim
facings 2
big_demon.x71
big_demon.x31
mina
name big demon
face big_demon.x71
y 2
weight 300000
alive 1
monster 1
end
more
object big_demon_8
anim
facings 2
big_demon.x71
big_demon.x31
mina
name big demon
face big_demon.x71
x 1
y 2
weight 300000
alive 1
monster 1
end
more
object big_demon_9
anim
facings 2
big_demon.x71
big_demon.x31
mina
name big demon
face big_demon.x71
x 2
y 2
weight 300000
alive 1
monster 1
end
more
object big_demon_A
anim
facings 2
big_demon.x71
big_demon.x31
mina
name big demon
face big_demon.x71
y 3
weight 300000
alive 1
monster 1
end
more
object big_demon_B
anim
facings 2
big_demon.x71
big_demon.x31
mina
name big demon
face big_demon.x71
x 1
y 3
weight 300000
alive 1
monster 1
end
more
object big_demon_C
anim
facings 2
big_demon.x71
big_demon.x31
mina
name big demon
face big_demon.x71
x 2
y 3
weight 300000
alive 1
monster 1
end

