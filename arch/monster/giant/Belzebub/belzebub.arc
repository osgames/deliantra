object belzebub
anim
belzebub.x11
belzebub.x11
belzebub.x12
belzebub.x12
belzebub.x11
belzebub.x11
mina
name Belzebub
race demon
face belzebub.x11
str 80
dex 30
con 50
wis 25
pow 40
int 25
hp 5000
maxhp 5000
sp 200
maxsp 200
exp 300000
dam 40
wc -20
ac -14
speed -0.5
level 30
resist_physical 35
resist_magic 100
resist_electricity 30
resist_poison 30
resist_blind 100
weight 1000000
randomitems belzebub
run_away 1
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
unaggressive 1
can_cast_spell 1
can_use_scroll 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end
more
object belzebub_2
anim
belzebub.x11
belzebub.x11
belzebub.x12
belzebub.x12
belzebub.x11
belzebub.x11
mina
name Belzebub
face belzebub.x11
x 1
weight 100000
alive 1
no_pick 1
end
more
object belzebub_3
anim
belzebub.x11
belzebub.x11
belzebub.x12
belzebub.x12
belzebub.x11
belzebub.x11
mina
name Belzebub
face belzebub.x11
y 1
weight 100000
alive 1
no_pick 1
end
more
object belzebub_4
anim
belzebub.x11
belzebub.x11
belzebub.x12
belzebub.x12
belzebub.x11
belzebub.x11
mina
name Belzebub
face belzebub.x11
x 1
y 1
weight 100000
alive 1
no_pick 1
end
more
object belzebub_5
anim
belzebub.x11
belzebub.x11
belzebub.x12
belzebub.x12
belzebub.x11
belzebub.x11
mina
name Belzebub
face belzebub.x11
y 2
weight 100000
alive 1
no_pick 1
end
more
object belzebub_6
anim
belzebub.x11
belzebub.x11
belzebub.x12
belzebub.x12
belzebub.x11
belzebub.x11
mina
name Belzebub
face belzebub.x11
x 1
y 2
weight 100000
alive 1
no_pick 1
end

