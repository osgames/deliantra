object fire_serpent
name fire serpent
race reptile
face serpent.x31
animation serpent
con 4
wis 15
pow 2
int 6
hp 120
maxhp 120
sp 5
maxsp 5
exp 400
dam 7
wc 4
ac -1
speed -0.10
level 9
attacktype 5
resist_fire 30
resist_cold -100
weight 90000
randomitems fire_serpent
alive 1
no_pick 1
monster 1
can_cast_spell 1
end
more
object fire_serpent_2
name fire serpent
face serpent.x31
animation serpent_2
x 1
weight 900000
alive 1
no_pick 1
monster 1
end

