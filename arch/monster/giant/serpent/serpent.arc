object serpent
anim
facings 2
serpent.x31
serpent.x32
serpent.x71
serpent.x72
mina
name serpent
race reptile
face serpent.x31
con 3.2
wis 15
int 8
hp 100
maxhp 100
exp 150
dam 9
wc 5
ac -2
speed -0.08
level 7
resist_cold -100
resist_poison 100
weight 90000
randomitems serpent
alive 1
no_pick 1
monster 1
end
more
object serpent_2
anim
facings 2
serpent.x31
serpent.x32
serpent.x71
serpent.x72
mina
name serpent
face serpent.x31
x 1
weight 900000
alive 1
no_pick 1
monster 1
end

