object EDK
anim
facings 2
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x15
Bk.x14
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x75
Bk.x74
mina
name Elite Dread Knight
race giant
face Bk.x11
str 65
dex 24
con 38
wis 30
pow 115
int 28
hp 6000
maxhp 6000
sp 100
maxsp 200
exp 500000
dam 45
wc -35
ac -15
speed 0.40
level 50
attacktype 5
resist_physical 70
resist_magic 100
resist_fire 50
resist_electricity 90
resist_cold 50
resist_confusion 100
resist_drain 100
resist_paralyze 100
resist_fear 100
weight 300000
randomitems EDK
run_away 1
pick_up 11
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end
more
object EDK3
anim
facings 2
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x15
Bk.x14
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x75
Bk.x74
mina
name Elite Dread Knight
face Bk.x11
y 1
alive 1
no_pick 1
end
more
object EDK5
anim
facings 2
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x15
Bk.x14
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x75
Bk.x74
mina
name Elite Dread Knight
face Bk.x11
y 2
alive 1
no_pick 1
end
more
object EDK2
anim
facings 2
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x15
Bk.x14
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x75
Bk.x74
mina
name Elite Dread Knight
face Bk.x11
x 1
alive 1
no_pick 1
end
more
object EDK4
anim
facings 2
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x15
Bk.x14
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x75
Bk.x74
mina
name Elite Dread Knight
face Bk.x11
x 1
y 1
alive 1
no_pick 1
end
more
object EDK6
anim
facings 2
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x13
Bk.x14
Bk.x11
Bk.x12
Bk.x15
Bk.x14
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x73
Bk.x74
Bk.x71
Bk.x72
Bk.x75
Bk.x74
mina
name Elite Dread Knight
face Bk.x11
x 1
y 2
alive 1
no_pick 1
end

