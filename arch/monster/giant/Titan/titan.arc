object titan
anim
facings 2
titan.x71
titan.x72
titan.x73
titan.x73
titan.x72
titan.x71
titan.x31
titan.x32
titan.x33
titan.x33
titan.x32
titan.x31
mina
race giant
face titan.x71
str 60
dex 25
con 6
wis 20
pow 15
int 15
hp 4000
maxhp 4000
maxsp 30
exp 100000
dam 20
wc -20
ac -5
speed -0.33
level 15
resist_magic 50
resist_electricity 100
resist_fear 100
weight 1500000
randomitems titan
run_away 1
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end
more
object titan_2
anim
facings 2
titan.x71
titan.x72
titan.x73
titan.x73
titan.x72
titan.x71
titan.x31
titan.x32
titan.x33
titan.x33
titan.x32
titan.x31
mina
name titan
face titan.x71
x -1
alive 1
no_pick 1
end
more
object titan_3
anim
facings 2
titan.x71
titan.x72
titan.x73
titan.x73
titan.x72
titan.x71
titan.x31
titan.x32
titan.x33
titan.x33
titan.x32
titan.x31
mina
name titan
face titan.x71
x -1
y -1
alive 1
no_pick 1
end
more
object titan_4
anim
facings 2
titan.x71
titan.x72
titan.x73
titan.x73
titan.x72
titan.x71
titan.x31
titan.x32
titan.x33
titan.x33
titan.x32
titan.x31
mina
name titan
face titan.x71
y -1
alive 1
no_pick 1
end
more
object titan_5
anim
facings 2
titan.x71
titan.x72
titan.x73
titan.x73
titan.x72
titan.x71
titan.x31
titan.x32
titan.x33
titan.x33
titan.x32
titan.x31
mina
name titan
face titan.x71
x 1
y -1
alive 1
no_pick 1
end
more
object titan_6
anim
facings 2
titan.x71
titan.x72
titan.x73
titan.x73
titan.x72
titan.x71
titan.x31
titan.x32
titan.x33
titan.x33
titan.x32
titan.x31
mina
name titan
face titan.x71
x 1
alive 1
no_pick 1
end
more
object titan_7
anim
facings 2
titan.x71
titan.x72
titan.x73
titan.x73
titan.x72
titan.x71
titan.x31
titan.x32
titan.x33
titan.x33
titan.x32
titan.x31
mina
name titan
face titan.x71
x 1
y 1
alive 1
no_pick 1
end
more
object titan_8
anim
facings 2
titan.x71
titan.x72
titan.x73
titan.x73
titan.x72
titan.x71
titan.x31
titan.x32
titan.x33
titan.x33
titan.x32
titan.x31
mina
name titan
face titan.x71
y 1
alive 1
no_pick 1
end
more
object titan_9
anim
facings 2
titan.x71
titan.x72
titan.x73
titan.x73
titan.x72
titan.x71
titan.x31
titan.x32
titan.x33
titan.x33
titan.x32
titan.x31
mina
name titan
face titan.x71
x -1
y 1
alive 1
no_pick 1
end

