object generate_giant
name bean stalk
race giant
other_arch giant
face giant_gen.x11
hp 100
maxhp 100
maxsp 1
exp 200
speed 0.003
level 10
weight 30000
alive 1
no_pick 1
generator 1
end

