object byakie
anim
byakie.x11
byakie.x12
mina
race chaos
face byakie.x11
str 100
con 20
wis 21
pow 1
int 1
hp 4000
maxhp 4000
sp 20
maxsp 20
exp 250000
dam 30
wc -15
ac -10
speed -0.35
level 25
resist_physical 100
resist_magic 100
resist_fire 50
resist_electricity 50
resist_cold 50
resist_confusion -100
resist_acid 50
resist_drain 50
resist_poison 100
resist_slow 50
resist_paralyze 50
resist_fear 100
resist_chaos 100
weight 30000000
randomitems byakie
run_away 2
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
end
more
object byakie_2
anim
byakie.x11
byakie.x12
mina
name byakie
face byakie.x11
x 1
weight 30000000
alive 1
no_pick 1
monster 1
end
more
object byakie_3
anim
byakie.x11
byakie.x12
mina
name byakie
face byakie.x11
y 1
weight 30000000
alive 1
no_pick 1
monster 1
end
more
object byakie_4
anim
byakie.x11
byakie.x12
mina
name byakie
face byakie.x11
x 1
y 1
weight 30000000
alive 1
no_pick 1
monster 1
end

