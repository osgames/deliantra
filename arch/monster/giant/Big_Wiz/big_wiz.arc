object big_wiz
anim
big_wiz.x11
big_wiz.x12
big_wiz.x13
big_wiz.x14
big_wiz.x15
big_wiz.x15
big_wiz.x14
big_wiz.x13
big_wiz.x12
mina
name wizard
race giant
face big_wiz.x11
con 28
wis 15
pow 48
int 20
hp 3500
maxhp 3500
sp 100
maxsp 100
exp 100000
dam 19
wc -20
ac -15
speed 1.0
attack_movement 1
level 18
attacktype 128
resist_magic 100
resist_poison 100
resist_paralyze 100
resist_fear 100
resist_godpower 60
resist_holyword 60
resist_blind 75
weight 400000
randomitems big_wizard
run_away 3
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_scroll 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end
more
object big_wiz2
anim
big_wiz.x11
big_wiz.x12
big_wiz.x13
big_wiz.x14
big_wiz.x15
big_wiz.x15
big_wiz.x14
big_wiz.x13
big_wiz.x12
mina
name wizard
face big_wiz.x11
x 1
y 0
weight 4000000
alive 1
monster 1
end
more
object big_wiz3
anim
big_wiz.x11
big_wiz.x12
big_wiz.x13
big_wiz.x14
big_wiz.x15
big_wiz.x15
big_wiz.x14
big_wiz.x13
big_wiz.x12
mina
name wizard
face big_wiz.x11
x 2
y 0
weight 4000000
alive 1
monster 1
end
more
object big_wiz4
anim
big_wiz.x11
big_wiz.x12
big_wiz.x13
big_wiz.x14
big_wiz.x15
big_wiz.x15
big_wiz.x14
big_wiz.x13
big_wiz.x12
mina
name wizard
face big_wiz.x11
x 0
y 1
weight 4000000
alive 1
monster 1
end
more
object big_wiz5
anim
big_wiz.x11
big_wiz.x12
big_wiz.x13
big_wiz.x14
big_wiz.x15
big_wiz.x15
big_wiz.x14
big_wiz.x13
big_wiz.x12
mina
name wizard
face big_wiz.x11
x 1
y 1
weight 4000000
alive 1
monster 1
end
more
object big_wiz6
anim
big_wiz.x11
big_wiz.x12
big_wiz.x13
big_wiz.x14
big_wiz.x15
big_wiz.x15
big_wiz.x14
big_wiz.x13
big_wiz.x12
mina
name wizard
face big_wiz.x11
x 2
y 1
weight 4000000
alive 1
monster 1
end
more
object big_wiz7
anim
big_wiz.x11
big_wiz.x12
big_wiz.x13
big_wiz.x14
big_wiz.x15
big_wiz.x15
big_wiz.x14
big_wiz.x13
big_wiz.x12
mina
name wizard
face big_wiz.x11
x 0
y 2
weight 4000000
alive 1
monster 1
end
more
object big_wiz8
anim
big_wiz.x11
big_wiz.x12
big_wiz.x13
big_wiz.x14
big_wiz.x15
big_wiz.x15
big_wiz.x14
big_wiz.x13
big_wiz.x12
mina
name wizard
face big_wiz.x11
x 1
y 2
weight 4000000
alive 1
monster 1
end
more
object big_wiz9
anim
big_wiz.x11
big_wiz.x12
big_wiz.x13
big_wiz.x14
big_wiz.x15
big_wiz.x15
big_wiz.x14
big_wiz.x13
big_wiz.x12
mina
name wizard
face big_wiz.x11
x 2
y 2
weight 4000000
alive 1
monster 1
end

