object giant
anim
giant.x11
giant.x12
mina
name hill giant
race giant
face giant.x11
str 30
con 4
wis 10
int 13
hp 250
maxhp 250
exp 1500
dam 20
wc 2
ac 1
speed -0.085
level 10
resist_electricity 50
resist_blind -30
weight 300000
randomitems giant
run_away 6
pick_up 1
alive 1
no_pick 1
monster 1
can_use_weapon 1
sleep 1
can_use_skill 1
body_arm 2
body_skill 1
end
more
object giant_2
anim
giant.x11
giant.x12
mina
name hill giant
face giant.x11
y 1
weight 300000
alive 1
no_pick 1
monster 1
end

