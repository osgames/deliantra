object slug
anim
facings 2
slug.x31
slug.x32
slug.x71
slug.x72
mina
name slug
race insect
face slug.x31
con 10
wis 15
int 1
hp 250
maxhp 250
exp 500
dam 10
wc -1
ac -1
speed -0.08
level 8
attacktype 65
resist_fire -100
resist_electricity 50
resist_acid 100
resist_fear 100
resist_blind 100
weight 300000
run_away 5
alive 1
no_pick 1
monster 1
sleep 1
end
more
object slug_2
anim
facings 2
slug.x31
slug.x32
slug.x71
slug.x72
mina
name slug
face slug.x31
x 1
weight 300000
alive 1
no_pick 1
monster 1
end

