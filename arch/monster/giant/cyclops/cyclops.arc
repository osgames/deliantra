object cyclops
anim
facings 2
cyclops.x71
cyclops.x72
cyclops.x31
cyclops.x32
mina
race giant
face cyclops.x31
str 80
dex 30
con 56
wis 11
int 13
hp 8000
maxhp 8000
exp 250000
dam 50
wc -15
ac -10
speed -0.35
level 25
resist_physical 50
resist_magic 50
resist_fire 50
resist_electricity 50
resist_cold 50
resist_confusion -100
resist_acid 50
resist_drain 50
resist_poison 100
resist_slow 50
resist_paralyze 50
resist_chaos 100
resist_godpower 75
resist_holyword 85
resist_blind -100
weight 30000000
randomitems cyclops
run_away 5
alive 1
no_pick 1
monster 1
see_invisible 1
can_use_bow 1
can_use_weapon 1
sleep 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_skill 1
can_use_wand 1
end
more
object cyclops_2
anim
facings 2
cyclops.x71
cyclops.x72
cyclops.x31
cyclops.x32
mina
name cyclops
face cyclops.x31
x 1
alive 1
no_pick 1
monster 1
end
more
object cyclops_3
anim
facings 2
cyclops.x71
cyclops.x72
cyclops.x31
cyclops.x32
mina
name cyclops
face cyclops.x31
y 1
alive 1
no_pick 1
monster 1
end
more
object cyclops_4
anim
facings 2
cyclops.x71
cyclops.x72
cyclops.x31
cyclops.x32
mina
name cyclops
face cyclops.x31
x 1
y 1
alive 1
no_pick 1
monster 1
end
more
object cyclops_5
anim
facings 2
cyclops.x71
cyclops.x72
cyclops.x31
cyclops.x32
mina
name cyclops
face cyclops.x31
y 2
alive 1
no_pick 1
monster 1
end
more
object cyclops_6
anim
facings 2
cyclops.x71
cyclops.x72
cyclops.x31
cyclops.x32
mina
name cyclops
face cyclops.x31
x 1
y 2
alive 1
no_pick 1
monster 1
end

