object djinn_magenta
anim
facings 2
djinn_magenta.x11
djinn_magenta.x12
djinn_magenta.x21
djinn_magenta.x22
mina
name djinn
race faerie
face djinn_magenta.x11
con 50
wis 25
pow 50
int 29
hp 20000
maxhp 20000
sp 2200
maxsp 2200
exp 800000
dam 30
wc -15
ac -20
speed -0.50
attack_movement 7
level 100
attacktype 15
resist_physical 100
resist_magic 100
resist_fire 100
resist_electricity 100
resist_cold 100
resist_acid 100
resist_drain 50
resist_ghosthit 100
resist_poison 100
resist_fear 100
resist_chaos 50
weight 300000
randomitems djinn_magenta
run_away 5
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_scroll 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_rod 1
can_use_horn 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end
more
object djinn_magenta_2
anim
facings 2
djinn_magenta.x11
djinn_magenta.x12
djinn_magenta.x21
djinn_magenta.x22
mina
name djinn
face djinn_magenta.x11
y 1
weight 300000
alive 1
no_pick 1
monster 1
end

