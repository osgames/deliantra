object djinn_magenta_big
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
race faerie
face djinn_magenta_big.x11
con 70
wis 50
pow 70
int 30
hp 31000
maxhp 31000
sp 3300
maxsp 3300
exp 800000
dam 90
wc -45
ac -50
speed -0.50
attack_movement 7
level 116
attacktype 15
resist_physical 100
resist_magic 100
resist_fire 100
resist_electricity 100
resist_cold 100
resist_acid 100
resist_drain 70
resist_ghosthit 100
resist_poison 100
resist_fear 100
resist_chaos 70
weight 900000
randomitems djinn_magenta
run_away 5
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_scroll 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_rod 1
can_use_horn 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end
more
object djinn_magenta_big_2
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
y 1
weight 900000
alive 1
no_pick 1
monster 1
end
more
object djinn_magenta_big_3
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
y 2
weight 900000
alive 1
no_pick 1
monster 1
end
more
object djinn_magenta_big_4
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
y 3
weight 900000
alive 1
no_pick 1
monster 1
end
more
object djinn_magenta_big_5
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
y 4
weight 900000
alive 1
no_pick 1
monster 1
end
more
object djinn_magenta_big_6
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
y 5
weight 900000
alive 1
no_pick 1
monster 1
end
more
object djinn_magenta_big_7
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
x 1
weight 900000
alive 1
no_pick 1
monster 1
end
more
object djinn_magenta_big_8
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
x 1
y 1
weight 900000
alive 1
no_pick 1
monster 1
end
more
object djinn_magenta_big_9
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
x 1
y 2
weight 900000
alive 1
no_pick 1
monster 1
end
more
object djinn_magenta_big_10
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
x 1
y 3
weight 900000
alive 1
no_pick 1
monster 1
end
more
object djinn_magenta_big_12
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
x 1
y 4
weight 900000
alive 1
no_pick 1
monster 1
end
more
object djinn_magenta_big_13
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
x 1
y 5
weight 900000
alive 1
no_pick 1
monster 1
end
more
object djinn_magenta_big_14
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
x 2
weight 900000
alive 1
no_pick 1
monster 1
end
more
object djinn_magenta_big_15
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
x 2
y 1
weight 900000
alive 1
no_pick 1
monster 1
end
more
object djinn_magenta_big_16
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
x 2
y 2
weight 900000
alive 1
no_pick 1
monster 1
end
more
object djinn_magenta_big_17
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
x 2
y 3
weight 900000
alive 1
no_pick 1
monster 1
end
more
object djinn_magenta_big_18
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
x 2
y 4
weight 900000
alive 1
no_pick 1
monster 1
end
more
object djinn_magenta_big_19
anim
facings 2
djinn_magenta_big.x11
djinn_magenta_big.x12
djinn_magenta_big.x21
djinn_magenta_big.x22
mina
name djinn
face djinn_magenta_big.x11
x 2
y 5
weight 900000
alive 1
no_pick 1
monster 1
end

