object stonegiant
anim
stonegiant.x11
stonegiant.x12
mina
name stone giant
race giant
face stonegiant.x11
str 60
con 10
wis 10
int 13
hp 2500
maxhp 2500
exp 3500
dam 40
wc -20
ac -40
speed -0.085
level 60
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_poison 100
resist_slow 100
resist_paralyze 100
resist_turn_undead 100
resist_fear 100
resist_death 100
resist_blind 100
weight 300000
randomitems stonegiant_poor
run_away 6
pick_up 1
alive 1
no_pick 1
monster 1
can_use_weapon 1
sleep 1
can_use_skill 1
body_arm 2
body_skill 1
end
more
object stonegiant_2
anim
stonegiant.x11
stonegiant.x12
mina
name stone giant
face stonegiant.x11
y 1
weight 300000
alive 1
no_pick 1
monster 1
end

object stonegiant_onetile
anim
stonegiant.x11
stonegiant.x12
mina
name stone giant
race giant
face stonegiant.x11
str 60
con 10
wis 10
int 13
hp 2500
maxhp 2500
exp 3500
dam 40
wc -20
ac -40
speed -0.085
level 60
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_poison 100
resist_slow 100
resist_paralyze 100
resist_turn_undead 100
resist_fear 100
resist_death 100
resist_blind 100
weight 300000
randomitems stonegiant_poor
run_away 6
pick_up 1
alive 1
no_pick 1
monster 1
can_use_weapon 1
sleep 1
can_use_skill 1
body_arm 2
body_skill 1
end

