object fungus
anim
fungus.x11
fungus.x12
fungus.x13
fungus.x14
fungus.x15
mina
name violent fungi
race slime
other_arch fungus
face fungus.x11
wis 5
int 0
hp 5
maxhp 5
sp 129
maxsp 20
exp 25
dam 2
wc 2
ac 8
speed -0.03
level 3
attacktype 1024
weight 10000
alive 1
no_pick 1
monster 1
generator 1
sleep 1
end

