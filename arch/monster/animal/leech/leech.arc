object leech
anim
facings 2
leech.x31
leech.x32
leech.x33
leech.x32
leech.x71
leech.x72
leech.x73
leech.x72
mina
name giant leech
race animal
face leech.x71
con 2
wis 5
int 4
hp 350
maxhp 350
exp 7000
dam 30
wc 0
ac 3
speed -0.2
attack_movement 5
level 20
attacktype 16844928
resist_physical 20
resist_electricity -20
resist_cold 30
resist_acid -70
resist_drain 70
resist_poison 50
weight 500000
randomitems leech
run_away 0
alive 1
no_pick 1
monster 1
sleep 1
can_see_in_dark 1
anim_speed 4
end

