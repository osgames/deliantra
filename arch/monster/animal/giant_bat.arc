object giant_bat
anim
giant_bat.x11
giant_bat.x12
giant_bat.x13
mina
name giant bat
race animal
face giant_bat.x11
wis 15
int 9
hp 30
maxhp 30
exp 100
dam 13
wc 10
ac 2
speed 0.3
attack_movement 3
level 4
weight 5000
randomitems giant_bat
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
sleep 1
end

