object mouse
anim
mouse.x11
mouse.x12
mina
race animal
other_arch mouse
face mouse.x11
int 6
hp 1
maxhp 1
maxsp 15
exp 5
dam 2
wc 25
ac 7
speed -0.11
attack_movement 2
level 1
weight 80
randomitems mouse
alive 1
no_pick 1
monster 1
generator 1
end

object mouse_nongen
inherit mouse
name city mouse
int 7
generator 0
end

