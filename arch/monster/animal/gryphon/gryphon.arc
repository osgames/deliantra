object gryphon
anim
facings 2
gryphon.x31
gryphon.x32
gryphon.x33
gryphon.x34
gryphon.x33
gryphon.x32
gryphon.x71
gryphon.x72
gryphon.x73
gryphon.x74
gryphon.x73
gryphon.x72
mina
name Dark Gryphon
race animal
face gryphon.x31
str 80
dex 30
con 28
wis 15
pow 48
int 20
hp 3500
maxhp 3500
sp 100
maxsp 100
exp 350000
dam 19
wc -20
ac -15
speed 0.5
attack_movement 1
level 18
attacktype 128
resist_magic 100
resist_poison 100
resist_paralyze 100
resist_fear 100
resist_godpower 60
resist_holyword 60
weight 400000
randomitems gryphon
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 0
can_cast_spell 1
can_use_scroll 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
anim_speed 0.4
can_use_wand 1
end
more
object gryphon2
anim
facings 2
gryphon.x31
gryphon.x32
gryphon.x33
gryphon.x34
gryphon.x33
gryphon.x32
gryphon.x71
gryphon.x72
gryphon.x73
gryphon.x74
gryphon.x73
gryphon.x72
mina
name Dark Gryphon
face gryphon.x31
x 1
alive 1
monster 1
anim_speed 1.0
end
more
object gryphon3
anim
facings 2
gryphon.x31
gryphon.x32
gryphon.x33
gryphon.x34
gryphon.x33
gryphon.x32
gryphon.x71
gryphon.x72
gryphon.x73
gryphon.x74
gryphon.x73
gryphon.x72
mina
name Dark Gryphon
face gryphon.x31
x 2
alive 1
anim_speed 1.0
end
more
object gryphon4
anim
facings 2
gryphon.x31
gryphon.x32
gryphon.x33
gryphon.x34
gryphon.x33
gryphon.x32
gryphon.x71
gryphon.x72
gryphon.x73
gryphon.x74
gryphon.x73
gryphon.x72
mina
name Dark Gryphon
face gryphon.x31
y 1
alive 1
monster 1
anim_speed 1.0
end
more
object gryphon5
anim
facings 2
gryphon.x31
gryphon.x32
gryphon.x33
gryphon.x34
gryphon.x33
gryphon.x32
gryphon.x71
gryphon.x72
gryphon.x73
gryphon.x74
gryphon.x73
gryphon.x72
mina
name Dark Gryphon
face gryphon.x31
x 1
y 1
alive 1
monster 1
anim_speed 1.0
end
more
object gryphon6
anim
facings 2
gryphon.x31
gryphon.x32
gryphon.x33
gryphon.x34
gryphon.x33
gryphon.x32
gryphon.x71
gryphon.x72
gryphon.x73
gryphon.x74
gryphon.x73
gryphon.x72
mina
name Dark Gryphon
face gryphon.x31
x 2
y 1
alive 1
monster 1
anim_speed 1.0
end
more
object gryphon7
anim
facings 2
gryphon.x31
gryphon.x32
gryphon.x33
gryphon.x34
gryphon.x33
gryphon.x32
gryphon.x71
gryphon.x72
gryphon.x73
gryphon.x74
gryphon.x73
gryphon.x72
mina
name Dark Gryphon
face gryphon.x31
y 2
alive 1
monster 1
anim_speed 1.0
end
more
object gryphon8
anim
facings 2
gryphon.x31
gryphon.x32
gryphon.x33
gryphon.x34
gryphon.x33
gryphon.x32
gryphon.x71
gryphon.x72
gryphon.x73
gryphon.x74
gryphon.x73
gryphon.x72
mina
name Dark Gryphon
face gryphon.x31
x 1
y 2
alive 1
monster 1
anim_speed 1.0
end
more
object gryphon9
anim
facings 2
gryphon.x31
gryphon.x32
gryphon.x33
gryphon.x34
gryphon.x33
gryphon.x32
gryphon.x71
gryphon.x72
gryphon.x73
gryphon.x74
gryphon.x73
gryphon.x72
mina
name Dark Gryphon
face gryphon.x31
x 2
y 2
alive 1
monster 1
anim_speed 1.0
end

