object generate_scorpion
name generator
race animal
other_arch scorpion
face scorpi_gen.x11
hp 50
maxhp 50
maxsp 1
exp 50
ac 3
speed 0.002
level 1
weight 10000
alive 1
generator 1
end

