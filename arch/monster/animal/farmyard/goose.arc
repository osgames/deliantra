object goose
anim
facings 2
goose.x71
goose.x72
goose.x31
goose.x32
mina
race bird
face goose.x31
wis 1
int 4
hp 12
maxhp 12
exp 1
dam 15
wc 15
ac 9
speed -0.2
attack_movement 2
level 2
weight 8500
randomitems goose
run_away 80
alive 1
monster 1
end

