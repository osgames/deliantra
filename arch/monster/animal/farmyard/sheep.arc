object sheep
anim
facings 1
sheep.x31
sheep.x71
mina
race sheep
face sheep.x31
wis 5
int 8
hp 5
maxhp 5
exp 1
dam 1
wc 15
ac 9
speed 0.2
attack_movement 2
level 1
attacktype 18
resist_cold 10
weight 60000
randomitems sheep
run_away 80
alive 1
monster 1
end

