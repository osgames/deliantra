object chicken
anim
facings 2
chicken.x31
chicken.x32
chicken.x71
chicken.x72
mina
race bird
face chicken.x31
wis 1
int 4
hp 3
maxhp 3
exp 1
dam 0
wc 15
ac 9
speed -0.2
attack_movement 2
level 1
weight 1500
randomitems chicken
run_away 80
alive 1
monster 1
end

