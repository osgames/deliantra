object purple_worm
anim
facings 2
purple_worm.x31
purple_worm.x71
mina
name Purple Worm
race animal
face purple_worm.x31
str 40
dex 15
con 30
wis 20
pow 30
int 10
hp 9000
maxhp 9000
sp 80
maxsp 80
exp 100000
dam 50
wc -100
ac -15
speed -0.05
level 116
attacktype 3137
resist_magic 30
resist_fire 30
resist_electricity 50
resist_acid 100
resist_poison 100
resist_paralyze 100
resist_death 100
resist_blind 100
weight 1800000
randomitems purple_worm
alive 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
can_see_in_dark 1
armour 85
end
more
object purple_worm_2
anim
facings 2
purple_worm.x31
purple_worm.x71
mina
name Purple Worm
face purple_worm.x31
x 1
alive 1
end
more
object purple_worm_3
anim
facings 2
purple_worm.x31
purple_worm.x71
mina
name Purple Worm
face purple_worm.x31
y 1
alive 1
end
more
object purple_worm_4
anim
facings 2
purple_worm.x31
purple_worm.x71
mina
name Purple Worm
face purple_worm.x31
x 1
y 1
alive 1
end

