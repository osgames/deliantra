object long_worm
anim
facings 2
long_worm.x31
long_worm.x71
mina
name Long Worm
race animal
face long_worm.x31
str 40
dex 15
con 30
wis 20
pow 100
int 10
hp 4000
maxhp 4000
sp 3000
maxsp 3000
exp 100000
dam 50
wc -70
ac -15
speed -0.05
level 116
attacktype 3137
resist_magic 30
resist_fire 30
resist_electricity 50
resist_acid 100
resist_poison 100
resist_paralyze 100
resist_death 100
resist_blind 100
weight 1800000
randomitems long_worm
alive 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
can_see_in_dark 1
armour 85
end
more
object long_worm_2
anim
facings 2
long_worm.x31
long_worm.x71
mina
name Long Worm
face long_worm.x31
x 2
alive 1
end
more
object long_worm_3
anim
facings 2
long_worm.x31
long_worm.x71
mina
name Long Worm
face long_worm.x31
x 4
alive 1
end
more
object long_worm_4
anim
facings 2
long_worm.x31
long_worm.x71
mina
name Long Worm
face long_worm.x31
x 6
alive 1
end
more
object long_worm_5
anim
facings 2
long_worm.x31
long_worm.x71
mina
name Long Worm
face long_worm.x31
x 8
alive 1
end
more
object long_worm_6
anim
facings 2
long_worm.x31
long_worm.x71
mina
name Long Worm
face long_worm.x31
x 10
alive 1
end
more
object long_worm_7
anim
facings 2
long_worm.x31
long_worm.x71
mina
name Long Worm
face long_worm.x31
x 12
alive 1
end

