object ape
anim
facings 2
ape.x31
ape.x32
ape.x71
ape.x72
mina
name ape
race animal
face ape.x71
str 24
dex 25
con 2
wis 6
int 10
hp 160
maxhp 160
exp 600
dam 14
wc 5
ac 0
speed -0.2
level 7
resist_physical 20
resist_electricity 30
weight 90000
randomitems ape
run_away 35
alive 1
no_pick 1
monster 1
sleep 1
end

