object shadowtiger
anim
facings 2
shadowtiger.x31
shadowtiger.x32
shadowtiger.x33
shadowtiger.x34
shadowtiger.x32
blank.x11
blank.x11
blank.x11
blank.x11
blank.x11
shadowtiger.x31
blank.x11
blank.x11
blank.x11
shadowtiger.x71
shadowtiger.x72
shadowtiger.x73
shadowtiger.x74
shadowtiger.x72
blank.x11
blank.x11
blank.x11
blank.x11
blank.x11
shadowtiger.x71
blank.x11
blank.x11
blank.x11
mina
name Shadow Tiger
race animal
sound_destroy ss/roar
face shadowtiger.x31
con 30
int 18
hp 2000
maxhp 2000
exp 100000
dam 33
wc -20
ac -13
speed -0.3
attack_movement 3
level 20
attacktype 71041
resist_physical 100
resist_magic 100
resist_electricity 50
resist_confusion 100
resist_drain 100
resist_weaponmagic 50
resist_poison 100
resist_slow 100
resist_paralyze 100
weight 2000000
run_away 2
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
end
more
object shadowtiger_2
anim
facings 2
shadowtiger.x31
shadowtiger.x32
shadowtiger.x33
shadowtiger.x34
shadowtiger.x32
blank.x11
blank.x11
blank.x11
blank.x11
blank.x11
shadowtiger.x31
blank.x11
blank.x11
blank.x11
shadowtiger.x71
shadowtiger.x72
shadowtiger.x73
shadowtiger.x74
shadowtiger.x72
blank.x11
blank.x11
blank.x11
blank.x11
blank.x11
shadowtiger.x71
blank.x11
blank.x11
blank.x11
mina
name Shadow Tiger
face shadowtiger.x31
x 1
alive 1
end
more
object shadowtiger_3
anim
facings 2
shadowtiger.x31
shadowtiger.x32
shadowtiger.x33
shadowtiger.x34
shadowtiger.x32
blank.x11
blank.x11
blank.x11
blank.x11
blank.x11
shadowtiger.x31
blank.x11
blank.x11
blank.x11
shadowtiger.x71
shadowtiger.x72
shadowtiger.x73
shadowtiger.x74
shadowtiger.x72
blank.x11
blank.x11
blank.x11
blank.x11
blank.x11
shadowtiger.x71
blank.x11
blank.x11
blank.x11
mina
name Shadow Tiger
face shadowtiger.x31
y 1
alive 1
end
more
object shadowtiger_4
anim
facings 2
shadowtiger.x31
shadowtiger.x32
shadowtiger.x33
shadowtiger.x34
shadowtiger.x32
blank.x11
blank.x11
blank.x11
blank.x11
blank.x11
shadowtiger.x31
blank.x11
blank.x11
blank.x11
shadowtiger.x71
shadowtiger.x72
shadowtiger.x73
shadowtiger.x74
shadowtiger.x72
blank.x11
blank.x11
blank.x11
blank.x11
blank.x11
shadowtiger.x71
blank.x11
blank.x11
blank.x11
mina
name Shadow Tiger
face shadowtiger.x31
x 1
y 1
alive 1
end

