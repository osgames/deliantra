object shadowpup
anim
facings 2
panther.x31
panther.x32
blank.x11
panther.x33
blank.x11
panther.x71
panther.x72
blank.x11
panther.x73
blank.x11
mina
name shadowtiger pup
race animal
sound_destroy ss/roar
face panther.x31
con 2
wis 20
int 14
hp 100
maxhp 100
exp 60
dam 10
wc 1
ac 4
speed -0.3
attack_movement 3
level 4
weight 200000
run_away 15
alive 1
monster 1
sleep 1
end

