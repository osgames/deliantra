object behemoth
anim
facings 2
behemoth.x31
behemoth.x32
behemoth.x33
behemoth.x32
behemoth.x71
behemoth.x72
behemoth.x73
behemoth.x72
mina
race animal
face behemoth.x31
con 2
wis 20
int 8
hp 800
maxhp 800
exp 20000
dam 16
wc -10
ac -6
speed -0.5
level 12
attacktype 1025
resist_magic 30
resist_poison 90
resist_fear 100
weight 1800000
randomitems behemoth
alive 1
monster 1
sleep 1
end
more
object behemoth_2
anim
facings 2
behemoth.x31
behemoth.x32
behemoth.x33
behemoth.x32
behemoth.x71
behemoth.x72
behemoth.x73
behemoth.x72
mina
face behemoth.x31
x 1
alive 1
end
more
object behemoth_3
anim
facings 2
behemoth.x31
behemoth.x32
behemoth.x33
behemoth.x32
behemoth.x71
behemoth.x72
behemoth.x73
behemoth.x72
mina
face behemoth.x31
y 1
alive 1
end
more
object behemoth_4
anim
facings 2
behemoth.x31
behemoth.x32
behemoth.x33
behemoth.x32
behemoth.x71
behemoth.x72
behemoth.x73
behemoth.x72
mina
face behemoth.x31
x 1
y 1
alive 1
end

