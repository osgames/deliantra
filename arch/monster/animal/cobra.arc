object black_metallic_serpent
anim
cobra_black.x11
cobra_black.x12
cobra_black.x13
cobra_black.x12
mina
inherit cobra
name black metallic serpent
face cobra_black.x12
int 8
hp 500
maxhp 500
exp 10000
dam 30
attacktype 82945
resist_fire 30
resist_electricity 30
resist_poison 100
randomitems black_metallic_serpent
can_see_in_dark 1
end
more
object black_metallic_serpent_2
anim
cobra_black.x11
cobra_black.x12
cobra_black.x13
cobra_black.x12
mina
inherit cobra_2
face cobra_black.x12
y 1
end

object black_metallic_serpent_lightning
inherit black_metallic_serpent_sleek
name One sleek black metallic serpent
int 10
hp 1000
maxhp 1000
exp 65000
speed 1
randomitems black_metallic_serpent_lightning
end

object black_metallic_serpent_lightning_2
inherit black_metallic_serpent_sleek_2
end

object black_metallic_serpent_sleek
inherit black_metallic_serpent
name One sleek black metallic serpent
int 10
hp 500
maxhp 500
exp 25000
speed 0.7
end
more
object black_metallic_serpent_sleek_2
inherit black_metallic_serpent_2
end

object cobra
anim
cobra.x11
cobra.x12
cobra.x13
cobra.x12
mina
name giant cobra
race reptile
face cobra.x12
wis 10
int 10
hp 30
maxhp 30
exp 150
dam 40
wc -10
ac -2
speed -0.1
level 5
attacktype 1025
weight 20000
randomitems cobra
alive 1
no_pick 1
monster 1
sleep 1
anim_speed 3
end
more
object cobra_2
anim
cobra.x11
cobra.x12
cobra.x13
cobra.x12
mina
name giant cobra
face cobra.x12
y 1
alive 1
no_pick 1
end

