object panther
anim
facings 2
panther.x31
panther.x32
panther.x33
panther.x71
panther.x72
panther.x73
mina
race animal
sound_destroy ss/roar
face panther.x31
con 3
wis 20
int 8
hp 50
maxhp 50
exp 70
dam 10
wc 1
ac 4
speed -0.3
attack_movement 3
level 4
weight 60000
randomitems panther
run_away 15
alive 1
monster 1
sleep 1
can_see_in_dark 1
end

