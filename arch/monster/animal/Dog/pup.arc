object pup
anim
facings 2
pup.x31
pup.x32
pup.x33
pup.x32
pup.x31
pup.x71
pup.x72
pup.x73
pup.x72
pup.x71
mina
race animal
face pup.x31
con 1
wis 10
int 10
hp 4
maxhp 4
exp 5
dam 2
wc 1
ac 4
speed -0.2
attack_movement 3
level 1
weight 5000
randomitems pup
run_away 60
alive 1
monster 1
sleep 1
end

