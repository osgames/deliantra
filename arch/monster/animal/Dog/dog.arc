object dog
anim
facings 2
dog.x31
dog.x32
dog.x33
dog.x32
dog.x71
dog.x72
dog.x73
dog.x72
mina
race animal
face dog.x31
con 2
wis 20
int 8
hp 10
maxhp 10
exp 30
dam 5
wc 1
ac 4
speed -0.2
attack_movement 3
level 4
weight 30000
randomitems dog
run_away 15
alive 1
monster 1
sleep 1
end

