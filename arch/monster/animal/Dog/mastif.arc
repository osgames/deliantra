object mastif
anim
facings 2
mastif.x31
mastif.x32
mastif.x33
mastif.x32
mastif.x71
mastif.x72
mastif.x73
mastif.x72
mina
name mastif
race animal
face mastif.x31
con 2
wis 20
int 8
hp 120
maxhp 120
exp 200
dam 9
wc 1
ac 1
speed -0.2
attack_movement 3
level 8
weight 60000
randomitems mastif
run_away 5
alive 1
monster 1
sleep 1
can_see_in_dark 1
end

