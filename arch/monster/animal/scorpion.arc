object scorpion
anim
scorpion.x11
scorpion.x12
mina
race animal
face scorpion.x11
con 2
wis 8
int 4
hp 30
maxhp 30
exp 70
dam 10
wc 0
ac 10
speed 0.1
level 5
attacktype 1025
resist_fire 100
resist_electricity 50
resist_cold -100
resist_confusion -100
weight 500
randomitems scorpion
alive 1
no_pick 1
monster 1
sleep 1
end

