object giantrat
anim
facings 2
giantrat.x31
giantrat.x32
giantrat.x33
giantrat.x32
giantrat.x31
giantrat.x71
giantrat.x72
giantrat.x73
giantrat.x72
giantrat.x71
mina
name giant rat
race animal
face giantrat.x31
str 30
dex 30
con 20
wis 10
int 12
hp 4000
maxhp 4000
exp 30000
dam 60
wc -10
ac -6
speed -0.4
attack_movement 3
level 25
attacktype 1
resist_poison 75
resist_fear 100
weight 2400000
randomitems giantrat
alive 1
monster 1
can_cast_spell 1
sleep 1
end
more
object giantrat_2
anim
facings 2
giantrat.x31
giantrat.x32
giantrat.x33
giantrat.x32
giantrat.x31
giantrat.x71
giantrat.x72
giantrat.x73
giantrat.x72
giantrat.x71
mina
face giantrat.x31
x 1
alive 1
end
more
object giantrat_3
anim
facings 2
giantrat.x31
giantrat.x32
giantrat.x33
giantrat.x32
giantrat.x31
giantrat.x71
giantrat.x72
giantrat.x73
giantrat.x72
giantrat.x71
mina
face giantrat.x31
y 1
alive 1
end
more
object giantrat_4
anim
facings 2
giantrat.x31
giantrat.x32
giantrat.x33
giantrat.x32
giantrat.x31
giantrat.x71
giantrat.x72
giantrat.x73
giantrat.x72
giantrat.x71
mina
face giantrat.x31
x 1
y 1
alive 1
end

