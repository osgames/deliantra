object bear
anim
bear.x31
bear.x32
bear.x71
bear.x72
facings 2
mina
name bear
race animal
face bear.x31
str 30
con 2
wis 10
int 8
hp 200
maxhp 200
exp 700
dam 20
wc 2
ac 1
speed -0.085
attack_movement 4
level 6
attacktype 1
weight 300000
randomitems bear
run_away 6
alive 1
no_pick 1
monster 1
sleep 1
end
more
object bear_2
anim
bear.x31
bear.x32
bear.x71
bear.x72
facings 2
mina
name bear
face bear.x31
y 1
alive 1
no_pick 1
monster 1
end

