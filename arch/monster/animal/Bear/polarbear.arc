object polarbear
anim
polarbear.x31
polarbear.x32
polarbear.x71
polarbear.x72
facings 2
mina
name polar bear
race animal
face polarbear.x31
str 30
con 4
wis 10
int 13
hp 250
maxhp 250
exp 1300
dam 20
wc 2
ac 1
speed -0.085
attack_movement 4
level 9
attacktype 1
resist_cold 50
weight 600000
randomitems polarbear
run_away 6
alive 1
no_pick 1
monster 1
sleep 1
end
more
object polarbear_2
anim
polarbear.x31
polarbear.x32
polarbear.x71
polarbear.x72
facings 2
mina
name polar bear
face polarbear.x31
y 1
alive 1
no_pick 1
monster 1
end

