object bird
anim
facings 4
bird.x11
bird.x12
bird.x31
bird.x32
bird.x51
bird.x52
bird.x71
bird.x72
mina
race bird
sound_destroy ss/jungle-bird
face bird.x11
wis 8
int 5
hp 3
maxhp 3
exp 20
dam 1
wc 15
ac 2
speed -0.3
attack_movement 3
level 1
weight 1500
randomitems bird
run_away 80
move_type flying
alive 1
no_pick 1
monster 1
sleep 1
end

