object neko
anim
facings 2
neko.x11
neko.x12
neko.x13
neko.x14
neko.x15
neko.x16
neko.x17
neko.x18
mina
name neko chan
race animal
face neko.x11
wis 8
int 30
hp 2
maxhp 2
exp 1000
dam 2
wc 27
ac 18
speed 0.3
level 1
weight 30000
randomitems neko
run_away 90
alive 1
no_pick 1
monster 1
sleep 1
end

object nekosan
attach [["Nekosan"]]
anim
facings 2
neko.x11
neko.x12
neko.x13
neko.x14
neko.x15
neko.x16
neko.x17
neko.x18
mina
name neko san
race animal
face neko.x11
str 100
dex 100
con 100
wis 100
pow 100
cha 100
int 30
hp 30000
maxhp 30000
exp 1
dam 1
wc 120
ac -120
speed 0.07
attack_movement 54
level 200
resist_physical 99
resist_magic 100
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
resist_weaponmagic 100
resist_ghosthit 100
resist_poison 100
resist_slow 100
resist_paralyze 100
resist_turn_undead 100
resist_fear 100
resist_deplete 100
resist_death 100
resist_chaos 100
resist_godpower 100
resist_holyword 100
resist_blind 100
resist_disease 100
weight 30000
run_away 90
alive 1
no_pick 1
monster 1
identified 1
hitback 0
unaggressive 1
reflect_missile 1
reflect_spell 1
no_strength 1
sleep 0
end

