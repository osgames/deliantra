object horse2
race animal
face horse2.x31
con 2
wis 20
int 7
hp 10
maxhp 10
exp 30
dam 5
wc 1
ac 4
speed -0.2
attack_movement 3
level 4
weight 30000
run_away 15
alive 1
monster 1
sleep 1
end

object horse2u
name untamed horse
race horse
face horse2.x31
con 6
wis 6
pow 6
int 6
hp 200
maxhp 200
exp 22
dam 25
wc 4
ac 4
speed -0.1
level 8
attacktype 1
resist_fear -100
weight 600000
run_away 5
alive 1
no_pick 1
monster 1
sleep 1
end

