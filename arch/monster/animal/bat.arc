object bat
anim
bat.x11
bat.x12
bat.x13
mina
race animal
face bat.x11
wis 15
int 5
hp 2
maxhp 2
exp 8
dam 3
wc 12
ac 4
speed 0.2
level 2
resist_physical 30
weight 20
randomitems bat
run_away 80
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
sleep 1
end

