object snake
anim
snake.x11
snake.x12
snake.x13
mina
race reptile
other_arch snake
face snake.x11
wis 8
int 4
hp 5
maxhp 5
sp 134
maxsp 60
exp 35
dam 8
wc 10
ac 4
speed -0.1
level 4
weight 1000
randomitems snake
alive 1
no_pick 1
monster 1
generator 1
sleep 1
end

object snake_nongen
inherit snake
name city snake
int 5
generator 0
end

