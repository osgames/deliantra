object cockatrice
anim
cockatrice.x13
cockatrice.x17
mina
name cockatrice
race bird
face cockatrice.x17
con 11
wis 20
pow 32
int 14
hp 3500
maxhp 3500
sp 70
maxsp 100
exp 40000
dam 10
wc -127
ac -12
speed -0.4
level 116
resist_confusion 100
resist_fear 100
resist_blind 100
weight 200000
randomitems cockatrice
run_away 3
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
can_see_in_dark 1
end
more
object cockatrice_2
anim
cockatrice.x13
cockatrice.x17
mina
name cockatrice
face cockatrice.x17
x 1
weight 100000
move_type fly_low
alive 1
monster 1
end
more
object cockatrice_3
anim
cockatrice.x13
cockatrice.x17
mina
name cockatrice
face cockatrice.x17
y 1
weight 100000
move_type fly_low
alive 1
monster 1
end
more
object cockatrice_4
anim
cockatrice.x13
cockatrice.x17
mina
name cockatrice
face cockatrice.x17
x 1
y 1
weight 100000
move_type fly_low
alive 1
monster 1
end
more
object cockatrice_5
anim
cockatrice.x13
cockatrice.x17
mina
name cockatrice
face cockatrice.x17
y 2
weight 100000
move_type fly_low
alive 1
monster 1
end
more
object cockatrice_6
anim
cockatrice.x13
cockatrice.x17
mina
name cockatrice
face cockatrice.x17
x 1
y 2
weight 100000
move_type fly_low
alive 1
monster 1
end

