object cockatrice_small
anim
cockatrice_small.x11
cockatrice_small.x12
mina
name baby cockatrice
race bird
face cockatrice_small.x11
con 11
wis 20
pow 32
int 8
hp 500
maxhp 500
sp 70
maxsp 100
exp 40000
dam 5
wc -30
ac -6
speed -0.1
level 116
resist_confusion 50
resist_fear 50
resist_blind 50
weight 2000
randomitems cockatrice
run_away 3
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
can_see_in_dark 1
end

