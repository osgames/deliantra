object wolf
anim
wolf.x71
wolf.x71
wolf.x72
wolf.x31
wolf.x31
wolf.x32
facings 2
mina
race animal
sound_destroy wn/wolf-die
face wolf.x71
con 2
wis 20
int 8
hp 150
maxhp 160
exp 500
dam 25
wc 1
ac 1
speed -0.2
attack_movement 3
level 5
attacktype 1
weight 55000
randomitems wolf
run_away 15
alive 1
monster 1
sleep 1
can_see_in_dark 1
end

