object dire_wolf_sire
anim
dwolf.x71
dwolf.x71
dwolf.x72
dwolf.x31
dwolf.x31
dwolf.x32
facings 2
mina
name dire wolf sire
race animal
sound_destroy ss/roar
face dwolf.x71
con 4
wis 20
pow 7
int 18
hp 1450
maxhp 1460
sp 45
maxsp 45
exp 16000
dam 45
wc 1
ac -6
speed 0.2
attack_movement 3
level 18
attacktype 17
resist_fire -5
resist_cold 60
weight 160000
randomitems dwolf_sire
run_away 4
alive 1
no_pick 1
monster 1
can_cast_spell 1
sleep 1
can_see_in_dark 1
end

