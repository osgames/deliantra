object dire_wolf
anim
dwolf.x71
dwolf.x71
dwolf.x72
dwolf.x31
dwolf.x31
dwolf.x32
facings 2
mina
name dire wolf
race animal
sound_destroy ss/roar
face dwolf.x71
con 2
wis 20
pow 5
int 16
hp 450
maxhp 460
sp 30
maxsp 30
exp 6000
dam 45
wc 1
ac -4
speed 0.2
attack_movement 3
level 9
attacktype 17
resist_fire -25
resist_cold 50
weight 80000
randomitems dwolf
run_away 7
alive 1
no_pick 1
monster 1
can_cast_spell 1
sleep 1
can_see_in_dark 1
end

