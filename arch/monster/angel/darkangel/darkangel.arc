object darkangel
anim
darkangel.x11
darkangel.x13
darkangel.x12
darkangel.x13
mina
name angel of darkness
race angel
face darkangel.x11
con 30
wis 15
pow 30
int 30
hp 3000
maxhp 3000
sp 300
maxsp 300
exp 60000
dam 55
wc -106
ac -80
speed 0.3
level 115
attacktype 1048577
resist_physical 25
resist_magic 100
resist_electricity 25
resist_cold 25
resist_confusion 100
resist_acid 25
resist_drain 25
resist_weaponmagic 25
resist_ghosthit 90
resist_poison 25
resist_slow 25
resist_paralyze 100
resist_turn_undead 100
resist_fear 100
resist_cancellation 25
resist_deplete 25
resist_chaos 25
resist_counterspell 25
resist_godpower 25
resist_holyword 40
resist_blind 90
weight 75000
randomitems darkangel
run_away 18
pick_up 24
will_apply 2
move_type fly_low
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

