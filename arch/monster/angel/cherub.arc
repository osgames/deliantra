object cherub
name cherub
race angel
face cherub.x11
wis 8
int 8
hp 25
maxhp 25
exp 35
dam 5
wc 16
ac 6
speed 0.3
level 3
attacktype 1
resist_magic 50
weight 25000
run_away 18
move_type walk fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
can_see_in_dark 1
end

