object angel
anim
angel.x11
angel.x13
angel.x12
angel.x13
mina
name angel
race angel
face angel.x11
con 3
wis 15
pow 16
int 20
hp 150
maxhp 150
maxsp 40
exp 800
dam 12
wc 1
ac 0
speed 0.2
attack_movement 4
level 12
attacktype 1048577
resist_magic 100
resist_blind 80
weight 75000
randomitems angel
run_away 18
pick_up 24
will_apply 2
move_type fly_low
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

