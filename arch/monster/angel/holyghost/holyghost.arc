object holyghost
name holy ghost
race angel
face holyghost.x11
wis 10
int 14
hp 30
maxhp 30
exp 150
dam 30
wc 12
ac 0
speed 0.2
level 5
attacktype 512
resist_magic 50
resist_blind 50
weight 25000
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
sleep 1
can_see_in_dark 1
end

