object destroying_angel
name Retributioner
race angel
face retributioner.x11
con 10
wis 12
pow 32
int 20
hp 4000
maxhp 4000
sp 60
maxsp 60
exp 150000
dam 50
wc -30
ac -5
speed 0.3
level 27
attacktype 1048577
resist_magic 80
resist_holyword 80
resist_blind 100
weight 75000
randomitems angel
run_away 18
pick_up 24
will_apply 2
move_type fly_low
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end
more
object destroying_angel_2
name Retributioner
face retributioner.x11
x 1
weight 75000
alive 1
no_pick 1
monster 1
end
more
object destroying_angel_3
name Retributioner
face retributioner.x11
y 1
weight 75000
alive 1
no_pick 1
monster 1
end
more
object destroying_angel_4
name Retributioner
face retributioner.x11
x 1
y 1
weight 75000
alive 1
no_pick 1
monster 1
end
more
object destroying_angel_5
name Retributioner
face retributioner.x11
x 0
y 2
weight 75000
alive 1
no_pick 1
monster 1
end
more
object destroying_angel_6
name Retributioner
face retributioner.x11
x 1
y 2
weight 75000
alive 1
no_pick 1
monster 1
end

