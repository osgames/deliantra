object HighAngel
anim
archangel.x11
archangel.x12
archangel.x13
archangel.x14
archangel.x15
archangel.x16
archangel.x17
archangel.x18
mina
name High Angel
race angel
face archangel.x18
str 29
con 40
wis 15
pow 60
int 25
hp 2500
maxhp 2500
sp 100
maxsp 200
exp 200000
dam 20
wc -20
ac -5
speed 0.400000
attack_movement 4
level 25
attacktype 20745
resist_physical 50
resist_magic 100
resist_fire 50
resist_electricity 100
resist_cold 50
resist_confusion 100
resist_drain 100
resist_poison 100
resist_slow 100
resist_paralyze 100
resist_fear 100
resist_holyword 80
resist_blind 80
weight 150000
randomitems high_angel
run_away 18
pick_up 24
will_apply 2
move_type fly_low
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
reflect_spell 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

