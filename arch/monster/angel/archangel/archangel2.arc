object ArchAngel
anim
highangel.x11
highangel.x12
highangel.x13
mina
name Arch Angel
race angel
face highangel.x11
str 29
con 40
pow 60
int 25
hp 16000
maxhp 16000
sp 100
maxsp 200
exp 500000
dam 60
wc -40
ac -10
speed 0.300000
level 50
attacktype 20745
resist_physical 75
resist_magic 100
resist_fire 75
resist_electricity 100
resist_cold 75
resist_confusion 100
resist_drain 100
resist_poison 100
resist_slow 100
resist_paralyze 100
resist_fear 100
resist_godpower 80
resist_holyword 30
resist_blind 100
weight 150000
randomitems archangel
run_away 18
pick_up 24
will_apply 2
move_type fly_low
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
reflect_spell 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end
more
object ArchAngel_2
anim
highangel.x11
highangel.x12
highangel.x13
mina
name Arch Angel
face highangel.x11
x 1
weight 75000
alive 1
no_pick 1
monster 1
end
more
object ArchAngel_3
anim
highangel.x11
highangel.x12
highangel.x13
mina
name Arch Angel
face highangel.x11
y 1
weight 75000
alive 1
no_pick 1
monster 1
end
more
object ArchAngel_4
anim
highangel.x11
highangel.x12
highangel.x13
mina
name Arch Angel
face highangel.x11
x 1
y 1
weight 75000
alive 1
no_pick 1
monster 1
end
more
object ArchAngel_5
anim
highangel.x11
highangel.x12
highangel.x13
mina
name Arch Angel
face highangel.x11
x 0
y 2
weight 75000
alive 1
no_pick 1
monster 1
end
more
object ArchAngel_6
anim
highangel.x11
highangel.x12
highangel.x13
mina
name Arch Angel
face highangel.x11
x 1
y 2
weight 75000
alive 1
no_pick 1
monster 1
end

