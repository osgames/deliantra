object angel_knight
name Angel Knight
race angel
face angelknight.x11
wis 12
int 14
hp 100
maxhp 100
exp 600
dam 20
wc 6
ac 0
speed 0.3
level 8
attacktype 1048577
resist_magic 50
resist_holyword 40
resist_blind 90
weight 75000
randomitems all_spell_skills
run_away 18
pick_up 24
will_apply 2
move_type fly_low
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end
more
object angel_knight_2
name Angel Knight
face angelknight.x11
y 1
weight 75000
alive 1
no_pick 1
monster 1
end

