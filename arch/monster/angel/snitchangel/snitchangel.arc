object snitchangel
anim
snitchangel.x11
snitchangel.x13
snitchangel.x12
snitchangel.x13
mina
name snitch angel
race angel
face snitchangel.x11
str 30
dex 20
con 10
wis 35
pow 40
int 30
hp 20486
maxhp 20486
maxsp 768
exp 12500000
dam 105
wc -100
ac -120
speed 1.1
attack_movement 4
level 120
attacktype 1048577
resist_magic 100
resist_blind 80
weight 75000
randomitems snitchangel
run_away 0
pick_up 24
will_apply 2
move_type fly_low
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 0
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

