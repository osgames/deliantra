object whim
name whim
race angel
face wingedhalo.x11
wis 8
int 4
hp 3
maxhp 3
exp 10
dam 2
wc 20
ac 10
speed 0.2
level 1
attacktype 1
resist_magic 100
weight 75000
run_away 18
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
can_see_in_dark 1
end

