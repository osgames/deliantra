object liteangel
anim
liteangel.x11
liteangel.x12
liteangel.x13
liteangel.x14
mina
name light angel
race angel
face liteangel.x12
str 18
con 16
wis 15
pow 40
int 20
hp 500
maxhp 500
maxsp 200
exp 20000
dam 15
wc -10
ac -5
speed 0.25
attack_movement 4
level 13
attacktype 1048577
resist_physical 30
resist_fire 30
resist_cold 30
weight 150000
randomitems liteangel
run_away 18
pick_up 24
will_apply 2
move_type fly_low
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
unaggressive 0
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

