object archon
anim
archon.x11
archon.x12
archon.x13
archon.x14
mina
name Archon
race chaos
face archon.x11
is_animated 1
con 3
wis 15
pow 50
int 30
hp 1500
maxhp 1500
sp 300
maxsp 300
exp 100000
dam 80
wc -30
ac -30
speed -0.6
level 30
attacktype 262180
resist_physical 50
resist_magic 50
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion -100
resist_drain -100
resist_ghosthit 100
resist_poison 100
resist_fear 100
resist_death 100
resist_chaos 100
weight 30000
randomitems liv_chaos
move_type fly_low
alive 1
monster 1
can_cast_spell 1
end

