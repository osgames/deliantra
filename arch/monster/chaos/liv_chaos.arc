object living_chaos
anim
liv_chaos.x11
liv_chaos.x12
liv_chaos.x13
liv_chaos.x14
mina
name living chaos
race chaos
face liv_chaos.x11
is_animated 1
con 3
wis 15
pow 3
int 3
hp 250
maxhp 250
sp 1
maxsp 50
exp 50000
dam 20
wc -5
ac -15
speed -0.6
level 13
attacktype 262144
resist_physical 50
resist_magic 50
resist_fire 50
resist_electricity 50
resist_cold 50
resist_confusion -100
resist_drain -100
resist_weaponmagic -100
resist_ghosthit 100
resist_poison 100
resist_fear 100
resist_death 100
resist_chaos 100
weight 30000
randomitems liv_chaos
move_type fly_low
alive 1
monster 1
can_cast_spell 1
end

