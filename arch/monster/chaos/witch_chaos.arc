object chaos_witch
anim
facings 4
witch_air.x11
witch_air.x12
witch_chaos.x12
witch_chaos.x11
witch_earth.x11
witch_earth.x12
witch_chaos.x12
witch_chaos.x11
witch_water.x11
witch_water.x12
witch_chaos.x12
witch_chaos.x11
witch_fire.x11
witch_fire.x12
witch_chaos.x12
witch_chaos.x11
mina
name chaos witch
race chaos
face witch_chaos.x11
con 10
wis 10
pow 10
int 14
hp 1200
maxhp 1200
sp 50
maxsp 100
exp 10000
dam 30
wc -7
ac -8
speed -0.4
attack_movement 3
level 20
attacktype 262144
resist_physical 50
resist_magic 50
resist_fire 50
resist_electricity 50
resist_cold 50
resist_confusion -100
resist_drain -100
resist_weaponmagic -100
resist_ghosthit 100
resist_poison 100
resist_fear 100
resist_death 100
resist_chaos 100
weight 900
randomitems witch_chaos
move_type fly_low
alive 1
monster 1
can_cast_spell 1
can_use_skill 1
end

