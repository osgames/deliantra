object stonegolem
anim
stonegolem.x11
stonegolem.x12
mina
name stone golem
race unnatural
face stonegolem.x11
int 1
hp 2000
maxhp 2000
exp 2000
dam 200
wc 8
ac 5
speed 0.25
level 25
attacktype 1
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid -100
resist_poison 100
resist_fear 100
resist_blind 100
weight 1000000
randomitems stonegolem
alive 1
monster 1
end

