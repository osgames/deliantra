object sphinx
anim
facings 2
sphinx.x31
sphinx.x71
mina
name sphinx
race unnatural
msg
@match *
I don't know any riddles right now....GO AWAY!!!!
endmsg
face sphinx.x31
con 8
wis 20
pow 2
int 20
hp 300
maxhp 300
maxsp 10
exp 4001
dam 25
wc 2
ac 2
speed -0.07
level 8
attacktype 1
resist_fire 100
resist_fear 100
weight 8000000
randomitems sphinx
run_away 6
alive 1
no_pick 1
monster 1
see_invisible 1
unaggressive 1
can_cast_spell 1
sleep 1
end
more
object sphinx_2
anim
facings 2
sphinx.x31
sphinx.x71
mina
name sphinx
face sphinx.x31
x 1
alive 1
no_pick 1
monster 1
end

