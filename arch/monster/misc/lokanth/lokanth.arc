object lokanth
anim
facings 2
lokanth.x31
lokanth.x32
lokanth.x71
lokanth.x72
mina
race chaos
face lokanth.x31
str 20
dex 13
con 5
wis 10
int 12
hp 400
maxhp 400
sp 50
maxsp 50
exp 500
dam 10
wc -1
ac -4
speed -0.2
level 8
attacktype 1025
resist_physical 50
resist_magic 50
resist_confusion -100
resist_acid 100
resist_weaponmagic -100
resist_poison 100
resist_fear 100
resist_chaos 100
resist_godpower -100
weight 85000
randomitems lokanth
alive 1
no_pick 1
monster 1
can_cast_spell 1
can_use_skill 1
body_skill 1
end

