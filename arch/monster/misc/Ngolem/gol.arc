object golem_necro
anim
gol.x11
gol.x12
mina
name golem of Necromancer
race unnatural
face gol.x11
str 60
dex 25
con 20
wis 20
pow 15
int 15
hp 10000
maxhp 10000
maxsp 30
exp 100000
dam 50
wc -20
ac -40
speed -0.4
level 50
resist_magic 50
resist_electricity 100
resist_fear 100
weight 1500000
randomitems titan
run_away 1
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_rod 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end
more
object golem_necro_2
anim
gol.x11
gol.x12
mina
name golem of Necromancer
face gol.x11
x 1
alive 1
monster 1
end
more
object golem_necro_3
anim
gol.x11
gol.x12
mina
name golem of Necromancer
face gol.x11
y 1
alive 1
monster 1
end
more
object golem_necro_4
anim
gol.x11
gol.x12
mina
name golem of Necromancer
face gol.x11
x 1
y 1
alive 1
monster 1
end
more
object golem_necro_5
anim
gol.x11
gol.x12
mina
name golem of Necromancer
face gol.x11
y 2
alive 1
monster 1
end
more
object golem_necro_6
anim
gol.x11
gol.x12
mina
name golem of Necromancer
face gol.x11
x 1
y 2
alive 1
monster 1
end

