object pixie
anim
pixie.x11
pixie.x12
pixie.x11
pixie.x12
pixie.x11
pixie.x13
pixie.x13
mina
race faerie
face pixie.x11
wis 15
pow 1
int 16
hp 8
maxhp 8
sp 2
maxsp 2
exp 10
dam 1
wc 7
ac 6
speed -0.2
attack_movement 3
level 2
weight 10000
randomitems pixie
run_away 15
pick_up 1
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
body_range 1
can_use_wand 1
end

