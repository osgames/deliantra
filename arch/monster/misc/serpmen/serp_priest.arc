object serp_priest
anim
facings 2
serp_priest.x31
serp_priest.x71
mina
name Serpentman Priest
race reptile
face serp_priest.x31
str 12
dex 20
con 5
pow 24
int 25
hp 100
maxhp 100
sp 50
maxsp 50
exp 700
dam 6
wc 3
ac -1
speed 0.15
level 12
attacktype 1025
resist_physical 30
resist_magic 30
resist_fire 30
resist_electricity 100
resist_cold -100
resist_acid 100
resist_poison 100
resist_chaos 100
weight 75000
randomitems serp_priest
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end

