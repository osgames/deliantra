object serp_man
anim
facings 2
serp_man.x31
serp_man.x32
serp_man.x71
serp_man.x72
mina
name Serpentman
race reptile
face serp_man.x31
str 13
dex 20
con 3
int 12
hp 50
maxhp 50
exp 150
dam 8
wc 1
ac 3
speed -0.15
level 6
resist_fire 30
resist_electricity 30
resist_cold -100
resist_acid 30
resist_poison 30
resist_chaos 30
weight 75000
randomitems serp_man
alive 1
can_use_shield 1
no_pick 1
monster 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end

