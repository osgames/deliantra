object generate_serp_man
name generator
race reptile
other_arch serp_man
face serpman_gen.x11
hp 100
maxhp 100
sp 1
maxsp 1
exp 250
ac 10
speed -0.05
level 1
resist_magic 100
weight 90000
alive 1
generator 1
end

