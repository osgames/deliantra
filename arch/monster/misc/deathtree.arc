object deathtree
anim
deathtree.x11
deathtree.x12
deathtree.x13
deathtree.x14
deathtree.x15
mina
name tree
race faerie
face deathtree.x11
con 1
int 8
hp 500
maxhp 500
exp 1000
dam 3
wc -1
speed -0.2
level 13
resist_magic 100
weight 1000000
alive 1
no_pick 1
monster 1
stand_still 1
end

