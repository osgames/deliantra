object broo
anim
broo.x11
broo.x12
broo.x13
broo.x12
mina
face broo.x11
str 15
dex 12
con 15
int 10
hp 150
maxhp 150
exp 400
dam 12
wc 3
ac 3
speed -0.21
level 7
attacktype 1025
resist_confusion -100
resist_poison 100
resist_fear 100
resist_chaos 100
weight 75000
randomitems broo
alive 1
can_use_shield 1
no_pick 1
monster 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

