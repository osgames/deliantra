object c_minor_priest
anim
facings 2
c_priest.x31
c_priest.x32
c_priest.x71
c_priest.x72
mina
name Minor Chaos Priest
face c_priest.x31
str 10
dex 10
con 10
wis 10
pow 5
int 20
hp 100
maxhp 100
sp 40
maxsp 40
exp 1500
dam 8
wc 1
ac -1
speed 0.15
attack_movement 7
level 8
resist_magic 50
resist_confusion -100
resist_weaponmagic -100
resist_chaos 100
resist_godpower -100
weight 75000
randomitems c_m_priest
alive 1
can_use_shield 1
no_pick 1
monster 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

object c_priest
anim
facings 2
c_priest.x31
c_priest.x32
c_priest.x71
c_priest.x72
mina
name Chaos Priest
face c_priest.x31
str 13
dex 13
con 10
wis 10
pow 25
int 23
hp 400
maxhp 400
sp 200
maxsp 200
exp 5000
dam 16
wc -1
ac -5
speed 0.2
attack_movement 7
level 12
attacktype 1025
resist_physical 30
resist_magic 30
resist_confusion -100
resist_weaponmagic -100
resist_fear 100
resist_chaos 100
resist_godpower -100
weight 75000
randomitems c_priest
alive 1
can_use_shield 1
no_pick 1
monster 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

