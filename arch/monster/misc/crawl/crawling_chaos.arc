object crawling_chaos
anim
crawling_chaos.x11
crawling_chaos.x12
crawling_chaos.x13
mina
name crawling chaos
face crawling_chaos.x11
con 15
wis 5
int 14
hp 600
maxhp 600
exp 2500
dam 30
wc -5
ac 3
speed -0.25
level 10
attacktype 1089
resist_electricity -100
resist_confusion -100
resist_poison 100
resist_fear 100
resist_chaos 100
resist_blind 100
weight 750000
alive 1
no_pick 1
monster 1
end

