object mudman
anim
mudman.x11
mudman.x12
mina
name Mudman
race earth_elemental
face mudman.x11
str 12
dex 15
con 20
wis 15
pow 20
int 10
hp 1000
maxhp 1000
sp 150
maxsp 150
exp 50000
dam 20
wc -10
ac -4
speed 0.1
level 18
attacktype 7169
resist_cold 50
resist_acid 50
resist_poison 100
resist_slow 100
resist_paralyze 100
weight 100000
randomitems skill_use_magic_item
run_away 10
alive 1
can_use_shield 1
no_pick 1
monster 1
can_cast_spell 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

