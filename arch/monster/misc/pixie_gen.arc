object generate_pixie
name generator
race faerie
other_arch pixie
face pixie_gen.x11
hp 60
maxhp 60
maxsp 1
exp 20
ac 10
speed -0.003
level 1
weight 1000
alive 1
no_pick 1
generator 1
end

