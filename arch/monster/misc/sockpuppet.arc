object generate_sockpuppet
name old sock dryer
race earth_elemental
other_arch sockpuppet
face sockpuppet_gen.x11
hp 600
maxhp 600
maxsp 1
exp 2000
ac -10
speed -0.5
level 1
weight 1000
alive 1
no_pick 1
generator 1
end

object sockpuppet
name Sock Puppet
race earth_elemental
face sockpuppet.x11
str 1
dex 1
con 1
wis 1
pow 1
int 1
hp 1
maxhp 1
exp -1
dam 0
wc 25
ac 120
speed 4
level 1
attacktype 0
resist_fire 100
resist_cold 100
resist_acid 100
resist_poison 100
resist_slow 100
resist_paralyze 100
weight 300
alive 1
no_pick 1
monster 1
end

