object generate_stalker
name generator
race faerie
other_arch stalker
face stalkergen.x11
hp 150
maxhp 150
maxsp 1
exp 400
ac 1
speed -0.002
level 5
move_type fly_low
alive 1
no_pick 1
generator 1
end

