object big_hydra
anim
facings 2
hydra.x71
hydra.x31
mina
name hydra
race hydra
face hydra.x71
con 100
wis 20
pow 26
int 20
hp 9000
maxhp 9000
exp 90000
dam 170
wc -20
ac -12
speed -0.4
level 50
resist_fire -100
resist_confusion -100
resist_fear 100
resist_blind 100
weight 900000
randomitems hydra
run_away 3
alive 1
no_pick 1
monster 1
see_invisible 1
sleep 1
end
more
object big_hydra_2
anim
facings 2
hydra.x71
hydra.x31
mina
name hydra
face hydra.x71
x 1
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_hydra_3
anim
facings 2
hydra.x71
hydra.x31
mina
name hydra
face hydra.x71
x 2
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_hydra_4
anim
facings 2
hydra.x71
hydra.x31
mina
name hydra
face hydra.x71
x 3
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_hydra_5
anim
facings 2
hydra.x71
hydra.x31
mina
name hydra
face hydra.x71
y 1
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_hydra_6
anim
facings 2
hydra.x71
hydra.x31
mina
name hydra
face hydra.x71
x 1
y 1
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_hydra_7
anim
facings 2
hydra.x71
hydra.x31
mina
name hydra
face hydra.x71
x 2
y 1
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_hydra_8
anim
facings 2
hydra.x71
hydra.x31
mina
name hydra
face hydra.x71
x 3
y 1
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_hydra_9
anim
facings 2
hydra.x71
hydra.x31
mina
name hydra
face hydra.x71
y 2
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_hydra_10
anim
facings 2
hydra.x71
hydra.x31
mina
name hydra
face hydra.x71
x 1
y 2
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_hydra_11
anim
facings 2
hydra.x71
hydra.x31
mina
name hydra
face hydra.x71
x 2
y 2
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_hydra_12
anim
facings 2
hydra.x71
hydra.x31
mina
name hydra
face hydra.x71
x 3
y 2
weight 900000
alive 1
no_pick 1
monster 1
end

