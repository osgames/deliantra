object big_blackhydra
anim
facings 2
blackhydra.x71
blackhydra.x31
mina
name black hydra
race hydra
face blackhydra.x71
con 100
wis 20
pow 26
int 20
hp 32000
maxhp 32000
exp 120000
dam 666
wc -116
ac -96
speed -0.5
level 116
resist_fire 100
resist_confusion -100
resist_fear 100
resist_blind 100
weight 900000
randomitems hydra
run_away 3
alive 1
no_pick 1
monster 1
see_invisible 1
sleep 1
end
more
object big_blackhydra_2
anim
facings 2
blackhydra.x71
blackhydra.x31
mina
name black hydra
face blackhydra.x71
x 1
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_blackhydra_3
anim
facings 2
blackhydra.x71
blackhydra.x31
mina
name black hydra
face blackhydra.x71
x 2
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_blackhydra_4
anim
facings 2
blackhydra.x71
blackhydra.x31
mina
name black hydra
face blackhydra.x71
x 3
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_blackhydra_5
anim
facings 2
blackhydra.x71
blackhydra.x31
mina
name black hydra
face blackhydra.x71
y 1
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_blackhydra_6
anim
facings 2
blackhydra.x71
blackhydra.x31
mina
name black hydra
face blackhydra.x71
x 1
y 1
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_blackhydra_7
anim
facings 2
blackhydra.x71
blackhydra.x31
mina
name black hydra
face blackhydra.x71
x 2
y 1
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_blackhydra_8
anim
facings 2
blackhydra.x71
blackhydra.x31
mina
name black hydra
face blackhydra.x71
x 3
y 1
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_blackhydra_9
anim
facings 2
blackhydra.x71
blackhydra.x31
mina
name black hydra
face blackhydra.x71
y 2
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_blackhydra_10
anim
facings 2
blackhydra.x71
blackhydra.x31
mina
name black hydra
face blackhydra.x71
x 1
y 2
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_blackhydra_11
anim
facings 2
blackhydra.x71
blackhydra.x31
mina
name black hydra
face blackhydra.x71
x 2
y 2
weight 900000
alive 1
no_pick 1
monster 1
end
more
object big_blackhydra_12
anim
facings 2
blackhydra.x71
blackhydra.x31
mina
name black hydra
face blackhydra.x71
x 3
y 2
weight 900000
alive 1
no_pick 1
monster 1
end

