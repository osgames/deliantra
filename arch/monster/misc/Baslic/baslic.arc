object baslic
anim
facings 2
baslic.x31
baslic.x32
baslic.x71
baslic.x72
mina
name basilisk
race unnatural
face baslic.x31
str 10
con 5
wis 20
pow 5
int 8
hp 1200
maxhp 1200
maxsp 20
exp 40000
dam 10
wc -15
ac -8
speed -0.2
level 19
attacktype 4194305
resist_physical 40
resist_magic 50
resist_fire -100
resist_cold 100
resist_fear 100
resist_blind 100
weight 2000000
randomitems baslic
run_away 4
alive 1
no_pick 1
monster 1
see_invisible 1
sleep 1
end
more
object baslic_2
anim
facings 2
baslic.x31
baslic.x32
baslic.x71
baslic.x72
mina
name baslic
face baslic.x31
x 1
alive 1
end
more
object baslic_3
anim
facings 2
baslic.x31
baslic.x32
baslic.x71
baslic.x72
mina
name baslic
face baslic.x31
x 2
alive 1
end
more
object baslic_4
anim
facings 2
baslic.x31
baslic.x32
baslic.x71
baslic.x72
mina
name baslic
face baslic.x31
y 1
alive 1
end
more
object baslic_5
anim
facings 2
baslic.x31
baslic.x32
baslic.x71
baslic.x72
mina
name baslic
face baslic.x31
x 1
y 1
alive 1
end
more
object baslic_6
anim
facings 2
baslic.x31
baslic.x32
baslic.x71
baslic.x72
mina
name baslic
face baslic.x31
x 2
y 1
alive 1
end

