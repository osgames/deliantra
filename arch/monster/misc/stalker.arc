object stalker
anim
stalker.x12
stalker.x11
stalker.x12
stalker.x13
stalker.x12
stalker.x11
stalker.x12
stalker.x13
stalker.x13
stalker.x13
mina
race faerie
face stalker.x11
con 1
wis 15
int 2
hp 75
maxhp 75
exp 250
dam 5
wc 10
ac -1
speed -0.4
attack_movement 3
level 8
resist_electricity 50
run_away 25
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
sleep 1
end

