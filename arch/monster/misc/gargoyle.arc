object gargolye
anim
gargoyle.x11
gargoyle.x12
gargoyle.x11
gargoyle.x12
gargoyle.x11
mina
face gargoyle.x11
str 30
con 10
wis 15
int 10
hp 300
maxhp 300
exp 1000
dam 20
wc -7
ac -6
speed -0.1
level 10
resist_physical 50
resist_magic 100
resist_acid 100
resist_poison 100
weight 100000
randomitems gargoyle
move_type fly_low
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
body_arm 2
body_torso 1
body_head 1
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
end

