object sandy
anim
sandy.x12
sandy.x13
sandy.x14
sandy.x15
sandy.x14
sandy.x13
sandy.x12
mina
name sandy
race faerie
face sandy.x11
int 0
hp 280
maxhp 280
exp 500
dam 50
wc 8
ac 5
speed -0.08
level 8
attacktype 16
resist_fire -100
resist_cold 100
weight 100000
alive 1
monster 1
end

