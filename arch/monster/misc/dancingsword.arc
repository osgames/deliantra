object dancingsword
anim
dancingsword.x11
dancingsword.x12
dancingsword.x11
dancingsword.x13
mina
name dancing sword
face dancingsword.x11
str 15
dex 20
con 1
wis 3
pow 0
cha 0
int 3
hp 100
maxhp 100
exp 10000
dam 20
wc -5
ac -5
speed -0.3
level 15
attacktype 256
resist_physical 25
resist_fire 30
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_drain 100
resist_ghosthit 100
resist_poison 100
resist_paralyze 100
resist_fear 100
resist_death 100
resist_blind 100
weight 15000
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
end

