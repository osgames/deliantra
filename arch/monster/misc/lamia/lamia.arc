object lamia
anim
facings 2
lamia.x31
lamia.x32
lamia.x71
lamia.x72
mina
race faerie
face lamia.x31
str 28
dex 30
con 36
wis 25
pow 19
int 30
hp 3000
maxhp 3000
sp 50
maxsp 50
exp 100000
dam 20
wc -10
ac -10
speed -0.30
level 30
attacktype 65
resist_physical 50
resist_magic 50
resist_acid 100
resist_poison 100
resist_fear 100
resist_chaos 100
weight 100000
randomitems lamia
run_away 5
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_use_rod 1
can_use_horn 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end
more
object lamia_2
anim
facings 2
lamia.x31
lamia.x32
lamia.x71
lamia.x72
mina
face lamia.x31
x 1
weight 100000
alive 1
no_pick 1
monster 1
end

