object ent
anim
ent.x11
ent.x11
ent.x11
ent.x12
ent.x12
ent.x13
ent.x13
ent.x12
mina
race faerie
msg
@match *
Hey! Careful of my roots, stranger.
endmsg
face ent.x11
con 10
wis 15
int 10
hp 1500
maxhp 1500
exp 16000
dam 25
wc -1
ac -1
speed -0.3
level 12
resist_fire -50
resist_electricity 50
resist_cold 50
weight 3000000
randomitems giant
run_away 3
alive 1
monster 1
unaggressive 1
can_use_weapon 1
sleep 1
can_see_in_dark 1
body_arm 2
end
more
object ent_2
anim
ent.x11
ent.x11
ent.x11
ent.x12
ent.x12
ent.x13
ent.x13
ent.x12
mina
name ent
face ent.x11
x 1
weight 3000000
alive 1
monster 1
end
more
object ent_3
anim
ent.x11
ent.x11
ent.x11
ent.x12
ent.x12
ent.x13
ent.x13
ent.x12
mina
name ent
face ent.x11
y 1
weight 3000000
alive 1
monster 1
end
more
object ent_4
anim
ent.x11
ent.x11
ent.x11
ent.x12
ent.x12
ent.x13
ent.x13
ent.x12
mina
name ent
face ent.x11
x 1
y 1
weight 3000000
alive 1
monster 1
end

