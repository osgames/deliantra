object papergolem
anim
papergolem.x11
papergolem.x12
mina
name paper golem
race unnatural
face papergolem.x11
int 1
hp 1000
maxhp 1000
exp 1000
dam 100
wc 8
ac 5
speed 0.5
level 20
attacktype 1
resist_fire -100
resist_confusion 100
resist_poison 100
resist_fear 100
resist_blind 100
weight 4000
randomitems papergolem
alive 1
monster 1
end

