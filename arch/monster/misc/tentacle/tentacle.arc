object tentacle
anim
blank.x11
blank.x11
blank.x11
tentacle.x11
tentacle.x12
tentacle.x13
tentacle.x12
tentacle.x11
mina
name leathery tentacle
race animal
face tentacle.x13
con 3
wis 3
int 0
hp 120
maxhp 120
exp 400
dam 20
wc 1
ac 1
speed -0.12
attack_movement 5
level 10
attacktype 17
resist_physical 25
resist_cold 40
resist_acid 30
resist_poison 100
resist_blind 90
weight 300000
randomitems tentacle
run_away 20
pick_up 64
alive 1
monster 1
see_invisible 1
sleep 1
can_see_in_dark 1
anim_speed 6
end

