object tentacle2
anim
tentacle2.x11
tentacle2.x12
tentacle2.x13
tentacle2.x12
tentacle2.x11
blank.x11
blank.x11
blank.x11
mina
name spotted tentacle
race animal
face tentacle2.x13
con 4
wis 3
int 0
hp 200
maxhp 200
exp 1000
dam 25
wc -1
ac 0
speed -0.17
attack_movement 5
level 16
attacktype 1093
resist_physical 20
resist_fire 20
resist_electricity -10
resist_cold 5
resist_acid 90
resist_poison 100
resist_blind 90
weight 300000
randomitems tentacle
pick_up 64
alive 1
monster 1
see_invisible 1
sleep 1
can_see_in_dark 1
anim_speed 5
end

