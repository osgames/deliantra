object c_knight
anim
c_knight.x11
c_knight.x12
mina
name Chaos Knight
face c_knight.x11
str 25
dex 20
con 10
wis 10
int 17
hp 200
maxhp 200
exp 500
dam 12
wc -1
ac -7
speed 0.15
level 10
attacktype 1025
resist_physical 30
resist_magic 50
resist_fear 100
resist_chaos 100
weight 75000
randomitems c_knight
alive 1
can_use_shield 1
no_pick 1
monster 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
can_use_skill 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

