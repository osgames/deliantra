object earth_elemental
anim
facings 8
elem_earth.x11
elem_earth.x12
elem_earth.x13
elem_earth.x14
elem_earth.x15
elem_earth.x16
elem_earth.x17
elem_earth.x18
mina
name earth elemental
race earth_elemental
face elem_earth.x11
int 4
hp 280
maxhp 280
exp 250
dam 50
wc 8
ac 5
speed -0.08
level 8
attacktype 1
resist_physical 70
resist_fire -60
resist_cold 50
resist_blind 100
weight 100000
alive 1
monster 1
end

