object fire_elemental
anim
elem_fire.x11
elem_fire.x12
mina
name fire elemental
race fire_elemental
face elem_fire.x11
int 4
hp 200
maxhp 200
exp 250
dam 20
wc 10
ac 2
speed -0.25
level 8
attacktype 4
resist_fire 100
resist_confusion -100
resist_blind 100
weight 200
glow_radius 1
move_type fly_low
alive 1
monster 1
end

