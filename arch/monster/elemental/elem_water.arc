object water_elemental
anim
elem_water.x11
elem_water.x12
mina
name water elemental
race water_elemental
face elem_water.x11
int 8
hp 140
maxhp 140
exp 250
dam 40
wc 8
ac 4
speed -0.1
level 8
attacktype 17
resist_fire -50
resist_cold 50
resist_confusion 100
resist_blind 100
weight 50000
alive 1
monster 1
end

