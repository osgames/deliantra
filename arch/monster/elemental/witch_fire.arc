object fire_witch
anim
witch_fire.x11
witch_fire.x12
mina
name fire witch
race fire_elemental
face witch_fire.x11
con 2
wis 10
pow 5
int 14
hp 600
maxhp 600
sp 50
maxsp 70
exp 5000
dam 30
wc 1
ac -2
speed -0.4
attack_movement 4
level 14
attacktype 4
resist_physical 100
resist_magic 25
resist_fire 100
resist_cold -100
resist_poison 100
weight 20
glow_radius 2
randomitems witch_fire
move_type fly_low
alive 1
monster 1
can_cast_spell 1
can_use_skill 1
end

