object dust_elemental
anim
elem_dust.x11
elem_dust.x12
mina
name dust devil
race air_elemental
face elem_dust.x11
int 8
hp 160
maxhp 160
exp 250
dam 10
wc 8
ac 0
speed -0.3
level 8
attacktype 8
resist_physical -100
resist_electricity 100
resist_blind 100
weight 200
move_type fly_low
alive 1
monster 1
end

