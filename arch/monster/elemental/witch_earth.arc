object earth_witch
anim
witch_earth.x11
witch_earth.x12
mina
name earth witch
race earth_elemental
face witch_earth.x11
con 2
wis 10
pow 5
int 14
hp 1120
maxhp 1120
sp 30
maxsp 50
exp 4000
dam 40
wc 10
ac -2
speed -0.1
attack_movement 3
level 15
attacktype 1
resist_physical 50
resist_magic 25
resist_fire -20
resist_cold 50
resist_poison 100
weight 5200
randomitems witch_earth
alive 1
monster 1
can_cast_spell 1
can_use_skill 1
end

