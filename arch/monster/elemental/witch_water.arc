object water_witch
anim
witch_water.x11
witch_water.x12
mina
name water witch
race water_elemental
face witch_water.x11
con 2
wis 10
pow 5
int 12
hp 520
maxhp 520
sp 40
maxsp 60
exp 5000
dam 30
wc -1
ac -6
speed -0.2
attack_movement 3
level 14
attacktype 17
resist_physical 90
resist_magic 25
resist_fire -100
resist_cold 100
resist_poison 100
weight 1200
randomitems witch_water
alive 1
monster 1
can_cast_spell 1
can_use_skill 1
end

