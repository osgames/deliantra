object air_elemental
anim
elem_air.x11
elem_air.x12
mina
name air elemental
race air_elemental
face elem_air.x11
int 10
hp 160
maxhp 160
exp 250
dam 10
wc 8
ac 0
speed -0.3
level 8
attacktype 8
resist_physical -100
resist_electricity 100
resist_blind 100
weight 200
move_type fly_low
alive 1
monster 1
end

