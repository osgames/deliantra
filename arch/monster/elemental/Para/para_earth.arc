object para_earth
anim
para_earth.x11
para_earth.x12
para_earth.x13
para_earth.x12
mina
name earth para-elemental
race earth_elemental
face para_earth.x11
int 8
hp 200
maxhp 200
exp 210
dam 20
wc 9
ac 5
speed -0.08
level 6
attacktype 16
resist_cold 100
weight 100000
randomitems para_elemental
alive 1
monster 1
end

