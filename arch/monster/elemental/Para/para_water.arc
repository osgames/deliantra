object para_water
anim
para_water.x11
para_water.x12
para_water.x13
mina
name water para-elemental
race water_elemental
face para_water.x11
int 8
hp 120
maxhp 120
exp 200
dam 30
wc 9
ac 6
speed -0.1
level 6
attacktype 1
resist_cold -100
resist_confusion 100
weight 50000
randomitems para_elemental
alive 1
monster 1
end

