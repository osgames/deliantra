object para_lava
anim
para_lava.x11
para_lava.x12
para_lava.x13
para_lava.x12
mina
name lava para-elemental
race fire_elemental
face para_lava.x11
int 8
hp 150
maxhp 150
exp 200
dam 20
wc 16
ac 5
speed -0.25
level 6
attacktype 4
resist_fire 100
resist_cold -100
weight 50000
randomitems para_elemental
move_type fly_low
alive 1
monster 1
end

