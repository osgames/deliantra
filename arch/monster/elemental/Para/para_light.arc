object para_lightning
anim
para_light.x11
para_light.x12
para_light.x13
mina
name lightning para-elemental
race air_elemental
face para_light.x11
int 8
hp 120
maxhp 120
exp 200
dam 10
wc 9
ac 3
speed -0.3
level 6
attacktype 8
resist_fire 100
resist_electricity 100
weight 200
randomitems para_elemental
move_type fly_low
alive 1
monster 1
end

