object para_ice
anim
para_ice.x11
para_ice.x12
para_ice.x13
para_ice.x12
mina
name ice para-elemental
race water_elemental
face para_ice.x11
int 8
hp 120
maxhp 120
exp 200
dam 30
wc 9
ac 8
speed -0.1
level 6
attacktype 16
resist_fire -100
resist_cold 100
resist_confusion 100
weight 50000
randomitems para_elemental
alive 1
monster 1
end

