object para_fire
anim
para_fire.x11
para_fire.x12
mina
name fire para-elemental
race fire_elemental
face para_fire.x11
int 8
hp 150
maxhp 150
exp 200
dam 20
wc 16
ac 5
speed -0.25
level 6
attacktype 4
resist_fire 100
resist_confusion -100
weight 200
randomitems para_elemental
move_type fly_low
alive 1
monster 1
end

