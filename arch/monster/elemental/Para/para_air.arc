object para_air
anim
para_air.x11
para_air.x12
para_air.x13
para_air.x14
mina
name air para-elemental
race air_elemental
face para_air.x11
int 8
hp 100
maxhp 100
exp 200
dam 10
wc 10
ac 3
speed -0.3
level 6
attacktype 8
resist_electricity 100
weight 200
randomitems para_elemental
move_type fly_low
alive 1
monster 1
end

