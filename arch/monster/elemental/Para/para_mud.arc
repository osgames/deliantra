object para_mud
anim
para_mud.x11
para_mud.x12
para_mud.x13
para_mud.x14
para_mud.x15
para_mud.x14
para_mud.x13
para_mud.x12
mina
name mud para-elemental
race earth_elemental
face para_mud.x11
int 8
hp 150
maxhp 150
exp 200
dam 30
wc 9
ac 8
speed -0.08
level 6
attacktype 1
resist_fire -100
resist_cold 100
weight 100000
randomitems para_elemental
alive 1
monster 1
end

