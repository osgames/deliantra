object shadow
anim
shadow.x11
shadow.x12
mina
name shadow
race shadow
face shadow.x11
con 20
wis 20
pow 5
int 10
hp 900
maxhp 900
sp 20
maxsp 30
exp 30000
dam 13
wc -15
ac -20
speed -0.4
attack_movement 3
level 30
attacktype 32912
resist_physical 50
resist_magic 25
resist_fire 50
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
resist_poison 100
resist_fear 100
resist_deplete 100
resist_blind 100
weight 1000
randomitems shadow
run_away 5
alive 1
monster 1
can_cast_spell 1
sleep 1
can_use_skill 1
can_see_in_dark 1
end

