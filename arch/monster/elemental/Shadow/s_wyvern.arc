object s_wyvern
anim
facings 2
s_wyvern.x31
s_wyvern.x32
s_wyvern.x71
s_wyvern.x72
mina
name shadow wyvern
race shadow
face s_wyvern.x71
con 25
wis 20
pow 5
int 16
hp 1500
maxhp 1500
sp 30
maxsp 50
exp 50000
dam 33
wc -17
ac -25
speed -0.3
attack_movement 3
level 40
attacktype 32912
resist_physical 50
resist_magic 25
resist_fire 50
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
resist_poison 100
resist_fear 100
resist_deplete 100
resist_blind 100
weight 1000
randomitems s_wyvern
run_away 5
alive 1
monster 1
can_cast_spell 1
sleep 1
can_use_skill 1
can_see_in_dark 1
end
more
object s_wyvern_2
anim
facings 2
s_wyvern.x31
s_wyvern.x32
s_wyvern.x71
s_wyvern.x72
mina
name shadow wyvern
face s_wyvern.x71
x 1
weight 1000
alive 1
no_pick 1
monster 1
end

