object s_mastif
anim
facings 2
s_mastif.x31
s_mastif.x32
s_mastif.x33
s_mastif.x32
s_mastif.x71
s_mastif.x72
s_mastif.x73
s_mastif.x72
mina
name shadow mastif
race shadow
face s_mastif.x31
con 20
wis 20
int 10
hp 600
maxhp 600
exp 20000
dam 13
wc -15
ac -20
speed -0.4
attack_movement 3
level 25
attacktype 32912
resist_physical 50
resist_magic 25
resist_fire 50
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
resist_poison 100
resist_fear 100
resist_deplete 100
resist_blind 100
weight 1000
run_away 5
alive 1
monster 1
sleep 1
can_see_in_dark 1
end

