object air_witch
anim
witch_air.x11
witch_air.x12
mina
name air witch
race air_elemental
face witch_air.x11
con 2
wis 10
pow 5
int 14
hp 200
maxhp 200
sp 50
maxsp 70
exp 6000
dam 15
wc 10
ac -11
speed -0.6
attack_movement 1
level 15
attacktype 9
resist_physical 100
resist_magic 25
resist_electricity 50
resist_cold -75
resist_poison 100
weight 50
randomitems witch_air
move_type fly_low
alive 1
monster 1
can_cast_spell 1
can_use_skill 1
end

