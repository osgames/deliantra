object armour_troll2
anim
armourtroll2.x11
armourtroll2.x12
mina
name small armoured troll
race troll
sound_destroy wn/orc-die-2
face armourtroll2.x11
str 20
dex 18
con 5
wis 10
int 5
hp 180
maxhp 180
exp 550
dam 12
wc 4
ac -10
speed 0.12
level 7
resist_fire -100
weight 100000
randomitems troll
run_away 5
pick_up 8
will_apply 2
alive 1
no_pick 1
monster 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_see_in_dark 1
body_arm 2
body_skill 1
body_finger 2
end

object armour_troll3
name small armoured troll
race troll
face armourtroll3.x11
str 21
dex 18
con 5
wis 10
int 5
hp 180
maxhp 180
exp 550
dam 14
wc 4
ac -15
speed 0.12
level 7
resist_fire -100
weight 100000
randomitems troll
run_away 5
pick_up 8
will_apply 2
alive 1
no_pick 1
monster 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_see_in_dark 1
body_arm 2
body_skill 1
body_finger 2
end

object armour_troll4
name small armoured troll
race troll
face armourtroll4.x11
str 22
dex 18
con 5
wis 10
int 5
hp 180
maxhp 180
exp 550
dam 16
wc 4
ac -15
speed 0.12
level 7
resist_fire -100
weight 100000
randomitems troll
run_away 5
pick_up 8
will_apply 2
alive 1
no_pick 1
monster 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_see_in_dark 1
body_arm 2
body_skill 1
body_finger 2
end

