object gaelotroll
anim
facings 2
gaelotroll.x31
gaelotroll.x32
gaelotroll.x33
gaelotroll.x32
gaelotroll.x31
gaelotroll.x32
gaelotroll.x33
gaelotroll.x32
gaelotroll.x71
gaelotroll.x72
gaelotroll.x73
gaelotroll.x72
gaelotroll.x71
gaelotroll.x72
gaelotroll.x73
gaelotroll.x72
mina
race troll
sound_destroy wn/troll-die
face gaelotroll.x31
str 55
dex 25
con 20
wis 13
int 10
hp 3000
maxhp 3000
exp 70000
dam 25
wc -50
ac -15
speed -0.2
level 12
attacktype 65
resist_physical 50
resist_fire -100
resist_fear 50
weight 3000000
randomitems gaelotroll
run_away 3
alive 1
monster 1
can_use_weapon 1
sleep 1
can_use_skill 1
can_see_in_dark 1
body_arm 2
body_skill 1
end
more
object gaelotroll_2
anim
facings 2
gaelotroll.x31
gaelotroll.x32
gaelotroll.x33
gaelotroll.x32
gaelotroll.x31
gaelotroll.x32
gaelotroll.x33
gaelotroll.x32
gaelotroll.x71
gaelotroll.x72
gaelotroll.x73
gaelotroll.x72
gaelotroll.x71
gaelotroll.x72
gaelotroll.x73
gaelotroll.x72
mina
name troll
face gaelotroll.x31
x 1
alive 1
monster 1
end
more
object gaelotroll_3
anim
facings 2
gaelotroll.x31
gaelotroll.x32
gaelotroll.x33
gaelotroll.x32
gaelotroll.x31
gaelotroll.x32
gaelotroll.x33
gaelotroll.x32
gaelotroll.x71
gaelotroll.x72
gaelotroll.x73
gaelotroll.x72
gaelotroll.x71
gaelotroll.x72
gaelotroll.x73
gaelotroll.x72
mina
name troll
face gaelotroll.x31
y 1
alive 1
monster 1
end
more
object gaelotroll_4
anim
facings 2
gaelotroll.x31
gaelotroll.x32
gaelotroll.x33
gaelotroll.x32
gaelotroll.x31
gaelotroll.x32
gaelotroll.x33
gaelotroll.x32
gaelotroll.x71
gaelotroll.x72
gaelotroll.x73
gaelotroll.x72
gaelotroll.x71
gaelotroll.x72
gaelotroll.x73
gaelotroll.x72
mina
name troll
face gaelotroll.x31
x 1
y 1
alive 1
monster 1
end
more
object gaelotroll_5
anim
facings 2
gaelotroll.x31
gaelotroll.x32
gaelotroll.x33
gaelotroll.x32
gaelotroll.x31
gaelotroll.x32
gaelotroll.x33
gaelotroll.x32
gaelotroll.x71
gaelotroll.x72
gaelotroll.x73
gaelotroll.x72
gaelotroll.x71
gaelotroll.x72
gaelotroll.x73
gaelotroll.x72
mina
name troll
face gaelotroll.x31
y 2
alive 1
monster 1
end
more
object gaelotroll_6
anim
facings 2
gaelotroll.x31
gaelotroll.x32
gaelotroll.x33
gaelotroll.x32
gaelotroll.x31
gaelotroll.x32
gaelotroll.x33
gaelotroll.x32
gaelotroll.x71
gaelotroll.x72
gaelotroll.x73
gaelotroll.x72
gaelotroll.x71
gaelotroll.x72
gaelotroll.x73
gaelotroll.x72
mina
name troll
face gaelotroll.x31
x 1
y 2
alive 1
monster 1
end

