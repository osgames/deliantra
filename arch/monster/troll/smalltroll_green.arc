object smallt_green
anim
smalltroll.x11
smalltroll.x12
mina
name small troll
race troll
sound_destroy wn/orc-die-2
face smalltroll.x11
str 20
dex 18
con 5
wis 10
int 5
hp 90
maxhp 90
exp 450
dam 10
wc 4
ac 2
speed 0.12
level 7
resist_fire -100
weight 100000
randomitems troll
run_away 5
pick_up 8
will_apply 2
alive 1
no_pick 1
monster 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_see_in_dark 1
body_arm 2
body_skill 1
body_finger 2
end

