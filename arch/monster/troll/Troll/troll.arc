object troll
anim
troll.x11
troll.x12
troll.x13
troll.x12
mina
race troll
sound_destroy wn/troll-die
face troll.x11
str 40
dex 15
con 10
wis 13
int 8
hp 1000
maxhp 1000
exp 8000
dam 15
wc -3
ac -2
speed -0.2
level 12
resist_fire -100
resist_fear 50
weight 500000
randomitems giant
run_away 3
alive 1
monster 1
can_use_weapon 1
sleep 1
can_use_skill 1
can_see_in_dark 1
body_arm 2
body_skill 1
end
more
object troll_2
anim
troll.x11
troll.x12
troll.x13
troll.x12
mina
name troll
face troll.x11
x 1
alive 1
monster 1
end
more
object troll_3
anim
troll.x11
troll.x12
troll.x13
troll.x12
mina
name troll
face troll.x11
y 1
alive 1
monster 1
end
more
object troll_4
anim
troll.x11
troll.x12
troll.x13
troll.x12
mina
name troll
face troll.x11
x 1
y 1
alive 1
monster 1
end

