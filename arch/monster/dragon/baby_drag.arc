object baby_dragon
anim
baby_drag.x11
baby_drag.x12
baby_drag.x13
mina
name baby dragon
race dragon
face baby_drag.x11
con 8
wis 20
pow 8
int 14
hp 550
maxhp 550
maxsp 40
exp 55000
dam 10
wc -10
ac -8
speed -0.3
level 10
resist_magic 30
resist_fire 100
resist_cold -100
resist_fear 100
resist_blind 50
weight 225000
randomitems dragon
run_away 3
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
end

