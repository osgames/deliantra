object big_dragon_ancient
anim
facings 2
dragon_ac.x71
dragon_ac.x72
dragon_ac.x73
dragon_ac.x74
dragon_ac.x31
dragon_ac.x32
dragon_ac.x33
dragon_ac.x34
mina
name ancient red dragon
race dragon
face dragon_ac.x71
con 11
wis 20
pow 26
int 20
hp 7500
maxhp 7500
sp 60
maxsp 80
exp 200000
dam 40
wc -25
ac -16
speed -0.5
level 26
attacktype 5
resist_physical 40
resist_magic 50
resist_fire 100
resist_cold -50
resist_confusion -100
resist_acid 30
resist_paralyze 80
resist_fear 100
resist_blind 100
weight 400000
randomitems dragon
run_away 3
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
end
more
object big_dragon_ancient2
anim
facings 2
dragon_ac.x71
dragon_ac.x72
dragon_ac.x73
dragon_ac.x74
dragon_ac.x31
dragon_ac.x32
dragon_ac.x33
dragon_ac.x34
mina
name dragon
face dragon_ac.x71
x 1
weight 4000000
alive 1
monster 1
end
more
object big_dragon_ancient3
anim
facings 2
dragon_ac.x71
dragon_ac.x72
dragon_ac.x73
dragon_ac.x74
dragon_ac.x31
dragon_ac.x32
dragon_ac.x33
dragon_ac.x34
mina
name dragon
face dragon_ac.x71
x 2
weight 4000000
alive 1
monster 1
end
more
object big_dragon_ancient4
anim
facings 2
dragon_ac.x71
dragon_ac.x72
dragon_ac.x73
dragon_ac.x74
dragon_ac.x31
dragon_ac.x32
dragon_ac.x33
dragon_ac.x34
mina
name dragon
face dragon_ac.x71
y 1
weight 4000000
alive 1
monster 1
end
more
object big_dragon_ancient5
anim
facings 2
dragon_ac.x71
dragon_ac.x72
dragon_ac.x73
dragon_ac.x74
dragon_ac.x31
dragon_ac.x32
dragon_ac.x33
dragon_ac.x34
mina
name dragon
face dragon_ac.x71
x 1
y 1
weight 4000000
alive 1
monster 1
end
more
object big_dragon_ancient6
anim
facings 2
dragon_ac.x71
dragon_ac.x72
dragon_ac.x73
dragon_ac.x74
dragon_ac.x31
dragon_ac.x32
dragon_ac.x33
dragon_ac.x34
mina
name dragon
face dragon_ac.x71
x 2
y 1
weight 4000000
alive 1
monster 1
end

