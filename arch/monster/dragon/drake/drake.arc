object drake
anim
facings 2
drake.x11
drake.x12
drake.x13
drake.x12
drake.x71
drake.x72
drake.x73
drake.x72
mina
name drake
race dragon
face drake.x11
str 15
con 5
wis 15
int 16
hp 220
maxhp 220
exp 5000
dam 20
wc 5
ac -3
speed 0.42
level 9
attacktype 1025
resist_magic 85
resist_blind 30
weight 70000
randomitems drake
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 0
sleep 1
end

