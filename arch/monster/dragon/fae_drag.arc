object faerie_dragon
anim
facings 2
fae_drag.x71
fae_drag.x72
fae_drag.x31
fae_drag.x32
mina
name faerie dragon
race faerie
face fae_drag.x71
con 1
wis 15
pow 6
int 14
hp 40
maxhp 40
sp 20
maxsp 40
exp 1000
dam 10
wc 7
ac 5
speed -0.07
attack_movement 1
level 8
resist_magic 100
resist_poison 100
resist_paralyze 100
resist_fear 100
resist_blind 60
weight 70000
randomitems wyvern
run_away 20
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
unaggressive 1
can_cast_spell 1
sleep 1
end

