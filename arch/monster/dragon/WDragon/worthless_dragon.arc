object big_dragon_worthless
anim
facings 2
worthless_dragon.x71
worthless_dragon.x72
worthless_dragon.x73
worthless_dragon.x72
worthless_dragon.x31
worthless_dragon.x32
worthless_dragon.x33
worthless_dragon.x32
mina
name worthless dragon
race dragon
face worthless_dragon.x71
con 11
wis 20
pow 26
int 24
hp 3500
maxhp 3500
sp 60
maxsp 60
exp 1
dam 25
wc -20
ac -12
speed -0.4
level 18
resist_magic 30
resist_fire 100
resist_cold -100
resist_confusion -100
resist_fear 100
weight 400000
randomitems dragon
run_away 3
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
end
more
object big_worthless_dragon_2
anim
facings 2
worthless_dragon.x71
worthless_dragon.x72
worthless_dragon.x73
worthless_dragon.x72
worthless_dragon.x31
worthless_dragon.x32
worthless_dragon.x33
worthless_dragon.x32
mina
name worthless_dragon
face worthless_dragon.x71
x 1
weight 4000000
alive 1
monster 1
end
more
object big_worthless_dragon_3
anim
facings 2
worthless_dragon.x71
worthless_dragon.x72
worthless_dragon.x73
worthless_dragon.x72
worthless_dragon.x31
worthless_dragon.x32
worthless_dragon.x33
worthless_dragon.x32
mina
name worthless_dragon
face worthless_dragon.x71
x 2
weight 4000000
alive 1
monster 1
end
more
object big_worthless_dragon_4
anim
facings 2
worthless_dragon.x71
worthless_dragon.x72
worthless_dragon.x73
worthless_dragon.x72
worthless_dragon.x31
worthless_dragon.x32
worthless_dragon.x33
worthless_dragon.x32
mina
name worthless_dragon
face worthless_dragon.x71
y 1
weight 4000000
alive 1
monster 1
end
more
object big_worthless_dragon_5
anim
facings 2
worthless_dragon.x71
worthless_dragon.x72
worthless_dragon.x73
worthless_dragon.x72
worthless_dragon.x31
worthless_dragon.x32
worthless_dragon.x33
worthless_dragon.x32
mina
name worthless_dragon
face worthless_dragon.x71
x 1
y 1
weight 4000000
alive 1
monster 1
end
more
object big_worthless_dragon_6
anim
facings 2
worthless_dragon.x71
worthless_dragon.x72
worthless_dragon.x73
worthless_dragon.x72
worthless_dragon.x31
worthless_dragon.x32
worthless_dragon.x33
worthless_dragon.x32
mina
name worthless_dragon
face worthless_dragon.x71
x 2
y 1
weight 4000000
alive 1
monster 1
end

