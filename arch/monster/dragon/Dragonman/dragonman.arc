object dragonman
anim
facings 2
dragonman.x31
dragonman.x32
dragonman.x33
dragonman.x32
dragonman.x71
dragonman.x72
dragonman.x73
dragonman.x72
mina
name dragonman
race dragon
face dragonman.x31
con 11
wis 20
pow 13
int 18
hp 3500
maxhp 3500
sp 80
maxsp 80
exp 160000
dam 10
wc -10
ac -10
speed -0.4
level 18
resist_physical 30
resist_fire 100
resist_electricity 100
resist_cold -30
resist_confusion 30
resist_acid 100
resist_drain 100
resist_poison 100
resist_slow 100
resist_paralyze 100
resist_fear 100
resist_blind 100
weight 150000
randomitems dragon
run_away 3
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
end
more
object dragonman_2
anim
facings 2
dragonman.x31
dragonman.x32
dragonman.x33
dragonman.x32
dragonman.x71
dragonman.x72
dragonman.x73
dragonman.x72
mina
name dragon man
face dragonman.x31
y 1
weight 4000000
alive 1
monster 1
end

