object chinese_dragon
anim
facings 2
chinese_dr.x71
chinese_dr.x72
chinese_dr.x73
chinese_dr.x72
chinese_dr.x31
chinese_dr.x32
chinese_dr.x33
chinese_dr.x32
mina
name chinese dragon
race dragon
face chinese_dr.x71
con 4
wis 20
pow 32
int 20
hp 1000
maxhp 1000
maxsp 40
exp 40000
dam 15
wc -15
ac -8
speed -0.2
level 14
resist_magic 30
resist_fire -100
resist_cold 100
resist_fear 100
resist_blind 100
weight 200000
randomitems chinese
run_away 4
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
end
more
object chinese_dragon_2
anim
facings 2
chinese_dr.x71
chinese_dr.x72
chinese_dr.x73
chinese_dr.x72
chinese_dr.x31
chinese_dr.x32
chinese_dr.x33
chinese_dr.x32
mina
name chinese dragon
face chinese_dr.x71
x 1
alive 1
end
more
object chinese_dragon_3
anim
facings 2
chinese_dr.x71
chinese_dr.x72
chinese_dr.x73
chinese_dr.x72
chinese_dr.x31
chinese_dr.x32
chinese_dr.x33
chinese_dr.x32
mina
name chinese dragon
face chinese_dr.x71
y 1
alive 1
end
more
object chinese_dragon_4
anim
facings 2
chinese_dr.x71
chinese_dr.x72
chinese_dr.x73
chinese_dr.x72
chinese_dr.x31
chinese_dr.x32
chinese_dr.x33
chinese_dr.x32
mina
name chinese dragon
face chinese_dr.x71
x 1
y 1
alive 1
end

