object Cwyvern
anim
Cwyvern.x71
Cwyvern.x72
Cwyvern.x31
Cwyvern.x32
facings 2
mina
name wyvern of chaos
race dragon
face Cwyvern.x71
con 48
wis 30
pow 76
hp 3000
maxhp 3000
maxsp 300
exp 200000
dam 30
wc -20
ac -10
speed -0.400000
level 30
attacktype 29
resist_physical 100
resist_fire 100
resist_electricity 50
resist_cold 100
resist_paralyze 100
resist_blind 100
weight 150000
randomitems Cwyvern
run_away 6
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
end
more
object Cwyvern_2
anim
facings 2
Cwyvern.x71
Cwyvern.x72
Cwyvern.x31
Cwyvern.x32
mina
name wyvern of chaos
face Cwyvern.x71
x 1
weight 800000
alive 1
no_pick 1
monster 1
end

