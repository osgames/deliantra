object dragon
anim
facings 2
wyvern.x71
wyvern.x72
wyvern.x31
wyvern.x32
mina
name wyvern
race dragon
face wyvern.x71
con 2
wis 20
pow 2
int 14
hp 300
maxhp 300
maxsp 10
exp 4000
dam 25
wc 2
ac 2
speed -0.07
level 8
attacktype 1
resist_fire 100
resist_cold -100
resist_confusion -100
resist_fear 100
resist_blind 50
weight 150000
randomitems wyvern
run_away 6
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
end
more
object dragon_2
anim
facings 2
wyvern.x71
wyvern.x72
wyvern.x31
wyvern.x32
mina
name wyvern
face wyvern.x71
x 1
weight 150000
alive 1
no_pick 1
monster 1
end

