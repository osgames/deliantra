object big_chaos_dragon
anim
facings 2
chaos_dragon.x71
chaos_dragon.x72
chaos_dragon.x73
chaos_dragon.x74
chaos_dragon.x31
chaos_dragon.x32
chaos_dragon.x33
chaos_dragon.x34
mina
name dragon of chaos
race dragon
face chaos_dragon.x71
con 68
wis 30
pow 86
int 24
hp 32000
maxhp 32000
sp 1800
maxsp 1800
exp 450000
dam 30
wc -20
ac -12
speed -0.4
level 68
attacktype 29
resist_physical 100
resist_fire 100
resist_electricity 50
resist_cold 100
resist_paralyze 100
resist_blind 100
weight 400000
randomitems chaosdragon
run_away 3
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
end
more
object big_chaos_dragon.2
anim
facings 2
chaos_dragon.x71
chaos_dragon.x72
chaos_dragon.x73
chaos_dragon.x74
chaos_dragon.x31
chaos_dragon.x32
chaos_dragon.x33
chaos_dragon.x34
mina
name dragon of chaos
face chaos_dragon.x71
x 1
weight 4000000
alive 1
monster 1
end
more
object big_chaos_dragon.3
anim
facings 2
chaos_dragon.x71
chaos_dragon.x72
chaos_dragon.x73
chaos_dragon.x74
chaos_dragon.x31
chaos_dragon.x32
chaos_dragon.x33
chaos_dragon.x34
mina
name dragon of chaos
face chaos_dragon.x71
x 2
weight 4000000
alive 1
monster 1
end
more
object big_chaos_dragon.4
anim
facings 2
chaos_dragon.x71
chaos_dragon.x72
chaos_dragon.x73
chaos_dragon.x74
chaos_dragon.x31
chaos_dragon.x32
chaos_dragon.x33
chaos_dragon.x34
mina
name dragon of chaos
face chaos_dragon.x71
y 1
weight 4000000
alive 1
monster 1
end
more
object big_chaos_dragon.5
anim
facings 2
chaos_dragon.x71
chaos_dragon.x72
chaos_dragon.x73
chaos_dragon.x74
chaos_dragon.x31
chaos_dragon.x32
chaos_dragon.x33
chaos_dragon.x34
mina
name dragon of chaos
face chaos_dragon.x71
x 1
y 1
weight 4000000
alive 1
monster 1
end
more
object big_chaos_dragon.6
anim
facings 2
chaos_dragon.x71
chaos_dragon.x72
chaos_dragon.x73
chaos_dragon.x74
chaos_dragon.x31
chaos_dragon.x32
chaos_dragon.x33
chaos_dragon.x34
mina
name dragon of chaos
face chaos_dragon.x71
x 2
y 1
weight 4000000
alive 1
monster 1
end

