object generate_dragon
name dragoncave
race dragon
other_arch dragon
face dragoncave.x11
hp 300
maxhp 300
maxsp 1
exp 1000
ac 3
speed 0.0008
level 10
resist_fire 100
weight 1000000
alive 1
no_pick 1
generator 1
end

