object bone_drake
anim
bone_drake.x11
bone_drake.x12
mina
name bone drake
race dragon
face bone_drake.x11
con 6
int 20
hp 8000
maxhp 8000
maxsp 40
exp 100000
dam 40
wc -60
ac -20
speed -0.2
level 14
resist_fire 70
resist_electricity 70
resist_cold 70
resist_fear 100
resist_blind 100
weight 200000
randomitems chinese
run_away 4
alive 1
no_pick 1
monster 1
sleep 1
end
more
object bone_drake_2
anim
bone_drake.x11
bone_drake.x12
mina
name bone drake
face bone_drake.x11
x 1
alive 1
end
more
object bone_drake_3
anim
bone_drake.x11
bone_drake.x12
mina
name bone drake
face bone_drake.x11
y 1
alive 1
end
more
object bone_drake_4
anim
bone_drake.x11
bone_drake.x12
mina
name bone drake
face bone_drake.x11
x 1
y 1
alive 1
end

