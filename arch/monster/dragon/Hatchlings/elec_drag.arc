object elec_dragon
anim
facings 2
elec_drag.x71
elec_drag.x72
elec_drag.x31
elec_drag.x32
mina
name electric dragon hatchling
race dragon
face elec_drag.x71
con 3
wis 15
pow 2
int 14
hp 90
maxhp 90
sp 40
maxsp 40
exp 1800
dam 15
wc 7
ac 5
speed -0.07
attack_movement 1
level 8
resist_magic 30
resist_electricity 100
resist_blind 50
weight 70000
randomitems elec_drag
run_away 20
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
end

