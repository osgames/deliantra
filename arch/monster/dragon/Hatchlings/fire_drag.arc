object fire_dragon
anim
facings 2
fire_drag.x71
fire_drag.x72
fire_drag.x31
fire_drag.x32
mina
name dragon hatchling
race dragon
face fire_drag.x71
con 3
wis 15
pow 3
int 14
hp 100
maxhp 100
sp 40
maxsp 40
exp 2000
dam 15
wc 7
ac 5
speed -0.07
attack_movement 1
level 8
resist_magic 30
resist_fire 100
resist_blind 50
weight 70000
randomitems wyvern
run_away 20
move_type fly_low
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
end

