object big_elec
anim
elec_dr.x11
elec_dr.x12
elec_dr.x11
elec_dr.x12
elec_dr.x11
elec_dr.x12
mina
name electric dragon
race dragon
face elec_dr.x11
con 11
wis 20
pow 32
int 18
hp 3500
maxhp 3500
sp 70
maxsp 100
exp 70000
dam 25
wc -20
ac -12
speed -0.4
level 18
resist_fire -100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_fear 100
resist_blind 100
weight 200000
randomitems acdc
run_away 3
alive 1
no_pick 1
monster 1
see_invisible 1
can_cast_spell 1
sleep 1
end
more
object big_elec_2
anim
elec_dr.x11
elec_dr.x12
elec_dr.x11
elec_dr.x12
elec_dr.x11
elec_dr.x12
mina
name electric dragon
face elec_dr.x11
x 1
weight 4000000
alive 1
monster 1
end
more
object big_elec_3
anim
elec_dr.x11
elec_dr.x12
elec_dr.x11
elec_dr.x12
elec_dr.x11
elec_dr.x12
mina
name electric dragon
face elec_dr.x11
y 1
weight 4000000
alive 1
monster 1
end
more
object big_elec_4
anim
elec_dr.x11
elec_dr.x12
elec_dr.x11
elec_dr.x12
elec_dr.x11
elec_dr.x12
mina
name electric dragon
face elec_dr.x11
x 1
y 1
weight 4000000
alive 1
monster 1
end
more
object big_elec_5
anim
elec_dr.x11
elec_dr.x12
elec_dr.x11
elec_dr.x12
elec_dr.x11
elec_dr.x12
mina
name electric dragon
face elec_dr.x11
y 2
weight 4000000
alive 1
monster 1
end
more
object big_elec_6
anim
elec_dr.x11
elec_dr.x12
elec_dr.x11
elec_dr.x12
elec_dr.x11
elec_dr.x12
mina
name electric dragon
face elec_dr.x11
x 1
y 2
weight 4000000
alive 1
monster 1
end

