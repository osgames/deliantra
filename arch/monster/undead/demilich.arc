object demilich
anim
demilich.x11
demilich.x12
demilich.x11
demilich.x13
demilich.x11
mina
inherit class_undead_monster
name demilich
race undead
face demilich.x11
str 10
wis 20
pow 80
int 18
hp 1000
maxhp 1000
sp 100
maxsp 100
exp 90000
dam 20
wc -3
ac -15
speed 0.25
level 31
attacktype 65552
resist_physical 50
resist_magic 100
resist_fire 100
resist_electricity 100
resist_cold 100
resist_confusion 100
resist_acid 100
resist_drain 100
resist_poison 100
resist_slow 100
resist_paralyze 100
resist_fear 100
resist_blind 100
weight 150000
randomitems demilich
alive 1
no_pick 1
monster 1
see_invisible 1
undead 1
unaggressive 1
can_cast_spell 1
anim_speed 5
random_movement 1
end

object doubledemilich
inherit demilich
name doubledemilich
con 4
pow 20
int 20
hp 2000
maxhp 2000
sp 200
maxsp 200
exp 270000
dam 60
wc -20
ac -30
x 15
y 63
speed 0.75
attacktype 70680
unaggressive 0
end

