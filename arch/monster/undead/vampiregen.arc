object generate_vampire
name sarcophagus
race undead
other_arch vampire
face vampiregen.x11
hp 400
maxhp 400
sp 127
maxsp 1
exp 200
ac 0
speed 0.001
level 1
resist_cold 100
weight 2000000
alive 1
generator 1
undead 1
end

