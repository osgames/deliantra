object generate_zombie
name generator
race undead
other_arch zombie
face zombie_gen.x11
hp 100
maxhp 100
maxsp 1
exp 50
ac 3
speed 0.01
level 1
resist_cold 30
weight 750000
randomitems grave
alive 1
no_pick 1
generator 1
undead 1
end

