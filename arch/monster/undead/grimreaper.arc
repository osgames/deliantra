object grimreaper
anim
grimreaper.x11
grimreaper.x12
grimreaper.x13
grimreaper.x14
mina
inherit class_undead_monster
race undead
face grimreaper.x11
con 1
wis 15
int 8
hp 50
maxhp 50
exp 800
dam 10
wc 1
ac 10
speed 0.3
level 4
type 28
attacktype 128
resist_physical 100
resist_magic -100
resist_cold 50
resist_drain 100
resist_fear 100
resist_blind 70
weight 50000
randomitems grimreaper
run_away 15
move_type fly_low
alive 1
no_pick 1
monster 1
undead 1
sleep 1
can_see_in_dark 1
end

