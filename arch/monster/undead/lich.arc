object lich
anim
lich.x11
lich.x12
lich.x13
mina
inherit class_undead_monster
name Lich
race undead
face lich.x11
wis 17
pow 43
int 26
hp 1000
maxhp 1000
maxsp 40
exp 40000
dam 20
wc -3
ac -3
speed 0.15
level 15
attacktype 4225
resist_physical 50
resist_magic 50
resist_blind 90
randomitems lich
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
see_invisible 1
undead 1
can_cast_spell 1
can_use_scroll 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

