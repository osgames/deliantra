object skeleton
anim
skeleton.x11
skeleton.x12
mina
inherit class_undead_monster
race undead
face skeleton.x11
con 2
wis 14
int 6
hp 45
maxhp 45
exp 80
dam 6
wc 15
ac 4
speed -0.2
level 6
attacktype 17
resist_fire -100
resist_cold 30
resist_fear 100
weight 15000
randomitems skeleton
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
undead 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_see_in_dark 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
end

