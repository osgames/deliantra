object banshee
anim
banshee.x11
banshee.x12
mina
inherit class_undead_monster
name Banshee
race undead
face banshee.x11
wis 17
pow 35
int 16
hp 500
maxhp 500
maxsp 60
exp 50000
dam 15
wc 5
ac -5
speed 0.12
level 10
attacktype 17
resist_physical 50
resist_magic 50
resist_blind 90
invisible 1
randomitems banshee
pick_up 24
will_apply 2
alive 1
no_pick 1
monster 1
see_invisible 1
undead 1
can_cast_spell 1
sleep 1
body_range 1
body_finger 2
end

