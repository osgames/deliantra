object ghost
anim
ghost.x11
ghost.x12
mina
inherit class_undead_monster
race undead
face ghost.x11
wis 10
int 2
hp 15
maxhp 15
exp 40
dam 14
wc 2
ac 10
speed 0.14
level 3
attacktype 528
resist_cold 50
resist_fear 100
resist_blind 30
weight 500
randomitems ghost
move_type fly_low
alive 1
no_pick 1
monster 1
undead 1
sleep 1
can_see_in_dark 1
one_hit 1
end

