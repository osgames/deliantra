object nightmare
anim
nightmare.x11
nightmare.x12
mina
inherit class_undead_monster
name nightmare
race undead
face nightmare.x11
con 1
wis 15
int 1
hp 50
maxhp 50
exp 800
dam 10
wc 1
ac 10
speed 0.3
level 4
attacktype 66048
resist_physical 100
resist_magic -100
resist_confusion 50
resist_drain 100
resist_turn_undead -100
resist_fear 100
resist_blind 100
weight 5000
run_away 15
move_type fly_low
alive 1
no_pick 1
monster 1
undead 1
sleep 1
can_see_in_dark 1
one_hit 1
end

