object ghast
anim
ghast.x11
ghast.x12
ghast.x13
mina
inherit class_undead_monster
race undead
face ghast.x11
con 5
wis 14
int 1
hp 100
maxhp 100
exp 100
dam 4
wc 14
ac 2
speed 0.15
level 8
attacktype 4095
resist_fire 50
resist_cold 50
resist_fear 100
resist_blind 60
weight 5000
randomitems standard
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
undead 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_see_in_dark 1
one_hit 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
end

