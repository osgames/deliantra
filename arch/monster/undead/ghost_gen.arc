object generate_ghost
name generator
race undead
other_arch ghost
face ghost_gen.x11
maxsp 1
exp 70
ac 5
speed 0.01
level 1
resist_cold 50
weight 25000
alive 1
no_pick 1
generator 1
undead 1
end

