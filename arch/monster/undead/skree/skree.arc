object skree
anim
facings 2
skree.x31
skree.x32
skree.x33
skree.x71
skree.x72
skree.x73
mina
inherit class_undead_monster
race undead
face skree.x31
str 30
con 20
wis 20
pow 80
int 7
hp 1500
maxhp 2000
sp 60
maxsp 60
exp 300000
dam 30
wc -30
ac -15
speed -0.5
level 35
attacktype 1025
resist_magic 90
resist_fire 90
resist_electricity 90
resist_cold 100
resist_fear 100
resist_godpower 80
resist_holyword 70
resist_blind 100
weight 1000000
randomitems skree
alive 1
monster 1
undead 1
can_cast_spell 1
sleep 1
end
more
object skree_2
anim
facings 2
skree.x31
skree.x32
skree.x33
skree.x71
skree.x72
skree.x73
mina
face skree.x31
x 1
alive 1
end
more
object skree_3
anim
facings 2
skree.x31
skree.x32
skree.x33
skree.x71
skree.x72
skree.x73
mina
face skree.x31
y 1
alive 1
end
more
object skree_4
anim
facings 2
skree.x31
skree.x32
skree.x33
skree.x71
skree.x72
skree.x73
mina
face skree.x31
x 1
y 1
alive 1
end

