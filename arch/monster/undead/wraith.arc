object wraith
anim
wraith.x11
wraith.x12
wraith.x13
mina
inherit class_undead_monster
race undead
face wraith.x11
con 2
wis 10
int 4
hp 40
maxhp 40
exp 120
dam 30
wc -5
ac 2
speed 0.16
level 8
attacktype 528
resist_fire -100
resist_cold 50
resist_fear 100
resist_blind 60
weight 700
randomitems wraith
alive 1
no_pick 1
monster 1
undead 1
sleep 1
can_see_in_dark 1
one_hit 1
end

