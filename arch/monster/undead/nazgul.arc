object nazgul
anim
nazgul.x11
nazgul.x12
mina
inherit class_undead_monster
race undead
face nazgul.x11
con 10
wis 15
int 8
hp 200
maxhp 200
exp 2000
dam 10
wc -3
speed 0.8
attack_movement 5
level 13
type 28
attacktype 160
resist_physical 100
resist_magic 50
resist_cold 50
resist_drain 100
resist_fear 100
resist_blind 90
carrying 100
weight 50000
randomitems nazgul
move_type fly_low
alive 1
no_pick 1
monster 1
undead 1
reflect_spell 1
sleep 1
can_see_in_dark 1
end

