object generate_skeleton
name generator
race undead
other_arch skeleton
face gravestone.x11
hp 75
maxhp 75
maxsp 1
exp 50
ac 3
speed 0.006
level 1
resist_cold 50
weight 750000
alive 1
no_pick 1
generator 1
undead 1
end

