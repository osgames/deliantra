object wight
anim
wight.x11
wight.x12
wight.x13
wight.x14
mina
inherit class_undead_monster
race undead
face wight.x11
wis 10
int 4
hp 75
maxhp 75
exp 75
dam 5
wc 13
ac 6
speed .10
level 5
attacktype 16385
resist_electricity 50
resist_cold 100
resist_fear 100
weight 15000
randomitems wight
alive 1
no_pick 1
monster 1
undead 1
sleep 1
can_see_in_dark 1
end

