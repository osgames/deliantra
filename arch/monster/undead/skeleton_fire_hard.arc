object skeleton_fire_hard
anim
skeleton_fire_hard.x11
skeleton_fire_hard.x12
mina
name skeleton
race undead
face skeleton_fire_hard.x11
con 30
wis 20
int 6
hp 600
maxhp 600
exp 400
dam 40
wc -80
ac -40
speed -0.8
level 80
attacktype 5
resist_fire 100
resist_cold 30
resist_fear 100
weight 15000
randomitems skeleton_fire
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
undead 1
can_use_armour 1
can_use_ring 1
sleep 1
can_see_in_dark 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
#can_use_weapon 1
end

