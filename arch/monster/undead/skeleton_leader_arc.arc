object skeleton_leader_arc
anim
skeleton_leader.x11
skeleton_leader.x11
mina
inherit class_undead_monster
name skeleton bezerk
race undead
face skeleton_leader.x11
con 2
wis 14
int 7
hp 60
maxhp 60
exp 120
dam 10
wc 12
ac 3
speed 0.2
level 7
attacktype 17
resist_physical 20
resist_fire -100
resist_cold 30
resist_fear 100
weight 15000
randomitems skeleton
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
undead 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_see_in_dark 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
end

