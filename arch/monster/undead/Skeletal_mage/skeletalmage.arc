object skeletalmage
anim
skeletalmage.x11
skeletalmage.x12
skeletalmage.x13
skeletalmage.x12
mina
inherit class_undead_monster
name Skeletal Mage
race undead
face skeletalmage.x11
str 15
con 2
wis 14
pow 5
int 3
hp 80
maxhp 80
sp 20
maxsp 20
exp 400
dam 5
wc 10
ac 4
speed 0.1
level 7
attacktype 17
resist_magic 10
resist_fire -20
resist_cold 85
resist_poison 100
resist_paralyze 50
resist_fear 100
resist_blind 70
weight 14000
randomitems skeletalmage
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
undead 1
can_cast_spell 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_see_in_dark 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end

