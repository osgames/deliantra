object skeletalmage_fire
anim
skeletalmage_fire.x11
skeletalmage_fire.x12
skeletalmage_fire.x13
skeletalmage_fire.x12
mina
name Skeletal Burning Mage
race undead
face skeletalmage_fire.x11
str 15
con 20
wis 25
pow 20
int 6
hp 600
maxhp 600
sp 300
maxsp 300
exp 800
dam 15
wc -45
ac -20
speed 0.2
level 55
attacktype 5
resist_magic 10
resist_fire 100
resist_cold 85
resist_poison 100
resist_paralyze 50
resist_fear 100
resist_blind 70
weight 14000
randomitems skeletalmage_fire
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
undead 1
can_cast_spell 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_see_in_dark 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 1
body_hand 2
body_wrist 1
body_waist 1
can_use_wand 1
end

