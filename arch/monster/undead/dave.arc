object dave
anim
dave.x11
dave.x12
dave.x13
mina
inherit class_undead_monster
name Dave
race undead
face dave.x11
str 100
dex 100
con 100
wis 100
pow 100
int 30
hp 30000
maxhp 30000
maxsp 40
exp 10000
dam 200
wc -100
ac -100
speed 0.150000
level 50
attacktype 4225
resist_physical 50
resist_magic 50
randomitems lich
pick_up 24
will_apply 2
alive 1
no_pick 1
monster 1
see_invisible 1
undead 1
can_cast_spell 1
can_use_scroll 1
can_use_ring 1
sleep 1
body_range 1
body_finger 2
can_use_wand 1
end

