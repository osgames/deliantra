object skull
anim
skull.x11
skull.x12
mina
inherit class_undead_monster
race undead
face skull.x11
con 2
wis 20
pow 16
int 10
hp 250
maxhp 250
maxsp 50
exp 5000
dam 15
wc -5
ac -2
speed 0.1
attack_movement 1
level 15
attacktype 17
resist_magic 100
resist_cold 100
resist_poison 100
resist_paralyze 100
resist_fear 100
resist_blind 80
weight 1500
randomitems skull
run_away 15
move_type fly_low
alive 1
no_pick 1
monster 1
undead 1
can_cast_spell 1
sleep 1
can_see_in_dark 1
end

