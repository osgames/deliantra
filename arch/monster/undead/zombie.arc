object zombie
anim
zombie.x11
zombie.x12
zombie.x13
zombie.x12
mina
inherit class_undead_monster
race undead
sound_destroy elmex/zombie
face zombie.x11
wis 10
int 0
hp 35
maxhp 35
exp 60
dam 5
wc 8
ac 9
speed -0.05
level 4
attacktype 1
resist_cold 50
resist_fear 100
weight 15000
randomitems zombie
alive 1
no_pick 1
monster 1
undead 1
sleep 1
can_see_in_dark 1
end

