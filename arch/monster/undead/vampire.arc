object vampire
anim
vampire.x11
vampire.x11
vampire.x12
vampire.x12
vampire.x12
vampire.x12
mina
inherit class_undead_monster
race undead
face vampire.x11
con 13
wis 15
pow 32
int 24
hp 150
maxhp 150
sp 40
maxsp 40
exp 2000
dam 10
wc 2
ac 0
speed -0.4
level 11
resist_physical 100
resist_cold 100
resist_blind 70
weight 70000
randomitems vampire
run_away 3
pick_up 64
will_apply 7
alive 1
can_use_shield 1
no_pick 1
monster 1
undead 1
can_cast_spell 1
can_use_scroll 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_rod 1
can_see_in_dark 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

