object deathshead
anim
deathshead.x11
deathshead.x12
deathshead.x13
deathshead.x14
mina
inherit class_undead_monster
name Death's Head
race undead
face deathshead.x11
con 100
wis 40
pow 100
int 6
hp 22000
maxhp 22000
sp 10000
maxsp 10000
exp 900000
dam 400
wc -70
ac -70
speed 0.5
attack_movement 1
level 130
attacktype 17
resist_physical 75
resist_magic 100
resist_fire 100
resist_electricity 90
resist_cold 100
resist_confusion 100
resist_acid 90
resist_drain 100
resist_weaponmagic 80
resist_ghosthit 100
resist_poison 100
resist_slow 100
resist_paralyze 100
resist_fear 100
resist_cancellation 65
resist_deplete 100
resist_death 100
resist_chaos 100
resist_counterspell 65
resist_godpower 80
resist_blind 100
weight 1500
glow_radius -5
randomitems deathshead
move_type fly_low
alive 1
no_pick 1
monster 1
undead 1
can_cast_spell 1
sleep 1
can_see_in_dark 1
end

