object xan
anim
xan.x11
xan.x12
mina
name xan
race insect
face xan.x11
wis 6
int 4
hp 1
maxhp 1
exp 20
dam 1
wc 1
ac 10
speed 0.5
level 5
attacktype 1025
weight 7
randomitems xan
move_type fly_low
alive 1
no_pick 1
monster 1
is_turnable 1
sleep 1
can_see_in_dark 1
end

