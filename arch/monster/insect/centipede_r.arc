object centipede_r
anim
centipede_r.x11
centipede_r.x12
centipede_r.x13
mina
name giant centipede
race insect
other_arch centipede_r
face centipede_r.x11
con 1
wis 5
int 6
hp 50
maxhp 50
sp 135
maxsp 60
exp 32
dam 10
wc 8
ac 10
speed -0.1
level 4
weight 5000
alive 1
no_pick 1
monster 1
generator 1
sleep 1
can_see_in_dark 1
end

