object generate_ant
name generator
race insect
other_arch ant
face ant_gen.x11
hp 120
maxhp 120
maxsp 1
exp 35
ac 20
speed 0.03
level 1
weight 100000
alive 1
no_pick 1
generator 1
end

