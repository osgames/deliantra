object beehive
anim
beehive.x11
beehive.x12
mina
name beehive
race insect
other_arch killer_bee
face beehive.x11
hp 30
maxhp 30
sp 136
maxsp 1
exp 50
ac 12
speed -0.1
level 1
weight -1
alive 1
generator 1
end

