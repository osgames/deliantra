object centipede
anim
facings 8
centipede.x11
centipede.x12
centipede.x13
centipede.x14
centipede.x21
centipede.x22
centipede.x23
centipede.x24
centipede.x31
centipede.x32
centipede.x33
centipede.x34
centipede.x41
centipede.x42
centipede.x43
centipede.x44
centipede.x51
centipede.x52
centipede.x53
centipede.x54
centipede.x61
centipede.x62
centipede.x63
centipede.x64
centipede.x71
centipede.x72
centipede.x73
centipede.x74
centipede.x81
centipede.x82
centipede.x83
centipede.x84
mina
name large centipede
race insect
other_arch centipede
face centipede.x11
con 1
wis 5
int 5
hp 10
maxhp 10
sp 135
maxsp 60
exp 32
dam 10
wc 8
ac 10
speed -0.1
level 4
weight 5000
alive 1
no_pick 1
monster 1
generator 1
sleep 1
can_see_in_dark 1
end

