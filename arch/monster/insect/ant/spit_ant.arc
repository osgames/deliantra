object spit_ant
anim
facings 2
spit_ant.x31
spit_ant.x32
spit_ant.x31
spit_ant.x32
spit_ant.x33
spit_ant.x32
spit_ant.x71
spit_ant.x72
spit_ant.x71
spit_ant.x72
spit_ant.x73
spit_ant.x72
mina
name Spitting Ant
race insect
face spit_ant.x31
con 15
wis 10
int 6
hp 100
maxhp 100
exp 800
dam 15
wc -5
ac -5
speed -0.18
attack_movement 7
level 12
attacktype 1025
resist_physical 45
resist_cold -100
resist_acid 100
resist_poison 100
weight 15000
randomitems spit_ant
run_away 50
alive 1
no_pick 1
monster 1
can_use_bow 1
sleep 1
body_arm 2
end

