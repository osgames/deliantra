object work_ant
anim
facings 2
work_ant.x31
work_ant.x32
work_ant.x71
work_ant.x72
mina
name Worker Ant
race insect
face work_ant.x31
con 10
int 7
hp 15
maxhp 15
exp 50
dam 3
wc 12
ac 10
speed 0.1
level 3
resist_physical 20
resist_cold -100
resist_acid 30
resist_poison 30
weight 5000
alive 1
no_pick 1
monster 1
end

