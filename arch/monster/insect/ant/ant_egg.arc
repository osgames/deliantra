object ant_egg
race insect
other_arch ant_larvae
face ant_egg.x11
hp 1
maxhp 1
exp 1
ac 20
speed 0.001
level 1
resist_cold -100
resist_acid 30
resist_poison 30
weight 5000
alive 1
no_pick 1
monster 1
generator 1
is_used_up 1
end

