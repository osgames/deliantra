object war_ant
anim
facings 2
war_ant.x31
war_ant.x32
war_ant.x71
war_ant.x72
mina
name Warrior Ant
race insect
face war_ant.x31
con 10
wis 10
int 8
hp 100
maxhp 100
exp 500
dam 15
wc -5
ac -5
speed -0.2
level 10
attacktype 1025
resist_physical 45
resist_cold -100
resist_acid 30
resist_poison 100
weight 15000
alive 1
no_pick 1
monster 1
sleep 1
end

