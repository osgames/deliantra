object fly_ant
anim
facings 2
fly_ant.x31
fly_ant.x32
fly_ant.x71
fly_ant.x72
mina
race insect
face fly_ant.x31
wis 6
int 8
hp 1
maxhp 1
exp 20
dam 5
wc 1
ac 10
speed -0.3
level 3
weight 7
move_type fly_low
alive 1
no_pick 1
monster 1
sleep 1
end

