object breeder_ant
anim
breeder_ant.x11
breeder_ant.x11
breeder_ant.x12
mina
name Queen Ant
race insect
other_arch ant_egg
face breeder_ant.x11
con 20
int 16
hp 1000
maxhp 1000
exp 5000
dam 30
wc -12
ac 2
speed -0.1
attack_movement 7
level 10
attacktype 1025
resist_physical 30
resist_cold -100
resist_acid 30
resist_poison 100
weight 300000
alive 1
no_pick 1
monster 1
generator 1
end
more
object breeder_ant_2
anim
breeder_ant.x11
breeder_ant.x11
breeder_ant.x12
mina
name Queen Ant
face breeder_ant.x11
x 1
weight 300000
move_block all
alive 1
no_pick 1
monster 1
end

