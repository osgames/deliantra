object ant_larvae
anim
ant_larvae.x11
ant_larvae.x12
mina
race insect
other_arch war_ant
face ant_larvae.x11
int 0
hp 3
maxhp 3
exp 5
dam 1
wc 20
ac 18
speed 0.005
level 1
resist_physical 10
resist_cold -100
resist_acid 30
resist_poison 30
weight 5000
alive 1
no_pick 1
monster 1
generator 1
is_used_up 1
end

