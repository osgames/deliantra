object guard_ant
name Guardian Ant
race insect
face war_ant.x31
animation war_ant
con 10
wis 5
int 8
hp 100
maxhp 100
exp 250
dam 25
wc -5
ac -5
speed -0.2
level 10
resist_physical 50
resist_cold -100
resist_acid 30
resist_poison 100
weight 15000
alive 1
no_pick 1
monster 1
unaggressive 1
sleep 1
stand_still 1
end

