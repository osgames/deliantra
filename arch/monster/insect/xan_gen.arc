object generate_xan
name Mosquito eggs
race insect
other_arch xan
face xan_gen.x11
hp 30
maxhp 30
exp 40
ac 6
speed 0.1
level 1
weight 750000
alive 1
no_pick 1
generator 1
end

