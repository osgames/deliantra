object killer_bee
anim
killerbee.x11
killerbee.x12
mina
name killer bee
race insect
face killerbee.x11
con 1
wis 10
int 8
hp 15
maxhp 15
exp 50
dam 13
wc 8
ac 10
speed -0.25
level 4
weight -1
run_away 75
move_type fly_low
alive 1
no_pick 1
monster 1
sleep 1
can_see_in_dark 1
end

