object bee
anim
bee.x11
bee.x12
mina
race insect
face bee.x11
wis 10
int 6
exp 5
dam 1
wc 15
ac 4
speed 0.2
level 1
resist_physical 30
weight 20
move_type fly_low
alive 1
no_pick 1
monster 1
sleep 1
can_see_in_dark 1
end

