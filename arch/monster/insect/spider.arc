object spider
anim
spider.x11
spider.x12
spider.x13
spider.x14
spider.x21
spider.x22
spider.x23
spider.x24
spider.x31
spider.x32
spider.x33
spider.x34
spider.x41
spider.x42
spider.x43
spider.x44
spider.x51
spider.x52
spider.x53
spider.x54
spider.x61
spider.x62
spider.x63
spider.x64
spider.x71
spider.x72
spider.x73
spider.x74
spider.x81
spider.x82
spider.x83
spider.x84
facings 8
mina
race insect
face spider.x11
wis 8
int 4
hp 5
maxhp 5
exp 60
dam 30
wc 15
ac 8
speed 0.4
level 3
alive 1
no_pick 1
monster 1
sleep 1
can_see_in_dark 1
end

