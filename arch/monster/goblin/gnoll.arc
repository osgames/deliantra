object gnoll
anim
gnoll.x11
gnoll.x12
mina
race goblin
sound_destroy wn/goblin-die-2
face gnoll.x11
str 15
dex 12
wis 8
int 8
hp 8
maxhp 8
exp 30
dam 4
wc 12
ac 13
speed -0.1
level 3
weight 45000
randomitems gnoll
run_away 30
alive 1
no_pick 1
monster 1
sleep 1
can_use_skill 1
body_skill 1
end

