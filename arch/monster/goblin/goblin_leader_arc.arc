object goblin_leader_arc
anim
goblin_leader.x11
goblin_leader.x11
mina
name goblin champion
race goblin
sound_destroy wn/goblin-die-1
face goblin_leader.x11
wis 10
int 6
hp 16
maxhp 16
exp 40
dam 16
wc 16
ac 13
speed 0.1
level 3
weight 50000
randomitems goblin
run_away 10
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

