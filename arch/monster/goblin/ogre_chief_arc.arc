object ogre_chief_arc
anim
ogre_chief.x11
ogre_chief.x11
mina
name ogre chief
race giant
face ogre_chief.x11
str 22
dex 20
con 2
wis 10
int 9
hp 160
maxhp 160
exp 200
dam 30
wc 5
ac 6
speed 0.08
level 8
resist_physical 30
resist_electricity 30
weight 140000
randomitems ogre
run_away 30
alive 1
can_use_shield 1
no_pick 1
monster 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_see_in_dark 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

