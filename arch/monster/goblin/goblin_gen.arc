object generate_goblin
name generator
race goblin
other_arch goblin
face goblin_gen.x11
hp 30
maxhp 30
maxsp 1
exp 100
ac 8
speed 0.02
level 1
weight 1000000
alive 1
no_pick 1
generator 1
end

