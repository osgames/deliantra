object generate_ogre
name generator
race giant
other_arch ogre
face ogre_gen.x11
hp 60
maxhp 60
maxsp 1
exp 300
ac 7
speed 0.002
level 1
weight 1000000
alive 1
generator 1
end

