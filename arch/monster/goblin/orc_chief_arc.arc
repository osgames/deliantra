object orc_chief_arc
anim
orc_chief.x11
orc_chief.x12
mina
name orc chief
race goblin
sound_destroy wn/orc-die-2
face orc_chief.x11
wis 8
int 6
hp 24
maxhp 24
exp 75
dam 15
wc 17
ac 14
speed -0.15
level 3
resist_physical 30
weight 23000
randomitems orc
run_away 25
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

