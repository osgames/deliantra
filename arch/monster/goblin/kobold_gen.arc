object generate_kobold
name generator
race goblin
other_arch kobold
face kobold_gen.x11
hp 20
maxhp 20
maxsp 1
exp 30
ac 15
speed 0.02
level 1
weight 1000000
alive 1
generator 1
end

