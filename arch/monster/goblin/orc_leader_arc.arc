object orc_leader_arc
anim
orc_leader.x11
orc_leader.x12
mina
name orc champion
race goblin
sound_destroy wn/orc-die-3
face orc_leader.x11
wis 8
int 7
hp 16
maxhp 16
exp 35
dam 14
wc 18
ac 15
speed -0.15
level 2
weight 23000
randomitems orc
run_away 25
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

