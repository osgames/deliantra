object kobold
anim
kobold.x11
kobold.x12
mina
race goblin
sound_destroy wn/human-female-die-2
face kobold.x11
wis 8
int 8
hp 2
maxhp 2
exp 5
dam 2
wc 27
ac 18
speed 0.2
level 1
weight 30000
randomitems kobold
run_away 90
alive 1
no_pick 1
monster 1
sleep 1
end

