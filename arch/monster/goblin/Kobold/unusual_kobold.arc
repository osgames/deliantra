object unusual_kobold
anim
unusual_kobold.x11
unusual_kobold.x12
mina
name unusual kobold
race goblin
sound_destroy wn/human-female-die-2
face unusual_kobold.x11
is_animated 1
str 30
dex 30
con 100
wis 50
int 14
hp 10000
maxhp 10000
exp 5000
dam 70
wc -50
ac -100
speed .4
level 30
resist_physical 100
resist_cold -100
carrying 6000
weight 30000
alive 1
no_pick 1
monster 1
sleep 1
end

