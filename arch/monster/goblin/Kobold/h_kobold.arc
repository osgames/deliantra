object h_kobold
anim
h_kobold.x11
h_kobold.x12
mina
name hyper kobold
race goblin
sound_destroy wn/human-female-die-2
face h_kobold.x11
str 30
dex 30
con 30
wis 50
int 12
hp 2000
maxhp 2000
exp 50000
dam 70
wc -30
ac -30
speed 1.200000
attack_movement 3
level 50
resist_cold -100
carrying 6000
weight 30000
alive 1
no_pick 1
monster 1
sleep 1
end

