object gnoll_leader_arc
anim
gnoll_leader.x11
gnoll_leader.x11
mina
name gnoll champion
race goblin
sound_destroy wn/goblin-die-2
face gnoll_leader.x11
str 15
dex 12
wis 8
int 8
hp 21
maxhp 21
exp 50
dam 17
wc 11
ac 12
speed 0.1
level 4
weight 45000
randomitems gnoll
run_away 30
alive 1
no_pick 1
monster 1
sleep 1
can_use_skill 1
body_skill 1
end

