object ogre
anim
ogre.x11
ogre.x12
mina
name ogre
race giant
sound_destroy ss/weirdbirdcall2
face ogre.x11
str 22
dex 20
con 2
wis 10
int 8
hp 50
maxhp 50
exp 100
dam 8
wc 7
ac 10
speed -0.08
level 5
resist_electricity 30
weight 140000
randomitems ogre
run_away 30
alive 1
can_use_shield 1
no_pick 1
monster 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_see_in_dark 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

