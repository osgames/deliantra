object goblin_chief_arc
anim
facings 8
goblin.x11
goblin.x21
goblin.x12
goblin.x22
goblin.x13
goblin.x23
goblin.x14
goblin.x24
goblin.x15
goblin.x25
goblin.x16
goblin.x26
goblin.x17
goblin.x27
goblin.x18
goblin.x28
mina
name goblin chief
race goblin
sound_destroy wn/goblin-die-1
face goblin.x11
wis 10
int 5
hp 30
maxhp 30
exp 120
dam 16
wc 15
ac 12
speed 0.1
level 4
resist_physical 30
weight 50000
randomitems goblin
run_away 10
pick_up 24
will_apply 2
alive 1
can_use_shield 1
no_pick 1
monster 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

