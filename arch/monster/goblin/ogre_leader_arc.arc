object ogre_leader_arc
anim
ogre_leader.x11
ogre_leader.x11
mina
name ogre champion
race giant
sound_destroy ss/weirdbirdcall2
face ogre_leader.x11
str 22
dex 20
con 2
wis 10
int 10
hp 75
maxhp 75
exp 150
dam 22
wc 5
ac 8
speed 0.08
level 7
resist_electricity 50
weight 140000
randomitems ogre
run_away 30
alive 1
can_use_shield 1
no_pick 1
monster 1
can_use_scroll 1
can_use_bow 1
can_use_armour 1
can_use_weapon 1
can_use_ring 1
sleep 1
can_use_skill 1
can_see_in_dark 1
body_range 1
body_arm 2
body_torso 1
body_head 1
body_skill 1
body_finger 2
body_shoulder 1
body_foot 2
body_hand 2
body_wrist 2
body_waist 1
can_use_wand 1
end

