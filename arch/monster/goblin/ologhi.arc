object ologhi
anim
ologhi.x11
ologhi.x12
mina
name Olog-hi
race goblin
face ologhi.x11
is_animated 1
str 30
dex 20
con 10
wis 4
int 8
hp 5000
maxhp 5000
exp 1000
dam 100
wc -100
ac -100
speed -0.25
attack_movement 5
level 20
resist_physical 100
resist_magic -100
resist_electricity -100
carrying 20000
weight 140000
randomitems throw_stnd
alive 1
no_pick 1
monster 1
sleep 1
can_see_in_dark 1
end

