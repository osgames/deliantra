object gnoll_chief_arc
anim
gnoll_chief.x11
gnoll_chief.x11
mina
name gnoll chief
race goblin
sound_destroy wn/goblin-die-2
face gnoll_chief.x11
str 15
dex 12
wis 8
int 8
hp 40
maxhp 40
exp 120
dam 20
wc 10
ac 11
speed -0.1
level 5
resist_physical 30
weight 45000
randomitems gnoll
run_away 30
alive 1
no_pick 1
monster 1
sleep 1
can_use_skill 1
body_skill 1
end

