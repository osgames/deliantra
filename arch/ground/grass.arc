object grass
race /terrain/plains
face grass.x11
smoothlevel 40
smoothface grass.x11 grass_S.x11
type 67
randomitems grass
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
treasure_env 1
end

object grass_only
name grass
face grass.x11
smoothlevel 40
smoothface grass.x11 grass_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
end

