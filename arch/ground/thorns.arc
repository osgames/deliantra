# a specific type of spell effect (cone) which operates
# as a ground object.
object thorns
name thorns
face thorns.x11
food 1
dam 3
wc -1
speed 0.2
level 1
type 102
subtype 7
attacktype 1
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 3
no_pick 1
is_wooded 1
end

