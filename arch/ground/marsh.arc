object marsh
race /terrain/swamp
face marsh.x11
smoothlevel 9
smoothface marsh.x11 marsh_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 15
no_pick 1
is_floor 1
is_wooded 1
is_water 1
end

