object spalms1
name snow covered palms
race /terrain/forest
face spalms1.x11
type 67
move_on walk
move_slow walk
move_slow_penalty 2
no_pick 1
is_wooded 1
end

object spalms2
name snow covered palms
race /terrain/forest
face spalms2.x11
type 67
move_on walk
move_slow walk
move_slow_penalty 2
no_pick 1
is_wooded 1
end

