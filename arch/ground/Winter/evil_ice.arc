object evil_ice
anim
evil_ice.x11
evil_ice.x12
evil_ice.x13
evil_ice.x14
mina
name strange looking ice
face evil_ice.x11
hp 1
dam 10
wc -30
speed 0.2
level 1
type 102
subtype 7
attacktype 16
move_on walk
no_pick 1
is_floor 1
lifesave 1
end

