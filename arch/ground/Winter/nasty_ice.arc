object nasty_ice
anim
nasty_ice.x11
nasty_ice.x12
nasty_ice.x13
nasty_ice.x14
nasty_ice.x15
nasty_ice.x16
nasty_ice.x17
nasty_ice.x18
nasty_ice.x19
nasty_ice.x1A
nasty_ice.x1B
nasty_ice.x1C
mina
name very cold ice
face nasty_ice.x11
hp 1
dam 5
wc -30
speed 0.2
level 1
type 102
subtype 7
attacktype 16
move_on walk
no_pick 1
is_floor 1
lifesave 1
end

