object glacier
name glacier
race /terrain/mountain
face glacier.x11
smoothlevel 23
smoothface glacier.x11 empty_S.x11
dam 2
type 67
attacktype 16
move_on walk
move_slow walk
move_slow_penalty 15
no_pick 1
is_floor 1
is_hilly 1
end

