object soccergrass1
name grass
race /terrain/plains
face soccergrass.x11
smoothlevel 40
smoothface soccergrass.x11 grass_S.x11
type 67
move_block swim boat
move_on walk
move_slow -all
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
end

object soccergrass2
name grass
race /terrain/plains
face soccergrass.x12
smoothlevel 40
smoothface soccergrass.x12 grass_S.x11
type 67
move_block swim boat
move_on walk
move_slow -all
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
end

object soccergrass3
name painted grass
race /terrain/plains
face soccergrass.x13
smoothlevel 40
smoothface soccergrass.x13 grass_S.x11
type 67
move_block swim boat
move_on walk
move_slow -all
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
end

