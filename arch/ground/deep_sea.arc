object deep_sea
anim
deep_sea.x11
deep_sea.x12
deep_sea.x13
deep_sea.x14
deep_sea.x13
deep_sea.x12
mina
face deep_sea.x11
inherit sea_base
move_block walk fly_low swim boat
smoothlevel 17
smoothface deep_sea.x11 deep_sea_S.x11 deep_sea.x12 deep_sea_S.x12 deep_sea.x13 deep_sea_S.x13 deep_sea.x14 deep_sea_S.x14
speed -0.12
no_pick 1
is_floor 1
is_water 1
end

