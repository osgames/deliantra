object quicksilver_sea
anim
quicksilver_sea.x11
quicksilver_sea.x12
quicksilver_sea.x13
quicksilver_sea.x14
quicksilver_sea.x13
quicksilver_sea.x12
mina
name quicksilver sea
name_pl quicksilver sea
face quicksilver_sea.x11
smoothlevel 16
smoothface quicksilver_sea.x11 quicksilver_sea_S.x11 quicksilver_sea.x12 quicksilver_sea_S.x12 quicksilver_sea.x13 quicksilver_sea_S.x13 quicksilver_sea.x14 quicksilver_sea_S.x14
speed 0.35
move_block all
no_pick 1
is_floor 1
is_water 1
end

