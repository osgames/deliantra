object cactus1
name cactus
name_pl cacti
face cactus1.x11
move_slow walk
move_slow_penalty 1
no_pick 1
is_wooded 1
end

object cactus2
inherit cactus1
name cactus
face cactus2.x11
end

object cactus3
inherit cactus1
name cactus
face cactus3.x11
end

