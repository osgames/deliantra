object deep_swamp
anim
deep_swamp.x11
deep_swamp.x12
mina
name swamp
face deep_swamp.x11
smoothlevel 8
smoothface deep_swamp.x11 deep_swamp_S.x11 deep_swamp.x12 deep_swamp_S.x12
speed -0.07
type 47
move_on walk
move_slow walk
move_slow_penalty 5
no_pick 1
is_floor 1
is_wooded 1
is_water 1
end

