object dunes
race /terrain/desert
face dunes.x11
smoothlevel 91
smoothface dunes.x11 dunes_S.x11
type 67
randomitems desert
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
treasure_env 1
end

