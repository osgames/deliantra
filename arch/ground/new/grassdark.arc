object grassdark
name dark grass
face grassdark.x11
smoothlevel 39
smoothface grassdark.x11 grassdark_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
end

