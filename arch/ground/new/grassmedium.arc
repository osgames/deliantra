object grassmedium
name medium grass
face grassmedium.x11
smoothlevel 41
smoothface grassmedium.x11 empty_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
end

