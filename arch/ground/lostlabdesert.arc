object lostlabdesert
name desert
race /terrain/desert
face lostlabdesert.x11
smoothlevel 37
smoothface lostlabdesert.x11 lostlabdesert_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
end

object lostlabdesert2
name desert mountain
race /terrain/desert
face lostlabdesert2.x11
smoothlevel 37
smoothface lostlabdesert2.x11 lostlabdesert_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 5
no_pick 1
blocksview 1
is_floor 1
is_wooded 1
is_hilly 1
end

object lostlabdesert3
name desert
race /terrain/desert
face lostlabdesert3.x11
smoothlevel 37
smoothface lostlabdesert3.x11 lostlabdesert_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
end

object lostlabdesert4
name desert
race /terrain/desert
face lostlabdesert4.x11
smoothlevel 37
smoothface lostlabdesert4.x11 lostlabdesert_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
end

object lostlabdesert5
name desert
race /terrain/desert
face lostlabdesert5.x11
smoothlevel 37
smoothface lostlabdesert5.x11 lostlabdesert_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
end

