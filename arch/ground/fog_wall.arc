# a specific type of spell effect (cone) which operates
# as a ground object.
object fog_wall
anim
fog.x11
fog.x12
fog.x11
mina
name fog
face fog.x11
speed 0.001
type 102
subtype 7
resist_fire 100
resist_electricity 100
resist_confusion 100
resist_acid 100
resist_drain 100
resist_weaponmagic 100
resist_ghosthit 100
resist_poison 100
resist_slow 100
resist_paralyze 100
resist_turn_undead 100
resist_fear 100
resist_cancellation 100
resist_deplete 100
resist_death 100
materialname paper
move_type fly_low
no_pick 1
changing 1
blocksview 1
end

