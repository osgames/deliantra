# a specific type of spell effect (cone) which operates
# as a ground object.
object chaos_ball
anim
explosion.x11
burnout.x1O
fireball.x11
ball_lightning.x11
icestorm.x11
confusion.x11
acid_pool.x11
poisoncloud.x11
slow.x11
paralyse.x11
fear.x11
mina
name chaos
face ball_lightning.x11
dam 5
wc -30
speed 1
level 1
type 102
subtype 7
attacktype 262144
move_type fly_low
move_on walk fly_low
no_pick 1
is_used_up 1
lifesave 1
random_movement 1
end

