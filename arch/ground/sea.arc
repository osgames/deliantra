object sea
anim
sea.x11
sea.x12
sea.x13
sea.x14
sea.x13
sea.x12
mina
inherit sea_base
face sea.x11
smoothlevel 20
smoothface sea.x11 sea_S.x11 sea.x12 sea_S.x12 sea.x13 sea_S.x13 sea.x14 sea_S.x14
speed -0.12
end

object sea1
anim
sea.x11
sea.x12
sea.x13
sea.x14
sea.x13
sea.x12
mina
inherit sea_base
name sea
face sea.x11
speed -0.12
end

object sea_base
move_block walk fly_low swim
no_pick 1
is_floor 1
is_water 1
end

object sea_ne
inherit sea_base
name sea
face sea_ne.x11
end

object sea_nw
inherit sea_base
name sea
face sea_nw.x11
end

object sea_se
inherit sea_base
name sea
face sea_se.x11
end

object sea_sw
inherit sea_base
name sea
face sea_sw.x11
end

