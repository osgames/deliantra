object sea_cyan
anim
sea_cyan.x11
sea_cyan.x12
sea_cyan.x13
sea_cyan.x14
sea_cyan.x13
sea_cyan.x12
mina
inherit sea_base
face sea_cyan.x11
smoothlevel 20
smoothface sea_cyan.x11 sea_cyan_S.x11 sea_cyan.x12 sea_cyan_S.x12 sea_cyan.x13 sea_cyan_S.x13 sea_cyan.x14 sea_cyan_S.x14
speed -0.12
end