object evergreens
race /terrain/forest
face evergreens.x11
smoothlevel 135
smoothface evergreens.x11 evergreens_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 3
no_pick 1
is_floor 1
is_wooded 1
end

