object treed_hills
race /terrain/hills
face treed_hills.x11
smoothlevel 130
smoothface treed_hills.x11 hills_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 5
no_pick 1
is_floor 1
is_wooded 1
is_hilly 1
end

