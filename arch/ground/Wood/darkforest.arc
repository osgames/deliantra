object darkforest
race /terrain/forest
face darkforest.x11
smoothlevel 136
smoothface darkforest.x11 darkforest_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 5
no_pick 1
blocksview 1
is_floor 1
is_wooded 1
end

