object evergreens2
name evergreens
race /terrain/forest
face evergreens_2.x11
smoothlevel 125
smoothface evergreens_2.x11 empty_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 3
no_pick 1
is_wooded 1
end

