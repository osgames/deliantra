object bforest
name Dark Forest
race /terrain/jungle
face bforest.x11
smoothlevel 129
smoothface jungle_1.x11 jungle_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 5
no_pick 1
blocksview 1
is_floor 1
is_wooded 1
end

object jungle_1
name jungle
race /terrain/jungle
face jungle_1.x11
smoothlevel 129
smoothface jungle_1.x11 jungle_S.x11
type 67
randomitems jungle
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 5
no_pick 1
blocksview 1
is_floor 1
is_wooded 1
treasure_env 1
end

