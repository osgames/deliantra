object woods_3
name woods
race /terrain/forest
face woods_3.x11
smoothlevel 127
smoothface woods_3.x11 sgrass_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
end

