object jungle_2
name jungle
race /terrain/jungle
face jungle_2.x11
smoothlevel 128
smoothface jungle_2.x11 jungle_S.x11
type 67
randomitems jungle
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 5
no_pick 1
blocksview 1
is_floor 1
is_wooded 1
treasure_env 1
end

