object cyanbrush
race /terrain/plains
face cyanbrush.x11
smoothlevel 45
smoothface cyanbrush.x11 cyangrass_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 2
no_pick 1
is_floor 1
is_wooded 1
end

