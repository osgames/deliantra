object cyangrass
race /terrain/plains
face cyangrass.x11
smoothlevel 45
smoothface cyangrass.x11 cyangrass_S.x11
type 67
move_on walk
move_slow walk
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
end

object cyangrass_only
name cyangrass
face cyangrass.x11
smoothlevel 45
smoothface cyangrass.x11 cyangrass_S.x11
type 67
move_on walk
move_slow walk
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
end

