object beach
race /terrain/desert
face beach.x11
smoothlevel 90
smoothface beach.x11 beach_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 0
end

