object cmarsh
name cold marsh
race /terrain/swamp
face cmarsh.x11
smoothlevel 8
smoothface cmarsh.x11 empty_S.x11
type 67
move_on walk
move_slow walk
move_slow_penalty 20
no_pick 1
is_floor 1
is_wooded 0
is_water 1
end

