# a specific type of spell effect (cone) which operates
# as a ground object.
object lava
anim
lava.x11
lava.x12
lava.x13
lava.x14
lava.x15
mina
name lava
face lava.x11
smoothlevel 28
smoothface lava.x11 lava_S.x11 lava.x12 lava_S.x12 lava.x13 lava_S.x13 lava.x14 lava_S.x14 lava.x15 lava_S.x15
dam 3
wc -30
speed 0.2
level 1
type 102
subtype 7
attacktype 4
duration 60
move_block swim boat
move_on walk
no_pick 1
is_floor 1
end

object lava_ground
anim
lava.x11
lava.x12
lava.x13
lava.x14
lava.x15
mina
name lava
face lava.x11
speed 0.2
move_block swim boat
move_on walk
no_pick 1
is_floor 1
end

object permanent_lava
anim
lava.x11
lava.x12
lava.x13
lava.x14
lava.x15
mina
name lava
face lava.x11
hp 1
dam 3
wc -30
speed 0.2
level 1
type 102
subtype 7
attacktype 4
move_block swim boat
move_on walk
no_pick 1
is_floor 1
lifesave 1
end

