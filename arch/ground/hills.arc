object hills
race /terrain/hills
face hills.x11
smoothlevel 59
smoothface hills.x11 hills_S.x11
type 67
randomitems hills
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 3
no_pick 1
is_floor 1
is_hilly 1
treasure_env 1
end

