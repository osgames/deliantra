object hills_rocky
name hills
race /terrain/hills
face hillsrocky.x11
smoothlevel 60
smoothface hillsrocky.x11 hillsrocky_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 2
no_pick 1
is_floor 1
is_hilly 1
end

