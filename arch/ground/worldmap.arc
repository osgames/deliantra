object darkforest_nb
inherit darkforest
name darkforest
blocksview 0
end

# extra non-blocksview variants of some archetypes for the worldmap
object jungle_1_nb
inherit jungle_1
name jungle
blocksview 0
end

object jungle_2_nb
inherit jungle_2
name jungle
blocksview 0
end

object mountain_nb
inherit mountain
name mountain
blocksview 0
end

