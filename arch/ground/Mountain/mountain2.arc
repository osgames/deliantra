object mountain2
name high mountains
race /terrain/mountain
face mountain2.x11
smoothlevel 100
smoothface mountain2.x11 mountain_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 10
no_pick 1
blocksview 1
is_floor 1
is_hilly 1
end

