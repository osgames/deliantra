object mountain_2
name high mountain
face mountain_2.x11
move_block swim boat
move_slow walk
move_slow_penalty 20
no_pick 1
blocksview 1
is_floor 1
is_hilly 1
end
more
object mountain_2_2
name high mountain
face mountain_2.x11
x 1
move_block swim boat
move_slow walk
move_slow_penalty 20
no_pick 1
blocksview 1
is_floor 1
is_hilly 1
end
more
object mountain_2_3
name high mountain
face mountain_2.x11
y 1
move_block swim boat
move_slow walk
move_slow_penalty 20
no_pick 1
blocksview 1
is_floor 1
is_hilly 1
end
more
object mountain_2_4
name high mountain
face mountain_2.x11
x 1
y 1
move_block swim boat
move_slow walk
move_slow_penalty 20
no_pick 1
blocksview 1
is_floor 1
is_hilly 1
end

