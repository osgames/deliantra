object mountain
name mountains
race /terrain/mountain
face mountain1.x11
smoothlevel 100
smoothface mountain1.x11 mountain_S.x11
type 67
randomitems mountain
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 5
no_pick 1
blocksview 1
is_floor 1
is_hilly 1
treasure_env 1
end

