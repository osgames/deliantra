object active_volcano
name volcano
other_arch spell_create_lava
face volcano_lo.x11
speed -0.02
level 1
type 62
move_block swim boat
move_slow walk
move_slow_penalty 15
no_pick 1
is_hilly 1
activate_on_push 1
activate_on_release 1
end
more
object active_volcano_2
name volcano
other_arch spell_create_lava
face volcano_lo.x11
x 1
speed -0.02
level 1
type 62
move_block swim boat
move_slow walk
move_slow_penalty 15
no_pick 1
is_hilly 1
activate_on_push 1
activate_on_release 1
end

