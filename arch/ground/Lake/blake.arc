object biglake_center
face blake_F.x11
smoothlevel 255
smoothface blake_F.x11 empty_S.x11
move_block all
no_pick 1
is_floor 1
is_water 1
end

object biglake_e
face blake_2.x11
smoothlevel 255
smoothface blake_2.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_ew
face blake_A.x11
smoothlevel 255
smoothface blake_A.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_n
face blake_1.x11
smoothlevel 255
smoothface blake_1.x11 empty_S.x11
move_block all
no_pick 1
is_floor 1
is_water 1
end

object biglake_ne
face blake_3.x11
smoothlevel 255
smoothface blake_3.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_nes
face blake_7.x11
smoothlevel 255
smoothface blake_7.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_new
face blake_B.x11
smoothlevel 255
smoothface blake_B.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_nse
face blake_D.x11
smoothlevel 255
smoothface blake_D.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_nw
face blake_9.x11
smoothlevel 255
smoothface blake_9.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_s
face blake_4.x11
smoothlevel 255
smoothface blake_4.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_se
face blake_6.x11
smoothlevel 255
smoothface blake_6.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_sew
face blake_E.x11
smoothlevel 255
smoothface blake_E.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_sn
face blake_5.x11
smoothlevel 255
smoothface blake_5.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_sw
face blake_C.x11
smoothlevel 255
smoothface blake_C.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_w
face blake_8.x11
smoothlevel 255
smoothface blake_8.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object lake
face blake_0.x11
smoothlevel 255
smoothface blake_0.x11 empty_S.x11
move_block all
no_pick 1
is_floor 1
is_water 1
end

