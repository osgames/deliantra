object biglake_0
name lake
face blake_0.x11
smoothlevel 255
smoothface blake_0.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_1_1
name lake
face blake_B.x11
smoothlevel 255
smoothface blake_B.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_1_2
name lake
face blake_E.x11
smoothlevel 255
smoothface blake_E.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_1_3
name lake
face blake_7.x11
smoothlevel 255
smoothface blake_7.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_1_4
name lake
face blake_D.x11
smoothlevel 255
smoothface blake_D.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_2_1_1
name lake
face blake_A.x11
smoothlevel 255
smoothface blake_A.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_2_1_2
name lake
face blake_5.x11
smoothlevel 255
smoothface blake_5.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_2_2_1
name lake
face blake_C.x11
smoothlevel 255
smoothface blake_C.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_2_2_2
name lake
face blake_9.x11
smoothlevel 255
smoothface blake_9.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_2_2_3
name lake
face blake_3.x11
smoothlevel 255
smoothface blake_3.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_2_2_4
name lake
face blake_6.x11
smoothlevel 255
smoothface blake_6.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_3_1
name lake
face blake_4.x11
smoothlevel 255
smoothface blake_4.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_3_2
name lake
face blake_8.x11
smoothlevel 255
smoothface blake_8.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_3_3
name lake
face blake_1.x11
smoothlevel 255
smoothface blake_1.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_3_4
name lake
face blake_2.x11
smoothlevel 255
smoothface blake_2.x11 empty_S.x11
move_block all
no_pick 1
is_water 1
end

object biglake_4
name lake
face blake_F.x11
smoothlevel 255
smoothface blake_F.x11 empty_S.x11
move_block all
no_pick 1
is_floor 1
is_water 1
end

