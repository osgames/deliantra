object oil_sea
anim
oil_sea.x11
oil_sea.x12
oil_sea.x13
oil_sea.x14
oil_sea.x13
oil_sea.x12
mina
name oil ocean
name_pl oil ocean
face oil_sea.x11
smoothlevel 16
smoothface oil_sea.x11 oil_sea_S.x11 oil_sea.x12 oil_sea_S.x12 oil_sea.x13 oil_sea_S.x13 oil_sea.x14 oil_sea_S.x14
speed 0.1
move_block all
no_pick 1
is_floor 1
is_water 1
end

object oil_sea_ultra_viscous
anim
oil_sea.x11
oil_sea.x12
oil_sea.x13
oil_sea.x14
oil_sea.x13
oil_sea.x12
mina
name ultra viscous oil
name_pl ultra viscous oil
face oil_sea.x11
speed 0.01
move_block -all
no_pick 1
is_floor 1
is_water 1
# which will be swimable as water is (but much slower swiming ofcourse)
end

