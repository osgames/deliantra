object fog
anim
fog.x11
fog.x12
fog.x11
fog.x11
fog.x12
fog.x11
fog.x11
fog.x12
fog.x11
mina
other_arch fog
face fog.x11
smoothlevel 250
smoothface fog.x11 fog_S.x11 fog.x12 fog_S.x11
speed 0.001
type 67
resist_fire 100
resist_electricity 100
resist_confusion 100
resist_acid 100
resist_drain 100
resist_weaponmagic 100
resist_ghosthit 100
resist_poison 100
resist_slow 100
resist_paralyze 100
resist_turn_undead 100
resist_fear 100
resist_cancellation 100
resist_deplete 100
resist_death 100
materialname paper
move_type fly_low
no_pick 1
generator 1
is_used_up 1
changing 1
blocksview 1
end

