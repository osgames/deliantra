object grasspond
name small pond
race /terrain/plains
face grasspond.x11
smoothlevel 40
smoothface grasspond.x11 grass_S.x11
type 67
move_block swim boat
move_on walk
no_pick 1
is_floor 1
is_water 1
end

