object blood_sea
anim
blood_sea.x11
blood_sea.x12
blood_sea.x13
blood_sea.x14
blood_sea.x13
blood_sea.x12
mina
name sea of blood
name_pl sea of blood
face blood_sea.x11
smoothlevel 29
smoothface blood_sea.x11 blood_sea_S.x11 blood_sea.x12 blood_sea_S.x12 blood_sea.x13 blood_sea_S.x13 blood_sea.x14 blood_sea_S.x14
speed 0.2
move_block all
no_pick 1
is_floor 1
is_water 1
end

