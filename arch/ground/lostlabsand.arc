object lostlabbluesand
name sand
face lostlabbluesand.x11
smoothlevel 37
smoothface lostlabbluesand.x11 lostlabbluesand_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
end

object lostlabgreysand
name sand
face lostlabgreysand.x11
smoothlevel 37
smoothface lostlabgreysand.x11 lostlabgreysand_S.x11
type 67
move_block swim boat
move_on walk
move_slow walk
move_slow_penalty 1
no_pick 1
is_floor 1
is_wooded 1
end

