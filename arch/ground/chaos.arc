# This is actually set up as a cone effect.  IT basically persists
# and does a little amount of damage.
object chaos
anim
explosion.x11
burnout.x1O
fireball.x11
ball_lightning.x11
icestorm.x11
confusion.x11
acid_pool.x11
poisoncloud.x11
slow.x11
paralyse.x11
fear.x11
mina
name chaos
face ball_lightning.x11
dam 2
wc -30
speed 1
level 1
type 102
subtype 7
attacktype 262144
move_on walk
no_pick 1
is_floor 1
lifesave 1
end

object major_chaos
anim
explosion.x11
burnout.x1O
fireball.x11
ball_lightning.x11
icestorm.x11
confusion.x11
acid_pool.x11
poisoncloud.x11
slow.x11
paralyse.x11
fear.x11
mina
name chaos
face ball_lightning.x11
dam 10
wc -30
speed 1
level 1
type 102
subtype 7
attacktype 262144
move_on walk
no_pick 1
is_floor 1
lifesave 1
end

