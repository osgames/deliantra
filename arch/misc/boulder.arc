# old stone boulder
object boulder
face boulder.x11
type 79
weight 1000000
move_type walk
move_block all
no_pick 1
can_roll 1
end

object boulder_lava
face boulder_lava.x11
type 79
weight 1000000
move_type walk
move_block all
no_pick 1
can_roll 1
end

object boulder_steel
face boulder_steel.x11
type 79
weight 1000000
move_type walk
move_block all
no_pick 1
can_roll 1
end

