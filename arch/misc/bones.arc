object bones1
inherit type_weapon
name bones
name_pl bones
skill one handed weapons
face bones1.x11
dam 20
nrof 1
materialname bone
value 0
weight 7000
last_sp 19
client_type 8002
body_arm -1
body_combat -1
end

object bones2
inherit type_weapon
name bones
name_pl bones
skill one handed weapons
face bones2.x11
dam 20
nrof 1
materialname bone
value 0
weight 2000
last_sp 19
client_type 8002
body_arm -1
body_combat -1
end

object bones3
inherit type_weapon
name bones
name_pl bones
skill one handed weapons
face bones3.x11
dam 20
nrof 1
materialname bone
value 0
weight 1000
last_sp 19
client_type 8002
body_arm -1
body_combat -1
end

object bones4
inherit type_weapon
name bones
name_pl bones
skill one handed weapons
face bones4.x11
dam 20
nrof 1
materialname bone
value 0
weight 7000
last_sp 19
client_type 8002
body_arm -1
body_combat -1
end

object bones5
inherit type_weapon
name bones
name_pl bones
skill one handed weapons
face bones5.x11
dam 20
nrof 1
materialname bone
value 0
weight 11000
last_sp 19
client_type 8002
body_arm -1
body_combat -1
end

