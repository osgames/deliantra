# Permission is granted to copy, distribute and/or modify machine.base.x11.png
# under the terms of the GNU Free Documentation License, Version 1.2 or any
# later version published by the Free Software Foundation; with no Invariant
# Sections, no Front-Cover Texts, and no Back-Cover Texts.  A copy of the
# license may be found at http://www.gnu.org/licenses/fdl.html.
#
# The machine.base.x11.png artwork was created by Kevin R. Bulgrien (kbulgrien
# at att daht net) on 2008/01/10, and is a derivation of an image published at:
#
#   http://www.marcdatabase.com/
#     ~lemur/lemur.com/gallery-of-antiquarian-technology/
#       steam-engines/hf-steam-models/hf-table-engine-2-1200x1600.jpg
#
# The original work was licensed under the GFDL and is Copyright © 2001, 2004
# by David M. MacMillan and Rollande Krandall. David M. MacMillan (dmm at lemur
# daht com), the original author, granted permission on 2008/01/12 to relicense
# this derivative under the GPL for the purpose of its inclusion into the arch
# collection of the Crossfire project. Permission was granted by e-mail:
#
#   Message-Id: <200801120526.m0C5Qeig012934@quartz1.mhtc.net>
#   Subject: Re: hf-table-engine-2-1200x1600.jpg
#   Date: Fri, 11 Jan 2008 23:00:54 -0600
#
object Machine.1
name Machine
title of the Empire
face machine.x11
move_block -all
no_pick 1
identified 1
blocksview 0
end
more
object Machine.2
face machine.x11
x 1
move_block all
end
more
object Machine.3
face machine.x11
x 2
move_block -all
blocksview 0
end
more
object Machine.4
face machine.x11
y 1
move_block all
blocksview 0
end
more
object Machine.5
face machine.x11
x 1
y 1
move_block all
blocksview 1
end
more
object Machine.6
face machine.x11
x 2
y 1
move_block all
blocksview 1
end
more
object Machine.7
face machine.x11
y 2
move_block all
blocksview 1
end
more
object Machine.8
face machine.x11
x 1
y 2
move_block all
blocksview 0
end
more
object Machine.9
face machine.x11
x 2
y 2
move_block all
blocksview 1
end
more
object Machine.A
face machine.x11
y 3
move_block -all
blocksview 0
end
more
object Machine.B
face machine.x11
x 1
y 3
move_block all
blocksview 0
end
more
object Machine.C
face machine.x11
x 2
y 3
move_block -all
blocksview 0
end

