object pirate_hat
name pirate hat
name_pl pirate hats
face pirate_hat.x11
cha -5
ac 1
nrof 1
type 34
resist_poison 15
resist_fear 50
materialname cloth
value 140
weight 1000
client_type 271
gen_sp_armour 1
body_head -1
end

