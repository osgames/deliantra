object smallicecube
name small icecube
name_pl small icecubes
face smallicecube.x11
type 79
resist_cold 100
materialname ice
weight 1500
client_type 8020
move_slow walk
move_slow_penalty 1
end

