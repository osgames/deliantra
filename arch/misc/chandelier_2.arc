object chandelier_2
anim
chandelier_2.x11
chandelier_2.x12
chandelier_2.x13
chandelier_2.x14
mina
name chandelier
face chandelier_2.x11
speed -0.30
glow_radius 4
no_pick 1
end

object chandelier_2_copper
anim
chandelier_2_copper.x11
chandelier_2_copper.x12
chandelier_2_copper.x13
chandelier_2_copper.x14
mina
name chandelier
face chandelier_2_copper.x11
speed -0.30
glow_radius 4
no_pick 1
end

