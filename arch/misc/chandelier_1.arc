object chandelier_1
name grand chandelier
face chandelier_1.x11
glow_radius 5
no_pick 1
end
more
object chandelier_2_1
name grand chandelier
face chandelier_1.x11
x 1
no_pick 1
end
more
object chandelier_3_1
name grand chandelier
face chandelier_1.x11
x 2
glow_radius 5
no_pick 1
end
more
object chandelier_4_1
name grand chandelier
face chandelier_1.x11
x 3
glow_radius 5
no_pick 1
end
more
object chandelier_5_1
name grand chandelier
face chandelier_1.x11
y 1
glow_radius 5
no_pick 1
end
more
object chandelier_6_1
name grand chandelier
face chandelier_1.x11
x 1
y 1
glow_radius 5
no_pick 1
end
more
object chandelier_7_1
name grand chandelier
face chandelier_1.x11
x 2
y 1
glow_radius 5
no_pick 1
end
more
object chandelier_8_1
name grand chandelier
face chandelier_1.x11
x 3
y 1
glow_radius 5
no_pick 1
end
more
object chandelier_9_1
name grand chandelier
face chandelier_1.x11
y 2
glow_radius 5
no_pick 1
end
more
object chandelier_10_1
name grand chandelier
face chandelier_1.x11
x 1
y 2
glow_radius 5
no_pick 1
end
more
object chandelier_11_1
name grand chandelier
face chandelier_1.x11
x 2
y 2
glow_radius 5
no_pick 1
end
more
object chandelier_12_1
name grand chandelier
face chandelier_1.x11
x 3
y 2
glow_radius 5
no_pick 1
end
more
object chandelier_13_1
name grand chandelier
face chandelier_1.x11
y 3
glow_radius 5
no_pick 1
end
more
object chandelier_14_1
name grand chandelier
face chandelier_1.x11
x 1
y 3
glow_radius 5
no_pick 1
end
more
object chandelier_15_1
name grand chandelier
face chandelier_1.x11
x 2
y 3
glow_radius 5
no_pick 1
end
more
object chandelier_16_1
name grand chandelier
face chandelier_1.x11
x 3
y 3
glow_radius 5
no_pick 1
end

