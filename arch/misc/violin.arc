object violin
anim
violin.x11
violin.x12
violin.x13
violin.x14
violin.x15
violin.x16
violin.x17
violin.x19
violin.x1A
violin.x1B
violin.x1C
violin.x1D
mina
inherit type_horn
name violin
name_pl violins
skill use magic item
face violin.x11
speed -0.3
nrof 1
level 1
value 20000
weight 1800
client_type 721
body_range -1
end

