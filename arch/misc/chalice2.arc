object chalice_bro
anim
chalice_bro.x11
chalice_bro.x11
chalice_bro.x11
chalice_bro.x12
chalice_bro.x13
chalice_bro.x14
mina
name chalice
name_pl chalices
face chalice_bro.x11
is_animated 1
speed 0.2
nrof 1
type 60
materialname bronze
value 700
weight 23700
client_type 2030
end

object chalice_cop
anim
chalice_cop.x11
chalice_cop.x11
chalice_cop.x11
chalice_cop.x12
chalice_cop.x13
chalice_cop.x14
mina
name chalice
name_pl chalices
face chalice_cop.x11
is_animated 1
speed 0.2
nrof 1
type 60
materialname copper
value 500
weight 23700
client_type 2030
end

object chalice_iro
anim
chalice_iro.x11
chalice_iro.x11
chalice_iro.x11
chalice_iro.x12
chalice_iro.x13
chalice_iro.x14
mina
name chalice
name_pl chalices
face chalice_iro.x11
is_animated 1
speed 0.2
nrof 1
type 60
materialname iron
value 200
weight 23700
client_type 2030
end

object chalice_plat
anim
chalice_plat.x11
chalice_plat.x11
chalice_plat.x11
chalice_plat.x12
chalice_plat.x13
chalice_plat.x14
mina
name chalice
name_pl chalices
face chalice_plat.x11
is_animated 1
speed 0.2
nrof 1
type 60
materialname platinum
value 5000
weight 23700
client_type 2030
end

object chalice_sil
anim
chalice_sil.x11
chalice_sil.x11
chalice_sil.x11
chalice_sil.x12
chalice_sil.x13
chalice_sil.x14
mina
name chalice
name_pl chalices
face chalice_sil.x11
is_animated 1
speed 0.2
nrof 1
type 60
materialname silver
value 1200
weight 23700
client_type 2030
end

object chalice_tin
anim
chalice_tin.x11
chalice_tin.x11
chalice_tin.x11
chalice_tin.x12
chalice_tin.x13
chalice_tin.x14
mina
name chalice
name_pl chalices
face chalice_tin.x11
is_animated 1
speed 0.2
nrof 1
type 60
materialname tin
value 50
weight 23700
client_type 2030
end

