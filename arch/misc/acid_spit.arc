object acid_spit
anim
acidspit.x11
acidspit.x11
acidspit.x21
acidspit.x31
acidspit.x41
acidspit.x51
acidspit.x61
acidspit.x71
acidspit.x81
mina
name acid spit
race spit_acid
face acidspit.x11
is_animated 0
food 100
dam 5
wc -10
nrof 1
type 13
attacktype 65
is_turnable 1
no_drop 1
end

