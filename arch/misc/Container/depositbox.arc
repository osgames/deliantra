object close_dbox
name close the deposit box
face close_dbox.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object depositbox
name deposit box
race gold and jewels
other_arch close_dbox
face depositbox.x11
type 122
materialname marble
container 150000
no_pick 1
identified 1
end

