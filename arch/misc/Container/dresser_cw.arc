object close_dresser2_cw
name in the dresser
face dresser2_cw.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object close_dresser_cw
name in the dresser
face dresser_cw.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object dresser2_cw
name dresser
name_pl dressers
other_arch close_dresser2_cw
face dresser2_cw.x11
type 122
materialname cherry
value 2400
weight 160000
container 150000
client_type 51
identified 1
end

object dresser_cw
name dresser
name_pl dressers
other_arch close_dresser_cw
face dresser_cw.x11
type 122
materialname cherry
value 2400
weight 160000
container 150000
client_type 51
identified 1
end

