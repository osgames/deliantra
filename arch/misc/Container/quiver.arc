object close_quiver
name close the quiver
face close_quiv.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object quiver
name quiver
name_pl quivers
title of arrows
race arrow
other_arch close_quiver
face quiver.x11
str 10
food 13
type 122
materialname cloth
value 50
weight 100
container 150000
client_type 60
identified 1
end

object quiver_bolt
name quiver
name_pl quivers
inherit quiver
title of bolts
race bolt
end

