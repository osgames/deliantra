object bag
name bag
name_pl bags
other_arch close_bag
face bag.x11
str 10
type 122
materialname cloth
value 50
weight 10
container 80000
client_type 51
identified 1
end

object close_bag
name close the bag
face close_bag.x11
type 121
no_pick 1
identified 1
no_drop 1
end

