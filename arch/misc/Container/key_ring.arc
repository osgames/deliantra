object close_key_ring
name close the key ring
face close_keys.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object key_ring
name key ring
name_pl key rings
race keys
other_arch close_key_ring
face key_ring.x11
str 10
type 122
materialname cloth
value 20
weight 100
container 50000
client_type 60
identified 1
end

