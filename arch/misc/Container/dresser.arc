object close_dresser
name in the dresser
face dresser.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object close_dresser2
name in the dresser
face dresser2.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object dresser
name dresser
name_pl dressers
other_arch close_dresser
face dresser.x11
type 122
materialname bronze
value 150
weight 160000
container 150000
client_type 51
identified 1
end

object dresser2
name dresser
name_pl dressers
other_arch close_dresser2
face dresser2.x11
type 122
materialname bronze
value 150
weight 160000
container 150000
client_type 51
identified 1
end

