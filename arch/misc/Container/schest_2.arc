object close_schest
name close the chest
face schest.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object schest_2
name chest
name_pl chests
other_arch close_schest
face schest.x11
type 122
value 200
weight 55000
container 175000
client_type 51
identified 1
end

