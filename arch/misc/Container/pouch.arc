object close_pouch
name close the pouch
face close_pouc.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object pouch
name pouch
name_pl pouches
race gold and jewels
other_arch close_pouch
face pouch.x11
str 10
type 122
materialname cloth
value 40
weight 100
container 150000
client_type 55
identified 1
end

