object attache
name attache case
race scrolls
other_arch close_attache
face attache.x11
str 10
type 122
materialname cloth
value 150
weight 900
container 14000
identified 1
end

object close_attache
name close the attache case
face close_attache.x11
type 121
no_pick 1
identified 1
no_drop 1
end

