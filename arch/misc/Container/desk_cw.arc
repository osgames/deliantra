object close_desk_cw
name close the desk
face desk_cw.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object desk_cw
name desk
name_pl desks
other_arch close_desk_cw
face desk_cw.x11
str 10
type 122
materialname cherry
value 2100
weight 120000
container 80000
client_type 51
identified 1
end

