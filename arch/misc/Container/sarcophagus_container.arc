object close_sarcophagus_container
name close the sarcophagus
face sarcophagus.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object sarcophagus_container
name sarcophagus
name_pl sarcophagi
face sarcophagus.x11
hp 1
nrof 0
type 4
materialname stone
value 50
weight 800000
client_type 51
no_pick 1
identified 1
end

object sarcophagus_container2
name sarcophagus
name_pl sarcophagi
other_arch close_sarcophagus_container
face sarcophagus.x11
type 122
materialname stone
value 50
weight 800000
container 150000
client_type 51
no_pick 1
identified 1
end

