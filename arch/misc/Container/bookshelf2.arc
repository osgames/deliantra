object bookshelf2
name bookshelf
name_pl bookshelves
other_arch close_shelf2
face bookshelf2.x11
type 122
materialname bronze
value 50
weight 80000
container 150000
client_type 51
identified 1
end

object close_shelf2
name in the shelf
face bookshelf2.x11
type 121
no_pick 1
identified 1
no_drop 1
end

