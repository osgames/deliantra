object close_present_box_1
name close the box
face present_box_1.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object close_present_box_2
name close the box
face present_box_2.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object close_present_box_3
name close the box
face present_box_3.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object close_present_box_4
name close the box
face present_box_4.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object close_present_box_5
name close the box
face present_box_5.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object close_present_box_6
name close the box
face present_box_6.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object present_box_1
name box
name_pl boxes
other_arch close_present_box_1
face present_box_1.x11
type 122
materialname paper
value 70
weight 1000
container 50000
client_type 51
identified 1
end

object present_box_2
name box
name_pl boxes
other_arch close_present_box_2
face present_box_2.x11
type 122
materialname paper
value 70
weight 1000
container 50000
client_type 51
identified 1
end

object present_box_3
name box
name_pl boxes
other_arch close_present_box_3
face present_box_3.x11
type 122
materialname paper
value 70
weight 1000
container 50000
client_type 51
identified 1
end

object present_box_4
name box
name_pl boxes
other_arch close_present_box_4
face present_box_4.x11
type 122
materialname paper
value 70
weight 1000
container 50000
client_type 51
identified 1
end

object present_box_5
name box
name_pl boxes
other_arch close_present_box_5
face present_box_5.x11
type 122
materialname paper
value 70
weight 1000
container 50000
client_type 51
identified 1
end

object present_box_6
name box
name_pl boxes
other_arch close_present_box_6
face present_box_6.x11
type 122
materialname paper
value 70
weight 1000
container 50000
client_type 51
identified 1
end

