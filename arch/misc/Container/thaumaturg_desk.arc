object thaumaturg_desk
name thaumaturgist's desk
name_pl thaumaturgist's desks
skill thaumaturgy
face thaumaturg_desk.x11
str 0
type 122
materialname wood
value 18000
weight 80000
container 800000
client_type 51
no_pick 1
is_cauldron 1
end

