object close_sack
name close the sack
face close_sack.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object sack
name sack
name_pl sacks
other_arch close_sack
face sack.x11
str 10
type 122
materialname cloth
value 50
weight 100
container 150000
client_type 51
identified 1
end

