object close_desk
name close the desk
face desk.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object close_wizdesk
name close the desk
face wizdesk.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object desk
name desk
name_pl desks
other_arch close_desk
face desk.x11
str 10
type 122
materialname bronze
value 150
weight 120000
container 80000
client_type 51
identified 1
end

object wizdesk
name desk
name_pl desks
other_arch close_wizdesk
face wizdesk.x11
str 10
type 122
materialname bronze
value 190
weight 120000
container 80000
client_type 51
identified 1
end

