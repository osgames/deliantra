object stove
anim
stove.x11
stove.x12
stove.x13
mina
name_pl stoves
skill woodsman
face stove.x11
str 0
speed -0.2
type 122
materialname marble
value 18000
weight 80000
glow_radius 1
container 800000
client_type 51
no_pick 1
is_cauldron 1
end

