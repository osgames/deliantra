object close_mail
name close the mailbox
face closemail.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object mailbox
name mailbox
other_arch close_mail
face mailbox.x11
type 122
materialname bronze
value 50
weight 4000
container 1500
identified 1
end

