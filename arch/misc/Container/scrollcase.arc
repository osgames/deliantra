object close_scrollcase
name close the scroll case
face close_rsack.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object scrollcase
name scroll case
race scrolls
other_arch close_scrollcase
face r_sack.x11
str 10
type 122
materialname cloth
value 50
weight 500
container 7000
identified 1
end

