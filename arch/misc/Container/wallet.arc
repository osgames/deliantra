object close_wallet1
name close the wallet
face wallet1.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object close_wallet2
name close the wallet
face wallet2.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object close_wallet3
name close the wallet
face wallet3.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object close_wallet4
name close the wallet
face wallet4.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object wallet1
name wallet
name_pl wallets
other_arch close_wallet1
face wallet1.x11
str 10
type 122
materialname leather
value 50
weight 75
container 5000
client_type 60
identified 1
end

object wallet2
name wallet
name_pl wallets
other_arch close_wallet2
face wallet2.x11
str 10
type 122
materialname black leather
value 100
weight 75
container 5000
client_type 60
identified 1
end

object wallet3
name wallet
name_pl wallets
other_arch close_wallet3
face wallet3.x11
str 10
type 122
materialname white leather
value 100
weight 75
container 5000
client_type 60
identified 1
end

object wallet4
name wallet
name_pl wallets
other_arch close_wallet4
face wallet4.x11
str 10
type 122
materialname alligator skin
value 500
weight 75
container 5000
client_type 60
identified 1
end

