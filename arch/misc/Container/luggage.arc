object luggage
anim
luggage.x11
luggage.x12
luggage.x13
luggage.x14
luggage.x15
luggage.x16
luggage.x17
luggage.x18
mina
name The Luggage
name_pl The Luggages
face luggage.x11
str 50
speed 0.5
type 122
materialname iron
value 100000
weight 20000
container 500000
client_type 51
identified 1
end

