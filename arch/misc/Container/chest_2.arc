object chest_2
name chest
name_pl chests
other_arch close_chest
face chest_1.x11
type 122
materialname bronze
value 50
weight 40000
container 150000
client_type 51
identified 1
end

object close_chest
name close the chest
face closechest.x11
type 121
no_pick 1
identified 1
no_drop 1
end

