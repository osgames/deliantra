object bookshelf
name bookshelf
name_pl bookshelves
other_arch close_shelf
face bookshelf.x11
type 122
materialname bronze
value 50
weight 80000
container 150000
client_type 51
identified 1
end

object bookshelf_bottom
name bookshelf
name_pl bookshelves
other_arch bookshelf_bottom_close
face bookshelf_bottom.x11
type 122
materialname bronze
value 50
weight 80000
container 150000
client_type 51
identified 1
end

object bookshelf_bottom_close
name in the shelf
face bookshelf_bottom.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object bookshelf_left
name bookshelf
name_pl bookshelves
other_arch bookshelf_left_close
face bookshelf_left.x11
type 122
materialname bronze
value 50
weight 80000
container 150000
client_type 51
identified 1
end

object bookshelf_left_close
name in the shelf
face bookshelf_left.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object bookshelf_right
name bookshelf
name_pl bookshelves
other_arch bookshelf_right_close
face bookshelf_right.x11
type 122
materialname bronze
value 50
weight 80000
container 150000
client_type 51
identified 1
end

object bookshelf_right_close
name in the shelf
face bookshelf_right.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object close_shelf
name in the shelf
face bookshelf.x11
type 121
no_pick 1
identified 1
no_drop 1
end

