object close_rsack
name close the rucksack
face close_rsack.x11
type 121
no_pick 1
identified 1
no_drop 1
end

object r_sack
name rucksack
name_pl rucksacks
other_arch close_rsack
face r_sack.x11
str 15
type 122
materialname cloth
value 150
weight 100
container 550000
client_type 51
identified 1
end

