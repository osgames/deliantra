object bad_cauldron
name cracked cauldron
name_pl cracked cauldrons
other_arch cauldron_open
face cauldron.x11
str -80
type 122
materialname iron
value 5000
weight 20000
container 800000
client_type 51
end

object cauldron
name_pl cauldrons
skill alchemy
other_arch cauldron_open
face cauldron.x11
str 0
type 122
materialname iron
value 18000
weight 80000
container 800000
client_type 51
is_cauldron 1
end

object cauldron_open
name close the cauldron
face cauldron_open.x11
type 121
no_pick 1
no_drop 1
end

