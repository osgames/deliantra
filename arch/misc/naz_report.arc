object naz_report
name Nazgul Report
name_pl Nazgul Reports
skill literacy
msg
A Nazgul is somewhat like a grimreaper.
However, while grimreapers are vulnerable to
magic, Nazgul seem to be immune to magic.
Nazgul have a drain attack.
Nazgul cannot be attacked physically,
except in a Special Case:
* this scroll is broken, *
*so I cannot continue to read *
endmsg
face naz_report.x11
nrof 1
type 8
subtype 34
materialname paper
value 20000
weight 300
client_type 1042
end

