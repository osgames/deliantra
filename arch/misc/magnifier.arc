object magnifier
inherit type_weapon
name magnifying glass
name_pl magnifying glasses
slaying ant
skill one handed weapons
face magnifier.x11
dex 1
dam 1
nrof 1
materialname glass
value 500
weight 2000
last_sp 2
client_type 145
body_arm -1
body_combat -1
end

