object worldmap
name_pl worldmaps
attach [["item_worldmap"]]
msg
This is you sturdy, inexact, outdated, but highly useful avdenturer's map of the world. Scores
of Explorers gave their knowledge (and sometimes their life) for this map, so treat it
with respect.

In its most basic form, it is just a map. But it is said that with skill and knowledge
it can be much, much, more...
endmsg
face worldmap.x11
nrof 0
type 79
materialname paper
value 3750
weight 600
client_type 1041
end

