object gostone_b
name black stone
name_pl black stones
face gostone_b.x11
nrof 1
value 1
weight 1000
end

object gostone_w1
name white stone
name_pl white stones
face gostone_w1.x11
nrof 1
value 1
weight 1000
end

object gostone_w2
name white stone
name_pl white stones
face gostone_w2.x11
nrof 1
value 1
weight 1000
end

object gostone_w3
name white stone
name_pl white stones
face gostone_w3.x11
nrof 1
value 1
weight 1000
end

