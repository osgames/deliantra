object thewheel_short_ccwise-back
anim
thewheel_short.x11
thewheel_short.x11
thewheel_short.x11
thewheel_short.x11
thewheel_short.x11
thewheel_short.x12
thewheel_short.x13
thewheel_short.x14
thewheel_short.x15
thewheel_short.x15
thewheel_short.x15
thewheel_short.x14
thewheel_short.x13
thewheel_short.x13
thewheel_short.x12
thewheel_short.x11
thewheel_short.x11
thewheel_short.x11
mina
name the wheel
face thewheel_short.x11
is_animated 1
speed -0.1
move_block all
no_pick 1
end

object thewheel_short_static
name the wheel
face thewheel_short.x11
move_block all
no_pick 1
end

object thewheel_tall_ccwise-back
anim
thewheel_tall.x11
thewheel_tall.x11
thewheel_tall.x11
thewheel_tall.x11
thewheel_tall.x11
thewheel_tall.x12
thewheel_tall.x13
thewheel_tall.x14
thewheel_tall.x15
thewheel_tall.x15
thewheel_tall.x15
thewheel_tall.x14
thewheel_tall.x13
thewheel_tall.x13
thewheel_tall.x12
thewheel_tall.x11
thewheel_tall.x11
thewheel_tall.x11
mina
name the wheel
face thewheel_tall.x11
is_animated 1
speed -0.1
move_block all
no_pick 1
end
more
object thewheel_tall_2_ccwise-back
anim
thewheel_tall.x11
thewheel_tall.x11
thewheel_tall.x11
thewheel_tall.x11
thewheel_tall.x11
thewheel_tall.x12
thewheel_tall.x13
thewheel_tall.x14
thewheel_tall.x15
thewheel_tall.x15
thewheel_tall.x15
thewheel_tall.x14
thewheel_tall.x13
thewheel_tall.x13
thewheel_tall.x12
thewheel_tall.x11
thewheel_tall.x11
thewheel_tall.x11
mina
name the wheel
face thewheel_tall.x11
is_animated 1
y 1
speed -0.1
no_pick 1
end

object thewheel_tall_static
name the wheel
face thewheel_tall.x11
move_block all
no_pick 1
end
more
object thewheel_tall_2_static
name the wheel
face thewheel_tall.x11
y 1
no_pick 1
end

