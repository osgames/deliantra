object platter1_bro
name platter
name_pl platters
face platter1_bro.x11
nrof 1
type 60
materialname bronze
value 5400
weight 40000
client_type 2030
end

object platter1_cop
name platter
name_pl platters
face platter1_cop.x11
nrof 1
type 60
materialname copper
value 4000
weight 40000
client_type 2030
end

object platter1_gol
name platter
name_pl platters
face platter1_gol.x11
nrof 1
type 60
materialname gold
value 80000
weight 40000
client_type 2030
end

object platter1_iro
name platter
name_pl platters
face platter1_iro.x11
nrof 1
type 60
materialname iron
value 800
weight 40000
client_type 2030
end

object platter1_plat
name platter
name_pl platters
face platter1_plat.x11
nrof 1
type 60
materialname platinum
value 400000
weight 40000
client_type 2030
end

object platter1_sil
name platter
name_pl platters
face platter1_sil.x11
nrof 1
type 60
materialname silver
value 8000
weight 40000
client_type 2030
end

object platter1_tin
name platter
name_pl platters
face platter1_tin.x11
nrof 1
type 60
materialname tin
value 400
weight 40000
client_type 2030
end

