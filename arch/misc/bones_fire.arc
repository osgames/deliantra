object bones2_fire
name bones
name_pl bones
skill one handed weapons
face bones2_fire.x11
dam 2
nrof 1
type 15
materialname wood
value 0
weight 2000
last_sp 19
client_type 8002
body_arm -1
body_combat -1
end

object bones3_fire
name bones
name_pl bones
skill one handed weapons
face bones3_fire.x11
dam 2
nrof 1
type 15
materialname wood
value 0
weight 1000
last_sp 19
client_type 8002
body_arm -1
body_combat -1
end

object bones5_fire
name bones
name_pl bones
skill one handed weapons
face bones5_fire.x11
dam 2
nrof 1
type 15
materialname wood
value 0
weight 11000
last_sp 19
client_type 8002
body_arm -1
body_combat -1
end

