object evilpentagram
name pentagram
face evilpenta.x11
no_pick 1
end

object evilpentagramdarkgrey
name pentagram
face evilpentadarkgrey.x11
no_pick 1
end

object evilpentagramdarkred
name pentagram
face evilpentadarkred.x11
no_pick 1
end

object evilpentagramgrey
name pentagram
face evilpentagrey.x11
no_pick 1
end

object evilpentagramred
name pentagram
face evilpentared.x11
no_pick 1
end

