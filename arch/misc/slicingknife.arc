object b_slicingknife
name slicing knife
name_pl slicing knives
slaying cut
face b_slicingknife.x11
nrof 1
type 117
materialname bronze
value 7
weight 1000
client_type 8021
end

object slicingknife
name slicing knife
name_pl slicing knives
slaying cut
face slicingknife.x11
nrof 1
type 117
materialname iron
value 14
weight 1000
client_type 8021
end

