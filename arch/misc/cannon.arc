object cannon
name cannon
other_arch spell_shell
face cannon_0.x11
speed -0.4
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object cannon_1
name cannon
other_arch spell_shell
face cannon_0.x11
sp 1
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object cannon_2
name cannon
other_arch spell_shell
face cannon_0.x11
sp 2
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object cannon_3
name cannon
other_arch spell_shell
face cannon_0.x11
sp 3
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object cannon_4
name cannon
other_arch spell_shell
face cannon_0.x11
sp 4
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object cannon_5
name cannon
other_arch spell_shell
face cannon_0.x11
sp 5
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object cannon_6
name cannon
other_arch spell_shell
face cannon_0.x11
sp 6
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object cannon_7
name cannon
other_arch spell_shell
face cannon_7.x11
sp 7
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

object cannon_8
name cannon
other_arch spell_shell
face cannon_0.x11
sp 8
speed -0.1
level 1
type 62
move_block all
blocksview 1
activate_on_push 1
activate_on_release 1
end

