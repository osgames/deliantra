object potion_improve
name improvement potion
name_pl improvement potions
face potionimp.x11
nrof 1
level 1
type 5
attacktype 1048576
materialname glass
value 5200
weight 1800
client_type 651
need_an 1
on_use_yield potion_empty
end

