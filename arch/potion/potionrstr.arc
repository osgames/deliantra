object potion_restoration
name potion of life
name_pl potions of life
face potion_heal2.x11
nrof 1
level 1
type 5
attacktype 65536
value 6000
weight 1500
client_type 651
on_use_yield potion_empty
end

