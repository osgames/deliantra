object potion_heal
name healing potion
name_pl healing potions
face potionhea.x11
sp 35
nrof 1
level 1
type 5
materialname glass
value 50
weight 1800
client_type 651
on_use_yield potion_empty
end

