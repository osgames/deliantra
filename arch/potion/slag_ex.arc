object slag_ex
name slag ex(tm) bottle
name_pl slag ex(tm) bottles
msg
It removes any kind of slag or other stuff from
a cauldron or similiar devices. It will be like new!
H<Just apply it while standing over the cauldron, and it will remove anything
in the cauldron, including any sticky slag.>
endmsg
face slag_ex.x11
nrof 1
type 79
subtype 1
materialname glass
value 2000
weight 1500
client_type 651
on_use_yield slag_ex_empty
end

object slag_ex_empty
inherit type_weapon
name empty slag ex(tm) bottle
name_pl empty slag ex(tm) bottles
skill one handed weapons
face slag_ex_empty.x11
dam 1
nrof 1
attacktype 1
materialname glass
value 3
weight 700
last_sp 10
weapontype 8
client_type 611
body_arm -1
body_combat -1
end

