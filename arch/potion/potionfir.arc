object potion_fire
name potion of cold resistance
name_pl potions of cold resistance
face potionfir.x11
nrof 1
level 1
type 5
resist_cold 90
materialname glass
value 5200
weight 1800
client_type 651
on_use_yield potion_empty
end

object potion_fire2
name potion of frost resistance
name_pl potions of frost resistance
face potionfir.x11
nrof 1
level 1
type 5
resist_cold 95
materialname glass
value 28000
weight 1800
client_type 651
on_use_yield potion_empty
end

