object potion_magic
name magic power potion
name_pl magic power potions
face potionmag.x11
sp 67
nrof 1
level 1
type 5
materialname glass
value 15
weight 1800
client_type 651
on_use_yield potion_empty
end

