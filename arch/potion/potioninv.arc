object potion_aethereality
name potion of aethereality
name_pl potions of aethereality
face potion_aether.x11
nrof 1
level 1
type 5
resist_physical 95
materialname glass
value 28000
weight 2000
client_type 651
on_use_yield potion_empty
end

# for compatibility
object potion_invulnerability
name potion of shielding
name_pl potions of shielding
face potioninv.x11
nrof 1
level 1
type 5
resist_physical 90
materialname glass
value 10000
weight 2000
client_type 651
on_use_yield potion_empty
end

object potion_shielding
name potion of shielding
name_pl potions of shielding
face potioninv.x11
nrof 1
level 1
type 5
resist_physical 90
materialname glass
value 10000
weight 2000
client_type 651
on_use_yield potion_empty
end

