object vial_blue
name vial
name_pl vials
face vial_blue.x11
food 5
nrof 1
type 54
materialname organic
value 4
weight 150
client_type 611
on_use_yield vial_empty
end

object vial_cyan
name vial
name_pl vials
face vial_cyan.x11
food 5
nrof 1
type 54
materialname organic
value 4
weight 150
client_type 611
on_use_yield vial_empty
end

object vial_green
name vial
name_pl vials
face vial_green.x11
food 5
nrof 1
type 54
materialname organic
value 4
weight 150
client_type 611
on_use_yield vial_empty
end

object vial_magenta
name vial
name_pl vials
face vial_magenta.x11
food 5
nrof 1
type 54
materialname organic
value 4
weight 150
client_type 611
on_use_yield vial_empty
end

object vial_red
name vial
name_pl vials
face vial_red.x11
food 5
nrof 1
type 54
materialname organic
value 4
weight 150
client_type 611
on_use_yield vial_empty
end

object vial_water
name vial
name_pl vials
face vial_water.x11
food 5
nrof 1
type 54
materialname organic
value 4
weight 150
client_type 611
on_use_yield vial_empty
end

object vial_yellow
name vial
name_pl vials
face vial_yellow.x11
food 5
nrof 1
type 54
materialname organic
value 4
weight 150
client_type 611
on_use_yield vial_empty
end

