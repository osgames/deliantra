object potionpow
name potion of power
name_pl potions of power
face potionpow.x11
pow 1
nrof 1
level 1
type 5
materialname glass
value 13500
weight 1500
client_type 651
on_use_yield potion_empty
end

