object potionint
name potion of intelligence
name_pl potions of intelligence
face potionint.x11
int 1
nrof 1
level 1
type 5
materialname glass
value 13000
weight 1500
client_type 651
on_use_yield potion_empty
end

