object potionstr
name potion of strength
name_pl potions of strength
face potionstr.x11
str 1
nrof 1
level 1
type 5
materialname glass
value 15000
weight 1500
client_type 651
on_use_yield potion_empty
end

