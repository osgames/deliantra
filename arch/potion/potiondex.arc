object potiondex
name potion of dexterity
name_pl potions of dexterity
face potiondex.x11
dex 1
nrof 1
level 1
type 5
materialname glass
value 14000
weight 1500
client_type 651
on_use_yield potion_empty
end

