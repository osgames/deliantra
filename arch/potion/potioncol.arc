object potion_cold
name potion of fire resistance
name_pl potions of fire resistance
face potioncol.x11
nrof 1
level 1
type 5
resist_fire 90
materialname glass
value 5800
weight 1800
client_type 651
on_use_yield potion_empty
end

object potion_cold2
name potion of lava resistance
name_pl potions of lava resistance
face potioncol.x11
nrof 1
level 1
type 5
resist_fire 95
materialname glass
value 28000
weight 1800
client_type 651
on_use_yield potion_empty
end

