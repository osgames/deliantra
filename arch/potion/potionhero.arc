object potion_heroism
name potion of heroism
name_pl potions of heroism
face potionhero.x11
sp 80
nrof 1
level 1
type 5
materialname glass
value 500
weight 1000
client_type 651
on_use_yield potion_empty
end

