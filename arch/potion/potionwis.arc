object potionwis
name potion of wisdom
name_pl potions of wisdom
face potionwis.x11
wis 1
nrof 1
level 1
type 5
materialname glass
value 12000
weight 1500
client_type 651
on_use_yield potion_empty
end

