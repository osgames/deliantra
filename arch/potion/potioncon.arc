object potioncon
name potion of constitution
name_pl potions of constitution
face potioncon.x11
con 1
nrof 1
level 1
type 5
materialname glass
value 14500
weight 1500
client_type 651
on_use_yield potion_empty
end

