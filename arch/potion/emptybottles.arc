object boozebottle_empty
inherit type_weapon
name empty bottle
name_pl empty bottles
skill one handed weapons
face boozebottle_empty.x11
dam 1
nrof 1
attacktype 1
materialname glass
value 3
weight 1000
last_sp 10
weapontype 8
client_type 611
body_arm -1
body_combat -1
end

object coffee_empty
inherit type_weapon
name empty cup
name_pl empty cups
skill one handed weapons
face coffee_empty.x11
dam 1
nrof 1
attacktype 1
materialname glass
value 1
weight 50
last_sp 10
weapontype 8
client_type 611
body_arm -1
body_combat -1
end

object potion_empty
inherit type_weapon
name empty bottle
name_pl empty bottles
skill one handed weapons
face potion_empty.x11
dam 1
nrof 1
attacktype 1
materialname glass
value 3
weight 700
last_sp 10
weapontype 8
client_type 611
body_arm -1
body_combat -1
end

object vial_empty
inherit type_weapon
name empty vial
name_pl empty vials
skill one handed weapons
face vial_empty.x11
dam 1
nrof 1
attacktype 1
materialname glass
value 3
weight 70
last_sp 10
weapontype 8
client_type 611
body_arm -1
body_combat -1
end

object w_glass_empty
inherit type_weapon
name empty glass
name_pl empty glasses
skill one handed weapons
face w_glass_empty.x11
dam 1
nrof 1
attacktype 1
materialname glass
value 1
weight 500
last_sp 10
weapontype 8
client_type 611
body_arm -1
body_combat -1
end

object wbottle_empty
inherit type_weapon
name empty bottle
name_pl empty bottles
skill one handed weapons
face wbottle_empty.x11
dam 1
nrof 1
attacktype 1
materialname glass
value 3
weight 700
last_sp 10
weapontype 8
client_type 611
body_arm -1
body_combat -1
end

object winebottle_empty
inherit type_weapon
name empty bottle
name_pl empty bottles
skill one handed weapons
face winebottle_empty.x11
dam 1
nrof 1
attacktype 1
materialname glass
value 3
weight 600
last_sp 10
weapontype 8
client_type 611
body_arm -1
body_combat -1
end

