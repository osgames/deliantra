object trap_diseased_needle
anim
needle.x11
needle.x11
mina
name diseased needle
msg
You are pricked by a needle!
endmsg
face needle.x11
is_animated 0
cha 20
hp 1
dam 10
speed 1
level 1
type 96
attacktype 1
invisible 1
randomitems diseased_needle
move_on walk
no_pick 1
end

