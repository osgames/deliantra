object smallpox
name smallpox
race goblin,human,troll,giant,dwarf
msg
You have a nasty rash all over you.  Are those
pustules?
endmsg
str -1
dex -1
con -4
hp 1
maxhp 5
sp 1
maxsp 0
maxgrace 12
exp 10000
food -1
dam -10
wc 20
ac 0
speed -0.08
level 20
type 107
attacktype 1048576
invisible 1
magic -3
last_sp 0
last_grace 1
last_eat 0
end

