object typhoid
name typhoid
race human,goblin,giant,troll
msg
You feel feverish.  Your muscles spasm
oddly....  Breathing is difficult.
endmsg
str -3
dex -3
con -4
hp 1
maxhp 5
sp 1
maxsp 0
maxgrace 16
exp 1000
food -1
dam -7
wc 25
ac 0
speed -0.05
level 12
type 107
attacktype 1048576
invisible 1
magic -1
last_sp 30
last_grace 1
last_eat 0
end

