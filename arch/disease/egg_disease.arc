object chicken_egg
name egg
face ant_egg.x11
food 10
type 72
weight 20
end

object egg_disease
name egg disease
race *
msg
Buck, buck, buck, buck!  You lay an EGG!!
You feel ridiculous.
endmsg
other_arch chicken_egg
maxhp 1
maxsp 0
maxgrace 5
exp 100
dam 0
wc 0
speed -0.001
level 5
type 107
attacktype 1
invisible 1
magic 0
last_sp 0
last_eat 0
end

