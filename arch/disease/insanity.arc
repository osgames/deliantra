object insanity
name insanity
race *
msg
You start gibbering incoherently.  You
forget where you are and what you were doing.
endmsg
wis -1
int -1
maxhp 1
maxsp 0
maxgrace -1
exp 10000
food -1
dam 1
wc 10
ac 3
speed -0.001
level 15
type 107
attacktype 32
invisible 1
magic 0
last_sp 0
last_eat 0
end

