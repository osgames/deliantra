object creeping_frost
name creeping frost
title You freeze
race *
msg
Frost creeps over your body!
endmsg
face fireball.x11
hp 1
maxhp 0
sp 1
maxsp 0
maxgrace 10
exp 10000
food -1
dam -10
wc 3
ac 0
speed -0.1
level 10
type 107
attacktype 16
magic 0
last_heal 1
last_sp 50
last_eat 0
no_pick 1
no_drop 1
end

object creeping_frost_immunity
name creeping frost
level 127
type 98
invisible 1
no_pick 1
no_drop 1
end

