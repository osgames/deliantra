object puddle_of_saliva
name puddle of froth and saliva
face residue.x11
food 2
type 72
weight 2
end

object rabies
name rabies
race *
msg
You notice everyone is looking at you with
evil intent. You must kill them! You begin
to salivate.
endmsg
other_arch puddle_of_saliva
str -2
dex -2
cha -10
hp 1
maxhp 3
sp 1
maxsp 2
maxgrace -1
exp 5000
food -1
dam 2
wc 10
ac 2
speed -0.01
level 12
type 107
attacktype 1048608
invisible 1
magic 1
last_sp 50
last_eat 0
end

