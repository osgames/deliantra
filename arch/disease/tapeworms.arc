object tapeworms
name tapeworms
race *
msg
You feel more hungry than usual.  You also
feel an urge to refer to yourself in the
plural.
endmsg
maxhp 1
maxsp 0
maxgrace -1
exp 100
food -1
wc 1
ac 0
speed -0.001
level 5
type 107
attacktype 1
invisible 1
magic 1
last_sp 0
last_eat 1
end

