object disease_cold
name cold
race *
msg
You develop a sniffle.
endmsg
con -1
cha -1
hp 1
maxhp 10
sp 1
maxsp 0
maxgrace 6
exp 100
food -1
dam 1
wc 20
ac 0
speed -0.05
level 2
type 107
attacktype 1048576
invisible 1
magic 3
last_sp 50
last_grace 1
last_eat 0
end

