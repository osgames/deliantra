object flu
name flu
race *
msg
You have aches and fever, and you feel nauseous.
endmsg
str -1
dex -1
con -1
wis -1
cha -1
int -1
hp 1
maxhp 10
sp 1
maxsp 0
maxgrace 6
exp 100
food -1
dam 2
wc 20
ac 0
speed -0.05
level 2
type 107
attacktype 1048576
invisible 1
magic 3
last_sp 50
last_grace 1
last_eat 0
end

