object immolation
name immolation
title You set fire to
race *,undead
msg
You burn!
endmsg
other_arch burnout
face fireball.x11
hp 1
maxhp 0
sp 1
maxsp 0
maxgrace 11
exp 10000
food -1
dam -10
wc 3
ac 0
speed -0.1
level 10
type 107
attacktype 4
invisible 1
magic 0
last_heal 1
last_sp 50
last_grace 10
last_eat 0
no_pick 1
startequip 1
no_drop 1
end

object immolation_immunity
name immolation
level 127
type 98
invisible 1
no_pick 1
startequip 1
no_drop 1
end

