object leprosy
name leprosy
race *
msg
Splotches are spreading around your body.
You feel disgusted with yourself.  A piece
of skin flakes off and falls to the ground.
endmsg
other_arch leprous_skin
str -1
dex -1
con -1
cha -2
hp 1
maxhp 1
sp 1
maxsp 0
maxgrace -1
exp 1000
food -1
dam 1
wc 10
ac 5
speed -0.005
level 5
type 107
attacktype 1048576
invisible 1
magic 0
last_sp 0
last_eat 0
end

object leprous_skin
name leprous flake of skin
face skin.x11
food 5
type 72
materialname organic
weight 7
end

