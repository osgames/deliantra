object tooth_decay
name tooth decay
race *
msg
A tooth wiggles loose and falls to the
ground.  You should brush more.  Have I
mentioned that your breath is disgusting,
too?
endmsg
other_arch tooth
cha -1
maxhp 1
maxsp 0
maxgrace 24
exp 100
wc 1
ac 0
speed -0.005
level 5
type 107
attacktype 1
invisible 1
magic 1
last_sp 0
last_eat 0
end

