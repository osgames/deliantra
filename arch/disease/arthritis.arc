object arthritis
name arthritis
race *
msg
Your joints are swollen.  You feel weak and
less dextrous.
endmsg
str -1
dex -2
maxhp 1
maxsp 0
maxgrace -1
exp 10000
food -1
wc 5
ac 1
speed -0.001
level 7
type 107
attacktype 1048576
invisible 1
magic 0
last_sp 0
last_eat 0
end

