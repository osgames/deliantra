object warts
name warts
race *
msg
You have warts.  They are ugly and annoying.
endmsg
cha -1
maxhp 1
maxsp 0
maxgrace 50
exp 100
wc 1
ac 0
speed -0.001
level 5
type 107
attacktype 1
invisible 1
magic 1
last_sp 0
last_eat 0
end

