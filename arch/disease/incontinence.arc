object incontinence
name lack of bladder control
race *
msg
You can't control your bladder.  You have a
messy accident.  Yuck!
endmsg
other_arch reeking_urine
maxhp 1
maxsp 0
maxgrace 20
exp 100
dam 0
wc 0
speed -0.001
level 5
type 107
attacktype 1
invisible 1
magic 0
last_sp 0
last_eat 0
end

object reeking_urine
name reeking puddle of urine
face residue.x11
food 1
type 72
weight 20
end

