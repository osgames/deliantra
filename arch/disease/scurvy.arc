object scurvy
name scurvy
race *
msg
You spit out a tooth.  Better increase that
dietary vitamin C!
endmsg
other_arch tooth
con -1
maxhp 1
maxsp 0
maxgrace 15
exp 100
dam 0
wc 0
speed -0.001
level 5
type 107
attacktype 1
invisible 1
magic 0
last_sp 0
last_eat 0
end

