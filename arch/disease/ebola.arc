object ebola
name Red Death
race *
msg
Blood leaks out of your eyes and your pores!
endmsg
str -3
dex -2
con -4
wis -2
cha -7
int -4
hp 1
maxhp 5
sp 1
maxsp 0
maxgrace 15
exp 10000
food -1
dam -10
wc 30
ac 0
speed -0.05
level 10
type 107
attacktype 1048576
invisible 1
magic -1
last_sp 50
last_grace 5
last_eat 0
end

