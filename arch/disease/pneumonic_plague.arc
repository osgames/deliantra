object pneumonic_plague
name pneumonic plague
race *
msg
You cough up some nasty green phlegm.
endmsg
str -3
dex -3
con -4
cha -4
hp 1
maxhp 7
sp 1
maxsp 0
maxgrace 15
exp 10000
food -1
dam -11
wc 20
ac 0
speed -0.05
level 15
type 107
attacktype 1048576
invisible 1
magic 4
last_sp 50
last_grace 1
last_eat 0
end

