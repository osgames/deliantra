object anthrax
name anthrax
race animal
msg
You feel feverish.  Your muscles spasm
oddly....  Breathing is difficult.
endmsg
str -3
dex -3
con -4
hp 1
maxhp 100
sp 1
maxsp 0
maxgrace 23
exp 1000
food -1
dam -5
wc 5
ac 0
speed -0.05
level 12
type 107
attacktype 1048576
invisible 1
magic 1
last_sp 50
last_eat 0
end

