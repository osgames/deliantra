object high_boots_b
name jack boots
name_pl jack boots
face high_boots_b.x11
cha 1
ac 1
nrof 1
type 99
resist_physical 4
resist_death 1
materialname leather
value 1700
weight 6500
client_type 291
gen_sp_armour 4
body_foot -2
end

object high_boots_b_d
name jack boots
name_pl jack boots
title of death
face high_boots_b.x11
cha 1
ac 1
nrof 1
type 99
resist_physical 25
resist_death 25
materialname steel
value 30000
weight 13000
client_type 291
gen_sp_armour 4
body_foot -2
end

