object high_boots_w
name jack boots
name_pl jack boots
face high_boots_w.x11
cha 1
ac 1
nrof 1
type 99
resist_physical 4
resist_blind 1
materialname leather
value 2000
weight 6500
client_type 291
gen_sp_armour 4
body_foot -2
end

object high_boots_w_l
name jack boots
name_pl jack boots
title of light
face high_boots_w.x11
cha 1
ac 1
nrof 1
type 99
resist_physical 25
resist_blind 25
materialname steel
value 40000
weight 13000
client_type 291
gen_sp_armour 4
body_foot -2
end

