object wiz_hat
anim
wiz_hat.x11
wiz_hat.x12
wiz_hat.x13
mina
name Wizard Hat
name_pl Wizard Hats
face wiz_hat.x11
int 2
sp 4
ac 1
speed 0.1
nrof 1
type 34
resist_fire 15
resist_cold 15
resist_drain 30
resist_slow 30
resist_paralyze 30
materialname cloth
value 50000
weight 1000
last_sp 12
client_type 270
item_power 10
body_head -1
end

