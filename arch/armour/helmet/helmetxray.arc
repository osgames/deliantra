object helmet_of_xrays
anim
helmetxray.x11
helmetxray.x12
mina
name helmet
name_pl helmets
title of xray vision
face helmetxray.x11
ac 2
speed 0.05
nrof 1
type 34
resist_physical 5
materialname iron
value 70000
weight 6000
client_type 270
item_power 3
xrays 1
body_head -1
end

