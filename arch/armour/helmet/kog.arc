object kog
name Kabuto of Geisya
name_pl Kabutoes of Geisya
face kog.x11
wis 1
cha 2
int 1
ac 3
nrof 1
type 34
resist_physical 10
materialname iron
value 100000
weight 2000
client_type 270
item_power 4
gen_sp_armour 5
body_head -1
end

