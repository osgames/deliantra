object bighorned_helmet
name horned helmet
name_pl horned helmets
face bighorn_he.x11
ac 1
nrof 1
type 34
resist_physical 5
materialname bronze
value 22
weight 10000
client_type 271
gen_sp_armour 3
body_head -1
end

