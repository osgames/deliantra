object helmet_of_brilliance
name helmet
name_pl helmets
title of brilliance
face helmet_bri.x11
pow 2
int 2
sp 1
ac 2
nrof 1
type 34
resist_physical 5
value 95000
weight 7000
client_type 270
item_power 5
body_head -1
end

