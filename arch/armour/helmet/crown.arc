object crown
name crown
name_pl crowns
face crown.x11
nrof 1
type 34
resist_physical 3
materialname iron
value 1700
weight 12300
client_type 271
body_head -1
end

object crown_dark
name crown
name_pl crowns
face crown_dark.x11
nrof 1
type 34
resist_physical 3
materialname iron
value 1700
weight 12300
client_type 271
body_head -1
end

object crown_gray
name crown
name_pl crowns
face crown_gray.x11
nrof 1
type 34
resist_physical 3
materialname iron
value 1700
weight 12300
client_type 271
body_head -1
end

object crown_r
name crown
name_pl crowns
face crown_r.x11
nrof 1
type 34
resist_physical 3
materialname iron
value 1700
weight 12300
client_type 271
body_head -1
end

object crown_white
name crown
name_pl crowns
face crown_white.x11
nrof 1
type 34
resist_physical 3
materialname iron
value 1700
weight 12300
client_type 271
body_head -1
end

