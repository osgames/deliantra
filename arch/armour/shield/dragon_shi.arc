object dragon_shield
name dragon shield
name_pl dragon shields
face dragon_shi.x11
ac 1
nrof 1
type 33
resist_physical 8
resist_fire 30
materialname dragonscale
value 28000
weight 5000
magic 2
client_type 260
item_power 2
identified 1
body_arm -1
body_shield -1
end

