object DShield
anim
DShield.x11
DShield.x12
mina
name Demonspawn Shield
name_pl Demonspawn Shields
msg
  There is a evil spirit in the shield.
endmsg
face DShield.x11
cha -3
ac 3
speed 0.2
nrof 1
type 33
resist_physical 10
resist_fire 30
resist_drain 100
resist_ghosthit 70
materialname abyssium
value 50000
weight 25000
client_type 260
item_power 5
body_arm -1
body_shield -1
end

