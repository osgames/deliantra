object spiked_shield
name spiked shield
name_pl spiked shields
face spiked_shield.x11
dam 3
ac 1
nrof 1
type 33
resist_physical 3
materialname bronze
value 21
weight 2000
client_type 261
body_arm -1
body_shield -1
end

