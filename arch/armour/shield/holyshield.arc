object holy_shield
name holy shield
name_pl holy shields
face holyshield.x11
ac 4
nrof 1
type 33
resist_physical 10
resist_drain 100
resist_ghosthit 50
materialname iron
value 35000
weight 20000
client_type 260
item_power 6
body_arm -1
body_shield -1
end

