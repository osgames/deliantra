object reflector
anim
reflector.x11
reflector.x12
reflector.x13
reflector.x12
mina
name polished shield
name_pl polished shields
face reflector.x11
ac 3
speed 0.25
nrof 1
type 33
resist_physical 5
materialname adamant
value 100000
weight 18000
client_type 260
item_power 2
reflect_spell 1
body_arm -1
body_shield -1
end

