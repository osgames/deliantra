object DShieldms
anim
DShield.x11
DShield.x12
DShield.x11
DShield.x13
mina
name Demonspawn Shield
name_pl Demonspawn Shields
msg
  There is a powerful evil spirit in the
  shield dominating your mind. You are
  struggling to retain control of yourself.
endmsg
face DShield.x11
cha -5
ac 7
speed 0.25
nrof 1
type 33
resist_physical 15
resist_fire 30
resist_drain 100
resist_ghosthit 80
materialname abyssium
value 300000
weight 25000
client_type 260
item_power 10
reflect_missile 1
reflect_spell 1
body_arm -1
body_shield -1
end

