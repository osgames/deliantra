object frostshield
name Frost Shield
name_pl Frost Shields
face frostshield.x11
dam 1
ac -1
nrof 1
type 33
resist_physical 3
resist_fire 10
resist_cold 15
materialname bronze
value 100
weight 2000
client_type 261
body_arm -1
body_shield -1
end

