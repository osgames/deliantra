object DShields
anim
DShield.x11
DShield.x12
DShield.x11
DShield.x12
DShield.x11
DShield.x13
mina
name Demonspawn Shield
name_pl Demonspawn Shields
msg
  There is a strong evil spirit in the shield
  trying to dominate you.
endmsg
face DShield.x11
cha -3
ac 4
speed 0.2
nrof 1
type 33
resist_physical 10
resist_fire 30
resist_drain 100
resist_ghosthit 75
materialname abyssium
value 100000
weight 25000
client_type 260
item_power 8
reflect_spell 1
body_arm -1
body_shield -1
end

