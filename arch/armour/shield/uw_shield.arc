object uw_shield
name Belzebub's shield
name_pl Belzebub's shields
face uw_shield.x11
ac 4
nrof 1
type 33
resist_physical 15
resist_deplete 100
materialname abyssium
value 80000
weight 25000
client_type 260
item_power 5
body_arm -1
body_shield -1
end

