object eyeshield
name eyeshield
name_pl eyeshields
face eyeshield.x11
ac 3
nrof 1
type 33
resist_physical 3
resist_magic 30
materialname organic
value 30000
weight 15000
client_type 260
item_power 2
identified 1
body_arm -1
body_shield -1
end

