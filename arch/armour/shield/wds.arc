object wds
name White Dragon Shield
name_pl White Dragon Shields
face wds.x11
ac 2
nrof 1
type 33
resist_physical 8
resist_fire 30
resist_cold 30
value 45000
weight 6000
magic 2
client_type 260
item_power 4
identified 1
body_arm -1
body_shield -1
end

