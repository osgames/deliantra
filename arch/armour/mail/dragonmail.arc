object blue_dragon_mail
name blue dragon mail
name_pl blue dragon mails
face bluedragonmail.x11
ac 6
nrof 1
type 16
resist_physical 50
resist_electricity 40
materialname dragonscale
value 50000
weight 60000
magic 3
last_sp 13
client_type 251
item_power 5
gen_sp_armour 9
identified 1
body_torso -1
end

object dragon_mail
name dragon mail
name_pl dragon mails
face dragonmail.x11
ac 6
nrof 1
type 16
resist_physical 50
resist_fire 40
materialname dragonscale
value 50000
weight 60000
magic 3
last_sp 13
client_type 251
item_power 5
gen_sp_armour 9
identified 1
body_torso -1
end

object green_dragon_mail
name green dragon mail
name_pl green dragon mails
face greendragonmail.x11
ac 6
nrof 1
type 16
resist_physical 50
resist_cold 40
materialname dragonscale
value 50000
weight 60000
magic 3
last_sp 13
client_type 251
item_power 5
gen_sp_armour 9
identified 1
body_torso -1
end

