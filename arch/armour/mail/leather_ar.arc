object leather_armour
name armour
name_pl armours
face leather_ar.x11
ac 2
nrof 1
type 16
resist_physical 10
materialname leather
value 40
weight 20000
last_sp 13
client_type 254
gen_sp_armour 8
body_torso -1
end

