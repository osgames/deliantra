object mithril_ar_ele
anim
mithril_ar_ele.x11
mithril_ar_ele.x11
mithril_ar_ele.x12
mithril_ar_ele.x12
mithril_ar_ele.x13
mithril_ar_ele.x13
mina
name mithril chainmail of lightning
name_pl mithril chainmails of lightning
face mithril_ar_ele.x11
str 1
cha 1
exp 1
ac 4
speed 0.3
nrof 1
type 16
resist_physical 40
resist_electricity 30
materialname mithril
value 4000
weight 15000
magic 3
last_sp 27
client_type 250
item_power 9
gen_sp_armour 2
body_torso -1
end

