object mithril_chainmail
anim
mithril_ar.x11
mithril_ar.x12
mithril_ar.x13
mina
name mithril chainmail
name_pl mithril chainmails
face mithril_ar.x11
ac 6
speed 0.1
nrof 1
type 16
resist_physical 35
materialname mithril
value 8000
weight 15000
last_sp 18
client_type 250
item_power 1
gen_sp_armour 5
body_torso -1
end

