object Pdragon_mail
anim
Pdragonmail.x11
Pdragonmail.x12
mina
name Power Dragon Mail
name_pl Power Dragon Mails
face Pdragonmail.x11
exp 1
ac 8
speed 0.2
nrof 1
type 16
resist_physical 60
resist_fire 50
resist_electricity 30
materialname adamant
value 990000
weight 40000
magic 4
last_sp 13
client_type 251
item_power 12
gen_sp_armour 9
identified 1
body_torso -1
end

