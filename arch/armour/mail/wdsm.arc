object wdsm
anim
wdsm.x11
wdsm.x12
wdsm.x13
mina
name White Dragon Scale Mail
name_pl White Dragon Scale Mails
face wdsm.x11
ac 5
speed 0.1
nrof 1
type 16
resist_physical 45
resist_fire 95
resist_cold 30
value 220000
weight 5000
magic -3
last_sp 13
client_type 251
item_power 5
gen_sp_armour 9
startequip 1
cursed 1
no_steal 1
body_torso -1
end

