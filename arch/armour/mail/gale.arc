object gale
name Gale Armour
name_pl Gale Armours
face gale.x11
dex 2
ac 4
speed 2.000000
nrof 1
type 16
resist_physical 40
resist_electricity 30
materialname leather
value 220000
weight 10000
last_sp 50
client_type 250
item_power 4
body_torso -1
end

