object robe_midnight
anim
robe_midnight.x11
robe_midnight.x12
robe_midnight.x13
mina
name Midnight Robe
name_pl Midnight Robes
face robe_midnight.x11
dex 1
pow 2
int 2
ac 5
speed 0.1
nrof 1
type 16
resist_magic 30
resist_fire 20
resist_cold 20
resist_acid 75
resist_drain 20
resist_ghosthit -100
resist_slow 20
resist_paralyze 20
materialname cloth
value 100000
weight 5000
magic 5
client_type 250
item_power 25
reflect_spell 1
body_torso -1
end

