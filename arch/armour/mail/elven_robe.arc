object elven_robe
name Elven Robe
name_pl Elven Robes
face elven_robe.x11
dex 4
food 4
ac 3
nrof 1
type 16
resist_confusion 60
materialname cloth
value 3000
weight 5000
magic 1
last_sp 12
client_type 256
item_power 13
stealth 1
body_torso -1
end

