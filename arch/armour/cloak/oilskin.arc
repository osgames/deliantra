object oilskin
name oilskin cloak
name_pl oilskin cloaks
face oilskin.x11
dex -1
ac 1
nrof 1
type 87
resist_physical 3
resist_acid 70
materialname leather
value 120000
weight 1000
magic -4
client_type 280
damned 1
body_shoulder -1
end

