object magic_resist
name Cloak of Magic Resistance
name_pl Cloaks of Magic Resistance
face magic_resist.x11
ac 0
nrof 1
type 87
resist_magic 95
materialname astolare
value 220000
weight 5000
client_type 280
item_power 4
startequip 1
no_steal 1
body_shoulder -1
end

