object serp_cloak
name serpent cloak
name_pl serpent cloaks
face serp_cloak.x11
ac 1
nrof 1
type 87
resist_poison 30
materialname snakeskin
value 900
weight 700
client_type 280
item_power 2
body_shoulder -1
end

