object god_finger
anim
god_finger.x11
god_finger.x12
mina
name God Finger
name_pl God Fingers
face god_finger.x11
str 2
dex -1
dam 3
speed .1
nrof 1
type 100
resist_physical 3
materialname iron
value 120000
weight 1800
client_type 300
item_power 2
make_invisible 1
body_hand -2
end

