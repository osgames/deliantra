object shining_finger
anim
shining_finger.x11
shining_finger.x12
mina
name Shining Finger
name_pl Shining Fingers
face shining_finger.x11
str 2
dam 3
speed .1
nrof 1
type 100
resist_physical 3
materialname iron
value 120000
weight 1800
client_type 300
item_power 1
body_hand -2
end

