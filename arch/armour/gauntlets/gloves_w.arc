object gloves_w
name gloves
name_pl gloves
face gloves_w.x11
cha 1
nrof 1
type 100
resist_physical 1
resist_blind 2
materialname leather
value 6000
weight 100
client_type 301
body_hand -2
end

object gloves_w_l
name gloves
name_pl gloves
title of light
face gloves_w.x11
cha 1
nrof 1
type 100
resist_physical 20
resist_blind 20
materialname steel
value 42000
weight 200
client_type 301
body_hand -2
end

