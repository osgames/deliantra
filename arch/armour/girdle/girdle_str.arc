object girdle_str
anim
girdle_str.x11
girdle_str.x12
girdle_str.x13
girdle_str.x14
mina
name girdle
name_pl girdles
title of strength
face girdle_str.x11
str 2
speed 0.1
nrof 1
type 113
materialname leather
value 80000
weight 2500
client_type 321
item_power 1
body_waist -1
end

