object girdle_dam
anim
girdle_dam.x11
girdle_dam.x12
mina
name girdle
name_pl girdles
title of damage
face girdle_dam.x11
dam 10
speed 0.1
nrof 1
type 113
materialname leather
value 70000
weight 2500
client_type 321
item_power 1
body_waist -1
end

