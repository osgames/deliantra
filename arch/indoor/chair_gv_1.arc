object chair_gv_1.1
inherit type_weapon
name chair
name_pl chairs
skill two handed weapons
face chair_gv_1.x01
dam 5
nrof 1
materialname gold
value 45000
weight 45000
last_sp 17
client_type 8002
body_arm -2
body_combat -1
end

object chair_gv_1.2
inherit type_weapon
name chair
name_pl chairs
skill two handed weapons
face chair_gv_1.x11
dam 5
nrof 1
materialname gold
value 45000
weight 45000
last_sp 17
client_type 8002
body_arm -2
body_combat -1
end

object chair_gv_1.3
inherit type_weapon
name chair
name_pl chairs
skill two handed weapons
face chair_gv_1.x21
dam 5
nrof 1
materialname gold
value 45000
weight 45000
last_sp 17
client_type 8002
body_arm -2
body_combat -1
end

object chair_gv_1.4
inherit type_weapon
name chair
name_pl chairs
skill two handed weapons
face chair_gv_1.x31
dam 5
nrof 1
materialname gold
value 45000
weight 45000
last_sp 17
client_type 8002
body_arm -2
body_combat -1
end

