object table_2x1
name long table
name_pl tables
skill two handed weapons
face table_2x1.x11
dam 10
nrof 1
type 15
materialname wood
value 55
weight 150000
last_sp 19
client_type 8002
no_pick 1
body_arm -2
body_combat -1
end
more
object table_2x1_2
name long table
name_pl tables
skill two handed weapons
face table_2x1.x11
dam 10
x 1
nrof 1
type 15
materialname wood
value 55
weight 150000
last_sp 19
client_type 8002
no_pick 1
body_arm -2
body_combat -1
end

