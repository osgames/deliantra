object chair_2.1
inherit type_weapon
name chair
name_pl chairs
skill two handed weapons
face chair_2.x01
dam 5
nrof 1
materialname wood
value 25
weight 50000
last_sp 18
client_type 8002
body_arm -2
body_combat -1
end

object chair_2.2
inherit type_weapon
name chair
name_pl chairs
skill two handed weapons
face chair_2.x11
dam 5
nrof 1
materialname wood
value 25
weight 50000
last_sp 18
client_type 8002
body_arm -2
body_combat -1
end

object chair_2.3
inherit type_weapon
name chair
name_pl chairs
skill two handed weapons
face chair_2.x21
dam 5
nrof 1
materialname wood
value 25
weight 50000
last_sp 18
client_type 8002
body_arm -2
body_combat -1
end

object chair_2.4
inherit type_weapon
name chair
name_pl chairs
skill two handed weapons
face chair_2.x31
dam 5
nrof 1
materialname wood
value 25
weight 50000
last_sp 18
client_type 8002
body_arm -2
body_combat -1
end

