object table_1x2
name large table
name_pl tables
skill two handed weapons
face table_1x2.x11
dam 10
nrof 1
type 15
materialname wood
value 55
weight 150000
last_sp 19
client_type 8002
no_pick 1
body_arm -2
body_combat -1
end
more
object table_1x2_2
name large table
name_pl tables
skill two handed weapons
face table_1x2.x11
dam 10
y 1
nrof 1
type 15
materialname wood
value 55
weight 150000
last_sp 19
client_type 8002
no_pick 1
body_arm -2
body_combat -1
end

