object table_2x2
name big table
name_pl tables
skill two handed weapons
face table_2x2.x11
dam 10
nrof 1
type 15
materialname wood
value 100
weight 300000
last_sp 19
client_type 8002
no_pick 1
body_arm -2
body_combat -1
end
more
object table_2x2_2
name big table
name_pl tables
skill two handed weapons
face table_2x2.x11
dam 10
x 1
nrof 1
type 15
materialname wood
value 100
weight 300000
last_sp 19
client_type 8002
no_pick 1
body_arm -2
body_combat -1
end
more
object table_2x2_3
name big table
name_pl tables
skill two handed weapons
face table_2x2.x11
dam 10
y 1
nrof 1
type 15
materialname wood
value 100
weight 300000
last_sp 19
client_type 8002
no_pick 1
body_arm -2
body_combat -1
end
more
object table_2x2_4
name big table
name_pl tables
skill two handed weapons
face table_2x2.x11
dam 10
x 1
y 1
nrof 1
type 15
materialname wood
value 100
weight 300000
last_sp 19
client_type 8002
no_pick 1
body_arm -2
body_combat -1
end

