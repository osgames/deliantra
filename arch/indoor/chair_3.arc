object chair_3.1
inherit type_weapon
name chair
name_pl chairs
skill two handed weapons
face chair_3.x01
dam 6
nrof 1
materialname wood
value 25
weight 60000
last_sp 19
client_type 8002
body_arm -2
body_combat -1
end

object chair_3.2
inherit type_weapon
name chair
name_pl chairs
skill two handed weapons
face chair_3.x11
dam 6
nrof 1
materialname wood
value 25
weight 60000
last_sp 19
client_type 8002
body_arm -2
body_combat -1
end

