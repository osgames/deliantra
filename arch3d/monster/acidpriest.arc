Object acidpriest
name Acid priest
race unnatural
randomitems acidpriest_big
face acidpriest.x12
color_fg black
anim
facings 8
acidpriest.x11
acidpriest.x12
acidpriest.x13
acidpriest.x14
acidpriest.x15
acidpriest.x16
acidpriest.x17
acidpriest.x18
mina
exp 256000000
monster 1
alive 1
no_pick 1
flying 1
see_invisible 1
hp 80000
maxhp 80000
ac -120
wc -100
dam 95
level 90
speed 1.5
weight 15000
run_away 5
hitback 1
editable 1
attack_movement 0
movement_type 4
attacktype 464323
reflect_missile 1
reflect_spell 1
sp 500
maxsp 800
str 35
dex 23
con 40
pow 60
int 25
resist_physical 98
resist_magic 85
resist_fire 85
resist_electricity 60
resist_cold 100
resist_confusion 85
resist_acid 100
resist_drain 98
resist_weaponmagic 10
resist_death 60
resist_chaos 85
resist_godpower 85
resist_holyword 85
resist_blind 85
path_attuned 393232
path_repelled 589824
can_see_in_dark 1
can_cast_spell 1
can_use_ring 1
flying 1
end
More
Object acidpriest_2
name Acid priest
face acidpriest.x12
color_fg black
x 1
anim
facings 8
acidpriest.x11
acidpriest.x12
acidpriest.x13
acidpriest.x14
acidpriest.x15
acidpriest.x16
acidpriest.x17
acidpriest.x18
mina
alive 1
no_pick 1
flying 1
end
More
Object acidpriest_3
name Acid priest
face acidpriest.x12
color_fg black
y 1
anim
facings 8
acidpriest.x11
acidpriest.x12
acidpriest.x13
acidpriest.x14
acidpriest.x15
acidpriest.x16
acidpriest.x17
acidpriest.x18
mina
alive 1
no_pick 1
flying 1
end
More
Object acidpriest_4
name Acid priest
face acidpriest.x12
color_fg black
x 1
y 1
anim
facings 8
acidpriest.x11
acidpriest.x12
acidpriest.x13
acidpriest.x14
acidpriest.x15
acidpriest.x16
acidpriest.x17
acidpriest.x18
mina
alive 1
no_pick 1
flying 1
end
