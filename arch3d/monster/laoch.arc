Object laoch
name Laoch
randomitems laoch
race human
face laoch.111
color_fg brown
anim
facings 8
laoch.111
laoch.121
laoch.112
laoch.122
laoch.113
laoch.123
laoch.114
laoch.124
laoch.115
laoch.125
laoch.116
laoch.126
laoch.117
laoch.127
laoch.118
laoch.128
mina
monster 1
sleep 1
Wis 10
no_pick 1
alive 1
exp 20
ac 14
wc 17
dam 3
hp 6
maxhp 6
level 2
speed 0.3
weight 50000
run_away 10
will_apply 2
pick_up 24
can_apply 25
can_use_wand 1
can_use_bow 1
can_use_ring 1
can_use_shield 1
can_use_armour 1
can_use_weapon 1
editable 1
body_finger 2
body_torso 1
body_head 1
body_shoulder 1
body_foot 2
body_wrist 2
body_hand 2
body_waist 1
body_arm 2
body_range 1
end
