Object royalty
face royalty.111
race gold and jewels
nrof 1
type 36
skill literacy
material 1
value 5000
weight 1
editable 2048
name_pl imperials
msg
A royal bank note from
His Majesty, the King of Scorn
endmsg
level 1
client_type 2001
end
