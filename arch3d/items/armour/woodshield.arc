Object woodshield
name Wooden Shield
name_pl Wooden Shields
client_type 260
nrof 1
type 33
material 16
materialname wood
face woodshield.111
ac 7
resist_physical 20
resist_acid 10
weight 25000
value 50000
editable 5120
color_fg brown
body_arm -1
item_power 5
end
