Object thundar_bolt
name thundar bolt
race crossbow bolts
face thundar_bolt.101
anim
thundar_bolt.101
thundar_bolt.111
thundar_bolt.121
thundar_bolt.131
thundar_bolt.141
thundar_bolt.151
thundar_bolt.161
thundar_bolt.171
thundar_bolt.181
mina
color_fg dark_orange
is_animated 0
food 10
dam 100
wc -10
nrof 50
type 13
attacktype 9
material 2
value 20000
weight 1000
is_turnable 1
editable 1024
magicmap brown
name_pl thundar bolts
client_type 165
end
