# this module only exists mostly make CPAN believe that there is indeed,
# something called "Deliantra::Client" on CPAN.

package Deliantra::Client;

for (grep !ref, @INC) {
   my $path = "$_/Deliantra/Client/private";
   if (-d $path) {
      unshift @INC, $path;
      last;
   }
}

require DC;

$VERSION = $DC::VERSION;

package App::Deliantra;

$VERSION = $DC::VERSION;

1;

