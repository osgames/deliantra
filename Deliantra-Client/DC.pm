=head1 NAME

DC - undocumented utility garbage for our deliantra client

=head1 SYNOPSIS

 use DC;

=head1 DESCRIPTION

=over 4

=cut

package DC;

use Carp ();

our $VERSION;

BEGIN {
   $VERSION = '3.0';

   use XSLoader;
   XSLoader::load "Deliantra::Client", $VERSION;
}

use utf8;
use strict qw(vars subs);

use Socket ();
use AnyEvent ();
use AnyEvent::Util ();
use Pod::POM ();
use File::Path ();
use Storable (); # finally
use Fcntl ();
use JSON::XS qw(encode_json decode_json);
use Guard qw(guard);

# modules to support other DC::* packages
use List::Util ();
use IO::AIO ();
use Coro::AIO ();
use AnyEvent::AIO ();

use Deliantra::Util ();
use Deliantra::Protocol::Constants ();

=item shorten $string[, $maxlength]

=cut

sub shorten($;$) {
   my ($str, $len) = @_;
   substr $str, $len, (length $str), "..." if $len + 3 <= length $str;
   $str
}

sub asxml($) {
   local $_ = $_[0];

   s/&/&amp;/g;
   s/>/&gt;/g;
   s/</&lt;/g;

   $_
}

sub sanitise_cfxml($) {
   local $_ = shift;

   # we now weed out all tags we do not support
   s{ <(?! /?i> | /?u> | /?b> | /?big | /?small | /?s | /?tt | fg\ | /fg>)
   }{
      "&lt;"
   }gex;

   # now all entities
   s/&(?!amp;|lt;|gt;|apos;|quot;|#[0-9]+;|#x[0-9a-fA-F]+;)/&amp;/g;

   # handle some elements
   s/<fg name='([^']*)'>(.*?)<\/fg>/<span foreground='$1'>$2<\/span>/gs;
   s/<fg name="([^"]*)">(.*?)<\/fg>/<span foreground="$1">$2<\/span>/gs;

   s/\s+$//;

   $_
}

sub background(&;&) {
   my ($bg, $cb) = @_;

   my ($fh_r, $fh_w) = AnyEvent::Util::portable_socketpair
     or die "unable to create background socketpair: $!";

   my $pid = fork;

   if (defined $pid && !$pid) {
      local $SIG{__DIE__};

      open STDOUT, ">&", $fh_w;
      open STDERR, ">&", $fh_w;
      close $fh_r;
      close $fh_w;

      $| = 1;

      eval { $bg->() };

      if ($@) {
         my $msg = $@;
         $msg =~ s/\n+/\n/;
         warn "FATAL: $msg";
         DC::_exit 1;
      }

      # win32 is fucked up, of course. exit will clean stuff up,
      # which destroys our database etc. _exit will exit ALL
      # forked processes, because of the dreaded fork emulation.
      DC::_exit 0;
   }

   close $fh_w;

   my $buffer;

   my $w; $w = AnyEvent->io (fh => $fh_r, poll => 'r', cb => sub {
      unless (sysread $fh_r, $buffer, 4096, length $buffer) {
         undef $w;
         $cb->();
         return;
      }

      while ($buffer =~ s/^(.*)\n//) {
         my $line = $1;
         $line =~ s/\s+$//;
         utf8::decode $line;
         if ($line =~ /^\x{e877}json_msg (.*)$/s) {
            $cb->(JSON::XS->new->allow_nonref->decode ($1));
         } else {
            ::message ({
               markup => "background($pid): " . DC::asxml $line,
            });
         }
      }
   });
}

sub background_msg {
   my ($msg) = @_;

   $msg = "\x{e877}json_msg " . JSON::XS->new->allow_nonref->encode ($msg);
   $msg =~ s/\n//g;
   utf8::encode $msg;
   print $msg, "\n";
}

package DC;

our $RC_THEME;
our %THEME;
our @RC_PATH;
our $RC_BASE;

for (grep !ref, @INC) {
   $RC_BASE = "$_/Deliantra/Client/private/resources";
   last if -d $RC_BASE;
}

sub find_rcfile($) {
   my $path;

   for (@RC_PATH, "") {
      $path = "$RC_BASE/$_/$_[0]";
      return $path if -e $path;
   }

   die "FATAL: can't find required file \"$_[0]\" in \"$RC_BASE\"\n";
}

sub load_json($) {
   my ($file) = @_;

   open my $fh, $file
      or return;

   local $/;
   eval { JSON::XS->new->utf8->relaxed->decode (<$fh>) }
}

sub set_theme($) {
   return if $RC_THEME eq $_[0];
   $RC_THEME = $_[0];

   # kind of hacky, find the main theme file, then load all theme files and merge them

   %THEME = ();
   @RC_PATH = "theme-$RC_THEME";

   my $theme = load_json find_rcfile "theme.json"
      or die "FATAL: theme resource file not found";

   @RC_PATH = @{ $theme->{path} } if $theme->{path};

   for (@RC_PATH, "") {
      my $theme = load_json "$RC_BASE/$_/theme.json"
         or next;

      %THEME = ( %$theme, %THEME );
   }
}

sub read_cfg($) {
   my ($file) = @_;

   $::CFG = (load_json $file) || (load_json "$file.bak");
}

sub write_cfg($) {
   my $file = "$Deliantra::VARDIR/client.cf";

   $::CFG->{VERSION} = $::VERSION;
   $::CFG->{layout}  = DC::UI::get_layout ();

   open my $fh, ">:utf8", "$file~"
      or return;
   print $fh JSON::XS->new->utf8->pretty->encode ($::CFG);
   close $fh;

   rename $file, "$file.bak";
   rename "$file~", $file;
}

sub load_cfg() {
   if (-e "$Deliantra::VARDIR/client.cf") {
      DC::read_cfg "$Deliantra::VARDIR/client.cf";
   } else {
      $::CFG = { cfg_schema => 1, db_schema => 1 };
   }
}

sub save_cfg() {
   write_cfg "$Deliantra::VARDIR/client.cf";
}

sub upgrade_cfg() {
   my %DEF_CFG = (
      config_autosave     => 1,
      sdl_mode            => undef,
      fullscreen          => 1,
      fast                => 0,
      force_opengl11      => undef,
      disable_alpha       => 0,
      smooth_movement     => 1,
      smooth_transitions  => 1,
      texture_compression => 1,
      map_scale           => 1,
      fow_enable          => 1,
      fow_intensity       => 0,
      fow_texture         => 0,
      map_smoothing       => 1,
      gui_fontsize        => 1,
      log_fontsize        => 0.7,
      gauge_fontsize      => 1,
      gauge_size          => 0.35,
      stat_fontsize       => 0.7,
      mapsize             => 100,
      audio_enable        => 1,
      audio_hw_channels   => 0,
      audio_hw_frequency  => 0,
      audio_hw_chunksize  => 0,
      audio_mix_channels  => 8,
      effects_enable      => 1,
      effects_volume      => 1,
      bgm_enable          => 1,
      bgm_volume          => 0.5,
      output_rate         => "",
      pickup              => Deliantra::Protocol::Constants::PICKUP_SPELLBOOK
                           | Deliantra::Protocol::Constants::PICKUP_SKILLSCROLL
                           | Deliantra::Protocol::Constants::PICKUP_VALUABLES,
      inv_sort            => "mtime",
      default             => "profile", # default profile
      show_tips           => 1,
      logview_max_par     => 1000,
      shift_fire_stop     => 0,
      uitheme             => "wood",
      map_shift_x         => -24, # arbitrary
      map_shift_y         => +24, # arbitrary
   );

   while (my ($k, $v) = each %DEF_CFG) {
      $::CFG->{$k} = $v unless exists $::CFG->{$k};
   }

   if ($::CFG->{cfg_schema} < 1) {
      for my $profile (values %{ $::CFG->{profile} }) {
         $profile->{password} = unpack "H*", Deliantra::Util::hash_pw $profile->{password};
      }
      $::CFG->{cfg_schema} = 1;
   }
}

sub http_proxy {
   my @proxy = win32_proxy_info;

   if (@proxy) {
      "http://" . (@proxy < 2 ? "" : @proxy < 3 ? "$proxy[1]\@" : "$proxy[1]:$proxy[2]\@") . $proxy[0]
   } elsif (exists $ENV{http_proxy}) {
      $ENV{http_proxy}
   } else {
     ()
   }
}

sub set_proxy {
   my $proxy = http_proxy
      or return;

   $ENV{http_proxy} = $proxy;
}

sub lwp_useragent {
   require LWP::UserAgent;
   
   DC::set_proxy;

   my $ua = LWP::UserAgent->new (
      agent      => "deliantra $VERSION",
      keep_alive => 1,
      env_proxy  => 1,
      timeout    => 30,
   );
}

sub lwp_check($) {
   my ($res) = @_;

   $res->is_error
      and die $res->status_line;

   $res
}

sub fh_nonblocking($$) {
   my ($fh, $nb) = @_;

   if ($^O eq "MSWin32") {
      $nb = (! ! $nb) + 0;
      ioctl $fh, 0x8004667e, \$nb; # FIONBIO
   } else {
      fcntl $fh, &Fcntl::F_SETFL, $nb ? &Fcntl::O_NONBLOCK : 0;
   }
}

package DC::Layout;

$DC::OpenGL::INIT_HOOK{"DC::Layout"} = sub {
   glyph_cache_restore;
};

$DC::OpenGL::SHUTDOWN_HOOK{"DC::Layout"} = sub {
   glyph_cache_backup;
};

1;

=back

=head1 AUTHOR

 Marc Lehmann <schmorp@schmorp.de>
 http://home.schmorp.de/

=cut

