=head1 NAME

DC::Audio - audio support for deliantra client

=head1 SYNOPSIS

 use DC::Audio;

=head1 DESCRIPTION

=over 4

=cut

package DC::Audio;

use common::sense;
use DC ();

our $SDL_MIXER;

sub init() {
   unless ($::SDL_MIXER) {
      if (length $::CFG->{audio_driver}) {
         local $ENV{SDL_AUDIODRIVER} = $::CFG->{audio_driver};
         DC::SDL_InitSubSystem DC::SDL_INIT_AUDIO
            and die "SDL::Init failed!\n";
      } else {
         DC::SDL_InitSubSystem DC::SDL_INIT_AUDIO
            and die "SDL::Init failed!\n";
      }

      $ENV{MIX_EFFECTSMAXSPEED} = 1;
      $::SDL_MIXER = !DC::Mix_OpenAudio
         $::CFG->{audio_hw_frequency},
         DC::MIX_DEFAULT_FORMAT,
         $::CFG->{audio_hw_channels},
         $::CFG->{audio_hw_chunksize};

       DC::Mix_AllocateChannels $::CFG->{audio_mix_channels}
          if $::SDL_MIXER;
   }
}

sub _probe {
   my $pid = fork;

   return 1 unless defined $pid;

   unless ($pid) {
      eval {
         DC::SDL_Init DC::SDL_INIT_NOPARACHUTE;
         init;
      };
      DC::_exit $@ ? 1 : 0;
   }

   waitpid $pid, 0
      and !$?
}

sub probe {
   return unless $^O eq "linux";

   # on linux, both alsa and pulse (especially the latter)
   # are prone to segfaults, while the other interfaces are
   # simply unreliable.

   # let's check whether the default config and a few others segfault,
   # then decide

                                     return if _probe; # user selected config
   $::CFG->{audio_driver} = ""     ; return if _probe; # default sdl config
   $::CFG->{audio_driver} = "pulse"; return if _probe;
   $::CFG->{audio_driver} = "alsa" ; return if _probe;
   $::CFG->{audio_driver} = "esd"  ; return if _probe;
   $::CFG->{audio_driver} = "dsp"  ; return if _probe;
   $::CFG->{audio_driver} = "none" ;
}

1;

=back

=head1 AUTHOR

 Marc Lehmann <schmorp@schmorp.de>
 http://home.schmorp.de/

=cut

