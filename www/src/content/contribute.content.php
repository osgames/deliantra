<div id="content">
		<h2>Want to Contribute to Deliantra?</h2><br />
	<div style='clear: both; margin-bottom: 20px;'></div>
		<p>
			Do you have a new idea you want to implement?
			Want to make a new monster? Or a new map? Are you an artist and want to
			make some nice artwork or graphics? Then you are on the right page!
		</p>

		<h3>Some things you have to bear in mind:</h3>

		<ul>
			 <li>
				All things you contribute have to be licensed under
				<a href="http://www.gnu.org/copyleft/gpl.html">GPL</a>.
			 </li>
			 <li>
				All developers and contributors are investing their free time to
				improve the game. As all this is non-commercial and therefore, there
				is no reward we can give, except that you are mentioned in the credits
				and get the social status of a contributor who improved and helped to
				build this game.
			 </li>
			 <li>
				The immediate reward you get is the feeling that you helped the
				users of this server and the whole game and of course the status as
				contributor and the mention in the credits.
			 </li>
			 <li>
				We can't tell what you should do or assign any tasks to you like a
				company would do. There is, of course, a list of things we want done (ask via email or on
				the irc channel), but we can't and won't force you to work on that.
				Every developer is free and should do what he likes to do, as it is
				his free time he invests.
			 </li>
			 <li>
				Contribution to the game and the server isn't connected in any way
				to gain Dungeon Master status. It's essentially up to the server admins of
				deliantra who becomes DM. The reason for this is that the game
				development is not directly related to the server. Anyone could run
				a server and that doesn't imply that all the game developers are
				automatically Dungeon Master on all public servers.
			 </li>
		</ul>
		
		<h3>What to contribute?</h3>
		
		<p>
			It might help if you talk to other developers, for example
			on the IRC channel, to get an idea of what might be needed
			most at the moment or what isn't currently needed that
			much. We can't tell you what to do, but we could help you
			to find ideas of what to do.
		</p>
		
		<p>
			 If you are not familiar with the code or don't want to contribute
			 code to Deliantra, here is a list what things you could do:
		</p>
		
		<ul>
			 <li>
				 <b>Maps:</b> We strongly encourage users to make maps.  In fact, we wrote an entirely
				 new map editor (see <a href="editor.html">Gtk+ Deliantra Map Editor</a>)
				 to make mapediting more bearable and less tedious. If you want to make
				 maps we recommend to read <a href="http://crossfire.real-time.com/guides/map/index.html">the map guidelines of the original crossfire</a>.
			 </li>
			 <li>
				 <b>Graphics:</b> We are in the process of replacing the old tileset with new
				 and nicer graphics (monsters, houses, items, whatever). If you want to
				 make new graphics, please submit an archetype file along with the graphics!
				 If you have improved or redrawn an existing archetype, you can submit the
				 new graphics.
			 </li>
			 <li>
				 <b>Sounds:</b> New sounds would be a great addition and would improve the overall
				 atmosphere of the game.
			 </li>
		</ul>
		
		<p>
			 Aside from all that, you can checkout the entire source code from our
			 CVS, get familiar with it and try to find and fix bugs and/or implement
			 other kinds of improvements.
		</p>
		
		<h3>How to contribute?</h3>
		
		<p>
			 If you finished some nice maps/graphics/whatever, you should contact the
			 developers. They will help you getting your work into the CVS and online
			 on the game server. You can either send what you have made to
			 <tt>support@deliantra.net</tt> or you ask for CVS access.
		</p>
		
		<p>
			 You can also contact the developers.  See the <a href="contact.html">Contact</a> page.
		</p>
		
		<h3>Development Resources</h3>

		<ul>
			 <li>
				The <a href="testserver.html">testserver.deliantra.net:24 test server</a> which always
				runs the newest version of server code, maps and archetypes.
			 </li>
			 <li>
				 The <a href="deliantra_vm.html">Deliantra development VM</a>.
			 </li>
			 <li>
				The <a href="editor.html">Deliantra Editor</a> that is easy to install and use.
			 </li>
			 <li>
				The <a href="http://maps.deliantra.net/">maps.deliantra.net</a> server
				with browseable maps.
			 </li>
			 <li>
				A public <a href="http://software.schmorp.de/pkg/deliantra.html">
				CVS repository</a> that contains:
				<ul>
					 <li>
						The <a href="server.html">server</a>, in the
						<a href="http://cvs.schmorp.de/deliantra/server/">
						<code>server/</code></a> directory.
					 </li>
					 <li>
						The <a href="editor.html">Deliantra Editor</a>, in the
						<a href="http://cvs.schmorp.de/deliantra/gde/">
						<code>gde/</code></a> directory.
					 </li>
					 <li>
						The <a href="client.html">Deliantra MORPG Client</a>, in the
						<a href="http://cvs.schmorp.de/deliantra/Deliantra-Client/">
						<code>Deliantra-Client/</code></a> directory.
					 </li>
					 <li>
						The <a href="maps.html">maps and archetypes</a>, in the
						<a href="http://cvs.schmorp.de/deliantra/maps/">
						<code>maps/</code></a> and
						<a href="http://cvs.schmorp.de/deliantra/arch/">
						<code>arch/</code></a> directories.
					 </li>
					 <li>The Perl plug-ins, in the
						<a href="http://cvs.schmorp.de/deliantra/maps/perl/">
						<code>maps/perl/</code></a> directory.
					 </li>
					 <li>
						The Deliantra perl module family, in the
						<a href="http://cvs.schmorp.de/deliantra/Deliantra/">
						<code>Deliantra/</code></a> directory.
					 </li>
				</ul>
			 </li>
		</ul>

		<h3>Development Tutorials</h3>
		
		<ul>
			<li><a href='editor_tut.html'>Deliantra Editor and Server Installation Tutorial</a></li>
		</ul>
		
</div>
