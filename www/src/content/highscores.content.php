			<div id="content">
				<h2>Deliantra High Scores</h2>
				
				<p>
					This highscore list is updated every ten minutes to show Deliantra's most powerful players.
				</p>

<?php

	//Generate the highscore table.

	$jsonScoresUnparsed = file_get_contents("highscore.json");
	
	$jsonArray = json_decode($jsonScoresUnparsed);
	
	$timestamp = $jsonArray->date;
	$version = $jsonArray->version;
	$players = $jsonArray->data;
	
	echo "<table style='width: 100%'>\n";
	echo "<tr><th>#</th><th>Player Name</th><th style='text-align: center;'>Level</th><th style='text-align: right;'>Experience Points</th><th style='text-align: center;'>HP</th><th style='text-align: center;'>SP</th><th style='text-align: center;'>GR</th></tr>";
	
	$number = 0;
	$size = 100;
	foreach($players as $player) 
	{
		echo "<tr style='font-size: $size%'>";
		$number++;
		
		if($number>50 & $size>=50)
		{
		  $size -= .5;  //Decrease the font size.
		}
		
		$name = $player[1];
		$race = $player[2];
		$level = $player[3];
		$exp = $player[4];
		$death = $player[5];
		$location = $player[6];
		$hp = $player[7];
		$sp = $player[8];
		$gr = $player[9];
		echo "<td>$number</td><td>$name the $race</td><td style='text-align: center;'>$level</td><td align='right'>$exp</td><td style='text-align: center;'>$hp</td><td style='text-align: center;'>$sp</td><td style='text-align: center;'>$gr</td>";
		echo "</tr>\n";
	}
	
	echo "</table>\n";
?>

				</div>


