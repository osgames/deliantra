<div id="content">
	<h2>GDE - Gtk+ Deliantra (Map) Editor</h2>
	
	<p>
	 GDE is a map editor for Deliantra.
	</p>
	
	<p>
	 It is written in Perl and uses Gtk2 as GUI toolkit.
	</p>
	
	
	<h2>Screenshots</h2>
	
	<a href="/sc/gce.png">
	 <img border="0" src="/sc/tb_gce.png"  alt="sc/gce.png" />
	</a>
	
	<a href="/sc/gce2.png">
	 <img border="0" src="/sc/tb_gce2.png"  alt="sc/gce2.png" />
	</a>
	
	<h2>Help/Guide</h2>
	
	<p>
	 There is a manual included in GDE. You can reach it via the <em>Help</em>
	 entry in the main window.
	</p>
	
	
	<h2>Download</h2>
	
	<p>
		 For contributors we provide a development VM, which comes with all tools
		 preinstalled, check it out: <a href="deliantra_vm.html">Deliantra Development VM</a>.
	</p>
	
	<p>
		 If you are interested in building the editor yourself you might want to checkout
		 the CVS respository of it, which you will find on the <a href="contriube.html">Contribute Page</a>.
	</p>
	
	<h2>Bugs/Suggestions/Ideas</h2>
	
	<p>
	 Please send bug reports, suggestions and ideas to 
	 <a href="mailto:elmex@ta-sa.org">elmex@ta-sa.org</a>.
	</p>
	
	<h2>Authors/Contact</h2>
	
	<p> <b>The Deliantra Development Team:</b> </p>
	
	<p>
		 <p>
			<b>All of the editor GUI:</b><br />
			Robin Redeker <a href="mailto:elmex@ta-sa.org">elmex@ta-sa.org</a>
			<a href="http://www.ta-sa.org/">http://www.ta-sa.org/</a>
		 </p>
	
		 <p>
			<b>The Deliantra map handling module and map widget:</b><br />
			Marc Lehmann <a href="mailto:schmorp@schmorp.de">schmorp@schmorp.de</a>
			<a href="http://home.schmorp.de/">http://home.schmorp.de/</a>
		 </p>
	</p>
	
	<p></p>
	
	<h2>News</h2>
	
	<table border="1">
	 <tr>
		<th valign="top" width="20%">
		 30.09.2008 18:00
		</th>
		<td width="80%">
		 Linked to the <a href="deliantra_vm.html">Deliantra Development VM</a>,
		 where the editor is provided in pre-installed form, ready for editing.
		</td>
	 </tr>
	 <tr>
		<th valign="top" width="20%">
		 17.04.2008 06:48
		</th>
		<td width="80%">
		 A new windows version has been uploaded, with a few minor fixes here and there.
		</td>
	 </tr>
	 <tr>
		<th valign="top" width="20%">
		 14.10.2007 11:48
		</th>
		<td width="80%">
		 A better working windows version has been uploaded.
		</td>
	 </tr>
	 <tr>
		<th valign="top" width="20%">
		 12.08.2007 17:30
		</th>
		<td width="80%">
		 Another 1.2 version, no real new features but lots of bugfixes
		 and minor improvements.
		</td>
	 </tr>
	 <tr>
		<th valign="top" width="20%">
		 14.10.2006 15:13
		</th>
		<td width="80%">
		 Released version 1.2 again, rebuild binaries.
		</td>
	 </tr>
	 <tr>
		<th valign="top" width="20%">
		 9.8.2006 15:13
		</th>
		<td width="80%">
		 Released version 1.2, minor bugfixes and features added.
		</td>
	 </tr>
	 <tr>
		<th valign="top" width="20%">
		 21.4.2006 21:12
		</th>
		<td width="80%">
		 Released new binaries of version 1.1, with a bug fixed where the archetypes
		 weren't correctly found when starting GDE.
		</td>
	 </tr>
	 <tr>
		<th valign="top" width="20%">
		 20.4.2006 11:16
		</th>
		<td width="80%">
		 Released new binaries of version 1.1, with two bugs fixed: stack view didn't
		 update correctly under bigtiles and the msg/lore textview in the attribute
		 editor was broken.
		</td>
	 </tr>
	 <tr>
		<th valign="top" width="20%">
		 20.4.2006 06:40
		</th>
		<td width="80%">
		 Released new binaries of version 1.1.
		</td>
	 </tr>
	 <tr>
		<th valign="top" width="20%">
		 05.4.2006 02:13
		</th>
		<td width="80%">
		 Released version 1.1 (Features: Better inventory editor and context menus
		 in map window and pick windows).
		</td>
	 </tr>
	 <tr>
		<th valign="top" width="20%">
		 02.4.2006 18:20
		</th>
		<td width="80%">
		 Released version 1.0 (Features: Type-documentation in attr. editor and
		 autojoining of walls).
		</td>
	 </tr>
	 <tr>
		<th valign="top" width="20%">
		 29.3.2006 14:40
		</th>
		<td width="80%">
		 Released new binaries, where a bug with wall placement was fixed.
		</td>
	 </tr>
	 <tr>
		<th valign="top" width="20%">
		 26.3.2006 15:00
		</th>
		<td width="80%">
		 Released new binaries.
		</td>
	 </tr>
	 <tr>
		<th valign="top" width="20%">
		 21.3.2006 24:00
		</th>
		<td width="80%">
		 Created website for GDE and uploaded version 0.9 (beta)
		</td>
	 </tr>
	</table>
	
</div>
