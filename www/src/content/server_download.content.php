		<div id="content" style='min-height: 270px;'>
			<h2>The Deliantra MORPG Server</h2>
			<p>
			Here you find the current releases of the Deliantra server engine, resources and maps.
			</p>

			<h3>Release: 3.00 (Mon May 31 20:26:32 CEST 2010)</h3>

			<p>
			Major feature enhancement and bugfix release.
			</p>

			<p>
			Changes can be reviewed at <a href="http://cvs.schmorp.de/deliantra/server/Changes?pathrev=rel-3_0">Changes for 3.0</a>.

			There are really a lot of changes. Here only a very condensed version:

			<ul>
			<li>The transport routes have been rearranged completely.</li>
			<li>Added a scripted gravedigger to navar.</li>
			<li>Added the rat house quest from Dustfinger.</li>
			<li>Added new apartment and Wassar city made by LinuxLemoner.</li>
			<li>Added the mining skill.</li>
			<li>Rebalanced the jeweler skill.</li>
			<li>Singing skill was fixed a bit.</li>
			<li>Fixed the "ranged" item concept for a more deterministic player experience.</li>
			<li>Many huge and small balancing changes.</li>
			<li>And many bug fixes.</li>
			<li><a href="http://cvs.schmorp.de/deliantra/server/Changes?pathrev=rel-3_0">And many more changes!</a></li>
			</ul>

			</p>

			<p><b>Download:</b></p>

			<p><a class="button" href="http://dist.schmorp.de/deliantra/deliantra-server-3.0.tar.bz2">Deliantra Server 3.0</a></p>
			<p><a class="button" href="http://dist.schmorp.de/deliantra/deliantra-maps-3.0.tar.bz2">Deliantra Maps 3.0</a></p>
			<p><a class="button" href="http://dist.schmorp.de/deliantra/deliantra-arch-3.0.tar.bz2">Deliantra Archetypes 3.0</a></p>


			<p> <b>
			   See the <a href="http://cvs.schmorp.de/deliantra/server/README?pathrev=rel-3_0">README</a>
			   for further instructions and requirements!<br />
			</b> </p>

			<!--
			<h3>Release: 2.93 (Fri Feb 26 17:28:38 CET 2010)</h3>

			<p>
			This is a major bugfix and feature enhancement release.
			</p>

			<p>
			Changes can be reviewed at <a href="http://cvs.schmorp.de/deliantra/server/Changes?pathrev=rel-2_93">Changes for 2.93</a>.

			Here are some highlights:

			<ul>
			<li>Some map fixes: tindervale maps have been improved as well as the travel to valdor.</li>
			<li>Fixed a data corruption bug in the map code.</li>
			<li>The safe perl compartment is disabled by default for map code, and it has been extended to provide loop constructs.</li>
			<li>Poison traps in doors work again.</li>
			<li>Fixed map swapout under high load to stil swap out some maps.</li>
			<li>The mail box code was improved to not block the server anymore.</li>
			<li>'make install' now removes old extensions from the server directory.</li>
			</ul>

			</p>

			<p> <b>
			   See the <a href="http://cvs.schmorp.de/deliantra/server/README?pathrev=rel-2_93">README</a>
			   for further instructions and requirements!<br />
			</b> </p>

			<ul>
			   <li>Server code: <a class="button" href="http://dist.schmorp.de/deliantra/deliantra-server-2.93.tar.bz2">deliantra-server-2.93.tar.bz2</a></li>
			   <li>Maps: <a class="button" href="http://dist.schmorp.de/deliantra/deliantra-maps-2.93.tar.bz2">deliantra-maps-2.93.tar.bz2</a></li>
			   <li>Archetypes: <a class="button" href="http://dist.schmorp.de/deliantra/deliantra-arch-2.93.tar.bz2">deliantra-arch-2.93.tar.bz2</a></li>
			</ul>

			<h3>Release: 2.92 (Sat Dec 26 01:07:53 CET 2009)</h3>

			<p>
			This is a minor bugfix release.
			</p>

			<p>
			Changes can be reviewed at <a href="http://cvs.schmorp.de/deliantra/server/Changes?pathrev=rel-2_92">Changes for 2.92</a>.

			Here are some highlights:

			<ul>
			<li>Fixed memory coruption in the destruction spell.</li>
			<li>Fixed some issues with login of players.</li>
			<li>Some warrior archetype names were fixed.</li>
			<li>The wizpass command no longer makes the wizard non-blocking.</li>
			</ul>

			</p>

			<p> <b>
			   See the <a href="http://cvs.schmorp.de/deliantra/server/README?pathrev=rel-2_92">README</a>
			   for further instructions and requirements!<br />
			</b> </p>

			<ul>
			   <li>Server code: <a class="button" href="http://dist.schmorp.de/deliantra/deliantra-server-2.92.tar.bz2">deliantra-server-2.92.tar.bz2</a></li>
			   <li>Maps: <a class="button" href="http://dist.schmorp.de/deliantra/deliantra-maps-2.92.tar.bz2">deliantra-maps-2.92.tar.bz2</a></li>
			   <li>Archetypes: <a class="button" href="http://dist.schmorp.de/deliantra/deliantra-arch-2.92.tar.bz2">deliantra-arch-2.92.tar.bz2</a></li>
			</ul>

			<h3>Release: 2.91 (Tue Dec  1 17:07:09 CET 2009)</h3>

			<p>
			This is a major feature enhancement and bugfix release.
			</p>

			<p>
			Changes can be reviewed at <a href="http://cvs.schmorp.de/deliantra/server/Changes?pathrev=rel-2_91">Changes for 2.91</a>.

			Here are some highlights:

			<ul>
			<li>Item numer and volume limis are now enforced when a player drops something.</li>
			<li>The gorokh_final maps from Crossfire have been applied.</li>
			<li>All permanent stat potions have been replaced by random ones.</li>
			<li>Diseases could sometimes lead to half-dead Zombie monsters.</li>
			<li>Shop specialisation calculations have been fixed.</li>
			<li>Fixed magic mapping.</li>
			<li>Map handling has been optimized.</li>
			<li>Pet monster levels are now displayed correctly.</li>
			<li>Hanuk altar has been fixed.</li>
			<li>Added marble shop floor.</li>
			<li>Unrecognised cfpod directives could cause undefined behaviour.</li>
			<li>Added boes' marble shop to steinwandstadt.</li>
			<li>Fixed jeweler experience output.</li>
			<li>Added lock/unlock commands, thanks for the patch go to Shawn Robinson!</li>
			<li>And many more cleanups and optimizations.</li>
			</ul>

			</p>

			<p> <b>
			   See the <a href="http://cvs.schmorp.de/deliantra/server/README?pathrev=rel-2_91">README</a>
			   for further instructions and requirements!<br />
			</b> </p>

			<ul>
			   <li>Server code: <a class="button" href="http://dist.schmorp.de/deliantra/deliantra-server-2.91.tar.bz2">deliantra-server-2.91.tar.bz2</a></li>
			   <li>Maps: <a class="button" href="http://dist.schmorp.de/deliantra/deliantra-maps-2.91.tar.bz2">deliantra-maps-2.91.tar.bz2</a></li>
			   <li>Archetypes: <a class="button" href="http://dist.schmorp.de/deliantra/deliantra-arch-2.91.tar.bz2">deliantra-arch-2.91.tar.bz2</a></li>
			</ul>


			<h3>Release: 2.90 (Thu Oct 29 23:49:14 CET 2009)</h3>

			<p>
			This is a major feature enhancement and license cleanup release.
			</p>

			<p>
			Changes can be reviewed at <a href="http://cvs.schmorp.de/deliantra/server/Changes?pathrev=rel-2_90">Changes for 2.90</a>.

			Here are some highlights:

			<ul>
			<li>
			   Licensing issues have been cleared up: The modifications done by the
			   Deliantra development team are under Affero GPL. The remaining parts of the crossfire
			   project are still under GPL.
			</li>
			<li>Magic mapping on the world map has been enhanced with a flying bird.</li>
			<li>Added maps:
			   <ul>
				  <li>lostwages and lorak (valdor/varsia) from cfextended.</li>
				  <li>added lostwages and celvear apartments to rent system.</li>
				  <li>closed guild houses, this is not scalable, ask your friendly dm instead.</li>
				  <li>troll canyon, pygmy forest, elven moon and temple of justice mapsets
					  from crossfire, together with associated archetypes.</li>
				  <li>add extended region descriptions for some regions from crossfire.</li>
				  <li>all changes to mlab between 2006-08-01 and 2009-10-28
					  and most archetypes that go with them.</li>
				  <li>gotischerbereich from cfextended.</li>
			   </ul>
			<li>doors/containers can now use "match ..." expressions for the slaying field.</li>
			<li>properly close client-side dialogue when the player is teleported away.</li>
			<li>map-tags did not properly close it's database tables or wait
				for a quiescent state, causing memory corruption on reload.</li>
			<li>fixed and improved many plurals in the archetypes.</li>
			<li>bigchests are now properly non-unique, per_player and no_reset.</li>
			<li>fly_high movement gives better los, ignoring blocksview.</li>
			<li>added many more buildable marble floors.</li>
			<li>major namespace cleanup: save 45kb by making functions static
				that were unnecessarily global.</li>
			</ul>

			</p>

			<p> <b>
			   See the <a href="http://cvs.schmorp.de/deliantra/server/README?pathrev=rel-2_90">README</a>
			   for further instructions and requirements!<br />
			</b> </p>

			<ul>
			   <li>Server code: <a class="button" href="http://dist.schmorp.de/deliantra/deliantra-server-2.90.tar.bz2">deliantra-server-2.90.tar.bz2</a></li>
			   <li>Maps: <a class="button" href="http://dist.schmorp.de/deliantra/deliantra-maps-2.90.tar.bz2">deliantra-maps-2.90.tar.bz2</a></li>
			   <li>Archetypes: <a class="button" href="http://dist.schmorp.de/deliantra/deliantra-arch-2.90.tar.bz2">deliantra-arch-2.90.tar.bz2</a></li>
			</ul>

			<h3>Older Releases</h3>

			<p>
			Older releases are not available anymore due to unclear licensing.
			</p>

-->
		</div>
