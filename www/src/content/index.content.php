			<div id="imageBox">
				<img id="featuredImage1" src='images/dragon1.png' />
				<img id="featuredImage2" src='images/treasure.png' />
			  <div id="directionArrows">
			  	<img id="rightArrow" src='style/rightArrow.png' />
					<img id="leftArrow" src='style/leftArrow.png' />
			  </div>
			</div>
		<div id="content" style='min-height: 270px;'>
		  <div id='imageDescription1'>
		  	<h2>Welcome to the World of Deliantra!</h2>
				<p>
					Deliantra is a thrilling MMORPG in a retro, pixel art world where magic and monsters
					abound.
				</p>
				<p>
					Deliantra is free to download, and free to play.  There aren't any premium accounts, and
					you don't have to pay money to get decent equipment.  This free software (some say 'open source') game was built by
					players like you, and is made available free of charge because we love to play it, and
					hope that you will too. Please continue your tour of Deliantra
					by clicking on the arrows above.
				</p>
		  </div>
			<div id='imageDescription3' style='display: none;'>
		  	<h2>Find Valuable Treasure</h2>
				<p>
					Deliantra's dungeons are packed with valuable treasure to reward the hard-working
					adventurer.  You might have to fight dangerous foes or figure out tough puzzles, but
					remember: the harder the dungeon the bigger the reward.
				</p>
				<p>
					What will you find in Deliantra's dungeons?
					Some dungeons reward you with rare equipment and artifact weapons, others with shiny gems 
					that can be sold at your local gem shop for gold or platinum coins.  Sometimes you might even
					find flawless gems that will earn you a royalty or two!
				</p>
		  </div>
		  <div id='imageDescription2' style='display: none;'>
		  	<h2>Fight Powerful Monsters</h2>
				<p>
					Deliantra has hundreds of different monsters, each with its own special strengths
					and weaknesses.  Some monsters are best fought in melee combat, while others can only be destroyed
					by powerful spells.  In addition, some magical monsters attack with spells, requiring you to
					protect yourself from magic.
				</p>
				<p>
					The special resistances of different monsters require you to learn new skills or team up
					with other players.  For example, if you only have fire attacks you can't do much damage to
					a red dragon which has 100% fire resistance.
				</p>
		  </div>
		  <div id='imageDescription4' style='display: none;'>
		  	<h2>Go Exploring</h2>
				<p>
					Deliantra has a vast world of over 4000 maps to explore.  As you explore the large continents you will
					discover new monsters, and new landscapes that vary from snowy ice fields, to parched deserts and lush
					jungles.
				</p>
				<p>
					Each continent has a capital city that you can visit to buy equipment, sell loot, and start quests
					that will take you back out into the wilderness.
				</p>
		  </div>
		</div>
		<div id="trailer">
			<h2>Watch The Trailer</h2>
			<iframe width="560" height="349" src="http://www.youtube.com/embed/-4DwRDsTnys" frameborder="0" allowfullscreen></iframe>
		</div>
		<div id="recent_news">
		<a href="news.html"><h2>Recent News:</h2></a>
			<?php
				include "news.top.html.inc";
			?>
		</div>
		<div id="banners">
		<a href="http://www.topgamedb.com/in/4.html" title="Vote For us"><img src="http://www.topgamedb.com/images/vote.gif" alt="Vote For Us" /></a>
		</div>
