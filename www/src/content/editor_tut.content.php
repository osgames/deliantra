<div id='content'>

<div class="tutorial">
<h2>Deliantra map making and server installation tutorial for Linux</h2>

<p>
So, you don't know much about deliantra and or programming, but
you want to make maps? This Tutorial will explain how to
install the Deliantra server (from CVS) on your linux system
and making your first maps. If you are stuck somewhere, please
<a href="contact.html">contact</a> us (the developers).
</p>

<p> <em>
   Note: I am assuming here that you are using a <tt>bash</tt>-like shell. And that you are logged in as root.
   If you want to install and setup everything from a regular user account you have to take care of all permission
   handling yourself.
</em> </p>

<h3>Step 1: Getting the map editor and setting it up</h3>

<p>
Now download the editor! You can either install all the Perl modules and stuff by yourself
or just choose the binary: <a href="editor.html">editor download page</a>.
I'm not going to discuss the installation procedure for the CVS here, as it requires much
knowledge about your system and how to install (perl) software from source. If you only want to
make maps, the binary will do very fine (on x86 systems).
</p>

<p> So, the next thing is to download the editor. </p>

<pre>
root@localhost: ~/deliantra# wget http://dist.schmorp.de/gde/gce-gnu-linux-x86.bin
</pre>

<p>There it is, now we make it an executable and run it:</p>

<pre>
root@localhost: ~/deliantra# chmod a+x gce-gnu-linux-x86.bin
</pre>

<p>
Now we make a start script like for the server and run it
<em>Note: the first startup may take some while, the editor has to read all the archetypes and graphics.</em>:
</p>

<pre>
root@localhost: ~/deliantra# echo "CROSSFIRE_LIBDIR=/opt/deliantra/share/deliantra/ ./gce-gnu-linux-x86.bin" &gt; startgce
root@localhost: ~/deliantra# chmod a+x startgce
root@localhost: ~/deliantra# ./startgce
</pre>

<p><em>
   Note: If you run into problems when starting the editor make sure you have gtk2, glib2 and pango and cairo libraries
   correctly installed. As i don't know your linux system you would have to figure out that yourself.
</em></p> 

<p><em>
   Note 2: If you are familiar with your system i recommend to set the CROSSFIRE_LIBDIR variable in your shells startup configuration.
</em></p> 

<p>
Now i could let you run around for yourself, if you are planning to figure out stuff
yourself now, do it, but make sure you find the <b>manual of the editor in the toolbox window in the HELP menu!</b>
</p>

<p>But i guess the best thing is to explain now what to do, and how to do. The first things you will see is this:</p>

<a href="tutscrs/mainview1.png"><img src="tutscrs/mainview1_tb.jpg" alt="main view" /></a>

<p>Now open up 2 pickers, the stack view dialog and rearrange all the windows like this:</p>

<a href="tutscrs/mainview2.png"><img src="tutscrs/mainview2_tb.jpg" alt="main view with layout" /></a>

<p>
You notice that the pickers are still empty, go to the drop-down box on the top of the pickers
and select 'wall' and 'floor' from there:
</p>

<a href="tutscrs/mainview3.png"><img src="tutscrs/mainview3_tb.jpg" alt="main view with finished layout" /></a>

<p>
Now it's time to click the <em>Save Layout</em> entry in the <em>File</em> menu. It will save all the
window sizes and positions. So you don't have to make all the layout work next time again.
As you go on with map editing you maybe find other layouts more useful, try around, and of course: use more
pickers. I mostly have 3-4 pickers open to do all the stuff i want.
</p>

<a href="tutscrs/mainview4.png"><img src="tutscrs/mainview4_tb.jpg" alt="main view with finished and saved layout" /></a>

<p>Finished! You have now a layout we can work with. Go on to the next step:</p>

<h3>Step 2: Making your first map</h3>

<p>
This step will introduce some tools and shortcuts you may use on a regular basis when making maps.
</p>

<p>
First make a new map. Go to the <em>File-&gt;New</em> menu for that an click on it.
Next a dialog will open up, that will ask you for a map size. The default is 20x20. You
can adjust that like you want. But we recommend not to make maps much larger than 50x50 or 60x60.
It reduces the server load somewhat. Better use tiled maps or linked maps (with exits between them).
</p>

<p>
Click on the <em>new</em> button and the map editor will open up:
</p>

<a href="tutscrs/mapedit1.png"><img src="tutscrs/mapedit1_tb.jpg" alt="clean map editor" /></a>

<p>
You recognize the ugly color on the backgrounds of the pickers and the map editor window.
It's not configurable, and it's there to give a clear contrast to stuff on the map.
</p>

<p><b>
   Note: You can scroll in the map editor window and the pickers by holding down the middle mouse button
   and drag it. (This is similar to GIMP).
</b></p>

<p>
Now resize the map editor window a little bit and select the <em>Select</em> tool from the
left and draw a rectangle over the map with it. Next select a floor from the picker. The floor will appear in the
attribute editor.
</p>

<a href="tutscrs/mapedit2.png"><img src="tutscrs/mapedit2_tb.jpg" alt="map editor with selection" /></a>

<p>
Now look in the toolbox window on the bottom, where the tool-controls are. There are radio buttons: 
<em>place</em>, <em>erase</em> and <em>perl</em>. Place executes the place tool on every cell in the selection
and erase executes the erase tool on every cell in the selection. The perl thing is something advanced,
it runs a small perl script on every cell in the selection, it's likely to go away some day (maybe).
</p>

<p>
Select the <em>place</em> radio button, and press the <em>invoke</em> button. The selection will be filled
with the selected floor:
</p>

<a href="tutscrs/mapedit3.png"><img src="tutscrs/mapedit3_tb.jpg" alt="map editor with floor" /></a>

<p>
Next we are using the autojoining placement of walls, pick the "awall_0" tile from the picker with the walls.
Make sure the place tool is in <em>auto</em> mode.
</p>

<p>
   <em>
      Note: In future when you want to use the autojoining placement you have to take care that the
      wall tile you are using to draw ends in "_0". But some walls, like the cave walls, are just not autojoinable.
      You will have to place them yourself.
   </em>
</p>

<p>
When you picked the wall, just draw some walls like you would draw in a graphics program (like GIMP).
Draw something similiar like you see in the screenshot. With some holes in the walls.
</p>

<p>
If you make a mistake while drawing the wall just hold the <i>CTRL</i> key to temporarily switch to the
erase mode and erase all the bad walls and redraw the area. Or just <em>Edit->Undo</em> it. (Shortcut key
combination for undo is <i>CTRL+z<i> and redo is <i>CTRL+y</i>).
</p>

<a href="tutscrs/mapedit4.png"><img src="tutscrs/mapedit4_tb.jpg" alt="map editor with floor and walls" /></a>

<p> <b>to be continued...</b> </p>

<hr>

<h2>Installing the server for testing the maps</h2>


<h3>Step A: Downloading the maps and the server from CVS</h3>

<p>
I assume here you have <tt>cvs</tt> installed. (the debian shortcut would
be: <tt>apt-get install cvs</tt>).
</p>

<p>
Create a directory in your home where all your Deliantra (map) development
should take place:
</p>

<pre>
root@localhost: ~# mkdir ~/deliantra/
</pre>

<p> <em>
   NOTE: the 'root@localhost: ~#' stands for your shellprompt, where '~' is the
   current working directory. And the ~ stands for your root homedirectory, if you are working
   as user 'root' your homedirectory should be something like /root.
</em> </p>

<p> Go into that directory: </p>

<pre>
root@localhost: ~# cd ~/deliantra/
</pre>


<p>Now, we are going to get the files, first the maps:</p>

<pre>
root@localhost: ~/deliantra# cvs -z3 -d :pserver:anonymous@cvs.schmorp.de/schmorpforge co -d maps deliantra/maps
</pre>

<p>And then the archetypes (needed for building the server, and could take a while):</p>

<pre>
root@localhost: ~/deliantra# cvs -z3 -d :pserver:anonymous@cvs.schmorp.de/schmorpforge co -d arch deliantra/arch
</pre>

<p>Finnally the server source itself:</p>

<pre>
root@localhost: ~/deliantra# cvs -z3 -d :pserver:anonymous@cvs.schmorp.de/schmorpforge co -d server deliantra/server
</pre>

<p>
Now a <tt>ls -l</tt> should show something like this:
</p>

<pre>
root@localhost: ~/deliantra# ls -l
   1 drwxr-xr-x 39 root users    1192 2006-06-01 21:29 maps
   1 drwxr-xr-x 39 root users    4096 2006-05-26 12:38 arch
   1 drwxr-xr-x 17 root users    1072 2006-05-26 11:34 server
</pre>

<h3>Step B: Compiling and installing the server</h3>

<p>
Start by going into the server CVS checkout directory and just call configure (the script that
will find all the neccessary tools to compile the server). Please make sure you have
development tools like gcc (gnu c compiler) and headers installed:
</p>

<pre>
root@localhost: ~/deliantra# cd server
root@localhost: ~/deliantra/server# ln -s /root/deliantra/arch lib/arch
root@localhost: ~/deliantra/server# ./configure --prefix=/opt/deliantra/
</pre>

<p>
The prefix argument tells the configure script to choose /opt/deliantra/ as installation
destination. This is done to prevent the server installation from polluting your
system and to keep all files together.
</p>

<p> Next step is the compilation and installation of the server: </p>

<pre>
root@localhost: ~/deliantra/server# make install
</pre>

<p>
That should build the sources and install them. If you encounter problems here you could try to contact us.
If everythin went fine, there should be a <tt>/opt/deliantra/</tt> directory now with files in it.
</p>

<p>
Next step is to link the maps into the installed server directory. First go back into the parent
directory and then link the maps dir to the right location:
</p>

<pre>
root@localhost: ~/deliantra/server# cd ..
root@localhost: ~/deliantra# ln -s ~/deliantra/maps /opt/deliantra/share/deliantra/maps
</pre>

<p>
Now everything is in place and all that's left is to write a 
startup script for the server:
</p>

<pre>
root@localhost: ~/deliantra# echo "CROSSFIRE_LIBDIR=/opt/deliantra/share/deliantra/ /opt/deliantra/bin/deliantra" > startcf
root@localhost: ~/deliantra# chmod a+x startcf
</pre>

<p><em>
   Note: The CROSSFIRE_LIBDIR thing is setting an environment variable which 
   tells the server where to look for the configuration variables and the maps
</em></p>

<p>Now try to shoot it up:</p>

<pre>
root@localhost: ~/deliantra# ./startcf
</pre>

<p>
You should see lots of output running accross your screen. 
Congratulations, you just setup a Deliantra server.
You maybe want to dive into the server tree and look around in the <tt>doc/</tt>
directory to find more about the server.
</p>



</div>

</div>
