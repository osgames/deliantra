									<div id="content">
										<h2>Contact</h2>
										
										<h3>How to contact the Dungeon Masters of the game server:</h3>
										
										<p>
										Do you have a problem that needs DM interaction?
										Or did you find a bug in the Game? Or is someone behaving <em>very</em>
										bad on the game server? Feel free to contact the dungeon masters:
										</p>
										
										<p>
										<ul>
											 <li>
													You can contact them via IRC: server <tt>irc.schmorp.de</tt>
													(alternatively <tt>irc.deliantra.net</tt>), channel <tt>#cf</tt>, user(s)
													<tt>schmorp</tt> and/or <tt>elmex</tt>
											 </li>
											 <li>Or drop them a mail at <tt>support@deliantra.net</tt>.</li>
										</ul>
										
										</p>
										
										<h3>How to contact the developers of the game:</h3>
										
										<p>
											 Do you seek contact to the developers? Do you want to contribute?
											 Do you have questions? suggestions? bugs?
											 Or just want to have a nice chat? Then look here:
										</p>
										
										<p>
										<b>Stop!</b> Please make sure that your question isn't covered by a <a href="faq.html">FAQ</a> or Guide
										in the <a href="http://crossfire.real-time.com/guides/">Guides on the Crossfire Page</a>
										of the orignial Crossfire project. Some of them still apply to Deliantra.
										You may also look at the <a href="documentation.html">Deliantra documentation</a>.
										</p>
										
										<p>
											 <ul>
													<li>
														 The IRC channel, see above how to contact the DMs. The channel is the main
														 meeting room of all Deliantra developers and also users of the server.
													</li>
													<!-- <li>
														 Found a bug? Have a suggestion? Or an improvement idea?
														 Just send a mail at <tt>crossfire@schmorp.de</tt>.
														 Go to the <a href="http://deliantra/tracker/"><b>Bug Tracker</b></a> and submit it! 
													</li> -->
											 </ul>
										</p>
										
										<h3>Meet the Development Team</h3>
										
										<p><b>
													95% of the client, stuff like: protocol handling, widget toolkit, OpenGL integration, windows build,
													the map, help viewer, all the cool features and all other stuff the user never sees.
											 </b></p>
											 <p>
												Marc Lehmann <a href="mailto:schmorp@schmorp.de">schmorp@schmorp.de</a>
												<a href="http://home.schmorp.de/">http://home.schmorp.de/</a>
											 </p>
										
											 <p><b>The remaining 5%, stuff like: client theme graphics, (some) dialogs, key bindings, inventory and some 'concept' code</b></p>
											 <p>
												Robin Redeker <a href="mailto:elmex@ta-sa.org">elmex@ta-sa.org</a>
												<a href="http://www.ta-sa.org/">http://www.ta-sa.org/</a>
											 </p>
										
									</div>
