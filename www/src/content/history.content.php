		<div id="content">
			<h2>History of Deliantra</h2><br />
			<a class='button' href='about.html'>Introduction</a>
			<a class='button' href='faq.html'>FAQ</a>
			<a class='button' href='history.html'>History</a>
			<div style='clear: both; margin-bottom: 20px;'></div>
				<p>
					Deliantra is a heavily rewritten version of Crossfire. A lot of work
					has been done on the game server engine and the content.
					Not only has the server code been improved but much work has also been done to help
					balance the game.  This page lists some of the significant changes from Crossfire
					to Deliantra.
				</p>
				
				<h3>Code Changes</h3>
				
				<ul>
					 <li>
					 lots of bugfixes, mainly crashfixes
					 </li>
					 <li>
					 better handling of file saving, allowing more reliable regular backups
					 and also a more crash-safe behavoir.
					 </li>
					 <li>
					 a Perl plugin system which is actively used to improve interactivity features,
					 like the tradeshop, mana fountain, and NPC behaviour and dialog.
					 </li>
					 <li>
					 the server code has been converted to C++ and a few structs are improved with
					 C++ features (templates, methods, ...)
					 </li>
					 <li>
					 Perl has been moved further into the core of Deliantra, allowing more stable
					 and more interesting plugin extensions to the game.
					 </li>
					 <li>
					 Lots of the loading/saving code for players and maps has been rewritten/changed
					 to handle serialized Perl data.
					 </li>
					 <li>
					 The 'shutdown' command has been improved to save maps and players now before
					 exiting.
					 </li>
					 <li>
					 Updated the server with a linux-specific improvement to the socket handling,
					 The old code sent out 2 TCP packets per command, the new one will bundle them
					 into one segment. This will decrease bandwidth requirements a lot, decreases the
					 chance of packet loss a lot and greatly reduces chances for lag. As a side effect,
					 it decreases latency without the ill side-effects of disabling nagle.
					 </li>
					 <li>
					 Added Linux-specific fast timeout code that kicks players out when the client
					 doesn't ack data after 20 seconds. This makes it possible to quickly re-login
					 after a "catastrophic" even such as losing your internet conenction. Also
					 player-stealing between hostile players is possible.
					 </li>
					 <li>
					 Added a plug-in that asynchronously pre-fetches adjacent maps from disk
					 whenever a player enters a map. This make ist highly probable that maps entered by
					 the player are already in memory and comes at almost no cost (they are loaded into
					 the OS disk cache only). As a net effect, most short freezes due to map loading
					 should be gone.
					 </li>
				</ul>
				
				<h3>Balancing changes</h3>
				
				<ul>
					 <li>reduced draining effects by about a factor of 8</li>
					 <li>
					 Enabled limited playerkilling again, when both players are
					 hostile (there is no luck penalty). If one of the players isn't
					 hostile, then you can't attack him/her.
					 </li>
					 <li>
					 Rods are now limited by the use_magic_item skill of the player (+ some extra).
					 </li>
					 <li>
					 The experience table has been adjusted and made a little bit harder.
					 The 'nimbus', where you get back the lost experience compensates this a little bit.
					 </li>
					 <li>
					 Some of the treasurelists have been changed, for example nearly all
					 occurences of rings in monster loot have been replaced by gems.
					 </li>
					 <li>
					 The IPO and Bank don't use the Python plugins anymore and have been replaced by
					 more featureful Perl plugins, eg. live mail notification of the new IPO or the handling
					 of the new royalties in the bank.
					 </li>
					 <li>
					 Added 3d models for some houses and monsters, unfortunately there weren't enough
					 people who had time for modelling, so the graphics set looks more mixed now, more
					 modellers are needed.
					 </li>
					 <li>
					 Furthermore, a setting has been added that allows players to cast town portal
					 in their personal maps such as apartments, big chests in guilds etc.
					 </li>
					 <li>
					 A 'suicide' command has been added.
					 </li>
					 <li>
					 Stealing now never yields more exp than the monster has and also reduces monster experience.
					 </li>
					 <li>
					 Greatly reduced availability of skill scrolls and (to a lesser extent) enchantment scrolls,
					 to enhance class-based gameplay. Also reduced amount of generators in raffle.
					 </li>
					 <li>
					 Greatly reduced experience for playerkills. Disable friendly fire logic in the arena,
					 so you do not have to be hostile anymore just to visit the arena.
					 </li>
					 <li>
					 Put a bargaining scrolls in the Gem shop in scorn for a low price.
					 So that players can begin levelling bargaining early.
					 </li>
					 <li>
					 Monsters don't drop rings anymore, only gems and money instead.
					 Expanded the magic shops a bit, because monsters don't drop rings or spellbooks anymore.
					 </li>
					 <li>
					 Shops have now a default maximum.
					 </li>
					 <li>
					 the difficulty calculations for maps were adjusted together with the generation
					 of scrolls, rods, wands and staffs, which are now more close to the level difficulty.
					 </li>
				</ul>
				
				<h3>New systems</h3>
				
				<p>
				Some sub systems have been rewritten in Perl:
				</p>
				
				<ul>
					 <li>
					 The event objects have been completly removed from the archetypes and have been
					 replaced with an 'attach' field, which describes a linking point between extensions and
					 objects.<br />
					 Look at the <a href="http://cvs.schmorp.de/deliantra/maps/perl/">perl extensions in the cvs</a> for
					 examples.
					 </li>
				
					 <li>
					 NPCs have now a greatly increased functionality, they have a map-local states and
					 player-states. Look at the 
					 <a href="doc/development/NPC_Dialogue.html">NPC Dialogues</a>
					 documentation, which explaines the Perl interface.
					 The interface allows direct script integration, dialog trees and more.
					 </li>
				
					 <li>
					 Chat commands, NPC dialogues and lots of other things have been rewritten.
					 One new function that has already been implemented is the 'ignore' commmand.
					 One can ignore shouts/chats, tells or both from one person for a specified amount of
					 hours.
					 </li>
				</ul>
			</div>
