<div id='content'>
	<h2>What does Deliantra look like?</h2>
	
	<h3>Deliantra Client</h3>
	
	<table cellspacing="8" cellpadding="4">
		 <tr><th colspan="2" id="screenshot_note">State at 2010-04-26, with experience bars on top.</th></tr>
	
		 <tr>
		 <td>
				<a href="sc/sc_client_2010.png"><img src="sc/sc_client_2010_tb.jpg" /></a><br />
				The current look of the client, with experience progress bar on top of
				the client window. Showing the dialog with a NPC.
		 </td>
		 <td> </td>

		 </tr>

		 <tr><th colspan="2" id="screenshot_note">State at 2008-12-19, new scorn.</th></tr>
	
		 <tr>
	
		 <td>
				<a href="sc/sc_scorn_ppl.png"><img src="sc/sc_scorn_ppl_tb.jpg" /></a><br />
				A group of people meeting at the new central place in the redesigned main city Scorn.
		 </td>
		 <td> </td>
	
		 </tr>
	
		 <tr><th colspan="2" id="screenshot_note">State at 2008-04-18, mode indicators.</th></tr>
	
		 <tr>
				<td>
					 <a href="sc/deliantra_09978.png"><img src="sc/deliantra_09978_tb.jpg" /></a><br />
					 Resirs house in Scorn being looted.
				</td>
				<td>
					 <a href="sc/sc_goclub.jpg"><img src="sc/sc_goclub_tb.jpg" /></a><br />
					 The new go club with a quick 9x9 game (and mode indicators in the lower left).
				</td>
		 </tr>
	
		 <tr><th colspan="2" id="screenshot_note">State at 2007-08-23, chat tabs and experience bars.</th></tr>
	
		 <tr>
				<td>
					 <a href="sc/sc_expbars.png"><img src="sc/sc_expbars_tb.jpg" /></a><br />
					 The skill window, chat tabs and next-level bars in the lower right.
				</td>
		 </tr>
	
		 <tr><th colspan="2" id="screenshot_note">State at 2007-04-28, lots of ground faces have been changed.</th></tr>
	
		 <tr>
				<td>
					 <a href="sc/sc_oldscorn.jpg"><img src="sc/sc_oldscorn_tb.jpg" /></a><br />
					 Old Scorn as never seen before.
				</td>
				<td>
					 <a href="sc/sc_euthville.jpg"><img src="sc/sc_euthville_tb.jpg" /></a><br />
					 Euthville sure looks deserted.
				</td>
		 </tr>
	
		 <tr><th colspan="2" id="screenshot_note">State at 2007-04-13, beta version with 64x64 tileset.</th></tr>
	
		 <tr>
				<td>
					 <a href="sc/cfplus_64x64_nebel.png"><img src="sc/cfplus_64x64_nebel_tb.jpg" /></a><br />
					 Some experiment with smoothing on spell effects - foggy scorn.
				</td>
				<td>
					 <a href="sc/cfplus_new_64x64faceset.png"><img src="sc/cfplus_new_64x64faceset_tb.jpg" /></a><br />
					 The new 64x64 tileset monsters. Also with newly rendered Dreads.
				</td>
		 </tr>
	
		 <tr>
				<td>
					 <a href="sc/cfplus_new_64x64faceset2.png"><img src="sc/cfplus_new_64x64faceset2_tb.jpg" /></a><br />
					 Eastern part of Scorn with the new 64x64 tileset.
				</td>
				<td>
					 <a href="sc/cfplus_new_64x64faceset3.png"><img src="sc/cfplus_new_64x64faceset3_tb.jpg" /></a><br />
					 Western part of Scorn with the new 64x64 tileset.
				</td>
		 </tr>
	
		 <tr><th colspan="2" id="screenshot_note">State before beta.</th></tr>
	
		 <tr>
				<td>
					 <a href="sc/cfplus12.png"><img src="sc/cfplus12_tb.jpg" /></a><br />
					 The healthbars that were implemented on 2006-08-17.
				</td>
				<td></td>
		 </tr>
	
		 <tr><th colspan="2" id="screenshot_note">State at 2006-07-24 (alpha version)</th></tr>
	
		 <tr>
				<td>
					 <a href="sc/sc_alpha_npc_dialog.png"><img src="sc/sc_alpha_npc_dialog_tb.jpg" /></a><br />
					 The NPC dialogs.
				</td>
				<td></td>
		 </tr>
	
		 <tr>
				<td>
					 <a href="sc/sc_alpha_overview.png"><img src="sc/sc_alpha_overview_tb.jpg" /></a><br />
					 General overview of the client.
				</td>
				<td>
					 <a href="sc/sc_alpha_login.png"><img src="sc/sc_alpha_login_tb.jpg" /></a><br />
					 The login/server dialog.
				</td>
		 </tr>
	
		 <tr>
				<td>
					 <a href="sc/sc_alpha_command_completer.png"><img src="sc/sc_alpha_command_completer_tb.jpg" /></a><br />
					 The command completer.
				</td>
				<td>
					 <a href="sc/sc_alpha_pickup.png"><img src="sc/sc_alpha_pickup_tb.jpg" /></a><br />
					 The pickup dialog.
				</td>
		 </tr>
	
		 <tr>
				<td>
					 <a href="sc/sc_alpha_skilllist.png"><img src="sc/sc_alpha_skilllist_tb.jpg" /></a><br />
					 The skill list.
				</td>
				<td>
					 <a href="sc/sc_alpha_spelllist.png"><img src="sc/sc_alpha_spelllist_tb.jpg" /></a><br />
					 The spell list.
				</td>
		 </tr>
	
		 <tr>
				<td>
					 <a href="sc/sc_alpha_stats.png"><img src="sc/sc_alpha_stats_tb.jpg" /></a><br />
					 The stats window.
				</td>
				<td>
					 <a href="sc/sc_alpha_inventory.png"><img src="sc/sc_alpha_inventory_tb.jpg" /></a><br />
					 The inventory.
				</td>
		 </tr>
	
		 <tr><th colspan="2" id="screenshot_note">State before 2006-07-24</th></tr>
	
		 <tr>
				<td>
					 <a href="sc/cfplus11.png"><img src="sc/cfplus11_tb.jpg" /></a><br />
					 State of the client on 2006-06-15. The magic map has been implemented!
				</td>
				<td>
					 <a href="sc/cfplus10.png"><img src="sc/cfplus10_tb.jpg" /></a><br />
					 State of the client on 2006-06-15. The magic map has been implemented!
				</td>
		 </tr>
		 <tr>
				<td>
					 <a href="sc/cfplus9.png"><img src="sc/cfplus9_tb.jpg" /></a><br />
					 State of the client on 2006-06-15. The magic map has been implemented!
				</td>
				<td>
					 <a href="sc/cfplus8.png"><img src="sc/cfplus8_tb.jpg" /></a><br />
					 State of the client on 2006-06-12. A NPC-dialog system has been
					 implemented in the client (uses the new perl extendable protocol commands).
				</td>
		 </tr>
		 <tr>
				<td>
					 <a href="sc/cfplus6.png"><img src="sc/cfplus6_tb.jpg" /></a><br />
					 State of the client on 2006-06-06. The new character creation dialog.
				</td>
				<td>
					 <a href="sc/cfplus7.png"><img src="sc/cfplus7_tb.jpg" /></a><br />
					 State of the client on 2006-06-06. The new character creation dialog.
				</td>
		 </tr>
		 <tr>
				<td>
					 <a href="sc/cfplus5.png"><img src="sc/cfplus5_tb.jpg" /></a><br />
					 State of the client on 2006-06-02. Pickup configuration is now in.
					 And some other bugs in the widget system were fixed (window titles).
				</td>
		 </tr>
		 <tr>
				<td>
					 <a href="sc/cfplus4.png"><img src="sc/cfplus4_tb.jpg" /></a><br />
					 State of the client on 2006-05-30. Reworked bindings management
					 and first version of the spell dialog.
				</td>
				<td>
					 <a href="sc/cfplus3.png"><img src="sc/cfplus3_tb.jpg" /></a><br />
					 State of the client on 2006-05-29. The first version of the action recorder
					 was checked in.
				</td>
		 </tr>
		 <tr>
				<td>
					 <a href="sc/cfplus1.png"><img src="sc/cfplus1_tb.jpg" /></a><br />
					 State of the client on 2006-05-27. Mostly playable, only key-bindings
					 are missing.
				</td>
				<td>
					 <a href="sc/cfplus2.png"><img src="sc/cfplus2_tb.jpg" /></a><br />
					 The new bank-script on the server. Also from 2006-05-27.
				</td>
		 </tr>
		 <tr>
				<td>
					 <a href="sc/pclient4.png"><img src="sc/pclient4_tb.jpg" /></a><br />
					 An old test from pippijn.
				</td>
				<td>
					 <a href="sc/pclient5.png"><img src="sc/pclient5_tb.jpg" /></a><br />
					 Old screenshot from 2006-04-23.
				</td>
		 </tr>
	</table>
	
	<h3>Clients from the original Crossfire Project</h3>
	
	<table cellspacing="8" cellpadding="4">
		 <tr>
				<td>
					 <a href="sc/cf_plus_sc01.png"><img src="sc/cf_plus_sc01_tb.jpg" /></a><br />
					 The gtk1 client client. (smoothing disabled)
				</td>
		 </tr>
	</table>

</div>
