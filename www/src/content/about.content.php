  		<div id="content">
  			<h2>Welcome to Deliantra</h2><br />
  			<a class='button' href='about.html'>Introduction</a>
  			<a class='button' href='faq.html'>FAQ</a>
  			<a class='button' href='history.html'>History</a>
  			<div style='clear: both; margin-bottom: 20px;'></div>
				<div class="sidebox">
					<b>Deliantra is Free</b>
					<p>
						Deliantra is <a href='http://www.fsf.org/resources/what-is-fs'>free software</a> (some say 'open source').
					</p>
					<p>
						In addition, there are no premium accounts. You don't
						have to pay money to access the rest of the world or to
						get decent equipment for your character.
					</p>
  			</div>
				<p>
					Deliantra is a cooperative multiplayer RPG and adventure game set in a classical medieval
					environment with ubiquitious weapons and magic. The game play is quite similar in style
					and setting to the venerable games Nethack and Moria. Deliantra, however, is fully
					graphical and runs in realtime.
				</p>
				<p>
					What makes Deliantra special is its depth and the sheer amount of maps to explore.
					The world is vast, with over 4000 lovingly hand-designed maps organized into multiple
					continents.  There are hundeds of monsters to fight, including epic bosses lurking in the
					depths of Deliantra's dungeons.  With 16 unique races, 17
					classes, 40 skills, and hundreds of spells to choose from and combine with each other, gameplay
					in Deliantra is complex and full-featured.  The gameplay is also very open and sets very
					few limits on character development. Of course, the best part about gameplay in an MMORPG like
					Deliantra is the fun that is to be had by playing cooperatively with
					others exploring the same world.
				</p>
				<h2>Gameplay Details</h2>
				<p>
					The official Deliantra server is designed to make gameplay fun and engaging. Because of
					this death is not permanent, and it causes no stat depletion or loss of items to other players.
					Although there is a slight experience point penalty when you die, if you survive the test of gods you can
					even get away with no experience loss.  Currently the test of the gods is the classic game
					Minesweeper, so if you are careful you can die without any loss of experience at all.
				</p>
				<p>
					In addition, game play is tailored to make cooperation easy and enjoyable.  Friendly spell
					fire causes no damage, so you can fire area of effect spells without wounding your
					friends.  In addition, if you summon pets or monsters to help you they will not block or
					attack your friends.
				</p>
				<p>
					For those who like PvP it is possible to set your player state to hostile and fight with
					other players who have done the same.  However, everyone starts out in a friendly state that
					provides invulnerability from other players, even if they have their player state set to
					hostile.
				</p>
				<p>
					Even though it is free Deliantra is also reliable.  The server is monitored 24 hours a day and
					will be automatically restarted in case of problems.  If you do experience problems the
					admins can be reached via IRC: server irc.schmorp.de (alternatively irc.plan9.de),
					channel #schmorp, user(s) schmorp or elmex.
					Or you can reach the admins by sending email to: support@deliantra.net.
					For extra protection player files and other data is backed up every 15 minutes to a remote
					location.  Backup history for the past 3 months is available.
				</p>
  		</div>


