			<div id="content">
				<h2>Recent Deliantra Players</h2>
				
				<p>
					This list is updated every ten minutes to show who is online.
				</p>

<?php

	/**
	* array timeDiff(int $t1, int $t2)
	* $t1 and $t2 must be UNIX timestamp integers, order does not matter
	* returns array broken down into years/months/weeks/etc.
	*/
	function timeDiff($t1, $t2)
	{
		 if($t1 > $t2)
		 {
				$time1 = $t2;
				$time2 = $t1;
		 }
		 else
		 {
				$time1 = $t1;
				$time2 = $t2;
		 }
		 $diff = array(
				'years' => 0,
				'months' => 0,
				'weeks' => 0,
				'days' => 0,
				'hours' => 0,
				'minutes' => 0,
				'seconds' =>0
		 );
		 
		 foreach(array('years','months','weeks','days','hours','minutes','seconds')
					 as $unit)
		 {
				while(TRUE)
				{
					 $next = strtotime("+1 $unit", $time1);
					 if($next < $time2)
					 {
							$time1 = $next;
							$diff[$unit]++;
					 }
					 else
					 {
							break;
					 }
				}
		 }
		 return($diff);
	}

	function timeDifferenceText($pastDate)
	{ 	
		$rightNow = time();
		$difference = timeDiff($rightNow,$pastDate);
		
		if($difference['years']>0)
		{
			if($difference['years']==1)
			{
				$difference = " 1 year ago";	  	  
			}
			else
			{
				$difference = $difference['years'] . " years ago";
			}
		}
		else if($difference['months']>0)
		{
			if($difference['months']==1)
			{
				$difference = " 1 month ago";	  	  
			}
			else
			{
				$difference = $difference['months'] . " months ago";
			}
		}
		else if($difference['weeks']>0)
		{
			if($difference['weeks']==1)
			{
				$difference = " 1 week ago";	  	  
			}
			else
			{
				$difference = $difference['weeks'] . " weeks ago";
			}
		}
		else if($difference['days']>0)
		{
			if($difference['days']==1)
			{
				$difference = "1 day ago";	  	  
			}
			else
			{
				$difference = $difference['days'] . " days ago";
			}
		}
		else if($difference['hours']>0)
		{
			if($difference['hours']==1)
			{
				$difference = " 1 hour ago";	  	  
			}
			else
			{
				$difference = $difference['hours'] . " hours ago";
			}
		}
		else if($difference['minutes'])
		{
			if($difference['minutes']==1)
			{
				$difference = " 1 minute ago";	  	  
			}
			else
			{
				$difference = $difference['minutes'] . " minutes ago";
			}
		}
		else
		{
			$difference = " Just now";
		}
		
		return $difference;
	}

	//Generate the highscore table.
	
	$jsonScoresUnparsed = file_get_contents("recent.json");
	
	$jsonArray = json_decode($jsonScoresUnparsed);
	
	$timestamp = $jsonArray->date;
	$version = $jsonArray->version;
	$players = $jsonArray->data;
	
	echo "<table style='width: 100%'>\n";
	echo "<tr><th>Player Name</th><th>Last Login</th><th>Last Logout</th><th>Birthdate</th><th style='text-align: right;'>Logins</th><th style='text-align: right;'>Deaths</th><th>OS</th><th>Go</th></tr>";
	
	$number = 0;
	$size = 100;
	foreach($players as $player) 
	{
		echo "<tr style='font-size: $size%'>";
		$number++;
		
		if($number>50 & $size>=50)
		{
		  $size -= .5;  //Decrease the font size.
		}
		
		$name = $player[0];
		$birthdate = $player[1];
		$lastLogin = $player[2];
		$loginCount = $player[3];
		$lastLogout = $player[4];
		$client = $player[5];
		$deaths = $player[6];
		$location = $player[7];
		
/*
		//A quick PHP side filter.  If needed uncomment.

		$ancientTest = timeDiff(time(),$lastLogin);

		if($ancientTest['months']>0)
		{
		  break;  //Haven't logged in for a month.... they aren't exactly recent or online.
		}
*/
		
		if($deaths==="?")
		{
		  $deaths = 0;
		}
		
		$loginDifference = timeDifferenceText($lastLogin);
		if($lastLogout==null)
		{
		  $logoutDifference = "Currently online";
		}
		else
		{
			$logoutDifference = timeDifferenceText($lastLogout);
		}
		if($birthdate==null)
		{
			$birthdateDifference = "Before known time";
		}
		else
		{
			$birthdateDifference = timeDifferenceText($birthdate);
		}
		
		if(stripos($client,"darwin")!==FALSE)
		{
		  $operatingSystem = "mac";
		}
		else if(stripos($client,"unix")!==FALSE)
		{
		  $operatingSystem = "mac";
		}
		else if(stripos($client,"MSWin32")!==FALSE)
		{
			$operatingSystem = "win";
		}
		else if(stripos($client,"linux")!==FALSE)
		{
			$operatingSystem = "lin";
		}
		
		if($logoutDifference==="Currently online")
		{
		  $loggedInColor = "#96FFB2";
			echo "<td style='background-color: $loggedInColor;'>$name</td><td style='background-color: $loggedInColor;'>$loginDifference</td><td style='background-color: $loggedInColor;'>$logoutDifference</td><td style='background-color: $loggedInColor;'>$birthdateDifference</td><td style='background-color: $loggedInColor;' align='right'>$loginCount</td><td style='background-color: $loggedInColor;' align='right'>$deaths</td>\n";
			echo "<td align='center' style='background-color: $loggedInColor;'><img title='$client' style='width: 20px; height: 20px;' src='images/$operatingSystem.png' /></td>\n";
			
			if(stripos($location,"~")!==FALSE or stripos($location,"?random")!==FALSE)
			{
				echo "<td align='center' style='background-color: $loggedInColor;'><img title='$name is on a map you can not see: $location' style='width: 20px; height: 20px;' src='images/no.png' /></a></td>\n";
			}
			else
			{
				echo "<td align='center' style='background-color: $loggedInColor;'><a href='http://maps.deliantra.net$location'><img title='$location' style='width: 20px; height: 20px;' src='images/arrow.png' /></a></td>\n";
			}
		}
		else
		{
			echo "<td>$name</td><td>$loginDifference</td><td>$logoutDifference</td><td>$birthdateDifference</td><td align='right'>$loginCount</td><td align='right'>$deaths</td>\n";
			echo "<td align='center'><img title='$client' style='width: 20px; height: 20px;' src='images/$operatingSystem.png' /></td>\n";
			
			if(stripos($location,"~")!==FALSE or stripos($location,"?random")!==FALSE)
			{
				echo "<td align='center'><img title='$name is on a map you can not see: $location' style='width: 20px; height: 20px;' src='images/no.png' /></a></td>\n";
			}
			else
			{
				echo "<td align='center'><a href='http://maps.deliantra.net$location'><img title='$location' style='width: 20px; height: 20px;' src='images/arrow.png' /></a></td>\n";
			}
		}
		
		echo "</tr>\n";
	}
	
	echo "</table>\n";
?>

				</div>


