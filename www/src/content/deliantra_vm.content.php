<div id="content">

<h2>The Deliantra development VM</h2>

<h3>What is it?</h3>
<p>
   It's a virtual computer which comes with GNU/Linux as operating system
   and all necessary packages for Deliantra development pre installed: Map
   editor, server, all maps and archetypes. You can immediately edit and
   create maps on it, create or edit archetypes and test them easily with
   the installed server.
</p>

<h3>What do I need and where to get it?</h3>
<p>
   <ul>
   <li>
      First you will need something to run a virtual VMware image: If
      you don't already have the VMware workstation, VMware server or
      something like that installed we recommend to use the free-of-charge
      <a href="http://www.vmware.com/products/player/">VMware Player</a>.
      Download and install the right package for your system.
   </li>
   <li>
      The only other thing you need to download is our <a
      href="http://data.schmorp.de/deliantra-vm.rar">Deliantra
      development VMware image (deliantra-vm.rar)</a>. Download the
      file <tt>deliantra-vm.rar</tt>, unpack it (use <tt>unrar</tt> or
      <tt>winrar</tt>) and run it with the VMware player.
   </li>
   <li>
      <p>
         You should see a screen with boot messages of the linux system scrolling up.
         A few seconds or minutes later you should finally see a graphical desktop similiar to this:
      </p>
      <p>
         <img src="images/deliantra_vm_player.jpg" alt="deliantra VM run by VMware player" />
      </p>
   </li>
   </ul>
</p>

<h2>How do I...</h2>

<h3>...start and operate the test server?</h3>

<p>
   <b>First notice the "Terminal" window you get after the virtual machine has
   booted. It contains some valuable gems of information about the VM. You can
   always get back to that screen by opening a new terminal and type in the
   <tt>info</tt> command!</b> It shows you which IP address the virtual machine,
   and thus the test server, has. Remember that IP address, you will need it to
   connect to the test server after it has started.
</p>

<p>
   To start up the server there is a button on the right side of the
   desktop called "Start Server". Clicking it once will open a terminal
   window on the bottom of the screen with the server log messages
   scrolling through. The very first time you start the server it scans
   all maps, which can bring your machine down to its knees for a
   minute. If you want to stop the server you can press <tt>CTRL+C</tt> in
   that terminal window or simply hit the "close the window" button in
   the upper right corner. If you want to restart the server you can just
   click on the <tt>Start Server</tt> button again.
</p>

<p>
When the server is starting up wait a while until the messages in the server log window
have calmed down a bit.
</p>

<p>
   Next start up the Deliantra client and enter the IP address of the virtual
   machine in the "Host:Port" field in the "Server" tab of the Setup dialog
   in the client.  Next choose a character name and password for your test
   character (the server has no registered players) Log in and create the
   character.
</p>

<p>
In the test server everybody can become Dungeon Master by issuing the
command '<tt>dm</tt>'. As dungeon master you can use special commands that
manipulate the world, create items and walk and see through walls. This is a
small cheat sheet of useful DM commands you are able to use as dungeon master:
<ul>
   <li><tt>dm</tt> - become dungeon master</li>
   <li><tt>nodm</tt> - drop dungeon master privileges</li>
   <li><tt>goto &lt;path&gt;</tt> - become dungeon master<br />
      <p>
         For example to go to the inn in scorn you could use:<br />
         <tt>goto /scorn/taverns/inn</tt>
      </p>
   </li>
   <li><tt>reset .</tt> or <tt>reset &lt;path&gt;</tt> - resets the current map or the map with the path <tt>&lt;path&gt;</tt><br />
      <p>
         Resetting maps is necessary if you for example changed a map and
         want to load the changes from disk to the running server. Or if you
         messed it up somehow and want to reset it.
      </p>
      <p>
         <ul>
            <li>
               For example to reset the current map your DM character is on
               use:<br />
               <tt>reset .</tt>
            </li>
            <li>
               Or to reset the inn in scorn use:<br />
               <tt>reset /scorn/taverns/inn</tt>
            </li>
         </ul>
      </p>
   </li>

   <li><tt>create &lt;number&gt; &lt;archetype name&gt;</tt> - create an object from an archetype<br />
      <p>
         Some examples:<br />
         <ul>
            <li><tt>create 100 platinacoin</tt></li>
            <li><tt>create 1 dog hp 1000 name "strong dog"</tt></li>
         </ul>
      </p>
      <p>
         Regular objects will be put into your inventory and monsters are put
         on the ground where your DM character is.
      </p>
   </li>
   <li><tt>addexp &lt;player name&gt; &lt;amount of XP points&gt; [&lt;skill name&gt;]</tt> - adds experience to your overall experience or to the experience of a skill<br />
      <p>
         Some examples:<br />
         <ul>
            <li><tt>addexp elmex 1000000</tt> - boosts the overall experience of the character 'elmex' to level 15</tt></li>
            <li><tt>addexp elmex 1000000 punching</tt> - boosts the 'punching' skill experience of the character 'elmex' to level 15</tt></li>
         </ul>
      </p>
   </li>
</ul>
</p>

<h3>... navigate the file system?</h3>

<p>
The virtual machine is a regular (a bit stripped down, for smaller size) Debian
GNU/Linux distribution. If you are not familiar with using terminals and shells
to browse the file system here are some hints that might help: <a
href="http://tldp.org/LDP/GNU-Linux-Tools-Summary/html/using-filesystem.html">Moving around the filesystem</a>
(from the GNU Linux Tools Summary of the Linux documentation Project).
</p>

<p>
If you don't have the energy to look into that or still feel more comfortable with a graphical file manager
we recommend XFE. You can install it by issuing the following commands in a terminal window (an internet
conenction is required, as this will download the required packages from there):<br />
</p>
<ul>
<li><tt>apt-get update</tt></li>
<li><tt>apt-get -y install xfe</tt></li>
</ul>
<p>
And then you can start the XFE by typing <tt>xfe</tt> in a terminal window or use the
appliaction menu on the  "Other ..." to start it (you will find it in the menu Applications =&gt; File mangagement).
</p>

<h3>... find other interesting application packages?</h3>

<p>
Here is a list of other probably interesting applications you might want to install:<br />
<b>NOTE: </b> Remember to run <tt>apt-get update</tt> <i>before</i> using <tt>apt-get install</tt>!
</p>

<ul>
   <li><tt>apt-get -y install synaptic</tt> - graphical Debian package browser (if you don't like apt-get)</li>
   <li><tt>apt-get -y install gimp</tt> - The GNU Image manipulation Program - if you want to edit or create images</li>
</ul>

<h3>... stay up to date to Deliantra development?</h3>

<p>
As time passes there are going to be changes on the server, the maps
and/or the archetypes of Deliantra. To update your VM to the latest
state of our CVS, you just have to click on the "Update" button on the right.
It will automatically update the server, the maps and the archetypes on the VM.
This might take some time.
</p>

<p>
However, if you made local changes (for example on the maps or archetypes) you
might get conflicts when updating. If you get those conflicts, you will have to
sort them out yourself. (We recommend you to become more familiar with CVS, see <a href="http://ximbiot.com/cvs/manual/">CVS manuals</a>).
</p>

</div>