		<div id="content" style='min-height: 270px;'>
			<h2>The Deliantra MORPG Client</h2>

			<p>This is the Deliantra client which was written from scratch
			in Perl with C bindings to SDL/OpenGL. It was written to have a more
			immersive overall gameplay and to base as ground to implement new
			features.
			</p>

			<h3>Screenshots:</h3>

			<p>Screenshots? See on the <a href="screenshots.html">Screenshot page</a></p>

			<h3>Download:</h3>

			<p><a href="play.html">See the Play now! page</a> about the downloads!</p>

			<p>
			<b>For further instructions and informations look at the <i>Help!</i> button on the top!</b>
			</p>

			<h3>Building notes:</h3>
				<p>
				If you want to build the client from source you are mostly on your
				own. You might have success trying the CPAN shell and giving the command <tt>install Deliantra::Client</tt>.
				Otherwise you are encouraged to checkout the CVS that is linked to
				on the <a href="contribute.html">Contribute Page</a>.
				</p>
			</ul>

			<h3>News:</h3>

			<p>
			  <em>
			   2010-04-25 Version 2.11: Spell list has been rearranged to show more
			   interesting information and worked around some music related crashes.
			  </em>
			</p>

			<p>
			  <em>
			   2010-01-15 Version 2.10: Fixed the blending of the fog of war.
			  </em>
			</p>

			<p>
			  <em>
			   2009-12-22 Version 2.09: The fog of war and lighting transitions are now
			   smooth. Also the fog of war texture is customizable now and a crash with bad
			   user input in the "Host" field has been fixed.
			  </em>
			</p>


			<p>
			  <em>
			   2009-12-05 Version 2.06: The Apple/NVIDIA font problem has finally been fixed!
			   Also some other OpenGL driver bugs have a work around now.
			  </em>
			</p>

			<p>
			  <em>
			   2009-09-16 Version 2.05: The command completer algorithm has been improved by
			   marcmagus. Thanks for the contribution! Also a small bugfix for a crash
			   in the audio engine has been added.
			  </em>
			</p>

			<p>
			  <em>
			   2009-04-26 Version 2.04 is a small bugfix and feature enhancement release of
			   the client. A Japanese font is included with the distribution now and
			   a workaround of the incompatible ABI change in pango (font engine) has been
			   implemented. Another bugfix related to connecting to the server 
			   went into the windows release.
			  </em>
			</p>

			<p>
			  <em>
			   2009-01-15 First release in the new year, version 2.03 comes with a
			   many changes in the GUI. For example the auto pickup toggle has been
			   moved into a checkbox to the top of the main window.
			   The configuration is now saved automatically on exit.
			   Shimmering during smooth scroll has been reduced a lot and some displaying
			   bugs have been fixed in the inventory. More workarounds which make the
			   client more stable on broken OSes were also implemented.
			  </em>
			</p>

			<p>
			  <em>
			   2008-12-26 The Chrismas release of version 2.02: Hidden mapspaces are
			   marked visually by a question mark and are made as bright as the darkest
			   mapspaces. A crash has been fixed when the user clicks on the map during
			   login. New tips of the day have been introduced. And more minor stability and
			   visual fixes.
			  </em>
			</p>

			<p>
			  <em>
			   2008-12-12 Version 2.01: The client tutorial has been rewritten, to match
			   the GUI changes of the last months. The random switching between
			   message window tabs when a new tab was opened has been fixed.
			   Fixed minor displaying bugs of the speech bubble of NPCs and other
			   minor things.
			  </em>
			</p>

			<p>
			  <em>
			   2008-12-05 Version 2.0: Last minor fixes round up the version 2.0 of our
			   client: The Log window can't be closed anymore by Alt+x, Also wont Alt+x or
			   other Message window operations crash the client anymore directly after startup.
			   Also the invisible/cached parts of the map are darkened even more, so that
			   invisibile parts can be distinguished from dark parts better.
			  </em>
			</p>


			<p>
			  <em>
			   2008-11-09 Version 0.9978: Some minor fixes:
			   The healthbar is now correctly smoothly moved. Own text in NPC dialogues are
			   emphasized now. A crash bug in the macro dialogue has been fixed. The tooltips
			   in the Mesage Window tabs are updated correctly now. And Alt+x won't let
			   Message Window tabs disappear for ever anymore.
			  </em>
			</p>

			<p>
			  <em>
			   2008-09-29 Version 0.9977: A race condition has been fixed that could lead
			   to client freezes or crashes on login, especially on slower systems. The
			   smooth movement is no longer choppy. The default font for messages is now a
			   (hopefully nicer looking) proportional font. Text display has been tweaked.
			   The client no longer makes unnecessary screen refreshes and uses less CPU
			   time.
			  </em>
			</p>

			<p>
			  <em>
				2008-09-05 Next version of the client: 0.9976 comes with many fixes for Mac OS X,
				like some fixed keyboard issues on OS X. Improved the protocol with compression
				and fragmentation, leading to more efficent bandwidth usage. Implemented crash
				diagnostics, which are sent to the server, for easier debugging. Important fix
				with the default resolution, which was too small. Added theming support, with some
				basic themes. And some other minor fixes all over the place.
				</em>
			</p>

			<p>
			  <em>
				2008-08-27 Version 0.9975 comes with a small fix with the initial resolution and
				a carification of the weight/value autopickup option. There is also a <b>Mac OS X</b>
				port available now of the client!
			  </em>
			</p>

			<p>
			  <em>
				2008-08-01 Version 0.9974 comes with smooth scrolling for player movements,
				workaround for a possible OpenGL slowdown, and other small fixes and enhancements.
			  </em>
			</p>

			<p>
			  <em>
				2008-07-07 Some minor fixes and features: IPv6 support, chat tabs have tooltips
				again and converted to a new non-blocking API. Also fixed the CPAN distribution.
			  </em>
			</p>

			<p>
			  <em>
				2008-05-23 Next release: 0.9972. The map cache has been extended a bit to store more
				map changes (fixes some minor bugs). Fixed some crashbugs when the client is
				logged out and some UI elements were used. The server XML format is now also
				supportet in item tooltips. The keyrepeats are ignored now in some conditions.
				And crashes when the map cache is corrupted are now prevented. Also the client
				configuration is now stored in pretty printed JSON.
			  </em>
			</p>

			<p>
			  <em>
				 2008-05-05 Long time no release, but now we got some fixes for you: Some issues
				 with the default sort order have been sorted out, some tooltips were fixed,
				 a long standing bug with the stats tab being empty has been resolved and some GUI
				 fixes have been done. Also the pseudo-items are now only shown in the inventory
				 and not the floorbox. Have fun!
			  </em>
			</p>

			<p>
			  <em>
				 2008-03-30 Release of version 0.9970: Reduced documentation memory usage, fixed
				 the stat gauge tooltips that have gone missing since 0.9966. Fixced a grave bug
				 with tile id allocation, which effectively disabled the tile cache. And last
				 but not least: The NPC Dialogs have also been fixed.
			  </em>
			</p>

			<p>
			  <em>2008-03-24 Release of version 0.9967 of the Client: A memory leak was fixed
				  where the messages in the message window were kept for ever and the client
				  consumed more and more memory. The highlighting of the chattabs is now red and
				  not blue. The history handling of the chat input will now not erase the
				  typed input if the down cursor key is pressed. A keyboard modifier display has
				  also been implemented, (for example to signal whether numlock is on,
				  which caused problems under windows). Also a bug with the layout font under
				  windows has been fixed.
			   </em>
			</p>

			<p>
			  <em>
				 2008-01-19 Another release of the Client, version 0.9965: Some bugs
				 have been fixed: A race condition was found in the database access and
				 a bug which crashed the client with nvidia drivers has been fixed.
				 Some minor bugs in the GUI, the message fontsize and chatbox label padding,
				 have also been fixed.
			  </em>
			</p>

			<p>
			  <em>
				 2008-01-06 First release in the new year, version 0.9963: Among the
				 changes are some minor bugfixes, optical tweaks and performance
				 improvements on the GUI. Some to the user invisible changes were
				 improvements of the server side widgets and a reimplementation of the
				 message window to allow to undock chats from the central message window in
				 future releases.
			  </em>
			</p>

			<p>
			  <em>
				2007-12-25 Released version 0.9960 with some important fixes in the item animations
				and GUI code, along with an API update to a more recent EV version the documentation
				had some minor improvements. And last but not least the face loading consumes less CPU.
				too.
			  </em>
			</p>
			<p>
			  <em>
				2007-12-09 Released version 0.9959, main changes were a bugfix of the
				audio-jingle playing (sometimes a jingle was played repeatedly if there was no
				server-provided music available). Also some debugging hooks were added and
				the code was updated to follow API changes in some of the modules used.
			  </em>
			</p>

			<p>
			  <em>
				2007-11-27 Released version 0.9956, main changes were an improved event loop
				for better performance and compatibility under windows,
				and the server list has been disabled due to problems with activestate perl.
			  </em>
			</p>

			<p>
			 <em>
				2007-11-03 Released version 0.9955, Main changes were adjustment of the default
				resolution (to 800x600) and making fullscreen the default. The widget extension
				system was also officially enabled.
			 </em>
			</p>


			<p>
			 <em>
				2007-10-14 Released a new binary. Main feature: Rename to 'Deliantra'.
			 </em>
			</p>

			<p>
			 <em>
				2007-09-02 <b>Finally!</b> We released a non beta version of the client.
				The version 0.99 is considered mostly stable and playable now. Changes
				from the last release are: Better compatibility with OpenGL drivers,
				better face caching, background music and spatial sound effects,
				more default keymappings and the item descriptions are in the tooltips
				now (eg. in the inventory).
			 </em>
			</p>


			<p>
			 <em>
				2007-05-09 <b>Big news!</b> There is a <b>Linux binary</b> now too! Get it and play with it! :-)
			 </em>
			</p>

			<p>
			 <em>
				2007-04-24 Next windows beta with following changes is out:
				Better character creation dialog and item descriptions in the inventory
				are now in the tooltip rather than spread over the message window.
			 </em>
			</p>

			<p>For older news look at the bottom of this page.</p>

			<h3>Features:</h3>

			<p>
			These are the features that are already implemented and work (minus bugs).
			</p>

			<p><b>Technical:</b></p>
			<ul>
				<li>Runs on GNU/Linux and Windows.</li>
				<li>
					Uses hardware acceleration if it exists (but also runs without as
					long as you have OpenGL).
				</li>
				<li>
					Windowed/Fullscreen mode in multiple resolutions 
					with scaling invariant user interface.
				</li>
				<li>Smoothing and fog of war.</li>
				<li>Very nice font rendering with pango (gtk's font renderer).</li>
				<li>Sound and background music.</li>
				<li>Face prefetching for the game graphics.</li>
				<li>Integrated documentation browser with linking and images.</li>
				<li>Server side widgets (these are server side defined dialogues).</li>
			</ul>

			<p><b>Gameplay:</b></p>
			<ul>
				<li>
				   A minimap which keeps track of explored territories
				   and helps you to navigate on the worldmap.
				</li>
				<li>Easier key binding with the ability to <b>record</b> actions.</li>
				<li>Gained experience is shown in the statusbox.</li>
				<li>Reporting of stat-changes (stats, resistancies and other stuff) in the statusbox.</li>
				<li>Online wiki style help.</li>
				<li>Message window entry with history.</li>
				<li>
				   Command completer, eg. completes 'cbh' to 'cast burning hands', which
				   eases the input of commands.
				</li>
				<li>Command repetition with '.'.</li>
				<li>
				   There are tooltips all over the GUI which help a lot.
				</li>
				<li>Spell list with key-binding shortcuts and tooltip descriptions of the spells.</li>
				<li>Skill list with quick key binding.</li>
				<li>
				   NPC dialog system makes chatting with NPCs a lot easier and
				   improves overall game interaction.
				</li>
				<li>Inventory operation shortcuts, like: ignite/thaw, inscribe and rename.</li>
				<li>Healthbars above monsters and players.</li>
			</ul>

			<h3>News archive</h3>

			<p>
			 <em>
				2007-04-22 Another windows beta has been released: A problem with map-ghosting
				was fixed and other bugs werde fixed. Also a texture cache workaround for buggy
				opengl drivers was implemented (which could reduce the memory leak inside the
				driver).
			 </em>
			</p>

			<p>
			 <em>
				2007-04-14 Another windows beta has been released: Support for smoothing
				on crossfire+ servers has been implemented along with the support for the
				large 64x64 tileset!
			 </em>
			</p>

			<p>
			 <em>
				2007-01-25 Next beta released to the world: loads of bugfixes, added
				'bind apply ...' to the context menu of the inventory, improved local
				database handling, rework of the whole keybindings (much easier now!).
			 </em>
			</p>

			<p>
			 <em>
				2006-12-03 Released next beta, further bug fixes. Pickup flesh has been implemented too.
			 </em>
			</p>

			<p>
			 <em>
				2006-11-07 Another beta release: The off-by-one bug in the map displaying has finally been found
				and fixed! This hopefully also fixes other problems with the map :) Have fun and please tell us
				about your experience with the CFPlus client!
			 </em>
			</p>

			<p>
			 <em>
				2006-09-29 New beta is out! Changes: minor bugfixes, added documentation and a 'tip of the day' dialog.
			 </em>
			</p>

			<p>
			 <em>
				2006-09-21 Another update of the cfplus.exe is online now. Changes: Fix in the NPC speech bubble code.
			 </em>
			</p>

			<p>
			 <em>
				2006-09-20
				And again: A <b>new CFPlus beta</b> is out! The ghosts on maps have been fixed, along with other
				minor bugfixes. NPC speech bubbles were also added!
			 </em>
			</p>

			<p>
			 <em>
				2006-08-14 
				A <b>new CFPlus beta</b> is out! <b>Healthbars</b> are in!!! Players and Monsters now
				have a healthbar on top of them, which shows their actual health status.
				This renders the 'probe' spell mostly useless, but we will find other ways to
				replace it!
			 </em>
			</p>

			<p>
			 <em>
				2006-08-14 
				The <b>next CFPlus beta release is out!</b>: Inscription and renaming shortcuts have been
				added to the Inventory. The interface got line frames to be more eye pleasing.
				On top of that the documentation browser has been greatly improved and the documentation
				improved also a bit. And other minor stuff has been implemented and fixed.
			 </em>
			</p>

			<p>
			 <em>
				2006-07-30
				The long awaited beta release! The last bugs we wanted to fix and features we wanted
				to add are done and CFPlus is ready for the first more serious testings. Please make sure to read the
				documentation and the tooltips if there is something not yet clear. Although the documentation
				is far from complete at the moment, it might be helpful.
			 </em>
			</p>

			<p>
			 <em>
				2006-07-24
				Lots of detail work was done now, and there are still some minor issues that
				need to be cleared until the client is ready for a beta release. We were working mainly 
				on the documentation the last two days. There is light on the end of the tunnel.
			 </em>
			</p>

			<p>
			 <em>
				2006-07-03
				Finally: The alpha release! Lots has changed since 2006-06-12 and
				we are happy to present the first test release!
			 </em>
			</p>

			<p>
			 <em>
				2006-06-12
				The new NPC dialog system has been added, and character creation
				helper dialogs. Along with many other bugfixes and improvements.
			 </em>
			</p>

			<p>
			 <em>
				2006-06-02
				Added pickup configuration. The window titles work again and
				some other bugs and enhancements went into the gui toolkit.
				Shift-middle click lock/unlocks items in inventory now. First version
				of the spell dialog is in and a context menu item has been added to the
				inventory to allow to get/drop a specified count of items.
			 </em>
			</p>
		</div>
