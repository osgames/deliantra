﻿<div id="content">
			<h2>Frequently Asked Questions</h2><br />
			<a class='button' href='about.html'>Introduction</a>
			<a class='button' href='faq.html'>FAQ</a>
			<a class='button' href='history.html'>History</a>
			<div style='clear: both; margin-bottom: 20px;'></div>

<ul>
	<li>
		<a href="#FAQ_Tips_and_Tricks">FAQ, Tips and Tricks</a>
		<ul>
			<li>
				<a href="#Meta_About_the_Game_and_the_Client">Meta - About the Game and the Client</a>
				<ul>
					<li><a href="#What_is_the_relation_between_Deliant">What is the relation between Deliantra and Crossfire?</a></li>
					<li><a href="#I_found_a_bug_how_can_I_report_it_wh">I found a bug, how can I report it/what do I do?</a></li>
					<li><a href="#The_client_is_very_sluggish_and_slow">The client is very sluggish and slow, what can I do about this?</a></li>
					<li><a href="#The_client_doesn_t_correctly_react_t">The client doesn't correctly react to keypresses or mouseclicks.</a></li>
					<li><a href="#My_client_doesn_t_start_anymore_grap">My client doesn't start anymore/graphics are corrupted!</a></li>
			</ul>
		</li>
<li><a href="#Game_Mechanics">Game Mechanics</a>
<ul><li><a href="#What_is_this_place_I_go_to_when_I_di">What is this place I go to when I die?</a></li>
<li><a href="#I_lose_health_but_I_can_t_see_why_Or">I lose health, but I can't see why! Or how do I cure diseases/depletion/poison?</a></li>
<li><a href="#How_do_diseases_work">How do diseases work?</a></li>
<li><a href="#I_lost_an_item_How">I lost an item! How?</a></li>
<li><a href="#How_do_I_uncurse_undamn_items">How do I uncurse/undamn items?</a></li>
<li><a href="#When_I_log_in_I_keep_dying_Why_is_th">When I log-in, I keep dying! Why is this?</a></li>
<li><a href="#Where_do_I_find_beds_to_reality">Where do I find beds to reality?</a></li>
<li><a href="#How_does_the_i_identify_i_spell_work">How does the <i>identify</i> spell work?</a></li>
<li><a href="#What_do_altars_do">What do altars do?</a></li>
<li><a href="#How_come_I_don_t_get_any_change_from">How come I don't get any change from an altar?</a></li>
<li><a href="#What_diseases_cannot_be_cured_natura">What diseases cannot be cured naturally?</a></li>
<li><a href="#Where_can_I_use_building_materials">Where can I use building materials?</a></li>
<li><a href="#How_can_I_Prepare_Improve_Enchant_We">How can I Prepare/Improve/Enchant Weapons?</a></li>
<li><a href="#How_can_I_enchant_Armour">How can I enchant Armour?</a></li>
<li><a href="#What_is_this_item_power_business">What is this item power business?</a></li>
</ul>
</li>
<li><a href="#Generic_how_do_I">Generic how do I...</a>
<ul><li><a href="#How_do_I_uncurse_undamn_items_or_unw">How do I uncurse/undamn items or unwear cursed/damned items?</a></li>
<li><a href="#How_do_I_use_range_weapons_such_as_b">How do I use range weapons such as bows?</a></li>
<li><a href="#How_do_I_thaw_icecubes_Or_How_do_I_i">How do I thaw icecubes? Or: How do I ignite a torch?</a></li>
<li><a href="#How_do_I_cast_a_cone_spell_all_aroun">How do I cast a cone spell all around me (burning hands, holy word etc.)?</a></li>
<li><a href="#How_do_I_pay_for_items_from_a_shop">How do I pay for items from a shop?</a></li>
<li><a href="#How_do_I_sell_items_to_a_shop">How do I sell items to a shop?</a></li>
<li><a href="#How_do_I_increase_my_attributes">How do I increase my attributes?</a></li>
<li><a href="#How_do_I_get_my_literacy_experience_">How do I get my literacy experience up?</a></li>
<li><a href="#How_do_I_rent_and_maintain_an_apartm">How do I rent and maintain an apartment?</a></li>
<li><a href="#How_do_I_use_passes_keys">How do I use passes/keys?</a></li>
<li><a href="#How_do_I_find_out_where_the_key_is_t">How do I find out where the key is to a locked door?</a></li>
<li><a href="#How_do_I_see_how_much_an_altar_or_ta">How do I see how much an altar or table costs to use?</a></li>
<li><a href="#How_do_I_join_a_guild">How do I join a guild?</a></li>
<li><a href="#How_do_I_drop_items">How do I drop items?</a></li>
<li><a href="#How_do_I_pick_up_items">How do I pick up items?</a></li>
<li><a href="#How_do_I_make_money_fast">How do I make money, fast?!</a></li>
</ul>
</li>
<li><a href="#Playing_Together">Playing Together</a>
<ul><li><a href="#How_can_I_communicate_with_other_pla">How can I communicate with other players?</a></li>
<li><a href="#Can_I_form_parties_Which_benefits_do">Can I form parties? Which benefits does party-play have?</a></li>
</ul>
</li>
<li><a href="#Magic">Magic</a>
<ul><li><a href="#How_does_an_improvement_potion_work">How does an improvement potion work?</a></li>
<li><a href="#How_do_I_control_golems_I_summon">How do I control golems I summon?</a></li>
<li><a href="#How_can_I_summon_a_specific_monster_">How can I summon a specific monster with <code>summon pet monster</code>?</a></li>
<li><a href="#How_does_being_attuned_repelled_deni">How does being attuned/repelled/denied affect casting level?</a></li>
<li><a href="#What_is_the_quot_effective_quot_cast">What is the &quot;effective&quot; casting level?</a></li>
</ul>
</li>
<li><a href="#Praying_Gods_and_Cults">Praying, Gods and Cults</a>
<ul><li><a href="#How_do_I_pray_or_get_the_praying_ski">How do I pray or get the praying skill?</a></li>
<li><a href="#How_can_I_summon_a_specific_monster_-3">How can I summon a specific monster with <code>summon cult monster</code>?</a></li>
<li><a href="#How_do_I_join_a_cult_become_a_follow">How do I join a cult/become a follower of a god?</a></li>
<li><a href="#How_do_I_change_gods">How do I change gods?</a></li>
</ul>
</li>
<li><a href="#Exploring_Quests">Exploring / Quests</a>
<ul><li><a href="#What_areas_are_there_to_explore">What areas are there to explore?</a></li>
<li><a href="#Where_is_that_damn_Goblin_Chief">Where is that damn Goblin Chief?</a></li>
<li><a href="#Where_can_I_find_the_i_comet_i_spell">Where can I find the <i>comet</i> spell?</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="#Authors">Authors</a>
</li>
</ul>
<!-- INDEX END -->

<h1 id="FAQ_Tips_and_Tricks">FAQ, Tips and Tricks</h1>
<div id="FAQ_Tips_and_Tricks_CONTENT">
<p>Here are some questions you might asked yourself over time.
Also some nice tips and tricks are listed here, which you may find informative.</p>

</div>
<h2 id="Meta_About_the_Game_and_the_Client">Meta - About the Game and the Client</h2>
<div id="Meta_About_the_Game_and_the_Client_C">

</div>
<h3 id="What_is_the_relation_between_Deliant">What is the relation between Deliantra and Crossfire?</h3>
<div id="What_is_the_relation_between_Deliant-2">
<p>Deliantra was originally a Crossfire server, but by now most of the code
has been rewritten. Still, most maps are very similar or identical to
Crossfire maps, so the games share a lot.</p>
<p>The major differences are a better client, better graphics, a stable
server (i.e. no crashes every few hours with players and maps getting
reset) and the ability to let more players play, by using less resources
and having a fully asynchronous design, and full support for sound effects
and background music.</p>

</div>
<h3 id="I_found_a_bug_how_can_I_report_it_wh">I found a bug, how can I report it/what do I do?</h3>
<div id="I_found_a_bug_how_can_I_report_it_wh-2">
<p>Mail us at support@deliantra.net , which is best, unless it is a small,
temporary, or urgent issue. If it is, then tell schmorp, elmex, or Asarth.</p>

</div>
<h3 id="The_client_is_very_sluggish_and_slow">The client is very sluggish and slow, what can I do about this?</h3>
<div id="The_client_is_very_sluggish_and_slow-2">
<p>Most likely, you don't have accelerated OpenGL support. Try to find a
newer driver, or a driver from your hardware vendor, that features OpenGL
support.</p>
<p>If this is not an option, the following Setup options reduce the load and
will likely make the client playable with software rendering (it will
still be slow, though):</p>
<dl>
	<dt>* <strong>Video Mode</strong> should be set as low as possible (e.g. 640x480)</dt>
	<dt>* Enable <strong>Fast &amp; Ugly</strong> mode</dt>
	<dt>* Disable <strong>Fog of War</strong> and <code>Map Smoothing</code></dt>
	<dt>* Increase <strong>Map Scale</strong></dt>
</dl>

</div>
<h3 id="The_client_doesn_t_correctly_react_t">The client doesn't correctly react to keypresses or mouseclicks.</h3>
<div id="The_client_doesn_t_correctly_react_t-2">
<dl>
	<dt>Make sure Numlock is off.</dt>
	<dd>
		<p>Some Microsoft Windows versions/keyboard layouts do not report modifiers for the
numerical keypad keys while Numlock is active.</p>
	</dd>
	<dt>Make sure no other Modifier is &quot;pressed&quot;.</dt>
	<dd>
		<p>Mircosoft Windows versions have some accessability features that could
interfere with games like Deliantra. You might want to switch these off in
the keyboard settings. Sometimes modifiers can be &quot;unstuck&quot; by pressing
them down once.</p>
	</dd>
	<dt>Fullscreen switches confuse the keyboard modifiers.</dt>
	<dd>
		<p>Fullscreen switches on Microsoft Windows sometimes confuse which keys are
pressed and which aren't. Doing another switch to/from fullscreen might
help.</p>
	</dd>
</dl>

</div>
<h3 id="My_client_doesn_t_start_anymore_grap">My client doesn't start anymore/graphics are corrupted!</h3>
<div id="My_client_doesn_t_start_anymore_grap-2">
<p>Sometimes you hit a bug in the client. To reset the state of your client
you can delete your client database directory (your client will have to
re-download music and images and you will lose your minimap contents).</p>
<p>To do that, on Unix:</p>
<pre>   rm -rf ~/.deliantra/client-*

</pre>
<p>On windows:</p>
<p>Do <code>Start =&gt; Run</code>, enter <cite>%APPDATA%</cite>, press return, and in the
window that will open, first open <cite>deliantra</cite>, then delete the directory
starting with <cite>client-</cite>.</p>
<p>Alternatively you can delete the <cite>.deliantra</cite> or <cite>deliantra</cite> directory
itself, but this will also delete your settings and keybindings.</p>

</div>
<h2 id="Game_Mechanics">Game Mechanics</h2>
<div id="Game_Mechanics_CONTENT">

</div>
<h3 id="What_is_this_place_I_go_to_when_I_di">What is this place I go to when I die?</h3>
<div id="What_is_this_place_I_go_to_when_I_di-2">
<p>When you die, you go to a place called the nimbus. In the nimbus, you are
able to play minesweeper for your experience. If you win and have applied
all the spaces except for bombs, you will automatically be teleported to
a hallway you can walk down to be teleported back to life, with your
experience intact.</p>
<p>If you lose by applying a bomb, you are teleported to a hallway
with a flower, and you can walk down to be teleported back to life,
without your experience you lost when you died. There are rumors
that the Key to Life can let the losers walk in the winner's
hallway, however...</p>

</div>
<h3 id="I_lose_health_but_I_can_t_see_why_Or">I lose health, but I can't see why! Or how do I cure diseases/depletion/poison?</h3>
<div id="I_lose_health_but_I_can_t_see_why_Or-2">
<p>Here are the typical reasons why you lose health:</p>
<dl>
	<dt>* You are being attacked.</dt>
	<dd>
		<p>Sounds obvious, but sometimes you can't see the monster attacking
you. Check the <code>Log</code> tab, as you usually get a message when this happens.</p>
	</dd>
	<dt>* You are out of food.</dt>
	<dd>
		<p>When your food reaches <code>0</code> you will start to lose health. Eating (good)
food will replenish your food level.</p>
	</dd>
	<dt>* You are poisoned.</dt>
	<dd>
		<p>Poison will reduce your health. When your body heals fast enough it will
eventually get over the poison, otherwise you should seek professional
help such as the House of Healing in Scorn, or a player with the <code>cure
poison</code> spell.  It is rumored that neko san might sometimes cure poison.</p>
	</dd>
	<dt>* You have a disease.</dt>
	<dd>
		<p>Some diseases have such drastic effects on your organism that you can lose
health. Get help in the House of Healing in Scorn, or ask a player who can
cast <code>cure disease</code> on you.</p>
	</dd>
	<dt>* Your stats change.</dt>
	<dd>
		<p>Switching items or being restored from depletion can affect your stats,
which in turn can affect your healthpoints. While this is not a regular
effect (it only happens when you change items or are restored from
depletion), it can sometimes look as if you are losing health. You can
restore depletion by drinking a potion of life.</p>
	</dd>
</dl>

</div>
<h3 id="How_do_diseases_work">How do diseases work?</h3>
<div id="How_do_diseases_work_CONTENT">
<p>Diseases are obtained by things like traps or spells. Usually, they
will reduce some of your attributes, like strength. They might also
cause damage to you, or slow you down. Some of them infect monsters
and players around you, spreading the disease.</p>
<p>If your disease can be cured naturally, and if you do wait until
it is cured naturally, you develop immunity for that level of
disease. Higher level forms of that disease will still infect
you, but you can wait it out to get better immunity.</p>
<p>Some diseases, like rabies, cannot be cured by waiting.
Instead, you will have to cure it magically. For information on how
to do that, see the above entry.</p>

</div>
<h3 id="I_lost_an_item_How">I lost an item! How?</h3>
<div id="I_lost_an_item_How_CONTENT">
<p>Well, we don't know. If you don't know, either, here is a partial list of
ways people typically lose stuff:</p>
<dl>
	<dt>You accidentally dropped it in a shop (or elswehere).</dt>
	<dd>
		<p>If in a shop, if it was money, you can just go there and pick it up again:
shops do not &quot;buy&quot; money. For other items you will have to buy it back.</p>
	</dd>
	<dt>Thieves or monsters stole your items.</dt>
	<dd>
		<p>Yes, thieves and many other monsters (such as demons) can actually steal
stuff. They can steal about anything that you don't currently wield, and
if they are good, you don't even notice it. Your only chance of getting
the item back is to kill the thief or steal the item back with the steal
skill before the map he is in resets.</p>
		<p>Many adventurers find that the old road to Navar harbours a lot of thieves
and is best avoided by the beginning adventurers.</p>
	</dd>
</dl>

</div>
<h3 id="When_I_log_in_I_keep_dying_Why_is_th">When I log-in, I keep dying! Why is this?</h3>
<div id="When_I_log_in_I_keep_dying_Why_is_th-2">
<p>What happened is that you died of a cave-in. Cave-ins kill you when you
unsafely log out. An unsafe log out is when you disconnect without using a
bed to reality, or when you time out (after 10-20 seconds without reply,
the server automatically kicks players). Safe log outs are when you log
out using a bed to realtiy, or when you are disconnected when the server
crashes or restarts.</p>
<p>If the disconnection was safe, then you will appear where you were before
you got logged out. If it was unsafe, and more than one hour has passed, you
will die of a cave-in. If the map you were in reset, but one hour has not
passed, then you will be recalled to your bed to reality.</p>

</div>
<h3 id="Where_do_I_find_beds_to_reality">Where do I find beds to reality?</h3>
<div id="Where_do_I_find_beds_to_reality_CONT">
<p>Inns usually have them, and apartments almost always have beds to
reality. Almost every city has an inn. Taverns sometimes have them, and
they might be in surprising places. Scorn has an inn that is located in
the southern area.</p>

</div>
<h3 id="How_does_the_i_identify_i_spell_work">How does the <i>identify</i> spell work?</h3>
<div id="How_does_the_i_identify_i_spell_work-2">
<p>The <i>identify</i> spell identifies your items, making it give it's full
description. The spell goes through your inventory in a psuedo-random
order, skipping past identified items. If it identifies all the items
in your inventory, or they are already identified, it then checks for
identified items on the ground, again in a psuedo-random order. If it
identifies all the items on the ground, then the remaining power is
wasted.</p>
<p>The <i>identify</i> spell counts stacks of items, such as arrows or gems, as
one 'item'. It can only identify a certain amount of items. Therefore, the
only way to choose what it identifies is by dropping what you don't want
identified on the ground.</p>

</div>
<h3 id="What_do_altars_do">What do altars do?</h3>
<div id="What_do_altars_do_CONTENT">
<p>There are different types of altars. Altars found in churches and
chapels are aligned to gods; they usually have decoration based
on the god they are aligned to. You can pray at these altars to 
start worshipping the god the altar is aligned to, or pray to
get over your normal grace limit, get grace faster, or any
other effects the altar might have.</p>
<p>Pink altars usually require a sacrifice, which can be seen by
rolling your mouse over it at the bottom of the screen. If you
drop the sacrifiice, it will activate something, like a gate, or
maybe it will teleport something.</p>

</div>
<h3 id="How_come_I_don_t_get_any_change_from">How come I don't get any change from an altar?</h3>
<div id="How_come_I_don_t_get_any_change_from-2">
<p>Altars do not give change. This means that you might drop a platinum piece
on a table that identifies for 2 gold, or equivalent, and get only one
identify. The equivalent means that you can make the price up in smaller
currency, like 200 silver, but higher currency won't get you any more
identifies.</p>

</div>
<h3 id="What_diseases_cannot_be_cured_natura">What diseases cannot be cured naturally?</h3>
<div id="What_diseases_cannot_be_cured_natura-2">
<p>Tapeworms, leprosy, and arthritis are all unable to be cured naturally,
and thus you cannot have an immunity against them.</p>

</div>
<h3 id="Where_can_I_use_building_materials">Where can I use building materials?</h3>
<div id="Where_can_I_use_building_materials_C">
<p>You can only use them in tiles that are buildable. These tiles are found in
guild storages, and in some apartments.</p>

</div>
<h3 id="How_can_I_Prepare_Improve_Enchant_We">How can I Prepare/Improve/Enchant Weapons?</h3>
<div id="How_can_I_Prepare_Improve_Enchant_We-2">
<p>This is done in two steps: first you have to <i>prepare</i> your weapon
for the desired number of enchantments, then you apply the enchanments
(weapons remember how many times they can be enchanted further).</p>
<p>To prepare a weapon, wield it and <cite>mark</cite> (e.g. using the popup menu in
the inventory) it. Then, as a sacrifice, drop some diamonds on the floor
then read the <i>Prepare Weapon</i> scroll. The square root of the total 
number of diamonds sacrificed this way determines the number of 
enchantments the weapon accepts: one diamond for one enchanment, nine 
diamonds for three enchantments, 100 diamonds for ten enchantments, 
and so on.</p>
<p>Remember, once you prepare the weapon, it can only be wielded by you.</p>
<p>After that, you can apply improvement and enchantnment scrolls:</p>
<dl>
	<dt>Improve Damage</dt>
	<dd>
		<p>Each scroll read will increase the damage by five points, and likewise the
weight by five kilograms.</p>
	</dd>
	<dt>Lower Weight</dt>
	<dd>
		<p>This &quot;improves&quot; the weight of your weapon: each scroll reduces the weight
by one fifth (20%). It will not, however, create weightless weapons.</p>
	</dd>
	<dt>Enchant Weapon</dt>
	<dd>
		<p>Each scroll read increases the magic by one point.</p>
	</dd>
	<dt>Improve Stat</dt>
	<dd>
		<p>This scroll improves one of the basic stat improvements (Strength,
Dexterity and so on). For this it needs a further sacrifice in form of
stat potions. The stat potions must be of the same type and will determine
which stat gets improved.</p>
		<p>The number of stat potions you need to improve it is twice the number of
stat points the weapon currently gives, plus one (the minimum is two stat
potions, however). That is, a weapon which gives no stat bonus needs two
(<code>0 × 2 + 1 = 1</code> which is less than two, so two) stat potion, one that
already gives <i>Str +2</i> and <i>Int +1</i> will need seven (<code>3 × 2 + 1</code>) stat
potions. Negative stats are summed normally, so <i>Str +4</i> and <i>Int -2</i>
will result in needing five stat potions only (<code>2 × 2 + 1</code>).</p>
	</dd>
</dl>
<p>Keep in mind, however, that your character can only handle a limited
number of weapon improvements, see the <cite>skills</cite> command. Item power will
also increase.</p>

</div>
<h3 id="How_can_I_enchant_Armour">How can I enchant Armour?</h3>
<div id="How_can_I_enchant_Armour_CONTENT">
<p>Enchanting armour is easy: each time you read an <i>Enchant Armour</i> scroll,
the magic value will be increased by one, the speed, armour (physical
resistance) and weight are increased by some amount, as well as the item
power.</p>

</div>
<h3 id="What_is_this_item_power_business">What is this item power business?</h3>
<div id="What_is_this_item_power_business_CON">
<p>In general, the more interesting/powerful items have higher item power
values. Your character can handle only handle only a limited amount of
power - for each overall level you can handle <code>1.25</code> item power points,
so if you are level 16 you can handle a total of 20 item power points (see
the <cite>skills</cite> command for your current limit and remaining item power
points).</p>
<p>All the equipment you wear adds to this limit, so if you wield a weapon of
item power 30 plus two rings of item power 14 each you will need 58 item
power (and a level of 47).</p>

</div>
<h2 id="Generic_how_do_I">Generic how do I...</h2>
<div id="Generic_how_do_I_CONTENT">

</div>
<h3 id="How_do_I_uncurse_undamn_items_or_unw">How do I uncurse/undamn items or unwear cursed/damned items?</h3>
<div id="How_do_I_uncurse_undamn_items_or_unw-2">
<p>You need a spell to uncurse or undamn items, but luckily, magic shops
commonly have scrolls of remove curse or remove damnation. In addition,
all of the gods except the Devourers can remove curses, and Gnarg,
Lythander, Mostrai, Gaea, and Valriel can remove damnation. Gaea also
grants the remove damnation spell at 300 grace.</p>
<p>To actually remove the curse or damnation with a scroll, you need to
<cite>mark</cite> the item, and then <cite>apply</cite> the scroll. Once the curse or
damnation is removed, you can unwear the item.</p>

</div>
<h3 id="How_do_I_use_range_weapons_such_as_b">How do I use range weapons such as bows?</h3>
<div id="How_do_I_use_range_weapons_such_as_b-2">
<p>First, you have to <cite>apply</cite> the bow, then it will show up in the lower
right corner in the <code>Range:</code> slot. If it is already shown as <code>(applied)</code>
but not in the <code>Range:</code> slot you have to unapply and reaply it.</p>
<p>When you have a bow (or similar weapons like guns or crossbows) applied,
you just shoot as with spells or other range attacks - <code>Shift</code>+direction
key.</p>
<p>You need to have arrows (shells, bolts etc.) in your inventory or an
active container for this to work.</p>
<p>You can influence how you shoot via the <b>Bow Mode</b> (<cite>bowmode</cite>) in the <code>Playerbook =&gt; Settings</code> tab.</p>

</div>
<h3 id="How_do_I_thaw_icecubes_Or_How_do_I_i">How do I thaw icecubes? Or: How do I ignite a torch?</h3>
<div id="How_do_I_thaw_icecubes_Or_How_do_I_i-2">
<p>The right-click context menu in the playerbook inventory has an entry
<strong>ignite/thaw</strong>, which will ignite something or thaw icecubes.  You will
need a <strong>flint &amp; steel</strong> <a href="#ARCH_flint_and_steel_x11">$ARCH/flint_and_steel.x11</a> for
this to work!</p>
<p>An alternative would be to use the <b>mark</b> (<b>mark</b> in <cite>command</cite>) and
<b>apply</b> (<b>apply</b> in <cite>command</cite>) commands like this:</p>
<pre>   mark icecube
   apply flint and steel

</pre>
<p>You can bind these commands to a key and invoke them repeatedly.</p>

</div>
<h3 id="How_do_I_cast_a_cone_spell_all_aroun">How do I cast a cone spell all around me (burning hands, holy word etc.)?</h3>
<div id="How_do_I_cast_a_cone_spell_all_aroun-2">
<p>Use the <code>5</code>-key on your keypad. This will cats spells &quot;onto yourself&quot;
which has the desired effect with cone spells.</p>

</div>
<h3 id="How_do_I_pay_for_items_from_a_shop">How do I pay for items from a shop?</h3>
<div id="How_do_I_pay_for_items_from_a_shop_C">
<p>You need to have the unpaid item in your inventory, and then walk out of
the store with the shop mat. It will automatically pay for the item, if
you have enough money. If you don't, you will be prevented from leaving,
until you drop the unpaid item, or if you obtain enough money.</p>

</div>
<h3 id="How_do_I_sell_items_to_a_shop">How do I sell items to a shop?</h3>
<div id="How_do_I_sell_items_to_a_shop_CONTEN">
<p>Just drop the item in a shop. Some tiles in a shop might not be shop
tiles; usually, shop tiles are white brick floors. Try going over one of
them and dropping the item. If the shop is not interested, the item will
not leave your inventory.</p>

</div>
<h3 id="How_do_I_increase_my_attributes">How do I increase my attributes?</h3>
<div id="How_do_I_increase_my_attributes_CONT">
<p>To increase your attributes (strength, dexterity, etc.) you can wear items
that can increase them as long as you have them worn. You can get your
attributes up to 30 like this. To increase them permanently, you can
drink a stat potion, which have special colors and a letter on the potion
named 'potion of &lt;attribute&gt;'. These potions can increase your attributes
up to the natural maximum of your race; which can be viewd with the
statistics command. It increases your 'natural' attribute, up to the
'maximum' attribute.</p>

</div>
<h3 id="How_do_I_get_my_literacy_experience_">How do I get my literacy experience up?</h3>
<div id="How_do_I_get_my_literacy_experience_-2">
<p>You need to read books and scrolls to get literacy experience. You can
also use the literacy skill to identify scrolls and books. Identifying
them gives you literacy experience, and you are still able to read them
once you identify them.</p>

</div>
<h3 id="How_do_I_rent_and_maintain_an_apartm">How do I rent and maintain an apartment?</h3>
<div id="How_do_I_rent_and_maintain_an_apartm-2">
<p>Payment for an apartment is done in 3 steps. First, there is an entry fee
in the apartment map that you need to pay only once. Then, the apartment
rental fee depends on the apartment model, not you. The taxes depend on
your character level, and need to be paid, even while you are offline (the
rental fee is only paid while you are online.)</p>
<p>You can rent apartments in Scorn's Apartment Shop, and the hourly fees are
deducted from your bank account.</p>

</div>
<h3 id="How_do_I_use_passes_keys">How do I use passes/keys?</h3>
<div id="How_do_I_use_passes_keys_CONTENT">
<p>Whatever looks at your pass or key opens automatically, if you have it in
your inventory or in an applied container. Some keys might just open a
locked door, not a gate, and can be used by moving into the locked door.</p>

</div>
<h3 id="How_do_I_find_out_where_the_key_is_t">How do I find out where the key is to a locked door?</h3>
<div id="How_do_I_find_out_where_the_key_is_t-2">
<p>Usually, bumping into the locked door will tell you what kind of key
you need. This key is probably hidden somewhere in the same or close maps.
Some of the characters near by might give you hints or explain something.</p>

</div>
<h3 id="How_do_I_see_how_much_an_altar_or_ta">How do I see how much an altar or table costs to use?</h3>
<div id="How_do_I_see_how_much_an_altar_or_ta-2">
<p>You can move your mouse over it, and it will tell you how much currency it
needs.</p>

</div>
<h3 id="How_do_I_join_a_guild">How do I join a guild?</h3>
<div id="How_do_I_join_a_guild_CONTENT">
<p>To join a player guild, such as the Black Shield or Laughing Skull (not
the Adventurer's Guild, the Guild of Freedom, or anything else like
them), you need to be given a key by one of its current members.
The players might have special tasks and requirements for you, but all that
is needed is to have the key to enter the guild building and use its features.</p>

</div>
<h3 id="How_do_I_drop_items">How do I drop items?</h3>
<div id="How_do_I_drop_items_CONTENT">
<p>To drop items, you can right click on the item in your inventory screen in
the playerbook, which you can get by pressing F5 or the Playerbook tab,
and selecting drop. You can also shift + left click an item to drop it, or
enter the command</p>
<pre>   drop &lt;item&gt;

</pre>
<p>This command drops items with the names that have &lt;item&gt; in it. If you are
unable to drop an item, this will probably be because it is locked. You
can unlock it by right clicking the item in your inventory, and selecting
unlock.</p>

</div>
<h3 id="How_do_I_pick_up_items">How do I pick up items?</h3>
<div id="How_do_I_pick_up_items_CONTENT">
<p>You can pick up items by pressing the comma key (,) or by right clicking
the item on the floor, and selecting 'take'. You can also type the</p>
<pre>   take &lt;item&gt;

</pre>
<p>command.</p>

</div>
<h3 id="How_do_I_make_money_fast">How do I make money, fast?!</h3>
<div id="How_do_I_make_money_fast_CONTENT">
<p>You should identify the items you sell. Unidentified items sell MUCH
lower. Make sure you have the bargaining skill. It lets you save lots of
money when you buy stuff, and gain more money when you sell stuff. You can
level it up by buying items or selling expensive items. The higher it is
priced, the more experience. You can get a scroll of bargaining in Scorn's
gem shop.</p>
<p>Quests also might give you something valuable as a reward.</p>

</div>
<h2 id="Playing_Together">Playing Together</h2>
<div id="Playing_Together_CONTENT">

</div>
<h3 id="How_can_I_communicate_with_other_pla">How can I communicate with other players?</h3>
<div id="How_can_I_communicate_with_other_pla-2">
<p>There are five commands you can use to talk to others. All of them can
be entered by just typing them in, followed by the message, followed by
<code>Return</code>, e.g. <code>chat hey guys, what's up?</code>. If you have a <i>tab</i> open
in your message window you can also just type a text in there followed
by <code>Return</code> (this is especially handy as <code>Return</code> also activates the
current chat tab as well).</p>
<dl>
	<dt>chat &lt;message&gt; (short: <code>c</code>)</dt>
	<dd>
		<p>The most-used communications command. Whatever you <i>chat</i> will end up
in the <i>Chat</i> tab of everybody else who is currently logged in.</p>
		<p>Example:</p>
<pre>   c uh, that anchovis almost killed me

</pre>
	</dd>
	<dt>shout &lt;message&gt;</dt>
	<dd>
		<p>This is like <code>chat</code>, but &quot;louder&quot;. Everybody will get your message in red
in their <code>Chat</code> tabs.  You should not use <code>shout</code> unless you are in an
emergency or there is something really important to be said, otherwise you
risk being ignored by people.</p>
		<p>Example:</p>
<pre>   sh I am stuck! can anybody let me out?

</pre>
	</dd>
	<dt>say &lt;message&gt; (short: <code>s</code>)</dt>
	<dd>
		<p>This command can be used to talk to NPCs (without going through the NPC
dialogue window), but all players anywhere on the same map can hear you as
well.</p>
		<p>Example:</p>
<pre>   s good that schmorp cnanot hera us here!

</pre>
	</dd>
	<dt>tell &lt;playername&gt; &lt;message&gt; (short: <code>te</code>)</dt>
	<dd>
		<p>This is your prototypical two-way, private, chat channel. You can talk to
any other player that is currently logged in, and only he can hear you.</p>
	</dd>
	<dt>gsay &lt;message&gt; (short: <code>gs</code>)</dt>
	<dd>
		<p>The group say command sends a message to every member of the party/group
you are in. This is useful when playing in a team, to avoid cluttering the
chat channel.</p>
	</dd>
</dl>
<p>In addition to these, there are a large number of emotes you can use, use
the <cite>help</cite> command to get a list.</p>

</div>
<h3 id="Can_I_form_parties_Which_benefits_do">Can I form parties? Which benefits does party-play have?</h3>
<div id="Can_I_form_parties_Which_benefits_do-2">
<p>You can form <i>new</i> parties using the <code>party form &lt;name&gt;</code> name
command, and you can join <i>existing</i> parties using the <code>party join
&lt;name&gt;</code> command. There are other party commands, use <code>help party</code> to
find out more.</p>
<p>The main benefit of party-play is sharing experience: All experience you
gain is distributed to all party members accoridng to their level (people
with twice the level as you gain twice as much experience).</p>
<p>This is great for training skills you are bad in, but somebody else is
good in, and of course vice versa!</p>

</div>
<h2 id="Magic">Magic</h2>
<div id="Magic_CONTENT">

</div>
<h3 id="How_does_an_improvement_potion_work">How does an improvement potion work?</h3>
<div id="How_does_an_improvement_potion_work_">
<p>An improvement potion increases your hitpoints, spellpoints, or your grace, 
permanently. It does this by rerolling the rolls for your hitpoints, spellpoints,
or grace, and increasing it. This way, if you drink enough improvement
potions, your hitpoints, spellpoints, or grace would be as if you rolled perfectly.</p>
<p>However, only lower levels roll for hitpoints, spellpoints, or grace. After level 
10, skills increase hitpoints, spellpoints, or grace by a certain fixed amount.</p>

</div>
<h3 id="How_do_I_control_golems_I_summon">How do I control golems I summon?</h3>
<div id="How_do_I_control_golems_I_summon_CON">
<p>You can control golems by holding down shift + a direction key. It moves in the
direction you are holding.</p>

</div>
<h3 id="How_can_I_summon_a_specific_monster_">How can I summon a specific monster with <code>summon pet monster</code>?</h3>
<div id="How_can_I_summon_a_specific_monster_-2">
<p>Higher (effective) casting levels of <b>summoning</b> in <cite>skill_description</cite> give
you access to higher level monsters, but not all monsters have equal
abilities. Instead of summoning the highest level monster allowed by your
level you can also summon any other monster you had before by adding its
<i>archetype name</i> after the command, i.e. to summon bees you would use the
<code>cast summon pet monster bee</code> (short: <code>cspm bee</code>) command.</p>
<pre>   Level  Monster
       1  bat
       3  bird
       4  bee
       5  spider
       6  killer_bee
       7  pixie
       8  skeleton
       9  stalker
      11  devil
      13  beholder
      15  dark_elf
      17  skull
      20  angel
      25  vampire
      30  spectre
      35  lich
      40  demilich
      50  hellhound
      60  unusual_kobold
      70  chicken
      80  gr_hellhound
      90  dave
     100  laoch
     105  snitchangel

</pre>

</div>
<h3 id="How_does_being_attuned_repelled_deni">How does being attuned/repelled/denied affect casting level?</h3>
<div id="How_does_being_attuned_repelled_deni-2">
<p>Some spells are attuned to one or more spell classes (such as wounding,
summoning, fire and so on). The player can likewise be attuned to some of
these classes, giving a bonus, or repelled to them, giving a malus, or
denied to them, making her unable to cast the spell at all.</p>
<p>The skill level is the level of the skill that the spell uses (e.g.
summoning or sorcery).</p>
<p>When attuned, the bonus is up to 16 levels, but never higher than the
skill level itself, i.e. at level 2, the bonus is 2 also, resulting in an
attuned casting level of 4, at level 5, the bonus is 5, yielding a casting
level of 10, and at level 50, the bonus is 16, yielding 66.</p>
<p>When repelled, the malus is 16 levels always. If you are both attuned and
repelled at the same time, the effects will add.</p>
<p>These are added (or subtracted) to the skill level when checking whether
the player can cast the spell and when calculating the effective casting
level.</p>
<p>The effect on strength is as if the minimum spell level is lower (when
attuned) or higher (when repelled), and the strength will increase the
same as without any attunement.</p>

</div>
<h3 id="What_is_the_quot_effective_quot_cast">What is the &quot;effective&quot; casting level?</h3>
<div id="What_is_the_quot_effective_quot_cast-2">
<p>Effective casting levels start at 1 (lowest spell strength) and go up to
100 (highest nominal spell strength). Higher levels are also possible and
make the spell correspondingly stronger.</p>
<p>The effective casting level is calculated from the skill level, after
adding/subtracting any bonus from attunements.</p>
<p>If the skill level plus bonus is lower than 100, then the minimum spell
level comes into play: the minimum level is not only the minimum level
a caster is required to have, it is also the level where the effective
casting level equals 1. From there it grows till it reaches casting level
100, where the effective level will also be 100.</p>
<p>If the skill level plus bonus is 100 or higher, then this is the effective
spell level.</p>
<p>That means, when you have a spell with a minimum level of 90, then it's
effective range will be spread evenly over the 90..100 range, e.g. when
you cast this spell at skill level 90, the effective casting level will
be 1 (lowest). Casting at level 91 gives casting level 11, skill level
98 gives effective level 80, up to skill level 100, which then gives
effective level 100.</p>

</div>
<h2 id="Praying_Gods_and_Cults">Praying, Gods and Cults</h2>
<div id="Praying_Gods_and_Cults_CONTENT">

</div>
<h3 id="How_do_I_pray_or_get_the_praying_ski">How do I pray or get the praying skill?</h3>
<div id="How_do_I_pray_or_get_the_praying_ski-2">
<p>First enter the &quot;praying mode&quot; by readying your <b>praying</b> in <cite>skill_description</cite> skill
(by default bound to <code>Alt-P</code>, or by typing <code>rspraying</code>), then hold and keep holding
<code>Shift</code> and a direction key (e.g. <code>Shift-Up</code>).</p>
<p>You will now pray as long as you keep pressing the keys.</p>
<p>However, if you do not have the praying skill, you can obtain it by
reading a scroll of praying. This will let you learn the praying skill
depending on your intelligence. You can also use a holy symbol to use the
praying skill, as long as you have the holy symbol in your inventory. Holy
symbols can be found in shops and random dungeons.</p>

</div>
<h3 id="How_can_I_summon_a_specific_monster_-3">How can I summon a specific monster with <code>summon cult monster</code>?</h3>
<div id="How_can_I_summon_a_specific_monster_-4">
<p>You can't, your god selects the monsters for you.</p>

</div>
<h3 id="How_do_I_join_a_cult_become_a_follow">How do I join a cult/become a follower of a god?</h3>
<div id="How_do_I_join_a_cult_become_a_follow-2">
<p>That is simple, just find an altar of a god of your choice and start
<b>praying</b> in <cite>skill_description</cite>. Eventually, the god will recognize
you.</p>

</div>
<h3 id="How_do_I_change_gods">How do I change gods?</h3>
<div id="How_do_I_change_gods_CONTENT">
<p>That is far more difficult: gods really hate it when you defect
them. Expect to lose a lot of <b>praying</b> in <cite>skill_description</cite> experience in
the process.</p>
<p>First you should pray long enough to your god, until you gain 95%
resistance to god power. Then quickly go to the altar of your newly-chosen
god and pray on her/his altar. Your old god won't make it easy, but if you
are persistent your old god will let you go at one point.</p>

</div>
<h2 id="Exploring_Quests">Exploring / Quests</h2>
<div id="Exploring_Quests_CONTENT">

</div>
<h3 id="What_areas_are_there_to_explore">What areas are there to explore?</h3>
<div id="What_areas_are_there_to_explore_CONT">
<p>First, you should explore Scorn and find the port pass (and maybe also the
gate pass). Make sure you don't miss out the old city of scorn, many
a dangers, but also many treasures, can be found there.</p>
<p>When you finished most of the quests in Scorn you should investigate how
the pirates escape from the prison. This will lead to a number of nice
quests in and around Port Joseph.</p>
<p>Another nice series of quests, from low to medium level, can be found in
the King's Palace in Scorn. Who wouldn't want to meet the princess in
person?</p>
<p>The areas you can reach through the various transportation modes in Scorn
are Pupland (which has a lot of puzzles and quests from very low to very
high level), Stoneville and Santo Dominion (low and medium level quests,
also nice shops), and the Aldwulf Archipelago (which is more dangerous).</p>
<p>Through the gate you can reach Euthville and Santo Dominion to the north,
Brest (medium level) and Lake Country (medium to high level) to the south,
and Navar (medium level quests) in the east (a long travel).</p>
<p>From Navar you can travel to Darcap in the far north (medium level
quests), near the mystical country of Azumauindo, to Wolfsburg somewhere
on the ocean, a rather dangerous area with many thieves and pirates
(medium level quests) and Valleynoy to the south, with medium and high
level quests (did you find the southpole yet?).</p>
<p>Also, it is said that Navar somehow has access to the mystical City
de Clouds, the biggest city ever seen (high level and very high level
quests). There are rumors about lands beyond (St. Bartholemew and other
cities) and other even more mystical places, but little is known about
that.</p>

</div>
<h3 id="Where_is_that_damn_Goblin_Chief">Where is that damn Goblin Chief?</h3>
<div id="Where_is_that_damn_Goblin_Chief_CONT">
<p>In his cave to the north-east of the Scorn Gate. It is the one that starts
with a random maze (yes, there are multiple caves). Keep in mind that the
Goblin Chief's Head is fastened to his body, so you have to find and kill
him first.</p>

</div>
<h3 id="Where_can_I_find_the_i_comet_i_spell">Where can I find the <i>comet</i> spell?</h3>
<div id="Where_can_I_find_the_i_comet_i_spell-2">
<p>In the tower of stars, which is almost directly south of Scorn (but not
too near to it). You have to talk to people to solve this quest, and it
does not involve a random maze.</p>

</div>
<h1 id="Authors">Authors</h1>
<div id="Authors_CONTENT">
<p>The following people contributed to this document: Robin Redeker, John O'Donnell
and Marc Lehmann.
</p>
</div>

				
  		</div>


