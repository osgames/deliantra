<div id="content">
		<h2>Download the Deliantra MMORPG Client</h2>
		<p>
			Download the game client to connect to the world of Deliantra and begin playing.  Free downloads are available for four
			major computer platforms:
		</p>
		<h3>Windows</h3>
		<p>This is not a setup package.  Just extract the game client and double click to start it.
			 To uninstall just delete the program from your harddrive.
		</p>
		<a class='button' href="http://dist.schmorp.de/deliantra/ota/deliantra.exe">Download for Windows</a><br /><br />
		<h3>Linux (32 bit)</h3>
		<p>Extract the game client and then execute the binary.  This is the x86 version intended for
		   computers with a 32 bit operating system.
		</p>
		<a class='button' href="http://dist.schmorp.de/deliantra/ota/deliantra-gnu-linux-x86.tar.gz">Download for x86 Linux</a><br /><br />
		<h3>Linux (64 bit)</h3>
		<p>Extract the game client and then execute the binary.  This is the amd64 version optimized for
		   computers with 64 bit operating system.
		</p>
		<a class='button' href="http://dist.schmorp.de/deliantra/ota/deliantra-gnu-linux-amd64.tar.gz">Download for amd64 Linux</a><br /><br />
		<h3>Mac OS X</h3>
		<p>Just open up the disk image and a double click to start the game client.  If you want to install
		it on your computer you can drag the program to your Application folder.
		<b>NOTE: You will need an Intel CPU for this binary, it will not work with PPC CPU's.
		Also you will need Mac OS X 10.5 or later.</b>
		</p>
		<a class='button' href="http://dist.schmorp.de/deliantra/ota/Deliantra_MORPG_Client.dmg">Download for Mac OS X</a><br /><br />

		<h3>More Information</h3>
		<p>For more information about the client, for example news of what changed,
		you should have a look at the <a href="client.html">client page</a>.
		</p>

		<h3>Download the Deliantra MMORPG Server</h3>
		<p><b>You do not have to download the Deliantra server code to play the game.</b>  However, if you
		have the technical experience to start your own server, or if you just want to mess around with
		the server in a test environment on your own computer then you can download the server code.
		</p>
		See also: <a class="button" href="server_download.html">Downloads and Changes</a>
</div>

