<?xml version="1.0" encoding="utf-8"?>
<html>
  <head>
    <title>Deliantra MMORPG</title>
    
		<link rel='stylesheet' type='text/css' href='style/style.css' />
		<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="news.rss"/>

<?php
		//If this page has JavaScript logic then include it.
		if(isset($pageJavaScript))
		{
			include $pageJavaScript;
		}
?>
  </head>
  <body>
	<div id="mainWrapper">
		<div id="header">
			<div id="headerText">
			  Deliantra MMORPG
			</div>
		</div>
<?php
			//Output the link bar and then the page's content.
			include "template/links.php";
			include $pageContent;
?>
		<div id='footer'>
		  Copyright &#169; 2010-2016 by The Deliantra Team
		</div>
	</div>
        <div style="text-align: center; background: #2D1714; color: #2D1714; font-size: 4px">
           <span style="color: #2D1714">
              Join this free mmorpg and our great and friendly community. Big and unique game world, many dungeons, scripted quests and instances. Find friends and play in a group. You can join the development and create own maps, quests and content!
              mmorpg,instances,fantasy,open source,free software,games,game,gaming,online,multiplayer,rpg,RPG,online game,online games,pvp,community,clans,guilds,groups,massive,roleplaying,mmo,mmog,deliantra,crossfire,warcraft,everquest
              </span>
           </div>
        </div>
  </body>
</html>


