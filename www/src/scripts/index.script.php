		<script type='text/javascript' src='scripts/jquery-1.4.2.min.js'></script>
		
		<script type='text/javascript'>
		
			var images = new Array(
				"images/dragon1.png",
				"images/monsters.png",
				"images/treasure.png",
				"images/exploring.png"
			);
			
			var descriptions = new Array(
				"imageDescription1",
				"imageDescription2",
				"imageDescription3",
				"imageDescription4"
			);
			
			var currentlyShownSlot = 1;  //The number one image slot starts out displayed.
			var imageIndex = 0;
			var moving = false;  //Flag to prevent double clicking from messing up the animation.

			//Wait until the page loads and then set up dynamic events for the page elements.		
			$(document).ready(function()
			{
				//The action for the left image scroll arrow.
				$('#leftArrow').click(function()
				{
					if(moving==true)
					{
					  return;
					}
					moving = true;
					var oldIndex = imageIndex;
					imageIndex--;
				  if(imageIndex<0)
				  {
				    imageIndex = images.length-1;
				  }
				  $("#"+descriptions[oldIndex]).fadeOut('slow',function() {$("#"+descriptions[imageIndex]).fadeIn();});
				  
					//First set up the other image and put it in place for sliding in.
					if(currentlyShownSlot==1)
					{
					  //Then we need to put the second image in place off to the left of the image box.
					  $("#featuredImage2").attr("src",images[imageIndex]);
					  $("#featuredImage2").css("left", "-800px");
					  $("#featuredImage2").animate({"left": "+=800px"}, "slow");
						$("#featuredImage1").animate({"left": "+=800px"}, "slow", function()
						{
							if(currentlyShownSlot==1)
							{
								currentlyShownSlot = 2;
							}
							else
							{
								currentlyShownSlot = 1;
							}
							moving = false;
						});
					}
					else
					{
					  //Then we need to put the first image in place off to the left of the image box.
					  $("#featuredImage1").attr("src",images[imageIndex]);
					  $("#featuredImage1").css("left", "-800px");
					  $("#featuredImage1").animate({"left": "+=800px"}, "slow");
						$("#featuredImage2").animate({"left": "+=800px"}, "slow", function()
						{
							if(currentlyShownSlot==1)
							{
								currentlyShownSlot = 2;
							}
							else
							{
								currentlyShownSlot = 1;
							}
							moving = false;
						});
					}					
				});
				
				//The action for the right image scroll arrow.
				$('#rightArrow').click(function()
				{
					if(moving==true)
					{
						return;
					}
					moving = true;
					//Hide current image description
					var oldIndex = imageIndex
				  imageIndex++;
				  if(imageIndex>=images.length)
				  {
				    imageIndex = 0;
				  }
				  $("#"+descriptions[oldIndex]).fadeOut('slow',function() {$("#"+descriptions[imageIndex]).fadeIn();});
					//First set up the other image and put it in place for sliding in.
					if(currentlyShownSlot==1)
					{
					  //Then we need to put the second image in place off to the right of the image box.
					  $("#featuredImage2").attr("src",images[imageIndex]);
					  $("#featuredImage2").css("left", "800px");
					}
					else
					{
					  //Then we need to put the first image in place off to the right of the image box.
					  $("#featuredImage1").attr("src",images[imageIndex]);
					  $("#featuredImage1").css("left", "800px");
					}
					$("#featuredImage1").animate({"left": "-=800px"}, "slow");
					$("#featuredImage2").animate({"left": "-=800px"}, "slow", function()
					{
						if(currentlyShownSlot==1)
						{
							currentlyShownSlot = 2;
						}
						else
						{
						  currentlyShownSlot = 1;
						}
						moving = false;
					});
				});
			});
			
		</script>
