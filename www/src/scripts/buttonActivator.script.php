		<script type='text/javascript' src='scripts/jquery-1.4.2.min.js'></script>
		
		<script type='text/javascript'>
			$(document).ready(function()
			{
				$('.button').hover(
					function()
					{
						$(this).css('background','#d54113');
					},
					function()
					{
						$(this).css('background','#ab2c05');
					}
				);
				
				$('.button').mousedown(
					function()
					{
						$(this).css('background','#2D1714');
					}
				);
				
				$('.button').mouseup(
					function()
					{
						$(this).css('background','#ab2c05');
					}
				);
			});
		</script>
