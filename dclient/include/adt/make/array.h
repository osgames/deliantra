template<typename T>
value_t make_value (T const &v);

struct array_t
{
  template<typename... Args>
  array_t (Args const &...args)
    : data { make_value (args)... }
  {
  }

  value_array const data;
};
