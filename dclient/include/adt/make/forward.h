#include <utility>
#include <vector>

namespace make
{
  namespace detail
  {
    struct value_t;
    struct array_t;
    struct map_t;

    typedef std::vector<value_t>        value_array;
    typedef std::pair<value_t, value_t> value_pair;
    typedef std::vector<value_pair>     value_map;
  }
}
