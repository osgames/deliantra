struct value_t
{
  typedef void convert_fn (void const *data); typedef convert_fn *convert_fp;
  typedef std::string str_fn  (void const *data);

  enum type_code_t
  {
    VALUE,
    ARRAY,
    MAP
  };

  void const *data;
  str_fn *to_str;
  std::type_info const *type;

  value_t (void const *data, str_fn *to_str, std::type_info const &type)
    : data (data)
    , to_str (to_str)
    , type (&type)
  {
  }

  std::string str () const
  {
    assert (type_code () == VALUE);
    assert (to_str != NULL);
    return to_str (data);
  }

  array_t const &array () const
  {
    assert (type_code () == ARRAY);
    return *(array_t const *)data;
  }

  map_t const &map () const
  {
    assert (type_code () == MAP);
    return *(map_t const *)data;
  }

  type_code_t type_code () const
  {
    if (*type == typeid (map_t  )) return MAP;
    if (*type == typeid (array_t)) return ARRAY;
    return VALUE;
  }
};
