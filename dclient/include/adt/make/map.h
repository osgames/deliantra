template<typename... Args>
value_map make_map (Args const &...args);

struct map_t
{
  template<typename... Args>
  map_t (Args const &...args)
    : data (make_map (args...))
  {
  }

  value_map const data;
};
