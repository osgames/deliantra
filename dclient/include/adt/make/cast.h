#include "adt/make.h"
#include "adt/rtti_util.h"

namespace make
{
  template<typename T>
  T
  value_cast (value const &v)
  {
    return runtime_cast<T> (v.data, *v.type);
  }
}
