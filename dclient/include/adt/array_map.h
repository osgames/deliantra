#pragma once

#include <vector>
#include <utility>

template<typename T>
struct array_map
{
  typedef unsigned char size_type;

  array_map (std::initializer_list<std::pair<size_type, T>> init)
  {
    foreach (auto const &pair, init)
      {
        if (pair.first >= data.size ())
          data.resize (pair.first + 1);
        data.at (pair.first) = pair.second;
      }
  }

  bool has (size_type n) const { return data.size () > n && data[n]; }

  T       &operator [] (size_type n)       { return data.at (n); }
  T const &operator [] (size_type n) const { return data.at (n); }

private:
  std::vector<T> data;
};
