#pragma once

#include <typeinfo>

bool is_integral (std::type_info const &ti);
bool is_signed (std::type_info const &ti);
bool is_floating_point (std::type_info const &ti);

template<typename T>
T runtime_cast (void const *ptr, std::type_info const &ti);

template<> inline           char runtime_cast (void const *ptr, std::type_info const &ti) { return runtime_cast<  signed long> (ptr, ti); }
template<> inline   signed  char runtime_cast (void const *ptr, std::type_info const &ti) { return runtime_cast<  signed long> (ptr, ti); }
template<> inline unsigned  char runtime_cast (void const *ptr, std::type_info const &ti) { return runtime_cast<unsigned long> (ptr, ti); }
template<> inline   signed short runtime_cast (void const *ptr, std::type_info const &ti) { return runtime_cast<  signed long> (ptr, ti); }
template<> inline unsigned short runtime_cast (void const *ptr, std::type_info const &ti) { return runtime_cast<unsigned long> (ptr, ti); }
template<> inline   signed   int runtime_cast (void const *ptr, std::type_info const &ti) { return runtime_cast<  signed long> (ptr, ti); }
template<> inline unsigned   int runtime_cast (void const *ptr, std::type_info const &ti) { return runtime_cast<unsigned long> (ptr, ti); }
