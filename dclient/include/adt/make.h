#pragma once

#include <boost/lexical_cast.hpp>

#include <cassert>
#include <typeinfo>

#include "adt/make/forward.h"

namespace make
{
  namespace detail
  {
#include "adt/make/array.h"
#include "adt/make/map.h"
#include "adt/make/value.h"
  
    template<typename T>
    struct any_value_t
      : value_t
    {
      any_value_t (T const &v);
  
      static std::string str (void const *data)
      {
        return boost::lexical_cast<std::string> (*(T const *)data);
      }
    };

    template<typename T>
    any_value_t<T>::any_value_t (T const &v)
      : value_t (&v, str, typeid (v))
    {
    }
  
    template<>
    inline any_value_t<array_t>::any_value_t (array_t const &v)
      : value_t (&v, NULL, typeid (v))
    {
    }
  
    template<>
    inline any_value_t<map_t>::any_value_t (map_t const &v)
      : value_t (&v, NULL, typeid (v))
    {
    }


    template<typename T>
    value_t
    make_value (T const &v)
    {
      return any_value_t<T> (v);
    }


    static inline void
    fill_map (value_map &map)
    {
    }

    template<typename T1, typename T2, typename... Args>
    void
    fill_map (value_map &map, T1 const &v1, T2 const &v2, Args const &...args)
    {
      map.emplace_back (make_value (v1), make_value (v2));
      fill_map (map, args...);
    }

    template<typename... Args>
    value_map
    make_map (Args const &...args)
    {
      value_map map;
      fill_map (map, args...);
      return std::move (map);
    }
  }

  typedef detail::value_t value;
  typedef detail::array_t array;
  typedef detail::value_pair value_pair;
  typedef detail::map_t map;
}
