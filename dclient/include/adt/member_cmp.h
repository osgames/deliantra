#pragma once

template<typename C, typename T, T (C::*Member)>
struct class_member
{
  typedef C object_type;
  typedef T member_type;

  static member_type const &get (object_type const &v)
  {
    return v.*Member;
  }
};

template<typename C>
bool
member_cmp (C const &a, C const &b)
{
  return false;
}

template<typename Member, typename... Members>
bool
member_cmp (typename Member::object_type const &a,
            typename Member::object_type const &b)
{
  return Member::get (a) != Member::get (b)
       ? Member::get (a)  < Member::get (b)
       : member_cmp<Members...> (a, b)
       ;
}

#define MEMBER(class, member) class_member<class, decltype (class::member), &class::member>
