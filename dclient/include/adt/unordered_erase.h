#pragma once

template<typename T>
typename std::vector<T>::iterator
unordered_erase (std::vector<T> &vec, typename std::vector<T>::iterator iter)
{
  size_t pos = iter - vec.begin ();
  vec.at (pos) = vec.back ();
  vec.pop_back ();
  return vec.begin () + pos;
}

template<typename T>
typename std::vector<T>::iterator
unordered_erase (std::vector<T> &vec, typename std::vector<T>::size_type pos)
{
  return unordered_erase (vec, vec.begin () + pos);
}

template<typename T>
typename std::vector<T>::iterator
unordered_erase (std::vector<T> &vec, typename std::vector<T>::const_reference element)
{
  return unordered_erase (vec, &element - &vec.front ());
}
