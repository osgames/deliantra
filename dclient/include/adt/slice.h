#pragma once

template<typename Container>
struct slice_view
{
  typedef Container                                             container_type;

  typedef typename container_type::const_iterator               const_iterator;
  typedef typename container_type::iterator                     iterator;

  typedef typename container_type::const_reverse_iterator       const_reverse_iterator;
  typedef typename container_type::reverse_iterator             reverse_iterator;

  slice_view (Container const &container, size_t offset)
    : container (container)
    , offset (offset)
  {
    if (container.size () < offset)
      throw std::logic_error ("slice out of bounds");
    else if (container.size () == offset)
      throw std::logic_error ("empty slice interval");
  }

  const_iterator begin () const { return container.begin () + offset; }
  const_iterator end   () const { return container.end   ();          }

  const_reverse_iterator rbegin () const { return container.rbegin () + offset; }
  const_reverse_iterator rend   () const { return container.rend   ();          }

private:
  container_type const &container;
  size_t offset;
};

template<typename Container>
slice_view<Container>
slice (Container const &container, size_t offset)
{
  return slice_view<Container> (container, offset);
}
