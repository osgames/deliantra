#pragma once

template<typename Container>
struct reverse_view
{
  typedef Container                                             container_type;
  typedef typename container_type::const_reverse_iterator       const_iterator;

  const_iterator begin () const { return container.rbegin (); }
  const_iterator end   () const { return container.rend   (); }

  container_type const &container;
};

template<typename Container>
reverse_view<Container>
reverse (Container const &container)
{
  return { container };
}
