#if GCC_VERSION(4,6)
# define foreach(a, b) for (a : b)
#else
# include <boost/foreach.hpp>
# define foreach BOOST_FOREACH
#endif
