#include "adt/make.h"

#include <iostream>

using make::value;
using make::value_pair;
using make::array;
using make::map;

static void print (value const &arr);
static void print (array const &arr);
static void print (map const &arr);

static void
print (value const &v)
{
  switch (v.type_code ())
    {
    case value::VALUE:
      std::cout << v.str ();
      break;
    case value::ARRAY:
      print (v.array ());
      break;
    case value::MAP:
      print (v.map ());
      break;
    }
}

static void
print (array const &arr)
{
  std::cout << "array { ";
  foreach (value const &v, arr.data)
    {
      print (v);
      std::cout << ", ";
    }
  std::cout << "}";
}

static void
print (map const &arr)
{
  std::cout << "map { ";
  foreach (value_pair const &v, arr.data)
    {
      print (v.first);
      std::cout << " => ";
      print (v.second);
      std::cout << ", ";
    }
  std::cout << "}";
}


int
main ()
{
#define A1 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
#define A2 A1,A1,A1,A1,A1,A1,A1,A1,A1,A1,A1,A1,A1,A1,A1,A1,A1,A1,A1,A1,A1,A1,A1,A1
#define A3 A2,A2,A2,A2,A2,A2,A2,A2,A2,A2,A2,A2,A2,A2,A2,A2,A2,A2,A2,A2,A2,A2,A2,A2
  for (int i = 0; i < 1000; i++)
    array {
      "resource",
       "b",
       "exp_table",
       map { A1 },
       map {
         1, "2",
         "1", 2,
         array { 1, 2 }, array { 2, 1 }
       },
    };
  print (array {
    "resource",
    "b",
    "exp_table",
    array { 3, 4, 5 },
    map {
      1, "2",
      "1", 2,
      array { 1, 2 }, array { 2, 1 }
    },
  });

  std::cout << "\n";
}
