#include "test_function.h"

with_function wf;
with_virtual wv;

struct implementor
  : with_virtual::interface
{
  void cb () { }
};

static implementor itor;

void func () { }

void
init ()
{
  wf.cb = func;
  wv.iface = &itor;
}
