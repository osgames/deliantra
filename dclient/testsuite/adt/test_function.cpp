#include "test_function.h"

void init ();
void func ();

int
main ()
{
  int const iter = 1000000000;
  init ();

  for (int i = 0; i < iter; i++)
#if 0
    wf ();
#elif 0
    wv ();
#else
    func ();
#endif
}
