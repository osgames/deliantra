#if 1
namespace std
{

template<typename Func>
struct function;

template<typename R, typename... Args>
struct function<R (Args...)>
{
  typedef R function_type (Args...);

  function_type *func;

  R operator () (Args...)
  {
    return func ();
  }

  function &operator = (function_type *func)
  {
    this->func = func;
    return *this;
  }
};

}
#else
#include <functional>
#endif

struct with_function
{
  std::function<void ()> cb;

  void operator () () { cb (); }
};

struct with_virtual
{
  struct interface
  {
    virtual void cb () = 0;
  };

  interface *iface;

  void operator () () { iface->cb (); }
};

extern with_function wf;
extern with_virtual wv;
