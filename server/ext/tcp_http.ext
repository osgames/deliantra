#! perl # optional depends=tcp

# http server - this tries to speak enough of http 1.1 (and 1.0)
# to work with browsers. it does not even attempt to be a complete
# implementation, although it should be mostly correct for that it does.

sub send {
   my $self = $_[0];

   if (length $self->{wbuf}) {
      $self->{wbuf} .= $_[1];
   } else {
      $self->{wbuf} .= $_[1];

      my $len = syswrite $self->{fh}, $self->{wbuf};
      substr $self->{wbuf}, 0, $len, "";

      $self->{ww} = AE::io $self->{fh}, 1, sub {
         my $len = syswrite $self->{fh}, $self->{wbuf};
         substr $self->{wbuf}, 0, $len, "";

         delete $self->{ww} unless $len; # in case of errors, stop
         delete $self->{ww} unless length $self->{wbuf};
      } if length $self->{wbuf};
   }
}

sub fatal {
   my ($self) = @_;

   $self->send ("HTTP/1.1 500 internal error\015\012");
}

sub respond_304 {
   $_[0]->send ("HTTP/1.1 304 cache\015\012"
                . $_[0]{ohdr}
                . "\015\012");
}

sub respond {
   $_[0]->send ("HTTP/1.1 $_[1]\015\012"
                . "content-length: " . (0 + length $_[2]) . "\015\012"
                . "access-control-allow-origin: *\015\012"
                . $_[0]{ohdr}
                . "$_[3]\015\012" . ($_[0]{give_head} ? "" : $_[2]));
}

my $cache_headers = "cache-control: public, max-age=8640000\015\012"
                    . "etag: \"0\"\015\012";

my $revalidate_headers = "cache-control: public, max-age=60, must-revalidate\015\012";

my $nocache_headers = "cache-control: no-cache, max-age=0\015\012";

sub content_type($$) {
   return "content-type: image/png\015\012"                             if $_[1] =~ /^\x89PNG/;
   return "content-type: image/jpeg\015\012"                            if $_[1] =~ /^......JFIF/s;
   return "content-type: audio/wav\015\012"                             if $_[1] =~ /^RIFF/;
   return "content-type: audio/ogg\015\012"                             if $_[1] =~ /^OggS/;
   return "content-type: text/css; charset=utf-8\015\012"               if $_[0] =~ /\.css$/;
   return "content-type: application/javascript; charset=utf-8\015\012" if $_[0] =~ /\.js$/;
   return "content-type: text/html; charset=utf-8\015\012"              if $_[1] =~ /^</;

   "content-type: text/plain; charset=utf-8\015\012"
}

sub handle_con {
   my ($self) = @_;

   my ($method, $uri, $http, $req, $close, $len);

   while () {
      while ($self->{rbuf} =~ s/^( (?: [^\015]+ | . )+? \015\012)\015\012//xs) {
         $req = $1;

         # we ignore headers atm.

         $req =~ m%^(\S+) (\S+) HTTP/([0-9.]+)\015\012%ig
            or return $self->respond ("400 bad request");

         $method = uc $1;
         $uri    = $2;
         $http   = $3;

         $uri =~ s%^http://[^/]*%%i; # just in case
         $uri =~ s/%([0-9a-fA-F][0-9a-fA-F])/chr hex $1/ge; # %-decode

         if (0) {
         my %hdr;

         $hdr{lc $1} .= ",$2"
            while $req =~ /\G
                  ([^:\000-\037]*):
                  [\011\040]*
                  ((?: [^\012]+ | \012[\011\040] )*)
                  \012
               /gxc;

         $req =~ /\G$/
           or return $self->respond ("400 bad request");

         # remove the "," prefix we added to all headers above
         substr $_, 0, 1, ""
            for values %hdr;
         }

         if ($http == 1.0) {
            if ($req =~ /^connection\s*:\s*keep-alive/mi) {
               $self->{ohdr} = "connection: keep-alive\015\012";
            } else {
               $self->{ohdr} = "connection: close\015\012";
               $close = 1;
            }
         }

         $self->{give_head} = $method eq "HEAD"
            and $method = "GET";

         cf::debug "HTTP $method: $self->{id} $uri";

         if ($method ne "GET") {
            $self->respond ("405 no $method");
            next;
         }

         # not needed for GET, but who knows
         $self->send ("HTTP/1.1 100 go on\015\012")
            if $req =~ /^expect:.*\b100-continue\b/i;

         if ($uri =~ m%^/([0-9a-f]+)(M?)$%) { # faces
            my $want_meta = $2;
            my $idx = $cf::face::HASH{pack "H*", $1};

            $idx
               or do { $self->respond ("404 illegal face name"), next };

            if ($req =~ /if-none-match/i) { # dirtiest hack evar
               $self->respond_304;
               next;
            }

            my $type = cf::face::get_type $idx;
            my $data = cf::face::get_data $idx;

            (my $meta, $data) = unpack "(w/a*)*", $data
               if $type & 1;

            if ($want_meta) {
               if ($type & 1) {
                  $self->respond ("200 OK", $meta, "content-type: text/plain; charset=utf-8\015\012" . $cache_headers);
               } else {
                  $self->respond ("404 type $type has no metadata");
               }
            } else {
               $self->respond ("200 OK", $data, (content_type $uri, $data) . $cache_headers);
            }

         } elsif (my $idx = (cf::face::find "res/http$uri") || (cf::face::find "res/http${uri}index.html")) {
            my $data =              cf::face::get_data $idx;
            my $csum = unpack "H*", cf::face::get_csum $idx;
            my $hdr  = "etag: \"$csum\"\015\012" . $revalidate_headers;

            if ($req =~ /if-none-match: "$csum"/i) { # dirtiest hack evar
               $self->respond_304;
               next;
            }

            $self->respond ("200 OK", $data, (content_type $uri, $data) . $hdr);

         } elsif (cf::face::find "res/http$uri/index.html") {
            $self->respond ("302 dirslash", "", "location: $uri/\015\012" . $nocache_headers);

         } elsif ($uri eq "/debug") { # for debugging
            my @body = <<EOF;
<html><head><style>
th:nth-child(1) { text-align: right; }
th:nth-child(2) { text-align: left;  }
th:nth-child(3) { text-align: right; }
th:nth-child(4) { text-align: left;  }
th:nth-child(5) { text-align: left;  }
td:nth-child(1) { text-align: right;  font-family: monospace;}
td:nth-child(2) { text-align: left;   font-family: monospace;}
td:nth-child(3) { text-align: right;  font-family: monospace;}
td:nth-child(4) { text-align: left;  }
td:nth-child(5) { text-align: left;  }
</style><body>
EOF
            for my $type (6, 5, 4, 3, 2, 1, 0) {
               push @body, "<h1>$type</h1><table><tr><th>#</th><th>csum</th><th>size</th><th>name</th><th>meta</th></tr>";

               for (1 .. cf::face::faces_size - 1) {
                  cf::cede_to_tick;

                  next if $type != cf::face::get_type $_;
                  my $name = cf::face::get_name $_;
                  my $id   = unpack "H*", cf::face::get_csum $_, 0;
                  push @body, "<tr><td>$_</td><td>$id</td><td>$cf::face::SIZE[0][$_]</td><td><a href='$id'>$name</a></td>";
                  push @body, "<td><a href='${id}M'>meta</a>" if $type & 1;
                  push @body, "</tr>";
               }

               push @body, "</table>";
            }

            push @body, "</body></html>";

            my $body = join "", @body;
            utf8::encode $body;
            $self->respond ("200 OK", $body, "content-type: text/html; charset=utf-8\015\012");

         } else {
            $self->respond ("404 not found");
         }

         cf::cede_to_tick;
      }

      return if $close; # http 1.0 only currently
      return if length $self->{rbuf} > 8192; # headers too long

      Coro::AnyEvent::readable $self->{fh}, 6;

      $len = sysread $self->{fh}, $self->{rbuf}, 4096, length $self->{rbuf};

      return if $len <= 0;
   };
}

our $DETECTOR = ext::tcp::register http => 64, sub {
   # regex avoids conflict with websockets, which use /ws
   m{^(?i:GET|HEAD|OPTIONS) \  (?! (?i:http://[^/]+)? /ws \  ) }x
}, sub {
   my $self = bless {
      id   => $_[0],
      fh   => $_[1],
      rbuf => $_[2],
      wbuf => "",
   };

   $self->{async} = Coro::async_pool {
      $Coro::current->nice (4);
      $Coro::current->{desc} = "http $self->{id}";

      $self->handle_con;
   };
};

cf::register_exticmd http_faceurl => sub {
   my ($ns) = @_;

   "/"
};

