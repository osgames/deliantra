#! perl # mandatory

# This file is licensed under the GNU Affero General Public License, see
# the accompanying file COPYING.Affero for details.
#
# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, version 3 of the License only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# The authors can be reached via e-mail to <support@deliantra.com>

# The intent of this file is to provide the user of the server with
# a URL where he/she can download the source code of this program
# together with any maps and archetype data used to run the server.

# The default uses the location of the pristine release and works
# unless you modify the server, in which case you are required to
# either update the URL here or, preferably, change the source_url_tar
# and source_url_cvs settings in $CONFDIR/config.
CONF URL_CVS : source_url_cvs = "http://software.schmorp.de/pkg/cf.schmorp.de.html";
CONF URL_TAR : source_url_tar = "http://dist.schmorp.de/deliantra/";

our $URL_TAR_SERVER = "$URL_TAR/cfserver-" . cf::VERSION . ".tar.bz2";
our $URL_TAR_ARCH   = "$URL_TAR/cfarch-"   . cf::VERSION . ".tar.bz2";
our $URL_TAR_MAPS   = "$URL_TAR/cfmaps-"   . cf::VERSION . ".tar.bz2";

cf::register_command sourcecode => sub {
   my ($ob, $arg) = @_;

   $ob->reply (undef, <<EOF, cf::NDI_UNIQUE | cf::NDI_DK_ORANGE);
If this is a (possibly modified) release version of Deliantra, you can find
the corresponding sources at the following location, in accordance to section
13 of the GNU Affero General Public License, version 3.

   all files: $URL_TAR
   server:    $URL_TAR_SERVER
   arch data: $URL_TAR_ARCH
   maps:      $URL_TAR_MAPS


If this is an intermediate development snapshot, the sources might
alternatively be provided via anonymous CVS (or another VCS) with full
version history. Detailed instructions how to access the repository
(without unnecessary burden) are available here:

   $URL_CVS

EOF

   1
};

