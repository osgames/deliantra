#! perl

my @QUESTS = (
   {
      name         => "Scorn Farmhouse",
      match        => ['marker', 'quest_scorn_farmhouse'],
      start        => "Leave scorn by the eastern gate and look right to the south of the gate, you will find a small farm there. Investigate!",
      difficulty   => 'piece of cake',
      spoiler_cost => 30000,
      spoiler      => "Ok, get the key on the table in that house, go to the barn, open it with the key, kill all the monsters, especially the troll, take his head (\"Guuh's head\") and walk right in front of the farmer's wife. She will open the door to the storage room. Go in there and take the stuff.",
   },
   {
      name         => "Gramp Malone Earhorn",
      match        => ['flag', 'quest_gramp_malones_earhorn'],
      difficulty   => 'be observant',
      start        => 'Go to the hoves in the south of scorn, go into the south east house from the four houses and ask the woman and/or grandpa malone about his earhorn. H<You will have to find it, maybe it\'s not even on the same map>',
      spoiler_cost => 50000,
      spoiler      => "You will find his earhorn in the tavern \"The Barking Mule\", he left it in a workbench there.",
   },
   {
      name         => "Gramp Malone Walkingstick",
      match        => ['flag', 'quest_gramp_malones_walking_stick'],
      difficulty   => 'be observant',
      start        => 'Go to the hoves in the south of scorn, go into the south east house from the four houses and ask the woman and/or grandpa malone about his walking stick. H<You will have to find it, maybe it\'s not even on the same map>',
      spoiler_cost => 50000,
      spoiler      => "The walking stick was lost in the four houses called the \"Riverside Manor\" in the south east of Scorn. The House to the very south east has a table, beneath it you will find gramps walking stick."
   },
   {
      name       => 'Hero of Scorn',
      match      => ['marker', 'hero_of_scorn'],
      difficulty => 'easy',
      start       => "Go to the castle of the king of scorn,"
                    ."find the Hall of Quests and finish the first quest.",
      spoiler_cost => 100000,
      spoiler    => "You have to find the cave where the Goblin Chief hides. You can find that cave by leaving Scorn through the eastern gate, following the road to the east until see a small footpath going to the north. Follow it and you will find a cave. After some levels of fighting you will come to the final level where you will find the Goblin Chief, which usually looks very similar to usual Goblins. Kill him and grab his head, but be careful not to burn it accidentally. Bring that head to the Hall of Quests in the king's castle in scorn, and enter the first teleporter, where you will be awardened with the rank of 'Hero of Scorn'.",
   },
   {
      name       => "Wizard Yarid House",
      match      => ['marker', 'quest_wizard_yarid_house'],
      difficulty => 'tricky',
      start      => "Near the mad mages tower, in the western part of Scorn you will find a house with a red roof. It's wizard Yarid's house, he died while experimenting in his basement. Ask his house keeper for more information and explore the dungeon.",
   },
   {
      name       => "Mike Miller House",
      match      => ['marker', 'quest_mike_millers_house'],
      difficulty => 'tricky',
      start      => "In the north west of scorn you will find the 'West Scorn Trademarket', just south from the weapons shop. The first neighbour house to the east of the trademarket, just right next to the street, is Mike Miller's house. Speak to him. H<He wants the angry pixie's head, which you should drop right next to him.>",
   },
   {
      name       => "Jeweler Quest",
      match      => ['flag', 'jeweler_quest_1'],
      difficulty => 'tough fighting',
      start      => "You have to travel to Valleynoy. You find a ship to Valleynoy in Navar. Take it and search the valley of the jeweler town. Go to the welcome house and speak to the old man there."
   }
);

sub name2quest {
   my ($n) = @_;
   for my $q (@QUESTS) {
      if ($n =~ /\Q$q->{name}\E/i) {
         return $q
      }
   }
   return undef;
}

# this is the main command interface for the NPC
cf::register_script_function "quest_master::talk" => sub {
   my ($who, $msg, $npc) = @_;
   my ($cmd, $arguments) = split /\s+/, $msg, 2;
   $cmd = lc $cmd;

   if ($cmd eq 'finished'
       || $cmd eq 'unfinished') {
      my @finished;
      my @unfinished;

      for my $q (@QUESTS) {
         my $ok = 0;

         if ($q->{match}->[0] eq 'marker') {
            ext::map_lib::rec_inv_by_slaying ($who, $q->{match}->[1], sub { $ok = 1 });

         } elsif ($q->{match}->[0] eq 'flag') {
            if (exists $who->{dialog_flag}->{$q->{match}->[1]}
                && $who->{dialog_flag}->{$q->{match}->[1]}) {
               $ok = 1;
            }
         }

         if ($ok) {
            push @finished, $q;
         } else {
            push @unfinished, $q;
         }
      }

      if ($cmd eq 'finished') {
         $who->reply ($npc, "You finished these quests: ");
         for (@finished) {
            $who->reply ($npc, "- \"$_->{name}\"");
         }

      } else {
         $who->reply ($npc, "You didn't finish these quests yet: ");
         for (@unfinished) {
            $who->reply ($npc, "- \"$_->{name}\"");
         }
      }

   } elsif ($cmd eq 'start') {
      if ($arguments eq '') {
         $who->reply ($npc, "If you want to have a starting pointer for the following quests enter this into the message entry: 'start <name of quest>', for example: 'start hero of scorn'.");
         $who->reply ($npc, "I know starting points for these quests:");
         for my $q (@QUESTS) {
            next unless $q->{start};
            $who->reply ($npc, "- \"$q->{name}\"");
         }

      } else {
         my $q = name2quest ($arguments);

         if ($q) {
            $who->reply ($npc,
               "I can give you this starting point for the quest '$q->{name}': $q->{start}"
            );

         } else {
            $who->reply ($npc, "I don't know about the quest '$arguments'...");
         }
      }

   } elsif ($cmd eq 'spoiler') {

      if ($arguments eq '') {
         $who->reply ($npc, "If you want to have the spoiler for one the following quests enter this into the message entry: 'spoiler <name of quest>', for example: 'spoiler hero of scorn'.");
         $who->reply ($npc, "I know spoilers for these quests:");
         for my $q (@QUESTS) {
            next unless $q->{spoiler};
            $who->reply ($npc, "- \"$q->{name}\", cost: "
                               . cf::cost_string_from_value ($q->{spoiler_cost}));
         }

      } else {
         my $q = name2quest ($arguments);
         if ($q) {
            if ($who->pay_amount ($q->{spoiler_cost})) {
               $who->reply ($npc,
                  "Ok, I received ".cf::cost_string_from_value ($q->{spoiler_cost})
                  . " from you. This is the spoiler for the quest "
                  . "'$q->{name}': $q->{spoiler}"
               );
            } else {
               $who->reply ($npc,
                  "I'm sorry, but you don't have enough money to pay the spoiler "
                  . "for the quest '$q->{name}', it would cost you "
                  . cf::cost_string_from_value ($q->{spoiler_cost}) . "."
               );
            }

         } else {
            $who->reply ($npc, "I don't know about the quest '$arguments'...");
         }
      }

   } elsif ($cmd eq 'erase_quest_from_me') {
      my $q = name2quest ($arguments);

      if ($q->{match}->[0] eq 'marker') {
         my $force;
         ext::map_lib::rec_inv_by_slaying ($who, $q->{match}->[1], sub { $force = $_[0] });
         if ($force) {
            $force->destroy;
            $who->reply ($npc, "Successfully erased quest marker '$q->{name}' from you!");

         } else {
            $who->reply ($npc, "I'm sorry, but you never had a quest marker for the '$q->{name}' quest!");
         }

      } elsif ($q->{match}->[0] eq 'flag') {
         delete $who->{dialog_flag}->{$q->{match}->[1]};
         $who->reply ($npc, "Successfully erased quest flag '$q->{name}' from you!");
      }

   } else {
      $who->reply ($npc, <<REPL);
Welcome adventurer! What do you want to do?

Do you want me to list your finished quests?
Or do you want a list of unfinished quests?
Or maybe you want a starting point, or even a spoiler, for a quest?
REPL
   }

   1
};

