=head1 NAME

NPC_Dialogue

=head1 DESCRIPTION

NPC dialogue support module.

=over 4

=cut

package NPC_Dialogue;

use common::sense;

sub parse_message($) {
   map [split /\n/, $_, 2],
      grep length,
         split /^\@match /m,
            $_[0]
}

sub new {
   my ($class, %arg) = @_;

   $arg{ob} = $arg{pl}->ob;

   my $self = bless {
      %arg,
   }, $class;

   $self->{match} ||= [parse_message $self->{npc}->msg];

   $self;
}

sub greet {
   my ($self) = @_;

   $self->tell ("hi")
}

=item ($reply, @topics) = $dialog->tell ($msg)

Tells the dialog object something and returns its response and optionally
a number of topics that are refered to by this topic.

It supports a number of command constructs. They have to follow the
C<@match> directive, and there can be multiple commands that will be
executed in order.

=over 4

=item @comment text...

A single-line comment. It will be completely ignored.

=item @parse regex

Parses the message using a perl regular expression (by default
case-insensitive). Any matches will be available as C<< $match->[$index]
>>.

If the regular expression does not match, the topic is skipped.

Example:

   @match deposit
   @parse deposit (\d+) (\S+)
   @eval bank::deposit $match->[0], $match->[1]

=item @cond perl

Evaluates the given perl code. If it returns false (or causes an
exception), the topic will be skipped, otherwise topic interpretation is
resumed.

The following local variables are defined within the expression:

=over 4

=item $who - The cf::object::player object that initiated the dialogue.

=item $npc - The NPC (or magic_ear etc.) object that is being talked to.

=item $map - The map the NPC (not the player) is on.

=item $msg - The actual message as passed to this method.

=item $match - An arrayref with previous results from C<@parse>.

=item $state - A hashref that stores state variables associated
with the NPC and the player, that is, it's values relate to the the
specific player-NPC interaction and other players will see a different
state. Useful to react to players in a stateful way. See C<@setstate> and
C<@ifstate>.

=item $flag - A hashref that stores flags associated with the player and
can be seen by all NPCs (so better name your flags uniquely). This is
useful for storing e.g. quest information. See C<@setflag> and C<@ifflag>.

=item $find - see @find, below.

=back

The environment is that standard "map scripting environment", which is
limited in the type of constructs allowed (no loops, for example).

Here is a example:

=over 4

=item B<matching for an item name>

   @match hi
   @cond grep $_->name =~ /royalty/, $who->inv
   You got royalties there! Wanna have!

You may want to change the C<name> method there to something like C<title>,
C<slaying> or any other method that is allowed to be called on a
C<cf::object> here.

=item B<matching for an item name and removing the matched item>

   @match found earhorn
   @cond grep $_->slaying =~ /Gramp's walking stick/, $who->inv
   @eval my @g = grep { $_->slaying =~ /Gramp's walking stick/ } $who->inv; $g[0]->decrease;
   Thanks for the earhorn!

This example is a bit more complex. The C<@eval> statement will search
the players inventory for the same term as the C<@cond> and then
decreases the number of objects used there.

(See also the map: C<scorn/houses/cornerbrook.map> for an example how this is
used in the real world :-)

=back

=item @eval perl

Like C<@cond>, but proceed regardless of the outcome.

=item @msg perl

Like C<@cond>, but the return value will be stringified and prepended to
the reply message.

=item @check match expression

Executes a match expression (see
http://pod.tst.eu/http://cvs.schmorp.de/deliantra/server/lib/cf/match.pm)
to see if it matches.

C<self> is the npc object, C<object>, C<source> and C<originator> are the
player communicating with the NPC.

If the check fails, the match is skipped.

=item @find match expression

Like C<@check> in that it executes a match expression, but instead of
failing, it gathers all objects into an array and provides a reference to
the array in the C<$find> variable.

When you want to skip the match when no objects have been found, combine
C<@find> with C<@cond>:

   @match see my spellbook
   @find type=SPELLBOOK in inv
   @cond @$find
   It looks dirty.
   @match see my spellbook
   I can't see any, where do you have it?

=item @setstate state value

Sets the named state C<state> to the given C<value>. State values are
associated with a specific player-NPC pair, so each NPC has its own state
with respect to a particular player, which makes them useful to store
information about previous questions and possibly answers. State values
get reset whenever the NPC gets reset.

See C<@ifstate> for an example.

=item @ifstate state value

Requires that the named C<state> has the given C<value>, otherwise this
topic is skipped. For more complex comparisons, see C<@cond> with
C<$state>. Example:

  @match quest
  @setstate question quest
  Do you really want to help find the magic amulet of Beeblebrox?
  @match yes
  @ifstate question quest
  Then fetch it, stupid!

=item @setflag flag value

Sets the named flag C<flag> to the given C<value>. Flag values are
associated with a specific player and can be seen by all NPCs. with
respect to a particular player, which makes them suitable to store quest
markers and other information (e.g. reputation/alignment). Flags are
persistent over the lifetime of a player, so be careful :)

Perversely enough, using C<@setfflag> without a C<value> clears the flag
as if it was never set, so always provide a flag value (e.g. C<1>) when
you want to set the flag.

See C<@ifflag> for an example.

=item @ifflag flag value

Requires that the named C<flag> has the given C<value>, otherwise this
topic is skipped. For more complex comparisons, see C<@cond> with
C<$flag>.

If no C<value> is given, then the ifflag succeeds when the flag is true.

Example:

  @match I want to do the quest!
  @setflag kings_quest 1
  Then seek out Bumblebee in Navar, he will tell you...
  @match I did the quest
  @ifflag kings_quest 1
  Really, which quets?

And Bumblebee might have:

  @match hi
  @ifflag kings_quest
  Hi, I was told you want to do the kings quest?

=item @trigger connected-id [state]

Trigger all objects with the given connected-id.

When the state argument is omitted the trigger is stateful and retains an
internal state per connected-id. There is a limitation to the use of this: The
state won't be changed when the connection is triggered by other triggers.  So
be careful when triggering the connection from other objects.

When a state argument is given it should be a positive integer. Any value
C<!= 0> will 'push' the connection (in general, you should specify C<1>
for this) and C<0> will 'release' the connection. This is useful for
example when you want to let an NPC control a door.

Trigger all objects with the given connected-id by 'releasing' the connection.

=item @playersound face-name

Plays the given sound face (either an alias or sound file path) so that
only the player talking to the npc can hear it.

=item @npcsound face-name

Plays the given sound face (either an alias or sound file path) as if
the npc had made that sound, i.e. it will be located at the npc and all
players near enough can hear it.

=item @addtopic topic

Adds the given topic names (separated by C<|>) to the list of topics
returned.

=back

=cut

sub tell {
   my ($self, $msg) = @_;

   my $lcmsg = lc $msg;

   topic:
   for my $match (@{ $self->{match} }) {
      for (split /\|/, $match->[0]) {
         if ($_ eq "*" || $lcmsg =~ /\b\Q$_\E\b/i) {
            my $reply = $match->[1];
            my @kw;

            my @replies;
            my @match; # @match/@parse command results

            my $state = $self->{npc}{$self->{ob}->name}{dialog_state} ||= {};
            my $flag  = $self->{ob}{dialog_flag}                      ||= {};

            my @find;

            my %vars = (
               who   => $self->{ob},
               npc   => $self->{npc},
               map   => $self->{npc}->map,
               state => $state,
               flag  => $flag,
               msg   => $msg,
               match => \@match,
               find  => \@find,
            );

            local $self->{ob}{record_replies} = \@replies;

            # now execute @-commands (which can result in a no-match)
            while ($reply =~ s/^\@(\w+)\s*([^\n]*)\n?//) {
               my ($cmd, $args) = ($1, $2);

               if ($cmd eq "parse" || $cmd eq "match") { # match is future rename
                  no re 'eval'; # default, but make sure
                  @match = $msg =~ /$args/i
                     or next topic;

               } elsif ($cmd eq "comment") {
                  # nop

               } elsif ($cmd eq "playersound") {
                  $self->{ob}->contr->play_sound (cf::sound::find $args);

               } elsif ($cmd eq "npcsound") {
                  $self->{npc}->play_sound (cf::sound::find $args);

               } elsif ($cmd eq "cond") {
                  cf::safe_eval $args, %vars
                     or next topic;

               } elsif ($cmd eq "eval") {
                  cf::safe_eval $args, %vars;
                  warn "\@eval evaluation error: $@\n" if $@;

               } elsif ($cmd eq "check") {
                  eval {
                     cf::match::match $args, $self->{ob}, $self->{npc}, $self->{ob}
                        or next topic;
                  };
                  warn "\@check evaluation error: $@\n" if $@;

               } elsif ($cmd eq "find") {
                  @find = eval {
                     cf::match::match $args, $self->{ob}, $self->{npc}, $self->{ob}
                  };
                  warn "\@find evaluation error: $@\n" if $@;

               } elsif ($cmd eq "msg") {
                  push @replies, [$self->{npc}, (scalar cf::safe_eval $args, %vars)];

               } elsif ($cmd eq "setflag") {
                  my ($name, $value) = split /\s+/, $args, 2;
                  defined $value ? $flag->{$name} = $value
                                 : delete $flag->{$name};

               } elsif ($cmd eq "setstate") {
                  my ($name, $value) = split /\s+/, $args, 2;
                  defined $value ? $state->{$name} = $value
                                 : delete $state->{$name};

               } elsif ($cmd eq "ifflag") {
                  my ($name, $value) = split /\s+/, $args, 2;
                  defined $value ? $flag->{$name} eq $value
                                 : $flag->{$name}
                     or next topic;

               } elsif ($cmd eq "ifstate") {
                  my ($name, $value) = split /\s+/, $args, 2;
                  defined $value ? $state->{$name} eq $value
                                 : $state->{$name}
                     or next topic;

               } elsif ($cmd eq "trigger") {
                  my ($con, $state) = split /\s+/, $args, 2;

                  if (defined $state) {
                    $self->{npc}->map->trigger ($con, $state, $self->{npc}, $self->{ob});
                  } else {
                    my $rvalue = \$self->{npc}{dialog_trigger}{$con+0};
                    $self->{npc}->map->trigger ($con, $$rvalue = !$$rvalue, $self->{npc}, $self->{ob});
                  }

               } elsif ($cmd eq "addtopic") {
                  push @kw, split /\|/, $args;
                  $self->{add_topic}->(split /\s*\|\s*/, $args) if $self->{add_topic};

               } elsif ($cmd eq "deltopic") {
                  # not yet implemented, do it out-of-band
                  $self->{del_topic}->(split /\s*\|\s*/, $args) if $self->{del_topic};

               } else {
                  warn "unknown dialogue command <$cmd,$args> used (from " . $self->{npc}->msg . ")";
               }
            }

            delete $self->{npc}{$self->{ob}->name}{dialog_state} unless %$state;
            delete $self->{ob}{dialog_flag}                      unless %$flag;

            # ignores flags and npc from replies
            $reply = join "\n", (map $_->[1], @replies), $reply;

            # now mark up all matching keywords
            for my $match (@{ $self->{match} }) {
               for (sort { (length $b) <=> (length $a) } split /\|/, $match->[0]) {
                  if ($reply =~ /\b\Q$_\E\b/i) {
                     push @kw, $_;
                     last;
                  }
               }
            }

            $self->{npc}->use_trigger ($self->{ob})
               if $self->{npc}->type == cf::MAGIC_EAR;

            return wantarray ? ($reply, @kw) : $reply;
         }
      }
   }

   ()
}

1
