#! perl

sub set_package {
   my ($pkg, $from, $to, $bagname, $weight) = @_;

   $pkg->name ("$bagname T: $to F: $from");
   $pkg->weight_limit ($weight);
   $pkg->stats->Str (0);
}

# prices [in silver]
my %prices = (
   pen => [
      10000,  'stylus',
      sub { $_[0]->name ('IPO Writing Pen'); $_[0]->value (0); }
   ],
   literacy => [
      10000, 'scroll_literacy',
      sub { $_[0]->value (0) }
   ],
   mailscroll => [
      50, 'mailscroll_empty',
      sub {
         $_[0]->name    ("mailscroll T: $_[2] F: $_[1]");
         $_[0]->name_pl ("mailscrolls T: $_[2] F: $_[1]");
         $_[0]->value   (0);
      },
      'plarg'
   ],
   bag      => [  100, 'r_sack', sub { set_package (@_, bag     =>   5000) }, 'plarg' ],
   package  => [ 1000, 'r_sack', sub { set_package (@_, package =>  50000) }, 'plarg' ],
   carton   => [ 2000, 'r_sack', sub { set_package (@_, carton  => 100000) }, 'plarg' ],
   mailwarning => [
      0, 'diploma',
      sub {
         $_[0]->name    ("mailwarning T: $_[2] F: $_[1]");
         $_[0]->name_pl ("mailwarnings T: $_[2] F: $_[1]");
         $_[0]->value   (0);
      },
      'plarg'
   ],
);

my %mailtypes = (
   1 => ['scroll', 'mailscroll'],
   2 => ['note', 'newspaper'],
   3 => ['diploma', 'mailwarning'],
);

sub notify_players {
   my (%sent_targets) = @_;

   # lets message player ingame: this is a NEW feature from the perl IPO :-)
   for (keys %sent_targets) {
      if (my $player = cf::player::find_active $_) {
         my $cnt = $sent_targets{$_};

         if ($cnt == 1) {
            $player->ob->message ("You've got new mail.");
         } else {
            $player->ob->message ("You've got $cnt new mails.");
         }
      }
   }
}

sub create_object {
   my ($name, $map, $x, $y, $cb, @a) = @_;
   my $o = cf::object::new $name;
   my $r = $cb->($o, @a);
   $map->insert ($o, $x, $y);
   $r
}

# this handler notifies the player of new mail
cf::player->attach (
   on_login => sub {
      my ($pl) = @_;

      my $cnt = @{ $pl->{ip_mails} };

      if ($cnt == 1) {
         $pl->ob->message ("You got one mail.");
      } elsif ($cnt > 1) {
         $pl->ob->message ("You got $cnt mails.");
      } else {
         $pl->ob->message ("You haven't got any mail.");
      }
   },
);

# this event handler handles receiving of mails
cf::object::attachment ipo_mailbox =>
   on_apply => sub {
      my ($box, $ob) = @_;

      my $cnt;
      my $mails = $ob->contr->{ipo_mails} || [];

      # count the mails that are in the container
      # FIXME: the problem with on_apply is that it is called even when
      # the player closes the container. so we get a 'You have X mails.' message
      # twice. - This bug existed also with the old python plugin

      my $plname = $ob->name;

      for ($box->inv) {
         ++$cnt if $_->name =~ /^mail(?:scroll|warning) F: \S+ T: \Q$plname\E/;
      }

      for (@$mails) {
         my ($type, $from, $msg) = @$_;
         $type = $mailtypes{$type || 1} || ['scroll', 'mailscroll'];
         my $mail = cf::object::new $type->[0];
         $mail->name    ("$type->[1] F: $from T: $plname");
         $mail->name_pl ("$type->[1]s F: $from T: $plname");
         $mail->msg     ($msg);
         $mail->value   (0);
         $box->insert ($mail);
      }

      $cnt += @$mails;

      if ($cnt == 1) {
         $ob->message ("You got one mail.");
      } elsif ($cnt > 1) {
         $ob->message ("You got $cnt mails.");
      } else {
         $ob->message ("You haven't got any mail.");
      }

      delete $ob->contr->{ipo_mails};
   },
   # this event handler handles the sending of mails
   on_close => sub {
      my ($box, $ob) = @_;

      my @mails = grep $_->name =~ /^mail(?:scroll|warning) [TF]: /, $box->inv;
      $_->remove for @mails;

      # we can lose mails here, when the player is unloadable and the server crashes. shit happens.
      cf::async {
         my %sent_targets;

         for (@mails) {
            if ($_->name =~ m/^mail(scroll|warning) T: (\S+) F: (\S+)/) {
               CFMail::send_mail ($1 eq 'scroll' ? 1 : 3, $2, $3, $_->msg);
               $ob->message ("Sent mail$1 to $2 (from $3).");
               ++$sent_targets{$2};

            } elsif ($_->name =~ m/^mail(scroll|warning) F: (\S+) T: (\S+)/) {
               # this is for mails that remain in the queue for the player
               CFMail::store_mail ($1 eq 'scroll' ? 1 : 3, $3, $2, $_->msg);
            }

            $_->destroy;
         }

         notify_players (%sent_targets);
      };
   },
;

# this is the main command interface for the IPO NPC
cf::register_script_function "ipo::command" => sub {
   my ($who, $msg, $npc) = @_;
   my ($cmd, $arguments) = split /\s+/, $msg, 2;
   $cmd = lc $cmd;

   my $pl = cf::player::find_active $who->name;
   my ($x, $y) = ($pl->ob->x, $pl->ob->y);

   if (my $pr = $prices{$cmd}) {
      if ($cmd eq 'mailwarning' and !$who->flag (cf::FLAG_WIZ)) {
         return 1;
      }

      if ($who->pay_amount ($pr->[0])) {
         cf::async {
            if ($pr->[3] && not cf::player::exists $arguments) {
               $who->reply ($npc, "Sorry, there is no '$arguments'");
            } else {
               create_object ($pr->[1], $who->map, $x, $y, $pr->[2], $who->name, $arguments);
               $who->reply ($npc, "Here is your $cmd");
            }
         }
      } else {
         $who->reply ($npc, "Sorry, you don't have enough money.");
      }

   } elsif ($cmd eq 'receive') {
      cf::async {
         $Coro::current->{desc} = "ipo receive";

         my $storage = cf::map::find ("/planes/IPO_storage");
         unless ($storage) {
            $who->reply ($npc, "Sorry, our package delivery service is currently in strike. Please come back later.");
            return 1;
         }
         $storage->load;

         my $plname = $who->name;
         my $cnt;
         for ($storage->at (2, 2)) {
            if ($_->name () =~ /^\S+ F: \S+ T: \Q$plname\E$/) {
               $who->insert ($_);
               $cnt++;
            }
         }

         if ($cnt) {
            $who->reply ($npc, $cnt == 1 ? "Here is your package." : "Here are your packages.");
         } else {
            $who->reply ($npc, "Sorry, no deliveries for you sir.");
         }
      }

   } elsif ($cmd eq 'send') {
      unless ($arguments =~ /^\S+$/) {
         $who->reply ($npc, "Send to who?");
         return 1;
      }

      cf::async {
         $Coro::current->{desc} = "ipo send";

         my $storage = cf::map::find ("/planes/IPO_storage");
         unless ($storage) {
            $who->reply ($npc, "Sorry, our package delivery service is currently in strike. Please come back later.");
            return 1;
         }
         $storage->load;

         my $cnt;
         for ($who->inv) {
            if ($_->name () =~ /^(bag|package|carton) T: \Q$arguments\E F: (\S+)$/) {
               $_->name ("$1 F: $2 T: $arguments");
               $storage->insert ($_, 2, 2);
               $cnt++;
            }
         }

         if ($cnt) {
            $who->reply ($npc, $cnt == 1 ? "Package sent to $arguments." : "Sent $cnt packages to $arguments\n");
            CFMail::send_mail (1, $arguments, $who->name, "You got $cnt packages from " . $who->name);
            notify_players ($arguments => 1);
         } else {
            $who->reply ($npc, "Sorry, found no package to send to $arguments.");
         }
      }

   } else {
      $who->reply ($npc,
         sprintf "How can I help you?\n"
          . "Here is a quick list of commands I understand:\n\n"
          . " - pen                 (%s)\n"
          . " - literacy            (%s)\n"
          . " - mailscroll <friend> (%s)\n"
          . " - bag <friend>        (%s)\n"
          . " - package <friend>    (%s)\n"
          . " - carton <friend>     (%s)\n"
          . " - send <friend>       (send bags/packages/cartons)\n"
          . " - receive             (to receive packages for you)\n"
          . ($who->flag (cf::FLAG_WIZ) ? " - mailwarning <player>\n" : ""),
            cf::cost_string_from_value($prices{'pen'}[0]),
            cf::cost_string_from_value($prices{'literacy'}[0]),
            cf::cost_string_from_value($prices{'mailscroll'}[0]),
            cf::cost_string_from_value($prices{'bag'}[0]),
            cf::cost_string_from_value($prices{'package'}[0]),
            cf::cost_string_from_value($prices{'carton'}[0])
      );
   }
   1
};

package CFMail;

use POSIX qw/strftime/;

rename "$cf::LOCALDIR/crossfiremail", "$cf::LOCALDIR/crossfiremail.is-now-on-player";

sub store_mail {
   my ($type, $toname, $fromname, $message) = @_;

   my $pl = cf::player::find $toname
      or return;

   push @{ $pl->{ipo_mails} }, [$type, $fromname, $message];
}

sub send_mail {
   my ($type, $toname, $fromname, $message) = @_;

   my $time = strftime ("%a, %d %b %Y %H:%M:%S UTC", gmtime AE::now);
   my $msg = "From: $fromname\rTo: $toname\rDate: $time\n\n$message\n";

   store_mail $type, $toname, $fromname, $msg;
}

