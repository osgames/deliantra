#! perl # depends=widget mandatory

# this module implements a rather fancy worldmap

CONF WORLDMAP_UPDATE_INTERVAL = 2;

our $GENCOUNT = 0;
our %PLAYERINFO;

our ($MAPW, $MAPH) = (1024, 1024); # it's useful to know the map width/height in pixels

sub update_worldmap {
   my ($ws) = @_;

   return if $GENCOUNT == $ws->{gencount};
   $ws->{gencount} = $GENCOUNT;

   my $old = delete $ws->{labels};
   my $new;

   my $name = $ws->{ns}->pl->ob->name;

   while (my ($k, $v) = each %PLAYERINFO) {
      my $label = (delete $old->{$k}) || do {
         my $label = $ws->new (Label =>
            text     => $k,
            fontsize => 0.2,
         );

         my $marker = $ws->new (Face =>
            face => $ws->{ns}->need_face ("res/map-arrow"),
         );
         my $children = [$label, $marker];

         $ws->{canvas}->add (my $vbox = $ws->new (VBox =>
            children => $children,
            c_halign => -.5,
            c_valign => -1,
         ));
         $vbox->{children} = $children;

         $vbox
      };

      $new->{$k} = $label;

      if ($v != $label->{prevpos}) {
         $label->set (c_x => $v->[0], c_y => $v->[1]);
         $label->{prevpos} = $v;
         $ws->{window}->make_visible ($v->[0], $v->[1], .2)
            if $k eq $name;
      }
   }

   $ws->{labels} = $new;
}

sub create_widgets {
   my ($ns) = @_;

   my $ws = $ns->new_widgetset;

   $ws->{toplevel} = my $w = $ws->new (Toplevel =>
      title   => "Worldmap",
      name    => "server_item_worldmap",
      force_w => 400,
      force_h => 400,
      x       => "center",
      y       => "center",
      has_close_button => 1,
      on_delete => sub { shift->hide },
      on_visibility_change => sub {
         $_[0]{visibility} = $_[1];
         update_worldmap $_[0]{ws} if $_[1];
      },
   );

   $w->add (my $sw = $ws->{window} = $ws->new (ScrolledWindow => scroll_x => 1, scroll_y => 1));
   $sw->add (my $canvas = $ws->{canvas} = $ws->new (Canvas => expand => 1));

   $ws->{mapface} = $ws->new (Face =>
      size_w => undef,
      size_h => undef,
      face   => $ws->{ns}->need_face ("res/worldmap"),
   );

   $ws->{canvas}->add ($ws->{mapface});

   $ws
}

cf::object::attachment item_worldmap =>
   on_apply => sub {
      my ($self, $who) = @_;

      my $ns = $who->contr->ns;

      if ($ns->{can_widget}) {
         my $ws = $ns->{ws_worldmap} ||= create_widgets $ns;
         $ws->{toplevel}->toggle_visibility;
      } else {
         $ns->send_msg ("log", "Your client doesn't support the (required) widget extension. Try the deliantra client at http://www.deliantra.net/.", cf::NDI_RED);
      }

      cf::override 1;
   },
;

cf::post_init {
   our $UPDATER = cf::async_ext {
      $Coro::current->{desc} = "worldmap updater";

      while () {
         Coro::AnyEvent::sleep $WORLDMAP_UPDATE_INTERVAL;

         cf::get_slot 0.01, -50, "worldmap update";

         ++$GENCOUNT;

         # recalculate player info
         my %new;
         for (values %cf::PLAYER) {
            my $map = $_->ob->map
               or next;
            $map =~ /^\/world\/world_(\d\d\d)_(\d\d\d)/
               or next;

            my $ob = $_->ob;
            my $x = ($1 - 100) * 50 + $ob->x;
            my $y = ($2 - 100) * 50 + $ob->y;

            0 <= $x && 0 <= $y && $x < 1500 && $y < 1500
               or next;

            $x = int $x * $MAPW / 1500;
            $y = int $y * $MAPH / 1500;

            my $name = $ob->name;

            if (my $pi = delete $PLAYERINFO{$name}) {
               if ($pi->[0] == $x && $pi->[1] == $y) {
                  $new{$name} = $pi;
                  next;
               }
            }
                  
            $new{$name} = [$x, $y];
         }

         *PLAYERINFO = \%new;

         cf::get_slot 0.03, -50, "worldmap socket update";
         for (values %cf::PLAYER) {
            my $ns = $_->ns
               or next;

            update_worldmap $ns->{ws_worldmap}
               if $ns->{ws_worldmap} && $ns->{ws_worldmap}{toplevel}{visibility};
         }
      }
   };
};

