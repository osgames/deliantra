#! perl

use List::Util qw/min/;

my $WC_BASE_VALUE    = 15 * 5000;  # 15r
my $AC_BASE_VALUE    = 40 * 5000;  # 40r
my $DAM_BASE_VALUE   = 200;        # 4p
my $MAGIC_BASE_VALUE = 100 * 5000; # 100r

my $MAX_LEVEL     = cf::settings->max_level;

# maximum values of additions at $MAX_LEVEL difficulty
my $MAX_ADD_WC    = 10;
my $MAX_ADD_AC    = 5;  # weapons should rather give wc than ac
my $MAX_ADD_DAM   = 45;
my $MAX_ADD_MAGIC = 5;

# TODO: should become part of some stdlib
sub rand_range($$) {
   $_[0] + ($_[1] - $_[0]) * rand
}

sub determine_weapon_value {
   my ($weapon) = @_;

   my $arch  = $weapon->arch;
   my $value = $arch->value;

   my $wc_diff  = $weapon->stats->wc  - $arch->stats->wc;
   my $ac_diff  = $weapon->stats->ac  - $arch->stats->ac;
   my $dam_diff = $weapon->stats->dam - $arch->stats->dam;

   my $magic_diff = $weapon->magic - $arch->magic;

   if ($wc_diff    > 0) { $value += $WC_BASE_VALUE    * 1.5 * $wc_diff    * rand_range .5, 1 }
   if ($ac_diff    > 0) { $value += $AC_BASE_VALUE    * 1.5 * $ac_diff    * rand_range .5, 1 }
   if ($dam_diff   > 0) { $value += $DAM_BASE_VALUE   * ($dam_diff ** 2.4) * rand_range .7, 1 }
   if ($magic_diff > 0) { $value += $MAGIC_BASE_VALUE * 1.5 * $magic_diff * rand_range .5, 1 }

   #warn sprintf "WC DIFF: %3d, %3d, %3d, %3d | %10d = %10.3f r\n",
   #     $dam_diff, $wc_diff, $ac_diff, $magic_diff, $value,  $value / 5000
   #   if $cf::CFG{ext_bonus_debug};

   $weapon->value ($value);
}

cf::object->attach (
   type         => cf::WEAPON,
   on_add_bonus => sub {
      my ($item, $creator, $diff, $max_magic, $flags) = @_;
      return if $flags || !$creator;

      $diff /= $MAX_LEVEL;

      $item->stats->wc  ($item->stats->wc  + int $MAX_ADD_WC  * (1 + rand) * $diff * rand_range .5, 1) if rand() <= 0.2;
      $item->stats->ac  ($item->stats->ac  + int $MAX_ADD_AC  * (1 + rand) * $diff * rand_range .5, 1) if rand() <= 0.15;
      $item->stats->dam ($item->stats->dam + int $MAX_ADD_DAM * (1 + rand) * $diff * rand_range .5, 1) if rand() <= 0.85;
      $item->magic      ($item->magic      + int $MAX_ADD_MAGIC            * $diff * rand_range .5, 1) if rand() <= 0.1;

      determine_weapon_value $item;

      cf::override;
   },
);

