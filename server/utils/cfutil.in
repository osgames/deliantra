#!@PERL@

#
# This file is part of Deliantra, the Roguelike Realtime MMORPG.
#
# Copyright (©) 2007,2008,2009,2010,2011,2012 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
#
# Deliantra is free software: you can redistribute it and/or modify it under
# the terms of the Affero GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the Affero GNU General Public License
# and the GNU General Public License along with this program. If not, see
# <http://www.gnu.org/licenses/>.
#
# The authors can be reached via e-mail to <support@deliantra.net>
#

use common::sense;

my $prefix      = "@prefix@";
my $exec_prefix = "@exec_prefix@";
my $datarootdir = "@datarootdir@";
my $DATADIR     = "@datadir@/@PACKAGE@";

my $CONVERT  = "@CONVERT@";
my $IDENTIFY = "@IDENTIFY@";
my $OPTIPNG  = "@OPTIPNG@";
my $RSYNC    = "@RSYNC@";
my $PNGNQ    = "@PNGNQ@";

use Getopt::Long;
use File::Temp;
use POSIX ();
use Carp;

use YAML::XS ();
use Digest::MD5 ();
use Storable ();
use JSON::XS ();
use IO::AIO ();
use Compress::LZF ();

use AnyEvent;
use Coro 5.12;
use Coro::EV;
use Coro::AIO;
use Coro::Util;
use Coro::Channel;
use Coro::AnyEvent;
use Coro::Storable; $Storable::canonical = 1;

use Deliantra;

$SIG{QUIT} = sub { Carp::cluck "QUIT" };

sub usage {
   warn <<EOF;
Usage: cfutil [-v] [-q] [--force] [--cache]
              [--install-arch path]
              [--install-maps maps]
              [--print-statedir]
              [--print-confdir]
              [--print-datadir]
              [--print-libdir]
              [--print-bindir]
EOF
   exit 1;
}

my $VERBOSE = 1;
my $CACHE = 0;
my $FORCE;
my $TMPDIR  = "/tmp/cfutil$$~";
my $TMPFILE = "aaaa0";
my @COMMIT;

our %COLOR = (
   # original cf colours
   black      =>  0,
   white      =>  1,
   navy       =>  2, # "dark blue"
   red        =>  3,
   orange     =>  4,
   blue       =>  5, # "light blue"
   darkorange =>  6,
   green      =>  7,
   lightgreen =>  8,
   grey       =>  9,
   brown      => 10, # "yellow"
   gold       => 11, # "light yellow"
   tan        => 12, # yellowish gray

   # new for deliantra
   none       => 13,

   # compatibility to existing archetypes
   lightblue  =>  5,
   gray       =>  9,
   yellow     => 11,
   khaki      => 12,
);

END { system "rm", "-rf", $TMPDIR }

my $s_INT  = AE::signal INT  => sub { exit 1 };
my $s_TERM = AE::signal TERM => sub { exit 1 };

our %hash;

# here we could try to avoid collisions and reduce chksum size further
sub make_hash($\$\$;$) {
   my ($id, $dataref, $hashref, $clen) = @_;

   my $hash = substr +(Digest::MD5::md5 $$dataref), 0, $clen || 5;

   if (exists $hash{$hash}) {
      # hash collision, but some files are simply identical
      if (${$hash{$hash}[1]} ne $$dataref)  {
         warn "ERROR: hash collision $hash{$hash}[0] vs. $id (increase the default \$clen in make_hash?)\n";
         exit 1;
      } else {
         print "$hash{$hash}[0] and $id are identical (which is fine).\n" if $VERBOSE >= 3;
      }
   }
   $hash{$hash} = [$id, $dataref, $hashref];

   $$hashref = $hash;
}

sub optipng($) {
   system $OPTIPNG, "-o5", "-i0", "-q", $_[0];
   die "$_[0] has zero size, aborting." unless -s $_[0];
}

mkdir $TMPDIR, 0700
   or die "$TMPDIR: $!";

sub fork_exec(&) {
   my ($cb) = @_;

   if (my $pid = fork) {
      my $current = $Coro::current;
      my $w = AnyEvent->child (pid => $pid, cb => sub { $current->ready });
      Coro::schedule;
   } else {
      eval { $cb->() };
      POSIX::_exit 0 unless $@;
      warn $@;
      POSIX::_exit 1;
   }
}

sub imgsize($) {
   open my $fh, "-|", $IDENTIFY, qw(-ping -format %w,%h --), $_[0]
      or die "$IDENTIFY: $!";

   Coro::AnyEvent::readable $fh;

   my ($w, $h) = split /,/, <$fh>;

   ($w+0, $h+0)
}

# make $dst from $src, if not uptodate, by calling $how
sub make_file($$$) {
   my ($src, $dst, $how) = @_;

   my $t = (aio_stat $dst) ? -1 : (stat _)[9];

   for (ref $src ? @$src : $src) {
      aio_stat $_
         and die "$_: $!";

      if ((stat _)[9] > $t) {
         # outdated, redo
         $how->();
         last;
      }
   }
}

sub inst_maps($) {
   my (undef, $path) = @_;

   print "\nInstalling '$path' to '$DATADIR/maps'\n\n";

   if (!-f "$path/regions") {
      warn "ERROR: '$path' does not look like a maps directory ('regions' file is missing).\n";
      exit 1 unless $FORCE;
   }

   system $RSYNC, "-av", "--chmod=u=rwX,go=rX",
          "$path/.", "$DATADIR/maps/.",
          "--exclude", "CVS", "--exclude", "/world-precomposed",
          "--delete", "--delete-excluded"
      and die "map installation failed.\n";

   print "maps installed successfully.\n";
}

{
   our %ANIMINFO;
   our %FACEINFO;
   our %RESOURCE;
   our @ARC;
   our %ARC;
   our $TRS;
   our %PNG;
   our $NFILE;
   our $PATH;

   our $QUANTIZE = "+dither -colorspace RGB -colors 256";

   our $c_arc = new Coro::Channel;
   our $c_any = new Coro::Channel;

   our @c_png;

   # texture catalog size, in 64x64 tiles
   sub TC_W() { 1024 / 64 }
   sub TC_H() { 1024 / 64 }

   our @TC = ([]); # texture catalogs
   
   sub commit_png($$$$$$) {
      my ($stem, $name, $data, $T, $w, $h) = @_;

      $FACEINFO{$name}{stem}     = substr $stem, 1 + length $PATH;
      $FACEINFO{$name}{"data$T"} = $data;
      $FACEINFO{$name}{w}        = $w;
      $FACEINFO{$name}{h}        = $h;
   }

   sub process_png {
      while (@c_png) {
         my ($path, $delete) = @{pop @c_png};

         my $png;
         aio_lstat $path;
         my ($size, $mtime) = (stat _)[7,9];

         if (0 > aio_load $path, $png) {
            warn "ERROR: $path: $!, skipping.\n";
            next;
         }

         my $stem = $path;
         my $T;

         if ($stem =~ s/\.32x32\.png~?$//) {
            $T = 32;
         } elsif ($stem =~ s/\.64x64\.png~?$//) {
            $T = 64;
         } else {
            warn "ERROR: $path: weird filename, skipping.\n";
            next;
         }

         # quickly extract width and height of the (necessarily PNG) image
         unless ($png =~ /^\x89PNG\x0d\x0a\x1a\x0a....IHDR(........)/s) {
            warn "ERROR: $path: not a recognized png file, skipping.\n";
            next;
         }

         my ($w, $h) = unpack "NN", $1;

         if ($w < $T || $h < $T) {
            warn "ERROR: $path: too small ($w $h), skipping.\n";
            next;
         }

         if ($w % $T || $h % $T) {
            warn "ERROR: $path: weird png size ($w $h), skipping.\n";
            next;
         }

         (my $base = $stem) =~ s/^.*\///;

         my $fi = $FACEINFO{$base};
         unless ($fi) {
            #warn "$path: <$base> not referenced by any archetype, skipping.\n";
            #next;
         }

         my $arc = $fi->{arc} || { };

         unless ($path =~ /~$/) {
            # possibly enlarge
            if (0 > aio_stat "$stem.64x64.png") {
               my $other = "$stem.64x64.png~";

               make_file $path, $other, sub {
                  fork_exec {
                     my $CROP;
                     my $SRC = "png:\Q$path\E";

                     my $is_floor = $arc->{is_floor};
                     my $is_wall  = 0;

                     my ($wall_pfx, $wall_dir, $wall_sfx);

                     if (
                        !$is_floor
                        && !$arc->{alive}
                        && $arc->{move_block} eq "all"
                        && $path =~ /^(.*_)([0-9A-F])(\.x11.*\.png)$/
                     ) {
                        ($wall_pfx, $wall_dir, $wall_sfx) = ($1, hex $2, $3);

                        unless (grep { !-e sprintf "%s%X%s", $wall_pfx, $_, $wall_sfx } 0..15) {
                           $is_wall = 1;
                        }
                     }

                     if ($is_wall || $is_floor) {
                        # add a 4px border and add other images around it
                        $CROP = "-shave 8x8 +repage";

                        $w += 8;
                        $h += 8;

                        $SRC = "-size ${w}x${h} xc:transparent";
                        $SRC .= " png:\Q$path\E -geometry +4+4 -composite";

                        # 8 surrounding images
                        for (
                           # x   y  b  r0  r1
                           [-1, -1, 0,  6],
                           [ 0, -1, 1, 10, 14],
                           [+1, -1, 0, 12],

                           [-1,  0, 8,  5,  7],
                           #
                           [+1,  0, 2,  5, 13],

                           [-1, +1, 0,  3],
                           [ 0, +1, 4, 10, 11],
                           [+1, +1, 0,  9],
                        ) {
                           my ($x, $y, $d, $r0, $r1) = @$_;

                           my $tile = $is_floor ? $path
                                    : $is_wall  ? sprintf "%s%X%s", $wall_pfx, ($wall_dir & $d) ? $r1 : $r0, $wall_sfx
                                    : die;

                           $SRC .= sprintf " png:%s -geometry %+d%+d -composite",
                                           "\Q$tile",
                                           $x * ($w - 8) + 4,
                                           $y * ($h - 8) + 4;
                        }
                     }

                     system "$CONVERT -depth 8 $SRC rgba:-"
                          . "| $exec_prefix/bin/cfhq2xa $w $h 0"
                          . "| $CONVERT -depth 8 -size ".($w * 2)."x".($h * 2)." rgba:- $CROP $QUANTIZE -quality 00 png32:\Q$other\E~"
                        and die "convert/cfhq2xa pipeline error: status $? ($!)";
                     optipng "$other~";
                     rename "$other~", $other;
                  };
               };

               push @c_png, [$other, !$CACHE];
            }

            # possibly scale down
            if (0 > aio_stat "$stem.32x32.png") {
               my $other = "$stem.32x32.png~";

               make_file $path, $other, sub {
                  fork_exec {
                     system "$CONVERT png:\Q$path\E -geometry 50% -filter lanczos $QUANTIZE -quality 00 png32:\Q$other\E~";
                     optipng "$other~";

                     # reduce smoothfaces >10000 bytes
                     # obsolete, no longer required
                     if (0 && $stem =~ /_S\./ && (-s "$other~") > 10000) {
                        my $ncolor = 256;
                        while () {
                           system "<\Q$other~\E $PNGNQ -s1 -n$ncolor >\Q$other~~\E";
                           optipng "$other~~";
                           last if 10000 > -s "$other~~";
                           $ncolor = int $ncolor * 0.9;
                           $ncolor > 8 or die "cannot reduce filesize to < 10000 bytes";
                        }

                        printf "reduced %s from %d to %d bytes using %d colours.\n",
                              $other, -s "$other~", -s "$other~~", $ncolor
                           if $VERBOSE >= 2;
                        rename "$other~~", "$other~";
                     }

                     die "$other~ has zero size, aborting." unless -s "$other~";
                     rename "$other~", $other;
                  };
               };

               #warn "scaled down $path to $other\n";#d#
               push @c_png, [$other, !$CACHE];
            }
         }

         (my $face = $stem) =~ s/^.*\///;

         for (@{ $fi->{derive} }) {
            my ($dir, $type) = @$_;

            my $geom = "${T}x${T}";
            my $dpath = "$dir/$type:$face.$geom.png~";

            if ($type eq "buildmaterial") {
               my $ov = "$PATH/mapbuilding/buildmaterial.png";
               make_file [$path, $ov], $dpath, sub {
                  fork_exec {
                     system "$CONVERT -size 64x64 xc:transparent \\( \Q$path\E -geometry 52x52+6+6 \\) -composite \Q$ov\E -composite -geometry $geom png:\Q$dpath\E~";
                     optipng "$dpath~";
                     rename "$dpath~", $dpath;
                  };
               };
            } elsif ($type eq "buildmaterial-box") {
               my $ov = "$PATH/mapbuilding/buildmaterial-box.png";
               make_file [$path, $ov], $dpath, sub {
                  fork_exec {
                     system "$CONVERT \Q$ov\E "
                            . "\\( \Q$path\E -trim -geometry 34x31\\> "
                            . "-gravity center -background transparent -extent 37x34 +gravity "
                            . "-geometry +22+6 \\) -composite "
                            . "-geometry $geom png:\Q$dpath\E~";
                     optipng "$dpath~";
                     rename "$dpath~", $dpath;
                  };
               };
            } elsif ($type eq "buildmaterial-sbox") {
               my $ov = "$PATH/mapbuilding/buildmaterial-sbox.png";
               make_file [$path, $ov], $dpath, sub {
                  fork_exec {
                     system "$CONVERT \Q$ov\E "
                            . "\\( \Q$path\E -trim -geometry 16x24\\> "
                            . "-gravity center -background transparent -extent 18x26 +gravity "
                            . "-geometry +26+16 \\) -composite "
                            . "-geometry $geom png:\Q$dpath\E~";
                     optipng "$dpath~";
                     rename "$dpath~", $dpath;
                  };
               };
            } else {
               warn "ERROR: facename $type:$face contains unsupported derivation type.\n";
            }

            aio_load $dpath, my $png;
            IO::AIO::aio_unlink $dpath unless $CACHE;
            commit_png "$dir/$type:$face", "$type:$face", $png, $T, 1, 1;
         }

         # split all bigfaces, but avoid smoothfaces (*_S)
         if (($w > $T || $h > $T) && $face !~ /_S\./) {
            # split
            my @tile;
            for my $x (0 .. (int $w / $T) - 1) {
               for my $y (0 .. (int $h / $T) - 1) {
                  my $file = "$path+$x+$y~";
                  aio_lstat $file;
                  push @tile, [$x, $y, $file, (stat _)[9]];
               }
            }

            my $mtime = (lstat $path)[9];
            my @todo = grep { $_->[3] < $mtime } @tile;
            if (@todo) {
               fork_exec {
                  open my $convert, "|-", $CONVERT,
                     "png:-",
                     (map {
                        (
                           "(",
                           "+clone",
                           -crop => (sprintf "%dx%d+%d+%d", $T, $T, $_->[0] * $T, $_->[1] * $T),
                           "+repage",
                           -quality => "00",
                           -write => "png32:$_->[2]~",
                           "+delete",
                           ")",
                        )
                     } @todo),
                     "null:";

                  binmode $convert;
                  print $convert $png;
                  close $convert;

                  # pass 2, optimise, and rename
                  for (@todo) {
                     optipng "$_->[2]~";
                     rename "$_->[2]~", $_->[2];
                  }
               };
            }

            for (@tile) {
               my ($x, $y, $file) = @$_;
               my $tile;

               if (0 > aio_load $file, $tile) {
                  die "$path: unable to read tile +$x+$y, aborting.\n";
               }
               IO::AIO::aio_unlink $file unless $CACHE;
               commit_png $stem, $x|$y ? "$face+$x+$y" : $face, $tile, $T, 1, 1;
            }
         } else {
            # use as-is (either small, use smooth)
            commit_png $stem, $face, $png, $T, $w / $T, $h / $T;
         }

         IO::AIO::aio_unlink $path if $delete;
      }
   }

   sub process_faceinfo {
      my ($dir, $file) = @_;
      my $path = "$dir/$file";

      my $data;
      if (0 > aio_load $path, $data) {
         warn "ERROR: $path: $!, skipping.\n";
         return;
      }

      for (split /\n/, $data) {
         chomp;
         my ($face, $visibility, $fg, $bg, $glyph) = split /\s+/, $_, 5;
         # bg not used except for text clients

         utf8::decode $glyph;
         $glyph =~ s/^\?(?=.)//; # remove "autoglyph" flag
         $glyph =~ s/^"(.+)"$/$1/; # allow for ""-style quoting

         $fg = "white" if $fg eq "none"; # lots of faces have no fg colour yet

         (my $fgi = $COLOR{$fg})
            // warn "ERROR: $path: $face specifies unknown foreground colour '$fg'.\n";
         (my $bgi = $COLOR{$bg})
            // warn "ERROR: $path: $face specifies unknown background colour '$bg'.\n";

         my $fi = $FACEINFO{$face} ||= { };
         $fi->{visibility} = $visibility * 1;
         $fi->{magicmap}   = $fgi; # foreground colour becomes magicmap

         $glyph .= " " if 2 > length $glyph; # TODO kanji
         die "glyph $face too long" if 2 < length $glyph;

         $fi->{glyph} = "";
         for (split //, $glyph) {
            utf8::encode $_;
            $fi->{glyph} .= (chr $fgi) . (chr $bgi) . $_;
         }
      }
   }

   sub process_arc {
      while (my ($dir, $file) = @{ $c_arc->get }) {
         my $arc;
         aio_load "$dir/$file", $arc; # simply pre-cache, as read_arch wants a file :/

         my $arc = read_arch "$dir/$file";
         for my $o (values %$arc) {
            push @ARC, $o;
            for (my $m = $o; $m; $m = $m->{more}) {
               $ARC{$m->{_name}} = $m;
            }

            $o->{editor_folder} ||= "\x00$dir"; # horrible kludge

            # find upper left corner :/
            # omg, this is sooo broken
            my ($dx, $dy);
            for (my $o = $o; $o; $o = $o->{more}) {
               $dx = $o->{x} if $o->{x} < $dx;
               $dy = $o->{y} if $o->{y} < $dy;
            }

            for (my $o = $o; $o; $o = $o->{more}) {
               my $x = $o->{x} - $dx;
               my $y = $o->{y} - $dy;

               my $ext = $x|$y ? "+$x+$y" : "";

               $o->{face} .= $ext unless /^blank.x11$|^empty.x11$/ || !$o->{face};

               my $anim = delete $o->{anim};

               if ($anim) {
                  # possibly add $ext to the animation name to avoid
                  # the need to specify archnames for all parts
                  # of a multipart archetype.
                  $o->{animation} = "$o->{_name}";
                  my $facings = 1;
                  my @frames;

                  for (@$anim) {
                     if (/^facings\s+(\d+)/) {
                        $facings = $1*1;
                     } elsif (/^blank.x11$|^empty.x11$/) {
                        push @frames, $_;
                     } else {
                        push @frames, "$_$ext";
                     }
                  }

                  $ANIMINFO{$o->{animation}} = {
                     facings => $facings,
                     frames  => \@frames,
                  };
               }

               for ($o->{face} || (), @{$anim || []}) {
                  next if /^facings\s/;

                  my $face = $_;
                  $face =~ s/\+\d+\+\d+$//; # remove tile offset coordinates

                  # handle derived faces
                  if ($face =~ /^([^:]+):(.*)/) {
                     my ($type, $base) = ($1, $2);
                     push @{ $FACEINFO{$base}{derive} }, [$dir, $type];
                  }

                  $FACEINFO{$face}{arc} = $o;

                  next if $face =~ /^blank.x11$|^empty.x11$/;
               }

               if (my $smooth = delete $o->{smoothface}) {
                  my %kv = split /\s+/, $smooth;
                  my $level = $o->{smoothlevel}; #TODO: delete from $o if !gcfclient-support
                  while (my ($face, $smooth) = each %kv) {
                     $FACEINFO{$smooth}{arc}       = $o;

                     $FACEINFO{$face}{smooth}      = $smooth;
                     $FACEINFO{$face}{smoothlevel} = $level;
                  }
               }
            }
         }
      }
   }

   sub process_trs {
      my ($dir, $file) = @_;
      my $path = "$dir/$file";

      my $trs;
      if (0 > aio_load $path, $trs) {
         warn "ERROR: $path: $!, skipping.\n";
         return;
      }

      $TRS .= $trs;
   }

   my %FILECACHE;

   sub load_cached($;$) {
      unless (exists $FILECACHE{$_[0]}) {
         my $data;
         if (0 < aio_load $_[0], $data) {
            if ($_[1]) {
               $data = eval { $_[1]->($data) };
               warn "ERROR: $_[0]: $@" if $@;
            }
         }

         $FILECACHE{$_[0]} = $data;
      }

      $FILECACHE{$_[0]}
   }

   # convert an image and a palette to some indexed 2d-matrix structure
   sub process_plt {
      my ($base, $plt) = @_;

      my ($w, $h) = imgsize "$base.png";

      $w * $h
         or die "$base.png: unable to identify correct size\n";

      my @plt;
      my %map;

      for (split /\n/, $plt) {
         next unless /\S/;
         next if /^\s*#/;

         /^([0-9a-fA-F]{3,6})\s*(.*?)\s*$/
            or die "unparseable palette entry for $base.plt: $_";

         my ($rgb, $name) = ($1, $2);

         $rgb =~ s/^(.)(.)(.)$/$1$1$2$2$3$3/;

         $map{pack "H*", $rgb} = chr @plt;
         push @plt, $name;
      }

      make_file ["$base.plt", "$base.png"], "$base.tbl~", sub {
         warn "building $base\n" if $VERBOSE >= 3;

         fork_exec {
            open my $png, "-|", $CONVERT, qw(-depth 8 --), "$base.png", "rgb:"
               or die "$base.png: $!";

            local $/;
            $png = <$png>;

            $w * $h * 3 == length $png
               or die "$base.png: failed to read enough data from file\n";

            $png =~ s/(...)/$map{$1}/ge;

            $w * $h == length $png
               or die "$base.png: failed to map all data - wrong palette?\n";

            {
               open my $fh, ">:raw", "$base.tbl~~"
                  or die "$base.tbl~~: $!";
               syswrite $fh, $png;
            }

            rename "$base.tbl~~", "$base.tbl~";
         };
      };

      0 <= aio_load "$base.tbl~", my $tbl
         or die "$base.tbl~: $!";

      IO::AIO::aio_unlink "$base.tbl~" unless $CACHE;

      Compress::LZF::compress nfreeze {
         w   => $w,
         h   => $h,
         plt => \@plt,
         tbl => $tbl,
      }
   }

   sub process_res {
      my ($dir, $file, $type) = @_;

      0 <= aio_load "$dir/$file", my $data
         or die "$dir/$file: $!";

      my $meta = load_cached "$dir/meta", sub { JSON::XS->new->utf8->relaxed->decode (shift) };

      utf8::decode $dir;
      utf8::decode $file;

      # a meta file for resources is now mandatory
      unless (exists $meta->{$file}) {
         warn "skipping $dir/$file\n" if $VERBOSE >= 3;
         return;
      }

      $meta = {
         %{ $meta->{""   } || {} },
         %{ $meta->{$file} || {} },
      };

      if (exists $meta->{license} && $meta->{license} =~ s/^#//) { # exists == avoid autovivification
         # all these are acceptable for deliantra - if not specified, agpl 3+ is the default.
         $meta->{license} = ({
            "pd"              => "Public Domain",
            "gpl"             => "GNU General Public License, version 3.0 or any later",
            "2bsd"            => "2-clause BSD/MIT style license",
            "3bsd"            => "3-clause BSD style license",
            "cc/by/2.0"       => "Licensed under Creative Commons Attribution 2.0 http://creativecommons.org/licenses/by/2.0/",
            "cc/by/2.1"       => "Licensed under Creative Commons Attribution 2.1 http://creativecommons.org/licenses/by/2.1/",
            "cc/by/2.5"       => "Licensed under Creative Commons Attribution 2.5 http://creativecommons.org/licenses/by/2.5/",
            "cc/by/3.0"       => "Licensed under Creative Commons Attribution 3.0 http://creativecommons.org/licenses/by/3.0/",
         })->{$meta->{license}}
           || warn "ERROR: $dir/$file: license tag '$meta->{license}' not found.";
      }

      if (!exists $meta->{author} && $meta->{source} =~ m%^http://www.jamendo.com/en/artist/(.*)$%) {
         ($meta->{author} = $1) =~ s/_/ /g;
      }

      $file =~ s/\.(res|ogg|wav|jpg|png)$// unless $meta->{keep_suffix};

      if ($file =~ s/\.plt$//) {
         $data = process_plt "$dir/$file", $data;
      } elsif (my $filter = delete $meta->{cfutil_filter}) {
         if ($filter eq "yaml2json") {
            $data = JSON::XS::encode_json YAML::XS::Load $data;
         } elsif ($filter eq "json2json") {
            $data = JSON::XS::encode_json JSON::XS->relaxed->utf8->decode ($data);
         } elsif ($filter eq "perl2json") {
            $data = eval $data; die if $@;
            $data = JSON::XS::encode_json $data;
         } else {
            warn "ERROR: $dir/$file: unknown filter $filter, skipping\n";
         }
      }

      substr $dir, 0, 1 + length $PATH, "";

      my %info = (
         type   => (exists $meta->{type} ? delete $meta->{type} : $type),
         data   => $data,
         %$meta ? (meta => $meta) : (),
      );

      $RESOURCE{"$dir/$file"} = \%info;
   }

   sub process_any {
      while (my ($func, @args) = @{ $c_any->get }) {
         $func->(@args);
      }
   }

   sub find_files;
   sub find_files {
      my ($path) = @_;

      my $grp = IO::AIO::aio_group;

      my $scandir; $scandir = sub {
         my ($path) = @_;

         IO::AIO::aioreq_pri 4;
         add $grp IO::AIO::aio_scandir $path, 4, sub {
            my ($dirs, $nondirs) = @_;

            $scandir->("$path/$_")
               for grep $_ !~ /^(?:CVS|dev|\..*)$/, @$dirs;

            my $dir = $path;
            substr $dir, 0, 1 + length $PATH, "";

            for my $file (@$nondirs) {
               if ($dir =~ /^music(?:\/|$)/) {
                  $c_any->put ([\&process_res, $path, $file, 3]) # FT_MUSIC
                     if $file =~ /\.(ogg)$/;

               } elsif ($dir =~ /^sound(?:\/|$)/) {
                  $c_any->put ([\&process_res, $path, $file, 5]) # FT_SOUND
                     if $file =~ /\.(wav|ogg)$/;

               } elsif ($dir =~ /^res(?:\/|$)/) {
                  if ($file =~ /\.(jpg|png)$/) {
                     $c_any->put ([\&process_res, $path, $file, 0]) # FT_FACE
                  } elsif ($file =~ /\.(res)$/) {
                     $c_any->put ([\&process_res, $path, $file, 6]) # FT_RSRC
                  } else {
                     $c_any->put ([\&process_res, $path, $file, 6]); # was type undef, but now meta files are mandatory, so any mentioned file surely has a purpose
                  }

               } elsif ($file =~ /\.png$/) {
                  $PNG{$file} = $path;

               } elsif ($file =~ /\.faceinfo$/) {
                  $c_any->put ([\&process_faceinfo, $path, $file]);

               } elsif ($file =~ /\.trs$/) {
                  $c_any->put ([\&process_trs, $path, $file]);

               } elsif ($file =~ /\.arc$/) {
                  $c_arc->put ([$path, $file]);

               } else {
                  warn "ignoring $path/$file\n" if $VERBOSE >= 3;
               }
            }
         };
      };

      $scandir->($path);
      aio_wait $grp;
   }

   sub generate_plurals {
#      use Lingua::EN::Inflect ();
#      Lingua::EN::Inflect::classical;
#      Lingua::EN::Inflect::def_noun '(.*)staff' => '$1staves'; # policy
#      Lingua::EN::Inflect::def_noun '(.*)boots' => '$1boots'; # hack
# 
#      for my $a (@ARC) {
#         my $name = $a->{name} || $a->{_name};
# 
#         next unless $a->{name_pl};
#         next if $a->{invisible};
#         next if $a->{is_floor};
#         next if $a->{no_pick};
# 
#         my $test = Lingua::EN::Inflect::PL_N_eq $name, Lingua::EN::Inflect::PL $name;
#         my $pl = $test =~ /^(?:eq|p:.)$/
#                  ? $name
#                  : Lingua::EN::Inflect::PL $name;
# 
#         if ($pl ne $a->{name_pl}) {
#            warn "$a->{_name}: plural differs, $pl vs $a->{name_pl}\n";
#         }
#      }
   }

   sub inst_arch($) {
      my (undef, $path) = @_;

      $PATH = $path;

      print "\n",
            "Installing '$path' to '$DATADIR'\n",
            "\n",
            "This can take a long time if you run this\n",
            "for the first time or do not use --cache.\n",
            "\n",
            "Unless you run verbosely, all following warning\n",
            "or error messages indicate serious problems.\n",
            "\n";

      if (!-d "$path/treasures") {
         warn "FATAL: '$path' does not look like an arch directory ('treasures' directory is missing).\n";
         exit 1 unless $FORCE;
      }

      print "starting file scan, arc, any processing...\n" if $VERBOSE;

      my @a_arc = map +(async \&process_arc), 1..2;
      my @a_any = map +(async \&process_any), 1..4;

      find_files $path;

      $c_arc->shutdown;
      $c_any->shutdown;

      $_->join for @a_arc; # need to parse all archetypes before png processing
      print "finished arc processing...\n" if $VERBOSE;

      # fill png queue
      for (keys %FACEINFO) {
         # we only expect source pngs to exist for non-derived
         # faces, i.e. no split tiles (+x+x) and no face derivates
         # (xxx:)
         /^[^:]+\....$/
            or next;

         my $seen;

         if (exists $PNG{"$_.32x32.png"}) {
            push @c_png, ["$PNG{\"$_.32x32.png\"}/$_.32x32.png"];
            $seen = 1;
         }
         if (exists $PNG{"$_.64x64.png"}) {
            push @c_png, ["$PNG{\"$_.64x64.png\"}/$_.64x64.png"];
            $seen = 1;
         }

         warn "ERROR: $_: face not found, expect problems.\n"
            unless $seen;
      }

      print "starting png processing...\n" if $VERBOSE;

      # eight png crunchers work fine for my 4x smp machine
      my @a_png = map +(async \&process_png), 1..8;

      $_->join for @a_any;
      print "finished any processing...\n" if $VERBOSE;

      $_->join for @a_png;
      print "finished png processing...\n" if $VERBOSE;

      print "scanning done, processing results...\n" if $VERBOSE;
      {
         # remove path prefix from editor_folder
         $_->{editor_folder} =~ /^\x00/
            and substr $_->{editor_folder}, 0, 2 + length $path, ""
               for values %ARC;

         print "resolving inheritance tree...\n" if $VERBOSE;
         # resolve inherit
         while () {
            my $progress;
            my $loop;

            for my $o (values %ARC) {
               for my $other (split /,/, $o->{inherit}) {
                  if (my $s = $ARC{$other}) {
                     if ($s->{inherit}) {
                        $loop = $s;
                     } else {
                        delete $o->{inherit};
                        my %s = %$s;
                        delete @s{qw(_name more name name_pl)};
                        %$o = ( %s, %$o );
                        ++$progress;
                     }
                  } else {
                     warn "ERROR: archetype '$o->{_name}' tries to inherit from undefined archetype '$other', skipping.\n";
                     delete $ARC{$o->{_name}};
                  }
               }
            }

            unless ($progress) {
               die "inheritance loop detected starting at archetype '$loop->{_name}', aborting.\n"
                  if $loop;

               last;
            }
         }

         # remove base classes (by naming scheme, should use something like "baseclass xxx" to inherit
         @ARC = grep $_->{_name} !~ /^(?:type|class)_/, @ARC;

         # fix up archetypes without names, where the archanme doesn't work at all
         for (@ARC) {
            if (!exists $_->{name} and $_->{_name} =~ /_/) {
               for ($_->{name} = $_->{_name}) {
                  s/(?:_\d+)+$//;
                  s/_[nesw]+$//;
                  y/_/ /;
               }
            }
         }

         #print "generating plurals...\n" if $VERBOSE;
         #generate_plurals;

         printf "writing %d archetypes...\n", scalar @ARC if $VERBOSE;
         open my $fh, ">:utf8", "$DATADIR/archetypes~"
            or die "$DATADIR/archetypes~: $!";
         print $fh Deliantra::archlist_to_string [sort { $a->{_name} cmp $b->{_name} } @ARC];
      }

      {
         printf "writing treasures (%d octets)...\n", length $TRS if $VERBOSE;
         open my $fh, ">:utf8", "$DATADIR/treasures~"
            or die "$DATADIR/treasures~: $!";
         print $fh $TRS;
      }

      {
         print "processing facedata...\n" if $VERBOSE;
         
         for my $k (sort keys %FACEINFO) {
            my $v = $FACEINFO{$k};

            length $v->{data32} or warn "ERROR: face '$k' has no png32 - this will not work.\n";
            length $v->{data64} or warn "ERROR: face '$k' has no png64 - this will not work.\n";

            make_hash $k, $v->{data32}, $v->{hash32};
            make_hash $k, $v->{data64}, $v->{hash64};

            #length $v->{data32} <= 10000 or warn "WARNING: face '$k' has face32 larger than 10000 bytes, will not work with crossfire client.\n";
            #length $v->{data64} <= 10000 or warn "WARNING: face '$k' has face64 larger than 10000 bytes.\n";

            $v->{glyph}      // warn "ERROR: face '$k' has no glyph - missing faceinfo entry?\n";
            $v->{visibility} // warn "ERROR: face '$k' has no visibility info - missing faceinfo entry?\n";
            $v->{magicmap}   // warn "ERROR: face '$k' has no foreground colour - missing faceinfo entry?\n";

            if (0 && $v->{w} == 1 && $v->{h} == 1) { # texture catalogs
               my $id = @TC;
               my $n  = @{ $TC[-1] };
               my $x  =     $n % TC_W;
               my $y  = int $n / TC_W;

               push @{ $TC[-1] }, [$v, $x, $y];

               $v->{tc} = [$id, $x, $y];

               push @TC, [] if $n == TC_W * TC_H - 1; # start new texture if full
            }

            delete @$v{qw(w h arc stem derive)}; # not used by the server
         }

if (0) { #d# texture catalogs
         print "creating texture catalogs...\n" if $VERBOSE;

         for my $id (0 .. $#TC) {
            my $tc = $TC[$id];

            my $cmd = "$CONVERT -depth 8 -size " . (TC_W * 64) . "x" . (TC_H * 64) . " xc:transparent";
            my $idx = "a";

            for (@$tc) {
               my $path = "$TMPDIR/tc" . $idx++;

               open my $fh, ">:perlio", $path
                  or die "$path: $!";
               syswrite $fh, $_->[0]{data64};

               my $x = $_->[1] * 64;
               my $y = $_->[2] * 64;

               $cmd .= " png:\Q$path\E -geometry +$x+$y -composite";
            }

            system "$cmd png:\Q$TMPDIR/tc$id\E";
            optipng "$TMPDIR/tc$id";
         }
}

         print "processing resources...\n" if $VERBOSE;
         my $enc = JSON::XS->new->utf8->canonical->relaxed;
         for my $k (sort keys %RESOURCE) {
            my $v = $RESOURCE{$k};

            if ($v->{meta} && $v->{meta}{datadir}) {
               delete $RESOURCE{$k};

               $k =~ s/^res\/// or die "$k: datadir files must be in res/";

               printf "writing $k (%d octets)...\n", length $v->{data} if $VERBOSE;
               open my $fh, ">:raw", "$DATADIR/$k~"
                  or die "$DATADIR/$k~: $!";
               syswrite $fh, $v->{data};
               push @COMMIT, $k;

            } else {
               if ($v->{type} & 1) {
                  # prepend meta info

                  my $meta = $enc->encode ({
                     name => $k,
                     %{ $v->{meta} || {} },
                  });

                  $v->{data} = pack "(w/a*)*", $meta, $v->{data};
               }

               make_hash $k, $v->{data}, $v->{hash}, 6; # 6 for the benefit of existing clients
            }
         }

         printf "writing facedata...\n" if $VERBOSE;

         {
            open my $fh, ">:perlio", "$DATADIR/facedata~"
               or die "$DATADIR/facedata~: $!";

            print $fh "FACEDATA";
            my $fofs = 8;

            my $put = sub {
               my $len = length $_[0];
               my $ofs = $fofs;

               print $fh $_[0];
               $fofs += $len;

               ($len, $ofs)
            };

            for (values %FACEINFO) {
               ($_->{size32}, $_->{fofs32}) = $put->(delete $_->{data32});
               ($_->{size64}, $_->{fofs64}) = $put->(delete $_->{data64});
            }

            for (values %RESOURCE) {
               ($_->{size}, $_->{fofs}) = $put->(delete $_->{data});
            }

            close $fh;
         }

         printf "writing faceinfo (%d faces, %d anims, %d resources)...\n",
            scalar keys %FACEINFO,
            scalar keys %ANIMINFO,
            scalar keys %RESOURCE
               if $VERBOSE;

         open my $fh, ">:perlio", "$DATADIR/faceinfo~"
            or die "$DATADIR/faceinfo~: $!";

         print $fh nfreeze {
            version => 2,
            faceinfo => \%FACEINFO,
            animinfo => \%ANIMINFO,
            resource => \%RESOURCE,
         };
      }

      print "committing files...\n" if $VERBOSE;

      for (qw(archetypes faceinfo facedata treasures), @COMMIT) {
         chmod 0644, "$DATADIR/$_~";
         rename "$DATADIR/$_~", "$DATADIR/$_"
            or die "$DATADIR/$_: $!";
      }

      print "archetype data installed successfully.\n";
   }
}

Getopt::Long::Configure ("bundling", "no_ignore_case");
GetOptions (
   "verbose|v:+"    => \$VERBOSE,
   "cache"          => \$CACHE,
   "quiet|q"        => sub { $VERBOSE = 0 },
   "force"          => sub { $FORCE = 1 },
   "install-arch=s" => \&inst_arch,
   "install-maps=s" => \&inst_maps,
   "print-statedir" => sub { print "@pkgstatedir@\n" },
   "print-datadir"  => sub { print "$DATADIR\n" },
   "print-confdir"  => sub { print "@pkgconfdir@\n" },
   "print-libdir"   => sub { print "@libdir@/@PACKAGE@\n" },
   "print-bindir"   => sub { print "@bindir@/@PACKAGE@\n" },
) or usage;

