/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

/* Send bug reports to Raphael Quinet (quinet@montefiore.ulg.ac.be) */

/**
 * \file
 * Sound-related functions.
 *
 * \date 2003-12-02
 */

#include <global.h>
#include <sproto.h>
#include <sounds.h>

#include <unordered_map>

// the hashtable
typedef std::unordered_map
  <
    const char *,
    faceidx,
    str_hash,
    str_equal,
    slice_allocator< std::pair<const char *const, faceidx> >
  > HT;

static HT ht;

faceidx
sound_find (const char *str)
{
  auto (i, ht.find (str));

  if (i != ht.end ())
    return i->second;

  if (strncmp (str, "sound/", sizeof ("sound/") - 1))
    str = format ("sound/%s", str);

  return face_find (str);
}

void
sound_set (const char *str, faceidx face)
{
  auto (i, ht.find (str));

  if (i != ht.end ())
    i->second = face;
  else
    ht.insert (std::make_pair (strdup (str), face));
}

//TODO: remove
// for gcfclient-compatibility, to vanish at some point
faceidx old_sound_index [SOUND_CAST_SPELL_0];

/*
 * Plays a sound for specified player only
 */
void
client::play_sound (faceidx sound, int dx, int dy)
{
  if (!sound)
    return;

  uint8 vol = 255 - idistance (dx, dy) * 255 / MAX_SOUND_DISTANCE;

  // cut off volume here
  if (vol <= 0)
    return;

  if (fx_want [FT_SOUND])
    {
      // cfplus
      send_face (sound);
      flush_fx ();

      packet sl ("sc");

      uint8 *len = sl.cur;

      sl << uint8 (0) // group length, decoded as BER in clients
         << uint8 (0) // type == one-time effect
         << ber32 (sound)
         << sint8 (dx)
         << sint8 (dy)
         << uint8 (vol); // 0 == silent, 255 max

      *len = sl.cur - len - 1; // patch in group length

      send_packet (sl);
    }
  else if (this->sound)
    {
      //TODO: remove, or make bearable
      // gcfclient compatibility

      int gcfclient_sound;
      for (gcfclient_sound = SOUND_CAST_SPELL_0; gcfclient_sound--; )
        if (old_sound_index [gcfclient_sound] == sound)
          {
            packet sl ("sound");

            sl << uint8  (dx)
               << uint8  (dy)
               << uint16 (gcfclient_sound)
               << uint8  (SOUND_NORMAL);

            send_packet (sl);

            break;
          }
    }
}

