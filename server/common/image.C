/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#include <global.h>
#include <stdio.h>

#include "face.h"
#include "crc.h"

faceidx blank_face, empty_face, magicmouth_face;

facehash_t facehash;
std::vector<faceinfo> faces;

static std::vector<faceidx> faces_freelist;

faceidx face_alloc ()
{
  faceidx idx;

  if (!faces_freelist.empty ())
    {
      idx = faces_freelist.back ();
      faces_freelist.pop_back ();
    }
  else
    {
      idx = faces.size ();

      if (!idx) // skip index 0
        idx = 1;

      faces.resize (idx + 1);
    }

  return idx;
}

void
faceinfo::unref ()
{
  if (--refcnt)
    return;

  refcnt = 1;

}

faceidx
face_find (const char *name, faceidx defidx)
{
  if (!name)
    return defidx;

  facehash_t::iterator i = facehash.find (name);

  return i == facehash.end ()
         ? defidx : i->second;
}

faceinfo *
face_info (faceidx idx)
{
  assert (0 < (faceidx)-1); // faceidx must be unsigned

  if (idx >= faces.size ())
    return 0;

  return &faces [idx];
}

facedata *
faceinfo::data (int faceset) const
{
  if (!face [faceset].chksum_len)
    faceset = 0;

  return (facedata *)(face + faceset);
}

facedata *
face_data (faceidx idx, int faceset)
{
  if (faceinfo *f = face_info (idx))
    return f->data (faceset);

  return 0;
}

