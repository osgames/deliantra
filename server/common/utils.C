/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

/*
 * General convenience functions for deliantra.
 */

#include <cstdlib>
#include <sys/types.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>
#include <signal.h>

#include <global.h>
#include <material.h>
#include <object.h>

#include <sys/time.h>
#include <sys/resource.h>

#include <glib.h>

refcnt_base::refcnt_t refcnt_dummy;
ssize_t slice_alloc;

#if !GCC_VERSION(3,4)
int least_significant_bit (uint32_t x)
{
  x &= -x; // this isolates the lowest bit

  int r = 0;

  if (x & 0xaaaaaaaa) r +=  1;
  if (x & 0xcccccccc) r +=  2;
  if (x & 0xf0f0f0f0) r +=  4;
  if (x & 0xff00ff00) r +=  8;
  if (x & 0xffff0000) r += 16;

  return r;
}
#endif

/******************************************************************************/

/* Checks a player-provided string which will become the msg property of
 * an object for dangerous input.
 */
bool
msg_is_safe (const char *msg)
{
  bool safe = true;

  /* Trying to cheat by getting data into the object */
  if (!strncmp (msg, "endmsg", sizeof ("endmsg") - 1)
      || strstr (msg, "\nendmsg"))
    safe = false;

  /* Trying to make the object talk, and potentially access arbitrary code */
  if (object::msg_has_dialogue (msg))
    safe = false;

  return safe;
}

/////////////////////////////////////////////////////////////////////////////

void
fork_abort (const char *msg)
{
  if (!fork ())
    {
      signal (SIGINT , SIG_IGN);
      signal (SIGTERM, SIG_IGN);
      signal (SIGABRT, SIG_IGN);

      signal (SIGSEGV, SIG_DFL);
      signal (SIGFPE , SIG_DFL);
#ifdef SIGBUS
      signal (SIGBUS , SIG_DFL);
#endif
      signal (SIGILL , SIG_DFL);
      signal (SIGTRAP, SIG_DFL);

      // try to put corefiles into a subdirectory, if existing, to allow
      // an administrator to reduce the I/O load.
      chdir ("cores");

      // try to detach us from as many external dependencies as possible
      // as coredumping can take time by closing all fd's.
      {
        struct rlimit lim;

        if (getrlimit (RLIMIT_NOFILE, &lim))
          lim.rlim_cur = 1024;

        for (int i = 0; i < lim.rlim_cur; ++i)
          close (i);
      }

      {
        sigset_t empty;
        sigemptyset (&empty);
        sigprocmask (SIG_SETMASK, &empty, 0);
      }

      // try to coredump with SIGTRAP
      kill (getpid (), SIGTRAP);
      abort ();
    }

  LOG (llevError, "fork abort: %s\n", msg);
}

void *
salloc_ (int n) throw (std::bad_alloc)
{
  void *ptr = g_slice_alloc (n);

  if (!ptr)
    throw std::bad_alloc ();

  slice_alloc += n;
  return ptr;
}

void *
salloc_ (int n, void *src) throw (std::bad_alloc)
{
  void *ptr = salloc_ (n);

  if (src)
    memcpy (ptr, src, n);
  else
    memset (ptr, 0, n);

  return ptr;
}

/******************************************************************************/

#if DEBUG_SALLOC

#define MAGIC 0xa1b2c35543deadLL

void *
g_slice_alloc (unsigned long size)
{
  unsigned long *p = (unsigned long *) (g_slice_alloc)(size + sizeof (unsigned long));
  *p++ = size ^ MAGIC;
  //fprintf (stderr, "g_slice_alloc %ld %p\n", size, p);//D
  return (void *)p;
}

void *
g_slice_alloc0 (unsigned long size)
{
  return memset (g_slice_alloc (size), 0, size);
}

void
g_slice_free1 (unsigned long size, void *ptr)
{
  //fprintf (stderr, "g_slice_free %ld %p\n", size, ptr);//D
  if (expect_true (ptr))
    {
      unsigned long *p = (unsigned long *)ptr;
      unsigned long s = *--p ^ MAGIC;

      if (size != (unsigned long)(*p ^ MAGIC))
        {
          LOG (logBacktrace | llevError, "slice free size (%lx) doesn't match alloc size (%lx)\n", size, s);
          abort ();
        }

      *p = MAGIC;

      (g_slice_free1)(s + sizeof (unsigned long), p);
    }
}

#endif

/******************************************************************************/

refcnt_buf::refcnt_buf (size_t size)
{
  static uint32_t empty_buf [2] = { 0, 1 }; // 2 == never deallocated
  data = (char *)empty_buf + overhead;
  assert (overhead == sizeof (empty_buf));
  inc ();
}

refcnt_buf::refcnt_buf (void *data, size_t size)
{
  _alloc (size);
  memcpy (this->data, data, size);
}

refcnt_buf::~refcnt_buf ()
{
  dec ();
}

void
refcnt_buf::_dealloc ()
{
  sfree<char> (data - overhead, size () + overhead);
}

refcnt_buf &
refcnt_buf::operator =(const refcnt_buf &src)
{
  dec ();
  data = src.data;
  inc ();
  return *this;
}

/******************************************************************************/

int
assign (char *dst, const char *src, int maxsize)
{
  if (!src)
    src = "";

  int len = strlen (src);

  if (len >= maxsize)
    {
      if (maxsize <= 4)
        {
          memset (dst, '.', maxsize - 2);
          dst [maxsize - 1] = 0;
        }
      else
        {
          memcpy (dst, src, maxsize - 4);
          memcpy (dst + maxsize - 4, "...", 4);
        }

      len = maxsize;
    }
  else
    memcpy (dst, src, ++len);

  return len;
}

char *
vformat (const char *format, va_list ap)
{
  static dynbuf_text bufs[8];
  static int bufidx;

  dynbuf_text &buf = bufs [++bufidx & 7];

  buf.clear ();
  buf.vprintf (format, ap);
  return buf;
}

char *
format (const char *format, ...)
{
  va_list ap;
  va_start (ap, format);
  char *buf = vformat (format, ap);
  va_end (ap);

  return buf;
}

tstamp
now ()
{
  struct timeval tv;

  gettimeofday (&tv, 0);
  return tstamp (tv.tv_sec) + tstamp (tv.tv_usec) * tstamp (1e-6);
}

int
similar_direction (int a, int b)
{
  if (!a || !b)
    return 0;

  int diff = (b - a) & 7;
  return diff <= 1 || diff >= 7;
}

/* crc32 0xdebb20e3 table and supplementary functions.  */
extern const uint32_t crc_32_tab[256] =
{
    0x00000000UL, 0x77073096UL, 0xee0e612cUL, 0x990951baUL, 0x076dc419UL,
    0x706af48fUL, 0xe963a535UL, 0x9e6495a3UL, 0x0edb8832UL, 0x79dcb8a4UL,
    0xe0d5e91eUL, 0x97d2d988UL, 0x09b64c2bUL, 0x7eb17cbdUL, 0xe7b82d07UL,
    0x90bf1d91UL, 0x1db71064UL, 0x6ab020f2UL, 0xf3b97148UL, 0x84be41deUL,
    0x1adad47dUL, 0x6ddde4ebUL, 0xf4d4b551UL, 0x83d385c7UL, 0x136c9856UL,
    0x646ba8c0UL, 0xfd62f97aUL, 0x8a65c9ecUL, 0x14015c4fUL, 0x63066cd9UL,
    0xfa0f3d63UL, 0x8d080df5UL, 0x3b6e20c8UL, 0x4c69105eUL, 0xd56041e4UL,
    0xa2677172UL, 0x3c03e4d1UL, 0x4b04d447UL, 0xd20d85fdUL, 0xa50ab56bUL,
    0x35b5a8faUL, 0x42b2986cUL, 0xdbbbc9d6UL, 0xacbcf940UL, 0x32d86ce3UL,
    0x45df5c75UL, 0xdcd60dcfUL, 0xabd13d59UL, 0x26d930acUL, 0x51de003aUL,
    0xc8d75180UL, 0xbfd06116UL, 0x21b4f4b5UL, 0x56b3c423UL, 0xcfba9599UL,
    0xb8bda50fUL, 0x2802b89eUL, 0x5f058808UL, 0xc60cd9b2UL, 0xb10be924UL,
    0x2f6f7c87UL, 0x58684c11UL, 0xc1611dabUL, 0xb6662d3dUL, 0x76dc4190UL,
    0x01db7106UL, 0x98d220bcUL, 0xefd5102aUL, 0x71b18589UL, 0x06b6b51fUL,
    0x9fbfe4a5UL, 0xe8b8d433UL, 0x7807c9a2UL, 0x0f00f934UL, 0x9609a88eUL,
    0xe10e9818UL, 0x7f6a0dbbUL, 0x086d3d2dUL, 0x91646c97UL, 0xe6635c01UL,
    0x6b6b51f4UL, 0x1c6c6162UL, 0x856530d8UL, 0xf262004eUL, 0x6c0695edUL,
    0x1b01a57bUL, 0x8208f4c1UL, 0xf50fc457UL, 0x65b0d9c6UL, 0x12b7e950UL,
    0x8bbeb8eaUL, 0xfcb9887cUL, 0x62dd1ddfUL, 0x15da2d49UL, 0x8cd37cf3UL,
    0xfbd44c65UL, 0x4db26158UL, 0x3ab551ceUL, 0xa3bc0074UL, 0xd4bb30e2UL,
    0x4adfa541UL, 0x3dd895d7UL, 0xa4d1c46dUL, 0xd3d6f4fbUL, 0x4369e96aUL,
    0x346ed9fcUL, 0xad678846UL, 0xda60b8d0UL, 0x44042d73UL, 0x33031de5UL,
    0xaa0a4c5fUL, 0xdd0d7cc9UL, 0x5005713cUL, 0x270241aaUL, 0xbe0b1010UL,
    0xc90c2086UL, 0x5768b525UL, 0x206f85b3UL, 0xb966d409UL, 0xce61e49fUL,
    0x5edef90eUL, 0x29d9c998UL, 0xb0d09822UL, 0xc7d7a8b4UL, 0x59b33d17UL,
    0x2eb40d81UL, 0xb7bd5c3bUL, 0xc0ba6cadUL, 0xedb88320UL, 0x9abfb3b6UL,
    0x03b6e20cUL, 0x74b1d29aUL, 0xead54739UL, 0x9dd277afUL, 0x04db2615UL,
    0x73dc1683UL, 0xe3630b12UL, 0x94643b84UL, 0x0d6d6a3eUL, 0x7a6a5aa8UL,
    0xe40ecf0bUL, 0x9309ff9dUL, 0x0a00ae27UL, 0x7d079eb1UL, 0xf00f9344UL,
    0x8708a3d2UL, 0x1e01f268UL, 0x6906c2feUL, 0xf762575dUL, 0x806567cbUL,
    0x196c3671UL, 0x6e6b06e7UL, 0xfed41b76UL, 0x89d32be0UL, 0x10da7a5aUL,
    0x67dd4accUL, 0xf9b9df6fUL, 0x8ebeeff9UL, 0x17b7be43UL, 0x60b08ed5UL,
    0xd6d6a3e8UL, 0xa1d1937eUL, 0x38d8c2c4UL, 0x4fdff252UL, 0xd1bb67f1UL,
    0xa6bc5767UL, 0x3fb506ddUL, 0x48b2364bUL, 0xd80d2bdaUL, 0xaf0a1b4cUL,
    0x36034af6UL, 0x41047a60UL, 0xdf60efc3UL, 0xa867df55UL, 0x316e8eefUL,
    0x4669be79UL, 0xcb61b38cUL, 0xbc66831aUL, 0x256fd2a0UL, 0x5268e236UL,
    0xcc0c7795UL, 0xbb0b4703UL, 0x220216b9UL, 0x5505262fUL, 0xc5ba3bbeUL,
    0xb2bd0b28UL, 0x2bb45a92UL, 0x5cb36a04UL, 0xc2d7ffa7UL, 0xb5d0cf31UL,
    0x2cd99e8bUL, 0x5bdeae1dUL, 0x9b64c2b0UL, 0xec63f226UL, 0x756aa39cUL,
    0x026d930aUL, 0x9c0906a9UL, 0xeb0e363fUL, 0x72076785UL, 0x05005713UL,
    0x95bf4a82UL, 0xe2b87a14UL, 0x7bb12baeUL, 0x0cb61b38UL, 0x92d28e9bUL,
    0xe5d5be0dUL, 0x7cdcefb7UL, 0x0bdbdf21UL, 0x86d3d2d4UL, 0xf1d4e242UL,
    0x68ddb3f8UL, 0x1fda836eUL, 0x81be16cdUL, 0xf6b9265bUL, 0x6fb077e1UL,
    0x18b74777UL, 0x88085ae6UL, 0xff0f6a70UL, 0x66063bcaUL, 0x11010b5cUL,
    0x8f659effUL, 0xf862ae69UL, 0x616bffd3UL, 0x166ccf45UL, 0xa00ae278UL,
    0xd70dd2eeUL, 0x4e048354UL, 0x3903b3c2UL, 0xa7672661UL, 0xd06016f7UL,
    0x4969474dUL, 0x3e6e77dbUL, 0xaed16a4aUL, 0xd9d65adcUL, 0x40df0b66UL,
    0x37d83bf0UL, 0xa9bcae53UL, 0xdebb9ec5UL, 0x47b2cf7fUL, 0x30b5ffe9UL,
    0xbdbdf21cUL, 0xcabac28aUL, 0x53b39330UL, 0x24b4a3a6UL, 0xbad03605UL,
    0xcdd70693UL, 0x54de5729UL, 0x23d967bfUL, 0xb3667a2eUL, 0xc4614ab8UL,
    0x5d681b02UL, 0x2a6f2b94UL, 0xb40bbe37UL, 0xc30c8ea1UL, 0x5a05df1bUL,
    0x2d02ef8dL
};

void
thread::start (void *(*start_routine)(void *), void *arg)
{
  pthread_attr_t attr;

  pthread_attr_init (&attr);
  pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_DETACHED);
  pthread_attr_setstacksize (&attr, PTHREAD_STACK_MIN < sizeof (long) * 4096
                                    ? sizeof (long) * 4096 : PTHREAD_STACK_MIN);
#ifdef PTHREAD_SCOPE_PROCESS
  pthread_attr_setscope (&attr, PTHREAD_SCOPE_PROCESS);
#endif

  sigset_t fullsigset, oldsigset;
  sigfillset (&fullsigset);

  pthread_sigmask (SIG_SETMASK, &fullsigset, &oldsigset);

  if (pthread_create (&id, &attr, start_routine, arg))
    cleanup ("unable to create a new thread");

  pthread_sigmask (SIG_SETMASK, &oldsigset, 0);
}

