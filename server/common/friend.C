/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#include <global.h>

/*
 * Add a new friendly object to the linked list of friendly objects.
 * No checking to see if the object is already in the linked list is done.
 */
void
add_friendly_object (object *op)
{
  op->flag [FLAG_FRIENDLY] = 1;

  objectlink *ol;

  /* Add some error checking.  This shouldn't happen, but the friendly
   * object list usually isn't very long, and remove_friendly_object
   * won't remove it either.  Plus, it is easier to put a breakpoint in
   * the debugger here and see where the problem is happening.
   */
  for (ol = first_friendly_object; ol; ol = ol->next)
    {
      if (ol->ob == op)
        {
          LOG (llevError | logBacktrace, "add_friendly_object: Trying to add object already on list (%s)\n", &op->name);
          return;
        }
    }

  ol = first_friendly_object;
  first_friendly_object = get_objectlink ();
  first_friendly_object->ob = op;
  first_friendly_object->next = ol;
}

/*
 * Removes the specified object from the linked list of friendly objects.
 */
void
remove_friendly_object (object *op)
{
  objectlink *obj;

  op->clr_flag (FLAG_FRIENDLY);

  if (op->type == GOLEM
      && op->owner
      && op->owner->contr
      && op->owner->contr->golem == op)
    op->owner->contr->golem = 0;

  if (!first_friendly_object)
    {
      LOG (llevError, "remove_friendly_object called with empty friendly list, remove ob=%s\n", &op->name);
      return;
    }

  /* if the first object happens to be the one, processing is pretty
   * easy.
   */
  if (first_friendly_object->ob == op)
    {
      obj = first_friendly_object;
      first_friendly_object = obj->next;
      delete obj;
    }
  else
    {
      objectlink *prev = first_friendly_object;

      for (obj = first_friendly_object->next; obj; obj = obj->next)
        {
          if (obj->ob == op)
            break;

          prev = obj;
        }

      if (obj)
        {
          prev->next = obj->next;
          delete obj;
        }
    }
}

/* New function, MSW 2000-1-14
 * It traverses the friendly list removing objects that should not be here
 * (ie, do not have friendly flag set, freed, etc)
 */
void
clean_friendly_list ()
{
  objectlink *obj, *prev = NULL, *next;
  int count = 0;

  for (obj = first_friendly_object; obj; obj = next)
    {
      next = obj->next;
      if (obj->ob->flag [FLAG_FREED] || !obj->ob->flag [FLAG_FRIENDLY])
        {
          if (prev)
            prev->next = obj->next;
          else
            first_friendly_object = obj->next;

          count++;
          delete obj;
        }
      else
        /* If we removed the object, then prev is still valid.  */
        prev = obj;
    }

  if (count)
    LOG (llevDebug, "clean_friendly_list: Removed %d bogus links\n", count);
}

