/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

/* alchemy.c */
int apply_shop_mat (object *shop_mat, object *op);
const char *cost_string_from_value (sint64 cost, int approx);
void pay_player (object *pl, sint64 amount);
sint64 pay_player_arch (object *pl, const char *arch, sint64 amount);
void attempt_do_alchemy (object *caster, object *cauldron, object *skill);

/* apply.c */
int transport_can_hold (const object *transport, const object *op, int nrof);
int apply_transport (object *pl, object *transport, int aflag);
int should_director_abort (object *op, object *victim);
int apply_potion (object *op, object *tmp);
int esrv_apply_container (object *op, object *sack);
void move_apply (object *trap, object *victim, object *originator);
void do_learn_spell (object *op, object *spell, int special_prayer);
void do_forget_spell (object *op, const char *spell);
void apply_scroll (object *op, object *tmp, int dir);
void apply_poison (object *op, object *tmp);
void player_apply_below (object *pl);
int can_apply_object (object *who, object *op);
int auto_apply (object *op);
void fix_auto_apply (maptile *m);
void eat_special_food (object *who, object *food);
void apply_changes_to_player (object *pl, object *change);
void handle_apply_yield (object *op);
void apply_lamp (object *op, bool switch_on);
void get_animation_from_arch (object *op, arch_ptr a);
int convert_item (object *item, object *converter);

/* attack.c */
void cancellation (object *op);
void save_throw_object (object *op, int type, object *originator);
int hit_map (object *op, int dir, uint32_t type, int full_hit);
int attack_ob (object *op, object *hitter);
object *hit_with_arrow (object *op, object *victim);
int hit_player_attacktype (object *op, object *hitter, int dam, uint32_t attacknum, int magic);
int kill_object (object *op, int dam, object *hitter, int type);
int friendly_fire (object *op, object *hitter);
int hit_player (object *op, int dam, object *hitter, uint32_t type, int full_hit);
void confuse_player (object *op, object *hitter, int dam);
void blind_player (object *op, object *hitter, int dam);
void paralyze_player (object *op, object *hitter, int dam);

/* build_map.c */
void apply_map_builder (object *pl, int dir);

/* c_misc.c */
int command_motd (object *op, char *params);
int command_bug (object *op, char *params);
void malloc_info (object *op);
int command_whereabouts (object *op, char *params);
int command_malloc (object *op, char *params);
int command_maps (object *op, char *params);
int command_strings (object *op, char *params);
int command_sstable (object *op, char *params);
int command_time (object *op, char *params);
int command_weather (object *op, char *params);
int command_archs (object *op, char *params);
int command_hiscore (object *op, char *params);
int command_debug (object *op, char *params);
int command_dumpbelow (object *op, char *params);
int command_dumpfriendlyobjects (object *op, char *params);
int command_wizpass (object *op, char *params);
int command_wizcast (object *op, char *params);
int command_printlos (object *op, char *params);
int command_version (object *op, char *params);
void bug_report (const char *reportstring);
int command_statistics (object *pl, char *params);
int command_fix_me (object *op, char *params);
int command_players (object *op, char *paramss);
int command_bowmode (object *op, char *params);
int command_showpets (object *op, char *params);
int command_resistances (object *op, char *params);
int command_help (object *op, char *params);
int command_quit (object *op, char *params);
int command_real_quit (object *op, char *params);
void receive_player_name (object *op, char k);
void receive_player_password (object *op, char k);
int command_title (object *op, char *params);
int command_kill_pets (object *op, char *params);
int command_quests (object *pl, char *params);

/* c_move.c */
int command_east (object *op, char *params);
int command_north (object *op, char *params);
int command_northeast (object *op, char *params);
int command_northwest (object *op, char *params);
int command_south (object *op, char *params);
int command_southeast (object *op, char *params);
int command_southwest (object *op, char *params);
int command_west (object *op, char *params);
int command_stay (object *op, char *params);
int command_run (object *op, char *params);
int command_run_stop (object *op, char *params);
int command_fire (object *op, char *params);
int command_fire_stop (object *op, char *params);

/* c_new.c */
void execute_newserver_command (object *pl, char *command);

/* c_object.c */
object *find_best_object_match (object *pl, const char *params);
int command_build (object *pl, char *params);
int command_uskill (object *pl, char *params);
int command_rskill (object *pl, char *params);
int command_search (object *op, char *params);
int command_disarm (object *op, char *params);
int command_throw (object *op, char *params);
int command_apply (object *op, char *params);
int sack_can_hold (object *pl, object *sack, object *op, uint32 nrof);
void pick_up (object *op, object *alt);
int command_take (object *op, char *params);
void put_object_in_sack (object *op, object *sack, object *tmp, uint32 nrof);
void drop_object (object *op, object *tmp, uint32 nrof);
void drop_object (object *dropper, object *obj);
void drop (object *op, object *tmp);
int command_dropall (object *op, char *params);
int command_drop (object *op, char *params);
int command_examine (object *op, char *params);
void examine_monster (object *op, object *tmp);
const char *long_desc (object *tmp, object *pl);
void examine (object *op, object *tmp);
int command_pickup (object *op, char *params);
void set_pickup_mode (object *op, int i);
int command_search_items (object *op, char *params);
int command_rename_item (object *op, char *params);
int command_unlock (object *op, char *params);
int command_lock (object *op, char *params);
int command_mark (object *op, char *params);

/* c_party.c */
partylist *get_firstparty ();
void obsolete_parties ();
void add_kill_to_party (partylist *party, const char *killer, const char *dead, long exp);
void receive_party_password (object *op, char k);
int command_gsay (object *op, char *params);
int command_party (object *op, char *params);

/* c_range.c */
int command_invoke (object *op, char *params);
int command_cast (object *op, char *params);
int command_prepare (object *op, char *params);
int legal_range (object *op, int r);
int command_rotateshoottype (object *op, char *params);

/* c_wiz.c */
int command_loadtest (object *op, char *params);
int command_setgod (object *op, char *params);
int command_banish (object *op, char *params);
int command_save_overlay (object *op, char *params);
int command_freeze (object *op, char *params);
int command_arrest (object *op, char *params);
int command_summon (object *op, char *params);
int command_teleport (object *op, char *params);
int command_create (object *op, char *params);
int command_inventory (object *op, char *params);
int command_skills (object *op, char *params);
int command_dump (object *op, char *params);
int command_patch (object *op, char *params);
int command_remove (object *op, char *params);
int command_free (object *op, char *params);
int command_addexp (object *op, char *params);
int command_speed (object *op, char *params);
int command_stats (object *op, char *params);
int command_abil (object *op, char *params);
int command_invisible (object *op, char *params);
int command_learn_spell (object *op, char *params);
int command_learn_special_prayer (object *op, char *params);
int command_forget_spell (object *op, char *params);
void dm_stack_push (player *pl, tag_t item);
int command_stack_pop (object *op, char *params);
int command_stack_push (object *op, char *params);
int command_stack_list (object *op, char *params);
int command_stack_clear (object *op, char *params);
int command_diff (object *op, char *params);
int command_insert_into (object *op, char *params);

/* daemon.c */
FILE *BecomeDaemon (const char *filename);

/* disease.c */
int move_disease (object *disease);
int infect_object (object *victim, object *disease, int force);
int move_symptom (object *symptom);
int check_physically_infect (object *victim, object *hitter);
int cure_disease (object *sufferer, object *caster, object *spell);

/* egoitem.c */
int apply_power_crystal (object *op, object *crystal);

/* gods.c */
object *find_god (shstr_cmp name);
shstr_tmp determine_god (object *op);
void pray_at_altar (object *pl, object *altar, object *skill);
void become_follower (object *op, object *new_god);
archetype *determine_holy_arch (object *god, shstr_cmp type);
int tailor_god_spell (object *spellop, object *caster);

/* init.c */
void load_settings ();
void init (int argc, char **argv);
void usage ();
void help ();
void init_beforeplay ();
void init_signals ();
racelink *find_racelink (const char *name);

/* login.c */
void delete_character (const char *name);
int verify_player (const char *name, char *password);
int check_name (player *me, const char *name);
int create_savedir_if_needed (char *savedir);
void copy_file (const char *filename, FILE *fpout);

/* main.c */
void one_tick ();
void version (object *op);
void info_keys (object *op);
char *crypt_string (char *str, char *salt);
void enter_player_savebed (object *op);
void leave_map (object *op);
char *clean_path (const char *file);
char *unclean_path (const char *src);
void enter_exit (object *op, object *exit_ob);
void clean_tmp_files ();
void leave (player *pl,int draw_exit);
int main (int argc, char **argv);

/* monster.c */
object *check_enemy (object *npc, rv_vector *rv);
object *find_nearest_living_creature (object *npc);
int move_monster (object *op);
void monster_check_apply (object *mon, object *item);
void npc_call_help (object *op);
void check_earthwalls (object *op, maptile *m, int x, int y);
void check_doors (object *op, maptile *m, int x, int y);
void communicate (object *op, const char *txt);
int talk_to_npc (object *op, object *npc, const char *txt);
int talk_to_wall (object *pl, object *npc, const char *txt);
object *find_mon_throw_ob (object *op);
int can_detect_enemy (object *op, object *enemy, rv_vector *rv);
int stand_in_light (object *op);
int can_see_enemy (object *op, object *enemy);

/* move.c */
int transfer_ob (object *op, int x, int y, int randomly, object *originator);
int teleport (object *teleporter, uint8 tele_type, object *user);
void recursive_roll (object *op, int dir, object *pusher);
int push_ob (object *who, int dir, object *pusher);

/* pets.c */
object *get_pet_enemy (object *pet, rv_vector *rv);
void terminate_all_pets (object *owner);
void move_all_pets ();
int follow_owner (object *ob, object *owner);
void pet_move (object *ob);
void move_golem (object *op);
void control_golem (object *op, int dir);
int summon_golem (object *op, object *caster, int dir, object *spob);
int summon_object (object *op, object *caster, object *spell_ob, int dir, const char *spellparam);
int should_arena_attack (object *pet, object *owner, object *target);

/* player.c */
player *find_player (const char *plname);
player *find_player_partial_name (const char *plname);
int playername_ok (const char *cp);
object *get_nearest_player (object *mon);
int path_to_player (object *mon, object *pl, unsigned mindiff);
void give_initial_items (object *pl, treasurelist *items);
void get_name (object *op);
void get_password (object *op);
void play_again (object *op);
int receive_play_again (object *op, char key);
void confirm_password (object *op);
void get_party_password (object *op, partylist *party);
void Roll_Again (object *op);
void Swap_Stat (object *op, int Swap_Second);
int key_roll_stat (object *op, char key);
int key_change_class (object *op, char key);
int key_confirm_quit (object *op, char key);
int check_pick (object *op);
int fire_bow (object *op, object *part, object *arrow, int dir, int wc_mod, sint16 sx, sint16 sy);
bool fire (object *who, int dir);
object *find_key (object *pl, object *container, object *door);
bool move_player_attack (object *op, int dir);
bool move_player (object *op, int dir);
bool handle_newcs_player (object *op);
void do_some_living (object *op);
void kill_player (object *op);
void fix_weight ();
void fix_luck ();
void cast_dust (object *op, object *throw_ob, int dir);
void make_visible (object *op);
int is_true_undead (object *op);
int hideability (object *ob);
void do_hidden_move (object *op);
int stand_near_hostile (object *who);
int player_can_view (object *pl, object *op);
int op_on_battleground (object *op, int *x, int *y);
void dragon_ability_gain (object *who, int atnr, int level);

/* resurrection.c */
int cast_raise_dead_spell (object *op, object *caster, object *spell, int dir, const char *arg);

/* rune.c */
int write_rune (object *op, object *caster, object *spell, int dir, const char *runename);
void move_rune (object *op);
void spring_trap (object *trap, object *victim);
int dispel_rune (object *op, object *caster, object *spell, object *skill, int dir);
int trap_see (object *op, object *trap);
int trap_show (object *trap, object *where);
int trap_disarm (object *disarmer, object *trap, int risk, object *skill);
void trap_adjust (object *trap, int difficulty);

/* shop.c */
int get_payment (object *pl);
sint64 query_money (const object *op);
int pay_for_amount (sint64 to_pay, object *pl);
int pay_for_item (object *op, object *pl);
int can_pay (object *pl);
bool sell_item (object *op, object *pl);
double shopkeeper_approval (const maptile *map, const object *player);
int describe_shop (const object *op);
void shop_listing (object *sign, object *op);
bool is_in_shop (maptile *map, int x, int y);

/* skills.c */
int steal (object *op, int dir, object *skill);
int pick_lock (object *pl, int dir, object *skill);
int hide (object *op, object *skill);
int jump (object *pl, int dir, object *skill);
int skill_ident (object *pl, object *skill);
int use_oratory (object *pl, int dir, object *skill);
int singing (object *pl, int dir, object *skill);
int find_traps (object *pl, object *skill);
int remove_trap (object *op, int dir, object *skill);
int pray (object *pl, object *skill);
void meditate (object *pl, object *skill);
int write_on_item (object *pl, const char *params, object *skill);
int skill_throw (object *op, object *part, int dir, const char *params, object *skill);
bool skill_mining (object *who, object *tool, object *skill, int dir, const char *string);
bool skill_fishing (object *who, object *tool, object *skill, int dir, const char *string);

/* skill_util.c */
void init_skills ();
void link_player_skills (object *op);
int do_skill (object *op, object *part, object *skill, int dir, const char *string);
int calc_skill_exp (object *who, object *op, object *skill);
int learn_skill (object *pl, object *scroll);
void show_skills (object *op, const char *search);
int use_skill (object *op, const char *string);
int skill_attack (object *tmp, object *pl, int dir, const char *string, object *skill);

/* spell_attack.c */
void move_bolt (object *op);
int fire_bolt (object *op, object *caster, int dir, object *spob, object *skill);
void explosion (object *op);
void check_bullet (object *op);
void move_bullet (object *op);
int fire_bullet (object *op, object *caster, int dir, object *spob);
void move_cone (object *op);
int cast_cone (object *op, object *caster, int dir, object *spell);
void animate_bomb (object *op);
int create_bomb (object *op, object *caster, int dir, object *spell);
int cast_smite_spell (object *op, object *caster, int dir, object *spell);
void move_missile (object *op);
int cast_destruction (object *op, object *caster, object *spell_ob);
int cast_curse (object *op, object *caster, object *spell_ob, int dir);
int mood_change (object *op, object *caster, object *spell);
void move_ball_spell (object *op);
void move_swarm_spell (object *op);
int fire_swarm (object *op, object *caster, object *spell, int dir);
int cast_light (object *op, object *caster, object *spell, int dir);
int cast_cause_disease (object *op, object *caster, object *spell, int dir);

/* spell_effect.c */
void cast_magic_storm (object *op, object *tmp, int lvl);
int recharge (object *op, object *caster, object *spell_ob);
void polymorph_living (object *op);
void polymorph_melt (object *who, object *op);
void polymorph_item (object *who, object *op);
void polymorph (object *op, object *who);
int cast_polymorph (object *op, object *caster, object *spell_ob, int dir);
int cast_create_missile (object *op, object *caster, object *spell, int dir, const char *spellparam);
int cast_create_food (object *op, object *caster, object *spell_ob, int dir, const char *spellparam);
int probe (object *op, object *caster, object *spell_ob, int dir);
int makes_invisible_to (object *pl, object *mon);
int cast_invisible (object *op, object *caster, object *spell_ob);
int cast_earth_to_dust (object *op, object *caster, object *spell_ob);
void execute_word_of_recall (object *op);
int cast_word_of_recall (object *op, object *caster, object *spell_ob);
int cast_wonder (object *op, object *caster, int dir, object *spell_ob);
int perceive_self (object *op);
int magic_wall (object *op, object *caster, int dir, object *spell_ob);
int dimension_door (object *op, object *caster, object *spob, int dir, const char *spellparam);
int cast_heal (object *op, object *caster, object *spell, int dir);
int cast_change_ability (object *op, object *caster, object *spell_ob, int dir, int silent);
int cast_bless (object *op, object *caster, object *spell_ob, int dir);
int alchemy (object *op, object *caster, object *spell_ob);
int remove_curse (object *op, object *caster, object *spell);
int cast_identify (object *op, object *caster, object *spell);
int cast_detection (object *op, object *caster, object *spell, object *skill);
int cast_transfer (object *op, object *caster, object *spell, int dir);
void counterspell (object *op, int dir);
int cast_consecrate (object *op, object *caster, object *spell);
int animate_weapon (object *op, object *caster, object *spell, int dir);
int cast_change_map_lightlevel (object *op, object *caster, object *spell);
int create_aura (object *op, object *caster, object *spell);
void move_aura (object *aura);
void move_peacemaker (object *op);
int write_mark (object *op, object *spell, const char *msg);

/* spell_util.c */
object *find_random_spell_in_ob (object *ob, shstr_cmp skill = shstr_cmp ());
void set_spell_skill (object *op, object *caster, object *spob, object *dest);
void spell_effect (object *spob, int x, int y, maptile *map, object *originator);
object *check_spell_known (object *op, shstr_cmp name);
int reflwall (maptile *m, int x, int y, object *sp_op);
bool cast_create_obj (object *op, object *caster, object *new_op, int dir);
int ok_to_put_more (maptile *m, sint16 x, sint16 y, object *op, int immune_stop);
int fire_arch_from_position (object *op, object *caster, sint16 x, sint16 y, int dir, object *spell);
void regenerate_rod (object *rod);
void drain_rod_charge (object *rod);
object *find_target_for_friendly_spell (object *op, int dir);
int spell_find_dir (maptile *m, int x, int y, object *exclude);
int summon_hostile_monsters (object *op, int n, const char *monstername);
void shuffle_attack (object *op, int change_face);
void spell_failure (object *op, int failure, int power, object *skill);
int cast_spell (object *op, object *caster, int dir, object *spell_ob, char *spellparam);
void move_spell_effect (object *op);
void apply_spell_effect (object *spell, object *victim);
void create_exploding_ball_at (object *victim, int level);

/* swamp.c */
void walk_on_deep_swamp (object *op, object *victim);
void move_deep_swamp (object *op);

/* swap.c */
void read_map_log ();
void swap_map (maptile *map);
void check_active_maps ();
maptile *map_least_timeout (char *except_level);
void swap_below_max (char *except_level);
int players_on_map (maptile *m, int show_all);
void flush_old_maps ();

/* time.c */
void remove_door (object *op);
void remove_door2 (object *op);
void animate_trigger (object *op);
object *stop_item (object *op);
void fix_stopped_item (object *op, maptile *map, object *originator);
object *fix_stopped_arrow (object *op);
void move_arrow (object *op);
void move_teleporter (object *op);
void move_firewall (object *op);
void move_duplicator (object *op);
void move_creator (object *creator);
void move_marker (object *op);
void process_object (object *op);

/* weather.c */
void adjust_daylight ();

