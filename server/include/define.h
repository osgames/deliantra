/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2003-2005 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

/* This file is best viewed with a window width of about 100 character */

/* This file is really too large.  With all the .h files
 * around, this file should be better split between them - things
 * that deal with objects should be in objects.h, things dealing
 * with players in player.h, etc.  As it is, everything just seems
 * to be dumped in here.
 */

#ifndef DEFINE_H
#define DEFINE_H

#include <autoconf.h>

#define FONTDIR ""
#define FONTNAME ""

#ifdef __NetBSD__
# include <sys/param.h>
#endif

#define MAXNUMLEVELS 256      // maximum number of levels possible
#define MAXLEVEL_TREASURE 115 // for treasure calculations only

// maximum length of an object name in the protocol
#define NAME_LEN 127

#define MAX_STAT		30      /* The maximum legal value of any stat */
#define MIN_STAT		1       /* The minimum legal value of any stat */

#define MAX_FOOD		999

//TODO: not only use more reasonable values, also enforce them
#define MIN_WC                  -120
#define MAX_WC                  120
#define MIN_AC			-120
#define MAX_AC			120
#define MIN_DAM			0
#define MAX_DAM			200
#define MIN_DIGESTION		-35
#define MAX_DIGESTION		70

#define MAX_BUF			1024    /* Used for all kinds of things */

#define MAX_NAME 48

#define ATTUNE_REPELL		16 // levels diff for attune/repell

/* TYPE DEFINES */

/* Only add new values to this list if somewhere in the program code,
 * it is actually needed.  Just because you add a new monster does not
 * mean it has to have a type defined here.  That only needs to happen
 * if in some .c file, it needs to do certain special actions based on
 * the monster type, that can not be handled by any of the numerous
 * flags
 * Also, if you add new entries, try and fill up the holes in this list.
 * Additionally, when you add a new entry, include it in the table in item.c
 */

/* USED TYPES: (for dead types please look at the bottom of the type
 *              definitions)
 */

/* type 0 objects have the default behaviour */

#define PLAYER                             1
#define TRANSPORT                          2 /* see pod/objects.pod */
#define ROD                                3
#define TREASURE                           4
#define POTION                             5
#define FOOD                               6
#define POISON                             7
#define BOOK                               8
#define CLOCK                              9
#define VEIN                              10 // deliantra: mineral/ore/whatever vein
#define RANGED                            11 // deliantra: other range item (skill based)
#define SKILLSCROLL                       12 /* can add a skill to player's inventory -bt. */
#define ARROW                             13
#define BOW                               14
#define WEAPON                            15
#define ARMOUR                            16
#define PEDESTAL                          17
#define ALTAR                             18
#define T_MATCH                           19
#define LOCKED_DOOR                       20
#define SPECIAL_KEY                       21
#define MAP                               22
#define DOOR                              23
#define KEY                               24
#define PHYSICS                           25 // deliantra: "things that move"
#define TIMED_GATE                        26
#define TRIGGER                           27
#define GRIMREAPER                        28
#define MAGIC_EAR                         29
#define TRIGGER_BUTTON                    30
#define TRIGGER_ALTAR                     31
#define TRIGGER_PEDESTAL                  32
#define SHIELD                            33
#define HELMET                            34
#define HORN                              35
#define MONEY                             36
#define CLASS                             37 /* object for applying character class modifications to someone */
#define GRAVESTONE                        38
#define AMULET                            39
#define PLAYERMOVER                       40
#define TELEPORTER                        41
#define CREATOR                           42
#define SKILL                             43 /* also see SKILL_TOOL (74) below */
#define IDENTIFY_ALTAR                    44
#define EARTHWALL                         45
#define GOLEM                             46
#define DEEP_SWAMP                        47
#define THROWN_OBJ                        48
#define BLINDNESS                         49
#define GOD                               50
#define DETECTOR                          51 /* peterm:  detector is an object
                                              * which notices the presense of
                                              * another object and is triggered
                                              * like buttons.
                                              */
#define TRIGGER_MARKER                    52 /* inserts an invisible, weightless
                                              * force into a player with a specified string WHEN TRIGGERED.
                                              */
#define DEAD_OBJECT                       53
#define DRINK                             54
#define MARKER                            55 /* inserts an invisible, weightless
                                              * force into a player with a specified string.
                                              */
#define HOLY_ALTAR                        56
#define PLAYER_CHANGER                    57
#define BATTLEGROUND                      58 /* battleground, by Andreas Vogl */

#define PEACEMAKER                        59 /* Object owned by a player which can convert
                                              * a monster into a peaceful being incapable of attack.
                                              */
#define GEM                               60
#define MENU                              61 /* Mark Wedel (mark@pyramid.com) Shop inventories */
#define FIREWALL                          62
#define ANVIL                             63
#define CHECK_INV                         64 /* by b.t. thomas@nomad.astro.psu.edu */
#define MOOD_FLOOR                        65 /*
                                              * values of last_sp set how to change:
                                              * 0 = furious, all monsters become aggressive
                                              * 1 = angry, all but friendly become aggressive
                                              * 2 = calm, all aggressive monsters calm down
                                              * 3 = sleep, all monsters fall asleep
                                              * 4 = charm, monsters become pets
                                              * 5 = destroy monsters
                                              * 6 = destroy pets / friendlies
                                              */
#define EXIT                              66
#define ENCOUNTER                         67
#define SHOP_FLOOR                        68
#define SHOP_MAT                          69
#define RING                              70
//#define FLOOR                             71 /* this is a floor tile -> native layer 0 */ // UNUSED???
#define FLESH                             72 /* animal 'body parts' -b.t. */
#define INORGANIC                         73 /* metals and minerals */
#define SKILL_TOOL                        74 /* Allows the use of a skill */
#define LIGHTER                           75
//76
#define BUILDABLE_WALL                    77 /* this is a buildable wall */
//78
#define MISC_OBJECT                       79  /* misc. objects are for objects without a function
                                                 in the engine. Like statues, clocks, chairs,...
                                                 If perhaps we create a function where we can sit
                                                 on chairs, we create a new type and remove all
                                                 chairs from here. */
//80
#define TORCH                             81  /* a torch */
#define LAMP                              82  /* a lamp */
#define DUPLICATOR                        83  /* duplicator/multiplier object */
//84
#define SPELLBOOK                         85
//86
#define CLOAK                             87
#define MAPSCRIPT                         88 /* A perl-scripted connectable */
#define SAFE_GROUND                       89 /* This is a safe ground, means that neither bombs,
                                                potions, alchemy, or magic works here (elmex) */
#define SPINNER                           90
#define GATE                              91
#define BUTTON                            92
#define T_HANDLE                          93
#define HOLE                              94
#define TRAPDOOR                          95
#define RUNE                              96
#define TRAP                              97
#define SIGN                              98
#define BOOTS                             99
#define GLOVES                           100
#define SPELL                            101
#define SPELL_EFFECT                     102
#define CONVERTER                        103
#define BRACERS                          104
#define POISONING                        105
#define SAVEBED                          106
#define DISEASE                          107
#define SYMPTOM                          108
#define WAND                             109
#define INSCRIBABLE			 110 // inscribable things, st 0 book st 1 spell
#define SCROLL                           111
#define DIRECTOR                         112
#define GIRDLE                           113
#define FORCE                            114
#define POTION_EFFECT                    115 /* a force, holding the effect of a potion */
#define EVENT_CONNECTOR                  116 /* Lauwenmark: an invisible object holding a plugin event hook */
#define ITEM_TRANSFORMER                 117 /* Transforming one item with another */
#define POWER_CRYSTAL                    118
#define CORPSE                           119
//120
#define CLOSE_CON                        121 /* Eneq(@csd.uu.se): Id for close_container archetype. */
#define CONTAINER                        122
#define ARMOUR_IMPROVER                  123
#define WEAPON_IMPROVER                  124
#define BUILDER                          125 /* Generic item builder, see subtypes */
#define MATERIAL                         126 /* Material for building */

#define NUM_TYPES                        127 // must be max(type) + 1

/* END TYPE DEFINE */

typedef std::bitset<NUM_TYPES> typeset;

/* These are the items that currently can change digestion, regeneration,
 * spell point recovery and mana point recovery. Seems sort of an arbitary
 * list, but other items store other info into stats array.
 * As a special exception, bows use stats.sp for their own purposes.
 */
static const struct digest_types : typeset
{
  digest_types ()
  {
    set (WEAPON);
    set (BOW);
    set (ARMOUR);
    set (HELMET);
    set (SHIELD);
    set (RING);
    set (BOOTS);
    set (GLOVES);
    set (AMULET);
    set (GIRDLE);
    set (BRACERS);
    set (CLOAK);
    set (DISEASE);
    set (FORCE);
    set (SKILL);
  }
} digest_types;

// maximum supported subtype number + 1, can be increased to 256
// currently (2007-09) in use: 50
#define NUM_SUBTYPES    64

/* Subtypes for BUILDER */
#define ST_BD_BUILD     1      /* Builds an item */
#define ST_BD_REMOVE    2      /* Removes an item */

/* Subtypes for MATERIAL */
#define ST_MAT_FLOOR    1      /* Floor */
#define ST_MAT_WALL     2      /* Wall */
#define ST_MAT_ITEM     3      /* All other items, including doors & such */
#define ST_MAT_QUAD     4      /* Quad build material */

/* definitions for weapontypes */

#define WEAP_HIT	0       /* the basic */
#define WEAP_SLASH	1       /* slash */
#define WEAP_PIERCE	2       /* arrows, stiletto */
#define WEAP_CLEAVE	3       /* axe */
#define WEAP_SLICE	4       /* katana */
#define WEAP_STAB	5       /* knife, dagger */
#define WEAP_WHIP	6       /* whips n chains */
#define WEAP_CRUSH	7       /* big hammers, flails */
#define WEAP_BLUD	8       /* bludgeoning, club, stick */

typedef struct typedata
{
  int number;
  const char *name;
  const char *name_pl;
  int identifyskill;
  int identifyskill2;
} typedata;

extern const int ItemTypesSize;
extern typedata ItemTypes[];

/* definitions for detailed pickup descriptions.
 *   The objective is to define intelligent groups of items that the
 *   user can pick up or leave as he likes. */

/* high bit as flag for new pickup options */
#define PU_NOTHING		0x00000000

#define PU_DEBUG		0x10000000
#define PU_INHIBIT		0x20000000
#define PU_STOP			0x40000000
#define PU_ENABLE		0x80000000 // used to distinguish value density

#define PU_RATIO		0x0000000F

#define PU_FOOD			0x00000010
#define PU_DRINK		0x00000020
#define PU_VALUABLES		0x00000040
#define PU_BOW			0x00000080

#define PU_ARROW		0x00000100
#define PU_HELMET		0x00000200
#define PU_SHIELD		0x00000400
#define PU_ARMOUR		0x00000800

#define PU_BOOTS		0x00001000
#define PU_GLOVES		0x00002000
#define PU_CLOAK		0x00004000
#define PU_KEY			0x00008000

#define PU_MISSILEWEAPON	0x00010000
#define PU_ALLWEAPON		0x00020000
#define PU_MAGICAL		0x00040000
#define PU_POTION		0x00080000

#define PU_SPELLBOOK		0x00100000
#define PU_SKILLSCROLL		0x00200000
#define PU_READABLES		0x00400000
#define PU_MAGIC_DEVICE		0x00800000

#define PU_NOT_CURSED		0x01000000
#define PU_JEWELS		0x02000000
#define PU_FLESH		0x04000000


/* Instead of using arbitrary constants for indexing the
 * freearr, add these values.  <= SIZEOFFREE1 will get you
 * within 1 space.  <= SIZEOFFREE2 wll get you withing
 * 2 spaces, and the entire array (< SIZEOFFREE) is
 * three spaces
 */
#define SIZEOFFREE0  0
#define SIZEOFFREE1  8
#define SIZEOFFREE2 24
#define SIZEOFFREE3 48
#define SIZEOFFREE  49

#define NROF_SOUNDS (23 + NROFREALSPELLS)      /* Number of sounds */

/*
 * If any FLAG's are added or changed, make sure the flag_names structure in
 * common/loader.C is updated.
 */

/* the flags */

#define FLAG_ALIVE	           0      /* Object can fight (or be fought) */
#define FLAG_WIZ	           1      /* Object has special privilegies */
#define FLAG_REMOVED	           2      /* Object is not in any map or inventory */
#define FLAG_FREED	           3      /* Object is in the list of free objects */
#define FLAG_WIZLOOK	           4      /* disable los and lighting */
#define FLAG_APPLIED	           5      /* Object is ready for use by living */
#define FLAG_UNPAID	           6      /* Object hasn't been paid for yet */
#define FLAG_USE_SHIELD	           7	  /* Can this creature use a shield? */
#define FLAG_NO_PICK	           8      /* Object can't be picked up */
#define FLAG_IS_TRANSPARENT_FLOOR  9      // floor shows things on lower z-level(s), CF: walk_on
/*#define FLAG_NO_PASS	          10*//* Nothing can pass (wall() is true) */
#define FLAG_ANIMATE	          11      /* The object looks at archetype for faces */
/*#define FLAG_SLOW_MOVE          12*//* Uses the stats.exp/1000 to slow down */
/*#define FLAG_FLYING	          13*//* Not affected by WALK_ON or SLOW_MOVE) */
#define FLAG_HIDDEN               13      /* hidden monster (not invisible), TODO: used how, what for? TODO: why not saved? */
#define FLAG_MONSTER	          14      /* Will attack players */
#define FLAG_FRIENDLY	          15      /* Will help players */
#define FLAG_GENERATOR	          16      /* Will generate type ob->stats.food */
#define FLAG_IS_THROWN	          17      /* Object is designed to be thrown. */
#define FLAG_AUTO_APPLY	          18	  /* Will be applied when created */
#define FLAG_TREASURE_ENV         19      // put treasure into environment not in inventory
#define FLAG_PLAYER_SOLD          20      /* Object was sold to a shop by a player. */
#define FLAG_SEE_INVISIBLE        21      /* Will see invisible player */
#define FLAG_CAN_ROLL	          22      /* Object can be rolled */
#define FLAG_OVERLAY_FLOOR        23      /* Object is an overlay floor */
#define FLAG_IS_TURNABLE          24      /* Object can change face with direction */
/*#define FLAG_WALK_OFF	          25*//* Object is applied when left */
/*#define FLAG_FLY_ON	          26*//* As WALK_ON, but only with FLAG_FLYING */
/*#define FLAG_FLY_OFF	          27*//* As WALK_OFF, but only with FLAG_FLYING */
#define FLAG_IS_USED_UP	          28	  /* When (--food<0) the object will get destroyed */
#define FLAG_IDENTIFIED	          29	  /* Player knows full info about item */
#define FLAG_REFLECTING	          30	  /* Object reflects from walls (lightning) */
#define FLAG_CHANGING	          31      /* Changes to other_arch when anim is done, creates <food> new <other_archs> when !alive */
#define FLAG_SPLITTING	          32      /* Object splits into stats.food other objs */
#define FLAG_HITBACK	          33      /* Object will hit back when hit */
#define FLAG_STARTEQUIP	          34	  /* Object was given to player at start */
#define FLAG_BLOCKSVIEW	          35	  /* Object blocks view */
#define FLAG_UNDEAD	          36      /* Monster is undead */
#define FLAG_SCARED	          37      /* Monster is scared (mb player in future) */
#define FLAG_UNAGGRESSIVE         38      /* Monster doesn't attack players */
#define FLAG_REFL_MISSILE         39      /* Arrows will reflect from object */
#define FLAG_REFL_SPELL	          40	  /* Spells (some) will reflect from object */
#define FLAG_NO_MAGIC	          41      /* Spells (some) can't pass this object */
//#define FLAG_NO_FIX_PLAYER      42*/    /* fix_player() won't be called */
#define FLAG_IS_LIGHTABLE         43      /* object can be lit */
#define FLAG_TEAR_DOWN	          44      /* at->faces[hp*animations/maxhp] at hit */
#define FLAG_RUN_AWAY	          45      /* Object runs away from nearest player \
                                             but can still attack at a distance */
/*#define FLAG_PASS_THRU          46*//* Objects with can_pass_thru can pass \
                                         thru this object as if it wasn't there */
/*#define FLAG_CAN_PASS_THRU      47*//* Can pass thru... */
#define FLAG_PICK_UP	          48      /* Can pick up */
#define FLAG_UNIQUE	          49      /* Item is really unique (UNIQUE_ITEMS) */
#define FLAG_NO_DROP	          50      /* Object can't be dropped */
#define FLAG_WIZCAST	          51      /* The wizard can cast spells in no-magic area */
#define FLAG_CAST_SPELL	          52      /* (Monster) can learn and cast spells */
#define FLAG_USE_SCROLL	          53      /* (Monster) can read scroll */
#define FLAG_USE_RANGE	          54      /* (Monster) can apply and use range items */
#define FLAG_USE_BOW	          55      /* (Monster) can apply and fire bows */
#define FLAG_USE_ARMOUR	          56      /* (Monster) can wear armour/shield/helmet */
#define FLAG_USE_WEAPON	          57      /* (Monster) can wield weapons */
#define FLAG_USE_RING	          58      /* (Monster) can use rings, boots, gauntlets, etc */
#define FLAG_READY_RANGE          59      /* (Monster) has a range attack readied... 8) */
#define FLAG_READY_BOW	          60      /* (Monster) has valid bow readied */
#define FLAG_XRAYS	          61      /* X-ray vision */
#define FLAG_NO_APPLY	          62      /* Avoids step_on/fly_on to this object */
#define FLAG_IS_FLOOR	          63      /* Can't see what's underneath this object */
#define FLAG_LIFESAVE	          64      /* Saves a players' life once, then destr. */
#define FLAG_NO_STRENGTH          65      /* Strength-bonus not added to wc/dam */
#define FLAG_SLEEP	          66      /* NPC is sleeping */
#define FLAG_STAND_STILL          67      /* NPC will not (ever) move */
#define FLAG_RANDOM_MOVE          68      /* NPC will move randomly */
#define FLAG_ONLY_ATTACK          69      /* NPC will evaporate if there is no enemy */
#define FLAG_CONFUSED	          70      /* Will also be unable to cast spells */
#define FLAG_STEALTH	          71      /* Will wake monsters with less range */
#define FLAG_WIZPASS	          72      /* The wizard can go through walls */
#define FLAG_IS_LINKED	          73      /* The object is linked with other objects */
#define FLAG_CURSED	          74      /* The object is cursed */
#define FLAG_DAMNED	          75      /* The object is _very_ cursed */
#define FLAG_SEE_ANYWHERE         76      /* The object will be visible behind walls */
#define FLAG_KNOWN_MAGICAL        77      /* The object is known to be magical */
#define FLAG_KNOWN_CURSED         78      /* The object is known to be cursed */
#define FLAG_CAN_USE_SKILL        79      /* The monster can use skills */
#define FLAG_BEEN_APPLIED         80      /* The object has been applied */
#define FLAG_READY_SCROLL         81      /* monster has scroll in inv and can use it */
#define FLAG_USE_ROD	          82      /* (Monster) can apply and use rods */
#define FLAG_PRECIOUS	          83      // object is precious (pets)
#define FLAG_USE_HORN	          84      /* (Monster) can apply and use horns */
#define FLAG_MAKE_INVIS	          85	  /* (Item) gives invisibility when applied */
#define FLAG_INV_LOCKED	          86	  /* Item will not be dropped from inventory */
#define FLAG_IS_WOODED	          87      /* Item is wooded terrain */
#define FLAG_IS_HILLY	          88      /* Item is hilly/mountain terrain */
#define FLAG_READY_SKILL          89      /* (Monster or Player) has a skill readied */
#define FLAG_READY_WEAPON         90      /* (Monster or Player) has a weapon readied */
#define FLAG_NO_SKILL_IDENT       91      /* If set, item cannot be identified w/ a skill */
#define FLAG_BLIND	          92      /* If set, object cannot see (visually) */
#define FLAG_SEE_IN_DARK          93      /* if set ob not effected by darkness */
#define FLAG_IS_CAULDRON          94      /* container can make alchemical stuff */
/*#define FLAG_DUST	          95 *//* item is a 'powder', effects throwing */
#define FLAG_NO_STEAL	          96      /* Item can't be stolen */
#define FLAG_ONE_HIT	          97      /* Monster can only hit once before going
                                           * away (replaces ghosthit)
                                           */
#define FLAG_DEBUG	          98      // formerly FLAG_CLIENT_SENT, not used except for debugging
                                  
#define FLAG_BERSERK              99      /* monster will attack closest living
                                             object */
#define FLAG_NEUTRAL             100      /* monster is from type neutral */
#define FLAG_NO_ATTACK           101      /* monster doesn't attack */
#define FLAG_NO_DAMAGE           102      /* monster can't be damaged */
#define FLAG_OBJ_ORIGINAL        103      /* NEVER SET THIS.  Item was loaded by
                                           * load_original_map() */
#define FLAG_RANDOM_SPEED        104      /* speed_left should be randomised on instantiate */
#define FLAG_ACTIVATE_ON_PUSH    105      /* connected object is activated when 'pushed' */
#define FLAG_ACTIVATE_ON_RELEASE 106      /* connected object is activated when 'released' */
#define FLAG_IS_WATER            107      /* apparently not used inside the server for anything */
#define FLAG_CONTENT_ON_GEN      108
#define FLAG_IS_A_TEMPLATE       109      /* Object has no ingame life until instanciated */
#define FLAG_IS_BUILDABLE        110      /* Can build on item */
#define FLAG_DESTROY_ON_DEATH    111      /* Object will be destroyed when env dies */
#define FLAG_NO_MAP_SAVE         112      // item doesn't get saved with map
#define FLAG_IS_QUAD             113      /* This is a destructible and buildable item
                                           * (for the quads world for instance)
                                           */

// temporary assignments
#define FLAG_PHYSICS_QUEUE 114 // object queued for physics TODO: temporary allocation

#define NUM_FLAGS                115      /* Should always be equal to the last
                                           * defined flag + 1.
                                           */

/* If you add new movement types, you may need to update
 * describe_item() so properly describe those types.
 * change_abil() probably should be updated also.
 */
#define MOVE_WALK	0x01    /* Object walks */
#define MOVE_FLY_LOW	0x02    /* Low flying object */
#define MOVE_FLY_HIGH	0x04    /* High flying object */
#define MOVE_SWIM	0x08    /* Swimming object */
#define MOVE_BOAT	0x10    /* Boats/sailing */
#define MOVE_SHIP	0x20    /* boats suitable for oceans */

#define	MOVE_FLYING	0x06    /* Combo of fly_low and fly_high */
#define MOVE_ALL	0x3f    /* Mask of all movement types */

/* typdef here to define type large enough to hold bitmask of
 * all movement types.  Make one declaration so easy to update.
 */
typedef unsigned char MoveType;

/* Basic macro to see if ob2 blocks ob1 from moving onto this space.
 * Basically, ob2 has to block all of ob1 movement types.
 */
#define OB_MOVE_BLOCK(ob1, ob2) \
    ((ob1->move_type & ob2->move_block) == ob1->move_type)

/* Basic macro to see if if ob1 can not move onto a space based
 * on the 'type' move_block parameter
 * Add check - if type is 0, don't stop anything from moving
 * onto it.
 *
 */
#define OB_TYPE_MOVE_BLOCK(ob1, type) \
    ((type) && (ob1->move_type & type) == ob1->move_type)

#define GENERATE_SPEED(xyz)	((xyz)->stats.maxsp)    /* if(!RANDOM()%<speed>) */

#define EXIT_PATH(xyz)		(xyz)->slaying
#define EXIT_LEVEL(xyz)		(xyz)->stats.food
#define EXIT_X(xyz)		(xyz)->stats.hp
#define EXIT_Y(xyz)		(xyz)->stats.sp
#define EXIT_ALT_X(xyz)		(xyz)->stats.maxhp
#define EXIT_ALT_Y(xyz)		(xyz)->stats.maxsp

/* for use by the lighting code */
#define MAX_LIGHT_RADIUS	9       /* max radius for 'light' object, really
                                         * large values allow objects that can
                                         * slow down the game */
#define MAX_DARKNESS		5       /* maximum map darkness, there is no
                                         * practical reason to exceed this */
#define LOS_MAX                   4     /* max. los value for non-blocked spaces */
#define LOS_BLOCKED             100     /* fully blocked spaces */
#define BRIGHTNESS(xyz)		(xyz)->glow_radius>MAX_LIGHT_RADII ? \
                                  MAX_LIGHT_RADII : (xyz)->glow_radius;
// player position in blocked_los code
#define LOS_X0 (MAP_CLIENT_X / 2 - 1)
#define LOS_Y0 (MAP_CLIENT_Y / 2 - 1)


#define F_BUY		0
#define F_SELL		1
#define F_TRUE		2       /* True value of item, unadjusted */
#define F_NO_BARGAIN	4       /* combine with F_BUY or F_SELL to disable bargaining calc */
#define F_IDENTIFIED	8       /* flag to calculate value of identified item */
#define F_NOT_CURSED	16      /* flag to calculate value of uncursed item */
#define F_APPROX	32      /* flag to give a guess of item value */
#define F_SHOP		64      /* consider the effect that the shop that the player is in has */

#define DIRX(xyz)	freearr_x[(xyz)]
#define DIRY(xyz)	freearr_y[(xyz)]

#define ARMOUR_SPEED(xyz)	(xyz)->last_sp
#define ARMOUR_SPELLS(xyz)	(xyz)->gen_sp_armour
#define WEAPON_SPEED(xyz)	(xyz)->last_sp

/* GET_?_FROM_DIR if used only for positional firing where dir is X and Y
   each of them signed char, concatenated in a int16 */
#define GET_X_FROM_DIR(dir) (signed char) (  dir & 0xFF )
#define GET_Y_FROM_DIR(dir) (signed char) ( (dir & 0xFF00) >> 8)
#define SET_DIR_FROM_XY(X,Y) (signed char)X + ( ((signed char)Y)<<8)

#define FIRE_DIRECTIONAL 0
#define FIRE_POSITIONAL  1

/******************************************************************************/
/* Monster Movements added by kholland@sunlab.cit.cornell.edu                 */
/******************************************************************************/
/* if your monsters start acting wierd, mail me                               */
/******************************************************************************/
/* the following definitions are for the attack_movement variable in monsters */
/* if the attack_variable movement is left out of the monster archetype, or is*/
/* set to zero                                                                */
/* the standard mode of movement from previous versions of crossfire will be  */
/* used. the upper four bits of movement data are not in effect when the monst*/
/* er has an enemy. these should only be used for non agressive monsters.     */
/* to program a monsters movement add the attack movement numbers to the movem*/
/* ment numbers example a monster that moves in a circle until attacked and   */
/* then attacks from a distance:                                              */
/*                                                      CIRCLE1 = 32          */
/*                                              +       DISTATT = 1           */
/*                                      -------------------                   */
/*                      attack_movement = 33                                  */
/******************************************************************************/
#define DISTATT  1 /* move toward a player if far, but mantain some space,  */
                   /* attack from a distance - good for missile users only  */
#define RUNATT   2 /* run but attack if player catches up to object         */
#define HITRUN   3 /* run to then hit player then run away cyclicly         */
#define WAITATT  4 /* wait for player to approach then hit, move if hit     */
#define RUSH     5 /* Rush toward player blindly, similiar to dumb monster  */
#define ALLRUN   6 /* always run never attack good for sim. of weak player  */
#define DISTHIT  7 /* attack from a distance if hit as recommended by Frank */
#define WAIT2    8 /* monster does not try to move towards player if far    */
                   /* maintains comfortable distance                        */

#define PETMOVE 16 /* if the upper four bits of attack_movement             */
                   /* are set to this number, the monster follows a player  */
                   /* until the owner calls it back or off                  */
                   /* player followed denoted by 0b->owner                  */
                   /* the monster will try to attack whatever the player is */
                   /* attacking, and will continue to do so until the owner */
                   /* calls off the monster - a key command will be         */
                   /* inserted to do so                                     */
#define CIRCLE1 32 /* if the upper four bits of move_type / attack_movement */
                   /* are set to this number, the monster will move in a    */
                   /* circle until it is attacked, or the enemy field is    */
                   /* set, this is good for non-aggressive monsters and NPC */
#define CIRCLE2 48 /* same as above but a larger circle is used             */
#define PACEH   64 /* The Monster will pace back and forth until attacked   */
                   /* this is HORIZONTAL movement                           */
#define PACEH2  80 /* the monster will pace as above but the length of the  */
                   /* pace area is longer and the monster stops before      */
                   /* changing directions                                   */
                   /* this is HORIZONTAL movement                           */
#define RANDO   96 /* the monster will go in a random direction until       */
                   /* it is stopped by an obstacle, then it chooses another */
                   /* direction.                                            */
#define RANDO2 112 /* constantly move in a different random direction       */
#define PACEV  128 /* The Monster will pace back and forth until attacked   */
                   /* this is VERTICAL movement                             */
#define PACEV2 144 /* the monster will pace as above but the length of the  */
                   /* pace area is longer and the monster stops before      */
                   /* changing directions                                   */
                   /* this is VERTICAL movement                             */
#define LO4     15 /* bitmasks for upper and lower 4 bits from 8 bit fields */
#define HI4    240

#define BLANK_FACE_NAME      "blank.x11"
#define EMPTY_FACE_NAME      "empty.x11"
#define MAGICMOUTH_FACE_NAME "magicmouth.x11"

/*
 * Defines for the luck/random functions to make things more readable
 */

#define PREFER_HIGH	1
#define PREFER_LOW	0

/* Flags for apply_special() */
enum apply_flag
{
  /* Basic flags/mode, always use one of these */
  AP_TOGGLE       = 0,
  AP_APPLY        = 0x01,
  AP_UNAPPLY      = 0x02,
  AP_MODE         = 0x03,

  /* Optional flags, for bitwise or with a basic flag */
  AP_NO_MERGE     = 0x10,
  AP_IGNORE_CURSE = 0x20,
  AP_PRINT        = 0x40, /* Print what to do, don't actually do it */
  AP_NO_SLOT      = 0x80, // do not update the combat/ranged/current slots
};

/* Bitmask values for 'can_apply_object()' return values.
 * the CAN_APPLY_ prefix is to just note what function the
 * are returned from.
 *
 * CAN_APPLY_NEVER: who will never be able to use this - requires a body
 *      location who doesn't have.
 * CAN_APPLY_RESTRICTION: There is some restriction from using this item -
 *      this basically means one of the FLAGS are set saying you can't
 *      use this.
 * CAN_APPLY_NOT_MASK - this can be used to check the return value to see
 *	if this object can do anything to use this object.  If the value
 *	returned from can_apply_object() anded with the mask is non zero,
 *	then it is out of the control of this creature to use the item.
 *	otherwise it means that by unequipping stuff, they could apply the object
 * CAN_APPLY_UNAPPLY: Player needs to unapply something before applying
 *      this.
 * CAN_APPLY_UNAPPLY_MULT: There are multiple items that need to be
 *      unapplied before this can be applied.  Think of switching to
 *      a bow but you have a sword & shield - both the sword and
 *      shield need to be uneqipped before you can do the bow.
 * CAN_APPLY_UNAPPLY_CHOICE: There is a choice of items to unapply before
 *      this one can be applied.  Think of rings - human is wearing two
 *      rings and tries to apply one - there are two possible rings he
 *      could remove.
 *
 */
#define CAN_APPLY_NEVER		    0x01
#define CAN_APPLY_RESTRICTION	    0x02
#define CAN_APPLY_NOT_MASK	    0x0f

#define CAN_APPLY_UNAPPLY	    0x10
#define CAN_APPLY_UNAPPLY_MULT	    0x20
#define CAN_APPLY_UNAPPLY_CHOICE    0x40

// Cut off point of when an object is put on the active list or not
// we use 2**-n because that can be represented exactly
// also make sure that this is a float, not double, constant.
// some areas in the server divide by this value, so
// to avoid integer overflows it should not be much lower.
#define MIN_ACTIVE_SPEED	(1.f / 65536.f)

/* have mercy on players and guarantee a somewhat higher speed */
#define MIN_PLAYER_SPEED	0.04f

/*
 * Warning!
 * If you add message types here, don't forget
 * to keep the client up to date too!
 */

/* message types */
#define MSG_TYPE_BOOK            1
#define MSG_TYPE_CARD            2
#define MSG_TYPE_PAPER           3
#define MSG_TYPE_SIGN            4
#define MSG_TYPE_MONUMENT        5
#define MSG_TYPE_SCRIPTED_DIALOG 6
#define MSG_TYPE_MOTD            7
#define MSG_TYPE_ADMIN           8
#define MSG_TYPE_LAST            9

#define MSG_SUBTYPE_NONE         0

/* book messages subtypes */
#define MSG_TYPE_BOOK_CLASP_1    1
#define MSG_TYPE_BOOK_CLASP_2    2
#define MSG_TYPE_BOOK_ELEGANT_1  3
#define MSG_TYPE_BOOK_ELEGANT_2  4
#define MSG_TYPE_BOOK_QUARTO_1   5
#define MSG_TYPE_BOOK_QUARTO_2   6
#define MSG_TYPE_BOOK_SPELL_EVOKER    7
#define MSG_TYPE_BOOK_SPELL_PRAYER    8
#define MSG_TYPE_BOOK_SPELL_PYRO      9
#define MSG_TYPE_BOOK_SPELL_SORCERER  10
#define MSG_TYPE_BOOK_SPELL_SUMMONER  11

/* card messages subtypes*/
#define MSG_TYPE_CARD_SIMPLE_1    1
#define MSG_TYPE_CARD_SIMPLE_2    2
#define MSG_TYPE_CARD_SIMPLE_3    3
#define MSG_TYPE_CARD_ELEGANT_1   4
#define MSG_TYPE_CARD_ELEGANT_2   5
#define MSG_TYPE_CARD_ELEGANT_3   6
#define MSG_TYPE_CARD_STRANGE_1   7
#define MSG_TYPE_CARD_STRANGE_2   8
#define MSG_TYPE_CARD_STRANGE_3   9
#define MSG_TYPE_CARD_MONEY_1     10
#define MSG_TYPE_CARD_MONEY_2     11
#define MSG_TYPE_CARD_MONEY_3     12

/* Paper messages subtypes */
#define MSG_TYPE_PAPER_NOTE_1       1
#define MSG_TYPE_PAPER_NOTE_2       2
#define MSG_TYPE_PAPER_NOTE_3       3
#define MSG_TYPE_PAPER_LETTER_OLD_1 4
#define MSG_TYPE_PAPER_LETTER_OLD_2 5
#define MSG_TYPE_PAPER_LETTER_NEW_1 6
#define MSG_TYPE_PAPER_LETTER_NEW_2 7
#define MSG_TYPE_PAPER_ENVELOPE_1   8
#define MSG_TYPE_PAPER_ENVELOPE_2   9
#define MSG_TYPE_PAPER_SCROLL_OLD_1 10
#define MSG_TYPE_PAPER_SCROLL_OLD_2 11
#define MSG_TYPE_PAPER_SCROLL_NEW_1 12
#define MSG_TYPE_PAPER_SCROLL_NEW_2 13
#define MSG_TYPE_PAPER_SCROLL_MAGIC 14

/* road signs messages subtypes */
#define MSG_TYPE_SIGN_BASIC         1
#define MSG_TYPE_SIGN_DIR_LEFT      2
#define MSG_TYPE_SIGN_DIR_RIGHT     3
#define MSG_TYPE_SIGN_DIR_BOTH      4

/* stones and monument messages */
#define MSG_TYPE_MONUMENT_STONE_1      1
#define MSG_TYPE_MONUMENT_STONE_2      2
#define MSG_TYPE_MONUMENT_STONE_3      3
#define MSG_TYPE_MONUMENT_STATUE_1     4
#define MSG_TYPE_MONUMENT_STATUE_2     5
#define MSG_TYPE_MONUMENT_STATUE_3     6
#define MSG_TYPE_MONUMENT_GRAVESTONE_1 7
#define MSG_TYPE_MONUMENT_GRAVESTONE_2 8
#define MSG_TYPE_MONUMENT_GRAVESTONE_3 9
#define MSG_TYPE_MONUMENT_WALL_1       10
#define MSG_TYPE_MONUMENT_WALL_2       11
#define MSG_TYPE_MONUMENT_WALL_3       12

/*some readable flags*/

/* dialog messsage */
#define MSG_TYPE_DIALOG_NPC            1       /*A message from the npc */
#define MSG_TYPE_DIALOG_ANSWER         2       /*One of possible answers */
#define MSG_TYPE_DIALOG_ANSWER_COUNT   3       /*Number of possible answers */

/* admin messages */
#define MSG_TYPE_ADMIN_RULES           1
#define MSG_TYPE_ADMIN_NEWS            2

/**
 * Maximum distance a player may hear a sound from.
 * This is only used for client/server sound and say. If the sound source
 * on the map is farther away than this, we don't sent it to the client.
 */
#define MAX_SOUND_DISTANCE 16

#define LOG_CHANNEL  "log"               // the plain and ugly standard server log
#define INFO_CHANNEL "info"              // lower_left box
#define SAY_CHANNEL  "say"
#define CHAT_CHANNEL "chat"
#define MSG_CHANNEL(name)  ("c/" name) // predefined channel defined in lib/cf.pm %CHANNEL

/* The following are the color flags passed to new_draw_info.
 *
 * We also set up some control flags
 *
 * NDI = New Draw Info
 */

/* Color specifications - note these match the order in xutil.c */
/* Note 2:  Black, the default color, is 0.  Thus, it does not need to
 * be implicitly specified.
 */
#define NDI_BLACK	0
#define NDI_WHITE	1
#define NDI_NAVY	2
#define NDI_RED		3
#define NDI_ORANGE	4
#define NDI_BLUE	5	/* Actually, it is Dodger Blue */
#define NDI_DK_ORANGE	6	/* DarkOrange2 */
#define NDI_GREEN	7	/* SeaGreen */
#define NDI_LT_GREEN	8	/* DarkSeaGreen,  which is actually paler */
                                /* Than seagreen - also background color */
#define NDI_GREY	9
#define NDI_BROWN	10	/* Sienna */
#define NDI_GOLD	11
#define NDI_TAN		12	/* Khaki */

#define NDI_MAX_COLOR	12	/* Last value in */
#define NDI_COLOR_MASK	0x1f	/* Gives lots of room for expansion - we are */
                                /* using an int anyways, so we have the space */
                                /* to still do all the flags */

#define NDI_REPLY       0x20    // is a direct reply to a user command
#define NDI_NOCREATE	0x40    // do not create a tab if one doesn't exist
#define NDI_CLEAR	0x80	// clear tab/message area before output, if sensible
#define NDI_CLIENT_MASK 0xff    // what the client is allowed to see

#define NDI_UNIQUE	0x1000	/* Print this out immediately, don't buffer */
#define NDI_ALL		0x2000	/* Inform all players of this message */
#define NDI_DEF         0x4000  // ignore colour for channel protocol
#define NDI_VERBATIM	0x8000  // do not expand cfpod, send text verbatim

#endif /* DEFINE_H */

