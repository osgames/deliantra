/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

/* 'recipe' and 'recipelist' are used by the alchemy code */
typedef struct recipestruct
{
  shstr title;                  /* distinguishing name of product */
  //TODO: the next two should be a shstr vector
  size_t arch_names;            /* the size of the arch_name[] array */
  char **arch_name;             /* the possible archetypes of the final product made */
  int chance;                   /* chance that recipe for this item will appear
                                 * in an alchemical grimore */
  int diff;                     /* alchemical dfficulty level */
  int exp;                      /* how much exp to give for this formulae */
  int index;                    /* an index value derived from formula ingredients */
  int transmute;                /* if defined, one of the formula ingredients is
                                 * used as the basis for the product object */
  int yield;                    /* The maximum number of items produced by the recipe */
  linked_char *ingred;          /* comma delimited list of ingredients */
  struct recipestruct *next;
  shstr keycode;                /* keycode needed to use the recipe */
  shstr skill;                  /* skill name used to make this recipe */
  shstr cauldron;               /* the arch of the cauldron/workbench used to house the
                                 * formulae. */
} recipe;

typedef struct recipeliststruct
{
  int total_chance;
  int number;                   /* number of recipes in this list */
  struct recipestruct *items;   /* pointer to first recipe in this list */
  struct recipeliststruct *next;        /* pointer to next recipe list */
} recipelist;
