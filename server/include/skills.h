/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2003 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#ifndef SKILLS_H__
#define SKILLS_H__

/* This list is just a subtype <-> skill (code wise) in the
 * server translation.  In theory, the processing of the different
 * skills could be done via strncmp
 * This list doesn't really try to identify what the skills do.
 * The order of this list has no special meaning.  0 is not used
 * to denote improperly set objects.
 */
enum
{
  SK_NONE = 0,
# define def(uc, flags) SK_ ## uc,
#  include "skillinc.h"
# undef def
  NUM_SKILLS,
};

enum
{
  SF_COMBAT       = 0x01, // skill can be used in direct attack combat (hth or weapon-based, not ranged)
  SF_RANGED       = 0x02, // skill can be used for ranged attacks (not combat)

  SF_USE          = 0x04, // skill can be used directly, directionless
  SF_NEED_ITEM    = 0x08, // skill *requires* some object in the weapon slot
  SF_AUTARK       = 0x20, // skill is independent, cannot be readied or used by items, no chosen skill

  SF_MANA         = 0x40, // skill requires a mana-consuming spell, but cannot be readied on its own
  SF_GRACE        = 0x80, // skill can use a grace-consuming spell, or can be used on its own
};

/* This is used in the exp functions - basically what to do if
 * the player doesn't have the skill he should get exp in.
 */

#define SK_EXP_ADD_SKILL	0   /* Give the player the skill */
#define SK_EXP_TOTAL		1   /* Give player exp to total, no skill */
#define SK_EXP_NONE		2   /* Player gets nothing */
#define SK_SUBTRACT_SKILL_EXP	3   /* Used when removing exp */
#define SK_EXP_SKILL_ONLY       4   /* Player gets only skill experience */

#define USING_SKILL(op, skill)  ((op)->chosen_skill && (op)->chosen_skill->subtype == skill)

#define IS_COMBAT_SKILL(num) (skill_flags [num] & SF_COMBAT)
#define IS_RANGED_SKILL(num) (skill_flags [num] & SF_RANGED)

/* Like IS_COMBAT_SKILL above, but instead this is used to determine
 * how many mana points the player has.
 */
#define IS_MANA_SKILL(num) (skill_flags [num] & SF_MANA)

/* Currently only one of these, but put the define here to make
 * it easier to expand it in the future */
#define IS_GRACE_SKILL(num) (skill_flags [num] & SF_GRACE)

extern const uint8_t skill_flags[NUM_SKILLS];

// would like to use object_vector here...
extern vector<object_ptr> skillvec;
void add_skill_archetype (object *o);
#define SKILL_INDEX(o) (o)->arch->cached_sp

#endif

