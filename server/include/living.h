/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#ifndef LIVING_H
#define LIVING_H

#include <cassert>

#include <util.h>

//+GPL

// corresponds to some arrays in common/living.C
enum {
  STR,
  DEX,
  CON,
  INT,
  WIS,
  POW,
  CHA,
  NUM_STATS
};

/* Changed from NO_STAT to NO_STAT_VAL to fix conlfict on
 * AIX systems
 */
#define NO_STAT_VAL 99    /* needed by skills code -b.t. */

extern const char *const attacks[NROFATTACKS];

extern const float cha_bonus[MAX_STAT + 1];
extern const int dex_bonus[MAX_STAT + 1];
extern const int thaco_bonus[MAX_STAT + 1];
extern const int turn_bonus[MAX_STAT + 1];
extern const int max_carry[MAX_STAT + 1];
extern const int dam_bonus[MAX_STAT + 1];
extern const int learn_spell[];
extern const char *const restore_msg[NUM_STATS];
extern const char *const statname[NUM_STATS];
extern const char *const short_stat_name[NUM_STATS];
extern const char *const lose_msg[NUM_STATS];
extern const float speed_bonus[MAX_STAT + 1];
extern const uint32 weight_limit[MAX_STAT + 1];
extern const int cleric_chance[MAX_STAT + 1];
extern const int fear_bonus[MAX_STAT + 1];

/* Mostly used by "alive" objects */
INTERFACE_CLASS (living)
struct living
{
  sint64 ACC (RW, exp);		/* Experience. */
  sint32 ACC (RW, food);	/* How much food in stomach.  0 = starved. */

  sint8	 ACC (RW, Str), ACC (RW, Dex), ACC (RW, Con), ACC (RW, Int), ACC (RW, Wis), ACC (RW, Pow), ACC (RW, Cha);
  sint8	 ACC (RW, wc), ACC (RW, ac);	/* Weapon Class and Armour Class */
  sint8	 ACC (RW, luck);	/* Affects thaco and ac from time to time */
  sint16 ACC (RW, dam);		/* How much damage this object does when hitting */

  sint16 ACC (RW, hp);		/* Hit Points. */
  sint16 ACC (RW, maxhp);

  sint16 ACC (RW, sp);		/* Spell points.  Used to cast mage spells. */
  sint16 ACC (RW, maxsp);	/* Max spell points. */

  sint16 ACC (RW, grace);	/* Grace.  Used to invoke clerical prayers. */
  sint16 ACC (RW, maxgrace);	/* Grace.  Used to invoke clerical prayers. */

//-GPL

  // this method enforces field ordering and should not normally result in
  // any code being generated. strictly speaking, C++ compilers may reorder
  // stat fields, so check for it and declare those compilers unsupported.
  void check_ordering () const
  {
    assert ((sint8 *)&Str - (sint8 *)&Str == STR);
    assert ((sint8 *)&Dex - (sint8 *)&Str == DEX);
    assert ((sint8 *)&Con - (sint8 *)&Str == CON);
    assert ((sint8 *)&Int - (sint8 *)&Str == INT);
    assert ((sint8 *)&Wis - (sint8 *)&Str == WIS);
    assert ((sint8 *)&Pow - (sint8 *)&Str == POW);
    assert ((sint8 *)&Cha - (sint8 *)&Str == CHA);
  }

  sint8 &stat (int index)
  {
    check_ordering ();
    return ((sint8 *)&Str)[index];
  }

  sint8 stat (int index) const
  {
    check_ordering ();
    return ((const sint8 *)&Str)[index];
  }
};

#endif

