/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2001 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#ifndef OBJECT_H
#define OBJECT_H

#include "cfperl.h"
#include "shstr.h"

//+GPL

typedef int tag_t;

// also see common/item.C
enum
{
# define def(name, use, nonuse) body_ ## name,
#  include "slotinc.h"
# undef def
  NUM_BODY_LOCATIONS
};

/* See common/item.c */

struct Body_Locations
{
  const char *name;		/* Short name/identifier */
  keyword     kw;		/* Name used to load/save it to disk */
  const char *use_name;		/* Name used when describing an item we can use */
  const char *nonuse_name;	/* Name to describe objects we can't use */
};

extern Body_Locations body_locations[NUM_BODY_LOCATIONS];

// for each set of directions (1 == up, 2 == right, 4 == down, 8 == left)
// contains the wall suffix (0, 1_3, 1_4 and so on).
extern const char *wall_suffix[16];

#define NUM_COINS 4             /* number of coin types */
extern const char *const coins[NUM_COINS + 1];

// restart server when object_count reaches this value
#define RESTART_COUNT 0xe0000000

/*
 * Each object (this also means archetypes!) could have a few of these
 * "dangling" from it; this could also end up containing 'parse errors'.
 *
 * key and value are shared-strings.
 *
 * Please use kv_get/kv_set/kv_del from object rather than
 * accessing the list directly.
 * Exception is if you want to walk this list for some reason.
 */
struct key_value : slice_allocated
{
  key_value *next; // must be first element
  shstr key, value;
};

// "crossfires version of a perl hash."
struct key_values
{
  key_value *first; // must be first element

  bool empty() const
  {
    return !first;
  }

  void clear ();
  shstr_tmp get (shstr_tmp key) const;
  void del (shstr_tmp key);
  void set (shstr_tmp key, shstr_tmp value);

  void add (shstr_tmp key, shstr_tmp value); // liek set, but doesn't check for duplicates
  void reverse (); // reverses the ordering, to be used after loading an object
  key_values &operator =(const key_values &kv);

  // custom extra fields management
  struct access_proxy
  {
    key_values &kv;
    shstr_tmp key;

    access_proxy (key_values &kv, shstr_tmp key)
    : kv (kv), key (key)
    {
    }

    const access_proxy &operator =(shstr_tmp value) const
    {
      kv.set (key, value);
      return *this;
    }

    operator const shstr_tmp () const { return kv.get (key); }
    operator const char     *() const { return kv.get (key); }

  private:
    void operator =(int);
  };

  const access_proxy operator [](shstr_tmp key)
  {
    return access_proxy (*this, key);
  }
};

//-GPL

struct UUID
{
  uint64 seq;

  enum { MAX_LEN = 3 + 16 + 1 + 1 }; // <1. + hex + > + \0

  static UUID cur; // last uuid generated
  static void init ();
  static UUID gen ();

  UUID () { }
  UUID (uint64 seq) : seq(seq) { }
  operator uint64() { return seq; }
  void operator =(uint64 seq) { this->seq = seq; }

  bool parse (const char *s);
  char *append (char *buf) const;
  char *c_str () const;
};

//+GPL

/* Definition for WILL_APPLY values.  Replaces having harcoded values
 * sprinkled in the code.  Note that some of these also replace fields
 * that were in the can_apply area.  What is the point of having both
 * can_apply and will_apply?
 */
#define WILL_APPLY_HANDLE       0x01
#define WILL_APPLY_TREASURE     0x02
#define WILL_APPLY_EARTHWALL    0x04
#define WILL_APPLY_DOOR         0x08
#define WILL_APPLY_FOOD         0x10

struct body_slot
{
  signed char used:4;	/* Calculated value based on items equipped */
  signed char info:4;	/* body info as loaded from the file */
};

typedef struct oblnk
{				/* Used to link together several objects */
  object_ptr ob;
  struct oblnk *next;
} objectlink;

typedef struct oblinkpt
{				/* Used to link together several object links */
  struct oblnk *link;
  struct oblinkpt *next;
  shstr id;			/* Used as connected value in buttons/gates */
} oblinkpt;

INTERFACE_CLASS (object)
// these are being copied
struct object_copy : attachable
{
  sint16 ACC (RW, x), ACC (RW, y);	/* Position in the map for this object */

  uint8 ACC (RW, type);		/* PLAYER, BULLET, etc.  See define.h */
  uint8 ACC (RW, subtype);	/* subtype of object */
  sint8 ACC (RW, direction);	/* Means the object is moving that way. */
  sint8 ACC (RW, facing);	/* Object is oriented/facing that way. */

  shstr ACC (RW, name);		/* The name of the object, obviously... */
  shstr ACC (RW, name_pl);	/* The plural name of the object */
  shstr ACC (RW, title);	/* Of foo, etc */
  shstr ACC (RW, race);		/* human, goblin, dragon, etc */
  shstr ACC (RW, slaying);	/* Which race to do double damage to */
				/* If this is an exit, this is the filename */

  typedef bitset<NUM_FLAGS> flags_t;
  flags_t flag;                 /* various flags */
#if FOR_PERL
  bool ACC (RW, flag[NUM_FLAGS]);
#endif

  materialtype_t *material;	// What material this object consists of //TODO, make perl-accessible
  shstr ACC (RW, skill);	/* Name of the skill this object uses/grants */
  object_ptr ACC (RW, owner);	/* Pointer to the object which controls this one */
  object_ptr ACC (RW, enemy);	/* Monster/player to follow even if not closest */
  object_ptr ACC (RW, attacked_by);	/* This object start to attack us! only player & monster */
  object_ptr ACC (RW, chosen_skill);	/* the skill chosen to use */
  object_ptr ACC (RW, spellitem);	/* Spell ability monster is choosing to use */
  object_ptr ACC (RW, spell);	/* Spell that was being cast */
  object_ptr ACC (RW, current_weapon);	/* Pointer to the weapon currently used */
  arch_ptr ACC (RW, arch);	/* Pointer to archetype */
  arch_ptr ACC (RW, other_arch);/* Pointer used for various things - mostly used for what */

  float ACC (RW, speed);	/* The overall speed of this object */
  float ACC (RW, speed_left);	/* How much speed is left to spend this round */

  sint32 ACC (RW, nrof);	/* How many of the objects */
  /* This next big block is basically used for monsters and equipment */
  uint16 ACC (RW, client_type);	/* Public type information.  see doc/Developers/objects */
  sint16 ACC (RW, resist[NROFATTACKS]);	/* Resistance adjustments for attacks */

  uint32 ACC (RW, attacktype);	/* Bitmask of attacks this object does */
  uint32 ACC (RW, path_attuned);/* Paths the object is attuned to */
  uint32 ACC (RW, path_repelled);	/* Paths the object is repelled from */
  uint32 ACC (RW, path_denied);	/* Paths the object is denied access to */

  uint16 ACC (RW, materials);	/* What materials this object consists of */
  sint8 ACC (RW, magic);	/* Any magical bonuses to this item */
  uint8 ACC (RW, state);	/* How the object was last drawn (animation) */
  sint32 ACC (RW, value);	/* How much money it is worth (or contains) */

  /* Note that the last_.. values are sometimes used for non obvious
   * meanings by some objects, eg, sp penalty, permanent exp.
   */
  sint16 ACC (RW, last_heal);	/* Last healed. Depends on constitution */
  sint16 ACC (RW, last_sp);	/* As last_heal, but for spell points */
  sint16 ACC (RW, last_grace);	/* as last_sp, except for grace */
  sint16 ACC (RW, last_eat);	/* How long since we last ate */

  sint16 ACC (RW, invisible);	/* How much longer the object will be invis */
  sint16 ACC (RW, level);	/* Level of creature or object */

  uint8 ACC (RW, pick_up);	/* See crossfire.doc */
  sint8 ACC (RW, gen_sp_armour);/* sp regen penalty this object has (was last_heal) */
  sint8 ACC (RW, glow_radius);	/* indicates the glow radius of the object */
  uint8 ACC (RW, weapontype);	/* type of weapon */

  body_slot slot [NUM_BODY_LOCATIONS];

  faceidx ACC (RW, face);	/* the graphical face */

  faceidx ACC (RW, sound);      /* the sound face */
  faceidx ACC (RW, sound_destroy); /* played on destroy */

  sint32 ACC (RW, weight);	/* Attributes of the object */
  sint32 ACC (RW, weight_limit);/* Weight-limit of object */

  sint32 ACC (RW, carrying);	/* How much weight this object contains, must be 0 if nrof == 0 */

  sint64 ACC (RW, perm_exp);	/* Permanent exp */
  living ACC (RO, stats);	/* Str, Con, Dex, etc */
  /* See the pod/objects.pod for more info about body locations */

  /* Following mostly refers to fields only used for monsters */

  /* Spell related information, may be useful elsewhere
   * Note that other fields are used - these files are basically
   * only used in spells.
   */
  sint16 ACC (RW, duration);	/* How long the spell lasts */
  uint8 ACC (RW, casting_time); /* time left before spell goes off */
  uint8 ACC (RW, duration_modifier);	/* how level modifies duration */

  uint8 ACC (RW, dam_modifier);	/* How going up in level effects damage */
  sint8 ACC (RW, range);	/* Range of the spell */
  uint8 ACC (RW, range_modifier);	/* How going up in level effects range  */
  sint8 ACC (RW, item_power);	/* power rating of the object */

  uint8 ACC (RW, run_away);	/* Monster runs away if it's hp goes below this percentage. */
  MoveType ACC (RW, move_type);	/* Type of movement this object uses */
  MoveType ACC (RW, move_block);/* What movement types this blocks */
  MoveType ACC (RW, move_allow);/* What movement types explicitly allowd */

  MoveType ACC (RW, move_on);	/* Move types affected moving on to this space */
  MoveType ACC (RW, move_off);	/* Move types affected moving off this space */
  MoveType ACC (RW, move_slow);	/* Movement types this slows down */

  // 8 free bits

  //float ACC (RW, expmul) = 1.0;	/* needed experience = (calc_exp*expmul) - means some */
  //                                    /* races/classes can need less/more exp to gain levels */
  constexpr static const float expmul = 1.0;//D
  float ACC (RW, move_slow_penalty);	/* How much this slows down the object */

  /* Following are values used by any object */
  /* this objects turns into or what this object creates */
  treasurelist *ACC (RW, randomitems);	/* Items to be generated */

  uint8 ACC (RW, last_anim);	/* last sequence used to draw face */
  uint8 ACC (RW, smoothlevel);	/* how to smooth this square around */
  uint8 ACC (RW, will_apply);	/* See crossfire.doc */
  uint8 ACC (RW, anim_speed);	/* ticks between animation-frames */
  uint16 ACC (RW, animation_id);/* An index into the animation array */
  uint16 ACC (RW, cached_grace);/* cached grace points used for a spell, used by esrv_update_spells */

  uint16 ACC (RW, cached_eat);  /* cached food, used by esrv_update_spells */
  uint16 ACC (RW, cached_sp);   /* cached spell points used for a spell, used by esrv_update_spells */
  /* allows different movement patterns for attackers */
  uint8 ACC (RW, move_status);	/* What stage in attack mode */
  uint8 ACC (RW, attack_movement);/* What kind of attack movement */

  //16+ free bits

  // rarely-accessed members should be at the end
  shstr ACC (RW, tag);          // a tag used to tracking this object
  shstr ACC (RW, msg);		/* If this is a book/sign/magic mouth/etc */
  shstr ACC (RW, lore);		/* Obscure information about this object, */
				/* To get put into books and the like. */
  shstr ACC (RW, custom_name);	/* Custom name assigned by player */
};

const_utf8_string query_weight (const object *op);
const_utf8_string query_short_name (const object *op);
const_utf8_string query_name (const object *op);
const_utf8_string query_base_name (const object *op, int plural);
sint64 query_cost (const object *tmp, object *who, int flag);
const char *query_cost_string (const object *tmp, object *who, int flag);

int change_ability_duration (object *spell, object *caster);
int min_casting_level (object *caster, object *spell);
int casting_level (object *caster, object *spell);
sint16 SP_level_spellpoint_cost (object *caster, object *spell, int flags);
int SP_level_dam_adjust (object *caster, object *spob);
int SP_level_duration_adjust (object *caster, object *spob);
int SP_level_range_adjust (object *caster, object *spob);

struct freelist_item
{
  freelist_item *next;
  uint32_t count;
};

struct object : object_copy
{
  // These variables are not changed by ->copy_to
  maptile *ACC (RW, map);	/* Pointer to the map in which this object is present */

  UUID ACC (RW, uuid);          // Unique Identifier, survives saves etc.
  uint32_t ACC (RO, count);
  object_vector_index ACC (RO, index);	// index into objects
  object_vector_index ACC (RO, active); // index into actives

  player_ptr ACC (RW, contr);	/* Pointer to the player which control this object, ALWAYS set *iff* type == PLAYER */

  object *ACC (RW, below);	/* Pointer to the object stacked below this one */
  object *ACC (RW, above);	/* Pointer to the object stacked above this one */
                                /* Note: stacked in the *same* environment */
  object *inv;			/* Pointer to the first object in the inventory */

  //TODO: container must move into client
  object_ptr ACC (RW, container);/* Currently opened container.  I think this
				 * is only used by the player right now.
				 */
  object *ACC (RW, env);	/* Pointer to the object which is the environment.
				 * This is typically the container that the object is in.
				 */
  object *ACC (RW, more);	/* Pointer to the rest of a large body of objects */
  object *head;			/* Points to the main object of a large body */ // NO ACC, perl semantics are different

  MTH void set_flag (int flagnum)
  {
    flag [flagnum] = true;
  }

  MTH void clr_flag (int flagnum)
  {
    flag [flagnum] = false;
  }

  // extra key value pairs
  key_values kv;

//-GPL

  bool parse_kv (object_thawer &f); // parse kv pairs, (ab-)used by archetypes, which should not exist at all
  MTH void post_load_check ();          // do some adjustments after parsing
  static object *read (object_thawer &f, maptile *map = 0); // map argument due to toal design bogosity, must go.
  bool write (object_freezer &f);

  MTH static object *create ();
  const mapxy &operator =(const mapxy &pos);
  MTH void copy_to (object *dst);
  MTH object *clone (); // create + copy_to a single object
  MTH object *deep_clone (); // copy whole more chain and inventory
  void do_destroy ();
  void gather_callbacks (AV *&callbacks, event_type event) const;
  MTH void destroy ();
  MTH void drop_and_destroy ()
  {
    destroy_inv (true);
    destroy ();
  }

  // recursively destroy all objects in inventory, optionally dropping them to the ground instead
  MTH void destroy_inv (bool drop_to_ground = false);
  MTH void destroy_inv_fast (); // like destroy_inv (false), but only works when *this is destroyed, too
  MTH object *insert (object *item); // insert into inventory
  MTH void play_sound (faceidx sound) const;
  MTH void say_msg (const_utf8_string msg) const;

  void do_remove ();
  MTH void remove ()
  {
    if (!flag [FLAG_REMOVED])
      do_remove ();
  }

  MTH bool blocked (maptile *m, int x, int y) const;

  void move_to (const mapxy &pos)
  {
    remove ();
    *this = pos;
    insert_at (this, this);
  }

  // high-level move method.
  // object op is trying to move in direction dir.
  // originator is typically the same as op, but
  // can be different if originator is causing op to
  // move (originator is pushing op)
  // returns 0 if the object is not able to move to the
  // desired space, 1 otherwise (in which case we also
  // move the object accordingly.  This function is
  // very similiar to move_object.
  int move (int dir, object *originator);

  int move (int dir)
  {
    return move (dir, this);
  }

  // changes move_type to a new value - handles move_on/move_off effects
  MTH void change_move_type (MoveType mt);

  static bool can_merge_slow (object *op1, object *op2);

  // this is often used in time-critical code, so optimise
  MTH static bool can_merge (object *op1, object *op2)
  {
    return op1->value == op2->value
        && op1->name  == op2->name
        && can_merge_slow (op1, op2);
  }

  MTH void set_owner (object_ornull *owner);
  MTH void set_speed (float speed);
  MTH void set_glow_radius (sint8 rad);

  MTH void open_container (object *new_container);
  MTH void close_container ()
  {
    open_container (0);
  }

  // potential future accessor for "container"
  MTH object *container_ () const
  {
    return container;
  }

  MTH bool is_open_container () const
  {
    // strangely enough, using ?: here causes code to inflate
    return type == CONTAINER
        && ((env && env->container_ () == this)
            || (!env && flag [FLAG_APPLIED]));
  }

  MTH object *find_spell (const_utf8_string prefix) const;

  MTH object *force_find (shstr_tmp name);
  MTH void force_set_timer (int duration);
  MTH object *force_add (shstr_tmp name, int duration = 0);

  oblinkpt *find_link () const;
  MTH void add_link (maptile *map, shstr_tmp id);
  MTH void remove_link ();

  // overwrite the attachable should_invoke function with a version that also checks ev_want_type
  bool should_invoke (event_type event)
  {
    return ev_want_event [event] || ev_want_type [type] || cb;
  }

  MTH void instantiate ();

  // recalculate all stats
  MTH void update_stats ();
  MTH void roll_stats ();
  MTH void swap_stats (int a, int b);
  MTH void add_statbonus ();
  MTH void remove_statbonus ();
  MTH void drain_stat ();
  MTH void drain_specific_stat (int deplete_stats);
  MTH void change_luck (int value);

  // info must hold 256 * 3 bytes currently
  const_utf8_string debug_desc (char *info) const;
  MTH const_utf8_string debug_desc () const; // uses at least 3 round-robin buffers
  const_utf8_string flag_desc (char *desc, int len) const;

  MTH bool decrease (sint32 nr = 1); // returns true if anything is left
  MTH object *split (sint32 nr = 1); // return 0 on failure

  MTH int number_of () const
  {
    return nrof ? nrof : 1;
  }

  MTH weight_t total_weight () const
  {
    return sint64 (weight + carrying) * number_of ();
  }

  MTH void update_weight ();

  // return the dominant material of this item, always return something
  const materialtype_t *dominant_material () const
  {
    return material;
  }

  // return the volume of this object in cm³
  MTH volume_t volume () const
  {
    return (volume_t)total_weight ()
           * 1024 // 1000 actually
           * (type == CONTAINER ? 128 : 1)
           / dominant_material ()->density; // ugh, division
  }

  MTH bool is_arch   () const { return this == (const object *)(const archetype *)arch; }

  MTH bool is_wiz    () const { return flag [FLAG_WIZ]; }
  MTH bool is_weapon () const { return type == ARROW || type == BOW || type == WEAPON; }
  MTH bool is_armor  () const { return type == ARMOUR  || type == SHIELD || type == HELMET
                                    || type == CLOAK   || type == BOOTS  || type == GLOVES
                                    || type == BRACERS || type == GIRDLE; }
  MTH bool is_alive  () const { return (type == PLAYER
                                        || flag [FLAG_MONSTER]
                                        || (flag [FLAG_ALIVE] && !flag [FLAG_GENERATOR] && type != DOOR))
                                       && !flag [FLAG_IS_A_TEMPLATE]; }
  MTH bool is_arrow  () const { return type == ARROW
                                       || (type == SPELL_EFFECT
                                           && (subtype == SP_BULLET || subtype == SP_MAGIC_MISSILE)); }
  MTH bool is_range  () const { return type == BOW || type == ROD || type == WAND || type == HORN; }

  MTH bool is_dragon () const;

  MTH bool is_immunity () const { return invisible && type == SIGN; }

  MTH bool has_active_speed () const { return speed >= MIN_ACTIVE_SPEED; }

  // temporary: wether the object can be saved in a map file
  // contr => is a player
  // head  => only save head of a multitile object
  // owner => can not reference owner yet
  MTH bool can_map_save () const { return !head && (!owner || owner->contr) && !contr && !flag [FLAG_NO_MAP_SAVE]; }

  /* This return true if object has still randomitems which
   * could be expanded.
   */
  MTH bool has_random_items () const { return randomitems && !flag [FLAG_IS_A_TEMPLATE]; }

  static bool msg_has_dialogue (const char *msg) { return *msg == '@'; }

  MTH bool has_dialogue () const { return msg_has_dialogue (&msg); }

  /* need_identify returns true if the item should be identified.  This
   * function really should not exist - by default, any item not identified
   * should need it.
   */
  MTH bool need_identify () const;

  // returns the outermost owner, never returns 0
  MTH object *outer_owner ()
  {
    object *op;

    for (op = this; op->owner; op = op->owner)
      ;

    return op;
  }

  // returns the outermost environment, never returns 0
  MTH object *outer_env_or_self () const
  {
    const object *op;

    for (op = this; op->env; op = op->env)
      ;

    return const_cast<object *>(op);
  }

  // returns the outermost environment, may return 0
  MTH object *outer_env () const
  {
    return env ? outer_env_or_self () : 0;
  }

  // returns the player that has this object in his inventory, or 0
  // we assume the player is always the outer env
  MTH object *in_player () const
  {
    object *op = outer_env_or_self ();

    return op->type == PLAYER ? op : 0;
  }

  // "temporary" helper function
  MTH object *head_ () const
  {
    return head ? head : const_cast<object *>(this);
  }

  MTH bool is_head () const
  {
    return head_ () == this;
  }

  MTH bool is_on_map () const
  {
    return !env && !flag [FLAG_REMOVED];
  }

  MTH bool is_inserted () const
  {
    return !flag [FLAG_REMOVED];
  }

  MTH bool is_player () const
  {
    return !!contr;
  }

  /* elmex: this method checks whether the object is in a shop */
  MTH bool is_in_shop () const;

  MTH bool affects_los () const
  {
    return glow_radius || flag [FLAG_BLOCKSVIEW];
  }

  MTH bool has_carried_lights () const
  {
    return glow_radius;
  }

  // returns the player that can see this object, if any
  MTH object *visible_to () const;

  MTH std::string long_desc        (object *who = 0); // query_name . " " . describe
  MTH std::string describe_monster (object *who = 0);
  MTH std::string describe_item    (object *who = 0);
  MTH std::string describe         (object *who = 0); // long description, without name

  MTH const_utf8_string query_weight     () { return ::query_weight     (this); }
  MTH const_utf8_string query_name       () { return ::query_name       (this); }
  MTH const_utf8_string query_short_name () { return ::query_short_name (this); }
  MTH const_utf8_string query_base_name  (bool plural) { return ::query_base_name (this, plural); }

  // If this object has no extra parts but should have them,
  // add them, effectively expanding heads into multipart
  // objects. This method only works on objects not inserted
  // anywhere.
  MTH void expand_tail ();

  MTH void create_treasure (treasurelist *tl, int flags = 0);

  // makes sure the player has the named skill,
  // and also makes it innate if can_use is true.
  // returns the new skill or 0 if no such skill exists.
  MTH object *give_skill (shstr_cmp name, bool can_use = false);
  MTH void become_follower (object *new_god);

  // insert object at same map position as 'where'
  // handles both inventory and map "positions"
  MTH object *insert_at (object *where, object *originator = 0, int flags = 0);
  // check whether we can put this into the map, respect max_nrof, max_volume, max_items
  MTH bool can_drop_at (maptile *m, int x, int y, object *originator = 0);
  MTH void drop_unpaid_items ();

  MTH void activate   ();
  MTH void deactivate ();
  MTH void activate_recursive   ();
  MTH void deactivate_recursive ();

  // prefetch and activate the surrounding area
  MTH void prefetch_surrounding_maps ();

  // set the given flag on all objects in the inventory recursively
  MTH void set_flag_inv (int flag, int value = 1);

  void enter_exit (object *exit); // perl
  MTH bool enter_map (maptile *newmap, int x, int y);
  void player_goto (const_utf8_string path, int x, int y); // only for players
  MTH bool apply (object *ob, int aflags = AP_APPLY); // ob may be 0

  MTH object *mark () const;
  MTH void splay_marked ();

  // returns the mapspace this object is in
  mapspace &ms () const;

  // fully recursive iterator
  struct iterator_base
  {
    object *item;

    iterator_base (object *container)
    : item (container)
    {
    }

    operator object *() const { return item; }

    object *operator ->() const { return  item; }
    object &operator * () const { return *item; }
  };

  MTH unsigned int random_seed () const
  {
    return (unsigned int)uuid.seq;
  }

  // depth-first recursive iterator
  struct depth_iterator : iterator_base
  {
    depth_iterator (object *container);
    void next ();
    object *operator ++(   ) { next (); return item; }
    object *operator ++(int) { object *i = item; next (); return i; }
  };

  object *begin ()
  {
    return this;
  }

  object *end ()
  {
    return this;
  }

  /* This returns TRUE if the object is something that
   * a client might want to know about.
   */
  MTH bool client_visible () const
  {
    return !invisible && type != PLAYER;
  }

  // the client does nrof * this weight
  MTH sint32 client_weight () const
  {
    return weight + carrying;
  }

  MTH struct region *region () const;

  MTH void statusmsg (const_utf8_string msg, int color = NDI_BLACK);
  MTH void failmsg (const_utf8_string msg, int color = NDI_RED);
  void failmsgf (const_utf8_string format, ...); // always NDI_RED...

  MTH const_utf8_string query_inventory (object *who = 0, const_utf8_string indent = "");

  MTH const_octet_string ref () const; // creates and returns a consistent persistent object reference
  static object *deref (const_octet_string ref); // returns the object from the generated reference, if possible

  // make some noise with given item into direction dir,
  // currently only used for players to make them temporarily visible
  // when they are invisible.
  MTH void make_noise ();

  /* animation */
  MTH bool has_anim () const { return animation_id; }
  const animation &anim () const { return animations [animation_id]; }
  MTH faceidx get_anim_frame (int frame) const { return anim ().faces [frame]; }
  MTH void set_anim_frame (int frame) { face = get_anim_frame (frame); }
  /* anim_frames () returns the number of animations allocated.  The last
   * usuable animation will be anim_frames () - 1 (for example, if an object
   * has 8 animations, anim_frames () will return 8, but the values will
   * range from 0 through 7.
   */
  MTH int anim_frames  () const { return anim ().num_animations; }
  MTH int anim_facings () const { return anim ().facings; }

  MTH utf8_string as_string ();

  // low-level management, statistics, ...
  static uint32_t ACC (RW, object_count);
  static uint32_t ACC (RW, free_count);
  static uint32_t ACC (RW, create_count);
  static uint32_t ACC (RW, destroy_count);
  static freelist_item *freelist;
  MTH static void freelist_free (int count);

protected:
  void link   ();
  void unlink ();

  void do_delete ();

  object ();
  ~object ();

private:
  object &operator =(const object &);
  object (const object &);
};

// move this object to the top of its env's inventory to speed up
// searches for it.
static inline object *
splay (object *ob)
{
  if (ob->above && ob->env)
    {
      if (ob->above) ob->above->below = ob->below;
      if (ob->below) ob->below->above = ob->above;

      ob->above = 0;
      ob->below = ob->env->inv;
      ob->below->above = ob;
      ob->env->inv = ob;
    }

  return ob;
}

//+GPL

object *find_skill_by_name_fuzzy (object *who, const_utf8_string name);
object *find_skill_by_name (object *who, shstr_cmp sh);
object *find_skill_by_number (object *who, int skillno);

/*
 * The archetype structure is a set of rules on how to generate and manipulate
 * objects which point to archetypes.
 * This probably belongs in arch.h, but there really doesn't appear to
 * be much left in the archetype - all it really is is a holder for the
 * object and pointers.  This structure should get removed, and just replaced
 * by the object structure
 */

//-GPL

INTERFACE_CLASS (archetype)
struct archetype : object, zero_initialised
{
  static arch_ptr empty; // the empty_archetype
  MTH static void gc ();

  archetype (const_utf8_string name);
  ~archetype ();
  void gather_callbacks (AV *&callbacks, event_type event) const;

  MTH static archetype *find (const_utf8_string name);

  MTH void link   ();
  MTH void unlink ();

  MTH static object *get (const_utf8_string name); // (find() || singularity)->instance()
  MTH object *instance ();

  MTH void post_load_check ();          // do some adjustments after parsing

  object_vector_index ACC (RW, archid);         // index in archvector
  shstr ACC (RW, archname);			/* More definite name, like "generate_kobold" */

  sint8 ACC (RW, max_x);       /* extents, compared to the head (min_x, min_y should be zero, but aren't...) */

  // support for archetype loading
  static archetype *read (object_thawer &f);
  MTH static void commit_load (); // commit any objects loaded, resolves cyclic dependencies and more
  static void postpone_arch_ref (arch_ptr &ref, const_utf8_string other_arch); /* postpone  other_arch reference */

protected:
  void do_delete ();
};

// returns whether the object is a dragon player, which are often specialcased
inline bool
object::is_dragon () const
{
  return arch->race == shstr_dragon && is_player ();
}

inline void
object_freezer::put (const keyword_string k, archetype *v)
{
  if (expect_true (v))
    put (k, v->archname);
  else
    put (k);
}

typedef object_vector<object,    &object::index >    objectvec;
typedef object_vector<object,    &object::active>    activevec;
typedef object_vector<archetype, &archetype::archid> archvec;

extern objectvec objects;
extern activevec actives;
extern archvec   archetypes;

// "safely" iterate over inv in a way such that the current item is removable
// quite horrible, that's why its hidden in some macro
#define for_inv_removable(op,var)			\
  for (object *var, *next_ = (op)->inv; (var = next_), var && (next_ = var->below), var; )

#define for_all_objects(var)				\
  for (unsigned _i = 0; _i < objects.size (); ++_i)	\
    statementvar (object *, var, objects [_i])

#define for_all_actives(var)				\
  for (unsigned _i = 0; _i < actives.size (); ++_i)	\
    statementvar (object *, var, actives [_i])

#define for_all_archetypes(var)				\
  for (unsigned _i = 0; _i < archetypes.size (); ++_i)	\
    statementvar (archetype *, var, archetypes [_i])

//+GPL

/* Used by update_object to know if the object being passed is
 * being added or removed.
 */
#define UP_OBJ_INSERT   1
#define UP_OBJ_REMOVE   2
#define UP_OBJ_CHANGE   3
#define UP_OBJ_FACE     4	/* Only thing that changed was the face */

/* These are flags passed to insert_ob_in_map and
 * insert_ob_in_ob.  Note that all flags may not be meaningful
 * for both functions.
 * Most are fairly explanatory:
 * INS_NO_MERGE: don't try to merge inserted object with ones alrady
 *    on space.
 * INS_ABOVE_FLOOR_ONLY: Put object immediatly above the floor.
 * INS_NO_WALK_ON: Don't call check_walk_on against the
 *    originator - saves cpu time if you know the inserted object
 *    is not meaningful in terms of having an effect.
 * INS_ON_TOP: Always put object on top.  Generally only needed when loading
 *     files from disk and ordering needs to be preserved.
 * INS_BELOW_ORIGINATOR: Insert new object immediately below originator -
 *     Use for treasure chests so the new object is the highest thing
 *     beneath the player, but not actually above it.  Note - the
 *     map and x,y coordinates for the object to be inserted must
 *     match the originator.
 *
 * Note that INS_BELOW_ORIGINATOR, INS_ON_TOP, INS_ABOVE_FLOOR_ONLY
 * are mutually exclusive.  The behaviour for passing more than one
 * should be considered undefined - while you may notice what happens
 * right now if you pass more than one, that could very well change
 * in future revisions of the code.
 */
#define INS_NO_MERGE            0x0001
#define INS_ABOVE_FLOOR_ONLY    0x0002
#define INS_NO_WALK_ON          0x0004
#define INS_ON_TOP              0x0008
#define INS_BELOW_ORIGINATOR    0x0010
#define INS_NO_AUTO_EXIT	0x0020 // temporary, fix exits instead

//-GPL

#endif

