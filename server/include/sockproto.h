/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

/* image.c */
int is_valid_faceset(int fsn);
void free_socket_images();
void read_client_images();
void SetFaceMode(char *buf, int len, client *ns);
void AskFaceCmd(char *buf, int len, client *ns);
void send_image_info(client *ns, char *params);
void send_image_sums(client *ns, char *params);
/* info.c */
void new_draw_info(int flags, int pri, const object *pl, const char *buf);
void new_draw_info_format(int flags, int pri, const object *pl, const char *format, ...);
void new_info_map_except(int color, maptile *map, object *op, const char *str);
void new_info_map(int color, maptile *map, const char *str);
void set_title(object *pl, char *buf);
void draw_magic_map(object *pl);
/* init.c */
void init_ericserver();
void free_all_newserver();
void final_free_player(player *pl);
/* item.c */
void esrv_draw_look(player *pl);
void esrv_send_inventory(object *pl, object *op);
void esrv_update_item(int flags, object *pl, object *op);
void esrv_send_item(object *pl, object *op);
void esrv_del_item(player *pl, int tag);
void ExamineCmd(char *buf, int len, player *pl);
void ExCmd(char *buf, int len, player *pl);
void ApplyCmd(char *buf, int len, player *pl);
void LockItem(char *data, int len, player *pl);
void MarkItem(char *data, int len, player *pl);
void LookAt(char *buf, int len, player *pl);
void esrv_move_object(object *pl, tag_t to, tag_t tag, long nrof);
/* loop.c */
void RequestInfo(char *buf, int len, client *ns);
void watchdog();
/* lowlevel.c */
void write_cs_stats();
/* request.c */
void SetUp(char *buf, int len, client *ns);
void AddMeCmd(char *buf, int len, client *ns);
void ToggleExtendedInfos(char *buf, int len, client *ns);
void AskSmooth(char *buf, int len, client *ns);
void PlayerCmd(char *buf, int len, player *pl);
void NewPlayerCmd(char *buf, int len, player *pl);
void ReplyCmd(char *buf, int len, client *ns);
void VersionCmd(char *buf, int len, client *ns);
void SetSound(char *buf, int len, client *ns);
void MapRedrawCmd(char *buff, int len, player *pl);
void ExtCmd(char *buff, int len, player *pl);
void ExtiCmd(char *buff, int len, client *ns);
void MapInfoCmd(char *buff, int len, player *pl);
void MoveCmd(char *buf, int len, player *pl);
void send_query(client *ns, uint8 flags, const char *text);
void esrv_update_stats(player *pl);
void esrv_new_player(player *pl);
int getExtendedMapInfoSize(client *ns);
void draw_client_map(player *pl);
void esrv_update_spells(player *pl);
void esrv_remove_spell(player *pl, object *spell);
void esrv_add_spells(player *pl, object *spell);
