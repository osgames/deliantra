/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

//
// cfperl.h perl interface
//
#ifndef CFPERL_H__
#define CFPERL_H__

using namespace std;

#include <EXTERN.h>
#include <perl.h>
#include <XSUB.h>

#include <EVAPI.h>
#include <CoroAPI.h>

#include "util.h"
#include "keyword.h"
#include "dynbuf.h"
#include "callback.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// optimisations/workaround for functions requiring my_perl in scope (anti-bloat)
#undef localtime
#undef srand48
#undef drand48
#undef srandom
#undef opendir
#undef readdir
#undef closedir
#undef getprotobyname
#undef gethostbyname
#undef ctime
#undef strerror
#undef _

// same here, massive symbol spamming
#undef do_open
#undef do_close
#undef ref
#undef seed

// perl bug #40256: perl does overwrite those with reentrant versions
// but does not initialise their state structures.
#undef random
#undef crypt

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define _(msg)  (msg)
#define N_(msg) (msg)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// some macros to simplify perl in-calls

#define CHECK_ERROR			\
  if (SvTRUE (ERRSV))			\
    LOG (llevError, "runtime error in %s: %s", __func__, SvPVutf8_nolen (ERRSV));

inline int call_pvsv (const char *ob, I32 flags) { return call_pv (ob, flags); }
inline int call_pvsv (SV         *ob, I32 flags) { return call_sv (ob, flags); }

// TODO: temporarily enabled pushstack/popstack for all calls from the core, to maybe fix object memleak?
#define CALL_PUSH PUSHSTACKi (PERLSI_UNKNOWN)
#define CALL_POP  PUTBACK; POPSTACK; SPAGAIN
#define CALL_BEGIN(args) dSP; CALL_PUSH; ENTER; SAVETMPS; PUSHMARK (SP); EXTEND (SP, args)
#define CALL_ARG_SV(sv) PUSHs (sv_2mortal (sv)) // separate because no refcount inc
#define CALL_ARG(expr) PUSHs (sv_2mortal (to_sv (expr)))
#define CALL_CALL(name, flags) PUTBACK; int count = call_pvsv (name, (flags) | G_EVAL); SPAGAIN;
#define CALL_END PUTBACK; CHECK_ERROR; FREETMPS; LEAVE; CALL_POP

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void cfperl_init ();
void cfperl_main ();
void cfperl_tick ();
void cfperl_emergency_save ();
void cfperl_cleanup (int make_core);
void cfperl_make_book (object *book, int level);
void cfperl_send_msg (client *ns, int color, const_utf8_string type, const_utf8_string msg);
int cfperl_can_merge (object *ob1, object *ob2);
void cfperl_mapscript_activate (object *ob, int state, object *activator, object *originator = 0);
void cfperl_ix (client *ns, int set, faceidx idx, int pri);

bool is_match_expr (const_utf8_string expr);
// applies the match expression and returns true if it matches
bool match (const_utf8_string expr, object *ob, object *self = 0, object *source = 0, object *originator = 0);
// same as above, but returns the first object found, or 0
object *match_one (const_utf8_string expr, object *ob, object *self = 0, object *source = 0, object *originator = 0);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if IVSIZE >= 8
  typedef IV val64;
  typedef IV uval64;
# define newSVval64 newSViv
# define SvVAL64 SvIV
# define newSVuval64 newSVuv
# define SvUVAL64 SvUV
#else
  typedef double val64;
  typedef double uval64;
# define newSVval64 newSVnv
# define SvVAL64 SvNV
# define newSVuval64 newSVnv
# define SvUVAL64 SvNV
#endif

extern tstamp runtime; // virtual server time, excluding time jumps and lag
extern tstamp NOW;     // real time of current server tick

noinline utf8_string cfSvPVutf8_nolen (SV *sv);

enum event_klass
{
  KLASS_NONE,
  KLASS_GLOBAL,
  KLASS_ATTACHABLE,
  KLASS_CLIENT,
  KLASS_PLAYER,
  KLASS_OBJECT,
  KLASS_MAP,
  KLASS_COMMAND,
};

enum event_type
{
# define def(klass,name) EVENT_ ## klass ## _ ## name,
#  include "eventinc.h"
# undef def
  NUM_EVENT_TYPES
};

// in which global events or per-type events are we interested
extern bitset<NUM_EVENT_TYPES> ev_want_event;
extern bitset<NUM_TYPES>       ev_want_type;

#define ARG_AV(o)         DT_AV    , static_cast<AV *>   (o)
#define ARG_INT(v)        DT_INT   , static_cast<int>    (v)
#define ARG_INT64(v)      DT_INT64 , static_cast<sint64> (v)
#define ARG_DOUBLE(v)     DT_DOUBLE, static_cast<double> (v)
#define ARG_STRING(v)     DT_STRING, static_cast<const char *> (v)
#define ARG_DATA(s,l)     DT_DATA  , static_cast<const void *> (s), int (l)
#define ARG_OBJECT(o)     DT_OBJECT, (void *)(static_cast<object *>    (o))
#define ARG_MAP(o)        DT_MAP   , (void *)(static_cast<maptile *>   (o))
#define ARG_PLAYER(o)     DT_PLAYER, (void *)(static_cast<player *>    (o))
#define ARG_ARCH(o)       DT_ARCH  , (void *)(static_cast<archetype *> (o))
#define ARG_CLIENT(o)     DT_CLIENT, (void *)(static_cast<client *>    (o))
#define ARG_PARTY(o)      DT_PARTY , (void *)(static_cast<party *>     (o))
#define ARG_REGION(o)     DT_REGION, (void *)(static_cast<region *>    (o))

// the ", ## __VA_ARGS" is, unfortunately, a gnu-cpp extension
#define INVOKE(obj,event, ...) (expect_false ((obj)->should_invoke (event)) ? (obj)->invoke (event, ## __VA_ARGS__, DT_END) : 0)
#define INVOKE_GLOBAL(event, ...)          INVOKE (&gbl_ev, EVENT_ ## GLOBAL     ## _ ## event, ## __VA_ARGS__)
#define INVOKE_ATTACHABLE(event, obj, ...) INVOKE (obj    , EVENT_ ## ATTACHABLE ## _ ## event, ## __VA_ARGS__)
#define INVOKE_OBJECT(event, obj, ...)     INVOKE (obj    , EVENT_ ## OBJECT     ## _ ## event, ## __VA_ARGS__)
#define INVOKE_CLIENT(event, obj, ...)     INVOKE (obj    , EVENT_ ## CLIENT     ## _ ## event, ## __VA_ARGS__)
#define INVOKE_PLAYER(event, obj, ...)     INVOKE (obj    , EVENT_ ## PLAYER     ## _ ## event, ## __VA_ARGS__)
#define INVOKE_MAP(event, obj, ...)        INVOKE (obj    , EVENT_ ## MAP        ## _ ## event, ## __VA_ARGS__)

//TODO should index into @result
#define RESULT(idx,type) cfperl_result_ ## type (idx)
#define RESULT_DOUBLE(idx) RESULT(idx, DOUBLE)
#define RESULT_INT(idx)    RESULT(idx, INT)

double cfperl_result_DOUBLE (int idx);
int cfperl_result_INT (int idx);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

INTERFACE_CLASS (attachable)
struct attachable : refcnt_base
{
  static MGVTBL vtbl;

  enum {
    F_DESTROYED   = 0x01,
    F_DEBUG_TRACE = 0x02,
  };
  uint8 ACC (RW, attachable_flags);

  static unordered_vector<attachable *> mortals;
  MTH static void check_mortals ();

  // object is delete'd after the refcount reaches 0
  MTH int refcnt_cnt () const;
  // check wether the object has died and destroy
  MTH void refcnt_chk () { if (expect_false (refcnt <= 0)) do_check (); }

  // destroy the object unless it was already destroyed
  // this politely asks everybody interested the reduce
  // the refcount to 0 as soon as possible.
  MTH void destroy ();

  // return wether an object was destroyed already
  MTH bool destroyed () const { return attachable_flags & F_DESTROYED; }

  // destruct and free the memory for this object
  virtual void do_delete ();

  virtual void gather_callbacks (AV *&callbacks, event_type event) const;

#if 0
private:
  static refcounted *rc_first;
  refcounted *rc_next;
#endif

  HV *self;     // CF+ perl self
  AV *cb;       // CF+ callbacks
  shstr attach; // generic extension attachment information

  void sever_self (); // sever this object from its self, if it has one.
  void optimise ();   // possibly save some memory by destroying unneeded data

  attachable ()
  : attachable_flags (0), self (0), cb (0), attach (0)
  {
  }

  attachable (const attachable &src)
  : attachable_flags (0), self (0), cb (0), attach (src.attach)
  {
  }

  // set a custom key to the given value, or delete it if value = 0
  void set_key (const_utf8_string key, const_utf8_string value = 0, bool is_utf8 = 0);

  void set_key_text (const_utf8_string key, const_utf8_string value = 0)
  {
    set_key (key, value, 1);
  }

  void set_key_data (const_utf8_string key, const_utf8_string value = 0)
  {
    set_key (key, value, 0);
  }

  attachable &operator =(const attachable &src);

  // used to _quickly_ decide wether to shortcut the evaluation
  bool should_invoke (event_type event)
  {
    return ev_want_event [event] || cb;
  }

  bool invoke (event_type event, ...);

  MTH void instantiate ();
  void reattach ();

protected:
  // do the real refcount checking work
  void do_check ();

  // the method that does the real destroy work
  virtual void do_destroy ();

  // destructor is protected, should not be called by anybody
  virtual ~attachable ();
};

// the global object is a pseudo object that cares for the global events
struct global : attachable
{
  void gather_callbacks (AV *&callbacks, event_type event) const;
};

extern struct global gbl_ev;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "freezethaw.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct coroapi {
  static struct CoroAPI *GCoroAPI;

  static EV_ATOMIC_T cede_pending;

  static int nready () {                   return CORO_NREADY; }
  static int cede   () { cede_pending = 0; return CORO_CEDE  ; }

  static void do_cede_to_tick ();

  // actually cede's far more often
  static bool cede_to_tick ()
  {
    if (expect_true (!cede_pending))
      return false;

    do_cede_to_tick ();
    return true;
  }

  static void wait_for_tick ();
  static void wait_for_tick_begin ();
};

struct evapi
{
  static struct EVAPI *GEVAPI;
};

struct iow : ev_io, evapi, callback<void (iow &, int)>
{
  static void thunk (EV_P_ struct ev_io *w_, int revents)
  {
    iow &w = *static_cast<iow *>(w_);
    
    w (w, revents);
  }

  template<class O, class M>
  iow (O object, M method)
  : callback<void (iow &, int)> (object, method)
  {
    ev_init ((ev_io *)this, thunk);
  }

  void prio (int prio)
  {
    ev_set_priority ((ev_io *)this, prio);
  }

  void set (int fd, int events)
  {
    ev_io_set ((ev_io *)this, fd, events);
  }

  int poll () const { return events; }

  void poll (int events);

  void start ()
  {
    ev_io_start (EV_DEFAULT, (ev_io *)this);
  }

  void stop ()
  {
    ev_io_stop (EV_DEFAULT, (ev_io *)this);
  }

  ~iow ()
  {
    stop ();
  }
};

struct asyncw : ev_async, evapi, callback<void (ev_async &, int)>
{
  static void thunk (EV_P_ struct ev_async *w_, int revents)
  {
    asyncw &w = *static_cast<asyncw *>(w_);
    
    w (w, revents);
  }

  template<class O, class M>
  asyncw (O object, M method)
  : callback<void (asyncw &, int)> (object, method)
  {
    ev_init ((ev_async *)this, thunk);
  }

  void start ()
  {
    ev_async_start (EV_DEFAULT, (ev_async *)this);
  }

  void stop ()
  {
    ev_async_stop (EV_DEFAULT, (ev_async *)this);
  }

  ~asyncw ()
  {
    stop ();
  }
};

#endif

