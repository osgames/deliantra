/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

//   ident   , use string,                              nonuse string
def (skill   , "You can use it as your skill"         , "It is used as a skill")
def (combat  , "You can wield it as your weapon"      , "It is used as a combat weapon")
def (range   , "You can use it as your range weapon"  , "It is used as a range weapon")
def (shield  , "You can wield it as a shield"         , "It is used as a shield")
def (arm     , "You can put it on your arm"           , "It goes on a human's arm")
def (torso   , "You can wear it on your body"         , "It goes on a human's torso")
def (head    , "You can wear it on your head"         , "It goes on a human's head")
def (neck    , "You can wear it around your neck"     , "It goes around a human's neck")
def (finger  , "You can wear it on your finger"       , "It goes on a human's finger")
def (shoulder, "You can wear it around your shoulders", "It goes around a human's shoulders")
def (foot    , "You can put it on your foot"          , "It goes on a human's foot")
def (hand    , "You can put it on your hand"          , "It goes on a human's hand")
def (wrist   , "You can wear it around your wrist"    , "It goes around a human's wrist")
def (waist   , "You can wear it around your waist"    , "It goes around a human's waist")
//def (dragon_torso", "your body", "a dragon's body"}
