/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

/* This file defines various flags that both the new client and
 * newserver uses.  These should never be changed, only expanded.
 * Changing them will likely cause all old clients to not work properly.
 * While called newclient, it is really used by both the client and
 * server to keep some values the same.
 *
 * Name format is CS_(command)_(flag)
 * CS = Client/Server.
 * (command) is protocol command, ie ITEM
 * (flag) is the flag name
 */

/* It is trivial to keep a link of copy of this file in the client
 * or server area.  But keeping one common file should make things
 * more reliable, as both the client and server will definately be
 * talking about the same values.
 */

#ifndef NETWORK_H
#define NETWORK_H

//+GPL

#include <cstdarg>

#include "compiler.h"

/* Maximum size of any packet we expect.
 *
 * The size must be the same in the server and the client (stupid).
 * TODO: make the actual network configurable per client and use 2**16.
 */
#define MAXSOCKBUF 10239

#define CS_QUERY_YESNO	0x1	/* Yes/no question */
#define CS_QUERY_SINGLECHAR 0x2	/* Single character response expected */
#define CS_QUERY_HIDEINPUT 0x4	/* Hide input being entered */

/* These are multiplication values that should be used when changing
 * floats to ints, and vice version.  MULTI is integer representatin
 * (float to int), MULTF is float, for going from int to float.
 */
#define FLOAT_MULTI	100000
#define FLOAT_MULTF	100000.0

/* ID's for the various stats that get sent across. */

#define CS_STAT_HP	 1
#define CS_STAT_MAXHP	 2
#define CS_STAT_SP	 3
#define CS_STAT_MAXSP	 4
#define CS_STAT_STR	 5
#define CS_STAT_INT	 6
#define CS_STAT_WIS	 7
#define CS_STAT_DEX	 8
#define CS_STAT_CON	 9
#define CS_STAT_CHA	10
#define CS_STAT_EXP	11
#define CS_STAT_LEVEL	12
#define CS_STAT_WC	13
#define CS_STAT_AC	14
#define CS_STAT_DAM	15
#define CS_STAT_ARMOUR	16
#define CS_STAT_SPEED	17
#define CS_STAT_FOOD	18
#define CS_STAT_WEAP_SP 19
#define CS_STAT_RANGE	20
#define CS_STAT_TITLE	21
#define CS_STAT_POW	22
#define CS_STAT_GRACE	23
#define CS_STAT_MAXGRACE	24
#define CS_STAT_FLAGS	25
#define CS_STAT_WEIGHT_LIM	26
#define CS_STAT_EXP64	28
#define CS_STAT_SPELL_ATTUNE	29
#define CS_STAT_SPELL_REPEL	30
#define CS_STAT_SPELL_DENY	31

/* Start & end of resistances, inclusive. */
#define CS_STAT_RESIST_START	100
#define CS_STAT_RESIST_END	117
#define CS_STAT_RES_PHYS	100
#define CS_STAT_RES_MAG		101
#define CS_STAT_RES_FIRE	102
#define CS_STAT_RES_ELEC	103
#define CS_STAT_RES_COLD	104
#define CS_STAT_RES_CONF	105
#define CS_STAT_RES_ACID	106
#define CS_STAT_RES_DRAIN	107
#define CS_STAT_RES_GHOSTHIT	108
#define CS_STAT_RES_POISON	109
#define CS_STAT_RES_SLOW	110
#define CS_STAT_RES_PARA	111
#define CS_STAT_TURN_UNDEAD	112
#define CS_STAT_RES_FEAR	113
#define CS_STAT_RES_DEPLETE	114
#define CS_STAT_RES_DEATH	115
#define CS_STAT_RES_HOLYWORD	116
#define CS_STAT_RES_BLIND	117

/* CS_STAT_SKILLINFO is used as the starting index point.  Skill number->name
 * map is generated dynamically for the client, so a bunch of entries will
 * be used here.  CS_NUM_SKILLS does not match how many skills there really
 * are - instead, it is used as a range of values so that the client
 * can have some idea how many skill categories there may be.
 */
#define CS_STAT_SKILLINFO   140
#define CS_NUM_SKILLS	     50

/* These are used with CS_STAT_FLAGS above to communicate S->C what the
 * server thinks the fireon & runon states are.
 */
#define SF_FIREON   0x01
#define SF_RUNON    0x02

/* Flags for the item command */
enum {a_none, a_readied, a_wielded, a_worn, a_active, a_applied};

#define F_APPLIED       0x000F
#define F_LOCATION      0x00F0
#define F_UNPAID        0x0200
#define F_MAGIC         0x0400
#define F_CURSED        0x0800
#define F_DAMNED        0x1000
#define F_OPEN          0x2000
#define F_NOPICK        0x4000
#define F_LOCKED        0x8000

#define CF_FACE_NONE	0
#define CF_FACE_BITMAP	1
#define CF_FACE_XPM	2
#define CF_FACE_PNG	3
#define CF_FACE_CACHE	0x10

/* Used in the new_face structure on the magicmap field.  Low bits
 * are color informatin.  For now, only high bit information we need
 * is floor information.
 */
#define FACE_FLOOR	0x80
#define FACE_WALL	0x40	/* Or'd into the color value by the server
                                 * right before sending.
                                 */
#define FACE_COLOR_MASK	0xf

#define UPD_LOCATION	0x01
#define UPD_FLAGS	0x02
#define UPD_WEIGHT	0x04
#define UPD_FACE	0x08
#define UPD_NAME	0x10
#define UPD_ANIM	0x20
#define UPD_ANIMSPEED	0x40
#define UPD_NROF	0x80

/* The flag values sent by the updspell command */
#define UPD_SP_MANA	0x01
#define UPD_SP_GRACE	0x02
#define UPD_SP_LEVEL	0x04 // was UPD_SP_DAMAGE in <= server 2.93, client 2.10

#define SOUND_NORMAL	0
#define SOUND_SPELL	1

//-GPL

struct data
{
  unsigned int len;
  const void *ptr;

  data   (const void *ptr, int len) : len (len), ptr (ptr) { }
  data   (const char *str) : len (strlen (str ? str : 0)), ptr ((void *)str) { }
  data   (shstr_tmp sh)    : len (sh.length ()), ptr ((void *)&sh) { }
  data   (dynbuf &buf)     : len (buf.size ()), ptr (buf.linearise ()) { }
};

struct data8
{
  unsigned int len;
  const void *ptr;

  data8  (const void *ptr, int len) : len (len), ptr (ptr) { }
  data8  (const char *str) : len (strlen (str ? str : 0)), ptr ((void *)str) { }
  data8  (shstr_tmp sh)    : len (sh.length ()), ptr ((void *)&sh) { }
  data8  (dynbuf &buf)     : len (buf.size ()), ptr (buf.linearise ()) { }
};

struct data16
{
  unsigned int len;
  const void *ptr;

  data16 (const void *ptr, int len) : len (len), ptr (ptr) { }
  data16 (const char *str) : len (strlen (str ? str : 0)), ptr ((void *)str) { }
  data16 (shstr_tmp sh)    : len (sh.length ()), ptr ((void *)&sh) { }
  data16 (dynbuf &buf)     : len (buf.size ()), ptr (buf.linearise ()) { }
};

struct ber32
{
  enum { size = 5 }; // maximum length of an encoded ber32
  uint32 val;

  ber32 (uint32 val) : val (val) { }

  static int encoded_size (uint32 val)
  {
    if (val >= (1 << 7*4)) return 5;
    if (val >= (1 << 7*3)) return 4;
    if (val >= (1 << 7*2)) return 3;
    if (val >= (1 << 7*1)) return 2;
    return 1;
  }
};

/* Contains the base information we use to make up a packet we want to send. */
struct packet
{
  enum { bufsize = MAXSOCKBUF };
  enum { hdrlen  = 2 }; // keep this many bytes free at the front

  // strategy: only check on variable-length adds
  // and allow 256 bytes for any headers.
  uint8 *cur;
  uint8 buf_ [hdrlen + bufsize + 256];

  packet ()
  {
    reset ();
  }

  packet (const char *name);

  uint8  operator [](unsigned int i) const { return buf_ [i + hdrlen]; }
  uint8 &operator [](unsigned int i)       { return buf_ [i + hdrlen]; }

        uint8 *buf ()       { return buf_ + hdrlen; }
  const uint8 *buf () const { return buf_ + hdrlen; }

  void reset (int offset = 0) { cur = buf () + offset; }
  int length () const { return cur - buf (); }
  int room () { return bufsize - (cur - buf ()); }

  // unchecked
  packet &operator <<(uint8  v) { *cur++ = v; return *this; }

  packet &operator <<(uint16 v) { return *this << uint8  (v >>  8) << uint8  (v); }
  packet &operator <<(uint32 v) { return *this << uint16 (v >> 16) << uint16 (v); }
  packet &operator <<(uint64 v) { return *this << uint32 (v >> 32) << uint32 (v); }

  packet &operator <<(sint8  v) { return *this << (uint8 )v; }
  packet &operator <<(sint16 v) { return *this << (uint16)v; }
  packet &operator <<(sint32 v) { return *this << (uint32)v; }
  packet &operator <<(sint64 v) { return *this << (uint64)v; }

  // checked
  packet &operator <<(const ber32 v);

  packet &operator <<(const data   &v);
  packet &operator <<(const data8  &v);
  packet &operator <<(const data16 &v);

  packet &operator <<(const char *v);
  packet &operator <<(shstr_tmp sh) { return *this << data (sh); }
  packet &operator <<(const std::string &s) { return *this << data (s.data (), s.size ()); }

  void vprintf (const char *format, va_list ap);

  void printf (const char *format, ...) attribute ((format (printf, 2, 3)))
  {
    va_list ap;
    va_start (ap, format);
    vprintf (format, ap);
    va_end (ap);
  }
};

inline uint16 net_uint16 (uint8 *data) { return (data [0] << 8) | data [1]; }
inline uint32 net_uint32 (uint8 *data) { return (net_uint16 (data) << 16) | net_uint16 (data + 2); }
inline sint16 net_sint16 (uint8 *data) { return sint16 (net_uint16 (data)); }
inline sint16 net_sint32 (uint8 *data) { return sint32 (net_uint32 (data)); }

#endif

