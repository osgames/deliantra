/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

/* #defines are needed by living.h, so they must be loaded early */
#ifndef MATERIAL_H_
#define MATERIAL_H_

#define NROFMATERIALS		13

#define M_PAPER			1
#define M_IRON			2
#define M_GLASS			4
#define M_LEATHER		8
#define M_WOOD			16
#define M_ORGANIC		32
#define M_STONE			64
#define M_CLOTH			128
#define M_ADAMANT		256
#define M_LIQUID		512
#define M_SOFT_METAL		1024
#define M_BONE			2048
#define M_ICE			4096
#define M_SPECIAL		8192	/* when displaying names, don't show the materialname */

struct materialtype_t : zero_initialised
{
  struct materialtype_t *next;
  shstr name;
  shstr description;
  int	material;
  sint8	save[NROFATTACKS];
  sint8	mod[NROFATTACKS];
  sint8	chance;
  sint8	difficulty;
  sint8	magic;
  sint8	damage;
  sint8	wc;
  sint8	ac;
  sint8	sp;
  int	weight;
  int	value;
  int   density;		// g/cm³ * 1000 == kg/m³

  materialtype_t ();
  void reset ();
};

extern materialtype_t *materialt;
extern materialtype_t material_null; // defined in shstr.C

#define MATERIAL_NULL &material_null

void _reload_materials ();
// provide a material for the item if the material isn't set yet
void select_material (object *op, int difficulty);
materialtype_t *name_to_material (const shstr_tmp name);

#endif

