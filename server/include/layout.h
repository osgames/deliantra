/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#ifndef LAYOUT_H
#define LAYOUT_H

// a simple point helper struct
struct point
{
  int x;
  int y;

  point ()
  {
  }

  point (int x, int y)
  : x(x), y(y)
  {
  }
};

//
// reference
//
// \0  floor only
// #   wall
// D   door
// <   entrance (traditionally up)
// >   exit (traditionally down)
// C   "center" (of onion maze)
// .   ?? (rogue)
//

// use this in new code
INTERFACE_CLASS(layout)
struct layout
{
  typedef char cell;

  cell **data;
  int w, h;

  layout (int w, int h);
  layout (layout &copy);

  // reference rect in other layout - will not keep the data alive,
  // so never swap with it's orig, or free the orig when in use.
  layout (layout &orig, int x1, int y1, int x2, int y2);

  ~layout ();

  operator cell **() const
  {
    return data;
  }

  void swap (layout &maze)
  {
    ::swap (maze.data, data);
    ::swap (maze.w   , w   );
    ::swap (maze.h   , h   );
    ::swap (maze.size, size);
  }

  MTH void swap (layout *maze) { swap (*maze); }

  // for debugging, print maze to stdout
  MTH void print () const;

  // simple inpainting
  MTH void fill (char fill);
  MTH void clear () { fill (0); }
  MTH void border (char fill = '#');
  MTH void rect (int x1, int y1, int x2, int y2, char fill); // x2, y2 exclusive
  MTH void fill_rect (int x1, int y1, int x2, int y2, char fill); // x2, y2 exclusive

  MTH void fill_rand (int perc);
  MTH void replace (char from, char to);

  point find (char target, int mode = 0); // mode 0=random, 1=upleft, 2=upright, 3=downright, 4=downleft

  // makes sure all areas are connected
  // perturb = 0 - very horz/vert tunnels
  // perturb = 1 - straight but round
  // perturb = 2 - snaky tunnels
  MTH void isolation_remover (int perturb = 2);

  // generates a cave, subtype 0 is a rough cave, randomly open or closed
  MTH void gen_cave (int subtype);
  MTH void gen_castle (); // generates straightish structures

  // helper functions to modify the maze
  MTH void erode_1_2 (int c1, int c2 = -1, int repeat = 1);
  MTH void doorify ();
  MTH void roomify (); // make some rooms in it, works best on onions
  MTH void expand2x ();
  MTH void symmetrize (int symmetry);
  MTH void rotate (int rotation); // rotate by 1=90, 2=180, 3=270 degrees

  void generate (random_map_params *RP);
private:
  int size;
  void alloc (int w, int h);
};

#endif

