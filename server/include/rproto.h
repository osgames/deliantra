/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002,2007 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992,2007 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

/* random_map.c */
extern int make_wall (layout &maze, int x, int y, int dir);
/* room_gen_onion.c */
extern void map_gen_onion (layout &maze, int option, int layers);
/* room_gen_spiral.c */
extern void map_gen_spiral (layout &maze, int option);
/* maze_gen.c */
extern void maze_gen (layout &maze, int full);
/* wall.c */
extern int surround_flag (const layout &maze, int i, int j);
extern int surround_flag2 (const layout &maze, int i, int j);
extern int surround_flag3 (maptile *map, int i, int j);
extern void make_map_walls (maptile *map, layout &maze, const char *w_style, const char *m_style, random_map_params *RP);
extern object *retrofit_joined_wall (maptile *the_map, int i, int j, int insert_flag, random_map_params *RP);
/* monster.c */
extern void place_monsters (maptile *map, const char *monsterstyle, int difficulty, random_map_params *RP);
/* door.c */
extern void put_doors (maptile *the_map, layout &maze, const char *doorstyle, random_map_params *RP);
/* decor.c */
extern void put_decor (maptile *map, layout &maze, const char *decorstyle, int decor_option, random_map_params *RP);
/* exit.c */
extern void place_exits (maptile *map, layout &maze, const char *exitstyle, int orientation, random_map_params *RP);
extern void unblock_exits (maptile *map, layout &maze);
/* treasure.c */
extern int wall_blocked (maptile *m, int x, int y);
extern void place_treasure (maptile *map, layout &maze, const char *treasure_style, int treasureoptions, random_map_params *RP);
/* special.c */
extern void place_specials_in_map (maptile *map, layout &maze, random_map_params *RP);
/* rogue_layout.c */
extern int surround_check (layout &maze, int i, int j);
extern void roguelike_layout_gen (layout &maze, int options);
/* snake.c */
extern void make_snake_layout (layout &maze, int options);
/* square_spiral.c */
extern void make_square_spiral_layout (layout &maze, int options);
/* expand2x.c */
extern void expand2x (layout &maze);

