/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002-2005 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

/*
 * The maptile is allocated each time a new map is opened.
 * It contains pointers (very indirectly) to all objects on the map.
 */

#ifndef MAP_H
#define MAP_H

#include <tr1/unordered_map>

//+GPL

#include "region.h"
#include "cfperl.h"

/* We set this size - this is to make magic map work properly on
 * tiled maps. There is no requirement that this matches the
 * tiled maps size - it just seemed like a reasonable value.
 * Magic map code now always starts out putting the player in the
 * center of the map - this makes the most sense when dealing
 * with tiled maps.
 * We also figure out the magicmap color to use as we process the
 * spaces - this is more efficient as we already have up to date
 * map pointers.
 */
#define MAGIC_MAP_SIZE  50
#define MAGIC_MAP_HALF  MAGIC_MAP_SIZE/2

#define MAP_LAYERS 3

// tile map index - toggling the lowest bit reverses direction
enum
{
  TILE_NORTH,
  TILE_SOUTH,
  TILE_WEST,
  TILE_EAST,
  TILE_UP,
  TILE_DOWN,
  TILE_NUM
};

#define REVERSE_TILE_DIR(dir) ((dir) ^ 1)

/* Values for state below */
enum
{
  MAP_SWAPPED,  // header loaded, nothing else
  MAP_INACTIVE, // in memory, linkable, but not active
  MAP_ACTIVE,   // running!
};

/* GET_MAP_FLAGS really shouldn't be used very often - get_map_flags should
 * really be used, as it is multi tile aware.  However, there are some cases
 * where it is known the map is not tiled or the values are known
 * consistent (eg, op->map, op->x, op->y)
 */
// all those macros are herewith declared legacy
#define GET_MAP_FLAGS(M,X,Y)         (M)->at((X),(Y)).flags ()
#define GET_MAP_OB(M,X,Y)            (M)->at((X),(Y)).bot
#define GET_MAP_TOP(M,X,Y)           (M)->at((X),(Y)).top
#define GET_MAP_FACE_OBJ(M,X,Y,L)    (M)->at((X),(Y)).faces_obj[L]
#define GET_MAP_MOVE_BLOCK(M,X,Y)    (M)->at((X),(Y)).move_block
#define GET_MAP_MOVE_SLOW(M,X,Y)     (M)->at((X),(Y)).move_slow
#define GET_MAP_MOVE_ON(M,X,Y)       (M)->at((X),(Y)).move_on
#define GET_MAP_MOVE_OFF(M,X,Y)      (M)->at((X),(Y)).move_off

/* You should really know what you are doing before using this - you
 * should almost always be using out_of_map instead, which takes into account
 * map tiling.
 */
#define OUT_OF_REAL_MAP(M,X,Y) (!(IN_RANGE_EXC ((X), 0, (M)->width) && IN_RANGE_EXC ((Y), 0, (M)->height)))

/* These are used in the mapspace flags element.  They are not used in
 * in the object flags structure.
 */
#define P_BLOCKSVIEW	0x01
#define P_NO_MAGIC      0x02   /* Spells (some) can't pass this object */
#define P_NO_CLERIC     0x04   /* no clerical spells cast here */
#define	P_SAFE          0x08   /* If this is set the map tile is a safe space,
                                * that means, nothing harmful can be done,
                                * such as: bombs, potion usage, alchemy, spells
                                * this was introduced to make shops safer
                                * but is useful in other situations */

#define P_PLAYER        0x10   /* a player (or something seeing these objects) is on this mapspace */
#define P_IS_ALIVE      0x20   /* something alive is on this space */
#define P_UPTODATE	0x80   // this space is up to date

/* The following two values are not stored in the MapLook flags, but instead
 * used in the get_map_flags value - that function is used to return
 * the flag value, as well as other conditions - using a more general
 * function that does more of the work can hopefully be used to replace
 * lots of duplicate checks currently in the code.
 */
#define P_OUT_OF_MAP	0x10000 /* This space is outside the map */
#define	P_NEW_MAP	0x20000 /* Coordinates passed result in a new tiled map  */

// persistent flags (pflags) in mapspace
enum
{
  PF_VIS_UP = 0x01, // visible upwards, set by upmap, cleared by mapspace
};

/* Instead of having numerous arrays that have information on a
 * particular space (was map, floor, floor2, map_ob),
 * have this structure take care of that information.
 * This puts it all in one place, and should also make it easier
 * to extend information about a space.
 */
INTERFACE_CLASS (mapspace)
struct mapspace
{
  object *ACC (RW, bot);
  object *ACC (RW, top);                /* lowest/highest object on this space */
  object *ACC (RW, faces_obj[MAP_LAYERS]);/* face objects for the 3 layers */
  uint8 flags_;                         /* flags about this space (see the P_ values above) */
  sint8 ACC (RW, light);                /* How much light this space provides */
  MoveType ACC (RW, move_block);        /* What movement types this space blocks */
  MoveType ACC (RW, move_slow);         /* What movement types this space slows */
  MoveType ACC (RW, move_on);           /* What movement types are activated */
  MoveType ACC (RW, move_off);          /* What movement types are activated */
  uint16_t ACC (RW, items_);            // saturates at 64k
  uint32_t ACC (RW, volume_);           // ~dm³ (not cm³) (factor is actually 1024)
  uint32_t ACC (RW, smell);		// the last count a player was seen here, or 0
  static uint32_t ACC (RW, smellcount); // global smell counter

  uint8_t pflags;			// additional, persistent flags
  uint8_t pad [3];			// pad to 64 bytes on LP64 systems

//-GPL

  void update_ ();
  MTH void update ()
  {
    // we take advantage of the fact that 0x80 is the sign bit
    // to generate more efficient code on many cpus
    assert (sint8 (P_UPTODATE) < 0);
    assert (sint8 (-1 & ~P_UPTODATE) >= 0);

    if (expect_false (sint8 (flags_) >= 0))
      update_ ();

    // must be true by now (gcc seems content with only the second test)
    assume (sint8 (flags_) < 0);
    assume (flags_ & P_UPTODATE);
  }

  MTH uint8 flags ()
  {
    update ();
    return flags_;
  }

  MTH void update_up ();
  
  MTH void invalidate ()
  {
    flags_ = 0;

    if (pflags)
      update_up ();
  }

  MTH object *player ()
  {
    object *op;

    if (flags () & P_PLAYER)
      for (op = top; op->type != PLAYER; op = op->below)
        ;
    else
      op = 0;

    return op;
  }

  MTH uint32 items()
  {
    update ();
    return items_;
  }

  // return the item volume on this mapspace in cm³
  MTH volume_t volume ()
  {
    update ();
    return volume_ * 1024;
  }

  bool blocks (MoveType mt) const
  {
    return move_block && (mt & move_block) == mt;
  }

  bool blocks (object *op) const
  {
    return blocks (op->move_type);
  }
};

// a rectangular area of a map, used my split_to_tiles/unordered_mapwalk
struct maprect
{
  maptile *m;
  int x0, y0;
  int x1, y1;
  int dx, dy; // offset to go from local coordinates to original tile */
};

// (refcounted) list of objects on this map that need physics processing
struct physics_queue
: unordered_vector<object *>
{
  int i; // already processed
  physics_queue ();
  ~physics_queue ();
  object *pop ();
};

#define PHYSICS_QUEUES 16 // "activity" at least every 16 ticks

//+GPL

struct shopitems : zero_initialised
{
  const char *name;             /* name of the item in question, null if it is the default item */
  const char *name_pl;          /* plural name */
  int typenum;                  /* itemtype number we need to match 0 if it is the default price */
  sint8 strength;               /* the degree of specialisation the shop has in this item,
                                 * as a percentage from -100 to 100 */
  int index;                    /* being the size of the shopitems array. */
};

// map I/O, what to load/save
enum {
  IO_HEADER  = 0x01, // the "arch map" pseudo object
  IO_OBJECTS = 0x02, // the non-unique objects
  IO_UNIQUES = 0x04, // unique objects
};

/* In general, code should always use the macros
 * above (or functions in map.c) to access many of the
 * values in the map structure.  Failure to do this will
 * almost certainly break various features.  You may think
 * it is safe to look at width and height values directly
 * (or even through the macros), but doing so will completely
 * break map tiling.
 */
INTERFACE_CLASS (maptile)
struct maptile : zero_initialised, attachable
{
  sint32 ACC (RW, width), ACC (RW, height);        /* Width and height of map. */
  struct mapspace *spaces;         /* Array of spaces on this map */
  uint8 *regions;                  /* region index per mapspace, if != 0 */
  region_ptr *regionmap;           /* index to region */

  tstamp ACC (RW, last_access);    /* last time this map was accessed somehow */

  shstr ACC (RW, name);    /* Name of map as given by its creator */
  region_ptr ACC (RW, default_region); /* What jurisdiction in the game world this map is ruled by
                                    * points to the struct containing all the properties of
                                    * the region */
  double ACC (RW, reset_time);
  uint32 ACC (RW, reset_timeout);  /* How many seconds must elapse before this map
                                    * should be reset
                                    */
  bool ACC (RW, dirty);            /* if true, something was inserted or removed */
  bool ACC (RW, no_reset);         // must not reset this map
  bool ACC (RW, fixed_resettime);  /* if true, reset time is not affected by
                                    * players entering/exiting map
                                    */
  uint8 ACC (RW, state);           /* If not true, the map has been freed and must
                                    * be loaded before used.  The map,omap and map_ob
                                    * arrays will be allocated when the map is loaded */
  sint32 ACC (RW, timeout);        /* swapout is set to this */
  sint32 ACC (RW, swap_time);      /* When it reaches 0, the map will be swapped out */
  sint16 players;        /* How many players are on this map right now */
  uint16 ACC (RW, difficulty);     /* What level the player should be to play here */

  bool ACC (RW, per_player);
  bool ACC (RW, per_party);
  bool ACC (RW, outdoor);  /* True if an outdoor map */
  bool ACC (RW, no_drop);   /* avoid auto-dropping (on death) anything on this map */
  sint8 ACC (RW, darkness);        /* indicates level of darkness of map */
  static sint8 outdoor_darkness; /* the global darkness level outside */

  uint16 ACC (RW, enter_x);        /* enter_x and enter_y are default entrance location */
  uint16 ACC (RW, enter_y);        /* on the map if none are set in the exit */
  oblinkpt *buttons;       /* Linked list of linked lists of buttons */
  struct shopitems *shopitems;     /* a semi-colon seperated list of item-types the map's shop will trade in */
  shstr ACC (RW, shoprace);        /* the preffered race of the local shopkeeper */
  double ACC (RW, shopgreed);      /* how much our shopkeeper overcharges */
  sint64 ACC (RW, shopmin);        /* minimum price a shop will trade for */
  sint64 ACC (RW, shopmax);        /* maximum price a shop will offer */
  shstr ACC (RW, msg);     /* Message map creator may have left */
  shstr ACC (RW, maplore); /* Map lore information */
  shstr ACC (RW, tile_path[TILE_NUM]);      /* path to adjoining maps */
  maptile *ACC (RW, tile_map[TILE_NUM]);   /* Next map, linked list */
  shstr ACC (RW, path);   /* Filename of the map */
  volume_t ACC (RW, max_volume);            // maximum volume for all items on a mapspace
  int ACC (RW, max_items);                // maximum number of items on a mapspace

//-GPL

  physics_queue pq[PHYSICS_QUEUES];
  MTH void queue_physics (object *ob, int after = 0);
  MTH void queue_physics_at (int x, int y);
  MTH int run_physics (tick_t tick, int max_objects);
  MTH void activate_physics ();

  // the maptile:: is neccessary here for the perl interface to work
  MTH sint8 darklevel (sint8 outside = maptile::outdoor_darkness) const
  {
    return clamp (outdoor ? darkness + outside : darkness, 0, MAX_DARKNESS);
  }

  static void adjust_daylight ();

  MTH void activate ();
  MTH void deactivate ();

  // allocates all (empty) mapspace
  MTH void alloc ();
  // deallocates the mapspaces (and destroys all objects)
  MTH void clear ();

  MTH void post_load (); // update cached values in mapspaces etc.
  MTH void fix_auto_apply ();
  MTH void do_decay_objects ();
  MTH void update_buttons ();
  MTH int change_map_light (int change);
  MTH int estimate_difficulty () const;

  MTH void play_sound (faceidx sound, int x, int y) const;
  MTH void say_msg (const_utf8_string msg, int x, int y) const;

  // connected links
  oblinkpt *find_link (shstr_tmp id);
  MTH void trigger (shstr_tmp id, int state = 1, object *activator = 0, object *originator = 0);

  // set the given flag on all objects in the map
  MTH void set_object_flag (int flag, int value = 1);
  MTH void post_load_original ();

  MTH void link_multipart_objects ();
  MTH void clear_unique_items ();

  MTH void clear_header ();
  MTH void clear_links_to (maptile *m);

  MTH struct region *region (int x, int y) const;

  // load the header pseudo-object
  bool _load_header (object_thawer &thawer);
  MTH bool _load_header (object_thawer *thawer) { return _load_header (*thawer); }

  // load objects into the map
  bool _load_objects (object_thawer &thawer);
  MTH bool _load_objects (object_thawer *thawer) { return _load_objects (*thawer); }

  // save objects into the given file (uses IO_ flags)
  bool _save_objects (object_freezer &freezer, int flags);
  MTH bool _save_objects (const_octet_string path, int flags);

  // save the header pseudo object _only_
  bool _save_header (object_freezer &freezer);
  MTH bool _save_header (const_octet_string path);

  maptile ();
  maptile (int w, int h);
  void init ();
  ~maptile ();

  void do_destroy ();
  void gather_callbacks (AV *&callbacks, event_type event) const;

  MTH bool linkable () { return state >= MAP_INACTIVE; }

  MTH int size () const { return width * height; }

  MTH object *insert (object *op, int x, int y, object *originator = 0, int flags = 0);

  MTH void touch () { last_access = runtime; }

  // returns the map at given direction. if the map isn't linked yet,
  // it will either return false (if load is false), or otherwise try to link
  // it - if linking fails because the map is not loaded yet, it will
  // start loading the map and return 0.
  // thus, if you get 0, the map exists and load is true, then some later
  // call (some tick or so later...) will eventually succeed.
  MTH maptile *tile_available (int dir, bool load = true);

  // find the map that is at coordinate x|y relative to this map
  // TODO: need a better way than passing by reference
  // TODO: make perl interface
  maptile *xy_find (sint16 &x, sint16 &y);

  // like xy_find, but also loads the map
  maptile *xy_load (sint16 &x, sint16 &y);

  void do_load_sync ();//PERL
 
  // make sure the map is loaded
  MTH void load_sync ()
  {
    if (!spaces)
      do_load_sync ();
  }

  void make_map_floor (char **layout, const char *floorstyle, random_map_params *RP);
  bool generate_random_map (random_map_params *RP);

  static maptile *find_async (const_utf8_string path, maptile *original = 0, bool load = true);//PERL
  static maptile *find_sync (const_utf8_string path, maptile *original = 0);//PERL
  static maptile *find_style_sync (const_utf8_string dir, const_utf8_string file = 0);//PERL
  object *pick_random_object (rand_gen &gen = rndm) const;

  mapspace &at (uint32 x, uint32 y) const { return spaces [x * height + y]; }

  // return an array of maprects corresponding
  // to the given rectangular area. the last rect will have
  // a 0 map pointer.
  maprect *split_to_tiles (dynbuf &buf, int x0, int y0, int x1, int y1);

  MTH bool is_in_shop (int x, int y) const;
};

inline bool
object::is_in_shop () const
{
  return is_on_map ()
         && map->is_in_shop (x, y);
}

//+GPL

/* This is used by get_rangevector to determine where the other
 * creature is.  get_rangevector takes into account map tiling,
 * so you just can not look the the map coordinates and get the
 * righte value.  distance_x/y are distance away, which
 * can be negative.  direction is the crossfire direction scheme
 * that the creature should head.  part is the part of the
 * monster that is closest.
 * Note: distance should be always >=0. I changed it to UINT. MT
 */
struct rv_vector
{
  unsigned int distance;
  int distance_x;
  int distance_y;
  int direction;
  object *part;
};

// comaptibility cruft start
//TODO: these should be refactored into things like xy_normalise
// and so on.
int get_map_flags (maptile *oldmap, maptile **newmap, sint16 x, sint16 y, sint16 *nx, sint16 *ny);
int out_of_map (maptile *m, int x, int y);
maptile *get_map_from_coord (maptile *m, sint16 *x, sint16 *y);
void get_rangevector (object *op1, object *op2, rv_vector *retval, int flags);
void get_rangevector_from_mapcoord (maptile *m, int x, int y, const object *op2, rv_vector *retval, int flags = 0 /*unused*/);
int on_same_map (const object *op1, const object *op2);
int adjacent_map (maptile *map1, maptile *map2, int *dx, int *dy);

// adjust map, x and y for tiled maps and return true if the position is valid at all
static inline bool
xy_normalise (maptile *&map, sint16 &x, sint16 &y)
{
  // when in range, do a quick return, otherwise go the slow route
  return
    (IN_RANGE_EXC (x, 0, map->width) && IN_RANGE_EXC (y, 0, map->height))
    || !(get_map_flags (map, &map, x, y, &x, &y) & P_OUT_OF_MAP);
}
// comaptibility cruft end

//-GPL

inline mapspace &
object::ms () const
{
  return map->at (x, y);
}

struct mapxy {
  maptile *m;
  sint16 x, y;

  mapxy (maptile *m, sint16 x, sint16 y)
  : m(m), x(x), y(y)
  { }

  mapxy (object *op)
  : m(op->map), x(op->x), y(op->y)
  { }

  mapxy &move (int dx, int dy)
  {
    x += dx;
    y += dy;

    return *this;
  }

  mapxy &move (int dir)
  {
    return move (DIRX (dir), DIRY (dir));
  }

  operator void *() const { return (void *)m; }
  mapxy &operator =(const object *op)
  {
    m = op->map;
    x = op->x;
    y = op->y;

    return *this;
  }

  mapspace *operator ->() const { return &m->at (x, y); }
  mapspace &operator * () const { return  m->at (x, y); }

  bool normalise ()
  {
    return xy_normalise (m, x, y);
  }

  mapspace &ms () const
  {
    return m->at (x, y);
  }

  object *insert (object *op, object *originator = 0, int flags = 0) const
  {
    return m->insert (op, x, y, originator, flags);
  }
};

inline const mapxy &
object::operator =(const mapxy &pos)
{
  map = pos.m;
  x   = pos.x;
  y   = pos.y;

  return pos;
}

// iterate over a rectangular area relative to op
// can be used as a single statement, but both iterate macros
// invocations must not be followed by a ";"
// see common/los.C for usage example
// the walk will be ordered, outer loop x, inner loop y
// m will be set to the map (or 0), nx, ny to the map coord, dx, dy to the offset relative to op
// "continue" will skip to the next space
#define ordered_mapwalk_begin(op,dx0,dy0,dx1,dy1)			\
 for (int dx = (dx0); dx <= (dx1); ++dx)				\
   {									\
     sint16 nx, ny;							\
     maptile *m = 0;							\
     									\
     for (int dy = (dy0); dy <= (dy1); ++dy)				\
       {								\
         /* check to see if we can simply go one down quickly, */	\
         /* if not, do it the slow way                         */	\
         if (!m || ++ny >= m->height)					\
           {								\
             nx = (op)->x + dx; ny = (op)->y + dy; m = (op)->map; 	\
     									\
             if (!xy_normalise (m, nx, ny))				\
               m = 0;							\
           }

#define ordered_mapwalk_end						\
      }									\
  }

extern dynbuf mapwalk_buf; // can be used in simple non-recursive situations

// special "grave" map used to store all removed objects
// till they can be destroyed - saves a lot of checks in the rest
// of the code
struct freed_map
: maptile
{
  freed_map ()
  : maptile (3, 3)
  {
    path     = "<freed objects map>";
    name     = "/internal/freed_objects_map";
    no_drop  = 1;
    no_reset = 1;

    state = MAP_ACTIVE;
  }

  ~freed_map ()
  {
    destroy ();
  }
};

// initialised in common/shstr.C, due to shstr usage we need defined
// initialisation order!
extern struct freed_map freed_map; // freed objects are moved here to avoid crashes

// loop over every space in the given maprect,
// setting m, nx, ny to the map and -coordinate and dx, dy to the offset relative to dx0,dy0
// the iterator code must be a single statement following this macro call, similar to "if"
// "continue" will skip to the next space
#define rect_mapwalk(rect,dx0,dy0)					\
  statementvar (maptile *, m, (rect)->m)				\
    for (int nx = (rect)->x0; nx < (rect)->x1; ++nx)			\
      for (int ny = (rect)->y0; ny < (rect)->y1; ++ny)			\
        statementvar (int, dx, nx + (rect)->dx - (dx0))			\
        statementvar (int, dy, ny + (rect)->dy - (dy0))

// same as ordered_mapwalk, but the walk will not follow any particular
// order (unorded), but is likely faster.
// m will be set to the map (never 0!), nx, ny to the map coord, dx, dy to the offset relative to op
// "continue" will skip to the next space
#define unordered_mapwalk_at(buf,map,ox,oy,dx0,dy0,dx1,dy1)		\
  for (maprect *r_e_c_t = (map)->split_to_tiles (buf,			\
         (ox) + (dx0)    , (oy) + (dy0)    ,				\
         (ox) + (dx1) + 1, (oy) + (dy1) + 1);				\
       r_e_c_t->m;							\
       ++r_e_c_t)							\
    rect_mapwalk (r_e_c_t, (ox), (oy))

#define unordered_mapwalk(buf,op,dx0,dy0,dx1,dy1)			\
  unordered_mapwalk_at (buf,op->map, op->x, op->y, dx0, dy0, dx1, dy1)

#endif

