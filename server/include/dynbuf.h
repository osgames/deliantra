/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#ifndef DYNBUF_H__
#define DYNBUF_H__

#include <cstring>
#include <cassert>

#include "compiler.h"
#include "util.h"
#include "shstr.h"

// this is a "buffer" that can grow fast
// and is still somewhat space-efficient.
// unlike obstacks or other data structures,
// it never moves data around. basically,
// this is a fast strstream without the overhead.

struct dynbuf
{
protected:
  struct chunk
  {
    chunk *next;
    int alloc;
    int size;
    char data[0];
  };

  char *ptr, *end;
  int _size;

  int extend, cextend;
  chunk *first, *last;

  void reserve (int size);
  void init (int initial);  // allocate sinitial chunk
  void free (chunk *&chain); // free chain of chunks
  char *_linearise (int extra = 0);
  void finalise ();

public:
  
  // initial - the size of the initial chunk to be allocated
  // extend  - first incremental step when buffer size exceeded
  dynbuf (int initial = 4096, int extend = 16384)
  : extend (extend)
  {
    init (initial);
  }

  ~dynbuf ()
  {
    free (first);
  }

  // resets the dynbuf, but does not free the first chunk
  // which is either of size "initial" or the size of the last
  // linearise
  void clear ();

  int  size  () const { return _size + (ptr - last->data); }
  bool empty () const { return !size (); }

  void linearise (void *data);
  char *linearise () // does not 0-terminate(!)
  {
    return first->next ? _linearise () : first->data;
  }

  int room () const { return end - ptr; }

  // make sure we have "size" extra room
  char *force (int size)
  {
    if (expect_false (ptr + size > end))
      reserve (size);

    assume (ptr + size <= end);

    return ptr;
  }

  // used for force + alloc combo
  void alloc (char *p)
  {
    ptr = p;
  }

  // allocate size bytes and return pointer to them (caller must force())
  char *falloc (int size)
  {
    char *res = ptr;
    ptr += size;
    return res;
  }

  // allocate size bytes and return pointer to them
  char *alloc (int size)
  {
    force (size);
    return falloc (size);
  }

  void fadd (char c) { *ptr++ = c; }
  void fadd (unsigned char c) { fadd (char (c)); }
  void fadd (const void *p, int len)
  {
    memcpy (falloc (len), p, len);
  }

  void add (const void *p, int len)
  {
    force (len);
    fadd (p, len);
  }

  void add (char c)
  {
    alloc (1)[0] = c;
  }

  void add (const char *s)
  {
     add (s, strlen (s));
  }

  void add (shstr_tmp s)
  {
    add (s.s, s.length ());
  }

  // rather inefficient
  void splice (int offset, int olen, const char *s, int slen);
  void splice (int offset, int olen) { splice (offset, olen, "", 0); }

  //TODO
  //void add_destructive (dynbuf &buf);

  dynbuf &operator << (char c) { add (c); return *this; }
  dynbuf &operator << (unsigned char c) { return *this << char (c); }
  dynbuf &operator << (const char *s) { add (s); return *this; }
  dynbuf &operator << (shstr_tmp s) { add (s); return *this; }
  dynbuf &operator << (const std::string &s) { add (s.data(), s.size ()); return *this; }

  operator std::string ();
};

struct dynbuf_text : dynbuf
{
  dynbuf_text (int initial = 4096, int extend = 16384)
  : dynbuf (initial, extend)
  { }

  using dynbuf::add;
  void add (sint32 i);
  void add (sint64 i);

  //TODO: should optimise the case printf "(name %+d)" as it comes up extremely often

  //using dynbuf::operator <<; // doesn't work, sometimes C++ just suxx
  // instead we use an ugly template function
  template<typename T>
  dynbuf_text &operator << (T c) { *(dynbuf *)this << c; return *this; }

  dynbuf_text &operator << (sint16 i) { add (sint32 (i)); return *this; }
  dynbuf_text &operator << (uint16 i) { add (sint32 (i)); return *this; }
  dynbuf_text &operator << (sint32 i) { add (sint32 (i)); return *this; }
  dynbuf_text &operator << (sint64 i) { add (sint64 (i)); return *this; }
  dynbuf_text &operator << (uint32 i) { add (sint64 (i)); return *this; }
  dynbuf_text &operator << (uint64 i) { add (sint64 (i)); return *this; }

  void printf (const char *format, ...) attribute ((format (printf, 2, 3)));
  void vprintf (const char *format, va_list ap);

  void add_abilities (const char *name, uint32 abilities);
  void add_paths     (const char *name, uint32 paths);

  // we need to redefine, to keep the API :/
  using dynbuf::splice;
  void splice (int offset, int olen, const char *s)
  {
    dynbuf::splice (offset, olen, s, strlen (s));
  }

  // returns the string, linearised and with trailing \0
  operator char *();
};

#endif

