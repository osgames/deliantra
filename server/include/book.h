/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

/* Dec '95 - laid down initial file. Stuff in here is for BOOKs
 * hack. Information in this file describes fundental parameters
 * of 'books' - objects with type==BOOK. -b.t.
 */
 
/* Message buf size. If this is changed, keep in mind that big strings
 * may be unreadable by the player as the tail of the message
 * can scroll over the beginning (as of v0.92.2).
 */
#ifndef BOOK_H
#define BOOK_H

/* try not to create books much larger than this */
#define BOOK_BUF        2000
 
/* if little books arent getting enough text generated, enlarge this */
 
#define BASE_BOOK_BUF   250
 
/* Book buffer size. We shouldnt let little books/scrolls have
 * more info than big, weighty tomes! So lets base the 'natural'
 * book message buffer size on its weight. But never let a book
 * mesg buffer exceed the max. size (BOOK_BUF) */
 
#define BOOKSIZE(xyz)   BASE_BOOK_BUF+((xyz)->weight/10)>BOOK_BUF? \
                                BOOK_BUF:BASE_BOOK_BUF+((xyz)->weight/10)

struct readable_message_type
{
  uint8 message_type;
  uint8 message_subtype;
  const char *msgtype;
};

#endif

