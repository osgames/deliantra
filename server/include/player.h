/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#ifndef PLAYER_H_
#define PLAYER_H_

//+GPL

enum bowtype_t
{
  bow_normal     = 0,
  bow_threewide  = 1,
  bow_spreadshot = 2,
  bow_n  = 3,                    /* must stay at 3 */
  bow_ne = 4,
  bow_e  = 5,
  bow_se = 6,
  bow_s  = 7,
  bow_sw = 8,
  bow_w  = 9,
  bow_nw = 10,                  /* must stay at 10 */
  bow_bestarrow = 11
};

enum petmode_t
{
  pet_normal = 0,
  pet_sad    = 1,
  pet_defend = 2,
  pet_arena  = 3,
};

enum usekeytype
{
  key_inventory = 0,
  keyrings      = 1,
  containers    = 2,
};

/* This is used to control what to do when we need to unapply
 * an object before we can apply another one.
 */
enum unapplymode
{
  unapply_nochoice = 0,         /* Will unapply objects when there no choice to unapply */
  unapply_never = 1,            /* will not unapply objects automatically */
  unapply_always = 2            /* Will unapply whatever is necessary - this goes beyond */
    /* no choice - if there are multiple ojbect of the same type */
    /* that need to be unapplied, there is no way for the player */
    /* to control which of these will be unapplied. */
};

/* not really the player, but tied pretty closely */
INTERFACE_CLASS (partylist)
struct partylist
{
  utf8_string ACC (RW, partyleader);
  char ACC (RW, passwd)[9];
  partylist *ACC (RW, next);
  utf8_string ACC (RW, partyname);

  struct party_kill
  {
    char killer[MAX_NAME + 1], dead[MAX_NAME + 1];
    sint64 exp;
  } party_kills[PARTY_KILL_LOG];

  sint64 ACC (RW, total_exp);
  uint32 ACC (RW, kills);
};

// used for pet monster logic etc.
static inline bool
same_party (partylist *a, partylist *b)
{
  return a == b && a;
}

INTERFACE_CLASS (player)
struct player : zero_initialised, attachable
{
  client *ACC (RO, ns);               /* Socket information for this player, ALWAYS valid when a player is on a map */
  object *ACC (RW, ob);               /* The object representing the player, ALWAYS valid */
  object_vector_index ACC (RO, active);

  bowtype_t ACC (RW, bowtype);        /* which firemode? */
  petmode_t ACC (RW, petmode);        /* which petmode? */
  usekeytype ACC (RW, usekeys);       /* Method for finding keys for doors */
  unapplymode ACC (RW, unapply);      /* Method for auto unapply */
  uint32 ACC (RW, count);             /* Any numbers typed before a command */
  uint32 ACC (RW, mode);              /* Mode of player for pickup. */

  int ACC (RW, digestion);            /* Any bonuses/penalties to digestion */
  int ACC (RW, gen_hp);               /* Bonuses to regeneration speed of hp */
  int ACC (RW, gen_sp);               /* Bonuses to regeneration speed of sp */
  int ACC (RW, gen_sp_armour);        /* Penalty to sp regen from armour */
  int ACC (RW, gen_grace);            /* Bonuses to regeneration speed of grace */
  int ACC (RW, item_power);           /* Total item power of objects equipped */
  uint8 ACC (RW, gender);             /* 0 male, 1 female, others not yet defined */
  uint8 ACC (RW, hintmode);           /* 0 full, 1 hint, 2 disable */

  /* Try to put all the bitfields together - saves some small amount of memory */
  bool ACC (RW, braced);              /* Will not move if braced, only attack */
  bool ACC (RW, tmp_invis);           /* Will invis go away when we attack ? */
  bool ACC (RW, do_los);              /* If true, need to call update_los() in draw(), and clear */
  bool ACC (RW, fire_on);             /* Player should fire object, not move */
  bool ACC (RW, run_on);              /* Player should keep moving in dir until run is off */
  bool ACC (RW, peaceful);            /* If set, won't attack friendly creatures */
  bool ACC (RW, hidden);              /* If True, player (DM) is hidden from view */
  bool ACC (RW, dirty);               // set if player is dirty (not reliable yet!)

  float ACC (RW, weapon_sp);              /* Penalties to speed when fighting w speed >ws/10 */
  float ACC (RW, weapon_sp_left);         // same as speed_left, but for attacks
  living ACC (RO, orig_stats);            /* Permanent real stats of player */
  object_ptr last_skill_ob[CS_NUM_SKILLS];   /* the exp object */
  object_ptr ACC (RW, last_used);            /* Pointer to object last picked or applied */

  object_ptr ACC (RW, combat_ob);     // which weapon/bow/skill to use for direct attacks
  object_ptr ACC (RW, ranged_ob);     // which skill/item/spell to use for ranged attacks
  object_ptr ACC (RW, golem);         // the currently controlled golem
  object_ptr ACC (RW, observe);       // the object that is being observed (never 0)
  object_ptr ACC (RW, viewpoint);     // the object that is being viewed in the map (never 0)

  sint16 ACC (RW, bed_x), ACC (RW, bed_y);          /* x,y - coordinates of respawn (savebed) */
  shstr ACC (RW, savebed_map);    /* map where player will respawn after death */
  shstr ACC (RW, maplevel);       /* On which level is the player? */
  char ACC (RW, spellparam)[MAX_BUF];     /* What param to add to spells */

  char ACC (RW, own_title)[MAX_NAME];     /* Title the player has chosen for themself */
  /* Note that for dragon players, this is filled in for them */
  char ACC (RW, title)[64];         /* Default title, like fighter, wizard, etc */

  sint8 ACC (RW, levhp[11]);              /* What the player gained on that level */
  sint8 ACC (RW, levsp[11]);              /* Same for sp */
  sint8 ACC (RW, levgrace[11]);           /* And same for grace */

  object_ptr ACC (RW, killer);            /* Who last tried to kill this player (this object is usually destroyed) */

  float speed_left_save;                  // speed optimisation, see process_players[12]
  char write_buf[MAX_BUF];                /* Holds arbitrary input from client */ /* should go */
  char ACC (RW, password)[256];           /* 2 (seed) + 11 (crypted) + 1 (EOS) + 2 (safety) = 16 */

  partylist *ACC (RW, party);             /* Party this player is part of */
  partylist *ACC (RW, party_to_join);     /* used when player wants to join a party */
  /* but we will have to get password first */
  /* so we have to remember which party to */
  /* join */
  char ACC (RW, search_str)[256];         /* Item we are looking for */
  sint16 ACC (RW, encumbrance);           /* How much our player is encumbered  */
  uint16 ACC (RW, outputs_sync);          /* How often to print, no matter what */
  uint16 ACC (RW, outputs_count);         /* Print if this count is exceeded */
  object_ptr ACC (RW, mark);                 /* marked object */
  /* Special DM fields */
  tag_t *stack_items;           /* Item stack for patch/dump/... commands */
  int ACC (RW, stack_position);           /* Current stack position, 0 for no item */
  sint8 los[MAP_CLIENT_X][MAP_CLIENT_Y];        /* array showing what spaces */
                                                /* the player can see.  For maps smaller than */
                                                /* MAP_CLIENT_.., the center is used */

//-GPL

  // stats updates are very costly, so delay them if at all possible
  bool ACC (RW, delayed_update);
  void queue_stats_update ()
  {
    delayed_update = true;
  }
  void need_updated_stats ()
  {
    if (delayed_update)
      ob->update_stats ();
  }

#if 0
  enum {
    SA_SPELLS = 1<<0,
    SA_EXP    = 1<<1,
  };

  uint8 ACxC (RW, stat_update_flags);

  void need_stat_update (int flags)
  {
    stat_update_flags |= flags;
  }
#endif

  // return the los value for the given coordinate
  MTH sint8 blocked_los (int dx, int dy) const
  {
    dx += LOS_X0;
    dy += LOS_Y0;

    return 0 <= dx && dx < MAP_CLIENT_X
        && 0 <= dy && dy < MAP_CLIENT_Y
           ? los[dx][dy] : LOS_BLOCKED;
  }

  // unchecked variant
  sint8 blocked_los_uc (int dx, int dy) const
  {
    return los[dx + LOS_X0][dy + LOS_Y0];
  }

  MTH void clear_los (sint8 value = LOS_BLOCKED);
  MTH void update_los ();

  shstr ACC (RW, invis_race);             /* What race invisible to? */

  MTH const_utf8_string killer_name () const; // makes a string out of ->killer

  MTH static player *create ();
  static player *find (const_utf8_string name);

  static player *load_pl (object_thawer &thawer);
  MTH static player *load_pl (object_thawer *thawer) { return load_pl (*thawer); }

  MTH void link_skills ();
  MTH object *find_skill (shstr_cmp name) const;

  bool save_pl (object_freezer &freezer);
  MTH bool save_pl (const_octet_string path);

  void do_destroy ();
  void gather_callbacks (AV *&callbacks, event_type event) const;

  MTH dynbuf_text *expand_cfpod (const_utf8_string cfpod) const;
  static dynbuf_text *expand_cfpod (int hintmode, int gender, const_utf8_string cfpod);

  MTH void touch () { dirty = true; } // need to touch when logged out and changed

  MTH void play_sound (faceidx sound, int dx = 0, int dy = 0) const
  {
    if (ns)
      ns->play_sound (sound, dx, dy);
  }

  // wether the player can "see" this mapspace or not, decided by los
  // 0 - bright, 3 dark, 4 too dark, 100 blocked or out of range
  MTH sint8 darkness_at (maptile *map, int x, int y) const;

  MTH void connect (client *ns);
  MTH void disconnect ();

  MTH void activate ();
  MTH void deactivate ();

  MTH void chargen_race_done ();
  MTH void chargen_race_next ();

  MTH void set_observe (object_ornull *ob);
  MTH void set_viewpoint (object_ornull *ob);

  void send_msg (int color, const_utf8_string type, const_utf8_string msg)
  {
    ns->send_msg (color, type, msg);
  }

  // a prominent box that can easily be escaped away or so
  // should be used for informative output such as who, maps etc.
  // will stay on-screen
  MTH void infobox (const_utf8_string title, const_utf8_string msg, int color = NDI_BLACK);

  // a prominent msg that signifies some important event,
  // an improvement potion effect potion. should not be long.
  // might time out after a while
  MTH void statusmsg (const_utf8_string msg, int color = NDI_BLACK);

  // a prominent box that signifies some error such as a failed
  // improvement potion. should not be long.
  MTH void failmsg (const_utf8_string msg, int color = NDI_RED);
  void failmsgf (const_utf8_string format, ...); // always NDI_RED

  MTH void update_spells () const
  {
    if (ns)
      ns->update_spells = true;
  }

  ~player ();

private:
  void set_object (object *op);
  player ();
};

typedef object_vector<player, &player::active> playervec;

extern playervec players;

#define for_all_players(var)			\
  for (unsigned _i = 0; _i < ::players.size (); ++_i)	\
    statementvar (player *, var, ::players [_i])

#define for_all_players_on_map(var,mapp)		\
  for_all_players(var)					\
    if ((var)->ob->map == (mapp))

inline void
object::statusmsg (const_utf8_string msg, int color)
{
  if (expect_true (contr)) contr->statusmsg (msg, color);
}

inline void
object::failmsg (const_utf8_string msg, int color)
{
  if (expect_true (contr)) contr->failmsg (msg, color);
}

#endif

