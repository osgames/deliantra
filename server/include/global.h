/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#ifndef GLOBAL_H
#define GLOBAL_H

//#define _GLIBCXX_CONCEPT_CHECKS

#ifndef EXTERN
#define EXTERN extern
#endif

// used only for tagging structure members so scripting languages
// can easily parse the include files.
#define INTERFACE_CLASS(name)
#define ACC(meta,member) member
#define MTH
#define GENCONST_IV(patterns)

#include "includes.h"
#include "config.h"
#include "compiler.h"
#include "define.h"
#include "traits.h"

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

extern short freearr_x[SIZEOFFREE];
extern short freearr_y[SIZEOFFREE];
extern int freedir[SIZEOFFREE];
extern int rightof_x[9], rightof_y[9];
extern int leftof_x[9], leftof_y[9];

extern sint64 levels [MAXNUMLEVELS];

#include "keyword.h"
#include "logger.h"
#include "dynbuf.h"
#include "util.h"
#include "rng.h"
#include "shstr.h"
#include "cfperl.h"

/* This blob, in this order, is needed to actually define maps */
#include "face.h"
#include "sounds.h"

#include "spells.h"

#include "attack.h" /* needs to be before material.h */
#include "material.h" /* needs to be before freezethaw.h */
#include "living.h"
#include "object.h"
#include "region.h"
#include "map.h"
#include "tod.h"

#include "skills.h"

/* Pull in the socket structure - used in the player structure */
#include "network.h"
#include "client.h"

/* Pull in the player structure */
#include "player.h"

/* pull in treasure structure */
#include "treasure.h"

/* pull in book structures */
#include "book.h"

#include "quadland.h"

/*
 * So far only used when dealing with artifacts.
 * (now used by alchemy and other code too. Nov 95 b.t).
 */
struct linked_char
{
  shstr name;
  struct linked_char *next;
};

/* Pull in artifacts */
#include "artifact.h"

/* Now for gods */
#include "god.h"

/* Now for races */
#include "race.h"

/* Now for recipe/alchemy */
#include "recipe.h"

/*****************************************************************************
 * GLOBAL VARIABLES:							     *
 *****************************************************************************/

/*
 * These are the beginnings of linked lists:
 */
EXTERN artifactlist *first_artifactlist;
EXTERN objectlink *first_friendly_object;	/* Objects monsters will go after */
EXTERN godlink *first_god;
EXTERN racelink *first_race;

/*
 * Variables set by different flags (see init.c):
 */

EXTERN bool init_done;		/* Ignores signals until init_done is true */
EXTERN bool in_cleanup;		/* True when cleanup() is reached */

extern tick_t server_tick;	/* used by various function to determine */
                                /* how often to save the character */
/*
 * Misc global variables:
 */
EXTERN shstr first_map_path;	/* The start-level */
EXTERN shstr first_map_ext_path; /* Path used for per-race start maps */

EXTERN long ob_count;

extern dynbuf_text msg_dynbuf; // a dynbuf for text messages and other temporary data

/* do not use these in new code, see object.h::animation */
#define SET_ANIMATION(ob,frame) ob->set_anim_frame (frame)
#define NUM_ANIMATIONS(ob) (ob)->anim_frames ()
#define NUM_FACINGS(ob)    (ob)->anim_facings ()

// used only by treasure.C, does not handle null arch ptrs
#define IS_ARCH(arch,name) ((arch)->archname == shstr_ ## name)

extern void emergency_save ();

#include "libproto.h"
#include "sockproto.h"

INTERFACE_CLASS (Settings)
struct Settings {
  const char *ACC (RO, logfilename);   /* logfile to use */
  int         ACC (RW, debug);	    /* Default debugging level */
  int	    argc;
  char    **argv;
  const char *ACC (RO, confdir);	    /* configuration files */
  const char *ACC (RO, datadir);	    /* read only data files */
  const char *ACC (RO, localdir);	    /* read/write data files */
  const char *ACC (RO, playerdir);	    /* Where the player files are */
  const char *ACC (RO, mapdir);	    /* Where the map files are */
  const char *ACC (RO, uniquedir);	    /* directory for the unique items */
  const char *ACC (RO, tmpdir);	    /* Directory to use for temporary files */
  sint16  ACC (RW, pk_luck_penalty);    /* Amount by which player luck is reduced if they PK */
  uint8   ACC (RW, stat_loss_on_death);	/* If true, chars lose a random stat when they die */
  uint8   ACC (RW, permanent_exp_ratio); /* how much exp should be 'permenant' and unable to be lost*/
  uint8   ACC (RW, death_penalty_ratio); /* how much exp should be lost at death */
  uint8   ACC (RW, death_penalty_level); /* how many levels worth of exp may be lost on one death */
  uint8   ACC (RW, balanced_stat_loss); /* If true, Death stat depletion based on level etc */
  uint8   ACC (RW, not_permadeth);  /* if true, death is non-permament */
  uint8   ACC (RW, simple_exp);	    /* If true, use the simple experience system */
  uint8   ACC (RW, set_title);	    /* players can set thier title */
  uint8   ACC (RW, resurrection);   /* ressurection possible w/ permadeth on */
  uint8   ACC (RW, search_items);   /* search_items command */
  uint8   ACC (RW, spell_encumbrance); /* encumbrance effects spells */
  uint8   ACC (RW, spell_failure_effects); /* nasty backlash to spell failures */
  uint16  ACC (RW, set_friendly_fire);	/* Percent of damage done by peaceful player vs player damage */
  uint8   ACC (RW, spellpoint_level_depend); /* spell costs go up with level */

  int     ACC (RW, max_level);		    /* This is read out of exp_table */
  float   ACC (RW, item_power_factor);	    /* See note in setings file */

  int     ACC (RW, armor_max_enchant);  /* Maximum number of times an armor can be enchanted */
  int     ACC (RW, armor_weight_reduction); /* Weight reduction per enchantment */
  int     ACC (RW, armor_speed_improvement);    /* Speed improvement */
  uint8   ACC (RW, armor_weight_linear);  /* If 1, weight reduction is linear, else exponantiel */
  uint8   ACC (RW, armor_speed_linear);         /* If 1, speed improvement is linear, else exponantiel */
};

extern Settings settings;

void reset_signals ();

#ifdef DEVEL
# include "devel.h"
#endif

#endif /* GLOBAL_H */

