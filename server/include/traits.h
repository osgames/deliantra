/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

/* define traits (actually just types at the moment) for basic types */

#ifndef TRAITS_H__
#define TRAITS_H__

#include <inttypes.h>

typedef int8_t		sint8;
typedef uint8_t		uint8;
typedef int16_t		sint16;
typedef uint16_t	uint16;
typedef int32_t		sint32;
typedef uint32_t	uint32;
typedef int64_t		sint64;
typedef uint64_t	uint64;

typedef uint32		weight_t;
typedef sint64		volume_t;
typedef uint32		tick_t;
typedef uint16		faceidx;

inline weight_t
weight_to_kg_approx (weight_t w)
{
  // we divide by 1024, because otherwise we need a 64 bit multiply
  return w >> 10;
}

const int sint32_digits = 11; // number of digits an sint32 uses max.
const int sint64_digits = 20;

typedef char *octet_string;
typedef char *utf8_string;
typedef char *any_string;
typedef const char *const_octet_string;
typedef const char *const_utf8_string;
typedef const char *const_any_string;

struct client_container;
struct client;
struct player;
struct object;
struct maptile;
struct mapspace;
struct archetype;
struct region;
struct party;
struct treasurelist;
struct random_map_params;
struct faceinfo;
struct mapxy;
struct materialtype_t;

typedef object  object_ornull;
typedef maptile maptile_ornull;

// could have used templates, but a more traditional C api
// uses more explicit typing which is ok for this purpose.
enum data_type
{
  DT_END,     // no further arguments
  DT_AV,      // perl-only av that needs to be flattened out
  DT_INT,
  DT_INT64,
  DT_DOUBLE,
  DT_STRING,  // 0-terminated string
  DT_DATA,    // string + length
  DT_ATTACHABLE, // will go
  DT_OBJECT,
  DT_MAP,
  DT_CLIENT,
  DT_PLAYER,
  DT_ARCH,
  DT_PARTY,
  DT_REGION,

  NUM_DATA_TYPES
};

#endif

