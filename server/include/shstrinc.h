/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

// declare some predefined const shstr's

def (none)
def (NONE) //TODO, eradicate
def (All) //TODO, eradicate
def (NOCLASSFACECHANGE)
def (monk)
def (unknown)
def2(shstr_unknown_material_description, "an unknown substance")
def (bug)
def (player)
def (undead)
def (money)
def (clawing)
def (battleground)
def (shop_coords)
def (gold)
def (starting_wealth)
def (on_use_yield)
def (organic)
def (burnout)
def (icecube)
def (fire_trail)
def (noise_force)
def (killer_quit)
def (killer_logout)
def (rune_mark)
def (magic_mouth)
def (doppleganger)
def (keys)
def (GOD)
def (GOD_SLAYING)
def (GOD_FRIEND)
def (GODCULTMON)
def2(shstr_empty, "")
def2(shstr_one_handed_weapons, "one handed weapons")
def2(shstr_two_handed_weapons, "two handed weapons")
def2(shstr_missile_Weapons, "missile weapons")
def2(shstr_grace_limit, "grace limit")
def2(shstr_restore_grace, "restore grace")

// magical map exit path to identify random map generators
def2(shstr_random_map_exit, "/!")

// archetypes
def (poisoning)
def (confusion)
def (blindness)
def (rock)
def (fireball)
def (immunity)
def (symptom)
def (runedet)
def (detect_magic)
def (luck)
def (earthwall)
def (slowness)
def (finger)
def (depletion)
def (gravestone)
def (pyrite)
def (pyrite2)
def (pyrite3)
def (stone)
def (slag)
def (slags)
def (bomb)
def (quad_open_space)
def (quad_water_source)
def (quad_water_flow)

// dragons
def (dragon)
def (dragon_ability_force)
def (dragon_ability_fire)
def (dragon_ability_cold)
def (dragon_ability_elec)
def (dragon_ability_poison)
def (dragon_skin_force)

//treasures
def (ring)
def (amulet)
def (staff)
def (crown)
def (potion)
def (potions)
def (empty_archetype)

// hardcoded skill names
def (praying)
def (literacy)
def (throwing)
def (bargaining)

// rmg
def (fountain)
def (adamantium)
def (exit)
def (chest)
def (sign)
def (key_random_map)
def (locked_door1)

