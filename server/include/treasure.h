/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

/*
 * defines and variables used by the artifact generation routines
 */

#ifndef TREASURE_H
#define TREASURE_H

#define MAGIC_VALUE		10000	/* Might need to increase later */
#define CHANCE_FOR_ARTIFACT	20

#define STARTMAL        100
#define STEPMAL         10

/* List, What type to clone_arch(), max magic, how many */

#define MAXMAGIC 4

#define DIFFLEVELS 31

/*
 * Flags to generate_treasures():
 */
enum
{
  GT_ENVIRONMENT = 0x0001,  // put treasure at object, not into object
  GT_INVISIBLE   = 0x0002,
  GT_STARTEQUIP  = 0x0004,
  GT_APPLY       = 0x0008,
  GT_ONLY_GOOD   = 0x0010,
  GT_MINIMAL     = 0x0020,  // Do minimal adjustments
};

/* when a treasure got cloned from archlist, we want perhaps change some default
 * values. All values in this structure will override the default arch.
 * TODO: It is a bad way to implement this with a special structure.
 * Because the real arch list is a at runtime not changed, we can grap for example
 * here a clone of the arch, store it in the treasure list and then run the original
 * arch parser over this clone, using the treasure list as script until an END comes.
 * This will allow ANY changes which is possible and we use ony one parser.
 */

typedef struct _change_arch
{
  shstr name;              /* is != NULL, copy this over the original arch name */
  shstr title;             /* is != NULL, copy this over the original arch name */
  shstr slaying;           /* is != NULL, copy this over the original arch name */
} _change_arch;

/*
 * treasure is one element in a linked list, which together consist of a
 * complete treasure-list.  Any arch can point to a treasure-list
 * to get generated standard treasure when an archetype of that type
 * is generated (from a generator)
*/
struct treasure : zero_initialised
{
  arch_ptr item;	 	   /* Which item this link can be */
  shstr name;			   /* If non null, name of list to use
                                      instead */
  treasure *next;	    	   /* Next treasure-item in a linked list */
  treasure *next_yes;  		   /* If this item was generated, use */
                                   /* this link instead of ->next */
  treasure *next_no;               /* If this item was not generated, */
                                   /* then continue here */
  struct _change_arch change_arch; /* override default arch values if set in treasure list */
  uint16 chance;		   /* Percent chance for this item */
                                   /* If the entry is a list transition,
                                    * 'magic' contains the difficulty
                                    * required to go to the new list
 				    */
  uint16 nrof;			   /* random 1 to nrof items are generated */
  uint8 magic;			   /* Max magic bonus to item */

  treasure ()
  : chance (100)
  { }
};

struct treasurelist : zero_initialised
{
  shstr name;				/* Usually monster-name/combination */
  sint16 total_chance;			/* If non-zero, only 1 item on this
                                         * list should be generated.  The
                                         * total_chance contains the sum of
                                         * the chance for this list.
                                         */
  treasurelist *next;	/* Next treasure-item in linked list */
  treasure *items;		/* Items in this list, linked */

  void create (object *op, int flag, int difficulty);

  static treasurelist *read (object_thawer &f);
  static treasurelist *get  (const char *name); // find or create
  static treasurelist *find (const char *name);
};

inline void
object_freezer::put (const keyword_string k, treasurelist *v)
{
  if (expect_true (v))
    put (k, v->name);
  else
    put (k);
}

void create_treasure (treasurelist *t, object *op, int flag, int difficulty, int tries = 0);

#endif

