/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

/* This file contains various #defines that select various options.
 * Some may not be desirable, and some just may not work.
 *
 * There are some options that are not selectable in this file which
 * may not always be undesirable.  An example would be certain
 * treasures that you may not want to have available.  To remove the
 * activation code would make these items worthless - instead remove
 * these from the treasure file.  Some things to look for are:
 *
 * prepare_weapon, improve_*: Allow characters to enchant their own
 *	weapons
 * ench_armour: Allow characters to enchant their armor.
 *
 * In theory, most of the values here should just be defaults, and
 * everything here should just be selectable by different run time
 * flags  However, for some things, that would just be too messy.
 */

/* There are 4 main sections to this file-
 * Section 1 is feature selection (enabling/disabling certain features)
 *
 * Section 2 is compiler/machine dependant section (stuff that just
 *     makes the program compile and run properly, but don't change the
 *     behavior)
 *
 * Section 3 is location of certain files and other defaults.  Things in
 *     this section generally do not need to be changed, and generally do
 *     not alter the play as perceived by players.  However, you may
 *     have your own values you want to set here.
 *
 * Section 4 deals with save file related options.
 */

 /*******************************************************************
 * SECTION 1 - FEATURES
 *
 * You don't have to change anything here to get a working program, but
 * you may want to on personal preferance.  Items are arranged
 * alphabetically.
 *
 * Short list of features, and what to search for:
 * DEBUG - more verbose message logging?
 * MAP_CLIENT_X, MAP_CLIENT_Y - determines max size client map will receive
 * MAX_TIME - how long an internal tick is in microseconds
 * MANY_CORES - generate core dumps on gross errors instead of continuing?
 * PARTY_KILL_LOG - stores party kill information
 *
 ***********************************************************************/

/* Use a very easy, non-challenging server?
 * Defining the COZY_SERVER will make the server much less challenging:
 * no stats loss on death, much less experience loss.
 */
#define COZY_SERVER 1

/* Use balanced stat loss code?
 * This code is a little more merciful with repeated stat loss at lower
 * levels. Basically, the more stats you have lost, the less likely that
 * you will lose more. Additionally, lower level characters are shown
 * a lot more mercy (there are caps on how much of a stat you can lose too).
 * On the nasty side, if you are higher level, you can lose mutiple stats
 * _at_once_ and are shown less mercy when you die. But when you're higher
 * level, it is much easier to buy back your stats with potions.
 * Turn this on if you want death-based stat loss to be more merciful
 * at low levels and more cruel at high levels.
 * Only works when stats are depleted rather than lost. This option has
 * no effect if you are using genuine stat loss.
 *
 * The BALSL_.. values control this behaviour.
 * BALSL_NUMBER_LOSSES_RATIO determines the number of stats to lose.
 * the character level is divided by that value, and that is how many
 * stats are lost.
 *
 * BALSL_MAX_LOSS_RATIO puts the upper limit on depletion of a stat -
 * basically, level/max_loss_ratio is the most a stat can be depleted.
 *
 * BALSL_LOSS_CHANCE_RATIO controls how likely it is a stat is depleted.
 * The chance not to lose a stat is
 * depleteness^2 / (depletedness^2+ level/ratio).
 * ie, if the stats current depleted value is 2 and the character is level
 * 15, the chance not to lose the stat is 4/(4+3) or 4/7.  The higher the
 * level, the more likely it is a stat can get really depleted, but
 * this gets more offset as the stat gets more depleted.
 *
 */
/* GD */

#define BALSL_LOSS_CHANCE_RATIO    100
#define BALSL_NUMBER_LOSSES_RATIO  100
#define BALSL_MAX_LOSS_RATIO       100


/* Don't edit these values.  They are configured in lib/settings.  These are
   Simply the defaults. */

#define BALANCED_STAT_LOSS TRUE
#define PERMANENT_EXPERIENCE_RATIO 30
#define DEATH_PENALTY_RATIO 10
#define DEATH_PENALTY_LEVEL 2
#define SET_TITLE TRUE
#define SIMPLE_EXP TRUE
#define SPELLPOINT_LEVEL_DEPEND TRUE
#define SPELL_ENCUMBRANCE FALSE
#define SPELL_FAILURE_EFFECTS FALSE
#define RESURRECTION FALSE
#define SEARCH_ITEMS TRUE
#define NOT_PERMADETH TRUE
#define STAT_LOSS_ON_DEATH FALSE
#define PK_LUCK_PENALTY 0
#define SET_FRIENDLY_FIRE 0
#define ARMOR_MAX_ENCHANT   5
#define ARMOR_WEIGHT_REDUCTION  10
#define ARMOR_WEIGHT_LINEAR TRUE
#define ARMOR_SPEED_IMPROVEMENT 10
#define ARMOR_SPEED_LINEAR  TRUE
#define CREATE_HOME_PORTALS FALSE

/* you can edit the ones below */

/* DEBUG generates copious amounts of output.  I tend to change the CC options
 * in the crosssite.def file if I want this.  By default, you probably
 * dont want this defined.
 */
#ifndef DEBUG
# define DEBUG
#endif
/*
 * This option creates more core files.  In some areas, there are certain
 * checks done to try and make the program more stable (ie, check
 * parameter for null, return if it is).  These checks are being done
 * for things that should not happen (ie, being supplied a null parameter).
 * What MANY_CORES does, is if one of these checks is true, it will
 * dump core at that time, allowing for fairly easy tracking down of the
 * problem.  Better to fix problems than create thousands of checks.
 */

#define MANY_CORES

/*
 * This determines the maximum map size the client can request (and
 * thus what the server will send to the client.
 * Client can still request a smaller map size (for bandwidth reasons
 * or display size of whatever else).
 * The larger this number, the more cpu time and memory the server will
 * need to spend to figure this out in addition to bandwidth needs.
 * The server cpu time should be pretty trivial.
 * There may be reasons to keep it smaller for the 'classic' crossfire
 * experience which was 11x11.  Big maps will likely make the same at
 * least somewhat easier, but client will need to worry about lag
 * more.
 * I put support in for non square map updates in the define, but
 * there very well might be things that break horribly if this is
 * used.  I figure it is easier to fix that if needed than go back
 * at the future and have to redo a lot of stuff to support rectangular
 * maps at that point.
 *
 * MSW 2001-05-28
 */

// both must be 2**n
#define MAP_CLIENT_X	32
#define MAP_CLIENT_Y	32

/*
 * If you feel the game is too fast or too slow, change MAX_TIME.
 * You can experiment with the 'speed <new_max_time> command first.
 * The length of a tick is MAX_TIME microseconds.  During a tick,
 * players, monsters, or items with speed 1 can do one thing.
 */

#define MAX_TIME	120000

/*
 * MAX_ITEM_PER_ACTION defines how many items a player can drop/take etc. at once.
 * (be careful to set this to high values, as dropping lots of things
 * can be a performance problem (for which a fix is worked on)).
 */

#define MAX_ITEM_PER_ACTION 100

/* Polymorph as it currently stands is unbalancing, so by default
 * we have it disabled.  It can be enabled and it works, but
 * it can be abused in various ways.
 */
#define NO_POLYMORPH


/* This determine how many entries are stored in the kill log.  You
 * can see this information with the 'party kills' command. More entries
 * mean slower performance and more memory.
 */
#define PARTY_KILL_LOG 40

/*
 * The PERM_EXP values adjust the behaviour of permenent experience. - if
 * the setting permanent_experience_percentage is zero, these values have
 * no meaning. The value in the settings file is the percentage of the
 * experience that is permenent, the rest could be lost on death. When dying,
 * the greatest amount of non-permenent exp it is possible to lose at one time
 * is PERM_EXP_MAX_LOSS_RATIO  - this is calculated as
 * total exp - perm exp * loss ratio. The gain ratio is how much of experienced
 * experience goes to the permanent value. This does not detract from total
 * exp gain (ie, if you gained 100 exp, 100 would go to the skill total and
 * 10 to the permanent value).
 *
 * A few thoughts on these default value (by MSW)
 * gain ratio is pretty much meaningless until exp has been lost, as until
 * that poin, the value in the settings file will be used.
 * It is also impossible for the exp to actually be reduced to the permanent
 * exp ratio - since the loss ratio is .5, it will just get closer and
 * closer.  However, after about half a dozen hits, pretty much all the
 * exp that can be lost has been lost, and after that, only minor loss
 * will occur.
 */
/* GD */

#define PERM_EXP_GAIN_RATIO           0.10f
#define PERM_EXP_MAX_LOSS_RATIO       0.50f

/***********************************************************************
 * Section 3
 *
 * General file and other defaults that don't need to be changed, and
 * do not change gameplay as percieved by players much.  Some options
 * may affect memory consumption however.
 *
 * Values:
 *
 * DMFILE - file with dm/wizard access lists
 * LOGFILE - where to log if using -daemon option
 * MAP_ - various map timeout and swapping parameters
 * MAX_OBJECTS - how many objects to keep in memory.
 * MAX_OBJECTS_LWM - only swap maps out if below that value
 * PERM_FILE - limit play times
 * SHUTDOWN - used when shutting down the server
 * SOCKETBUFSIZE - size of buffer used internally by the server for storing
 *    backlogged messages.
 * TMPDIR - directory to use for temp files
 * UNIQUE_DIR - directory to put unique item files into
 ***********************************************************************
 */

/*
 * DMFILE
 * A file containing valid names that can be dm, one on each line.  See
 * example dm_file for syntax help.
 */
#ifndef DMFILE
#define DMFILE "dm_file"
#endif

/* LOGFILE specifies which file to log to when playing with the
 * -daemon option.
 */
#ifndef LOGFILE
#define LOGFILE "/var/log/deliantra/logfile"
#endif

/*
 * You can restrict playing in certain times by creating a PERMIT_FILE
 * in CONFDIR. See the sample for usage notes.
 */
#define PERM_FILE "forbid"

/*
 * If you want to take the game down while installing new versions, or
 * for other reasons, put a message into the SHUTDOWN_FILE file.
 * Remember to delete it when you open the game again.
 * (It resides in the CONFDIR directory)
 */
#ifndef SHUTDOWN_FILE
#define SHUTDOWN_FILE "shutdown"
#endif

/*
 * SOCKETBUFSIZE is the size of the buffer used internally by the server for
 * storing backlogged messages for the client.  This is not operating system
 * buffers or the like.  This amount is used per connection (client).
 * This buffer is in addition to OS buffers, so it may not need to be very
 * large.  When the OS buffer and this buffer is exhausted, the server
 * will drop the client connection for falling too far behind.  So if
 * you have very slow client connections, a larger value may be
 * warranted.
 */
#define SOCKETBUFSIZE 256*1024

/*
 * Your tmp-directory should be large enough to hold the uncompressed
 * map-files for all who are playing. Local to 'lib' directory.
 */
#define TMPDIR "tmp"

/* Directory to use for unique items. This is placed into the 'lib'
 * directory.  Changing this will cause any old unique items file
 * not to be used.
 */
#define UNIQUE_DIR "unique-items"

/*
 * These defines tells where, relative to LIBDIR, the maps, the map-index,
 * archetypes highscore and treaures files and directories can be found.
 */
#define MAPDIR		"maps"

#define MAXSTRING 20

#define COMMAND_HASH_SIZE 107	/* If you change this, delete all characters :) */



/***********************************************************************
 * Section 4 - save player options.
 *
 * There are a lot of things that deal with the save files, and what
 * gets saved with them, so I put them in there own section.
 *
 ***********************************************************************/

/*
 * If you want the players to be able to save their characters between
 * games, define SAVE_PLAYER and set PLAYERDIR to the directories
 * where the player-files will be put.
 * Remember to create the directory (make install will do that though).
 *
 * If you intend to run a central server, and not allow the players to
 * start their own crossfire, you won't need to define this.
 *
 */

#ifndef PLAYERDIR
# define PLAYERDIR "players"
#endif

/*
 * If you have defined SAVE_PLAYER, you might want to change this, too.
 * This is the access rights for the players savefiles.
 * Given that crossfire runs in a client/server model, there should
 * be no issue setting these to be quite restrictive (600 and 700).
 * Before client/server, multiple people might run the executable,
 * thus requiring that the server be setuid/setgid, and more generous
 * permisisons needed.
 * SAVE_MODE is permissions for the files, SAVE_DIR_MODE is permission
 * for nay directories created.
 */
/* IMPORTANT: there is a race during saving, where the umask is being applied */
#define	SAVE_MODE	0660
#define	SAVE_DIR_MODE	0770

/*
 * The message to send to clients when the server calls cleanup (on crash, shutdown, restart and so on).
 */
#define CLEANUP_MESSAGE "The server will likely restart within the minute. Our apologies."

