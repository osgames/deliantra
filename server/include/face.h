/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#ifndef FACE_H__
#define FACE_H__

#include <util.h>
#include <unordered_map>

#define CHKSUM_MAXLEN 15
#define MAX_FACES 65535 // we reserve face #65535

extern faceidx blank_face, empty_face, magicmouth_face;

struct facedata
{
  uint8 chksum[CHKSUM_MAXLEN];
  uint8 chksum_len;

  facedata ()
  : chksum_len (0)
  { }
};

/* New face structure - this enforces the notion that data is face by
 * face only - you can not change the color of an item - you need to instead
 * create a new face with that color.
 */
struct faceinfo
{
  faceinfo ()
  : number (0), smooth (0), type (0), smoothlevel (0), visibility (0), magicmap (0), refcnt (1)
  {
  }

  shstr name;
  facedata face[3];             // indexed by faceset, 0 == 32 bit or generic, 1 == 64, 2 == text
  faceidx number;		/* This is the image id.  It should be the */
                                /* same value as its position in the array */
  faceidx smooth;               /* the smooth face for this face, or 0 */
  uint8 type;                   // 0 normal face, otherwise other resource
  uint8 smoothlevel;            // smoothlevel is per-face in 2.x servers
  uint8 visibility;
  uint8 magicmap;		/* Color to show this in magic map */

  facedata *data (int faceset) const;

  int refcnt; // reference count - 1
  void ref () { ++refcnt; }
  void unref ();
};

inline void
object_freezer::put (const keyword_string k, faceinfo *v)
{
  if (expect_true (v))
    put (k, v->name);
  else
    put (k);
}

typedef std::unordered_map<const char *, int, str_hash, str_equal, slice_allocator< std::pair<const char *const, int> > > facehash_t;

extern facehash_t facehash;
extern std::vector<faceinfo> faces;

/* This returns an the face number of face 'name'.  Number is constant
 * during an invocation, but not necessarily between versions (this
 * is because the faces are arranged in alphabetical order, so
 * if a face is removed or added, all faces after that will now
 * have a different number.
 *
 * If a face is not found, then defidx is returned.  This can be useful
 * if you want some default face used, or can be set to negative so that
 * it will be known that the face could not be found.
 */
faceidx face_find (const char *name, faceidx defidx = 0);
faceidx face_alloc ();
faceinfo *face_info (faceidx idx);
facedata *face_data (faceidx idx, int faceset);

struct MapLook
{
  faceinfo *face;
  uint8 flags;
};

typedef uint16 animidx;

struct animation
{
  faceidx *faces;		/* The different animations */
  int num_animations;		/* How many different faces to animate */
  sint8 facings;		/* How many facings (1,2,4,8) */
  animidx number;
  shstr name;			/* Name of the animation sequence */

  static animation &create (const char *name, uint8 frames, uint8 facings = 1);
  static animation &find (const char *name);

  void resize (int new_size);
};

typedef std::unordered_map<const char *, int, str_hash, str_equal, slice_allocator< std::pair<const char *const, int> > > animhash_t;

extern std::vector<animation> animations;

#endif

