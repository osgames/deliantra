/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#ifndef CLIENT_H
#define CLIENT_H

#if HAVE_TCP_INFO
# include <netinet/tcp.h>
#else
  struct tcp_info {
    // dummy
    int tcpi_state;
    int tcpi_ca_state;
    int tcpi_retransmits;
    int tcpi_probes;
    int tcpi_backoff;
    int tcpi_options;
    int tcpi_snd_wscale;
    int tcpi_rcv_wscale;
    int tcpi_rto;
    int tcpi_ato;
    int tcpi_snd_mss;
    int tcpi_rcv_mss;
    int tcpi_unacked;
    int tcpi_sacked;
    int tcpi_lost;
    int tcpi_retrans;
    int tcpi_fackets;
    int tcpi_last_data_sent;
    int tcpi_last_ack_sent;
    int tcpi_last_data_recv;
    int tcpi_last_ack_recv;
    int tcpi_pmtu;
    int tcpi_rcv_ssthresh;
    int tcpi_rtt;
    int tcpi_rttvar;
    int tcpi_snd_ssthresh;
    int tcpi_snd_cwnd;
    int tcpi_advmss;
    int tcpi_reordering;
  };
#endif

//+GPL

// (possibly) max. number of objects "per page" in the ground container
#define FLOORBOX_PAGESIZE 50

struct MapCell
{
  tag_t player;                // this is, unfortunately, very wasteful of memory space, but pretty bandwidth-efficient
  int count;                   /* This is really darkness in the map1 command */
  faceidx faces[MAP_LAYERS];
  unsigned char stat_hp;       // health of something in this space, or 0
  unsigned char flags;
  uint8_t smooth[MAP_LAYERS];
};

#define MAX_CLIENT_X MAP_CLIENT_X
#define MAX_CLIENT_Y MAP_CLIENT_Y

struct Map
{
  struct MapCell cells[MAX_CLIENT_X][MAX_CLIENT_Y];
};

/* True max is 16383 given current map compaction method */
#define MAXANIMNUM  10000

struct statsinfo
{
  char *range, *title;
};

// states the socket can be in
enum {
 ST_DEAD,     // socket is dead
 ST_SETUP,    // initial handshake / setup / login
 ST_PLAYING,  // logged in an playing
 ST_CUSTOM,   // waiting for custom reply

 ST_GET_PARTY_PASSWORD,
};

// a handler for a specific type of packet
enum {
  PF_PLAYER  = 0x01, // must have valid player / will by synchronised
  PF_PLAYING = 0x02, // must be in playing state
  PF_COMMAND = 0x04, // is a user command
};

// face types. bit 0 means "has meta info prepended"
enum {
  FT_IMAGE = 0 * 2 + 0, // images
  FT_MUSIC = 1 * 2 + 1, // background music
  FT_SOUND = 2 * 2 + 1, // effects
  FT_RSRC  = 3 * 2 + 0, // generic data files
  FT_NUM,
};

struct packet_type
{
  const char *name;
  void *cb;
  int flags;
};

struct command
{
  tstamp stamp;
  const packet_type *handler;
  char *data;
  int datalen;
};

//-GPL

/* how many times we are allowed to give the wrong password before being kicked. */
#define MAX_PASSWORD_FAILURES 5

/* This contains basic information on the socket connection.  status is its
 * current state.  we set up our on buffers for sending/receiving, so we can
 * handle some higher level functions.  fd is the actual file descriptor we
 * are using.
 */
INTERFACE_CLASS (client) // should become player when newsocket is a baseclass of player
struct client : zero_initialised, attachable
{
  int ACC (RW, fd);
  unsigned int inbuf_len;                 // number of bytes valid in inbuf
  statsinfo stats;
  object_vector_index ACC (RO, active);
  player_ptr ACC (RO, pl);

  char *ACC (RW, host);                   /* Which host it is connected from (ip:port) */
  uint8 ACC (RW, state);                  /* Input state of the player (name, password, etc */

  sint8 ACC (RW, last_level);             /* Last level we sent to client */
  uint16 ACC (RW, last_flags);            /* fire/run on flags for last tick */
  float ACC (RW, last_weapon_sp);         /* if diff than weapon_sp, update client */
  sint32 ACC (RW, last_weight);           /* Last weight as sent to client; -1 means do not send weight */
  sint32 ACC (RW, last_weight_limit);     /* Last weight limit transmitted to client */
  uint32 ACC (RW, last_path_attuned);     /* Last spell attunment sent to client */
  uint32 ACC (RW, last_path_repelled);    /* Last spell repelled sent to client */
  uint32 ACC (RW, last_path_denied);      /* Last spell denied sent to client */
  living ACC (RO, last_stats);            /* Last stats as sent to client */
  float ACC (RW, last_speed);             /* Last speed as sent to client */
  sint16 ACC (RW, last_resist[NROFATTACKS]);/* last resist values sent to client */
  sint64 ACC (RW, last_skill_exp[CS_NUM_SKILLS]);/* shadow register. if != exp. obj update client */

  bool ACC (RW, afk);                     /* player is afk */
  bool ACC (RW, sent_scroll);
  bool ACC (RW, sound);                   /* does the client want sound */
  bool ACC (RW, bumpmsg);                 /* give message when bumping into walls */

  bool ACC (RW, plugincmd);               // extend the protocol through a plug-in */
  bool ACC (RW, mapinfocmd);              // return map info and send map change info
  uint8 ACC (RW, extcmd);                 // call into extensions/plugins
  bool ACC (RW, need_delinv0);            /* If true, we need to delinv 0 before sending new floorbox */

  bool ACC (RW, update_look);             /* If true, we need to send the look window */
  bool ACC (RW, update_spells);           // If true, we need to esrv_update_spells
  bool ACC (RW, has_readable_type);       /* If true client accept additional text information */
  uint8 ACC (RW, monitor_spells);         /* Client wishes to be informed when their spell list changes */

  bool ACC (RW, smoothing);               // deliantra-style smoothing
  bool ACC (RW, force_newmap);            // force a newmap before next map update
  uint16 ACC (RW, look_position);         /* start of drawing of look window */
  uint16 ACC (RW, mss);                   // likely tcp maximum segment size
  uint8 ACC (RW, mapx), ACC (RW, mapy);   /* How large a map the client wants */
  uint8 ACC (RW, itemcmd);                /* What version of the 'item' protocol command to use */
  uint8 ACC (RW, tileset);                // selected tileset
  uint8 ACC (RW, ws_version);             // websocket protocol versio for framing

  maptile_ptr ACC (RW, current_map);      // last/current player map
  region_ptr ACC (RW, current_region);    // last/current player region
  int ACC (RW, current_x), ACC (RW, current_y);     // CF+ last/current map position

  tstamp ACC (RW, last_send);             // last data send on socket.

  float ACC (RW, socket_timeout);         /* after how many seconds of no ack do we declare dead */
  int ACC (RW, rate_avail);               // current rate balance
  int ACC (RW, max_rate);                 // max. # of bytes to send per tick
  faceidx ACC (RW, scrub_idx);            // which face to send next
  int ACC (RW, bg_scrub);                 // how many ticks till the next background face send

  struct tcp_info tcpi;
  tstamp next_rate_adjust;

  // websocket frame buffer
  uint8 ws_inbuf_type; // current frame type
  uint8 *ws_inbuf;
  uint32 ws_inbuf_len;
  uint32 ws_inbuf_alloc;

  unordered_vector<char *> mapinfo_queue;
  void mapinfo_queue_clear ();
  void mapinfo_queue_run ();
  bool mapinfo_try (char *buf);

  struct ixsend {
    sint16 pri; // higher means faces are sent earlier, default 0
    faceidx idx;
    uint32 ofs;  // if != 0, need to send remaining bytes of partial_face
    uint8 *data;
    SV *data_sv;
  };
  std::vector<ixsend, slice_allocator<ixsend> > ixface; // which faces to send to the client using ix
  MTH void ix_send (faceidx idx, sint16 pri, SV *data_sv); // keeps a copy of data_sv
  void ix_pop (); // pops topmost ixsend

  std::vector<faceidx, slice_allocator<faceidx> > fxface; // which faces to send using fx
  MTH void flush_fx (); // send fx if required

  MTH void invalidate_face (faceidx idx);
  MTH void invalidate_all_faces ();

  void do_destroy ();
  void gather_callbacks (AV *&callbacks, event_type event) const;

  iow socket_ev; void socket_cb (iow &w, int revents);

  std::deque< command, slice_allocator<command> > cmd_queue;

  // large structures at the end please
  struct Map lastmap;
  std::bitset<MAXANIMNUM> anims_sent;
  std::bitset<MAX_FACES> faces_sent;
  std::bitset<FT_NUM> fx_want;

  // if we get an incomplete packet, this is used to hold the data.
  // we add 2 byte for the header, one for the trailing 0 byte
  uint8 inbuf[MAXSOCKBUF + 2 + 1];
  void inbuf_handle ();

  enum { MSG_BUF_SIZE = 80, MSG_BUF_COUNT = 10 };
  struct msg_buf
  {
    tick_t expire;
    int len;
    int count;
    char msg[MSG_BUF_SIZE];
  } msgbuf[MSG_BUF_COUNT];

  MTH bool msg_suppressed (const_utf8_string msg);

  /* The following is the setup for a ring buffer for storing output
   * data that the OS can't handle right away.
   * TODO: this member is enourmously large - optimise?
   */
  struct
  {
    char data[SOCKETBUFSIZE];
    int start;
    int len;
  } outputbuffer;

  bool may_execute (const packet_type *pkt) const;
  void execute (const packet_type *pkt, char *data, int datalen);

  void queue_command (packet_type *handler, char *data, int datalen);
  MTH bool handle_command ();
  // resets movement state
  MTH void reset_state ();
  // resets variable data used to send stat diffs
  MTH void reset_stats ();

  MTH bool handle_packet ();
  int next_packet ();                     // returns length of packet or 0
  void skip_packet (int len);             // we have processed the packet, skip it

  MTH void flush ();
  MTH void write_outputbuffer ();
  MTH int outputbuffer_len () const { return outputbuffer.len; }
  void send_raw (void *buf_, int len);

  void send (void *buf_, int len);
  void send_packet (const_octet_string buf);
  void send_packet (const_octet_string buf, int len);
  void send_packet_printf (const_utf8_string format, ...) attribute ((format (printf, 2, 3)));
  void send_packet (packet &sl);

  MTH void send_face (faceidx facenum, int pri = 0);
  MTH void send_faces (object *ob);
  MTH void send_animation (short anim_num);
  void send_msg (int color, const_utf8_string type, const_utf8_string msg);

  MTH void play_sound (faceidx sound, int dx = 0, int dy = 0);
  // called when something under the player changes
  MTH void floorbox_update () { update_look = 1; }
  // called when the player has been moved
  MTH void floorbox_reset  () { look_position = 0; floorbox_update (); }

  MTH void tick (); // called every server tick to do housekeeping etc.

  MTH static client *create (int fd, const char *peername);
  MTH static void clock ();
  MTH static void flush_sockets ();

  MTH void run (); // start handshake after init

protected:
  client (int fd, const char *from_ip);
  ~client ();
};

#if FOR_PERL
  ACC (RW, tcpi.tcpi_state);
  ACC (RW, tcpi.tcpi_ca_state);
  ACC (RW, tcpi.tcpi_retransmits);
  ACC (RW, tcpi.tcpi_probes);
  ACC (RW, tcpi.tcpi_backoff);
  ACC (RW, tcpi.tcpi_options);
  ACC (RO, tcpi.tcpi_snd_wscale);
  ACC (RO, tcpi.tcpi_rcv_wscale);
  ACC (RW, tcpi.tcpi_rto);
  ACC (RW, tcpi.tcpi_ato);
  ACC (RW, tcpi.tcpi_snd_mss);
  ACC (RW, tcpi.tcpi_rcv_mss);
  ACC (RW, tcpi.tcpi_unacked);
  ACC (RW, tcpi.tcpi_sacked);
  ACC (RW, tcpi.tcpi_lost);
  ACC (RW, tcpi.tcpi_retrans);
  ACC (RW, tcpi.tcpi_fackets);
  ACC (RW, tcpi.tcpi_last_data_sent);
  ACC (RW, tcpi.tcpi_last_ack_sent);
  ACC (RW, tcpi.tcpi_last_data_recv);
  ACC (RW, tcpi.tcpi_last_ack_recv);
  ACC (RW, tcpi.tcpi_pmtu);
  ACC (RW, tcpi.tcpi_rcv_ssthresh);
  ACC (RW, tcpi.tcpi_rtt);
  ACC (RW, tcpi.tcpi_rttvar);
  ACC (RW, tcpi.tcpi_snd_ssthresh);
  ACC (RW, tcpi.tcpi_snd_cwnd);
  ACC (RW, tcpi.tcpi_advmss);
  ACC (RW, tcpi.tcpi_reordering);
#endif

typedef object_vector<client, &client::active> sockvec;

extern sockvec clients;

#define for_all_clients(var)                    \
  for (int _i = 0; _i < clients.size (); ++_i)  \
    statementvar (client *, var, clients [_i])

// returns true when the message needs special (read: perl) treatment
static inline bool
msg_is_special (const_any_string msg, bool nl_special = true)
{
  return msg [strcspn (msg, nl_special ? "<&\n" : "<&")];
}

#endif

