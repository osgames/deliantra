/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#ifndef COMPILER_H
#define COMPILER_H

#define GCC_VERSION(major,minor) (__GNUC__ > (major) || (__GNUC__ == (major) && __GNUC_MINOR__ >= (minor)))

#if GCC_VERSION(3,0)
# define attribute(attrlist) __attribute__(attrlist)
#else
# define attribute(attrlist)
#endif

#if GCC_VERSION(3,1)
# define is_constant(c)             __builtin_constant_p (c)
# define expect(expr,value)         __builtin_expect ((expr),(value))
# define prefetch(addr,rw,locality) __builtin_prefetch (addr, rw, locality)
# define noinline                   __attribute__((__noinline__))
#else
# define is_constant(c)             0
# define expect(expr,value)         (expr)
# define prefetch(addr,rw,locality)
# define noinline
#endif

#define func_const attribute ((const))
#define func_pure  attribute ((pure))
#define func_hot   attribute ((hot))	// 4.x
#define func_cold  attribute ((cold))	// 4.x

#if GCC_VERSION(4,5)
# define unreachable() __builtin_unreachable ()
#else
 // this seems to work fine, but gcc always emits a warning for it :/
 static inline void unreachable () attribute ((noreturn));
 static inline void unreachable () { }
#endif

// put into ifs if you are very sure that the expression
// is mostly true or mosty false. note that these return
// booleans, not the expression.
#define expect_false(expr) expect ((expr) ? 1 : 0, 0)
#define expect_true(expr)  expect ((expr) ? 1 : 0, 1)

// try to tell the compiler that some condition is definitely true
#define assume(cond) do { if (!(cond)) unreachable (); } while (0)

#endif

