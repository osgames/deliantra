/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

// the order of skills MUST stay the same, with no holes, to match
// the archetypes. Do not remove or add skills in between.

//   symbol               flags
// 01
def (LOCKPICKING	, SF_RANGED)
def (HIDING		, SF_USE)
def (SMITHERY		, SF_USE)
def (BOWYER		, SF_USE)
def (JEWELER		, SF_USE)
def (ALCHEMY		, SF_USE)
def (STEALING		, SF_RANGED)
def (LITERACY		, SF_USE)
def (BARGAINING		, SF_USE)
// 10
def (JUMPING		, SF_RANGED)
def (DET_MAGIC		, SF_USE)
def (ORATORY		, SF_RANGED)
def (SINGING		, SF_RANGED)
def (DET_CURSE		, SF_USE)
def (FIND_TRAPS		, SF_USE | SF_RANGED)
def (MEDITATION		, SF_USE)
def (PUNCHING		, SF_COMBAT)
def (FLAME_TOUCH	, SF_COMBAT)
def (KARATE		, SF_COMBAT)
// 20
def (CLIMBING		, 0)
def (WOODSMAN		, SF_USE)
def (INSCRIPTION	, SF_USE)
def (ONE_HANDED_WEAPON	, SF_COMBAT | SF_NEED_ITEM)
def (MISSILE_WEAPON	, SF_RANGED | SF_NEED_ITEM)
def (THROWING		, SF_RANGED)
def (USE_MAGIC_ITEM	, SF_RANGED | SF_NEED_ITEM)
def (DISARM_TRAPS	, SF_USE | SF_RANGED)
def (SET_TRAP		, 0)
def (THAUMATURGY	, SF_USE)
// 30
def (PRAYING		, SF_RANGED | SF_GRACE)
def (CLAWING		, SF_COMBAT)
def (LEVITATION		, SF_AUTARK | SF_USE)
def (SUMMONING		, SF_RANGED | SF_MANA)
def (PYROMANCY		, SF_RANGED | SF_MANA)
def (EVOCATION		, SF_RANGED | SF_MANA)
def (SORCERY		, SF_RANGED | SF_MANA)
def (TWO_HANDED_WEAPON	, SF_COMBAT | SF_NEED_ITEM)
def (SPARK_TOUCH	, SF_COMBAT)
def (SHIVER		, SF_COMBAT)
// 40
def (ACID_SPLASH	, SF_COMBAT)
def (POISON_NAIL	, SF_COMBAT)
def (MINING		, SF_RANGED | SF_NEED_ITEM)

