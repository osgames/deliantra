/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

//   identifier,	lowercase,	description	for (xxx+5),		"change"?
def (PHYSICAL,		physical,	physical,	armour,			physical)
def (MAGIC,		magic,		magic,		resist magic,		magic)
def (FIRE,		fire,		fire,		resist fire,		fire)
def (ELECTRICITY,	electricity,	electricity,	resist electricity,	electricity)
def (COLD,		cold,		cold,		resist cold,		cold)
def (CONFUSION,		confusion,	confusion,	resist confusion,	confusion)
def (ACID,		acid,		acid,		resist acid,		acid)
def (DRAIN,		drain,		drain,		resist drain,		draining)
def (WEAPONMAGIC,	weaponmagic,	weapon magic,	resist weaponmagic,	weapon magic)
def (GHOSTHIT,		ghosthit,	ghost hit,	resist ghosthit,	ghosts)
def (POISON,		poison,		poison,		resist poison,		poison)
def (SLOW,		slow,		slow,		resist slow,		slow)
def (PARALYZE,		paralyze,	paralyze,	resist paralyzation,	paralyze)
def (TURN_UNDEAD,	turn_undead,	turn undead,	resist turn undead,	turn undead)
def (FEAR,		fear,		fear,		resist fear,		fear)
def (CANCELLATION,	cancellation,	cancellation,	resist cancellation,	cancellation)
def (DEPLETE,		deplete,	deplete,	resist depletion,	depletion)
def (DEATH,		death,		death,		resist death,		death attacks)
def (CHAOS,		chaos,		chaos,		resist chaos,		chaos)
def (COUNTERSPELL,	counterspell,	counterspell,	resist counterspell,	counterspell)
def (GODPOWER,		godpower,	god power,	resist god power,	god power)
def (HOLYWORD,		holyword,	holy word,	resist holy word,	holy word)
def (BLIND,		blind,		blind,		resist blindness,	blinding attacks)
def (INTERNAL,		internal,	internal,	resist internal,	internal)
def (LIFE_STEALING,	life_stealing,	life stealing,	resist life stealing,	life stealing)
def (DISEASE,		disease,	disease,	resist diseases,	disease)
