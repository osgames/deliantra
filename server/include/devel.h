/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

// This file is only included with -DDEVEL, and is only part
// of the distributon for completeness, it is not supposed to
// be useful outside the developers machines.

#ifndef DEVEL_H
#define DEVEL_H

// used for benchmarking (x86/amd64-specific)
#ifdef DEVEL64
typedef uint64_t cstamp;
#else
typedef uint32_t cstamp;
#endif

#if defined(__i386__) || defined(__x86_64__)

static inline cstamp
stamp ()
{
  uint32_t l, h;

  asm volatile ("rdtsc" : "=a" (l), "=d" (h));

#ifdef DEVEL64
  return ((cstamp)h << 32) | l;
#else
  return l;
#endif
}

#elif defined(__powerpc__)
static inline cstamp
stamp ()
{
  unsigned uint32_t u, l, tmp;

  asm volatile (
     "0:             \n"
     "\tmftbu   %0   \n"
     "\tmftb    %1   \n"
     "\tmftbu   %2   \n"
     "\tcmpw    %2,%0\n"
     "\tbne     0b   \n"
     : "=r" (h), "=r" (l), "=r" (tmp)
  );

#ifdef DEVEL64
  return ((cstamp)h << 32) | l;
#else
  return l;
#endif
}

#endif

static inline cstamp
measure (cstamp t)
{
  return stamp () - t;
}

// cstamp s = stamp ();
// s = measure (s);

#endif

