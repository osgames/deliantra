/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#ifndef LIBPROTO_H_
#define LIBPROTO_H_

/* anim.c */
void free_all_anim ();
void init_anim ();
void animate_object (object *op, int dir);
/* arch.c */
archetype *find_archetype_by_object_name (const char *name);
archetype *find_archetype_by_object_type_name (int type, const char *name);
object *get_archetype_by_skill_name (const char *skill, int type);
archetype *get_archetype_by_type_subtype (int type, int subtype);
object *get_archetype_by_object_name (const char *name);
object *find_best_weapon_used_match (object *pl, const char *params);
int item_matched_string (object *pl, object *op, const char *name);
void arch_info (object *op);
void clear_archetable ();
void dump_arch (archetype *at);
void dump_all_archetypes ();
void free_all_archs ();
void check_generators ();
bool load_archetype_file (const char *filename);
unsigned long hasharch (const char *str, int tablesize);
object *clone_arch (int type);
object *object_create_arch (archetype *at);
/* button.c */
void push_button (object *op, object *originator);
void update_button (object *op, object *originator);
void use_trigger (object *op, object *originator);
void animate_turning (object *op);
int check_altar_sacrifice (object *altar, object *sacrifice, object *originator = 0);
int operate_altar (object *altar, object **sacrifice, object *originator = 0);
int check_trigger (object *op, object *cause, object *originator = 0);
int get_button_value (const object *button);
void do_mood_floor (object *op, object *source = 0);
object *check_inv_recursive (object *op, const object *trig);
void check_inv (object *op, object *trig);
void verify_button_links (const maptile *map);
/* exp.c */
int exp_to_level (sint64 exp);
sint64 level_to_min_exp (int level);
int new_exp (const object *ob);
void _reload_exp_table ();
/* friend.c */
void add_friendly_object (object *op);
void remove_friendly_object (object *op);
void dump_friendly_objects ();
void clean_friendly_list ();
/* holy.c */
void init_gods ();
godlink *get_rand_god ();
object *pntr_to_god_obj (godlink *godlnk);
void dump_gods ();
/* info.c */
void dump_abilities ();
void print_monsters ();
void bitstostring (long bits, int num, char *str);
/* image.c */
void ReadBmapNames ();
int FindFace (const char *name, int error);
int ReadSmooth ();
int FindSmooth (uint16 face, uint16 *smoothed);
void free_all_images ();
/* init.c */
void init_library ();
void init_environ ();
void init_globals ();
void init_objects ();
void init_defaults ();
void init_dynamic ();
void init_clocks ();
void init_attackmess ();
/* item.c */
int get_power_from_ench (int ench);
int calc_item_power (const object *op, int flag);
const typedata *get_typedata (int itemtype);
const typedata *get_typedata_by_name (const char *name);
const char *describe_resistance (const object *op, int newline);
const char *ordinal (int i);
const char *describe_item (const object *op, object *owner);
int is_magical (const object *op);
void identify (object *op);
void examine (object *op, object *tmp);
/* links.c */
objectlink *get_objectlink ();
oblinkpt *get_objectlinkpt ();
void free_objectlink (objectlink *ol);
void free_objectlinkpt (oblinkpt *obp);
/* living.c */
void change_attr_value (living *stats, int attr, sint8 value);
void check_stat_bounds (living *stats);
int change_abil (object *op, object *tmp);
void set_dragon_name (object *pl, const object *abil, const object *skin);
object *give_skill_by_name (object *op, const char *skill_name);
void player_lvl_adj (object *who, object *op);
sint64 level_exp (int level, double expmul);
void calc_perm_exp (object *op);
sint64 check_exp_adjust (const object *op, sint64 exp);
void change_exp (object *op, sint64 exp, shstr_tmp skill_name, int flag);
void apply_death_exp_penalty (object *op);
int did_make_save (const object *op, int level, int bonus);
/* los.c */
void init_block ();
void update_all_map_los (maptile *map);
void update_all_los (const maptile *map, int x, int y);
void make_sure_seen (const object *op);
void make_sure_not_seen (const object *op);
/* map.c */
const char *create_pathname (const char *name);
int blocked_link (object *ob, maptile *m, int sx, int sy);
/* object.c */
char *dump_object (object *op);
void dump_me (object *op, char *outstr);
object *find_object (tag_t i);
object *find_object_uuid (UUID i);
object *find_object_name (const char *str);
void free_all_object_data ();
void update_turn_face (object *op);
void update_object (object *op, int action);
object *merge_ob (object *op, object *top);
object *insert_ob_in_map_at (object *op, maptile *m, object *originator, int flag, int x, int y);
object *insert_ob_in_map (object *op, maptile *m, object *originator, int flag);
void replace_insert_ob_in_map (shstr_tmp archname, object *op);
object *insert_ob_in_ob (object *op, object *where);
int check_move_on (object *op, object *originator, int flags = 0);
object *present_arch (const archetype *at, maptile *m, int x, int y);
object *present (unsigned char type, maptile *m, int x, int y);
object *present_in_ob (unsigned char type, const object *op);
object *present_in_ob_by_name (int type, const char *str, const object *op);
object *present_arch_in_ob (const archetype *at, const object *op);
void flag_inv (object *op, int flag);
void unflag_inv (object *op, int flag);
int find_free_spot (const object *ob, maptile *m, int x, int y, int start, int stop);
int find_first_free_spot (const object *ob, maptile *m, int x, int y);
void get_search_arr (int *search_arr);
int find_dir (maptile *m, int x, int y, object *exclude);
int distance (const object *ob1, const object *ob2);
int find_dir_2 (int x, int y);
int dirdiff (int dir1, int dir2);
int can_see_monsterP (maptile *m, int x, int y, int dir);
int can_pick (const object *who, const object *item);
object *find_obj_by_type_subtype (const object *who, int type, int subtype);
/* path.c */
char *path_combine (const char *src, const char *dst);
char *path_combine_and_normalize (const char *src, const char *dst);
/* player.c */
void free_player (player *pl);
int atnr_is_dragon_enabled (int attacknr);
/* quest.c */
int quest_is_quest_marker (const object *marker, int task);
int quest_is_in_progress (const object *marker, int task);
int quest_is_completed (const object *marker, int task);
const char *quest_get_name (const object *marker);
object *quest_get_player_quest (const object *pl, const char *name, const char *name_pl);
object *quest_get_override (const object *ob, const object *pl);
const char *quest_get_override_slaying (const object *ob, const object *pl);
const char *quest_get_override_msg (const object *ob, const object *pl);
void quest_apply_items (object *wrapper, player *pl);
int quest_on_activate (object *ob, player *pl);
int quest_is_override_compatible (const object *marker, const object *pl);
/* readable.c */
void init_readable ();
object *get_random_mon (int level);
void tailor_readable_ob (object *book, int msg_type);
void write_book_archive ();
readable_message_type *get_readable_message_type (object *readable);
/* recipe.c */
recipelist *get_formulalist (int i);
void init_formulae ();
void dump_alchemy ();
void dump_alchemy_costs ();
int strtoint (const char *buf);
artifact *locate_recipe_artifact (const recipe *rp, size_t idx);
recipe *get_random_recipe (recipelist *rpl);
/* region.c */
object *get_jail_exit (object *op);
/* shstr.c */
int buf_overflow (const char *buf1, const char *buf2, int bufsize);
/* time.c */
void reset_sleep ();
void log_time (long process_utime);
int enough_elapsed_time ();
void sleep_delta ();
void get_tod (timeofday_t *tod);
void print_tod (object *op);
void time_info (object *op);
/* treasure.c */
bool load_treasure_file (const char *filename);
object *generate_treasure (treasurelist *t, int difficulty);
void set_abs_magic (object *op, int magic);
void fix_generated_item (object *op, object *creator, int difficulty, int max_magic, int flags);
artifactlist *find_artifactlist (int type);
void dump_artifacts ();
void dump_monster_treasure_rec (const char *name, treasure *t, int depth);
void dump_monster_treasure (const char *name);
void init_artifacts ();
void add_abilities (object *op, object *change);
void give_artifact_abilities (object *op, object *artifct);
void generate_artifact (object *op, int difficulty);
void fix_flesh_item (object *item, object *donor);
/* utils.c */
int random_roll (int min, int max, const object *op, bool prefer_high);
sint64 random_roll64 (sint64 min, sint64 max, const object *op, bool prefer_high);
int die_roll (int num, int size, const object *op, bool prefer_high);
void transmute_materialname (object *op, const object *change);
void strip_endline (char *buf);
void replace (const char *src, const char *key, const char *replacement, char *result, size_t resultsize);
/* loader.c */
int set_variable (object *op, char *buf);
void init_vars ();
char *get_ob_diff (object *op, object *op2);
bool load_resource_file_ (const char *filename);
/* map.c */
maptile *find_style (const char *dirname, const char *stylename, int difficulty, bool recurse = false);
object *pick_random_object (maptile *style);

/* former funcpoint.h */
/*
 * These function-pointers are defined in common/glue.c
 * The functions used to set and initialise them are also there.
 *
 * Massive change. Those functions are just defined, no callback, and they should be implemented.
 * This means glue.c code & such can go away almost entirely.
 * Ryo 2005-07-15
 */

void	move_apply (object *, object *, object *);
void	draw_info (int, int, object *, const char *);
void	clean_tmp_files ();
void	fix_auto_apply (maptile *);
void	init_blocksview_players ();
void	monster_check_apply (object *, object *);
void	process_active_maps ();
void	remove_friendly_object (object *);
void	update_buttons (maptile *);
void	move_teleporter (object *);
void	move_firewall (object *);
void	move_creator (object *);
void move_marker (object *);
void	move_duplicator (object *);
void trap_adjust (object *, int);
void	esrv_send_item (object *, object *);
void	esrv_del_item (player *, int);
void	esrv_update_item (int, object *, object *);
void	esrv_update_spells (player *);
void dragon_ability_gain (object *, int, int);
object *	find_skill_by_number (object *, int);

#endif

