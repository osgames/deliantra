/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 1994 Mark Wedel
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#include "spells.h"

const char *const spellpathnames[NRSPELLPATHS] =
{
  "Protection",
  "Fire",
  "Frost",
  "Electricity",
  "Missiles",
  "Self",
  "Summoning",
  "Abjuration",
  "Restoration",
  "Detonation",
  "Mind",
  "Creation",
  "Teleportation",
  "Information",
  "Transmutation",
  "Transferrence",
  "Turning",
  "Wounding",
  "Death",
  "Light",
};

