/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2003-2006 Mark Wedel & Crossfire Development Team
 * Copyright (©) 2000 Tim Rightnour
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#ifndef TOD_H_
#define TOD_H_

#define TICK                    (MAX_TIME * 1e-6)

#define TICK2TIME(tick)         (tstamp (tick) * TICK)
#define TIME2TICK(time)         tstamp ((time) / TICK)

#define RUNTIME_PER_MINUTE	3.
#define RUNTIME_PER_HOUR	(RUNTIME_PER_MINUTE * 60.)
#define TICKS_PER_HOUR		int (RUNTIME_PER_HOUR / MAX_TIME * 1e6)

// the first year in the game
#define EPOCH			8437

/* game time */
#define HOURS_PER_DAY		28
#define DAYS_PER_WEEK		 7
#define WEEKS_PER_MONTH		 5
#define MONTHS_PER_YEAR		17

// one game minute is 3s
// one game hour   is 3m
// one game day    is 1h24s
// one game week   is 9h48s
// one game month  is 2d1h
// one game year   is 34d17h

/* convenience */
#define WEEKS_PER_YEAR		(WEEKS_PER_MONTH * MONTHS_PER_YEAR)
#define DAYS_PER_MONTH		(DAYS_PER_WEEK   * WEEKS_PER_MONTH)
#define DAYS_PER_YEAR		(DAYS_PER_MONTH  * MONTHS_PER_YEAR)
#define HOURS_PER_WEEK		(HOURS_PER_DAY   * DAYS_PER_WEEK)
#define HOURS_PER_MONTH		(HOURS_PER_WEEK  * WEEKS_PER_MONTH)
#define HOURS_PER_YEAR		(HOURS_PER_MONTH * MONTHS_PER_YEAR)

#define LUNAR_DAYS		DAYS_PER_MONTH

struct timeofday_t
{
  int year;
  int month;
  int day;
  int dayofweek;
  int hour;
  int minute;
  int weekofmonth;
  int season;
};

/* from common/time.c */
void get_tod (timeofday_t *tod);
char *format_tod (char *buf, int len, timeofday_t *tod);
void print_tod (object *op);

#endif

