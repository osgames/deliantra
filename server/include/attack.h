/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#ifndef ATTACK_H
#define ATTACK_H

/*
 * Attacktypes:
 * ATNR_... is the attack number that is indexed into the
 * the resist array in the object structure.
 */

#define NROFATTACKMESS		20
#define MAXATTACKMESS		20

/* attack message numbers must be less than NROFATTACKMESS */

#define ATM_ARROW	0
#define ATM_DRAIN	1
#define ATM_ELEC	2
#define ATM_COLD	3
#define ATM_FIRE	4
#define ATM_BASIC	5
#define ATM_KARATE	6
#define ATM_CLAW	7
#define ATM_PUNCH	8
#define ATM_SLASH	9
#define ATM_PIERCE	10
#define ATM_CLEAVE	11
#define ATM_SLICE	12
#define ATM_STAB	13
#define ATM_WHIP	14
#define ATM_CRUSH	15
#define ATM_BLUD	16
#define ATM_DOOR	17
#define ATM_SUFFER	18

/* attack messages structure */
typedef struct attackmess {
  int level;
  char *buf1;
  char *buf2;
  char *buf3;
} attackmess_t;

EXTERN attackmess_t attack_mess[NROFATTACKMESS][MAXATTACKMESS];

enum
{
# define def(uc, lc, name, plus, change) ATNR_ ## uc,
#  include "attackinc.h"
# undef def
  NROFATTACKS
};

enum
{
# define def(uc, lc, name, plus, change) AT_ ## uc = 1UL << ATNR_ ## uc,
#  include "attackinc.h"
# undef def
};

/* lowercase attacktype identifiers */
extern const char* const attacktype_name[NROFATTACKS];

/* short description of names of the attacktypes */
extern const char* const attacktype_desc[NROFATTACKS];

/* Note that internal should not ever be referenced in the last two
 * tables.  however, other resisttypes may be added, and if through some
 * bug these do get used somehow, might as well make it more easier to notice
 * and not have mystery values appear.
 */

/* This is the array that is what the player sees. */
extern const char* const resist_plus[NROFATTACKS];

/* These are the descriptions of the resistances displayed when a
 * player puts on/takes off an item. See change_abil() in living.c.
 */
extern const char* const change_resist_msg[NROFATTACKS];

/* If you want to weight things so certain resistances show up more often than
 * others, just add more entries in the table for the protections you want to
 * show up.
 */
extern int resist_table[NROFATTACKS];

#define num_resist_table 19

#endif

