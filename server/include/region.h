/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002-2005 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

/*
 * The maptile is allocated each time a new map is opened.
 * It contains pointers (very indirectly) to all objects on the map.
 */

#ifndef REGION_H
#define REGION_H

//+GPL

/*
 * Each map is in a given region of the game world and links to a region definition, so
 * they have to appear here in the headers, before the mapdef
 */
INTERFACE_CLASS (region)
struct region : zero_initialised, attachable
{
  shstr ACC (RW, name);                   /* Shortend name of the region as maps refer to it */
  region_ptr ACC (RW, parent);        	  /*
                                           * Pointer to the region that is a parent of the current
                                           * region, if a value isn't defined in the current region
                                           * we traverse this series of pointers until it is.
                                           */
  shstr ACC (RW, longname);               /* Official title of the region, this might be defined
                                           * to be the same as name*/
  shstr ACC (RW, msg);                    /* the description of the region */
  shstr ACC (RW, jailmap);                /*where a player that is arrested in this region should be imprisoned. */
  sint16 ACC (RW, jailx), ACC (RW, jaily);/* The coodinates in jailmap to which the player should be sent. */
  shstr ACC (RW, portalmap);
  object_vector_index ACC (RW, index);
  bool ACC (RW, fallback);                /* whether, in the event of a region not existing,
                                           * this should be the one we fall back on as the default */
  float treasure_density;                 // chance of treasure per mapspace
  treasurelist *treasure;                 // treasure to generate (mostly monsters)

//-GPL

  MTH static region *default_region ();
  MTH static region *find (shstr_cmp name);
  static region *read (object_thawer &f);

  void do_destroy ();
};

typedef object_vector<region, &region::index> regionvec;

extern regionvec regions;

#define for_all_regions(var)			\
  for (unsigned _i = 0; _i < regions.size (); ++_i)	\
    statementvar (region *, var, regions [_i])

#endif

