/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#ifndef SOUNDS_H
#define SOUNDS_H

// gcfclient-style, very limited
#define SOUND_NEW_PLAYER	0
#define SOUND_FIRE_ARROW	1
#define SOUND_LEARN_SPELL	2
#define SOUND_FUMBLE_SPELL	3
#define SOUND_WAND_POOF		4
#define SOUND_OPEN_DOOR		5
#define SOUND_PUSH_PLAYER	6
#define SOUND_PLAYER_HITS1	7
#define SOUND_PLAYER_HITS2	8
#define SOUND_PLAYER_HITS3	9
#define SOUND_PLAYER_HITS4	10
#define SOUND_PLAYER_IS_HIT1	11
#define SOUND_PLAYER_IS_HIT2	12
#define SOUND_PLAYER_IS_HIT3	13
#define SOUND_PLAYER_KILLS	14
#define SOUND_PET_IS_KILLED	15
#define SOUND_PLAYER_DIES	16
#define SOUND_OB_EVAPORATE	17
#define SOUND_OB_EXPLODE	18
#define SOUND_CLOCK		19
#define SOUND_TURN_HANDLE	20
#define SOUND_FALL_HOLE		21
#define SOUND_DRINK_POISON     	22
#define SOUND_CAST_SPELL_0	23
/* ... + other sounds for spells : SOUND_CAST_SPELL_0 + spell type */
/* NROF_SOUNDS is defined in "defines.h".  Don't forget to change this number
 * if you add or remove any sound.
 */

// cfplus
faceidx sound_find (const char *str);
void sound_set (const char *str, faceidx face);

#endif

