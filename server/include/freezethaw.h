/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#ifndef FREEZETHAW_H__
#define FREEZETHAW_H__

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// a little dirty hack, maybe unify with something else at a later time
// it is used to communicate as much info about the keyword string
// to the consuemr as possible, for maximum performance.
struct keyword_string
{
  const char *s;
  const int l;

  keyword_string (const char *s, int l)
  : s(s), l(l)
  { }

  keyword_string (keyword kw)
  : s(keyword_str [kw]), l(keyword_len [kw])
  {
  }

  keyword_string (shstr_tmp sh)
  : s(&sh), l(sh.length ())
  {
  }
};

#define CS(keyword) keyword_string (# keyword, sizeof (# keyword) - 1)
#define KW(keyword) CS(keyword)

INTERFACE_CLASS(object_freezer)
struct object_freezer : dynbuf_text
{
  AV *av;

  object_freezer ();
  ~object_freezer ();

  // serialise perl part
  void put_ (attachable *ext);
  void put (attachable *ext)
  {
    if (expect_false (ext->self))
      put_ (ext);
  }

  // null value (== no space after keyword)
  void put (const keyword_string k)
  {
    char *p = force (k.l + 1);
    memcpy (p, k.s, k.l); p += k.l; *p++ = '\n';
    alloc (p);
  }

  void put (const keyword_string k, const keyword_string v)
  {
    char *p = force (k.l + 1 + v.l + 1);
    memcpy (p, k.s, k.l); p += k.l; *p++ = ' ';
    memcpy (p, v.s, v.l); p += v.l; *p++ = '\n';
    alloc (p);
  }

  void put (const keyword_string k, const_utf8_string v)
  {
    if (expect_true (v))
      put (k, keyword_string (v, strlen (v)));
    else
      put (k);
  }

  void put (const keyword_string k, shstr_tmp v)
  {
    put (k, keyword_string (v));
  }

  void put (const keyword_string k, double v)
  {
    char *p = force (MAX_KEYWORD_LEN + 2 + 32);
    memcpy (p, k.s, k.l); p += k.l; *p++ = ' ';
    p += sprintf (p, "%.7g", v); *p++ = '\n';
    alloc (p);
  }

  void put_(const keyword_string k, sint64 v)
  {
    force (MAX_KEYWORD_LEN + 2 + sint64_digits);
    fadd (k.s, k.l);
    fadd (' ');
    add (v);
    fadd ('\n');
  }

  void put_(const keyword_string k, sint32 v)
  {
    force (MAX_KEYWORD_LEN + 2 + sint32_digits);
    fadd (k.s, k.l);
    fadd (' ');
    add (v);
    fadd ('\n');
  }

  void put (const keyword_string k,          float     v) { put (k, (double)v); }
  void put (const keyword_string k,   signed char      v) { put_(k, (sint32)v); }
  void put (const keyword_string k, unsigned char      v) { put_(k, (sint32)v); }
  void put (const keyword_string k,   signed short     v) { put_(k, (sint32)v); }
  void put (const keyword_string k, unsigned short     v) { put_(k, (sint32)v); }
  void put (const keyword_string k,   signed int       v) { put_(k, (sint32)v); }
  void put (const keyword_string k, unsigned int       v) { put_(k, (sint64)v); }
  void put (const keyword_string k,   signed long      v) { put_(k, (sint64)v); }
  void put (const keyword_string k, unsigned long      v) { put_(k, (sint64)v); }
  void put (const keyword_string k,   signed long long v) { put_(k, (sint64)v); }
  void put (const keyword_string k, unsigned long long v) { put_(k, (sint64)v); }

  void put (const keyword_string kbeg, const keyword_string kend, shstr_tmp v)
  {
    force (MAX_KEYWORD_LEN + 1);
    fadd (kbeg.s, kbeg.l); fadd ('\n');

    if (expect_true (v))
      {
        add (v);
        add ('\n');
      }

    force (MAX_KEYWORD_LEN + 1);
    fadd (kend.s, kend.l); fadd ('\n');
  }

  void put (const keyword_string k, archetype *v);
  void put (const keyword_string k, treasurelist *v);
  void put (const keyword_string k, faceinfo *v);

  template<typename T>
  void put (const keyword_string k, const refptr<T> &v)
  {
    put (k, (T *)v);
  }

  MTH bool save (const_octet_string path);
  utf8_string as_string (); // like strdup

  operator bool () { return !!av; }
};

// used as dir argument to object_thawer constructor
#define RESOURCE_DIR const_utf8_string (0)

INTERFACE_CLASS(object_thawer)
struct object_thawer
{
  static bool errors_are_fatal; // true during server startup only

  char *line; // current beginning of line
  SV *text; // text part
  AV *av;   // perl part
  int linenum;
  keyword kw;
  char *kw_str; // the keyword parsed, as string
  char *value;  // the value, or 0 if no value
  const char *value_nn; // the value, or the empty string if no value
  const char *name;

  operator bool () const { return !!text; }

  object_thawer (const_utf8_string path = 0);
  object_thawer (const_utf8_string dir, const_utf8_string file);
  object_thawer (const_utf8_string data, AV *perlav);
  ~object_thawer ();

  void get (attachable *obj, int oid);

  // parse next line as keyword-value pair
  MTH void next ();

  // parse next line, as a single value
  // skips initial whitespace and comments
  // and sets kw to KW_value on success.
  MTH bool next_line ();

  // skip the current key-value (usually fetch next line, for
  // multiline-fields, skips till the corresponding end-kw
  MTH void skip ();
  MTH void skip_block (); // skips till and over KW_end

  // returns true IFF a value was specified and it is not the empty string
  // formerly, this just etsetd for a value, but the editor is too dumb
  // and creates lots of empty values.
  bool has_value () const { return *value_nn; }
  const_utf8_string get_str () const { return value_nn; } // empty string when missing
  void get_ml (keyword kend, shstr &sh);

  void get_ornull (shstr &sh) const { sh = value; }
  void get (shstr &sh)        const { sh = value; } // might want to check for non-null here

  bool   get_bool   () const { return *value_nn == '1'; }
  sint32 get_sint32 () const;
  sint64 get_sint64 () const { return strtoll (value_nn, 0, 10); }
  double get_double () const { return strtod  (value_nn, 0); }

  void get (float  &v) const { v = get_double (); }
  void get (double &v) const { v = get_double (); }

  void get (bool   &i) const { i = get_bool   (); }
  void get (sint8  &i) const { i = get_sint32 (); }
  void get (uint8  &i) const { i = get_sint32 (); }
  void get (sint16 &i) const { i = get_sint32 (); }
  void get (uint16 &i) const { i = get_sint32 (); }
  void get (sint32 &i) const { i = get_sint32 (); }

  void get (uint32 &i) const { i = get_sint64 (); }
  void get (sint64 &i) const { i = get_sint64 (); }

  void get (region_ptr &r) const;
  void get (materialtype_t *&mt) const;

  MTH void parse_warn (const_utf8_string msg) const;
  MTH bool parse_error (const_utf8_string type = 0, const_utf8_string name = 0, bool skip = true) const;

  struct delayed_ref {
    attachable *op;
    object_ptr *ptr;
    const_utf8_string ref;
  };
  std::vector<delayed_ref> delrefs;

  void delayed_deref (attachable *op, object_ptr &ptr, const_utf8_string ref);
  MTH void resolve_delayed_derefs (bool deref = true);
private:
  void init (const_utf8_string path);
  void init_from_file (const_utf8_string path);
};

#endif

