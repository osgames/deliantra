/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#ifndef LOGGER_H
#define LOGGER_H

enum {
  llevError    = 0,
  llevWarn     = 1,
  llevInfo     = 2,
  llevDebug    = 3,
  llevTrace    = 4,

  logBacktrace = 0x10, // log the backtrace too, possibly delayed
  logSync      = 0x20, // log synchronously (AND OUT OF ORDER!)
};

int log_setfd (int fd);
void log_backtrace (const_utf8_string msg);
void log_cleanup ();
void LOG (int flags, const_utf8_string format, ...);

void log_suspend ();
void log_resume ();

#endif /* LOGGER_H */
