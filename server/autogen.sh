#!/bin/sh
#libtoolize -f -c
#mv -f ltmain.sh utils
aclocal-1.15 -I . || aclocal -I .
autoheader
automake-1.15 -a -c || automake -a -c
autoconf
if [ -x ../reconf ]; then
   ../reconf "$@"
else
   ./configure "$@"
fi

