/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#include <global.h>
#include <rmg.h>
#include <rproto.h>

/* where are there adjacent doors or walls? */
static int
surround_check2 (layout &maze, int i, int j)
{
  /* 1 = door or wall to left,
     2 = door or wall to right,
     4 = door or wall above
     8 = door or wall below */
  int surround_index = 0;

  if ((i >          0) && (maze[i - 1][j] == 'D' || maze[i - 1][j] == '#')) surround_index |= 1;
  if ((i < maze.w - 1) && (maze[i + 1][j] == 'D' || maze[i + 1][j] == '#')) surround_index |= 2;
  if ((j >          0) && (maze[i][j - 1] == 'D' || maze[i][j - 1] == '#')) surround_index |= 4;
  if ((j < maze.h - 1) && (maze[i][j + 1] == 'D' || maze[i][j + 1] == '#')) surround_index |= 8;

  return surround_index;
}

void
put_doors (maptile *the_map, layout &maze, const char *doorstyle, random_map_params *RP)
{
  int i, j;
  maptile *vdoors;
  maptile *hdoors;
  char doorpath[1024];

  if (!strcmp (doorstyle, "none"))
    return;

  vdoors = find_style ("/styles/doorstyles", doorstyle, RP->difficulty);

  if (vdoors)
    hdoors = vdoors;
  else
    {
      vdoors = find_style ("/styles/doorstyles/vdoors", doorstyle, RP->difficulty);
      if (!vdoors)
        return;

      sprintf (doorpath, "/styles/doorstyles/hdoors%s", strrchr (vdoors->path, '/'));
      hdoors = find_style (doorpath, 0, RP->difficulty);
      if (!hdoors)
        return;
    }

  for (i = 0; i < RP->Xsize; i++)
    for (j = 0; j < RP->Ysize; j++)
      {
        if (maze[i][j] == 'D')
          {
            int sindex = surround_check2 (maze, i, j);

            object *this_door = (sindex == 3 ? hdoors : vdoors)
                                ->pick_random_object (rmg_rndm);

            the_map->insert (this_door->clone (), i, j, 0, 0);
          }
      }
}

