/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

/* Created July 95 to separate skill utilities from actual skills -b.t. */

/* Reconfigured skills code to allow linking of skills to experience
 * categories. This is done solely through the init_new_exp_system() fctn.
 * June/July 1995 -b.t. thomas@astro.psu.edu
 */

/* July 1995 - Initial associated skills coding. Experience gains
 * come solely from the use of skills. Overcoming an opponent (in combat,
 * finding/disarming a trap, stealing from somebeing, etc) gains
 * experience. Calc_skill_exp() handles the gained experience using
 * modifications in the skills[] table. - b.t.
 */

/* define the following for skills utility debuging */

/* #define SKILL_UTIL_DEBUG */

#include <global.h>
#include <object.h>
#include <sproto.h>
#include <living.h>             /* for defs of STR,CON,DEX,etc. -b.t. */
#include <spells.h>

const uint8_t skill_flags[NUM_SKILLS] = {
  0, // SK_NONE
# define def(uc, flags) flags,
#  include "skillinc.h"
# undef def
};

vector<object_ptr> skillvec;

static int attack_hth (object *pl, int dir, const char *string, object *skill);
static int attack_melee_weapon (object *op, int dir, const char *string, object *skill);

/* init_skills basically just sets up the skill_names table
 * above.  The index into the array is set up by the
 * subtypes.
 */
void
init_skills ()
{
  // nop
}

void
add_skill_archetype (object *o)
{
  assert (("skill name must equal skill skill", o->name == o->skill));

  for (vector<object_ptr>::iterator i = skillvec.begin (); i != skillvec.end (); ++i)
    if ((*i)->name == o->name)
      {
        // replace existing entry
        SKILL_INDEX (o) = i - skillvec.begin ();
        *i = o;
        return;
      }

  // add new entry
  assert (("only CS_NUM_SKILLS skills supported by client protocol", skillvec.size () < CS_NUM_SKILLS));
  SKILL_INDEX (o) = skillvec.size ();
  skillvec.push_back (o);
}

/* This function goes through the player inventory and sets
 * up the last_skills[] array in the player object.
 * the last_skills[] is used to more quickly lookup skills -
 * mostly used for sending exp.
 */
void
player::link_skills ()
{
  for (int i = 0; i < CS_NUM_SKILLS; ++i)
    last_skill_ob [i] = 0;

  for (object *tmp = ob->inv; tmp; tmp = tmp->below)
    if (tmp->type == SKILL)
      {
        int idx = SKILL_INDEX (tmp);

        assert (IN_RANGE_EXC (idx, 0, CS_NUM_SKILLS));

        if (last_skill_ob [idx] != tmp)
          {
            last_skill_ob [idx] = tmp;
            if (ns)
              ns->last_skill_exp [idx] = -1;
          }
      }
}

static object *
find_skill (object *who, shstr_cmp name)
{
  if (who->chosen_skill
      && who->chosen_skill->skill == name
      && who->chosen_skill->type == SKILL)
    return who->chosen_skill;

  for (object *tmp = who->inv; tmp; tmp = tmp->below)
    if (tmp->skill == name && tmp->type == SKILL)
      return splay (tmp);

  return 0;
}

object *
player::find_skill (shstr_cmp name) const
{
  // might want to use last_skill_obj at one point, or maybe not
  return ::find_skill (ob, name);
}

/* This returns the skill pointer of the given name (the
 * one that accumulates exp, has the level, etc).
 *
 * It is presumed that the player will be needing to actually
 * use the skill, so thus if use of the skill requires a skill
 * tool, this code will equip it.
 */
object *
find_skill_by_name (object *who, shstr_cmp sh)
{
  object *skill_tool = 0;

  for (object *tmp = who->inv; tmp; tmp = tmp->below)
    if (tmp->skill == sh)
      {
        if (tmp->type == SKILL && (tmp->flag [FLAG_CAN_USE_SKILL] || tmp->flag [FLAG_APPLIED]))
          /* If this is a skill that can be used without applying tool, return it */
          return splay (tmp);
        /* Try to find appropriate skilltool.  If the player has one already
         * applied, we try to keep using that one.
         */
        else if (tmp->type == SKILL_TOOL && !skill_tool)
          skill_tool = tmp;
      }

  if (!skill_tool)
    return 0;

  /* Player has a tool to use the skill. If not applied, apply it -
   * if not successful, return null. If they do have the skill tool
   * but not the skill itself, give it to them.
   */
  object *skill = who->give_skill (skill_tool->skill);

  if (!skill_tool->flag [FLAG_APPLIED])
    if (!who->apply (splay (skill_tool)))
      return 0;

  return splay (skill);
}

object *
find_skill_by_name_fuzzy (object *who, const char *name)
{
  if (name)
    for (object *tmp = who->inv; tmp; tmp = tmp->below)
      if ((tmp->type == SKILL || tmp->type == SKILL_TOOL)
          && tmp->skill.starts_with (name))
        if (object *skop = find_skill_by_name (who, tmp->skill))
          return skop;

  return 0;
}

/* This returns the skill pointer of the given name (the
 * one that accumulates exp, has the level, etc).
 *
 * It is presumed that the player will be needing to actually
 * use the skill, so thus if use of the skill requires a skill
 * tool, this code will equip it.
 *
 * This code is basically the same as find_skill_by_name() above,
 * but instead a skill name, we search by matching number.
 * this replaces find_skill.
 *
 * MUST NOT BE USED IN NEW CODE! (schmorp)
 */
object *
find_skill_by_number (object *who, int skillno)
{
  for (object *tmp = who->inv; tmp; tmp = tmp->below)
    if (tmp->type == SKILL && tmp->subtype == skillno)
      if (object *skop = find_skill_by_name (who, tmp->skill))
        return skop;

  return 0;
}

object *
object::give_skill (shstr_cmp name, bool can_use)
{
  object *skill = find_skill (this, name);

  if (!skill)
    skill = give_skill_by_name (this, name);

  if (skill && can_use)
    skill->flag [FLAG_CAN_USE_SKILL] = true;

  return skill;
}

/* do_skill() - Main skills use function-similar in scope to cast_spell().
 * We handle all requests for skill use outside of some combat here.
 * We require a separate routine outside of fire() so as to allow monsters
 * to utilize skills.  Returns 1 on use of skill, otherwise 0.
 * This is changed (2002-11-30) from the old method that returned
 * exp - no caller needed that info, but it also prevented the callers
 * from know if a skill was actually used, as many skills don't
 * give any exp for their direct use (eg, throwing).
 * It returns 0 if no skill was used.
 */
int
do_skill (object *op, object *part, object *skill, int dir, const char *string)
{
  int success = 0, exp = 0;

  if (!skill)
    return 0;

  /* The code below presumes that the skill points to the object that
   * holds the exp, level, etc of the skill.  So if this is a player
   * go and try to find the actual real skill pointer, and if the
   * the player doesn't have a bucket for that, create one.
   */
  if (skill->type != SKILL && op->type == PLAYER)
    {
      for (object *tmp = op->inv; tmp; tmp = tmp->below)
        if (tmp->type == SKILL && tmp->skill == skill->skill)
          {
            skill = tmp;
            goto found;
          }

      skill = give_skill_by_name (op, skill->skill);
found: ;
    }

  // skill, by_whom, on_which_object, which direction, skill_argument
  if (INVOKE_OBJECT (USE_SKILL, skill, ARG_OBJECT (op), ARG_OBJECT (part), ARG_INT (dir), ARG_STRING (string)))
    return 0;

  switch (skill->subtype)
    {
      case SK_LEVITATION:
        /* Not 100% sure if this will work with new movement code -
         * the levitation skill has move_type for flying, so when
         * equipped, that should transfer to player, when not,
         * shouldn't.
         */
        if (skill->flag [FLAG_APPLIED])
          {
            skill->clr_flag (FLAG_APPLIED);
            new_draw_info (NDI_UNIQUE, 0, op, "You come to earth.");
          }
        else
          {
            skill->set_flag (FLAG_APPLIED);
            new_draw_info (NDI_UNIQUE, 0, op, "You rise into the air!");
          }

        op->update_stats ();
        success = 1;
        break;

      case SK_STEALING:
        exp = success = steal (op, dir, skill);
        break;

      case SK_LOCKPICKING:
        exp = success = pick_lock (op, dir, skill);
        break;

      case SK_HIDING:
        exp = success = hide (op, skill);
        break;

      case SK_JUMPING:
        exp = success = jump (op, dir, skill);
        break;

      case SK_INSCRIPTION:
        exp = success = write_on_item (op, string, skill);
        break;

      case SK_MEDITATION:
        meditate (op, skill);
        success = 1;
        break;
        /* note that the following 'attack' skills gain exp through hit_player() */

      case SK_KARATE:
        attack_hth (op, dir, "karate-chopped", skill);
        break;

      case SK_PUNCHING:
        attack_hth (op, dir, "punched", skill);
        break;

      case SK_FLAME_TOUCH:
        attack_hth (op, dir, "flamed", skill);
        break;

      case SK_SPARK_TOUCH:
        attack_hth (op, dir, "zapped", skill);
        break;

      case SK_SHIVER:
        attack_hth (op, dir, "froze", skill);
        break;

      case SK_ACID_SPLASH:
        attack_hth (op, dir, "dissolved", skill);
        break;

      case SK_POISON_NAIL:
        attack_hth (op, dir, "injected poison into", skill);
        break;

      case SK_CLAWING:
        attack_hth (op, dir, "clawed", skill);
        break;

      case SK_ONE_HANDED_WEAPON:
      case SK_TWO_HANDED_WEAPON:
        attack_melee_weapon (op, dir, NULL, skill);
        break;

      case SK_FIND_TRAPS:
        exp = success = find_traps (op, skill);
        break;

      case SK_SINGING:
        exp = success = singing (op, dir, skill);
        break;

      case SK_ORATORY:
        exp = success = use_oratory (op, dir, skill);
        break;

      case SK_SMITHERY:
      case SK_BOWYER:
      case SK_JEWELER:
      case SK_ALCHEMY:
      case SK_THAUMATURGY:
      case SK_LITERACY:
      case SK_WOODSMAN:
        /* first, we try to find a cauldron, and do the alchemy thing.
         * failing that, we go and identify stuff.
         */
        {
          bool found_cauldron = false;

          for (object *next, *tmp = GET_MAP_OB (op->map, op->x, op->y); tmp; tmp = next)
            {
              next = tmp->above;

              if (tmp->flag [FLAG_IS_CAULDRON])
                {
                  found_cauldron = true;

                  if (tmp->skill != skill->skill)
                    {
                      op->failmsgf ("You can't use the %s with the %s skill!",
                                    query_name (tmp),
                                    query_name (skill));
                      break;
                    }

                  attempt_do_alchemy (op, tmp, skill);

                  if (tmp->flag [FLAG_APPLIED])
                    esrv_send_inventory (op, tmp);
                }
            }

          if (!found_cauldron)
            exp = success = skill_ident (op, skill);
        }
        break;

      case SK_DET_MAGIC:
      case SK_DET_CURSE:
        exp = success = skill_ident (op, skill);
        break;

      case SK_DISARM_TRAPS:
        exp = success = remove_trap (op, dir, skill);
        break;

      case SK_THROWING:
        success = skill_throw (op, part, dir, string, skill);
        break;

      case SK_SET_TRAP:
        new_draw_info (NDI_UNIQUE, 0, op, "This skill is not currently implemented.");
        break;

      case SK_USE_MAGIC_ITEM:
      case SK_MISSILE_WEAPON:
        new_draw_info (NDI_UNIQUE, 0, op, "There is no special attack for this skill.");
        break;

      case SK_PRAYING:
        success = pray (op, skill);
        break;

      case SK_BARGAINING:
        success = describe_shop (op);
        break;

      case SK_SORCERY:
      case SK_EVOCATION:
      case SK_PYROMANCY:
      case SK_SUMMONING:
      case SK_CLIMBING:
        new_draw_info (NDI_UNIQUE, 0, op, "This skill is already in effect.");
        break;

      case SK_MINING:
        success = skill_mining (op, part, skill, dir, string);
        break;

      default:
        LOG (llevDebug, "%s attempted to use unknown skill: %d\n", query_name (op), op->chosen_skill->stats.sp);
        break;
    }

  /* For players we now update the speed_left from using the skill.
   * Monsters have no skill use time because of the random nature in
   * which use_monster_skill is called already simulates this.
   * If certain skills should take more/less time, that should be
   * in the code for the skill itself.
   */
  if (op->type == PLAYER)
    op->speed_left -= 1.f;

  /* this is a good place to add experience for successfull use of skills.
   * Note that add_exp() will figure out player/monster experience
   * gain problems.
   */

  if (success && exp)
    change_exp (op, exp, skill->skill, 0);

  return success;
}

/* calc_skill_exp() - calculates amount of experience can be gained for
 * successfull use of a skill.  Returns value of experience gain.
 * Here we take the view that a player must 'overcome an opponent'
 * in order to gain experience. Examples include foes killed combat,
 * finding/disarming a trap, stealing from somebeing, etc.
 * The gained experience is based primarily on the difference in levels,
 * exp point value of vanquished foe, the relevent stats of the skill being
 * used and modifications in the skills[] table.
 *
 * For now, monsters and players will be treated differently. Below I give
 * the algorithm for *PLAYER* experience gain. Monster exp gain is simpler.
 * Monsters just get 10% of the exp of the opponent.
 *
 * players get a ratio, eg, opponent lvl / player level.  This is then
 * multiplied by various things.  If simple exp is true, then
 * this multiplier, include the level difference, is always 1.
 * This revised method prevents some cases where there are big gaps
 * in the amount you get just because you are now equal level vs lower
 * level
 * who is player/creature that used the skill.
 * op is the object that was 'defeated'.
 * skill is the skill used.  If no skill is used, it should just
 * point back to who.
 *
 */
int
calc_skill_exp (object *who, object *op, object *skill)
{
  int op_exp = 0, op_lvl = 0;
  float base, value, lvl_mult = 0.0;

  if (!skill)
    skill = who;

  /* Oct 95 - where we have an object, I expanded our treatment
   * to 3 cases:
   * non-living magic obj, runes and everything else.
   *
   * If an object is not alive and magical we set the base exp higher to
   * help out exp awards for skill_ident skills. Also, if
   * an item is type RUNE, we give out exp based on stats.Cha
   * and level (this was the old system) -b.t.
   */
  if (!op)
    {                           /* no item/creature */
      op_lvl = max (1, who->map->difficulty);
      op_exp = 0;
    }
  else if (op->type == RUNE || op->type == TRAP)
    {                           /* all traps. If stats.Cha > 1 we use that
                                 * for the amount of experience */
      op_exp = op->stats.Cha > 1 ? op->stats.Cha : op->stats.exp;
      op_lvl = op->level;
    }
  else
    {                           /* all other items/living creatures */
      op_exp = op->stats.exp;
      op_lvl = op->level;
      if (!op->flag [FLAG_ALIVE])
        op_lvl += 5 * abs (op->magic); /* for ident/make items */
    }

  if (op_lvl < 1)
    op_lvl = 1;

  if (who->type != PLAYER)
    {                           /* for monsters only */
      return ((int) (op_exp * 0.1) + 1);        /* we add one to insure positive value is returned */
    }
  else
    {                           /* for players */
      base = op_exp;
      /* if skill really is a skill, then we can look at the skill archetype for
       * bse reward value (exp) and level multiplier factor.
       */
      if (skill->type == SKILL)
        {
          base += skill->arch->stats.exp;
          if (settings.simple_exp)
            {
              if (skill->arch->level)
                lvl_mult = (float) skill->arch->level / 100.0;
              else
                lvl_mult = 1.0; /* no adjustment */
            }
          else
            {
              if (skill->level)
                lvl_mult = ((float) skill->arch->level * (float) op_lvl) / ((float) skill->level * 100.0);
              else
                lvl_mult = 1.0;
            }
        }
      else
        {
          /* Don't divide by zero here! */
          lvl_mult = (float) op_lvl / (float) max (1, skill->level);
        }
    }

  /* assemble the exp total, and return value */

  value = base * lvl_mult;
  if (value < 1)
    value = 1;                  /* Always give at least 1 exp point */

#ifdef SKILL_UTIL_DEBUG
  LOG (llevDebug, "calc_skill_exp(): who: %s(lvl:%d)  op:%s(lvl:%d)\n", who->name, skill->level, op->name, op_lvl);
#endif
  return ((int) value);
}

/* Learn skill. This inserts the requested skill in the player's
 * inventory. The skill field of the scroll should have the
 * exact name of the requested skill.
 * This one actually teaches the player the skill as something
 * they can equip.
 * Return 0 if the player knows the skill, 1 if the
 * player learns the skill, 2 otherwise.
 */
int
learn_skill (object *pl, object *scroll)
{
  if (!scroll->skill)
    {
      LOG (llevError, "skill scroll %s does not have skill pointer set.\n", &scroll->name);
      return 2;
    }

  object *tmp = find_skill (pl, scroll->skill);

  /* player already knows it */
  if (tmp && tmp->flag [FLAG_CAN_USE_SKILL])
    return 0;

  /* now a random change to learn, based on player Int.
   * give bonus based on level - otherwise stupid characters
   * might never be able to learn anything.
   */
  if (random_roll (0, 99, pl, PREFER_LOW) > (learn_spell[pl->stats.Int] + (pl->level / 5)))
    return 2;                   /* failure :< */

  if (!tmp)
    tmp = give_skill_by_name (pl, scroll->skill);

  if (!tmp)
    {
      LOG (llevError, "skill scroll %s does not have valid skill name (%s).\n", &scroll->name, &scroll->skill);
      return 2;
    }

  tmp->set_flag (FLAG_CAN_USE_SKILL);

  return 1;
}

/* Gives a percentage clipped to 0% -> 100% of a/b. */
/* Probably belongs in some global utils-type file? */
static int
clipped_percent (sint64 a, sint64 b)
{
  int rv;

  if (b <= 0)
    return 0;

  rv = (int) ((100.0f * ((float) a) / ((float) b)) + 0.5f);

  if (rv < 0)
    return 0;
  else if (rv > 100)
    return 100;

  return rv;
}

static int
cmp_skillp (const void *sk1, const void *sk2)
{
  return strcmp (&((*(const object **) sk1)->name),
                 &((*(const object **) sk2)->name));
}

/* show_skills() - Meant to allow players to examine
 * their current skill list.
 * This shows the amount of exp they have in the skills.
 * we also include some other non skill related info (god,
 * max weapon improvments, item power).
 * Note this function is a bit more complicated because we
 * we want to sort the skills before printing them. If we
 * just dumped this as we found it, this would be a bit
 * simpler.
 */
void
show_skills (object *pl, const char *search)
{
  const char *cp;
  int i, num_skills_found = 0;
  object *skills[CS_NUM_SKILLS];
  object *op = pl->contr->ob;

  /* find the skills */
  for (object *tmp = op->inv; tmp; tmp = tmp->below)
    {
      if (tmp->type == SKILL)
        {
          if (search && !tmp->name.contains (search))
            continue;

          skills[num_skills_found++] = tmp;

          /* I don't know why some characters get a bunch of skills, but
           * it sometimes happens (maybe a leftover from bugier earlier code
           * and those character are still about).  In any case, lets handle
           * it so it doesn't crash the server - otherwise, one character may
           * crash the server numerous times.
           */
          if (num_skills_found >= CS_NUM_SKILLS)
            {
              new_draw_info (NDI_RED | NDI_REPLY, 0, op, "Your character has too many skills.");
              new_draw_info (NDI_RED | NDI_REPLY, 0, op, "Something isn't right - contact the server admin");
              break;
            }
        }
    }

  dynbuf_text &msg = msg_dynbuf; msg.clear ();

  msg << "T<Player skills:>\n\n";
  if (num_skills_found > 1)
    qsort (skills, num_skills_found, sizeof (skills [0]), cmp_skillp);

  char buf[31]; /* +1 for '\0' */
  const char *const periods = ".............................."; // 30

  for (i = 0; i < num_skills_found; i++) {
    object *tmp = skills[i];

    /* Basically want to fill this out to 30 spaces with periods */
    snprintf (buf, sizeof (buf), "%s%s", &tmp->name, periods);

    msg << " C<";

    if (settings.permanent_exp_ratio)
      msg.printf ("%slvl:%3d (xp:%" PRId64 "/%" PRId64 "/%d%%)",
                  buf, tmp->level, tmp->stats.exp, level_exp (tmp->level + 1, op->expmul),
		  clipped_percent (tmp->perm_exp, tmp->stats.exp));
    else
      msg.printf ("%slvl:%3d (xp:%" PRId64 "/%" PRId64 ")",
                  buf, tmp->level, tmp->stats.exp, level_exp (tmp->level + 1, op->expmul));

    msg << ">\n";
  }

  msg << "\nYou can handle " << op->level / 5 + 5 << " weapon improvements.\r";

  cp = determine_god (op);
  msg << "You worship " << (cp ? cp : "no god at current time") << ".\r";

  msg << "Your equipped item power is " << (int)op->contr->item_power
      << " out of " << int (op->level * settings.item_power_factor)
      << ".\n";

  pl->contr->infobox (MSG_CHANNEL ("skills"), msg);
}

/* use_skill() - similar to invoke command, it executes the skill in the
 * direction that the user is facing. Returns false if we are unable to
 * change to the requested skill, or were unable to use the skill properly.
 * This is tricky because skills can have spaces.  We basically roll
 * our own find_skill_by_name so we can try to do better string matching.
 */
int
use_skill (object *op, const char *string)
{
  object *skop;
  size_t len;

  if (!string)
    return 0;

  for (skop = op->inv; skop; skop = skop->below)
    if ((skop->type == SKILL || skop->type == SKILL_TOOL)
        && !strncmp (string, skop->skill, min (strlen (string), strlen (skop->skill))))
      {
        skop = find_skill_by_name (op, skop->skill);
        break;
      }

  if (!skop)
    {
      op->failmsgf ("Unable to find skill %s.", string);
      return 0;
    }

  if (!(skill_flags [skop->subtype] & SF_USE))
    {
      op->failmsgf (
         "You feel as if you wanted to do something funny, but you can't remember what. "
         "H<The %s skill cannot be C<use_skill>'ed - maybe you need to C<ready_skill> it, "
         "use it with some item, or it's always active.>",
         &skop->skill
      );
      return 0;
    }

  len = strlen (skop->skill);

  /* All this logic goes and skips over the skill name to find any
   * options given to the skill.  Its pretty simple - if there
   * are extra parameters (as deteremined by string length), we
   * want to skip over any leading spaces.
   */
  if (len >= strlen (string))
    string = NULL;
  else
    {
      string += len;
      while (*string == 0x20)
        string++;

      if (strlen (string) == 0)
        string = NULL;
    }

#ifdef SKILL_UTIL_DEBUG
  LOG (llevDebug, "use_skill() got skill: %s\n", sknum > -1 ? skills[sknum].name : "none");
#endif

  if (do_skill (op, op, skop, op->facing, string))
    return 1;

  return 0;
}

static bool
hth_skill_p (object *skill)
{
  return (skill_flags [skill->subtype] & (SF_COMBAT | SF_NEED_ITEM)) == SF_COMBAT;
}

/* This finds the first unarmed skill the player has, and returns it.
 */
static object *
find_player_hth_skill (object *op)
{
  for (object *tmp = op->inv; tmp; tmp = tmp->below)
    if (tmp->type == SKILL && tmp->flag [FLAG_CAN_USE_SKILL] && hth_skill_p (tmp))
      return tmp;

  return 0;
}

/* do_skill_attack() - We have got an appropriate opponent from either
 * move_player_attack() or skill_attack(). In this part we get on with
 * attacking, take care of messages from the attack and changes in invisible.
 * Returns true if the attack damaged the opponent.
 * tmp is the targetted monster.
 * op is what is attacking
 * string is passed along to describe what messages to describe
 * the damage.
 */
static int
do_skill_attack (object *tmp, object *op, const char *string, object *skill)
{
  if (INVOKE_OBJECT (SKILL_ATTACK, op, ARG_OBJECT (tmp), ARG_STRING (string), ARG_OBJECT (skill)))
    return RESULT_INT (0);

  /* For Players only: if there is no ready weapon, and no "attack" skill
   * is readied either then try to find a skill for the player to use.
   * it is presumed that if skill is set, it is a valid attack skill (eg,
   * the caller should have set it appropriately).  We still want to pass
   * through that code if skill is set to change to the skill.
   */
  if (player *pl = op->contr)
    {
      if (skill)
        {
          if (!op->apply (skill))
            return 0;
        }
      else
        {
          if (!pl->combat_ob)
            {
              if (op->flag [FLAG_READY_WEAPON])
                {
                  for (tmp = op->inv; tmp; tmp = tmp->below)
                    if (tmp->type == WEAPON && tmp->flag [FLAG_APPLIED])
                      break;

                  if (!tmp)
                    LOG (llevError, "Could not find applied weapon on %s\n", &op->name);

                  pl->combat_ob = tmp;
                }

              if (!pl->combat_ob)
                {
                  /* See if the players chosen skill is a combat skill, and use
                   * it if appropriate.
                   */
                  if (op->chosen_skill && hth_skill_p (op->chosen_skill))
                    skill = op->chosen_skill;
                  else
                    {
                      skill = find_player_hth_skill (op);

                      if (!skill)
                        new_draw_info (NDI_BLACK, 0, op, "You have no unarmed combat skills!");
                    }

                  op->apply (skill);
                }

              if (!pl->combat_ob)
                {
                  LOG (llevError, "Could not find anything to attack on %s\n", &op->name);
                  return 0;
                }
            }

          if (!op->apply (pl->combat_ob))
            return 0;

          if (!op->chosen_skill)
            {
              LOG (llevError, "do_skill_attack: weapon has no skill (%s)", pl->combat_ob->debug_desc ());
              new_draw_info (NDI_RED | NDI_REPLY, 0, op, "You hit a bug in the server, please contact an admin!");
              return 0;
            }
        }

      /* lose invisiblity/hiding status for running attacks */
      if (pl->tmp_invis)
        {
          pl->tmp_invis = 0;
          op->invisible = 0;
          op->flag [FLAG_HIDDEN] = 0;
          update_object (op, UP_OBJ_CHANGE);
        }
    }

  int success = attack_ob (tmp, op);

  /* print appropriate  messages to the player */

  if (success && string && tmp && !tmp->flag [FLAG_FREED])
    {
      if (op->type == PLAYER)
        new_draw_info_format (NDI_UNIQUE, 0, op, "You %s %s!", string, query_name (tmp));
      else if (tmp->type == PLAYER)
        new_draw_info_format (NDI_UNIQUE, 0, tmp, "%s %s you!", query_name (op), string);
    }

  return success;
}

/* skill_attack() - Core routine for use when we attack using a skills
 * system. In essence, this code handles
 * all skill-based attacks, i.e. hth, missile and melee weapons should be
 * treated here. If an opponent is already supplied by move_player(),
 * we move right onto do_skill_attack(), otherwise we find if an
 * appropriate opponent exists.
 *
 * This is called by move_player() and attack_hth()
 *
 * Initial implementation by -bt thomas@astro.psu.edu
 */
int
skill_attack (object *tmp, object *pl, int dir, const char *string, object *skill)
{
  sint16 tx, ty;
  maptile *m;
  int mflags;

  if (!dir)
    dir = pl->facing;

  tx = DIRX (dir);
  ty = DIRY (dir);

  /* If we don't yet have an opponent, find if one exists, and attack.
   * Legal opponents are the same as outlined in move_player_attack()
   */
  if (!tmp)
    {
      m = pl->map;
      tx = pl->x + DIRX (dir);
      ty = pl->y + DIRY (dir);

      mflags = get_map_flags (m, &m, tx, ty, &tx, &ty);
      if (mflags & P_OUT_OF_MAP)
        return 0;

      /* space must be blocked for there to be anything interesting to do */
      if (!OB_TYPE_MOVE_BLOCK (pl, GET_MAP_MOVE_BLOCK (m, tx, ty)))
        return 0;

      for (tmp = GET_MAP_OB (m, tx, ty); tmp; tmp = tmp->above)
        if ((tmp->flag [FLAG_ALIVE] && tmp->stats.hp >= 0) || tmp->flag [FLAG_CAN_ROLL] || tmp->type == LOCKED_DOOR)
          {
            /* Don't attack party members */
            if ((pl->type == PLAYER && tmp->type == PLAYER) && (pl->contr->party != NULL && pl->contr->party == tmp->contr->party))
              return 0;

            break;
          }
    }

  if (!tmp)
    {
      if (pl->type == PLAYER)
        new_draw_info (NDI_UNIQUE, 0, pl, "There is nothing to attack!");

      return 0;
    }

  return do_skill_attack (tmp, pl, string, skill);
}

/* attack_hth() - this handles all hand-to-hand attacks -b.t. */

/* July 5, 1995 - I broke up attack_hth() into 2 parts. In the first
 * (attack_hth) we check for weapon use, etc in the second (the new
 * function skill_attack() we actually attack.
 */
static int
attack_hth (object *pl, int dir, const char *string, object *skill)
{
  object *enemy = NULL, *weapon;

  if (pl->flag [FLAG_READY_WEAPON])
    for (weapon = pl->inv; weapon; weapon = weapon->below)
      {
        if (weapon->type == WEAPON && weapon->flag [FLAG_APPLIED])
          {
            weapon->clr_flag (FLAG_APPLIED);
            pl->clr_flag (FLAG_READY_WEAPON);
            pl->update_stats ();
            if (pl->type == PLAYER)
              {
                new_draw_info (NDI_UNIQUE, 0, pl, "You unwield your weapon in order to attack.");
                esrv_update_item (UPD_FLAGS, pl, weapon);
              }

            break;
          }
      }

  return skill_attack (enemy, pl, dir, string, skill);
}

/* attack_melee_weapon() - this handles melee weapon attacks -b.t.
 * For now we are just checking to see if we have a ready weapon here.
 * But there is a real neato possible feature of this scheme which
 * bears mentioning:
 * Since we are only calling this from do_skill() in the future
 * we may make this routine handle 'special' melee weapons attacks
 * (like disarming manuever with sai) based on player SK_level and
 * weapon type.
 */
static int
attack_melee_weapon (object *op, int dir, const char *string, object *skill)
{

  if (!op->flag [FLAG_READY_WEAPON])
    {
      if (op->type == PLAYER)
        new_draw_info (NDI_UNIQUE, 0, op, "You have no ready weapon to attack with!");

      return 0;
    }

  return skill_attack (NULL, op, dir, string, skill);
}

