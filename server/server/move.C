/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2002 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#include <global.h>
#ifndef __CEXTRACT__
# include <sproto.h>
#endif

int
object::move (int dir, object *originator)
{
  sint16 newx = x + DIRX (dir);
  sint16 newy = y + DIRY (dir);

  mapxy pos (this);
  pos.move (dir);

  /* If the space the object is moving to is out of the map,
   * bail now - we know it can't work.
   */
  if (!pos.normalise ())
    return 0;

  /* Is this space blocked?  Players with wizpass are immune to
   * this condition.
   */
  if (blocked_link (this, pos.m, pos.x, pos.y) && !flag [FLAG_WIZPASS])
    return 0;

  // check tail movability
  if (more && !more->move (dir, more->head))
    return 0;

  /* we need to set the direction for the new animation code.
   * it uses it to figure out face to use - I can't see it
   * breaking anything, but it might.
   */
  direction = dir;

  if (will_apply & 4)
    check_earthwalls (this, pos.m, pos.x, pos.y);

  if (will_apply & 8)
    check_doors (this, pos.m, pos.x, pos.y);

  /* If this is a tail portion, just want to tell caller that move is
   * ok - the caller will deal with actual object removal/insertion
   */
  if (head)
    return 1;

  if (pos.m != map && contr)
    {
      if (INVOKE_MAP (LEAVE, map, ARG_PLAYER (contr)))
        return 0;

      remove ();

      pos.m->activate ();

      if (INVOKE_PLAYER (MAP_CHANGE, contr, ARG_MAP (pos.m), ARG_INT (pos.x), ARG_INT (pos.y)))
        return 0;

      if (INVOKE_MAP (ENTER, pos.m, ARG_PLAYER (contr), ARG_INT (pos.x), ARG_INT (pos.y)))
        return 0;
    }

  /* insert_ob_in_map will deal with any tiling issues */
  pos.insert (this, originator);

  return 1;
}

/*
 * transfer_ob(): Move an object (even linked objects) to another spot
 * on the same map.
 *
 * Does nothing if there is no free spot.
 *
 * randomly: If true, use find_free_spot() to find the destination, otherwise
 * use find_first_free_spot().
 *
 * Return value: 1 if object was destroyed, 0 otherwise.
 */

int
transfer_ob (object *op, int x, int y, int randomly, object *originator)
{
  int i;

  if (randomly)
    i = find_free_spot (op, op->map, x, y, 0, SIZEOFFREE);
  else
    i = find_first_free_spot (op, op->map, x, y);

  if (i == -1)
    return 0;                   /* No free spot */

  op = op->head_ ();
  op->remove ();

  for (object *tmp = op; tmp; tmp = tmp->more)
    {
      tmp->x = x + DIRX (i) + tmp->arch->x;
      tmp->y = y + DIRY (i) + tmp->arch->y;
    }

  op = insert_ob_in_map (op, op->map, originator, 0);

  return !op;
}

/*
 * Return value: 1 if object was destroyed, 0 otherwise.
 * Modified so that instead of passing the 'originator' that had no
 * real use, instead we pass the 'user' of the teleporter.  All the
 * callers know what they wanted to teleporter (move_teleporter or
 * shop map code)
 * tele_type is the type of teleporter we want to match against -
 * currently, this is either set to SHOP_MAT or TELEPORTER.
 * It is basically used so that shop_mats and normal teleporters can
 * be used close to each other and not have the player put to the
 * one of another type.
 */
int
teleport (object *teleporter, uint8 tele_type, object *user)
{
  if (!user)
    return 0;

  object *other_teleporter = 0;
  int nrofalt = 0;

  user = user->head_ ();

  /* Find all other teleporters within range.  This range
   * should really be setable by some object attribute instead of
   * using hard coded values.
   */
  unordered_mapwalk (mapwalk_buf, teleporter, -5, -5, 5, 5)
    {
      mapspace &ms = m->at (nx, ny);

      for (object *tmp = ms.top; tmp; tmp = tmp->below)
        if (tmp->type == tele_type)
          {
            if ((dx || dy) && !rndm (++nrofalt))
              other_teleporter = tmp;

            break;
          }
      }

  if (!nrofalt)
    {
      LOG (llevError, "%s: no alternative teleporters around (user %s).\n",
           teleporter->debug_desc (), user->debug_desc ());
      return 0;
    }

  int k = find_free_spot (user, other_teleporter->map, other_teleporter->x, other_teleporter->y, 1, SIZEOFFREE1 + 1);

  /* if k==-1, unable to find a free spot.  If this is shop
   * mat that the player is using, find someplace to move
   * the player - otherwise, player can get trapped in the shops
   * that appear in random dungeons.  We basically just make
   * sure the space isn't no pass (eg wall), and don't care
   * about is alive.
   */
  if (k == -1)
    {
      if (tele_type == SHOP_MAT && user->is_player ())
        {
          for (k = 1; k < 9; k++)
            {
              maptile *m;
              sint16 sx, sy;

              if (get_map_flags (other_teleporter->map, &m,
                                 other_teleporter->x + DIRX (k), other_teleporter->y + DIRY (k), &sx, &sy) & P_OUT_OF_MAP)
                continue;

              if (!OB_TYPE_MOVE_BLOCK (user, GET_MAP_MOVE_BLOCK (m, sx, sy)))
                break;
            }

          if (k == 9)
            {
              LOG (llevError, "Shop mat %s (%d, %d) is in solid rock?\n",
                   &other_teleporter->name, other_teleporter->x, other_teleporter->y);
              return 0;
            }
        }
      else
        return 0;
    }

  return !other_teleporter->map->insert (
            user, other_teleporter->x + DIRX (k), other_teleporter->y + DIRY (k)
         );
}

/*
 * This is a new version of blocked, this one handles objects
 * that can be passed through by monsters with the CAN_PASS_THRU defined.
 *
 * very new version handles also multipart objects
 * This is currently only used for the boulder roll code.
 * Returns 1 if object does not fit, 0 if it does.
 */
static int
try_fit (object *op, maptile *m, int x, int y)
{
  object *tmp, *more;
  sint16 tx, ty;
  int mflags;
  maptile *m2;

  if (op->head)
    op = op->head;

  for (more = op; more; more = more->more)
    {
      tx = x + more->x - op->x;
      ty = y + more->y - op->y;

      mflags = get_map_flags (m, &m2, tx, ty, &tx, &ty);

      if (mflags & P_OUT_OF_MAP)
        return 1;

      for (tmp = GET_MAP_OB (m2, tx, ty); tmp; tmp = tmp->above)
        {
          if (tmp->head == op || tmp == op)
            continue;

          if ((tmp->flag [FLAG_ALIVE] && tmp->type != DOOR))
            return 1;

          if (OB_MOVE_BLOCK (op, tmp))
            return 1;

        }
    }
  return 0;
}

/*
 * this is not perfect yet.
 * it does not roll objects behind multipart objects properly.
 * Support for rolling multipart objects is questionable.
 */
static int
roll_ob (object *op, int dir, object *pusher)
{
  sint16 x, y;
  int flags;
  maptile *m;
  MoveType move_block;

  if (op->head)
    op = op->head;

  x = op->x + DIRX (dir);
  y = op->y + DIRY (dir);

  if (!op->flag [FLAG_CAN_ROLL]
      || (op->weight && random_roll (0, op->weight / 50000 - 1, pusher, PREFER_LOW)
          > pusher->stats.Str))
    return 0;

  m = op->map;
  flags = get_map_flags (m, &m, x, y, &x, &y);

  if (flags & (P_OUT_OF_MAP | P_IS_ALIVE))
    return 0;

  move_block = GET_MAP_MOVE_BLOCK (m, x, y);

  /* If the target space is not blocked, no need to look at the objects on it */
  if ((op->move_type & move_block) == op->move_type)
    {
      for (object *tmp = GET_MAP_OB (m, x, y); tmp; tmp = tmp->above)
        {
          if (tmp->head == op)
            continue;

          if (OB_MOVE_BLOCK (op, tmp) && !roll_ob (tmp, dir, pusher))
            return 0;
        }
    }

  if (try_fit (op, m, x, y))
    return 0;

  op->move (dir);

  return 1;
}

void
recursive_roll (object *op, int dir, object *pusher)
{
  if (!roll_ob (op, dir, pusher))
    {
      new_draw_info_format (NDI_UNIQUE, 0, pusher, "You fail to push the %s.", query_name (op));
      return;
    }

  pusher->move (dir);
  new_draw_info_format (NDI_BLACK, 0, pusher, "You move the %s.", query_name (op));
  return;
}

/* returns 1 if pushing invokes a attack, 0 when not */
int
push_ob (object *who, int dir, object *pusher)
{
  int str1, str2;
  object *owner;

  if (who->head != NULL)
    who = who->head;
  owner = who->owner;

  /* Wake up sleeping monsters that may be pushed */
  who->clr_flag (FLAG_SLEEP);

  /* player change place with his pets or summoned creature */
  /* TODO: allow multi arch pushing. Can't be very difficult */
  if (who->more == NULL
      && ((owner && owner->contr && pusher->contr && same_party (owner->contr->party, pusher->contr->party))
          || owner == pusher)
    )
    {
      int temp;
      maptile *m;

      who->remove ();
      pusher->remove ();
      temp = pusher->x;
      pusher->x = who->x;
      who->x = temp;

      temp = pusher->y;
      pusher->y = who->y;
      who->y = temp;

      m = pusher->map;
      pusher->map = who->map;
      who->map = m;

      insert_ob_in_map (who, who->map, pusher, 0);
      insert_ob_in_map (pusher, pusher->map, pusher, 0);

      return 0;
    }

  /* We want ONLY become enemy of evil, unaggressive monster. We must RUN in them */
  /* In original we have here a unaggressive check only - that was the reason why */
  /* we so often become an enemy of friendly monsters... */
  /* funny: was they set to unaggressive 0 (= not so nice) they don't attack */
  if (owner != pusher && pusher->type == PLAYER && who->type != PLAYER &&
      !who->flag [FLAG_FRIENDLY] && !who->flag [FLAG_NEUTRAL])
    {
      if (pusher->contr->run_on)        /* only when we run */
        {
          new_draw_info_format (NDI_UNIQUE, 0, pusher, "You start to attack %s!!", &who->name);
          who->clr_flag (FLAG_UNAGGRESSIVE);  /* the sucker don't like you anymore */
          who->enemy = pusher;
          return 1;
        }
      else
        new_draw_info_format (NDI_UNIQUE, 0, pusher, "You avoid attacking %s. H<You have to run if you wish to attack unnagressive NPCs.>", &who->name);
    }

  /* now, let's test stand still. we NEVER can push stand_still monsters. */
  if (who->flag [FLAG_STAND_STILL])
    {
      new_draw_info_format (NDI_UNIQUE, 0, pusher, "You can't push %s. H<You simply can't.>", &who->name);
      return 0;
    }

  /* This block is basically if you are pushing friendly but
   * non pet creaturs.
   * It basically does a random strength comparision to
   * determine if you can push someone around.  Note that
   * this pushes the other person away - its not a swap.
   */

  str1 = (who->stats.Str > 0 ? who->stats.Str : who->level);
  str2 = (pusher->stats.Str > 0 ? pusher->stats.Str : pusher->level);
  if (who->flag [FLAG_WIZ] ||
      random_roll (str1, str1 * 5 / 2, who, PREFER_HIGH) >=
      random_roll (str2, str2 * 5 / 2, pusher, PREFER_HIGH) || !who->move (dir))
    {
      if (who->type == PLAYER)
        new_draw_info_format (NDI_UNIQUE, 0, who, "%s tried to push you.", &pusher->name);

      return 0;
    }

  /* If we get here, the push succeeded.
   * Let everyone know the status.
   */
  if (who->type == PLAYER)
    new_draw_info_format (NDI_UNIQUE, 0, who, "%s pushed you.", &pusher->name);

  if (pusher->type == PLAYER)
    new_draw_info_format (NDI_UNIQUE, 0, pusher, "You pushed %s back.", &who->name);

  return 1;
}
