/*
 * This file is part of Deliantra, the Roguelike Realtime MMORPG.
 *
 * Copyright (©) 2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Marc Alexander Lehmann / Robin Redeker / the Deliantra team
 * Copyright (©) 2001-2003 Mark Wedel & Crossfire Development Team
 * Copyright (©) 1992 Frank Tore Johansen
 *
 * Deliantra is free software: you can redistribute it and/or modify it under
 * the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */

#include <global.h>
#include <object.h>
#include <tod.h>

#include <sproto.h>
#include <time.h>

#include <glib.h>

#include <rmg.h>
#include <rproto.h>
#include "path.h"

void
version (object *op)
{
  new_draw_info_format (NDI_UNIQUE, 0, op, "This is Deliantra v%s", VERSION);
}

/* This is a basic little function to put the player back to his
 * savebed.
 */
void
enter_player_savebed (object *op)
{
  op->player_goto (op->contr->savebed_map, op->contr->bed_x, op->contr->bed_y);
}

/*
 * enter_map():  Moves the player and pets from current map (if any) to
 * new map.  map, x, y must be set.  map is the map we are moving the
 * player to - it could be the map he just came from if the load failed for
 * whatever reason.  If default map coordinates are to be used, then
 * the function that calls this should figure them out.
 */
bool
object::enter_map (maptile *newmap, int x, int y)
{
  if (destroyed () || !newmap || !newmap->linkable ())
    return false;

  if (out_of_map (newmap, x, y))
    {
      LOG (llevError, "enter_map: supplied coordinates are not within the map! (%s: %d, %d)\n", &newmap->path, x, y);
      x = newmap->enter_x;
      y = newmap->enter_y;
      if (out_of_map (newmap, x, y))
        {
          LOG (llevError, "enter_map: map %s provides invalid default enter location (%d, %d) > (%d, %d)\n",
               &newmap->path, x, y, newmap->width, newmap->height);
          new_draw_info (NDI_UNIQUE, 0, this, "The exit is closed");
          return false;
        }
    }

  if (contr && map != newmap && map)
    if (INVOKE_MAP (LEAVE, map, ARG_PLAYER (contr)))
      return false;

  // remove, so stupid blocked does not trigger a failure
  remove ();

  /* try to find a spot for the player */
  if (blocked (newmap, x, y))
    {                           /* First choice blocked */
      /* We try to find a spot for the player, starting closest in.
       * We could use find_first_free_spot, but that doesn't randomize it at all,
       * So for example, if the north space is free, you would always end up there even
       * if other spaces around are available.
       * Note that for the second and third calls, we could start at a position other
       * than one, but then we could end up on the other side of walls and so forth.
       */
      int i = find_free_spot (this, newmap, x, y, 1, SIZEOFFREE1 + 1);

      if (i < 0)
        {
          i = find_free_spot (this, newmap, x, y, 1, SIZEOFFREE2 + 1);
          if (i < 0)
            i = find_free_spot (this, newmap, x, y, 1, SIZEOFFREE);
        }

      if (i >= 0)
        {
          maptile *m = newmap;
          sint16 nx  = x + DIRX (i);
          sint16 ny  = y + DIRY (i);

          if (xy_normalise (m, nx, ny))
            {
              newmap = m;
              x = nx;
              y = ny;
            }
        }
      else
        /* not much we can do in this case. */
        LOG (llevInfo, "enter_map: Could not find free spot for player - will dump on top of object (%s: %d, %d)\n", &newmap->path, x, y);
    }

  if (contr && map != newmap)
    {
      if (INVOKE_PLAYER (MAP_CHANGE, contr, ARG_MAP (newmap), ARG_INT (x), ARG_INT (y)))
        return false;

      if (INVOKE_MAP (ENTER, newmap, ARG_PLAYER (contr), ARG_INT (x), ARG_INT (y)))
        return false;
    }

  enemy = 0;

  newmap->activate (); // workaround for activate activating everyhing on the map, includign the palyer on the {link} map
  //newmap->insert (this, x, y);
  newmap->insert (this, x, y, 0, INS_NO_AUTO_EXIT);
  prefetch_surrounding_maps ();

  if (map == newmap) // see if we actually arrived there - insert might trigger a teleport
    {
      if (contr)
        {
          contr->maplevel = newmap->path;
          contr->count = 0;

          /* Update any golems */
          if (object *golem = contr->golem)
            {
              int i = find_free_spot (golem, newmap, x, y, 1, SIZEOFFREE);

              if (i < 0)
                golem->drop_and_destroy ();
              else
                {
                  newmap->insert (golem, x + DIRX (i), y + DIRY (i));
                  golem->direction = find_dir_2 (golem->x - x, golem->y - y);
                }
            }
        }

      /* since the players map is already loaded, we don't need to worry
       * about pending objects.
       */
      move_all_pets ();

      return true;
    }

  return false;
}

/* process_players1 and process_players2 do all the player related stuff.
 * I moved it out of process events and process_map.  This was to some
 * extent for debugging as well as to get a better idea of the time used
 * by the various functions.  process_players1() does the processing before
 * objects have been updated, process_players2() does the processing that
 * is needed after the players have been updated.
 */
static void
process_players1 ()
{
  /* Basically, we keep looping until all the players have done their actions. */
  for (int flag = 1; flag != 0;)
    {
      flag = 0;
      for_all_players (pl)
        {
          pl->refcnt_chk ();

          if (expect_false (!pl->ob || !pl->ns || !pl->ob->active))
            continue;

          if (handle_newcs_player (pl->ob))
            flag = 1;
        }
    }

  for_all_players (pl)
    {
      object *ob = pl->ob;

      // process_objects destroys the speed_left value
      pl->speed_left_save = ob->speed_left;

      if (expect_false (!ob || !pl->ns || !ob->active))
        continue;

      do_some_living (ob);
    }
}

static void
process_players2 ()
{
  /* Then check if any players should use weapon-speed instead of speed */
  for_all_players (pl)
    {
      // restore speed_left value saved by process_players1
      pl->ob->speed_left = pl->speed_left_save;

      pl->weapon_sp_left = min (pl->weapon_sp, pl->weapon_sp_left + pl->weapon_sp);
      pl->ob->speed_left = min (pl->ob->speed, pl->ob->speed_left + pl->ob->speed);
    }
}

static void
process_events ()
{
  process_players1 ();

  for_all_actives (op)
    {
      // try to prefetch some stuff we expect to need
      // obviously, it should be grouped into the same cacheline.
      // preliminary results indicate that this gives halves the speed
      // used for the inner loop
      if (_i < actives.size ()) // HACK, rely on _i :/
        {
          object *next = actives [_i + 1];

          prefetch (&next->flag      , 0, 1);
          prefetch (&next->speed     , 0, 1);
          prefetch (&next->anim_speed, 0, 1);
          prefetch (&next->contr     , 0, 1);
        }

      /* Now process op */
      if (expect_false (op->flag [FLAG_FREED]))
        {
          LOG (llevError, "BUG: process_events(): Free object on list (%s)\n", op->debug_desc ());
          op->set_speed (0);
          continue;
        }

      if (expect_false (!op->has_active_speed ()))
        {
          LOG (llevError, "BUG: process_events(): Object %s has no speed (%f), "
                          "but is on active list\n", op->debug_desc (), op->speed);
          op->set_speed (0);
          continue;
        }

      if (expect_false (op->flag [FLAG_REMOVED]))
        {
          LOG (llevError, "BUG: process_events(): removed object is on active list: %s\n",
               op->debug_desc ());
          op->set_speed (0);
          continue;
        }

      /* Animate the object. Bug or feature that anim_speed
       * is based on ticks, and not the creatures speed?
       */
      if (op->anim_speed && op->last_anim >= op->anim_speed)
        {
          animate_object (op, op->contr ? op->facing : op->direction);
          op->last_anim = 1;
        }
      else
        ++op->last_anim;

      if (expect_false (op->speed_left > 0.f))
        {
          --op->speed_left;
          process_object (op);
        }

      // this will destroy the speed_left value for players, but
      // process_players1 and ..2 save/restore the real value,
      // so we can avoid a costly test here.
      op->speed_left = min (op->speed, op->speed_left + op->speed);
    }

  process_players2 ();
}

/* clean up everything before exiting */
void
emergency_save ()
{
  LOG (llevInfo, "emergency_save: enter\n");

  LOG (llevInfo, "emergency_save: saving book archive\n");
  write_book_archive ();

  cfperl_emergency_save ();

  LOG (llevInfo, "emergency_save: leave\n");
}

// send all clients some informational text
static void
cleanup_inform (const char *cause, bool make_core)
{
  int flags = NDI_REPLY | NDI_UNIQUE | NDI_ALL | (make_core ? NDI_RED : NDI_GREEN);

  new_draw_info_format (flags, 0, 0, "The server will now shutdown.");
  new_draw_info_format (flags, 0, 0, "Cause for this shutdown: %s", cause);

  if (make_core)
    new_draw_info_format (flags, 0, 0, "This is considered a crash, but all maps and players have been saved.");
  else
    new_draw_info_format (flags, 0, 0, "This is considered to be a clean shutdown, and all maps and players will be saved now.");

  new_draw_info_format (flags, 0, 0, "%s", CLEANUP_MESSAGE);

  client::flush_sockets ();
}

/* clean up everything before exiting */
void
cleanup (const char *cause, bool make_core)
{
  if (make_core)
    fork_abort (cause);

  LOG (make_core ? llevError : llevInfo, "cleanup cause: %s\n", cause);

  if (!make_core)
    cleanup_inform (cause, make_core);

  LOG (llevDebug, "cleanup begin.\n");

  if (init_done && !in_cleanup)
    {
      in_cleanup = true;
      emergency_save ();
    }
  else
    in_cleanup = true;

  LOG (llevDebug, "running cleanup handlers.\n");
  INVOKE_GLOBAL (CLEANUP);

  LOG (llevDebug, "cleanup done.\n");

  log_cleanup ();

  if (make_core)
    cleanup_inform (cause, make_core);
  else
    {
      new_draw_info_format (NDI_REPLY | NDI_UNIQUE | NDI_ALL | NDI_GREEN, 0, 0, "Maps and players successfully saved, exiting.");
      new_draw_info_format (NDI_REPLY | NDI_UNIQUE | NDI_ALL | NDI_GREEN, 0, 0, "And again: " CLEANUP_MESSAGE);
      client::flush_sockets ();
    }

  cfperl_cleanup (make_core);
  _exit (make_core);
}

/*
 * do_specials() is a collection of functions to call from time to time.
 * Modified 2000-1-14 MSW to use the global server_tick count to determine how
 * often to do things.  This will allow us to spred them out more often.
 * I use prime numbers for the factor count - in that way, it is less likely
 * these actions will fall on the same tick (compared to say using 500/2500/15000
 * which would mean on that 15,000 tick count a whole bunch of stuff gets
 * done).  Of course, there can still be times where multiple specials are
 * done on the same tick, but that will happen very infrequently
 *
 * I also think this code makes it easier to see how often we really are
 * doing the various things.
 */
static void
do_specials ()
{
  shstr::gc ();
  archetype::gc ();

  if (expect_false (!(server_tick % TICKS_PER_HOUR)))
    maptile::adjust_daylight ();

  if (expect_false (!(server_tick % 2503)))
    fix_weight ();              /* Hack to fix weightproblems caused by bugs */

  if (expect_false (!(server_tick % 5003)))
    write_book_archive ();

  if (expect_false (!(server_tick % 5009)))
    clean_friendly_list ();

  if (expect_false (!(server_tick % 5011)))
    obsolete_parties ();

  if (expect_false (!(server_tick % 12503)))
    fix_luck ();
}

void
one_tick ()
{
  // first do the user visible stuff
  INVOKE_GLOBAL (CLOCK);
  process_events (); // "do" something with objects with speed
  client::clock ();  // draw client maps etc.

  // then do some bookkeeping, should not really be here
  do_specials ();               /* Routines called from time to time. */
  attachable::check_mortals ();

  // now that we aggressively reuse id's, this is very unlikely to happen...
  if (object::object_count >= RESTART_COUNT)
    cleanup ("running out of protocol ID values - need full restart");
}

// normal main
int
main (int argc, char **argv)
{
  settings.argc = argc;
  settings.argv = argv;

  rndm.seed (time (0));

  // temporary(?) testing hack
  if (argc >= 2 && !strcmp (argv [1], "--noise"))
    {
      void noise_test ();
      noise_test ();
      exit (0);
    }

#if 0
  // code sometiems used for timing benchmarks
  random_number_generator<freeciv_random_generator> rng;
  rng.seed(0);
  for (int i = 0; i < 10; ++i)
    printf ("%08x\n", rng.get_u32());
  for (int i = 0; i < 1000000; ++i)
    volatile int r = rng.get_u32 ();
  cstamp s = stamp ();
  for (int i = 0; i < 1000000; ++i)
    volatile int r = rng.next ();
  printf ("c %d\n", (int)measure (s));
  exit (0);
#endif
  
  init (argc, argv);

  for (;;)
    cfperl_main ();
}

