=head1 DELIANTRA EXTENSION INTRODUCTION

In Deliantra, the plugin/extension and event API was completly rewritten
in Perl and C++. Here is a small guide or introduction on how to use it.

If you have any questions don't hesitate to contact the developers,
see: http://cf.schmorp.de/contact.shtml


=head2 Extension to <thing> attachments

You can "attach" extensions to global events, to type/subtypes,
to specifix objects, to players and to maps.
On top of that an extension can implement new user commands.

If an extension for example wants to attach itself to all jeweler
skills it has to attach itself like this:

   cf::attach_to_type cf::SKILL, cf::SK_JEWELER,
      on_use_skill => sub {
         ... handling code here ...
      };

This function attaches itself to the type SKILL with the subtype
SK_JEWELER. And everytime someone uses the skill the callback
registered as 'on_use_skill' is called.
Multiple extensions can attach themself to this type, and they
can specify a priority with 'prio => -100' to be executed earlier.

You can also attach a Perl package to the skill like this:

   cf::attach_to_type cf::SKILL, cf::SK_JEWELER,
      package => 'ext::JewelerSkill';

cf::attach_to_objects will attach handlers for events on _all_ objects
in the game, this is mainly for debugging purposes, as it will produce a
high load.

The map attachments work like this:

If an extension wants to attach itself to the 'trigger' event (this is
the event that is generated when a connection is activated (pushed or
released)), it has to do this:

   cf::attach_to_maps
      on_trigger => sub {
         my ($map, $connection, $state) = @_;

         print "CONNECITON TRIGGERED: $connection : $state\n";

         for ($map->find_link ($connection)) {
            print "connected obj: " . $_->name . "\n";
         }
      };

This small attachment dumps all connection activations and the connected
objects. If this attachment now decides to overwrite connection 10, so that
nothing happens if it is triggered, it has to do this:

   cf::attach_to_maps
      on_trigger => sub {
         my ($map, $connection, $state) = @_;

         if ($connection == 10) {
            cf::override;
            return;     # the idiom 'return cf::override;' is quite common in our code
                        # but i want to empasize that cf::override is just a functioncall
         }
      };

The call of cf::override sets a flag in the event system that will prevent any execution
of further code that handles this event. This way all attachments with 'lower' priority
and the C/C++ code is inhibited/overridden.

=head2 <thing> to extension attachments

The attachments are not limited to 'an extension attaches itself to <...>', a map or an
objects itself can attach to a 'registered' attachment. For example our nice cat which runs
around in scorn and heals people at random has following line in its archetype:

   Object nekosan
   ...
   attach [["Nekosan"]]
   end

The value of the attach field is a 2 dimensional array in JSON (JavaScript Object Notation)
is a array of arrays which contain following fields:
   [ <attachment name/key>, { <key value pairs of arguments for the attachment> }]

The code side of this looks like this:

   cf::register_attachment "Nekosan", package => __PACKAGE__;

This registeres an attachment under the name/key 'Nekosan' so that objects like our
cat can attach itself.

Where the package defines for example this function:
   sub on_monster_move {
      my ($self, $enemy) = @_;
      ...
   }

$self is a mostly empty object, the attachment object, which is initalized
with the arguments that are given in the 'attach' value, if nekosan would have an
attach field like this:

   attach [["Nekosan", {"foo":"bar"}]]

The attached function can access the value of the key "foo" like this:

    $self->{Nekosan}->{foo}

This way multiple different attachments have a seperate field for storing
their arguments.

=head2 Defining new user commands

If an extension wants to redefine a user command it does it like this:

   cf::register_command invite => 10, sub {
      my ($who, $args) = @_;
      ...
   }

This registers the 'invite' command with the execution time of 10. The
arguments to the function are ($who, $args), where $who is the player
and $args the argument string to the command.

=head2 Adding event invocations to the C++ code

There are already a lot of events implemented, look in the L<events.pod> for
a reference of existing events. But when you need a new event, here
is how to do add it:

Here an example for move_trigger in move_apply ():

Add a line to doc/events.pod with documentation looking like this:

   =head3 move_trigger (object victim originator -- )

   Invoked whenever a trap-like B<object> has been activated, usually by
   moving onto it. This includes not just traps, but also buttons, holes,
   signs and similar stuff.

And put some lines like this to move_apply:

   if (INVOKE_OBJECT (MOVE_TRIGGER, trap, ARG_OBJECT (victim), ARG_OBJECT (originator)))
      goto leave;

This invokes all attachments that are interested in the object event MOVE_TRIGGER, with
the object being 'trap' and the arguments victim and originator. This is all C++ code
that has to be added when one wants to add an event. 'make' will update include/eventinc.h
from the events.pod automatically when building.

=head2 Resources / See also

Here is the documentation for all this, which is maybe partly unfinished. But there
are also a lot of examples:

=over 4

=item lib/cf.pm

Has some documentation of the attachment API

=item pod/events.pod

A reference for all existing events

=item Examples

Examples can be found in http://cvs.schmorp.de/cf.schmorp.de/maps/perl/

=item NPC_Dialogue.pod in maps/perl/

The NPC dialogue system might also be interesting: http://cf.schmorp.de/doc/development/NPC_Dialogue.html
