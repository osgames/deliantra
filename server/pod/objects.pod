=head1 DELIANTRA OBJECT AND INTERNALS DOCUMENTATION

Here is all information about the object types Deliantra
supports at the moment. This is not a complete documentation (yet)
and browsing the source is still recommended to learn about
the objects that aren't documented here.

This documentation is in a sketchy state. It's mostly
used to collect notes about the internal behaviour of the
objects.

=head2 About the notation and terms

The term 'archetype' stands for a collection of fields.
The term 'object' stands for an archetype instance.
The term 'field' is used for an object fields and archetype fields.

Field names will be displayed like this: I<fieldname>

Type names will be displayed like this: B<TYPENAME>

Flag names will be displayer like this: FLAG_NAME

=head2 About archetypes and objects

Archetypes are 'templates' of objects. If an object is derived
from an archetype the object fields will be set to the corresponding
fields in the archetype.

When a map is instanciated (loaded), the 'object' description on the
map are considered patches of the archetype.

This document does explain the behaviour of the objects and the meaning of
their fields in the server engine, which are derived from archetypes.

This is an example of an archetype:

   object button_trigger
   name button
   type 30
   face button_sma.x11
   anim
   button_sma.x11
   button_sma.x12
   mina
   is_animated 0
   exp 30
   no_pick 1
   editable 48
   visibility 50
   weight 1
   end

The first B<field> is I<name>: 'button_trigger', which basically means that
instances (objects) that are created/derived from this archetype have the
name 'button' (which means that the field I<name> of the object will be set
to the same value as the archetypes field I<name>).

The next field I<type> decides the behaviour of objects derived from this archetype.
For a comprehensive list of types see include/define.h. For this case
you might find a line like:

   #define TRIGGER_BUTTON           30

The behaviour of objects is further determined by B<Flags>, like FLAG_APPLIED.
For more information on this look in the Flags subsection in the next section

The following documentation will also document the meaning of internal used
fields of objects. These fields are marked as (internal) and can't
or shouldn't be set by an archetype. 

=head2 Description of (mostly) generic object fields

These are the fields that most of the objects have and/or their
default behaviour.

=over 4

=item I<name> <string>

The name of the object.

=item I<name_pl> <string>

The name of a collection of these objects (the plural of the name).

=item I<face> <facename>

The graphical appearance of this object.

=item I<x> <number>

The x position of the object when it is on a map.

=item I<y> <number>

The y position of the object when it is on a map.

=item I<map> (internal)

The map the object is on.

=item I<invisible> <number>

If the <number> is greater than 0 the object is invisible.
For players this field reflects the duration of the invisibility
and is decreased every tick by 1.

For non-player objects this field is not changed by server ticks.

=item I<glow_radius> <number>

This field indicates how far an object glows. Default is a radius of 0 (no
glowing at all). Negative glow radii darken areas - currently, negative
glow radii are stronger than positive ones.

=item I<speed> <float>

If this field is greater than MIN_ACTIVE_SPEED (~0.0001) the object is placed
on the active object list and will be processed each tick (see also speed_left!).

If I<speed> drops below the MIN_ACTIVE_SPEED the object is removed
from the active object list and it won't experience any processing per tick.

Negative speed settings in archetypes and files cause a speed_left
randomisation on load or instantiatian, but for calculations, the absolute
value is used always.

=item I<speed_left> <float>

If this field is greater than 0 and the object is on the
active list (mostly means it's speed is also greater than 0):

   - speed_left is decreased by 1
   - and this object is processed and experiences a server tick.

If the object is on the active list and I<speed_left> is lower or
equal to 0 the absolute value of the I<speed> is added to I<speed_left>
on the end of the tick.

This means: the lower I<speed> is (but still above MIN_ACTIVE_SPEED)
the more seldom the object is processed. And the higher I<speed> is
the more often the object is processed.

=item I<connected> <identifier>

When this field is set the object will be linked to a connection with the
same <identifier>. What happens when the connection is 'activated' depends on the
type of the object.

FLAG_ACTIVATE_ON_PUSH and FLAG_ACTIVATE_ON_RELEASE they will control
when to activate the object, see description of these below for further details.

=item I<no_drop> (0|1)

Sets the flag FLAG_NO_DROP.
See Flags section below.

=item I<applied> (0|1)

Sets the flag FLAG_APPLIED.
See Flags section below.

=item I<is_used_up> (0|1)

Sets the flag FLAG_IS_USED_UP.
See Flags section below.

=item I<changing> (0|1)

Sets the flag FLAG_CHANGING.
See Flags section below.

=item I<auto_apply> (0|1)

Sets the flag FLAG_AUTO_APPLY.
See Flags section below.

=item I<no_steal> (0|1)

Sets the flag FLAG_NO_STEAL.
See Flags section below.

=item I<reflecting> (0|1)

Sets the flag FLAG_REFLECTING.
See Flags section below.

=item I<reflect_spell> (0|1)

Sets the flag FLAG_REFL_SPELL.
See Flags section below.

=item I<no_skill_ident> (0|1)

Sets the flag FLAG_NO_SKILL_IDENT.
See Flags section below.

=item I<activate_on_push> (0|1) (default: 1)

Sets the flag FLAG_ACTIVATE_ON_PUSH.
See Flags section below.

=item I<activate_on_release> (0|1) (default: 1)

Sets the flag FLAG_ACTIVATE_ON_RELEASE.
See Flags section below.

=item I<is_lightable> (0|1)

Sets the flag FLAG_IS_LIGHTABLE.
See Flags section below.

=item I<editable> (more than deprecated)

This field had a special meaning for crossedit, which used parts
of the server code for editing. Wherever you see this field being
set in an archetype ignore it and/or remove it. No code interprets this
field anymore.

=item I<last_heal>, I<last_sp>

For monsters and other living stuff that heals or regenarates hp or sp,
these contain the fractional part of any healing that couldn't be applied
to hp and sp yet.

=back

=head3 Flags

Here are the effects of the flags described.

=over 4

=item FLAG_NO_DROP

An object can't be picked up and dropped.

=item FLAG_APPLIED

This flag mostly states whether this object has been 'applied' by the player.
For objects that are applied by the code or have this flag set in the archetype
it mostly means 'this object is active'.

For example the player adjustments of the I<hp>/I<sp>/I<grace> fields and inheritance
of flags from objects in his inventory is toggled by this flag.

=item FLAG_IS_USED_UP

This flag controls whether an object is 'used up'. If it is set I<food>
is decreased by 1 each tick, and will be removed when I<food> is lower or equal 0.

If also the flag FLAG_APPLIED is set, the I<duration> field controls whether
this object is removed or not, see the B<FORCE> type below for the meaning
of the duration field in this context.

If FLAG_APPLIED is not set the object is destroyed.

=item FLAG_CHANGING

If the I<state> field of the object is 0 the object will be processed periodically
(if I<speed> is set). If the I<state> field is 1 it won't be processed.

This flag indicates that the object is changing into a different object.
The object has to have the I<other_arch> field set. The object the changing object
changes into is derived from the archetype in I<other_arch>.

When the object does not have FLAG_ALIVE set the I<food> field will be decremented
each time the object is processed, and if I<food> reaches 0 one new object will be generated.

When the object has FLAG_ALIVE set the I<food> field is the number of objects that
will be generated.

After the new object is created the I<hp> field from the old object is copied into
the new one.

=item FLAG_IS_A_TEMPLATE (internal use)

This flag is set on the inventory of generators like B<CREATOR>s and B<CONVERTER>s,
or other objects that have the flags FLAG_GENERATOR and FLAG_CONTENT_ON_GEN set.

=item FLAG_AUTO_APPLY

This flag has currently only meaning for the B<TREASURE> type, see below.

=item FLAG_ACTIVATE_ON_PUSH

This flag has only meaning for objects that can be linked together
by the I<connected> field and controls wether the object should
be activated when the connection is 'pushed' or it is 'released'.

What 'pushed' and 'released' means depends on the object that
activates the connection.

This flag is by default on.

=item FLAG_ACTIVATE_ON_RELEASE

This flag has only meaning for objects that can be linked together
by the I<connected> field and controls wether the object should
be activated when the connection is 'pushed' or it is 'released'.

What 'pushed' and 'released' means depends on the object that
activates the connection.

This flag is by default on.

=item FLAG_NO_STEAL

When this flag is set this object can't be stolen. The flag will be
reset once the object is placed on a map.

When this flag is set on a monster it can defend attempts at stealing
(but in this context the flag is only used internally).

=item FLAG_NO_SKILL_IDENT

This flag is mostly used internal and prevents unidentified objects
(objects which don't have FLAG_IDENTIFIED set) being identified
multiple times by skills.

This flag is used to mark objects which were unsuccessfully identified by a
players skill. So that multiple tries of identifying aren't more effective than
one.

=item FLAG_REFLECTING

This flag is used by spell effects (eg. SP_BOLT), B<THROWN_OBJ> and B<ARROW>
to indicate whether this object reflects off walls.

=item FLAG_REFL_SPELL

This flag indicates whether something reflects spells, like spell reflecting
amuletts.

=item FLAG_IS_LIGHTABLE

This flag indicates whether a B<LIGHTER> can light this object. See also the
description of the B<LIGHTER> type. How easy you can light an item depends
partially on the material of the object.

=item FLAG_MONSTER

Enables NPC behaviour in general (both monster AI and friendly AI). Numerous fields
change their meaning, including:

=over 4

=item I<wis>

Governs the "wake-up radius" - the radius within a monster detects an enemy.

Also, I<wis> governs pathfinding intelligence: 8 and up means the monster
will partake in basic smell finding. 10 and up additionally spreads smell
knowledge, and 15 and up additionally will try to perturb the path as to
find shortcuts.

=back

=back

=head2 Description of type specific fields and behaviour

The beginning of the headers of the following subsection
are the server internal names for the objects types, see include/define.h.

=head3 B<TRANSPORT> - type 2 - Player transports

This type is implemented by the transport extension and has currently no special
fields that affect it.

=head3 B<ROD>, B<HORN> - type 3, 35 - Rods that fire spells

Rods contain spells and can be fired by a player.

=over 4

=item I<level> <number>

This field is used for calculating the spell level that can be fired
with this rod, it's also the maximum level of the spell that can be fired.
The level of the spell that is being fired depends mostly on
the 'use magic item' skill level of the player and 1/10 of the level of the
rod is added as bonus.

=item I<hp> <number>

The amount of spellpoints this rod has left. Recharges at a rate of C<1 +
maxhp/10> per tick.

=item I<maxhp> <number>

The maximum amount of spellpoints this rod has.

=item I<skill> <skill name>

This field determines which skill you need to apply this object.

=back

=head3 B<TREASURE> - type 4 - Treasures

This type of objects are for random treasure generation in maps.
If this object is applied by a player it will replace itself with it's
inventory. If it is automatically applied
generate a treasure and replace itself with the generated treasure.

Chests are also of this type, their treasures are generated by
the auto apply code on map instantiation.

=over 4

=item I<hp> <number>

The number of treasures to generate.

=item I<exp> <level>

If FLAG_AUTO_APPLY is not set the exp field has no further meaning
and the difficulty for the treasurecode only depends on the maps difficulty,
otherwise the exp field has the following meaning:

If this field is not 0 it is passed as the difficulty
to the treasure generation code to determine how good, how much
worth a treasure is or what bonuses it is given by the treasure code.

If this field is not set or 0 the difficulty of the map is passed to the treasure
generation code.

=item I<randomitems> <treasurelist>

The treasurelist to use to generate the treasure which is put in the
treasure objects inventory.

=back

=head3 B<POTION> - type 5 - Potions for drinking and other nastynesses

These objects contain a spell and will emit it on apply, which most
of the time has the meaning of 'drinking'.

If no resistancy field, stat field or attacktype is set and no spell
is put in the potion by the sp field or the randomitems the
potion will become an artifact and the artifact code decides which kind
of potion will be generated.

If the potion has FLAG_CURSED or FLAG_DAMNED set the usage of this potion
will yield an explosion and hurt the player.

=over 4

=item I<Str>, I<Dex>, I<Con>, I<Int>, I<Wis>, I<Cha>, I<Pow> <number>

These stat fields determine how many stat points the player gets
when he applies this potion.

If FLAG_CURSED or FLAG_DAMNED is set the player will loose that many stat points.

=item I<sp> <number>

If this field is set and the randomitems field is not set
the field is interpreted as spell number, please look the right
number up in common/loader.C.

If this field is set the randomitems field will be unset by the
map loading code.

=item I<attacktype> <attacktype>

This field has some special meaning in potions, currently the
bits for AT_DEPLETE and AT_GODPOWER control whethere this is a
restoration potion or improvement potion.
See include/attackinc.h for the bits of these types.

If AT_DEPLETE is set the player will be restored and the "depletion"
will be removed from him. If the potion has FLAG_CURSED or FLAG_DAMNED
set the player will be drained a random stat by inserting an "depletion"
into him.

If AT_GODPOWER is enabled the player will gain +1 maxvalue in his hp, sp or grace stat.
When the potion has FLAG_CURSED or FLAG_DAMNED set he will loose one in one of these stats.

=item I<resist_RESISTANCY> <number>

If this stat is set and no spell is in the potion the potion
will create a force that give the player this specific resistancy.
The forces type will be changed to POTION_EFFECT (see POTION_EFFECT type below)
and the potion will last 10 times longer than the default force archetype
FORCE_NAME (at the moment of this writing spell/force.arc).

=item I<randomitems> <treasurelist>

The inventory/spell of the potion will be created by calling the treasure code
with the treasurelist specified here. (I guess it's highly undefined what
happens if there is not a spell in the potions inventory).

=item I<on_use_yield> <archetype>

When this object is applied an instance of <archetype> will be created.

=item I<subtypes> <potion subtype>

see include/spells.h for possible potion subtypes, there are currently 4:

=over 4

=item POT_SPELL

Unused, default behaiour of a potion.

=item POT_DUST

This potion can be thrown to cast the spell that it has in it's inventory,
the behaviour is not defined if there is not a B<SPELL> in the inventory and the
server will log an error.

=item POT_FIGURINE

Unused, default behaiour of a potion.

=item POT_BALM

Unused, default behaiour of a potion.

=back

=back

=head3 B<FOOD> - type 6 - Edible stuff

This is for objects that are representing general eatables like
beef or bread.

The main difference between B<FOOD>, B<FLESH> and B<DRINK> is that they
give different messages.

The specialty of B<FLESH> is that it inherits the resistancies of the
monsters it was generated in and will let dragons raise their resistancies
with that. If the monster has the B<POISON> attacktype the B<FLESH>
will change into B<POISON>.

If a player runs low on food he will grab for B<FOOD>, B<DRINK> and B<POISON>
and if he doesn't find any of that he will start eating B<FLESH>.

=over 4

=item I<title> <string>

If the food has B<title> set or is cursed it is considered 'special', which
means that the fields I<Str>, I<Dex>, I<Con>, I<Int>, I<Wis>, I<Pow>,
I<resist_RESISTANCY>, I<hp> and I<sp> are interpreted and have further effects
on the player.

The higher the I<food> field is the longer the improvement of the player lasts
(except for I<hp> and I<sp>).

=item I<food> <number>

This is the amount of food points the player gets when he eats this.

=item I<on_use_yield> <archetype>

When this object is applied an instance of <archetype> will be created.

=back

=head3 B<POISON> - type 7 - Poisonous stuff

This type is for objects that can poison the player when he drinks/applies it.
When applied it will hit the attacked with AT_POISON and will create
a B<POISONING> object in the one who was hit.

=over 4

=item I<level> <number>

This field affects the probability of poisoning. The higher the level difference
between the one who is hit and the poision the more probable it is the attacked
one will be poisoned.

=item I<slaying> <race>

This field has the usual meaning of 'slaying', when the
poisoned's race matches the I<slaying> field the damage done by the poison
is multiplied by 3.

=item I<hp> <number>

This is the amount of damage the player will receive from applying this.  The
attacktype AT_POISON will be used to hit the player and the damage will
determine the strenght, duration and depletion of stats of the poisoning.  The
created B<POISONING> object which is being placed in the one who was attacked will
get the damage from this field (which is maybe adjusted by slaying or the
resistancies).

=item I<food> <number>

1/4 of <number> will be drained from the players I<food>.

=item I<on_use_yield> <archetype>

When this object is applied an instance of <archetype> will be created.

=back

=head3 B<BOOK> - type 8 - Readable books

This type is basically for representing text books in the game.

Reading a book also identifys it (if FLAG_NO_SKILL_IDENT is not set).

=over 4

=item I<msg> <text>

This is the contents of the book. When this field is unset
at treasure generation a random text will be inserted.

=item I<skill> <skill name>

The skill required to read this book. (The most resonable
skill would be literacy).

=item I<exp> <number>

The experience points the player get for reading this book.

=item I<subtype> <readable subtype>

This field determines the type of the readable.
Please see common/readable.C in the readable_message_types table.

=back

=head3 B<CLOCK> - type 9 - Clocks

This type of objects just display the time when being applied.

=head3 B<VEIN> - type 10 - item veins for mining

Provides a place to apply to mining skill to.

=item I<other_arch> <archname>

The architecture to create on a successful mine.

=item I<food> <number>

The number of items to produce from this vein.

=item I<ac> <percentage>

The base chance of getting an item. 

=item I<race> <identifier>

Race of required extraction tools.

=back

=head3 B<LIGHTNING> - type 12 - Lightnings (DEPRECATED: see B<SPELL_EFFECT> subtype SP_BOLT)

This is a spell effect of a moving bolt. It moves straigt forward
through the map until something blocks it.
If FLAG_REFLECTING is set it even reflects on walls.

FLAG_IS_TURNABLE should be set on these objects.

=over 4

=item I<move_type> <movetype>

This field affects the move type with which the lightning moves through
the map and which map cells will reflect or block it.

=item I<attacktype> <attacktype>

The attacktype with which it hits the objects on the map.

=item I<dam> <number>

The damage this bolt inflicts when it hits objects on the map.

=item I<Dex> <number>

This is the fork percentage, it is reduced by 10 per fork.
And the I<dam> field is halved on each fork.

=item I<Con> (internal)

This value is a percentage of which the forking lightning
is deflected to the left. This value should be mostly used internally.

=item I<duration> <number>

The duration the bolt stays on a map cell. This field is decreased each time
the object is processed (see the meaning of I<speed> and I<speed_left> fields in
the generic object field description).

=item I<range> <number>

This is the range of the bolt, each space it advances this field is decreased.

=back

=head3 B<ARROW> - type 13 - Arrows

This is the type for objects that represent projectiles like arrows.
The movement of B<THROWN_OBJ>s behave similar to this type.

Flying arrows are stopped either when they hit something blocking
(I<move_block>) or something which is alive.
If it hits something that is alive, which doesn't have FLAG_REFL_MISSILE
set, it will inflict damage. If FLAG_REFL_MISSILE is set it will inflict
damage with a small chance which is affected by the I<level> field of the arrow.

If FLAG_REFLECTING is set on the arrow it will bounce off everything
that is not alive and blocks it's movement.

When an arrow is being shot it's I<dam>, I<wc>, I<attacktype>, I<slaying>
fields will be saved in the I<sp>, I<hp>, I<grace> and I<spellarg> fields of
the object, to restore them once the arrow has been stopped.

=over 4

=item I<dam> <number>

The amount of damage that is being done to the victim that gets hit.
This field is recomputed when the arrow is fired and will consist
of the sum of a damage bonus (see description of the B<BOW> type),
the arrows I<dam> field, the bows I<dam> field, the bows I<magic> field
and the arrows I<magic> field.

=item I<wc> <number>

The weapon class of the arrow, which has effect on the probability of hitting.

It is recomputed when the arrow is being fired by this formula:

  wc = 20 - bow->magic - arrow->magic - (skill->level or shooter->level)
          - dex_bonus - thaco_bonus - arrow->stats.wc - bow->stats.wc + wc_mod

When the arrow is not being shot by an player dex_bonus and thaco_bonus and the
level is not added.

wc_mod is dependend on the fire mode of the bow.  For a more detailed
explanation of dex_bonus, thaco_bonus and wc_mod please consult the code.

=item I<magic> <number>

This field is added to the damage of the arrow when it is shot and
will also improve it's I<speed> by 1/5 of it's value.

=item I<attacktype> <attacktype>

Bitfield which decides the attacktype of the damage, see include/attackinc.h
On fireing the I<attacktype> of the bow is added to the arrows I<attacktype>.

=item I<level> <number> (interally used)

The level of the arrow, this affects the probability of piercing FLAG_REFL_MISSILE,
see above in the B<ARROW> description.

The I<level> is set when the arrow is fired to either the skill level or the
shooters level.

=item I<speed> <number> (internal)

This field shouldn't be set directly in the archetype, the arrow will get it's
I<speed> from the bow.  This fields value has to be at least 0.5 or otherwise the
arrow will be stopped immediatly.

On fireing the I<speed> of the arrow is computed of 1/5 of the
sum of the damage bonus (see BOW), bow I<magic> and arrow I<magic>. After that 1/7
of the bows I<dam> field is added to the I<speed> of the arrow.

The minimum I<speed> of an arrow is 1.0.

While flying the arrows I<speed> is decreased by 0.05 each time it's moved.

If the I<speed> is above 10.0 it goes straight through the creature it hits and
its I<speed> is reduced by 1. If the I<speed> is lower or equal 10.0 the arrow is
stopped and either stuck into the victim (see I<weight> field description) or
put on its map square (if it didn't break, see description of the I<food> field).

=item I<weight> <number>

This field is the weight of the arrow, if I<weight> is below or equal 5000 (5 kg)
the arrow will stick in the victim it hits. Otherwise it will fall to the ground.

=item I<food> <number>

The breaking percentage. 100 (%) means: breaks on usage for sure.

=item I<inventory> (internal)

If the flying/moving object has something in it's inventory and it stops, it
will be replaced with it's inventory. Otherwise it will be handled as usual,
which means: it will be calculated whether the arrow breaks and it will be
reset for reuse.

=item I<slaying> <string>

When the bow that fires this arrow has it's I<slaying> field set it is copied
to the arrows I<slaying> field. Otherwise the arrows I<slaying> field remains.

=item I<move_type> <movetype> (internally used)

This field is set when the arrow is shot to MOVE_FLY_LOW.

=item I<move_on> <movetype> (internally used)

This field is set when the arrow is shot to MOVE_FLY_LOW and MOVE_WALK.

=item I<race> <string>

The I<race> field is a unique key that assigns arrows, bows and quivers. When
shooting an arrow the bows I<race> is used to search for arrows (which have the
same I<race> as the bow) in the players inventory and will recursively search in
the containers (which are applied and have the same I<race> as the bow and the arrow).

=back

=head3 B<BOW> - type 14 - Bows, those that fire B<ARROW>s

TODO, but take into account B<ARROW> description above!

=head3 B<WEAPON> - type 15 - Weapons

This type is for general hack and slash weapons like swords, maces
and daggers and and ....

=over 4

=item I<weapontype> <type id>

decides what attackmessages are generated, see include/define.h

=item I<attacktype> <bitmask>

bitfield which decides the attacktype of the damage, see include/attackinc.h

=item I<dam> <number>

amount of damage being done with the attacktype

=item I<item_power> <level>

the itempower of this weapon.

=item I<name>

the name of the weapon.

=item I<level> (internal)

The improvement state of the weapon.
If this field is greater than 0 the I<name> field starts with the
characters name who improved this weapon.

=item I<last_eat> (internal)

This seems to be the amount of improvements of a weapon,
the formular for equipping a weapon seems to be (server/apply.C:check_weapon_power):

  ((who->level / 5) + 5) >= op->last_eat

=item I<last_sp>

the weapon speed (see magic description)

=item I<food> <number>

addition to food regeneration of the player

=item I<hp> <number>

addition to health regeneration

=item I<sp> <number>

addition to mana regeneration

=item I<grace> <number>

addititon to grace regeneration

=item I<gen_sp_armour> <number>

the players I<gen_sp_armour> field (which is per default 10) is being added the
<number> amount.  gen_sp_armour seems to be a factor with which gen_sp in
do_some_living() is multiplied:  gen_sp *= 10/<number> meaning: values > 10 of
I<gen_sp_armour> limits the amout of regenerated spellpoints.

Generally this field on weapons is in ranges of 1-30 and decides the slowdown of the
I<sp> regeneration.

=item I<body_BODYSLOT>

The part/slot of the body you need to use this weapon, possible values for
C<BODYSLOT> should be looked up in common/item.C at body_locations.

The value (in the range C<-7..7>) gives the number of those body slots
used up by the item (if negative) or the number of body slots this object
has (if positive, e.g. for monsters or players). The special value C<0>
indicates that this object cannot equip items requiring these body slots.

=item I<resist_RESISTANCY> <number>

this is the factor with which the difference of the players resistancy and 100%
is multiplied, something like this:

   additional_resistancy = (100 - current_resistancy) * (<number>/100)

if <number> is negative it is added to the total vulnerabilities,
and later the total resistance is decided by:

  'total resistance = total protections - total vulnerabilities'

see also common/living.C:fix_player.

=item I<path_(attuned|repelled|denied)>

this field modifies the pathes the player is attuned to, see include/spells.h PATH_*
for the pathes.

=item I<luck> <number>

this luck is added to the players I<luck>

=item I<move_type>

if the weapon has a I<move_type> set the player inherits it's I<move_type>

=item I<exp> <number>

the added_speed and bonus_speed of the player is raised by <number>/3.
if <number> < 0 then the added_speed is decreased by <number>

=item I<weight>

the weight of the weapon

=item I<magic>

the I<magic> field affects the amounts of the following fields:

  - wc : the players wc is adjusted by: player->wc -= (wc + magic)

  - ac : the players ac is lowered by (ac + magic) if (player->ac + magic) > 0

  - dam: the players dam is adjusted by: player->dam += (dam + magic)

  - weapon speed (last_sp): weapon_speed is calculated by: (last_sp * 2 - magic) / 2
                            (minium is 0)

=item I<ac> <number>

the amount of ac points the player's I<ac> is decreased when applying this object.

=item I<wc> <number>

the amount of wc points the player's I<wc> is decreased when applying this object.

=back

=head4 Player inherits following flags from weapons:

   FLAG_LIFESAVE
   FLAG_REFL_SPELL
   FLAG_REFL_MISSILE
   FLAG_STEALTH
   FLAG_XRAYS
   FLAG_BLIND
   FLAG_SEE_IN_DARK
   FLAG_UNDEAD

=head3 B<GRIMREAPER> - type 28 - Grimreapers

These type are mostly used for monsters, they give the
monster the ability to dissapear after 10 hits with AT_DRAIN.

=over 4

=item I<value> <number>

This field stores the hits the monster did yet.

=back

=head3 B<CREATOR> - type 42 - Object creators

Once a creator is activated by a connection it creates a number of objects
(cloned from it's inventory or a new object derived from the archetype
named in the other_arch slot).

If FLAG_LIVESAFE is set the number of uses is unlimited.

=over 4

=item I<hp> <number>

If FLAG_LIVE_SAVE is not set it is the absolute number of times the creator can
be used.

=item I<speed> <float>

If I<speed> is set the creator will create an object periodically,
see I<speed> and I<speed_left> fields in the general object field description
for more details.

=item I<slaying> <string>

If set the generated object's name and title will be set to this.

=item I<other_arch> <string>

If the inventory of the creator is empty new objects will be derived from the
archetype named by <string>.

=item I<connected> <identifier>

See generic object field description.

=back

=head3 B<SKILL> - type 43 - Skills

This type is basically for representing skills in the game.

=over 4

=item I<subtype> <skill number>

=item I<skill> <string>

The skill identifier used by other items, usually the skill name

=item I<level> <percentage>

not used?

=item I<exp> <number>

Base amount of experience in a skill, for skills not starting at zero.

=item I<expmul> <float>

Experience is multiplied by this factor.

=item I<cached_sp> <integer>

Used internally by the server (cannot be used in files).

=back

=head3 B<DRINK> - type 54 - Drinkable stuff

See B<FOOD> description.

=head3 B<CHECK_INV> - type 64 - Inventory checkers

This object checks whether the player has a specific item in his
inventory when he moves above the inventory checker. If the player has
the item (or not, which can be controlled with a flag) a connection will be triggered.

If you set I<move_block> you can deny players and monsters to reach the space where
the inventory checker is on, see I<move_block> description below.

The conditions specified by I<hp>, I<slaying> and I<race> are OR concationated.
So matching one of those conditions is enough.

=over 4

=item I<move_block> <move type bitmask>

If you set this field to block a movetype the move code will block any moves
onto the space with the inventory checker, IF the moving object doesn't have
(or has - if I<last_sp> = 0) the item that the checker is searching for.

=item I<last_sp> (0|1)

If I<last_sp> is 1 'having' the item that is being checked for will
activate the connection or make the space with the checker non-blocking.
If I<last_sp> is 0 'not having' the item will activate the connection
or make the space with the checker non-blocking.

=item I<last_heal> (0|1)

If I<last_heal> is 1 the matching item will be removed if the inventory checker
activates a connection and finds the item in the inventory.

(A inventory checker that blocks a space won't remove anything from inventories)

=item I<hp> <number>

If this field is not 0 the inventory checker will search for an object
with the type id <number>.

=item I<slaying> <string>

If this field is set the inventory checker will search for an object that
has the same string in the I<slaying> field (for example a key string of a key).

=item I<race> <string>

If this field is set the inventory checker will search for an object which
has the archetype name that matches <string>.

=item I<connected> <identifier>

This is the connection that will be activated. The connection is
'pushed' when someone enters the space with the inventory checker,
and it is 'released' when he leaves it.

See also the description of the I<connected> field in the generic object
field section.

=back

=head3 B<MOOD_FLOOR> - type 65 - change mood of monsters

speed == 0 for triggered mood changes, speed != 0 for non-triggered mood
changes.

 (based on value that last_sp takes):
 0:  'furious'      Makes all monsters aggressive
 1:  'angry'        As above but pets are unaffected
 2:  'calm'         Makes all monsters unaggressive
 3:  'sleep'        Puts all monsters to sleep
 4:  'charm'        Makes monster into a pet of person
                    who triggers the square. This setting
                    is not enabled for continous operation
 5:  'destroy mons' destroy any monsters on this space
 6:  'destroy pets' destroy friendly monsters on this space

=head3 B<FLESH> - type 72 - Organs and body parts

See B<FOOD> description.

=head3 B<MISC_OBJECT> - type 79 - Misc. objects

A type for any object that has no special behaviour.

=head3 B<DUPLICATOR> - type 83 - Duplicators or: Multiplicators

This type of objects multiplies objects that are above it when it is activated.
You can even multiply by 0, which will destroy the object.

=over 4

=item I<level> <number>

The multiplicator, if set to 0 or lower it will destroy the objects above it.

=item I<other_arch> <string>

The archetype name of the objects that should be multiplied.

=item I<connected> <identifier>

See generic object field description.

=back

=head3 B<HOLE> - type 94 - Holes

B<HOLE>s are holes in the ground where objects can fall through.  When the hole
opens and/or is completly open all objects above it fall through (more
precisely: if their head is above the hole).

When the B<HOLE> is activated it's speed is set to 0.5.

These holes can only transfer the one who falls through to other coordinates
on the same map.

=over 4

=item I<maxsp> (0|1)

This field negates the state of the connection: When maxsp is 1 the pit will
open/close when the connection is deactivated.  Otherwise it will open/close
when the connection is activated. This field only has effect when the
connection is triggered. So if you put a closed hole on a map, and the
connection is deactivated, and I<maxsp> is 1 the hole will remain closed until the
connection was triggered once.

=item I<connected> <identifier>

This is the connection id, which lets the hole opening or closing when
activated. The flags FLAG_ACTIVATE_ON_PUSH and FLAG_ACTIVATE_ON_RELEASE control
at which connection state the object is activated.

For example: if FLAG_ACTIVATE_ON_RELEASE is set to 0 the hole won't react when
the connection is released.

=item I<wc> <number> (internal)

This is an internal field. If it is greater than 0 it means that the hole is not
yet fully open. More preciesly: this field is the animation-step and if it is
set to the 'closed' step of the animation the hole is closed and if it is on
the 'open' animation step (I<wc> = 0), the hole is open.

=item I<sp> <number>

The destination y coordinates on the same map.

=item I<hp> <number>

The destination x coordinates on the same map.

=back

=head3 B<POISONING> - type 105 - The poisoning of players and monsters

This type is doing the actual damage to the ones who were attacked
via AT_POISON (or drank B<POISON>).

The duration is handled via the FLAG_IS_USED_UP mechanism (please look
there for details).

=over 4

=item I<dam> <number>

Each time the poisoning is proccessed (which is determined by the I<speed> and
I<speed_left> fields, see the general object fields description above) it hits
the player with <number> damage and the AT_INTERNAL attacktype (means: it will
simply hit the player with no strings attached).

=item I<food> <number>

Just a note: The posioning is removed if I<food> == 1 and not if
the whole I<duration> is up, because the B<POISONING> code has to remove
the poison-effects from the player before the FLAG_IS_USED_UP mechanism
deletes the B<POISONING> object.

=back

=head3 B<FORCE> - type 114 - Forces

Forces are a very 'thin' type. They don't have much behaviour other than
disappearing after a time and/or affecting the player if they are in his
inventory.

Forces only take effect on the player if they have FLAG_APPLIED set.

Whether the I<duration> field is processed or not per tick is controlled by the
I<speed> and I<speed_left> fields. Look above in the generic object field description.

NOTE: If FLAG_IS_USED_UP is set on a B<FORCE> it's I<food> field will also
interpreter like described in the description of the FLAG_IS_USED_UP flag.
BUT: If I<food> reaches 0 before I<duration> and FLAG_APPLIED is set, the force
will still last for I<duration>.  If the FLAG_APPLIED is not set the force is
removed when I<food> reaches 0.  Generally this means: FLAG_IS_USED_UP doesn't
have good semantics on forces, try to avoid it.

=over 4

=item I<duration>

While this field is greater than 0 the force/object is not destroyed.  It is
decreased each tick by 1.

If it reaches 0 the force/object is destroyed.

This field can have this meaning for B<any> object if that object has
FLAG_IS_USED_UP and FLAG_APPLIED set. See the description of FLAG_IS_USED_UP
what happens then.

=back

=head3 B<POTION_EFFECT> - type 115 - Potion effects (resistancies)

This object is generated by the B<POTION> code when the potion is a resistance
giving potion. It has mainly the same behaviour as a B<FORCE>.

The specialty of the potion effect is that the resistancy it gives is absolute,
so if you drink a resistancy potion of fire+60 you will get 60% absolute resistancy to
fire.

Multiple potion effects only give you the maximum of their resistancy.
